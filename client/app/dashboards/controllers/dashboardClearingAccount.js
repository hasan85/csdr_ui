'use strict';

angular.module('cmisApp')
  .controller('DashboardClearingAccount', ['$scope',
    function ($scope) {
      $scope.viewCodes = {
        activeTasks: 6103,
        candidateTasks: 6104,
        finishedTasks: 6106,
        completedTasks: 6112
      }
    }]);
