'use strict';

angular.module('cmisApp')
  .controller('DashboardAccountsController', ['$scope',
    function ($scope) {
      $scope.viewCodes = {
        issuers: 1202,
        shareholders: 6111,
        members: 6110,
        accounts: 1403
      }
    }]);
