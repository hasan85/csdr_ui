'use strict';

angular.module('cmisApp')
  .controller('DashboardShareholderAccountStatementController', ['$scope',
    "recordService",
    function ($scope, recordService) {
    recordService.getMyShareholderAccounts().then(function (response) {
      $scope.accountNumbers = response.data;
    });
      $scope.cda = function (num) {
        recordService.dontKnowHowToPassDataBetweenController = num;
      };

      $scope.viewCodes = {
        accStatement: 4453
      }
    }]);
