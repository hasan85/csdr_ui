'use strict';

angular.module('cmisApp').controller('DashboardTasksController', ['$scope', 'operationService', '$rootScope', function ($scope, operationService, $rootScope) {
  $scope.viewCodes = {
    activeTasks: 6103,
    candidateTasks: 6104,
    finishedTasks: 6106,
    completedTasks: 6112
  };
  $rootScope.$on('taskCompleted', function () {
    operationService.getDashBoardContents("TASKS_DASHBOARD").then(function (data) {
      $scope.dashboardContent = data;
    });
  });

  $rootScope.$on("INCOMING_TASK", function () {
    try {
      $scope.dashboardContent.candidateTaskCount++;
    } catch (e) {

    }
  });

  $rootScope.$on("CLAIMED_TASK", function () {
    try {
      $scope.dashboardContent.candidateTaskCount--;
    } catch (e) {

    }
  });

}]);
