'use strict';

angular.module('cmisApp')
  .controller('DashboardMemberCashAccountsController', ['$scope',
    function ($scope) {


      $scope.sum = function(data){
        var sum = 0
          angular.forEach(data, function(obj){
              sum+= obj.amount
          })

        return sum
      }
    }]);

