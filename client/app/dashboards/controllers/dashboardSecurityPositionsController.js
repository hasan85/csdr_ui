'use strict';

angular.module('cmisApp')
  .controller('DashboardSecurityPositionsController', ['$scope',
    function ($scope) {
      $scope.viewCodes = {
        securityPositions: 6119
      }
    }]);
