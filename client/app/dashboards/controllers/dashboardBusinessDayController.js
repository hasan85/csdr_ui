'use strict';

angular.module('cmisApp')
  .controller('DashboardBusinessDayController', ['$scope',
    function ($scope) {
      $scope.$watch("dashboardContent.date", function(newVal) {
        if(newVal) {
          $scope.dashboardContent.date = kendo.toString(kendo.parseDate($scope.dashboardContent.date), "dd MMMM, yyyy");
        }
      })
    }]);
