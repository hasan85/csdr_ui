'use strict';

angular.module('cmisApp')
  .controller('DashboardClearingLimitsController', ['$scope',
    function ($scope) {
      $scope.viewCodes = {
        clearingLimits: 4305
      }
    }]);
