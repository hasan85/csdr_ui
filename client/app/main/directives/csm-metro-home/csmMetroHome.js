'use strict';

angular.module('cmisApp')
  .directive('csmMetroHome', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {

        var setTilesAreaSize = function () {
          var groups = $(".tile-group");
          var tileAreaWidth = 80;
          $.each(groups, function (i, t) {
            var childWidth = 0;
            var childCount = 0;

            $(t).find('.tile-container div[data-role="tile"]').each(function (i, j) {

              childCount++;
              childWidth += parseInt($(j).outerWidth());

            });
            if (childCount != 3) {
              childWidth += 79;
            }
            var ratio = Math.ceil(childWidth / 240);
            var minColWidth = 160;

            //if ($(t).hasClass('dashboard')) {
            //  $(this).css({
            //    width: 320
            //  });
            //}
            //else
            //if ($(t).hasClass('operations')) {
            //  $(this).css({
            //    width: 320
            //  });
            //}
            //else {
              $(this).css({
                width: (minColWidth * ratio)
              });
            //}

            var outerWidth = parseInt($(t).outerWidth());
            tileAreaWidth += ( outerWidth + 80);

          });

          $("#index").css({
            width: tileAreaWidth
          });
        };

        scope.$on('tilesCreated', function () {
          setTilesAreaSize();
        });


        $(document).ready(function () {
          updateMaxHeight();
          $(window).on("resize", updateMaxHeight);
        });


        function updateMaxHeight() {
          // $(".content").css("height", parseInt($(window).height() - (110)));
         // $(".content").css("height", parseInt($(window).height() - (110)));
        //  $(".tile-group").css("height", parseInt($(window).height() - (110 + 70)));

        }

        $.StartScreen = function () {

          var plugin = this;
          var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

          plugin.init = function () {
            setTilesAreaSize();
            $('.prv, .nxt').click(function (e) {
              $('#home').stop().animate({
                scrollLeft: (this.className.match('nxt') ? '+=' : '-=') + 600
              }, 500);
            });
          };

          var setTilesAreaSize = function () {
            var groups = $('.tile-group');
            var tileAreaWidth = 80;
            $.each(groups, function (i, t) {
              if (width <= 640) {
                tileAreaWidth = width;
              } else {
                tileAreaWidth += $(t).outerWidth() + 80;
              }
            });

            $('.tile-area').css({
              width: tileAreaWidth
            });

          };

          plugin.init();
        };
        $.StartScreen();

      }
    };
  });
