'use strict';

angular.module('cmisApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/templates/main.html',
        controller: 'MainController'
      });
  });
