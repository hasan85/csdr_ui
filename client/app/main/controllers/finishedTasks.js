'use strict';

angular.module('cmisApp')
  .controller('FinishedTasksController', ['$scope', 'operationService', '$windowInstance', 'id', 'Loader',
    'gettextCatalog', 'appConstants', '$rootScope', 'helperFunctionsService', 'SweetAlert', '$http',
    function ($scope, operationService, $windowInstance, id, Loader, gettextCatalog, appConstants,
              $rootScope, helperFunctionsService, SweetAlert, $http) {

      var webServiceUrl = "/api/task-services/getMyFinishedTasks";

      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        result.success = true;

        return result;
      };
      $scope.params = {
        searchUrl: webServiceUrl,
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };
      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];
      $scope.task = {
        formName: 'taskFindForm',
        form: {},
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedTaskIndex: false,
        findData: findData
      };

      var findData = function (e) {

        if ($scope.task.searchResultGrid !== undefined) {
          $scope.task.searchResultGrid.refresh();
        }
        if ($scope.task.formName.$dirty) {
          $scope.task.formName.$submitted = true;
        }

        if ($scope.task.formName.$valid || !$scope.task.formName.$dirty) {

          $scope.task.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.task.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            formData.startStartDate = $scope.task.form.startStartDate ? $scope.task.form.startStartDateObj : null;
            formData.startFinishDate = $scope.task.form.startFinishDate ? $scope.task.form.startFinishDateObj : null;
            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.task.searchResult.data = data;
                } else {
                  $scope.task.searchResult.isEmpty = true;
                }
                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total ? data.total : 0
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });

          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };
      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view: false,
          print: false
        }
      };

      // Initialize scope
      $scope.view = {
        selectedTaskId: null,
        notAvailableTasks: false
      };


      // Get view data
      $scope.gridOptions = {

        selectable: true,
        dataSource: {
          transport: {
            read: function (e) {
              findData(e);
            }
          },
          schema: {
            data: "Data",
            total: "Total",
            model: {
              id: "id",
              fields: {
                startDate: {type: "date"}
              }
            }
          },
          serverPaging: true,
          pageSize: 20,
          serverSorting: true,
          sort: {field: "startDate", dir: "desc"}
        },

        scrollable: true,
        pageable: true,
        columns: [
          {
            field: "processInstanceId",
            title: '#',
            width: '7rem'
          },
          {
            field: "name" + $rootScope.lnC,
            title: gettextCatalog.getString("Name")
          },
          {
            field: "startDate",
            title: gettextCatalog.getString("Start Date"),
            type: "date",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },
          {
            field: "shortDescription",
            title: gettextCatalog.getString("Short Description"),
            template: "<div title='#= shortDescription #'>#= shortDescription # </div>"
          }

        ]
      };

      $scope.findData = function () {
        var formValidationResult = validateForm(angular.copy($scope.task.form));
        if (formValidationResult.success) {
          if ($scope.task.searchResultGrid) {
            $scope.task.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }
      };
      // Get id of selected task

      // Open selected task
      var openTask = function () {

        Loader.show(true);
        operationService.openFinishedTask($scope.view.selectedTaskId).then(function (res) {
          Loader.show(false);
          operationService.openTask(res, appConstants.operationStates.completed);

        });
      };

      $scope.openTask = openTask;
      $scope.selectTask = function (data) {
        $scope.view.selectedTaskId = data.processInstanceId;
      };

      // Subscribe for grid double click event
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.task.searchResultGrid) {

          $scope.task.searchResultGrid.element.on('dblclick', function () {
            openTask();
          });
        }
      });


      $scope.taskDataGrid = function (dataItem) {

        return {
          selectable: true,
          dataSource: {
            data: dataItem.taskData,
            schema: {
              model: {
                id: "id",
                fields: {
                  name: {type: "string"},
                  createDate: {type: "date"},
                  endDate: {type: "date"},
                  assignee: {type: "string"}
                }
              }
            },
            serverSorting: true,
            serverPaging: true
          },

          scrollable: true,
          pageable: false,
          columns: [
            {
              field: "id",
              title: '#',
              width: '7rem'
            },
            {
              field: "name" + $rootScope.lnC,
              title: gettextCatalog.getString("Task")
            },
            {
              field: "createDate",
              title: gettextCatalog.getString("Create Date"),
              type: "date",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "endDate",
              title: gettextCatalog.getString("End Date"),
              type: "date",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "assigne",
              title: gettextCatalog.getString("Assignee")
            }
          ]
        };
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };


      $scope.resetForm = function () {
        $scope.task.form.name = null;
        $scope.task.form.shortDescription = null;
        $scope.task.form.startStartDate = null;
        $scope.task.form.startFinishDate = null;
        $scope.task.form.processInstanceId = null;

      };
    }]);
