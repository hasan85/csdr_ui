'use strict';

angular.module('cmisApp')
  .controller('MainController',
  [
    '$scope', '$http', '$socket', '$kWindow', 'userService', 'settingsService',
    'appConstants', 'gettextCatalog', '$rootScope', '$cookies', '$mdSidenav', 'operationService', 'Loader', 'taskbarService', 'SweetAlert', 'helperFunctionsService', "$timeout",
    function ($scope, $http, $socket, $kWindow, userService, settingsService,
              appConstants, gettextCatalog, $rootScope, $cookies, $mdSidenav, operationService, Loader, taskbarService, SweetAlert, helperFunctionsService, $timeout) {

      var $socketInstance;

      $rootScope.screenId = '';
      $scope.screenTypes = appConstants.screenTypes;

      $scope.$on('$destroy', function (event) {
        if($socketInstance) {
          $socketInstance.removeAllListeners();
        }
      });

      //flatten org node chain
      $scope.flattenedOrgNodeChain = [];

      (function () {
        $timeout(function () {
          if($rootScope.user.userContext){
            $rootScope.user.userContext.contexts = $rootScope.user.userContext.contexts.length === undefined ? [$rootScope.user.userContext.contexts] : $rootScope.user.userContext.contexts;

            $rootScope.user.userContext.contexts.map(function (context) {
              context.personContexts = context.personContexts.length === undefined ? [context.personContexts] : context.personContexts;

              context.personContexts.map(function (personContext) {
                personContext.orgNodeChain = personContext.orgNodeChain.length === undefined ? [personContext.orgNodeChain] : personContext.orgNodeChain;
                personContext.orgNodeChain.map(function (orgNode) {
                  $scope.flattenedOrgNodeChain.push(orgNode.code);
                })
              })
            });


            $socketInstance = $socket.connect();

            $socketInstance.on("connect", function() {
              console.log("Socket: connect");
              $socketInstance.emit('join', {userID: $rootScope.user.userContext.userID, orgNodes: $scope.flattenedOrgNodeChain});

              $socketInstance.on('INCOMING_TASK', function (payload) {
                console.log("Event: INCOMING_TASK", payload);

                $rootScope.$broadcast('INCOMING_TASK');
                $rootScope.widgets.kendoNotification.show({
                  message: payload.incomingType["name"+$rootScope.lnC],
                  taskName: payload.taskName,
                  sentFrom: payload.sentFrom
                }, payload.type);
              });

              $socketInstance.on('CLAIMED_TASK', function (payload) {
                console.log("Event: CLAIMED_TASK", payload);

                $rootScope.$broadcast('CLAIMED_TASK');
                payload.userID != $rootScope.user.userContext.userID && $rootScope.widgets.kendoNotification.show({
                  message: "İşə başlandı",
                  taskName: payload.taskName,
                  claimedBy: payload.claimedBy
                }, payload.type);

              });

              $socketInstance.on('UPDATE_AZIPS_DASHBOARD', function (data) {
                $rootScope.$broadcast('UPDATE_AZIPS_DASHBOARD', data);
              });


            });

          }

        }, 1000);
      })();

      $scope.kendoNotificationOptions = {
        position: {
          top: 20,
          right: 20,
          pinned: true
        },
        autoHideAfter: 5000,
        width: 250,
        templates: [
            {
              type: "INCOMING_TASK",
              template: "<div class='incoming-task'>" +
              "<p>#= message #</p>" +
              "<p>İş: #= taskName #</p>" +
              "<p>Göndərən: #= sentFrom #</p>" +
              "</div>"
            },
            {
              type: "CLAIMED_TASK",
              template: "<div class='incoming-task'>" +
              "<p>#= message #</p>" +
              "<p>İş: #= taskName #</p>" +
              "<p>İşə başlayan: #= claimedBy #</p>" +
              "</div>"
            }
        ]
      };

      $scope.settings = settingsService.getSettings;

      $scope.search = {option: $scope.settings.search.options[0]};

      $scope.search.changeOption = function (value) {
        $scope.search.option = value;
      };

      $scope.changeSetting = function (key) {

        var value = false;

        switch (key) {
          case "language":
            value = $scope.user.settings.language;

            break;
          case "default_context":
            value = $scope.user.settings.default_context;
            break;
          case "default_tile_organization_option":
            value = $scope.user.settings.default_tile_organization_option;
            break;
        }

        if (value) {
          userService.changeSetting(key, value).then(function (data) {
            if (key == "language") {
              $scope.kendoNotification.show(gettextCatalog.getString('Application language will change after you restart application.'), 'info');
            }
            $scope.user = data;
          });
        }
      };

      $scope.changeLanguage = function (index) {
        var url = 'ln/?lang=' + index.toLowerCase();
        window.location.replace(url);
      };
      $scope.changeUserContext = function (val) {
        userService.changeContext(val).then(function (data) {
          $scope.user = data;
        });
      };
      $scope.logout = function () {
        window.location = '/logout';
      };
      $scope.currentLanguage = $cookies.get('locale') ? $cookies.get('locale').toUpperCase() : 'AZ';

      // Open selected task
      $scope.openTask = function (attrs) {
        console.log(attrs);
        var operationData = {
          viewId: attrs.key,
          windowTitle: attrs['name' + $rootScope.lnC],
          screenId: attrs.screenID,
          windowController: attrs.controller,
          windowTemplateUrl: attrs.template,
          screenType: attrs.type
        };

        operationService.startTask(operationData, null);
      };

      $scope.searchClick = function(){
        $scope.showAC = true;
        $timeout(function(){
          $('#operation_autocomplete').focus()
        },200);
      };

      $scope.goToHomePage = function () {
        for (var task in $rootScope.activeWindows) {
          if (!task.isOnTaskbar) {
            taskbarService.addWindowToTaskbar(task, $rootScope.activeWindows[task].title, $rootScope.activeWindows[task].screenId);
          }
        }
        $rootScope.$broadcast('selectHomePageTab', 0);
      };

      $rootScope.filteredOperations = [];
      $rootScope._findScreenById = function (screen) {
        var id;
        if(screen && screen.screenID) {
          id = screen.screenID;
        } else {
          id = screen;
        }

        if(id) {
          Loader.show(true);

          if ($rootScope.user.operations) {
            for (var i = 0; i < $rootScope.user.operations.length; i++) {

              if ($rootScope.user.operations[i].operations) {
                for (var j = 0; j < $rootScope.user.operations[i].operations.length; j++) {
                  if (id == $rootScope.user.operations[i].operations[j].screenID) {
                    Loader.show(false);

                    $scope.openTask($rootScope.user.operations[i].operations[j]);
                    $scope.screenId = '';

                    screen = undefined;
                    $scope.ctrl.selectedItem = undefined;
                    $scope.ctrl.searchText = "";
                    return;
                  }
                }
              }

            }
          }

          if ($rootScope.user.views) {
            for (var i = 0; i < $rootScope.user.views.length; i++) {

              if ($rootScope.user.views[i].operations) {
                for (var j = 0; j < $rootScope.user.views[i].operations.length; j++) {
                  if (id == $rootScope.user.views[i].operations[j].screenID) {
                    Loader.show(false);

                    $scope.openTask($rootScope.user.views[i].operations[j]);
                    $scope.screenId = '';

                    screen = undefined;
                    $scope.ctrl.selectedItem = undefined;
                    $scope.ctrl.searchText = "";
                    return;
                  }
                }
              }

            }
          }

          Loader.show(false);
        }

      };

      $scope.userProfile = {
        upWindowOpts: {
          width: 440,
          height: 240,
          visible: false,
          modal: true,
          appendTo: 'body',
          actions: window.showChangePasswordModal == true ? []: ["Close"]
        },
        labels: {
          windowLabel: gettextCatalog.getString('Change Password')
        },
        form: {
          name: 'userProfileForm',
          data: {}
        },
        changePassword: function (kWindow) {
          this.form.name.$submitted = true;
          if (this.form.name.$valid) {
            if (this.form.data.newPassword == this.form.data.currentPassword) {
              SweetAlert.swal('', gettextCatalog.getString('Old and new passwords cannot be the same!'), 'error');
            } else {
              Loader.show(true);
              $http({
                method: "POST",
                url: "/api/main/changePassword",
                data: {
                  passwordNew: this.form.data.newPassword,
                  passwordCurrent: this.form.data.currentPassword
                }
              }).success(function (data) {

                if (data.success == "true") {
                  SweetAlert.swal({
                    title: "",
                    text: gettextCatalog.getString('Your password successfully changed. Next time you should login with your new password.'),
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                  }, function () {
                    window.onbeforeunload = null;
                    if(window.showChangePasswordModal == true) {
                      window.location.reload();
                    }
                  });

                  $scope.userProfile.form.data = {};
                  kWindow.close();
                } else {
                  if(data.message) {
                    data.message = data.message.replace("Plug-in org.forgerock.openam.idrepo.ldap.DJLDAPv3Repo encountered an ldap exception 19:", " ");
                  }
                   SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }
                Loader.show(false);
              });
            }
          }
        },
        closeWindow: function () {
          this.form.name.$setPristine();
          this.form.name.$setUntouched();
          this.form.data = {};
        }
      };
      $rootScope.showWindow = function (window) {
        window.open();
      };


      $rootScope.showChangePasswordModal = function(kWindow) {
        if(window.showChangePasswordModal == true) {
          $rootScope.showWindow(kWindow);
          kWindow.center();
        }
      };

      $rootScope.showChangePasswordWarning = function() {
        if(showChangePasswordWarning == true) {
          SweetAlert.swal('', window.userData.message, 'warning');
        }
      };

      $rootScope.changePasswordFnWrapper = function() {
        setTimeout(function() {
          $rootScope.showChangePasswordModal($scope.userProfile.upWindow);
          $rootScope.showChangePasswordWarning();
        }, 750);
      };


    }]);
