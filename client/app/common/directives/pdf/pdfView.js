'use strict';


var b64toBlob = function (b64Data, contentType, sliceSize) {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  return atob(b64Data)
};


angular.module("cmisApp").directive('pdfView', function () {
  return {
    scope: {
      data: '='
    },
    template: '<div>' +
    '<div style="background-color: rgba(0,0,0,0.6); padding: 5px 10px; direction: rtl; color: white"><i class="glyphicon glyphicon-print push-right" ng-click="print()"></i></div>' +
    '<canvas style="width: 100%; border: 1px solid lightgray"></canvas>' +
    '</div>',
    link: function (scope, element, attrs) {

      PDFJS.workerSrc = '/js/pdf.worker.js';

      scope.$watch('data', function (newVal, oldVal) {


        if (newVal) {

          //
          // var currentPage = 1;
          // var pages = [];
          //
          // PDFJS.getDocument({data: pdfData}).then(function (pdf) {
          //   pdf.getPage(currentPage).then(renderPage);
          //
          //   function renderPage(page) {
          //     var height = 700;
          //     var viewport = page.getViewport(1);
          //     var scale = height / viewport.height;
          //     var scaledViewport = page.getViewport(scale);
          //
          //     var canvas = element.find('canvas')[0];
          //     var context = canvas.getContext('2d');
          //     canvas.height = scaledViewport.height;
          //     canvas.width = scaledViewport.width;
          //
          //     var renderContext = {
          //       canvasContext: context,
          //       viewport: scaledViewport
          //     };
          //     page.render(renderContext).then(function () {
          //       if (currentPage < pdf.numPages) {
          //         pages[currentPage] = canvas;
          //         currentPage++;
          //         pdf.getPage(currentPage).then(renderPage);
          //       } else {
          //         for (var i = 1; i < pages.length; i++) {
          //           element.appendChild(pages[i]);
          //         }
          //       }
          //     });
          //   }
          //
          //
          // })


          var pdfData = b64toBlob(newVal, 'application/pdf');

          var loadingTask = PDFJS.getDocument({data: pdfData});
          loadingTask.promise.then(function (pdf) {
            console.log('PDF loaded');

            // Fetch the first page
            var pageNumber = 1;
            pdf.getPage(pageNumber).then(function (page) {
              console.log('Page loaded');

              var scale = 1.5;
              var viewport = page.getViewport(scale);

              var canvas = element.find('canvas')[0];
              var context = canvas.getContext('2d');
              canvas.height = viewport.height;
              canvas.width = viewport.width;

              // Render PDF page into canvas context
              var renderContext = {
                canvasContext: context,
                viewport: viewport
              };
              var renderTask = page.render(renderContext);
              renderTask.then(function () {
                console.log('Page rendered');
              });
            });

            // console.log(pdf.getJavaScript())
          }, function (reason) {
            // PDF loading error
            console.error(reason);
          });


          scope.print = function () {
            var size = attrs.size || 'portrait'
            var canvas = element.find('canvas')[0];
            var win = window.open('', '', '');
            var html = "<img src='" + canvas.toDataURL() + "' style='height: 267mm !important; page-break-after: always; page-break-before: always; page-break-inside: avoid'>";

            win.document.write('<html><head><title>Print it!</title>' +
              '<style type="text/css">' +
              '@media print {header{display: none !important;} footer{display: none !important;}} @page { size: ' + size + ';  margin: 0 auto;}' +
              '</style> ' +
              '</head><body>');
            win.document.write(html);
            win.document.write('</body></html>');
            win.document.close();
            win.focus();
            setTimeout(function () {
              win.print();
              win.close();
            }, 100)

          }

        }
      })
    }
  };
});
