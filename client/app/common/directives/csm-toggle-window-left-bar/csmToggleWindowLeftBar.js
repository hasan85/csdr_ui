'use strict';

angular.module('cmisApp')
  .directive('csmToggleWindowLeftBar', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var toggler = $(element);
        var homeBlock = $("#home");
        var charm = $(attrs.toggleTarget);
        if (charm.data('hidden') == undefined) {
          charm.data('hidden', true);
        }
        var showBlock = function () {
          charm.css('display', 'block');
          charm.animate({
            display: "block",
            opacity: 1,
            width: 304
          }, {duration: 400, queue: false});
          homeBlock.animate({
            left: 311
          }, {duration: 400, queue: false});
          //$('.prv').animate({
          //  left: 320
          //}, {duration: 400, queue: false});
          charm.data('hidden', false);
          toggler.css('display', 'block');
        };
        var hideBlock = function () {
          charm.animate({
            display: "none",
            opacity: 0,
            width: 0
          }, {duration: 400, queue: false});
          charm.css('display', 'none');
          toggler.animate({
            display: 'none',
          }, {duration: 400, queue: false});
          homeBlock.animate({
            left: 4
          }, {duration: 400, queue: false});
          toggler.css('display', 'none');
          charm.data('hidden', true);
        };
        var toggleBlock = function () {
          if (!charm.data('hidden')) {
            hideBlock();
          }
          else {
            showBlock();
          }
        };
        element.on('click', function () {
          toggleBlock();
        });
        $("body").on("mousemove", function (event) {
          if (event.pageX < 1) {
            if (charm.data('hidden')) {
              showBlock();
            }
          }
        });
      }
    };
  });
