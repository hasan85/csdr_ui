'use strict';

angular.module('cmisApp')
  .directive('csmLoader', [function () {
    return {
      'restrict': 'A',
      'controller': ['$scope', 'Loader', function ($scope, Loader) {
        $scope.Loader = Loader;
      }]
    }
  }]);
