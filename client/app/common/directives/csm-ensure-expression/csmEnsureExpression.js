'use strict';

angular.module('cmisApp')
  .directive('csmEnsureExpression', ['$parse',function ($parse) {
    return {
      require: 'ngModel',
      link: function(scope, ele, attrs, ngModelController) {
        scope.$watch(attrs.ngModel, function(value) {

          var booleanResult = $parse(attrs.csmEnsureExpression)(scope);
          ngModelController.$setValidity('expression', booleanResult);
        });
      }
    }
  }]);
