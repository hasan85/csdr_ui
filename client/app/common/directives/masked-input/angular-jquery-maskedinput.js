'use strict';
angular.module('angular-jquery-maskedinput', []).directive('mask', function () {
  return {
    restrict: 'A',
    link: function (scope, elem, attr, ctrl) {
        console.log(attr.mask);
        elem.mask(attr.mask, { autoclear: false, placeholder: attr.maskPlaceholder });
    }
  };
});
