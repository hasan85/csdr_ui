'use strict';

angular.module('cmisApp')
  .directive('csmMetroHome', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.css("height", parseInt($(window).height() - (110)));
      }
    };
  });
