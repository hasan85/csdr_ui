'use strict';

angular.module('cmisApp')
  .directive('csmToggleRightBar', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.on('click', function () {
          var charm = $(attrs.toggleTarget);
          if (charm.data('hidden') == undefined) {
            charm.data('hidden', true);
          }
          if (!charm.data('hidden')) {
            charm.animate({
              right: -300
            });
            charm.data('hidden', true);
          } else {
            charm.animate({
              right: 0
            });
            charm.data('hidden', false);
          }
        });
      }
    };
  });
