'use strict';

angular.module('cmisApp')
  .directive('csmKendoSelect', function () {
    return {
      link: function (scope, element, attrs) {
        $(element).kendoDropDownList({
          dataTextField: attrs.kDataTextField,
          dataValueField: "id",
          dataSource: scope[attrs.kDataSource],
          change: function () {
            var that = this;
            var item = that.dataItem();

            scope.$apply(function () {
              scope[attrs.kDataItem] = item.toJSON();
            });
          }
        });
      }
    };
  });
