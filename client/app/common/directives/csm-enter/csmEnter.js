'use strict';

angular.module("cmisApp").directive('csmEnter', function () {
  return {
    link: function (scope, elements, attrs) {
      elements.bind('keydown keypress', function (event) {
        if (event.which === 13) {
          scope.$apply(function () {
            scope.$eval(attrs.csmEnter);
          });
          event.preventDefault();
        }
      });
    }
  };
});
