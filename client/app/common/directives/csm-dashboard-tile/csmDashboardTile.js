'use strict';

angular.module('cmisApp')
  .directive('csmDashboardTile', ['operationService',
    function (operationService) {
      return {
        templateUrl: 'app/common/directives/csm-dashboard-tile/csmDashboardTile.html',
        restrict: 'AE',
        link: function (scope, element, attrs) {

          operationService.getDashBoardContents(attrs.key).then(function (data) {
            scope.dashboardContent = data;
          });

          scope.contentSettings = angular.fromJson(attrs.contentSettings);
          scope.icon = attrs.icon;
          scope.label = attrs.label;

          if(['NDC_BANK_ACCOUNTS_DASHBOARD','NDC_CASH_ACCOUNTS_DASHBOARD', 'MEMBER_CASH_ACCOUNTS_DASHBOARD'].indexOf(attrs.key) != -1){
            scope.$on("UPDATE_AZIPS_DASHBOARD", function (e, data) {
              operationService.getDashBoardContents(attrs.key).then(function (data) {
                scope.dashboardContent = data;
              });
            });
          }

        }
      }
    }]);
