'use strict';

angular.module('cmisApp')
  .directive('csmToggleInput', [function () {
    return {
      'restrict': 'A',
      require: '?ngModel',
      link: function (scope, element, attrs, controller) {

        element.click(function () {

          var model = attrs['ngModel'];

          // update the scope if model is defined
          if (model) {
            if (scope[model] == true) {
              scope[model] = false;
            }
            else {
              scope[model] = true;
            }
          }
        });
      }

    }
  }]);
