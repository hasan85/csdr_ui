'use strict';

angular.module('cmisApp')
  .directive('csmTile', ['$kWindow', 'operationService', 'Loader', 'appConstants',
    function ($kWindow, operationService, Loader, appConstants) {
      return {
        templateUrl: 'app/common/directives/csm-tile/csmTile.html',
        restrict: 'AE',
        scope: {
          badge: '@',
          label: '@',
          window_title: '@'
        },
        link: function (scope, element, attrs) {

          scope.icon = attrs.icon;

          scope.label = attrs.label;

          if (!attrs.label) {
            throw new Error('Label is required.');
          }

          var invalidTileForWindow = false;

          if (!("windowId" in attrs)) {
            invalidTileForWindow = true;
            throw new Error('data-window-id attribute must be set empty.');
          }

          if (!attrs.viewId) {
            invalidTileForWindow = true;
            throw new Error('data-view-id is required.');
          }

          if (!attrs.windowTitle) {
            invalidTileForWindow = true;
            throw new Error('data-window-title is required.');
          }

          if (!invalidTileForWindow) {
            // Show window
            element.click(function () {

              var windowId = element.attr('data-window-id');

              if (windowId !== undefined && windowId !== '') {

                var win = $('#' + windowId).data('kendoWindow');

                var windowWrapper = $("#" + windowId).closest('.k-window');

                windowWrapper.css({
                  'display': 'block'
                });

                win.restore();
                win.toFront();
                win.open();
              }

              else {
                operationService.startTask(attrs,element);
              }

            });
          }
        }
      };
    }]);
