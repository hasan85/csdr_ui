/**
 * Created by maykinayki on 12/6/16.
 */

angular.module('cmisApp').directive('onClickOutside', function($timeout) {
  return {
    restrict: 'A',
    scope: {
      onClickOutside: "&"
    },
    link: function(scope, element, attr) {

      $(document).on('click', function(event) {
        var isChild = childOf(event.target, element[0]);
        if (!isChild && !$(event.target).is($(".md-scroll-mask"))) {
          scope.$apply(scope.onClickOutside);
        }
      });

      function childOf(c, p) {
        while ((c = c.parentNode) && c !== p);
        return !!c;
      }
    }

  };

});
