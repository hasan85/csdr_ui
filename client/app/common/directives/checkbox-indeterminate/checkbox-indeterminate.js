/**
 * Created by maykinayki on 6/29/16.
 */
'use strict';

angular.module('cmisApp').directive('ngIndeterminate', [function () {
  return {
    restrict: 'A',
    scope: true,
    link: function ($scope, element, attrs) {
      var checkboxStates = '<svg width="18" height="18"><path class="checked" d="M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z"></path><path class="indeterminate" d="M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z"></path>' +
        '<svg class="unchecked" fill="white" height="18" viewBox="0 0 24 24" width="18" xmlns="http://www.w3.org/2000/svg">' +
        '<path d="M0 0h24v24H0z" fill="#c4112e"/>' +
        '<path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>' +
        '</svg>' +
        '</svg>';
      element.after(checkboxStates);
      $scope.$watch(attrs.ngIndeterminate, function (value) {
        if (value == true)
          element.prop("indeterminate", true);
        else
          element.prop("indeterminate", false);
      });
    }
  };
}]);
