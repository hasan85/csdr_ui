'use strict';

angular.module('cmisApp')
  .directive('csmKendoSwitch',['gettextCatalog', function (gettextCatalog) {
    return {
      link: function (scope, elm, attrs, ngModel) {
        $(elm).kendoMobileSwitch({
          onLabel: gettextCatalog.getString("Yes"),
          offLabel:gettextCatalog.getString("No")
        });
      }
    };
  }]);
