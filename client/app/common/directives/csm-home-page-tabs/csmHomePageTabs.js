'use strict';

angular.module("cmisApp").directive('csmHomePageTabs', ['$rootScope', function ($rootScope) {
  return {
    link: function (scope, elements, attrs) {

      $("#homePageTabs").kendoTabStrip({
        animation: {
          open: {
            effects: "fadeIn"
          }
        }
      });
      var tabStrip = $("#homePageTabs").kendoTabStrip().data("kendoTabStrip");
      $rootScope.$on('selectHomePageTab', function (event, index) {

        tabStrip.select(index);
        $rootScope.selectedHomePageTab = index;

      });

      //var tabStrip = $("#tabstrip").kendoTabStrip().data("kendoTabStrip");
      //
      //tabStrip.select(1);
    }
  };
}]);

