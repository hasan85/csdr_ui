'use strict';

angular.module('cmisApp')
  .directive('csmDisabled', function ($compile) {
    return {
      priority: 9999, // compiles first
      compile: function compile(tElement, tAttrs, transclude) {
        return {
          pre: function preLink(scope, iElement, iAttrs, controller) {
          //  iElement.prop('ng-disabled',true);

          },
          post: function postLink(scope, iElement, iAttrs, controller) {
            //console.log(iAttrs);
            //iElement.css('display','none');
            //console.log('compile2');
            tAttrs.$set('disabled', true);
          }
        }
        // or
        // return function postLink( ... ) { ... }
      },
    };
  });
