'use strict';
angular.module('cmisApp').directive('alphaNumeric', function () {
  return {
    scope: {
      ngModel: '='
    },
    restrict: 'A',
    link: function (scope, elem, attr, ctrl) {
      scope.$watch('ngModel', function (newVal, oldVal) {
        if(newVal != oldVal && newVal){
          scope.ngModel = newVal.replace(/[^a-z0-9]/gi,'');

        }
      })

    }
  };
});
