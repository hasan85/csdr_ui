'use strict';

angular.module('cmisApp')
  .directive('onFinishRender', ['$timeout', function ($timeout) {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {

        var currentGroupTilesCount = parseInt(attr.currentGroupTilesCount);
        var currentGroupIndex = parseInt(attr.currentGroupIndex);
        var allGroupsCount = parseInt(attr.allGroupsCount);
        var currentIndex = parseInt(attr.currentIndex);

        if ((currentGroupIndex + 1) === allGroupsCount && (currentIndex + 1) === currentGroupTilesCount) {
          scope.$emit('tilesCreated');
        }
      }
    }
  }]);
