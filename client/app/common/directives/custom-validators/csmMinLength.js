'use strict';

angular.module('cmisApp').directive('csmMinLength', function () {
  return {
    require: 'ngModel',
    scope: {
      model: '=ngModel'
    },
    link: function (scope, elm, attrs, ngModel) {
      scope.$watch("model", function (val) {
        if(val) {
          if (val.length >= attrs.csmMinLength) {
            ngModel.$setValidity("csmMinLength", true);
          } else {
            ngModel.$setValidity("csmMinLength", false);
          }
        }
      }, true);
    }
  };
});
