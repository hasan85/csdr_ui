'use strict';

angular.module('cmisApp')
  .directive('csmMaxLength', function () {
    return {
      require: 'ngModel',
      scope: {
        model: '=ngModel'
      },
      link: function (scope, elm, attrs, ngModel) {
        scope.$watch("model", function (val) {
          if (val.length >= attrs.csmMaxLength) {
            ngModel.$setValidity("csmMaxLength", true);
          } else {
            ngModel.$setValidity("csmMaxLength", false);
          }
        }, true);
      }
    };
  });
