'use strict';

angular.module('cmisApp')
  .directive('csmKendoDate', function () {
    return {
      require: 'ngModel',

      link: function (scope, elm, attrs, ngModel) {

        scope.$watch(attrs.ngModel, function (val) {

          if (val) {
            var timestamp = Date.parse(val)

            if (isNaN(timestamp) == false) {
              ngModel.$setValidity("csmKendoDate", true);
            } else {
              ngModel.$setValidity("csmKendoDate", false);
            }
          }
        }, true);
      }
    };
  });
