'use strict';
// validate kendo date picker text input value
angular.module('cmisApp')
  .directive('csmKendoDateVal', function () {
    return {
      require: 'ngModel',

      link: function (scope, elm, attrs, ngModel) {
        scope.$watch(attrs.ngModel, function (val) {
          if (val) {
            var timestamp = kendo.parseDate(val);
            if (timestamp) {
              ngModel.$setValidity("csmdd", true);
            } else {
              ngModel.$setValidity("csmdd", false);
            }
          } else {
            ngModel.$setValidity("csmdd", true);
          }
        }, true);
        attrs.$observe("disabled", function (disabled) {
          if (disabled) {
            ngModel.$setValidity('csmdd', true);
          } else {
            if (ngModel.$viewValue)
              ngModel.$setValidity('csmdd', !!kendo.parseDate(ngModel.$viewValue));
          }
        });
      }
    };
  });
