'use strict';

angular.module('cmisApp')
  .directive('csmKendoDateMin', function () {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ngModel) {
        scope.$watch(attrs.ngModel, function (val) {

          if (val) {
            var selectedDate = Date.parse(val);
            var minDate = Date.parse(attrs.csmKendoDateMin.toString());

            if (!isNaN(selectedDate) && !isNaN(minDate)) {
              if (selectedDate < minDate) {
                ngModel.$setValidity("csmKendoDateMin", false);
              } else {
                ngModel.$setValidity("csmKendoDateMin", true);
              }
            }
          }
        }, true);
      }
    };
  });
