'use strict';

angular.module('cmisApp')
  .directive('csmToggleWindowRightBar', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {

        element.on('click', function () {

          var toggler = $(this);
          var charm = $(attrs.toggleTarget);

          if (charm.data('hidden') == undefined) {
            charm.data('hidden', true);
          }

          if (!charm.data('hidden')) {
            charm.animate({
              opacity: 0,
              width: 0
            }, function () {
              charm.hide();
            });
            toggler.animate({
              right: 0
            });
            charm.data('hidden', true);

          } else {

            $(this).animate({
              right: 325
            }, function () {
              charm.show();
              charm.animate({
                opacity: 1
              });
            });
            charm.data('hidden', false);
          }
        });

      }
    };
  });
