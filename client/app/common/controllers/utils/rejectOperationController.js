'use strict';

angular.module('cmisApp')
  .controller('RejectOperationController', ['$scope', '$windowInstance', 'operationRejectService', 'Loader', 'rejectionConfig',
    function ($scope, $windowInstance, operationRejectService, Loader, rejectionConfig) {

      $scope.rejectTask = {
        formName: 'personFindForm',
        form: {
          decision: 'APPROVAL_DECISION_START_OVER'
        }
      };
      
      $scope.rejectionConfig = rejectionConfig;

      $scope.sendFeedback = function () {

        $scope.rejectTask.formName.$submitted = true;
        if ($scope.rejectTask.formName.$valid) {
          operationRejectService.sendFeedback($scope.rejectTask.form);
          $windowInstance.close();
        }
      };

      $scope.closeWindow = function () {
        operationRejectService.resetPromise();
        $windowInstance.close();
      };

    }]);
