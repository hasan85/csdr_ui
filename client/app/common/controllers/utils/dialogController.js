'use strict';

angular.module('cmisApp')
  .controller('DialogController', ['$scope', '$windowInstance', 'dialogService', 'message', 'dialogConfiguration',
    function ($scope, $windowInstance, dialogService, message, dialogConfiguration) {

      $scope.message = message;

      $scope.sendResponse = function (res) {
        dialogService.sendResponse(res);
        $windowInstance.close();
      };

      $scope.dialogConfiguration = dialogConfiguration;

      $scope.sendYes = function () {
        $scope.sendResponse('yes');
      };

      $scope.sendNo = function () {
        $scope.sendResponse('no');
      };

      $scope.sendCancel = function () {
        $scope.sendResponse('cancel');
      };

    }]);
