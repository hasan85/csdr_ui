'use strict';

angular.module('cmisApp')
  .controller('CancelOperationController', ['$scope', '$windowInstance', 'operationCancelService', 'Loader',
    function ($scope, $windowInstance, operationCancelService, Loader) {

      $scope.cancelTask = {
        formName: 'personFindForm',
        form: {
          operatorError: true
        }
      };

      $scope.sendFeedback = function () {

        $scope.cancelTask.formName.$submitted = true;
        if ($scope.cancelTask.formName.$valid) {
          operationCancelService.sendFeedback($scope.cancelTask.form);
          $windowInstance.close();
        }
      };

      $scope.closeWindow = function () {
        operationCancelService.resetPromise();
        $windowInstance.close();
      };

    }]);
