'use strict';

angular.module('cmisApp')
  .controller('DebtorSearchTemplateController',
    ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
      '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
      function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
                $http, helperFunctionsService, recordsService, $rootScope) {

        var validateForm = function (formData) {
          var result = {
            success: false,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };

          result.success = true;

          return result;
        };

        var debtorSelected = function (data) {
          var sResult = angular.copy($scope.debtor.searchResult.data.data);
          for (var i = 0; i < sResult.length; i++) {
            if (sResult[i].ID == data.ID) {
              $scope.debtor.selectedDebtorIndex = i;
              break;
            }
          }
        };

        $scope.debtorSelected = debtorSelected;

        var _params = {
          searchUrl: "/api/instruments/getInstrumentsInDebtorsList/",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {},
            searchOnInit: false
          },
          criteriaEnabled: true
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _debtor = {
          formName: 'debtorFindForm',
          form: {},
          data: {},

          searchResult: {
            data: null,
            isEmpty: false
          },

          selectedDebtorIndex: false,
          debtorSelected: debtorSelected,
          debtorDetails: false
        };

        $scope.debtor = angular.merge($scope.searchConfig.debtor, _debtor);

        var findDebtor = function (e) {

          //$scope.person.debtorDetails = false;

          if ($scope.debtor.searchResultGrid !== undefined) {
            $scope.debtor.searchResultGrid.refresh();
          }

          $scope.debtor.selectedDebtorIndex = false;

          if ($scope.debtor.formName.$dirty) {
            $scope.debtor.formName.$submitted = true;
          }

          if ($scope.debtor.formName.$valid || !$scope.debtor.formName.$dirty) {

            $scope.debtor.searchResult.isEmpty = false;

            var formData = angular.copy($scope.debtor.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({
              method: 'POST',
              url: $scope.params.searchUrl,
              data: {data: requestData}
            }).success(function (data, status, headers, config) {

              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data) {
                $scope.debtor.searchResult.data = data;
              } else {
                $scope.debtor.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };


        $scope.debtor.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  startDate: {type: "date"},
                  finishDate: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findDebtor(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "instrument.ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "10rem"
            },
            {
              field: "instrument.issuerName",
              title: gettextCatalog.getString('Issuer'),
              width: "10rem"
            },
            {
              field: "startDate",
              title: gettextCatalog.getString("Registration Date"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "finishDate",
              title: gettextCatalog.getString("Finish Date"),
              width: '10rem',
              template: function (e) {
                var years = e.finishDate ? e.finishDate.getFullYear() : 0;
                return years != 9999 ? (kendo.toString(e.finishDate, 'dd-MMMM-yyyy')) : '';
              }
            },

            {
              field: "comment",
              title: gettextCatalog.getString("Comment"),
              width: '15rem'
            }
          ]
        };


        $scope.debtorSelected = debtorSelected;


        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.debtor.form.ISIN = null;
          $scope.debtor.form.issuerName = null;
        };
        $scope.findDebtor = function () {
          var formValidationResult = validateForm(angular.copy($scope.debtor.form));
          if (formValidationResult.success) {
            if ($scope.debtor.searchResultGrid) {
              $scope.debtor.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.debtor.searchResultGrid) {
            $scope.debtor.searchResultGrid = widget;
          }
        });

        $scope.detailInit = function (dataItem) {

          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/instruments/getWhiteListForInstrumentRejectionList/',
            data: {
              id: dataItem.data.ID,
            }
          }).success(function (data) {
            Loader.show(false);
            if (data['success'] === "true") {
              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data.data) {
                for (var i = 0; i < data.data.length; i++) {
                  data.data[i].rejectionActionClasses = helperFunctionsService.convertObjectToArray(data.data[i].rejectionActionClasses);
                }
              }
              dataItem.data.whiteList = {
                data: data.data,
                pageSize: 10,
                schema: {
                  model: {
                    fields: {
                      startDate: {type: "date"},
                      finishDate: {type: "date"},
                    }
                  }
                }
              };
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
          });
        };

        $scope.detailGrid = function () {
          return {
            scrollable: true,
            pageable: true,
            sortable: true,
            filterable: true,
            columns: [
              {
                field: "shareholder.name",
                title: gettextCatalog.getString("Shareholder")
              }, {
                field: "shareholder.accountNumber",
                title: gettextCatalog.getString("Account Number")
              },

              {
                field: "startDate",
                title: gettextCatalog.getString("Start Date"),
                type: "date",
                format: "{0:dd-MMMM-yyyy}"
              },
              {
                field: "finishDate",
                title: gettextCatalog.getString("Finish Date"),
                type: "date",
                template: function (e) {
                  var years = e.finishDate ? e.finishDate.getFullYear() : 0;
                  return years != 9999 ? (kendo.toString(e.finishDate, 'dd-MMMM-yyyy')) : '';
                }
              },
              {
                field: "comment",
                title: gettextCatalog.getString("Comment"),
                width: '15rem'
              }

            ]
          };
        };
      }]);
