'use strict';

angular.module('cmisApp')
  .controller('BlackListFindController', ['$scope', '$windowInstance', 'debtorsService', 'searchCriteria',
    function ($scope, $windowInstance, debtorsService, searchCriteria) {

      $scope.searchConfig = {
        debtor: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedDebtorIndex: false
        },
        params: {
          searchUrl: '/api/instruments/getInstrumentsInBlackList/',
          showSearchCriteriaBlock: true,
          config: {
            searchOnInit: true,
            searchCriteria:searchCriteria
          }
        }
      };

      $scope.selectDebtor = function () {
        debtorsService.selectDebtor($scope.searchConfig.debtor.searchResult.data.data[$scope.searchConfig.debtor.selectedDebtorIndex]);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
