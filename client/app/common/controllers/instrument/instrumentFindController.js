'use strict';

angular.module('cmisApp')
  .controller('InstrumentFindController', ['$scope', 'instrumentFindService', 'Loader', 'gettextCatalog', 'searchParams',
    '$windowInstance', 'referenceDataService', 'SweetAlert', 'helperFunctionsService', '$http',
    function ($scope, instrumentFindService, Loader, gettextCatalog, searchParams, $windowInstance, referenceDataService,
              SweetAlert, helperFunctionsService, $http) {


      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        //  if (formData.isin || formData.issuerName || formData.currencyID || formData.isInDebtorsList || formData.isInBlackList || formData.searchByRejection) {
        result.success = true;
        //  }
        return result;
      };
      $scope.metaData = {};
      Loader.show(true);
      referenceDataService.getCurrencies().then(function (data) {

        $scope.metaData.currencies = data;

        referenceDataService.addEmptyOption([
          $scope.metaData.currencies
        ]);
      });


      $scope.instrument = {
        formName: 'instrumentFindForm',
        form: {
          searchByRejection: false
        },
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedInstrumentIndex: false
      };

      var paramsLocal = {
        config: {
          issuer: {
            isVisible: true
          },
          ISIN: {
            isVisible: true
          }
        },
        params: {
          issuerId: null,
          ISIN: null
        },
        showSearchCriteriaBlock: false,
        searchUrl: 'findInstrument/',
        searchOnInit: false,
      };

      $scope.params = angular.merge(paramsLocal, searchParams ? searchParams : {});

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: false
        },
        {
          size: '70%',
          collapsible: false
        }
      ];


      var findInstrument = function (e) {
        if ($scope.instrument.searchResultGrid !== undefined) {
          $scope.instrument.searchResultGrid.refresh();
        }

        if ($scope.params.params.issuerId) {
          $scope.instrument.form.issuerId = $scope.params.params.issuerId;
        }
        if ($scope.params.params.isIssue != undefined) {
          $scope.instrument.form.isIssue = $scope.params.params.isIssue;
        }
        if ($scope.params.params.ISIN) {
          $scope.instrument.form.ISIN = $scope.params.params.ISIN;
        }

        if ($scope.params.params.date !== undefined) {
          $scope.instrument.form.date = $scope.params.params.date;
        }

        if (!$scope.instrument.form.searchByRejection) {
          $scope.instrument.form.isInDebtorsList = null;
          //$scope.instrument.form.isInBlackList = null;
          $scope.instrument.form.searchByDebtorsList = false;
        } else {


          $scope.instrument.form.searchByDebtorsList = true;
          if ($scope.instrument.form.isInDebtorsList === null) {
            $scope.instrument.form.isInDebtorsList = false;
          }
          //if ($scope.instrument.form.isInBlackList === null) {
          //  $scope.instrument.form.isInBlackList = false;
          //}
        }
        $scope.instrument.form.searchByBlacklist = true;
        $scope.instrument.form.isInBlackList = false;
        $scope.instrument.selectedInstrumentIndex = false;

        if ($scope.instrument.formName.$dirty) {
          $scope.instrument.formName.$submitted = true;
        }

        $scope.instrument.searchResult.isEmpty = false;

        var requestData = angular.extend(angular.copy($scope.instrument.form), {
          skip: e.data.skip,
          take: e.data.take,
          sort: e.data.sort
        });

        Loader.show(true);
        $http({method: 'POST', url: "/api/common/" + $scope.params.searchUrl, data: {data: requestData}}).
          success(function (data, status, headers, config) {
            if (data) {
              $scope.instrument.searchResult.data = data;
            } else {
              $scope.instrument.searchResult.isEmpty = true;
            }
            if (data['success'] === "true") {
              data.data = helperFunctionsService.convertObjectToArray(data.data);
              e.success({Data: data.data ? data.data : [], Total: data.total});
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
            Loader.show(false);
          });
      };

      $scope.findInstrument = function () {

        var formValidationResult = validateForm(angular.copy($scope.instrument.form));
        if (formValidationResult.success) {
          if ($scope.instrument.searchResultGrid) {
            $scope.instrument.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };


      $scope.selectInstrument = function () {
        instrumentFindService.selectInstrument($scope.instrument.searchResult.data.data[$scope.instrument.selectedInstrumentIndex]);
        $windowInstance.close();
      };

      $scope.instrument.instrumentSelected = function (data) {

        var sResult = angular.copy($scope.instrument.searchResult.data.data);

        for (var i = 0; i < sResult.length; i++) {

          if (sResult[i].id == data.id) {
            $scope.instrument.selectedInstrumentIndex = i;
            break;
          }
        }
      };

      $scope.searchCriteriaCollapse = function () {

        $scope.$apply(function () {

          $scope.params.showSearchCriteriaBlock = false;
        });
      };
      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {

          $scope.params.showSearchCriteriaBlock = true;
        });
      };
      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };

      $scope.resetForm = function () {
        $scope.instrument.form = {
          isin: null,
          issuerName: null,
          currencyID: '',
          isInDebtorsList: null,
          searchByRejection: false
        };
      };

      $scope.instrument.form.currencyID = '';
      $scope.instrument.mainGridOptions = {
        rowTemplate: '<tr class="#: isInDebtorsList =="true" ? "debtors-list-color" : "" #" data-uid="#: uid #">' +
        '<td>#: instrumentName #</td>' +
        '<td>#: isin #</td>' +
        '<td>#: issuerName #</td>' +
        '<td>#: parValue #</td>' +
        '<td>#: currency #</td>' +
        '<td>#: quantity #</td>' +
        '<td>#: CFI.nameAz #</td>' +
        '</tr>',
        dataSource: {
          excel: {
            allPages: true
          },

          schema: {
            data: "Data",
            total: "Total"
          },
          transport: {
            read: function (e) {
              findInstrument(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "instrumentName",
            title: gettextCatalog.getString("Name")
          },
          {
            field: "isin",
            title: gettextCatalog.getString("ISIN")
          },
          {
            field: "issuerName",
            title: gettextCatalog.getString("Issuer")
          },
          {
            field: "parValue",
            title: gettextCatalog.getString("Par Value")
          },
          {
            field: "currency",
            title: gettextCatalog.getString("Currency")
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity")
          },
          {
            field: "CFI",
            title: gettextCatalog.getString("CFI")
          }
        ]
      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.instrument.searchResultGrid) {
          $scope.instrument.searchResultGrid = widget;
        }
      });
    }]);
