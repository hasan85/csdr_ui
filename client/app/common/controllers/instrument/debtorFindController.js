'use strict';

angular.module('cmisApp')
  .controller('DebtorFindController', ['$scope', '$windowInstance', 'debtorsService', 'searchCriteria',
    function ($scope, $windowInstance, debtorsService, searchCriteria) {

      $scope.searchConfig = {
        debtor: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedDebtorIndex: false
        },
        params: {
          searchUrl: '/api/instruments/getInstrumentsInDebtorsList/',
          showSearchCriteriaBlock: true,
          config: {
            searchOnInit: true,
            searchCriteria: searchCriteria
          }
        }
      };

      $scope.selectDebtor = function () {
        debtorsService.selectDebtor($scope.searchConfig.debtor.searchResult.data.data[$scope.searchConfig.debtor.selectedDebtorIndex]);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
