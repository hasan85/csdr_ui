'use strict';

angular.module('cmisApp')
  .controller('ViewIamasAccountSearchController',
    ['$scope', 'Loader', 'gettextCatalog', '$rootScope', 'SweetAlert', 'personFindService',
      function ($scope, Loader, gettextCatalog, $rootScope, SweetAlert, personFindService) {
        $scope.form = {
          SerialId: "",
          pinCode: ""
        };
        $scope.findData = function () {
          Loader.show(true);
          var searchData = {
            id: $scope.form.SerialId,
            pinCode: $scope.form.pinCode,
            isDMX: $scope.form.isDMX
          };
          // if (!$scope.form.SerialId.length || !$scope.form.pinCode.length) {
          //   SweetAlert.swal('', 'Forma Doldurma Xətası!', 'error');
          //   Loader.show(false);
          //   return;
          // }


          personFindService.PersonalCardbyId(searchData).then(function (response) {
            console.log("Iamas", response);
            if (response.data) {
              if (response.data.Profile.isActive !== "true") {
                SweetAlert.swal('', 'Şəxsiyyət Vəsiqəsi Aktiv Deyil', 'error');
              }
               // else {
              //   $scope.form.idCard = null;
              //   $scope.form.idCard = angular.copy(response.data);
              // }

                $scope.form.idCard = null;
                $scope.form.idCard = angular.copy(response.data);
            } else {
              SweetAlert.swal('', response.message, 'error');
            }
            Loader.show(false);
          });
        }
      }]
  );
