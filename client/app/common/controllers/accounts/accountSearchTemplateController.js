'use strict';

angular.module('cmisApp')
  .controller('AccountSearchTemplateController',
    ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert',
      'referenceDataService', 'helperFunctionsService', '$http', '$rootScope',
      function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert,
                referenceDataService, helperFunctionsService, $http, $rootScope) {

        var validateForm = function (formData) {

          var result = {
            success: false,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };


          result.success = true;

          return result;
        };

        var findPerson = function (e) {

          $scope.person.personDetails = false;

          if ($scope.person.searchResultGrid !== undefined) {
            $scope.person.searchResultGrid.refresh();
          }

          $scope.person.selectedPersonIndex = false;

          if ($scope.person.formName.$dirty) {
            $scope.person.formName.$submitted = true;
          }

          if ($scope.person.formName.$valid || !$scope.person.formName.$dirty) {

            $scope.person.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.person.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {


              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }

              formData.creationDateStart = $scope.person.form.creationDateStart ? $scope.person.form.creationDateStartObj : null;
              formData.personClassId = formData.personClassId ? parseInt(formData.personClassId) : null;
              formData.accountClassId = formData.accountClassId ? parseInt(formData.accountClassId) : null;
              formData.creationDateFinish = $scope.person.form.creationDateFinish ? $scope.person.form.creationDateFinishObj : null;
              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take,
                sort: e.data.sort
              });

              console.log('Request Data', requestData);

              Loader.show(true);
              $http({
                method: 'POST',
                url: $scope.params.searchUrl,
                data: {data: requestData}
              }).success(function (data, status, headers, config) {
                if (data) {
                  $scope.person.searchResult.data = data;
                } else {
                  $scope.person.searchResult.isEmpty = true;
                }

                if (data['success'] === "true") {
                  data.data = personFindService.normalizeAccountArrayFields(data.data);
                  e.success({Data: data.data ? data.data : [], Total: data.total});
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


            } else {
              Loader.show(false);
              //$scope.params.showSearchCriteriaBlock = true;
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };

        var _params = {
          searchUrl: "/api/persons/getParticipantAccounts",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {
              personType: {
                isVisible: true,
                default: appConstants.personClasses.juridicalPerson
              }
            },
            searchOnInit: true

          }
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _person = {
          formName: 'personFindForm',
          form: {},
          data: {
            personClasses: [],
            personClassCodes: {
              natural_person: appConstants.personClasses.naturalPerson,
              juridical_person: appConstants.personClasses.juridicalPerson
            }
          },

          searchResult: {
            data: null,
            isEmpty: false
          },
          selectedPersonType: $scope.params.config.criteriaForm.personType.default,
          selectedPersonIndex: false,
          findPerson: findPerson,
          personDetails: false
        };

        $scope.person = angular.merge($scope.searchConfig.person, _person);


        Loader.show(true);

        referenceDataService.getFunctionalAccountClasses().then(function (data) {

          referenceDataService.addEmptyOption([data]);

          $scope.person.data.functionalAccountClasses = data;

          $scope.person.mainGridOptions = {
            dataSource: {
              schema: {
                data: "Data",
                total: "Total",
                model: {
                  fields: {
                    creationDate: {type: "date"}
                  }
                }
              },
              transport: {
                read: function (e) {
                  findPerson(e);
                }
              },

              serverPaging: true,
              serverSorting: true
            },
            excelExport: function (e) {
              var sheet = e.workbook.sheets[0];
              var header = sheet.rows[0];
              var upIndex = -1;
              var upFound = false;

              for (var cellIndex = 0; cellIndex < header.cells.length; cellIndex++) {
                if ('colSpan' in header.cells[cellIndex]) upIndex = upIndex + header.cells[cellIndex].colSpan;
                else upIndex = upIndex + 1;

                if (header.cells[cellIndex].value == 'Sahiblər') {
                  upFound = true;
                  break;
                }
              }
              for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                var row = sheet.rows[rowIndex];
                if (row.type === "data" && upFound) {
                  var cell = row.cells[upIndex];
                  var res = [];
                  for (var cr = 0; cr < cell.value.length; cr++) {
                    res.push(cell.value[cr].nameAz);
                  }
                  cell.value = res.join(",");
                }
              }
            },
            selectable: true,
            scrollable: true,
            pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
            sortable: true,
            resizable: true,
            columns: [
              {
                field: "number",
                title: gettextCatalog.getString("Participant Account"),
                template: function (e) {
                  return "<span class='" + personFindService.resolveAccountStatusTextClass(e) + "'> " + e.number ? e.number : '' + "</span>"
                },
                width: "10rem"

              },
              {
                field: "owners",
                title: gettextCatalog.getString("Owners"),
                template: function (e) {
                  var t = "<ul class=\"list-no-style\">";
                  if (e.owners) {
                    for (var i = 0; i < e.owners.length; i++) {
                      t += "<li><a class='cursor-pointer blue-link'  ng-click='showPersonDetails(" + e.owners[i].id + ")'>" + e.owners[i].nameAz + "</a></li>";
                    }
                  }
                  t += "</ul>";
                  return t;
                },
                width: "10rem"
              },

              {
                field: "functionalAccounts",
                title: gettextCatalog.getString("Account Type"),
                sortable: false,
                template: function (e) {
                  var t = "<ul class=\"list-no-style\">";
                  if (e.functionalAccounts) {
                    for (var i = 0; i < e.functionalAccounts.length; i++) {
                      t += ("<li class='" + personFindService.resolveAccountStatusTextClass(e) + "'> " + e.functionalAccounts[i].type.nameAz + "</li>");
                    }
                  }
                  t += "</ul>";
                  return t;
                },
                width: "10rem"
              },
              {
                field: "functionalAccounts",
                title: gettextCatalog.getString("Account Number"),
                sortable: false,
                template: function (e) {
                  var t = "<ul class=\"list-no-style\">";
                  if (e.functionalAccounts) {
                    for (var i = 0; i < e.functionalAccounts.length; i++) {
                      t += "<li class='" + personFindService.resolveAccountStatusTextClass(e) + "'> " + e.functionalAccounts[i].number + "</li>";
                    }
                  }
                  t += "</ul>";
                  return t;
                },
                width: "10rem"
              },
              {
                field: "creationDate",
                title: gettextCatalog.getString("Creation Date"),
                width: "10rem",
                template: function (e) {
                  var t = "<ul class=\"list-no-style\">";
                  if (e.functionalAccounts) {
                    for (var i = 0; i < e.functionalAccounts.length; i++) {
                      t += "<li> " + kendo.toString(new Date(e.functionalAccounts[i].creationDate), "dd-MMMM-yyyy HH:mm") + "</li>";
                    }
                  }
                  t += "</ul>";
                  return t;
                }

              }

            ]
          };

          Loader.show(true);
          referenceDataService.getPersonClasses().then(function (pc) {
            referenceDataService.addEmptyOption([pc]);
            $scope.person.data.personClasses = pc;
            Loader.show(false);
          });
          Loader.show(false);
        });


        $scope.$on('refreshGrid', function () {
          findPerson();
        });

        $scope.showPersonDetails = function (id) {

          var sResult = angular.copy($scope.person.searchResult.data.data);
          for (var i = 0; i < sResult.length; i++) {
            if (sResult[i].id == id) {
              $scope.person.selectedPersonIndex = i;
              break;
            }
          }
          personFindService.findPersonById(id).then(function (data) {
            $scope.person.personDetails = data;
          });

        };

        $scope.closePersonDetails = function () {
          $scope.person.personDetails = false;
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {

          $scope.person.form.fullName = null;
          $scope.person.form.accountNumber = null;
          $scope.person.form.accountClassId = "";
          $scope.person.form.isSuspended = false;
          $scope.person.form.isDeleted = false;

        };


        $scope.findPerson = function () {

          var formValidationResult = validateForm(angular.copy($scope.person.form));
          if (formValidationResult.success) {
            if ($scope.person.searchResultGrid) {
              $scope.person.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.person.searchResultGrid) {
            $scope.person.searchResultGrid = widget;
          }
        });
      }]);
