'use strict';

angular.module('cmisApp')
  .controller('AccountPrescriptionSearchTemplateController',
  ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http', "$filter",
    function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http, $filter) {


      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        // if (formData.clientName || formData.brokerName || formData.finishDateStart || formData.finishDateFinish) {
        result.success = true;
        //}
        return result;
      };
      var findData = function (e) {

        if ($scope.accountPrescription.searchResultGrid !== undefined) {
          $scope.accountPrescription.searchResultGrid.refresh();
        }
        if ($scope.accountPrescription.formName.$dirty) {
          $scope.accountPrescription.formName.$submitted = true;
        }

        if ($scope.accountPrescription.formName.$valid || !$scope.accountPrescription.formName.$dirty) {

          $scope.accountPrescription.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.accountPrescription.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }
            formData.startDateStart = $scope.accountPrescription.form.startDateStart ? $scope.accountPrescription.form.startDateStartObj : null;
            formData.startDateFinish = $scope.accountPrescription.form.startDateFinish ? $scope.accountPrescription.form.startDateFinishObj : null;
            formData.finishDateStart = $scope.accountPrescription.form.finishDateStart ? $scope.accountPrescription.form.finishDateStartObj : null;
            formData.finishDateFinish = $scope.accountPrescription.form.finishDateFinish ? $scope.accountPrescription.form.finishDateFinishObj : null;
            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.accountPrescription.searchResult.data = data;
                } else {
                  $scope.accountPrescription.searchResult.isEmpty = true;
                }

                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };

      var _params = {
        searchUrl: "/api/persons/getAccountPrescriptions/",
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _accountPrescription = {
        formName: 'accountPrescriptionFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedaccountPrescriptionIndex: false,
        findData: findData
      };

      $scope.accountPrescription = angular.merge($scope.searchConfig.accountPrescription, _accountPrescription);

      $scope.findData = function () {

        var formValidationResult = validateForm(angular.copy($scope.accountPrescription.form));
        if (formValidationResult.success) {
          if ($scope.accountPrescription.searchResultGrid) {
            $scope.accountPrescription.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      $scope.accountPrescription.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                finishDate: {type: "date"},
                startDate: {type: "date"}

              }
            }
          },
          transport: {
            read: function (e) {
              findData(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "broker.name",
            title: gettextCatalog.getString("Broker"),
            width: "10rem"
          },
          {
            field: "client.name",
            title: gettextCatalog.getString("Client Name"),
            width: "10rem"
          },
          {
            field: "number",
            title: gettextCatalog.getString("Client Account Number"),
            width: "10rem"
          },
          {
            field: "client.accountNumber",
            title: gettextCatalog.getString("Client DEPO Account Number"),
            width: "10rem"
          },
          {
            field: "startDate",
            title: gettextCatalog.getString("Start Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "finishDate",
            title: gettextCatalog.getString("Finish Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          }
        ]
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.accountPrescription.form.brokerName = null;
        $scope.accountPrescription.form.clientName = null;
        $scope.accountPrescription.form.startDateStart = null;
        $scope.accountPrescription.form.startDateFinish = null;
        $scope.accountPrescription.form.finishDateStart = null;
        $scope.accountPrescription.form.finishDateFinish = null;
      };

      $scope.instrumentsGrid = function (dataItem) {

        return {
          scrollable: true,
          pageable: true,

          dataSource: {
            data: dataItem.items,
            schema: {
              model: {
                fields: {
                  ISIN: {type: "string"},
                  issueName: {type: "string"}
                }
              }
            },
            pageSize: 5
          },
          columns: [
            {
              field: "actionClass.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Action Class")
            },
            {
              field: "instrument.instrumentName",
              title: gettextCatalog.getString("Instrument")
            },
            {
              field: "instrument.ISIN",
              title: gettextCatalog.getString("ISIN")
            },
            {
              field: "instrument.issuerName",
              title: gettextCatalog.getString("Issuer")
            },
            {
              field: "quantity",
              title: gettextCatalog.getString("Quantity"),
              template: function(dataItem) {
                return dataItem.quantity ? $filter("formatNumber")(dataItem.quantity, false) : "";
              }
            }

          ]
        };
      };
    }]);
