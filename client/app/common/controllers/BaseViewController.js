'use strict';

angular.module('cmisApp')
  .controller('BaseViewController', ['$scope', 'gettextCatalog', 'Loader', function ($scope, gettextCatalog, Loader) {

    $scope.config._buttons = {
      view: {
        title: gettextCatalog.getString('View Item'),
        icon: 'eye-open',
        disabled: false,
        click: function () {
          return alert('base');
        }
      },
      refresh: {
        title: gettextCatalog.getString('Refresh'),
        icon: 'refresh',
        disabled: false,
        click: function () {

          Loader.show(true);
          $scope.config.mainGrid.dataSource.read();
          $scope.config.mainGrid.refresh();
          Loader.show(false);
        }
      },
      export: {
        title: gettextCatalog.getString('Export'),
        icon: 'export',
        disabled: false,
      },
      print: {
        title: gettextCatalog.getString('Print'),
        icon: 'print',
        disabled: false,
      },
      close: {
        title: gettextCatalog.getString('Close'),
        icon: 'remove',
        disabled: false,
        click: function () {
          $scope.config.window.close();

        }
      }
    };

    $scope.closeWindow = function () {
      $scope.config.window.close();

    };

    $scope.config._buttons = angular.merge($scope.config._buttons, $scope.config.buttons);

    // Merge buttons
    $scope.$watch('config.buttons', function () {

      $scope.config._buttons = angular.merge($scope.config._buttons, $scope.config.buttons);

    }, true);

    $scope.openWindow = function (elem) {
      elem.open();
      elem.center();
    };

    $scope.exportToExcel = function (grid) {
      console.log(grid.saveAsExcel);
      grid.saveAsExcel();
    };


  }]);
