'use strict';

angular.module('cmisApp')
  .controller('BankStatementSearchTemplateController',
  ['$scope', 'clearingService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', '$rootScope', 'helperFunctionsService', "$filter",
    function ($scope, clearingService, Loader, gettextCatalog, appConstants, SweetAlert, $rootScope, helperFunctionsService, $filter) {

      $scope.metaData = {};
      var bankStatementSelected = function (data) {
        var sResult = angular.copy($scope.bankStatement.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.bankStatement.selectedBankStatementIndex = i;
            break;
          }
        }
      };
      $scope.bankStatementSelected = bankStatementSelected;
      var _params = {
        searchUrl: "/api/clearing/getBankStatements",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };
      $scope.params = angular.merge(_params, $scope.searchConfig.params);
      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];
      var _bankStatement = {
        formName: 'bankStatementFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedBankStatementIndex: false,
        bankStatementSelected: bankStatementSelected,
        bankStatementDetails: false
      };
      $scope.bankStatement = angular.merge($scope.searchConfig.bankStatement, _bankStatement);
      var findBankStatement = function (e) {
        //$scope.person.bankStatementDetails = false;
        if ($scope.bankStatement.searchResultGrid !== undefined) {
          $scope.bankStatement.searchResultGrid.refresh();
        }

        $scope.bankStatement.selectedBankStatementIndex = false;

        if ($scope.bankStatement.formName.$dirty) {
          $scope.bankStatement.formName.$submitted = true;
        }

        if ($scope.bankStatement.formName.$valid || !$scope.bankStatement.formName.$dirty) {

          $scope.bankStatement.searchResult.isEmpty = false;

          var formData = angular.copy($scope.bankStatement.form);


          if ($scope.params.config.searchCriteria) {
            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }
          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          Loader.show(true);

          clearingService.getBankStatementsByUrl($scope.params.searchUrl, requestData).then(function (data) {

            data.data = helperFunctionsService.convertObjectToArray(data.data);
            if (data) {
              $scope.bankStatement.searchResult.data = data;
            } else {
              $scope.bankStatement.searchResult.isEmpty = true;
            }
            if (data['success'] === "true") {

              e.success({
                Data: data.data ? data.data : [], Total: data.total, schema: {
                  model: {
                    fields: {
                      date: {type: "date"}
                    }
                  }
                }
              });
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }

            Loader.show(false);
          });
        }
      };
      $scope.bankStatement.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                date: {type: "date"},
              }
            }
          },
          transport: {
            read: function (e) {
              findBankStatement(e);
            }
          },

          serverPaging: true,
          sort: {field: "date", dir: "desc"},
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "date",
            title: gettextCatalog.getString("Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}",
          },
          {
            field: "status.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Status"),
            width: "10rem"
          },
          {
            field: "account.name",
            title: gettextCatalog.getString("Account"),
            width: "220px"
          },
          {
            field: "broker.name",
            title: gettextCatalog.getString("Broker"),
            width: "220px"
          },

          {
            field: "accountNumber",
            title: gettextCatalog.getString("Account Number"),
            width: "220px"
          },
          {
            field: "destination",
            title: gettextCatalog.getString("Destination"),
            width: "10rem"
          },
          {
            field: "referenceNumber",
            title: gettextCatalog.getString("Reference Number"),
            width: "10rem"
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount"),
            width: "10rem",
            template: function(dataItem) {
              return dataItem.amount ? $filter("formatNumber")(dataItem.amount) : "";
            }
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency"),
            width: "10rem"
          },
          {
            field: "correspondentAccount.accountNumber",
            title: gettextCatalog.getString("Correspondent Account"),
            width: "10rem"
          },


        ]
      };
      $scope.bankStatementSelected = bankStatementSelected;
      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };
      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };
      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };
      $scope.resetForm = function () {
        $scope.bankStatement.form.comment = null;
        $scope.bankStatement.form.recordNumber = null;
      };

      $scope.findBankStatement = function () {

          if ($scope.bankStatement.searchResultGrid) {
            $scope.bankStatement.searchResultGrid.dataSource.page(1);
          }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.bankStatement.searchResultGrid) {
          $scope.bankStatement.searchResultGrid = widget;
        }
      });


    }]);
