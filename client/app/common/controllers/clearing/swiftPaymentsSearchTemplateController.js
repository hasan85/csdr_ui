'use strict';

angular.module('cmisApp')
  .controller('SwiftPaymentsSearchTemplateController',
    ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
      '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
      function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
                $http, helperFunctionsService, recordsService, $rootScope) {

        var validateForm = function (formData) {
          var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

          return result;
        };

        var subscriptionOrdersSelected = function (data) {
          var sResult = angular.copy($scope.subscriptionOrders.searchResult.data.data);
          for (var i = 0; i < sResult.length; i++) {
            if (sResult[i].ID == data.ID) {
              $scope.subscriptionOrders.selectedSubscriptionOrdersIndex = i;
              break;
            }
          }
        };

        $scope.subscriptionOrdersSelected = subscriptionOrdersSelected;

        var _params = {
          searchUrl: "/api/clearing/getSwiftPayment/",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {},
            searchOnInit: true
          },
          criteriaEnabled: true
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _subscriptionOrders = {
          formName: 'subscriptionOrdersFindForm',
          form: {},
          data: {},

          searchResult: {
            data: null,
            isEmpty: false
          },

          selectedSubscriptionOrdersIndex: false,
          subscriptionOrdersSelected: subscriptionOrdersSelected,
          subscriptionOrdersDetails: false
        };

        $scope.subscriptionOrders = angular.merge($scope.searchConfig.subscriptionOrders, _subscriptionOrders);

        var findSubscriptionOrders = function (e) {

          //$scope.person.subscriptionOrdersDetails = false;

          if ($scope.subscriptionOrders.searchResultGrid !== undefined) {
            $scope.subscriptionOrders.searchResultGrid.refresh();
          }

          $scope.subscriptionOrders.selectedSubscriptionOrdersIndex = false;

          if ($scope.subscriptionOrders.formName.$dirty) {
            $scope.subscriptionOrders.formName.$submitted = true;
          }

          if ($scope.subscriptionOrders.formName.$valid || !$scope.subscriptionOrders.formName.$dirty) {

            $scope.subscriptionOrders.searchResult.isEmpty = false;

            var formData = angular.copy($scope.subscriptionOrders.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }


            formData.paymentDateStart = $scope.subscriptionOrders.form.paymentDateStart ? $scope.subscriptionOrders.form.paymentDateStartObj : null;
            formData.paymentDateFinish = $scope.subscriptionOrders.form.paymentDateFinish ? $scope.subscriptionOrders.form.paymentDateFinishObj : null;
            
            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            //console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data) {
                $scope.subscriptionOrders.searchResult.data = data;
              } else {
                $scope.subscriptionOrders.searchResult.isEmpty = true;

              }

              //console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };

        $scope.subscriptionOrders.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  paymentDate: {type: "date"}
                }
              }
            },
            sort: { field: "paymentDate", dir: "desc" },
            transport: {
              read: function (e) {
                findSubscriptionOrders(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "paymentDate",
              title: gettextCatalog.getString("Payment Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy}",
            },
            {
              field: "referenceNumber",
              title: gettextCatalog.getString("Reference Number"),
              width: "10rem"
            },
            {
              field: "destination",
              title: gettextCatalog.getString("Destination"),
              width: "10rem"
            },
            {
              field: "amount",
              title: gettextCatalog.getString("Amount"),
              width: "10rem"
            },
            {
              field: "paymentClass.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Payment Class"),
              width: "10rem"

            },
            {
              field: "currency",
              title: gettextCatalog.getString("Currency"),
              width: "10rem"
            },
            {
              field: "status.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Status"),
              width: "10rem"

            },
          ]
        };

        $scope.subscriptionOrdersSelected = subscriptionOrdersSelected;

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.subscriptionOrders.form.destination = null;
          $scope.subscriptionOrders.form.referenceNumber = null;
          $scope.subscriptionOrders.form.paymentDateStart = null;
          $scope.subscriptionOrders.form.paymentDateFinish = null;
          $scope.subscriptionOrders.form.status = null;
          $scope.subscriptionOrders.form.paymentClass = null;
        };

        $scope.findSubscriptionOrders = function () {
          var formValidationResult = validateForm(angular.copy($scope.subscriptionOrders.form));
          if (formValidationResult.success) {
            if ($scope.subscriptionOrders.searchResultGrid) {
              $scope.subscriptionOrders.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.subscriptionOrders.searchResultGrid) {
            $scope.subscriptionOrders.searchResultGrid = widget;
          }
        });

        $scope.detailInit = function (dataItem) {
          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/clearing/getMatchedRecordByPaymentID/',
            data: {
              paymentID: dataItem.data.ID
              //paymentID: 170
            }
          }).success(function (data) {
            Loader.show(false);
            if (data['success'] === "true") {
              data.data = helperFunctionsService.convertObjectToArray(data.data);
              console.log("xumar", dataItem);
              $scope.recordNumbers = {};
              dataItem.data.details = {payments: data.data};
              $scope.recordNumbers[dataItem.data.ID] = data.data;
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
          });
        };


      }]);

