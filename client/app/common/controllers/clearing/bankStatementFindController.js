'use strict';

angular.module('cmisApp')
  .controller('BankStatementFindController', ['$scope', '$windowInstance', 'clearingService', 'searchCriteria',
    function ($scope, $windowInstance, clearingService, searchCriteria) {

      $scope.searchConfig = {
        bankStatement: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedCorporateActionIndex: false
        },
        params: {
          showSearchCriteriaBlock: true,
          config: {
            searchOnInit: true,
            searchCriteria: searchCriteria
          }
        }
      };
      $scope.selectBankStatement = function () {
        clearingService.selectBankStatement($scope.searchConfig.bankStatement.searchResult.data.data[$scope.searchConfig.bankStatement.selectedBankStatementIndex]);
        $windowInstance.close();
      };

    }]);
