'use strict';

angular.module('cmisApp')
  .controller('BaseOperationController',
  ['$scope', 'operationService', 'appConstants', 'gettextCatalog',
    '$rootScope', 'Loader', 'operationCancelService', 'dialogService', 'SweetAlert', '$sce', 'helperFunctionsService',
    '$mdSidenav', 'operationRejectService', 'FileUploader',
    function ($scope, operationService, appConstants, gettextCatalog,
              $rootScope, Loader, operationCancelService, dialogService, SweetAlert, $sce, helperFunctionsService,
              $mdSidenav, operationRejectService, FileUploader) {

      // TASK document MANAGEMENT [[[
      $scope.config.uploader = new FileUploader({
        queueLimit: 10,
        fileFormDataName: ['file[0]', 'file[1]']
      });

      $scope.config.uploader.filters.push({
        name: "fileSizeLimit",
        fn: function (item) {
          return item.size < appConstants.fileSizeLimit;
        }
      });
      $scope.config.uploader.onWhenAddingFileFailed = function (item, filter) {
        var errorMessages = {
          fileSizeLimit: gettextCatalog.getString('You cannot upload file larger than 5 mb'),
          queueLimit: gettextCatalog.getString('You cannot upload more than 10 file')
        };
        SweetAlert.swal('', errorMessages[filter.name], 'error');
      };
      $scope.config.uploader.onAfterAddingFile = function (item, filter) {
        var formData = new FormData();
        formData.append("uploadedFile", item._file);
        formData.append("payload", angular.toJson({
          docID: item.documentId,
          docClass: item.documentClassCode,
          fileName: item.file.name,
          procInstID: $scope.config.task.processInstanceId
        }));
        operationService.uploadTaskAttachment(formData).then(function (res) {
          if (res.success == "true") {
            $scope.task.document.forEach(function (doc) {
              if (doc.documentClass.code == item.documentClassCode) {
                if (!doc.documents) {
                  doc.documents = [];
                }
                doc.documents.push({
                  fileName: item.file.name,
                  ID: res.data
                })
              }
            });
            $scope.config.uploader.clearQueue()
            SweetAlert.swal("", gettextCatalog.getString('File uploaded successfully', 'info'), 'success');
          }
          else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
          }
        });
      };
      $scope._removeDocument = function (model, $index, id) {
        SweetAlert.swal({
            title: gettextCatalog.getString('Warning'),
            text: gettextCatalog.getString('Are you sure to delete this document?'),
            type: "warning",
            showCancelButton: true,
            confirmButtonText: gettextCatalog.getString('Yes'),
            cancelButtonText: gettextCatalog.getString('Cancel'),
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function (isConfirm) {
            if (isConfirm) {
              operationService.removeDocument(id).then(function (res) {
                if (res.success == "true") {
                  model.splice($index, 1);
                }
                else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
                }
              });
            }
          });
      };
      $scope.getDocumentById = function (id) {
        operationService.getDocumentByID(id).then(function (res) {
          if (res.success == "true") {
            if (res.data) {
              if (res.data.mimeType) {
                res.data.mimeType = res.data.mimeType.split(";");
                helperFunctionsService.saveBase64AsBlob(/*res.data.mimeType*/ res.data.content, res.data.mimeType[0], res.data.mimeType[1]);

              } else {
                SweetAlert.swal("", gettextCatalog.getString('Can not resolve file type!'), 'error');
              }

            } else {

              SweetAlert.swal("", gettextCatalog.getString('Can not get file!'), 'error');
            }
          }
          else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
          }
        });
      };
      $scope.config.uploader.onErrorItem = function () {
        Loader.show(false);
        SweetAlert.swal('', gettextCatalog.getString('Files did not uploaded, please try again!'), 'error');
      };
      // TASK document MANAGEMENT ]]]

      // Initialize variables
      $scope.task = $scope.config.task;
      $scope.task.comment = helperFunctionsService.convertObjectToArray($scope.task.comment);
      $scope.task.document = helperFunctionsService.convertObjectToArray($scope.task.document);
      if ($scope.task.document && $scope.task.document.length) {
        $scope.task.document = $scope.task.document.map(function (item) {
          if (item.readonly === "true") {
            item.readonly = true;
          } else if (item.readonly == "false") {
            item.readonly = false;
          }
          item.documents = helperFunctionsService.convertObjectToArray(item.documents);
          return item;
        });
      }

      console.log('Payload On Init', angular.fromJson($scope.task.draft));
      $scope.config._buttons = {};
      $scope.config.window.widget.wrapper.find(".k-i-close").click(function (e) {
        if ($scope.config.operationType == appConstants.operationTypes.entry) {
          $scope.$emit('closeTask');
          e.preventDefault();
          e.stopPropagation();
        }
      });

      var getComments = function () {
        var comments = angular.copy($scope.task.comment);
        if ($scope.config.taskMessageBuf) {
          if (!comments) {
            comments = [];
          }
          //    if (data.data) {
          comments.push({
            createDate: new Date(),
            userId: $scope.task.assigne,
            message: $scope.config.taskMessageBuf
          });

        }
        return comments;
      };
      var saveTask = function () {
        $scope.$emit('saveTask');
      };
      // Show model window for getting reason of canceling task from user
      var unclaimTaskEventListener = function () {

        operationCancelService.showDialog().then(function (data) {

          var taskCancelData = {
            taskId: $scope.config.task.id,
            processInstanceId: $scope.config.task.processInstanceId,
            userId: 'user'
          };
          taskCancelData = {taskCancellation: angular.merge(taskCancelData, data)};
          operationService.cancelTask(taskCancelData).then(function () {
            $rootScope.widgets.kendoNotification.show(gettextCatalog.getString('Task canceled successfully', 'info'));
            $rootScope.$broadcast('taskCompleted', null);
          });
          $scope.config.window.close();

        });

      };
      // Show model window for getting reason of canceling task from user
      var rejectTaskEventListener = function () {
        operationRejectService.showDialog($scope.rejectionConfig).then(function (data) {

            var comments = angular.copy(getComments());
            if(!angular.isArray(comments))
                comments = [];
            if(data.comment !== undefined && data.comment.length > 0) {
                comments.push({
                    createDate: new Date(),
                    userId: $scope.task.assigne,
                    message: data.comment
                });
            }
          var formData = {
            taskData: {
              id: $scope.task.id,
              key: $scope.task.key,
              comment: comments,
              processInstanceId: $scope.task.processInstanceId,
              draft: $scope.task.draft,
              approverDecision: data.decision,
            }
          };
          operationService.completeTask(formData).then(function (res) {
            if (res.resultCode == 200 && res.success == "true") {
              if (res.success === "true" && res.data) {
                operationService.getSingleOperation(res.data.key).then(function (config) {

                  var taskData = res.data;
                  taskData.config = config;
                  operationService.openTask(taskData);
                });
                $scope.config.window.close(null, false);
                $rootScope.$broadcast('selectHomePageTab', 1);
              }
              else {
                $rootScope.widgets.kendoNotification.show(gettextCatalog.getString('Task rejected successfully', 'info'));
                $rootScope.$broadcast('taskCompleted', null);
                $scope.config.window.close(null, true);
              }

            }
            else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
            }
          });

        });
      };
      $scope.config.showTaskSavePrompt = function (formIsDirty) {

        if ($scope.config.operationType == appConstants.operationTypes.entry && formIsDirty) {
          dialogService.showDialog(gettextCatalog.getString('Close Task'),
            gettextCatalog.getString('Do you want to save changes?')).then(function (res) {

              if (res == "yes") {
                saveTask();
                $scope.config.window.close();
              }
              else if (res == "no") {
                $scope.config.window.close();
              }

            });
        }
        else {
          $scope.config.window.close();
        }
      };

      var validateTaskDocuments = function () {
        var result = {
          success: true,
          message: gettextCatalog.getString('You have to upload below documents:')
        };
        if ($scope.task.document && $scope.task.document.length) {
          for (var i = 0; i < $scope.task.document.length; i++) {
            var hasError = false;
            var item = $scope.task.document[i];
            item.minLimit = parseInt(item.minLimit);
            if (item.minLimit && item.disabled != true) {
              if (item.documents) {
                if (item.documents.length < item.minLimit) {
                  hasError = true;
                }
              } else {
                hasError = true;
              }
            }
            if (hasError) {
              result.success = false;
              result.message += "\n" + item.documentClass['name' + $rootScope.lnC] + ", " + item.minLimit + " " + gettextCatalog.getString('file');

            }
          }
        }
        return result;
      };

      window.validateTaskDocuments = validateTaskDocuments;

      // Complete task for approve, send payload data
      $scope.config.completeTaskApprove = function (payload) {
          var formData = new FormData();
          for (var i = 0; i < $scope.config.uploader.queue.length; i++) {
              formData.append("uploadedFile", $scope.config.uploader.queue[i]['_file']);
              $scope.config.uploader.queue[i]['file'].isSuccess = true;
          }
          var formData = {
              taskData: {
                  id: $scope.task.id,
                  key: $scope.task.key,
                  processInstanceId: $scope.task.processInstanceId,
                  draft: angular.toJson(payload),
                  comment: getComments(),
                  approverDecision: 'APPROVAL_DECISION_APPROVE',
                  formData: formData
              }
          };
          operationService.completeTask(formData).then(function (res) {
              if (res.resultCode == 200 && res.success == "true") {
                  if (res.success === "true" && res.data) {
                      operationService.getSingleOperation(res.data.key).then(function (config) {

                          var taskData = res.data;
                          taskData.config = config;
                          operationService.openTask(taskData);
                      });
                      $scope.config.window.close(null, false);
                      $rootScope.$broadcast('selectHomePageTab', 1);
                  }
                  else {
                      $rootScope.widgets.kendoNotification.show(gettextCatalog.getString('Task completed successfully', 'info'));
                      $rootScope.$broadcast('taskCompleted', null);
                      $scope.config.window.close(null, true);

                  }

              }
              else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
              }
          });
      };
// Complete task , send payload data
      $scope.config.completeTask = function (payload) {

        var validationResult = validateTaskDocuments();
        if (validationResult.success == true) {
          console.log('Payload After Complete :', angular.fromJson(payload));
          var taskData = {
            taskData: {
              id: $scope.task.id,
              key: $scope.task.key,
              comment: getComments(),
              processInstanceId: $scope.task.processInstanceId,
              draft: angular.toJson(payload)
            }
          };
          console.log('Task Data',taskData);

          operationService.completeTask(taskData).then(function (res) {
            if (res.resultCode == 200 && res.success == "true") {
              if (res.success === "true" && res.data) {
                operationService.getSingleOperation(res.data.key).then(function (config) {
                  var taskData = res.data;
                  taskData.config = config;
                  operationService.openTask(taskData);
                });
                $scope.config.window.close(null, false);
                $rootScope.$broadcast('selectHomePageTab', 1);
              }
              else {
                $rootScope.widgets.kendoNotification.show(gettextCatalog.getString('Task completed successfully', 'info'));
                $rootScope.$broadcast('taskCompleted', null);
                $scope.config.window.close(null, true);
              }
            }
            else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
            }
          });
        } else {
          SweetAlert.swal('', validationResult.message, 'error')
        }

      };

// Save task as draft
      $scope.config.saveTask = function (payload) {
        var formData = {
          taskData: {
            comment: getComments(),
            id: $scope.task.id,
            key: $scope.task.key,
            processInstanceId: $scope.task.processInstanceId,
            draft: angular.toJson(payload)
          }
        };
        Loader.show(true);
        operationService.saveTask(formData).then(function (res) {
          Loader.show(false);

          if (res.resultCode == 200 && res.success == "true") {
            $rootScope.widgets.kendoNotification.show(gettextCatalog.getString('Task saved successfully', 'info'));
          }
          else {

            SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
          }
        });

      };

// action configurations
      var operationEntryButtons = {
        unclaim: {
          title: gettextCatalog.getString('Cancel'),
          icon: 'minus',
          click: unclaimTaskEventListener
        },
        complete: {
          title: gettextCatalog.getString('Complete'),
          icon: 'ok'
        },
        saveAsDraft: {
          title: gettextCatalog.getString('Save'),
          icon: 'floppy-save',
          click: function () {
            saveTask();
          }
        },
        close: {
          title: gettextCatalog.getString('Close'),
          icon: 'remove',
          click: function () {
            $scope.$emit('closeTask');
          }
        }
      };
      var operationApprovalButtons = {
        unclaim: {
          title: gettextCatalog.getString('Cancel'),
          icon: 'minus',
          click: unclaimTaskEventListener
        },
        reject: {
          title: gettextCatalog.getString('Reject'),
          icon: 'ban-circle',
          click: rejectTaskEventListener
        },
        complete: {
          title: gettextCatalog.getString('Complete'),
          icon: 'ok',
          click: function () {

            var formData = new FormData();
            for (var i = 0; i < $scope.config.uploader.queue.length; i++) {
              formData.append("uploadedFile", $scope.config.uploader.queue[i]['_file']);
              $scope.config.uploader.queue[i]['file'].isSuccess = true;
            }
            var formData = {
              taskData: {
                id: $scope.task.id,
                key: $scope.task.key,
                processInstanceId: $scope.task.processInstanceId,
                draft: $scope.task.draft,
                comment: getComments(),
                approverDecision: 'APPROVAL_DECISION_APPROVE',
                formData: formData
              }
            };
            operationService.completeTask(formData).then(function (res) {
              if (res.resultCode == 200 && res.success == "true") {
                if (res.success === "true" && res.data) {
                  operationService.getSingleOperation(res.data.key).then(function (config) {

                    var taskData = res.data;
                    taskData.config = config;
                    operationService.openTask(taskData);
                  });
                  $scope.config.window.close(null, false);
                  $rootScope.$broadcast('selectHomePageTab', 1);
                }
                else {
                  $rootScope.widgets.kendoNotification.show(gettextCatalog.getString('Task completed successfully', 'info'));
                  $rootScope.$broadcast('taskCompleted', null);
                  $scope.config.window.close(null, true);

                }

              }
              else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
              }
            });
          }
        },
        close: {
          title: gettextCatalog.getString('Close'),
          icon: 'remove',
          click: function () {
            $scope.config.window.close();
          }
        }
      };
// Approval task settings
      if ($scope.config.operationType == appConstants.operationTypes.approval) {

        $scope.config._buttons = operationApprovalButtons;
        $scope.taskData = angular.copy(angular.fromJson($scope.task.draft));

      }
// Entry task settings
      else {
        $scope.config._buttons = operationEntryButtons;
      }
// Check if task is unclaimed then claim task
      if ($scope.config.state == appConstants.operationStates.unclaimed) {
        operationService.claimTask($scope.config.task.id);
      }

// Merge task actions
      $scope.config._buttons = angular.merge($scope.config._buttons, $scope.config.buttons);

// Global Methods [[[
      $scope.openWindow = function (elem) {
        elem.open();
        elem.center();
      };
      $scope.renderHtml = function (html_code) {
        return $sce.trustAsHtml(html_code);
      };


      $scope._saveBase64File = helperFunctionsService.saveBase64File;
      $scope._printHtml = helperFunctionsService.printHtml;
      $scope._getDayOfWeek = helperFunctionsService.getDayOfWeek;
      $scope._saveBase64AsBlob = helperFunctionsService.saveBase64AsBlob;
      $scope._openBase64File = helperFunctionsService.openBase64File;
      $scope._toggleCommentBlock = function (id) {
        $mdSidenav(id + "_rightCommentBlock").toggle();
      };
      $scope._toggleHelpBlock = function (id) {
        $mdSidenav(id + "_helpBlock").toggle();
      };
      $scope._toggleInformationBlock = function (id) {
        $mdSidenav(id + "_rightInformationBlock").toggle();
      };
      $scope._toggleDocumentBlock = function (id) {
        $mdSidenav(id + "_rightDocumentBlock").toggle();
      };
      $rootScope._closeWindow = function (window) {
        if (window) {
          window.close();
        }
      };
      $scope._exportToExcel = function (grid) {
        grid.saveAsExcel();
      };

// Global Methods ]]]

      $scope.removeComment = function () {
        $scope.config.taskMessageBuf = null;
      }

    }])
;
