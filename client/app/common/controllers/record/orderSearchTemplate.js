'use strict';

angular.module('cmisApp')
  .controller('OrderSearchTemplateController',
  ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', '$rootScope', 'helperFunctionsService', "$filter",'$document',
    function ($scope, recordService, Loader, gettextCatalog, appConstants, SweetAlert, $rootScope, helperFunctionsService, $filter, $document) {

      $scope.gridColumns = [

        {
          field: "orderID",
          title: gettextCatalog.getString("Order ID"),
          width: "10rem"
        },
        {
          field: "refOrderID",
          title: gettextCatalog.getString("Reference Order ID"),
          width: "10rem"
        },
        {
          field: "orderClass.name" + $rootScope.lnC,
          title: gettextCatalog.getString("Order Class"),
          width: "10rem"
        },
        {
          field: "orderType.name" + $rootScope.lnC,
          title: gettextCatalog.getString("Order Type"),
          width: "10rem"
        },
        {
          field: "instrumentCode",
          title: gettextCatalog.getString("Instrument Code"),
          width: "10rem"
        },
        {
          field: "clientAccountNumber",
          title: gettextCatalog.getString("Client Account Number"),
          width: "10rem"
        },
        {
          field: "brokerCode",
          title: gettextCatalog.getString("Broker Code"),
          width: "10rem"
        },
        {
          field: "quantity",
          title: gettextCatalog.getString("Quantity"),
          width: "10rem",
          template: function(dataItem) {
            return dataItem.quantity ? $filter("formatNumber")(dataItem.quantity, false) : "";
          }
        },
        {
          field: "price",
          title: gettextCatalog.getString("Price"),
          width: "10rem",
          template: "<div style='text-align: right'>#= kendo.toString(price, 'n2') # </div>"
        },
        {
          field: "amount",
          title: gettextCatalog.getString("Amount"),
          width: "10rem",
          template: function(dataItem) {
            return dataItem.amount ? "<div style='text-align: right;'>" + $filter("formatNumber")(dataItem.amount) + "</div>" : "";
          }
        },
        {
          field: "filledQuantity",
          title: gettextCatalog.getString("Filled Quantity"),
          width: "10rem"
        },
        {
          field: "filledAmount",
          title: gettextCatalog.getString("Filled Amount"),
          width: "10rem",
          template: "<div style='text-align: right'>#= kendo.toString(filledAmount, 'n2') # </div>"
        },
        {
          field: "orderTime",
          title: gettextCatalog.getString("Order Time"),
          width: "10rem",
          format: "{0:dd-MMMM-yyyy HH:mm}"
        },
        {
          field: "marketID",
          title: gettextCatalog.getString("Market ID"),
          width: "10rem"
        },
        {
          field: "boardID",
          title: gettextCatalog.getString("Board ID"),
          width: "10rem"
        },
        {
          field: "rejectionCode",
          title: gettextCatalog.getString("Rejection Code"),
          width: "10rem"
        },
        {
          field: "rejectionMessage",
          title: gettextCatalog.getString("Rejection Message"),
          width: "10rem"
        }
      ];

      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        if (formData.recordNumber || formData.comment) {
          result.success = true;
        }
        return result;
      };

      var findData = function () {

        if ($scope.order.searchResultGrid !== undefined) {
          $scope.order.searchResultGrid.refresh();
        }

        $scope.order.selectedOrderIndex = false;

        if ($scope.order.formName.$dirty) {
          $scope.order.formName.$submitted = true;
        }

        if ($scope.order.formName.$valid || !$scope.order.formName.$dirty) {

          $scope.order.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.order.form);


          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            recordService.getOrders($scope.params.searchUrl, formData).then(function (data) {

              if (data.success === "true") {
                data = {
                  'data': data.data,
                  pageSize: 100,

                  schema: {
                    model: {
                      fields: {
                        orderTime: {type: "date"}
                      }
                    }
                  },
                  sort: {
                    field: "orderID",
                    dir: "desc"
                  }

                };
                $scope.gridData = data;
                $scope.order.searchResult.data = data;

              } else {
                $scope.order.searchResult.isEmpty = true;
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }


              Loader.show(false);
            });
          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };

      var _params = {
        searchUrl: "/api/records/getOrders",
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _order = {
        formName: 'orderFindDorm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedOrderIndex: false,
        findData: findData
      };

      $scope.order = angular.merge($scope.searchConfig.order, _order);

      $scope.findData = findData;

      $scope.$on('refreshGrid', function () {
        findData();
      });

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.order.form.comment = null;
        $scope.order.form.recordNumber = null;
      };

      if ($scope.params.config.searchOnInit == true) {
        findData();
      }

      setTimeout(function () {
        $('.k-pager-refresh').on('click',function(){
          findData();
        })
      }, 2000)


      var orderSelected = function (data) {

        var sResult = angular.copy($scope.order.searchResult.data.data);

        for (var i = 0; i < sResult.length; i++) {

          if (sResult[i].id == data.id) {

            $scope.order.selectedOrderIndex = i;
            break;
          }
        }
      };

      $scope.orderSelected = orderSelected;
    }]);
