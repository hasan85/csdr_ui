'use strict';

angular.module('cmisApp')
  .controller('MatchedTradeFindController', ['$scope', '$windowInstance', 'recordsService', 'searchCriteria',
    function ($scope, $windowInstance, recordsService, searchCriteria) {

      $scope.searchConfig = {
        OTCTrade: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedFreezingIndex: false
        },
        params: {
                 showSearchCriteriaBlock: true,
          config: {
            searchOnInit: true,
            searchCriteria:searchCriteria
          }
        }
      };

      $scope.selectMatchedTrade = function () {
        recordsService.selectMatchedTrade($scope.searchConfig.OTCTrade.searchResult.data.data[$scope.searchConfig.OTCTrade.selectedOTCTradeIndex]);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
