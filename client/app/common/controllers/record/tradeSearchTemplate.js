'use strict';

angular.module('cmisApp')
  .controller('TradeSearchTemplateController',
  ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
    function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {


      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};


          result.success = true;

        return result;
      };

      var findData = function (e) {
        if ($scope.trade.searchResultGrid !== undefined) {
          $scope.trade.searchResultGrid.refresh();
        }
        if ($scope.trade.formName.$dirty) {
          $scope.trade.formName.$submitted = true;
        }

        if ($scope.trade.formName.$valid || !$scope.trade.formName.$dirty) {

          $scope.trade.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.trade.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            formData.regDateStart = $scope.trade.form.regDateStart ? $scope.trade.form.regDateStartObj : null;
            formData.regDateFinish = $scope.trade.form.regDateFinish ? $scope.trade.form.regDateFinishObj : null;
            formData.settlementDateStart = $scope.trade.form.settlementDateStart ? $scope.trade.form.settlementDateStartObj : null;
            formData.settlementDateFinish = $scope.trade.form.settlementDateFinish ? $scope.trade.form.settlementDateFinishObj : null;
            formData.valueDateStart = $scope.trade.form.valueDateStart ? $scope.trade.form.valueDateStartObj : null;
            formData.valueDateFinish = $scope.trade.form.valueDateFinish ? $scope.trade.form.valueDateFinishObj : null;
            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            window.onbeforeunload = null;
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

              setTimeout(function() {
                window.onbeforeunload = $rootScope.onBeforeUnloadHandler;
              }, 1000);

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.trade.searchResult.data = data;
                } else {
                  $scope.trade.searchResult.isEmpty = true;
                }

                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };

      var _params = {
        searchUrl: "/api/records/getTrades/",
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _trade = {
        formName: 'tradeFindForm',
        form: {},
        data: {},
        metaData: {
          marketTypes: ['',"STK", "BND", "PRM", "RPO"]
        },
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedtradeIndex: false,
        findData: findData
      };

      $scope.trade = angular.merge($scope.searchConfig.trade, _trade);

      $scope.findData = function () {

        var formValidationResult = validateForm(angular.copy($scope.trade.form));
        if (formValidationResult.success) {
          if ($scope.trade.searchResultGrid) {
            $scope.trade.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      $scope.trade.mainGridOptions = {
        excel: {
          allPages: true,
          fileName: "birja_eqdleri.xlsx",
          proxyURL: "/api/main/export",
          forceProxy: true
        },
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                regDate: {type: "date"},
                settlementDate: {type: "date"},
                valueDate: {type: "date"},
                maturityDate: {type: "date"},
                price: { type: "number" },
                amount: { type:"number" },
                quantity: { type: "number" },
                parValue: {
                  type: "number",
                  from: "instrument.parValue"
                }
              }
            }
          },
          transport: {
            read: function (e) {
              findData(e);
            }
          },

          serverPaging: true,
          sort: {field: "regDate", dir: "desc"},
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "recordNumber",
            title: gettextCatalog.getString("Trade Number"),
            width: 200,
            locked: true,
            lockable: false
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Trade Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },
          {
            field: "recordStatus.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Trade Status"),
            width: "10rem"
          },
          {
            field: "settlementDate",
            title: gettextCatalog.getString("Settlement Date"),
            type: "date",
            width: "10rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },

          {
            field: "buySideMember.name",
            title: gettextCatalog.getString("Buy Side Member"),
            width: "10rem"
          },
          {
            field: "buySideClient.name",
            title: gettextCatalog.getString("Buy Side Client"),
            width: "10rem"
          },
            // {
            //     field: 'buySideClient.personClass',
            //     title: 'Müştərinin növü',
            //     width: '10rem'
            // },
          {
            field: 'buySideClient.accountNumber',
            title: gettextCatalog.getString("Buy Side Client Account Number"),
            width: "15rem"
          },
          {
            field: "sellSideMember.name",
            title: gettextCatalog.getString("Sell Side Member"),
            width: "10rem"
          },
          {
            field: "sellSideClient.name",
            title: gettextCatalog.getString("Sell Side Client"),
            width: "10rem"
          },
            // {
            //     field: "sellSideClient.personClass",
            //     title: 'Müştərinin növü',
            //     width: '10rem'
            // },
          {
            field: "sellSideClient.accountNumber",
            title: gettextCatalog.getString("Sell Side Client Account Number"),
            width: "14rem"
          },
          {
            field: "instrument.instrumentName",
            title: gettextCatalog.getString("Instrument"),
            width: "10rem"
          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("ISIN"),
            width: "10rem"
          },
          {
            field: "instrument.issuerName",
            title: gettextCatalog.getString("Issuer"),
            width: "10rem"
          },
          {
            field: "parValue",
            title: gettextCatalog.getString("Par Value"),
            width: "10rem"
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "10rem"
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount"),
            width: "10rem"
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency"),
            width: "10rem"
          },
            {
                field: "commission",
                title: gettextCatalog.getString("Commission"),
                width: "10rem"
            },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },
          {
            field: "maturityDate",
            title: gettextCatalog.getString("Maturity Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: "10rem"
          },
          {
            field: "price",
            title: gettextCatalog.getString("Price"),
            width: "10rem"
          },
          {
            field: "marketType",
            title: gettextCatalog.getString("Market ID"),
            width: "10rem"
          }
        ]
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.trade.form.destination = null;
        $scope.trade.form.referenceNumber = null;
        $scope.trade.form.regDateStart = null;
        $scope.trade.form.regDateFinish = null;
        $scope.trade.form.settlementDateStart = null;
        $scope.trade.form.settlementDateFinish = null;
        $scope.trade.form.sellSideMemberName = null;
        $scope.trade.form.sellSideClientName = null;
        $scope.trade.form.buySideMemberName = null;
        $scope.trade.form.buySideClientName = null;
        $scope.trade.form.ISIN = null;
        $scope.trade.form.issuerName = null;
        $scope.trade.form.marketType = '';
        //$scope.OTCTrade.form.marketType = null;

      };
    }]);
