'use strict';

angular.module('cmisApp')
  .controller('SuccessionSearchTemplateController',
  ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
    function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {


      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};


          result.success = true;

        return result;
      };

      var findData = function (e) {

        if ($scope.OTCTrade.searchResultGrid !== undefined) {
          $scope.OTCTrade.searchResultGrid.refresh();
        }
        if ($scope.OTCTrade.formName.$dirty) {
          $scope.OTCTrade.formName.$submitted = true;
        }

        if ($scope.OTCTrade.formName.$valid || !$scope.OTCTrade.formName.$dirty) {

          $scope.OTCTrade.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.OTCTrade.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            formData.regDateStart = $scope.OTCTrade.form.regDateStart ? $scope.OTCTrade.form.regDateStartObj : null;
            formData.regDateFinish = $scope.OTCTrade.form.regDateFinish ? $scope.OTCTrade.form.regDateFinishObj : null;
            formData.settlementDateStart = $scope.OTCTrade.form.settlementDateStart ? $scope.OTCTrade.form.settlementDateStartObj : null;
            formData.settlementDateFinish = $scope.OTCTrade.form.settlementDateFinish ? $scope.OTCTrade.form.settlementDateFinishObj : null;
            formData.valueDateStart = $scope.OTCTrade.form.valueDateStart ? $scope.OTCTrade.form.valueDateStartObj : null;
            formData.valueDateFinish = $scope.OTCTrade.form.valueDateFinish ? $scope.OTCTrade.form.valueDateFinishObj : null;
            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.OTCTrade.searchResult.data = data;
                } else {
                  $scope.OTCTrade.searchResult.isEmpty = true;
                }

                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };

      var _params = {
        searchUrl: "/api/records/getOTCTrades/",
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _OTCTrade = {
        formName: 'OTCTradeFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedOTCTradeIndex: false,
        findData: findData
      };

      $scope.OTCTrade = angular.merge($scope.searchConfig.OTCTrade, _OTCTrade);

      $scope.findData = function () {

        var formValidationResult = validateForm(angular.copy($scope.OTCTrade.form));
        if (formValidationResult.success) {
          if ($scope.OTCTrade.searchResultGrid) {
            $scope.OTCTrade.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      $scope.OTCTrade.mainGridOptions = {
        excel: {
          allPages: true
        },
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                regDate: {type: "date"},
                settlementDate: {type: "date"},
                valueDate: {type: "date"},
                maturityDate: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findData(e);
            }
          },

          serverPaging: true,
          sort: {field: "regDate", dir: "desc"},
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "recordNumber",
            title: gettextCatalog.getString("Record Number"),
            locked: true,
            lockable: false,
            width: "220px"
            //width: "10rem"
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Record Date"),
            width: "12rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },
          {
            field: "recordStatus.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Record Status"),
            width: "12rem"
          },
          {
            field: "settlementDate",
            title: gettextCatalog.getString("Settlement Date"),
            type: "date",
            width: "12rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },

          {
            field: "buySideClient.name",
            title: gettextCatalog.getString("Holder"),
            width: "12rem"
          },
          {
            field: "sellSideClient.name",
            title: gettextCatalog.getString("Successor"),
            width: "12rem"
          },
          {
            field: "buySideClient.shareholderAccountNumber",
            title: gettextCatalog.getString("Holder Account Number"),
            width: "12rem"
          },
          {
            field: "sellSideClient.shareholderAccountNumber",
            title: gettextCatalog.getString("Successor Account Number"),
            width: "12rem"
          },
          {
            field: "buySideClient.pinCode",
            title: "Mərhumun pin kodu",
            width: "12rem"
          },
          {
            field: "sellSideClient.pinCode",
            title: "Varisin pin kodu",
            width: "12rem"
          },
          {
            field: "buySideClient.participantAccountNumber",
            title: "Mərhumun iştirakçı hesabı",
            width: "12rem"
          },
          {
            field: "sellSideClient.participantAccountNumber",
            title: "Varisin iştirakçı hesabı",
            width: "12rem"
          },
          {
            field: "instrument.instrumentName",
            title: gettextCatalog.getString("Instrument"),
            width: "12rem"
          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("ISIN"),
            width: "12rem"
          },
          {
            field: "instrument.issuerName",
            title: gettextCatalog.getString("Issuer"),
            width: "12rem"
          },

          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "12rem"
          },
            {
                field: "commission",
                title: gettextCatalog.getString("Commission"),
                width: "12rem"
            },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: "12rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          }
        ]
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.OTCTrade.form.destination = null;
        $scope.OTCTrade.form.referenceNumber = null;
        $scope.OTCTrade.form.regDateStart = null;
        $scope.OTCTrade.form.regDateFinish = null;
        $scope.OTCTrade.form.settlementDateStart = null;
        $scope.OTCTrade.form.settlementDateFinish = null;
        $scope.OTCTrade.form.valueDateStart = null;
        $scope.OTCTrade.form.valueDateFinish = null;
        $scope.OTCTrade.form.sellSideMemberName = null;
        $scope.OTCTrade.form.sellSideClientName = null;
        $scope.OTCTrade.form.buySideMemberName = null;
        $scope.OTCTrade.form.buySideClientName = null;
        $scope.OTCTrade.form.ISIN = null;
        $scope.OTCTrade.form.issuerName = null;
      };
    }]);
