'use strict';

angular.module('cmisApp')
  .controller('UnknownPaymentRecordsSearchController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        var defaultParams = {
          searchUrl: "/api/persons/getUnderwriters/",
          showSearchCriteriaBlock: false,
          config: {
            searchOnInit: false,
          }
        };

        $scope.params = angular.merge(defaultParams, $scope.searchConfig.params);

        var validateForm = function (formData) {

          var result = {
            success: false,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };
          // if (formData.clientName || formData.brokerName || formData.finishDateStart || formData.finishDateFinish) {
          result.success = true;
          //}
          return result;
        };

        $scope.test = {
          personSelected:function (data) {
            var sResult = angular.copy($scope.bla);

            for (var i = 0; i < sResult.length; i++) {

              if (sResult[i].ID == data.ID) {
                $scope.searchConfig.accountPrescription.selectedPersonIndex = sResult[i];

                break;
              }
            }

          }
        };


        var findData = function (e) {


          if ($scope.search.searchResultGrid !== undefined) {
            $scope.search.searchResultGrid.refresh();
          }
          if ($scope.search.formName.$dirty) {
            $scope.search.formName.$submitted = true;
          }

          if ($scope.search.formName.$valid || !$scope.search.formName.$dirty) {

            $scope.search.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.search.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {

              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }


              formData.paymentDateStart = $scope.search.form.paymentDateStart ? $scope.search.form.paymentDateStartObj : null;
              formData.paymentDateFinish = $scope.search.form.paymentDateFinish ? $scope.search.form.paymentDateFinishObj : null;


              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take,
                sort: e.data.sort
              });


              console.log(requestData)
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).success(function (data) {
                $scope.bla = angular.isArray(data.data) ? data.data: [data.data]
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };


        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];


        $scope.search = angular.merge(
          $scope.searchConfig.accountPrescription,
          {
            formName: 'search',
            form: {
              status: "STATUS_NOT_SETTLED"
            },
            data: {},

            searchResult: {
              data: null,
              isEmpty: false
            }
          }
        );


        $scope.findData = function () {

          var formValidationResult = validateForm(angular.copy($scope.search.form));

          if (formValidationResult.success) {
            if ($scope.search.searchResultGrid) {
              $scope.search.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };


        $scope.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  creationDate: {type: "date"},
                  paymentDate: {type: "date"}

                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },
            sort: {field: "creationDate", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "creationDate",
              title: gettextCatalog.getString("Creation Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy hh:mm}"
            },
            {
              field: "refNumber",
              title: gettextCatalog.getString("Reference Number"),
              width: "10rem"
            },
            {
              field: "paymentDate",
              title: gettextCatalog.getString("Payment Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "currency.code",
              title: gettextCatalog.getString("Currency"),
              width: "5rem"
            },
            {
              field: "paidAmount",
              title: gettextCatalog.getString("Amount"),
              width: "7rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(paidAmount, 'n2') # </div>"
            },
            {
              field: "destination",
              title: "Təyinat",
              width: "10rem"
            },
            {
              field: "statusMessage",
              title: "Status mesajı",
              width: "15rem"
            },
            {
              field: "payerClientAccountNumber",
              title: "Ödəyici tərrəfin hesab nömrəsi",
              width: "15rem"
            },
            {
              field: "payerClientName",
              title: "Ödəyici tərəfin adı",
              width: "10rem"
            },
            {
              field: "payerClientTin",
              title: "Ödəyici tərəfin VÖEN-i",
              width: "10rem"
            },
            {
              field: "payerClientCode",
              title: "Ödəyici tərəfin PİN Kodu",
              width: "10rem"
            },
            {
              field: "payerBankTin",
              title: "Ödəyici bankın VÖEN-i",
              width: "10rem"
            },
            {
              field: "payerBankCode",
              title: "Ödəyici bankın kodu",
              width: "10rem"
            },
            {
              field: "payerBankBIC",
              title: "Ödəyici bankın BİC-i",
              width: "10rem"
            },
            {
              field: "payerBankCorrespondentAccount",
              title: "Ödəyici bankın müxbir hesabı",
              width: "10rem"
            }
          ]
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.search.form.paymentDateStart = null;
          $scope.search.form.paymentDateFinish = null;
          $scope.search.form.referenceNumber = null;
          $scope.search.form.currencyCode = null;
          $scope.search.form.payeeClientAccountNumber = null;
          $scope.search.form.payeeClientAccountName = null;
          $scope.search.form.serviceClass = null;
        };
      }]
  );
