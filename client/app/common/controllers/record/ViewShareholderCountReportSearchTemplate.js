'use strict';

angular.module('cmisApp')
  .controller('ViewShareholderCountReportSearchTemplate',
  ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
    function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

      $scope.reportDatePickerOptions = {
        value: new Date()
      };

      var validateForm = function (formData) {

        var result = {success: false, message: "Bütün bölmələr daxil edilməlidir"};

        if(formData.count && formData.reportDate)
          result.success = true;

        return result;
      };

      var findData = function (e) {
        if ($scope.trade.searchResultGrid !== undefined) {
          $scope.trade.searchResultGrid.refresh();
        }
        if ($scope.trade.formName.$dirty) {
          $scope.trade.formName.$submitted = true;
        }

        if ($scope.trade.formName.$valid || !$scope.trade.formName.$dirty) {

          $scope.trade.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.trade.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            if(formData.reportDateObj) {
              formData.reportDate = formData.reportDateObj;
            }

            var requestData = angular.extend(formData, {
              // skip: e.data.skip,
              // take: e.data.take,
              // sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.trade.searchResult.data = data;
                } else {
                  $scope.trade.searchResult.isEmpty = true;
                }

                console.log('Response data', data);
                if (data['success'] === "true") {

                  var gridData = data.data ? data.data: [];
                  e.success({
                    Data: gridData, Total: gridData.length
                  });
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };

      var _params = {
        searchUrl: "/api/reports/getShareholderCountReport/",
        showSearchCriteriaBlock: true,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _trade = {
        formName: 'tradeFindForm',
        form: {
          count: 100,
          reportDate: helperFunctionsService.generateDateTime(new Date()),
          reportDateObj: new Date()
        },
        data: {},
        metaData: {
          marketTypes: ['',"STK", "BND", "PRM", "RPO"]
        },
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedtradeIndex: false,
        findData: findData
      };

      $scope.trade = angular.merge($scope.searchConfig.trade, _trade);

      $scope.findData = function () {

        var formValidationResult = validateForm(angular.copy($scope.trade.form));

        if (formValidationResult.success) {
          if ($scope.trade.searchResultGrid) {
            $scope.trade.searchResultGrid.dataSource.read();
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      $scope.trade.mainGridOptions = {
        excel: {
          allPages: true,
          fileName: "shareholder_count_report.xlsx"
        },
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                // regDate: {type: "date"},
                // settlementDate: {type: "date"},
                // valueDate: {type: "date"},
                // maturityDate: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findData(e);
            }
          },

          serverPaging: false,
          serverSorting: false
        },
        selectable: false,
        scrollable: true,
        pageable: {"pageSize": 30, "refresh": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "issuerName",
            title: gettextCatalog.getString("Issuer"),
            width: 180
          },
          {
            field: "ISIN",
            title: gettextCatalog.getString("ISIN"),
            width: 170
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: 150
          }
        ]
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.trade.form.reportDate = null;
        $scope.trade.form.count = null;

      };
    }]);
