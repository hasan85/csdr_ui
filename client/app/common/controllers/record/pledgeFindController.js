'use strict';

angular.module('cmisApp')
  .controller('PledgeFindController', ['$scope', '$windowInstance', 'recordsService', 'searchCriteria',
    function ($scope, $windowInstance, recordsService, searchCriteria) {

      $scope.searchConfig = {
        pledge: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPledgeIndex: false
        },
        params: {
                 showSearchCriteriaBlock: true,
          config: {
            searchOnInit: true,
            searchCriteria:searchCriteria
          }
        }
      };

      $scope.selectPledge = function () {
        recordsService.selectPledge($scope.searchConfig.pledge.searchResult.data.data[$scope.searchConfig.pledge.selectedPledgeIndex]);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
