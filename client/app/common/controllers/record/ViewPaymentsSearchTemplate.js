'use strict';

angular.module('cmisApp')
  .controller('ViewPaymentsSearchTemplateController',
  ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants',
    '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http', "$q",
    function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http, $q) {


      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

        return result;
      };

      var findData = function (e) {

        if ($scope.searchConfig.OTCTrade.searchResultGrid !== undefined) {
          $scope.searchConfig.OTCTrade.searchResultGrid.refresh();
        }
        if ($scope.OTCTrade.formName.$dirty) {
          $scope.OTCTrade.formName.$submitted = true;
        }

        if ($scope.OTCTrade.formName.$valid || !$scope.OTCTrade.formName.$dirty) {

          $scope.OTCTrade.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.OTCTrade.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            formData.startTime = $scope.OTCTrade.form.startTime ? $scope.OTCTrade.form.startTimeObj : null;
            formData.finishTime = $scope.OTCTrade.form.finishTime ? $scope.OTCTrade.form.finishTimeObj : null;

            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.OTCTrade.searchResult.data = data;
                } else {
                  $scope.OTCTrade.searchResult.isEmpty = true;
                }

                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };

      var _params = {
        searchUrl: "/api/records/getOTCTrades/",
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _OTCTrade = {
        formName: 'OTCTradeFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },
        hasSelectedOTCTrade: false,
        findData: findData
      };

      $scope.OTCTrade = angular.merge($scope.searchConfig.OTCTrade, _OTCTrade);

      $scope.findData = function () {

        var formValidationResult = validateForm(angular.copy($scope.OTCTrade.form));
        if (formValidationResult.success) {
          if ($scope.searchConfig.OTCTrade.searchResultGrid) {
            $scope.searchConfig.OTCTrade.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.OTCTrade.mainGridOptions = {
        excel: {
          allPages: true
        },
        change: function(e) {
          var selectedRow = this.dataItem(this.select());
          $scope.OTCTrade.hasSelectedOTCTrade = !!selectedRow;
          $scope.$apply();
        },
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                date: {type: "date"},
                creationDate: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findData(e);
            }
          },

          serverPaging: true,
          sort: {field: "creationDate", dir: "desc"},
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "creationDate",
            title: "Yaranma tarixi",
            width: "12rem",
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },
          {
            field: "regNumber",
            title: "Qeydiyyat nömrəsi",
            width: "220px"
          },
          {
            field: "date",
            title: "Ödəniş tarixi",
            width: "12rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "currency.code",
            title: "Valyuta",
            width: "12rem"
          },
          {
            field: "amount",
            title: "Məbləğ",
            width: "220px"
          },

          {
            title: "Emitent iştirakçı",
            columns: [
              {
                field: "payerClientAccountNumber",
                title: "Müştəri hesab nömrəsi",
                width: "220px"
              },
              {
                field: "payerClientName",
                title: "Müştərinin adı",
                width: "220px"
              },
              {
                field: "payerClientTin",
                title: "Müştərinin VÖEN-i",
                width: "220px"
              },
              {
                field: "payerClientCode",
                title: "Müştərinin kodu",
                width: "220px"
              },
              {
                field: "payerBankTin",
                title: "Bankın VÖEN-i",
                width: "220px"
              },
              {
                field: "payerBankCode",
                title: "Bankın kodu",
                width: "220px"
              },
              {
                field: "payerBankBIC",
                title: "BIC",
                width: "220px"
              },
              {
                field: "payerBankCorrespondentAccount",
                title: "Muxbir hesab",
                width: "220px"
              },

            ]
          },

          {
            title: "Benefisiar iştirakçı",
            columns: [
              {
                field: "payeeClientAccountNumber",
                title: "Müştəri hesab nömrəsi",
                width: "220px"
              },
              {
                field: "payeeClientName",
                title: "Müştərinin adı",
                width: "220px"
              },
              {
                field: "payeeClientTin",
                title: "Müştərinin VÖEN-i",
                width: "220px"
              },
              {
                field: "payeeClientCode",
                title: "Müştərinin kodu",
                width: "220px"
              },
              {
                field: "payeeBankTin",
                title: "Bankın VÖEN-i",
                width: "220px"
              },
              {
                field: "payeeBankCode",
                title: "Bankın kodu",
                width: "220px"
              },
              {
                field: "payeeBankBIC",
                title: "BIC",
                width: "220px"
              },
              {
                field: "payeeBankCorrespondentAccount",
                title: "Muxbir hesab",
                width: "220px"
              },

            ]
          },
          {
            field: "destination",
            title: "Təyinat",
            width: "220px"
          },
          {
            field: "payerToPayeeInfo",
            title: "Ödəyicidən vəsaiti alana məlumat",
            width: "290px"
          },
          {
            field: "direction.name" + $rootScope.lnC,
            title: "İstiqamət",
            width: "220px"
          },
          {
            field: "status.name" + $rootScope.lnC,
            title: "Status",
            width: "220px"
          },
          {
            field: "errorMessage",
            title: "Səhv haqqında məlumat",
            width: "220px"
          },

        ]
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.OTCTrade.form.regNumber = null;
        $scope.OTCTrade.form.currencyCode = null;
        $scope.OTCTrade.form.startTime = null;
        $scope.OTCTrade.form.finishTime = null;
        $scope.OTCTrade.form.payerClientAccountNumber = null;
        $scope.OTCTrade.form.payerClientName = null;
        $scope.OTCTrade.form.payerBankBIC = null;
        $scope.OTCTrade.form.payeeClientAccountNumber = null;
        $scope.OTCTrade.form.payeeClientName = null;
        $scope.OTCTrade.form.payeeBankBIC = null;
        $scope.OTCTrade.form.destination = null;
        $scope.OTCTrade.form.statusCode = null;
        $scope.OTCTrade.form.direction = null;
        $scope.OTCTrade.form.errorMessage = null;
      };


    }]);
