'use strict';

angular.module('cmisApp')
  .controller('PledgeSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

        return result;
      };

      var pledgeSelected = function (data) {
        var sResult = angular.copy($scope.pledge.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].id == data.id) {
            $scope.pledge.selectedPledgeIndex = i;
            break;
          }
        }
      };


      $scope.pledgeSelected = pledgeSelected;

      var _params = {
        searchUrl: "/api/records/getPledgeRecords/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _pledge = {
        formName: 'pledgeFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedPledgeIndex: false,
        pledgeSelected: pledgeSelected,
        pledgeDetails: false
      };

      $scope.pledge = angular.merge($scope.searchConfig.pledge, _pledge);

      var findPledge = function (e) {

        //$scope.person.pledgeDetails = false;

        if ($scope.pledge.searchResultGrid !== undefined) {
          $scope.pledge.searchResultGrid.refresh();
        }

        $scope.pledge.selectedpledgeIndex = false;

        if ($scope.pledge.formName.$dirty) {
          $scope.pledge.formName.$submitted = true;
        }

        if ($scope.pledge.formName.$valid || !$scope.pledge.formName.$dirty) {

          $scope.pledge.searchResult.isEmpty = false;

          var formData = angular.copy($scope.pledge.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          formData.regDateStart = $scope.pledge.form.regDateStart ? $scope.pledge.form.regDateStartObj : null;
          formData.regDateFinish = $scope.pledge.form.regDateFinish ? $scope.pledge.form.regDateFinishObj : null;
          formData.settlementDateStart = $scope.pledge.form.settlementDateStart ? $scope.pledge.form.settlementDateStartObj : null;
          formData.settlementDateFinish = $scope.pledge.form.settlementDateFinish ? $scope.pledge.form.settlementDateFinishObj : null;
          formData.valueDateStart = $scope.pledge.form.valueDateStart ? $scope.pledge.form.valueDateStartObj : null;
          formData.valueDateFinish = $scope.pledge.form.valueDateFinish ? $scope.pledge.form.valueDateFinishObj : null;

          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.pledge.searchResult.data = data;
              } else {
                $scope.pledge.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };


      $scope.pledge.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                regDate: {type: "date"},
                valueDate: {type: "date"},
                maturityDate: {type: "date"},
                amount: {type: 'number'}
              }
            }
          },
          transport: {
            read: function (e) {
              findPledge(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "pledgeClass.name" + $rootScope.lnC,
            title: gettextCatalog.getString('Record Type'),
            width: '10rem'
          },
          {
            field: "recordNumber",
            title: gettextCatalog.getString("Order Number"),
            width: '10rem'
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Registration Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "maturityDate",
            title: gettextCatalog.getString("Maturity Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "pledgor.name",
            title: gettextCatalog.getString("Pledgor"),
            width: '10rem'
          },
          {
            field: "pledgee.name",
            title: gettextCatalog.getString("Pledgee"),
            width: '10rem'
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount"),
            template: "<div style='text-align: right'>#= kendo.toString(amount === null ? undefined : amount, 'n2') #</div>",
            width: '8rem'
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency"),
            width: '8rem'
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: '15rem'
          }
        ]
      };


      $scope.pledgeSelected = pledgeSelected;


      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {

        $scope.pledge.form.destination = null;
        $scope.pledge.form.referenceNumber = null;
        $scope.pledge.form.regDateStart = null;
        $scope.pledge.form.regDateFinish = null;
        $scope.pledge.form.settlementDateStart = null;
        $scope.pledge.form.settlementDateFinish = null;
        $scope.pledge.form.sellSideMemberName = null;
        $scope.pledge.form.sellSideClientName = null;
        $scope.pledge.form.buySideMemberName = null;
        $scope.pledge.form.buySideClientName = null;
        $scope.pledge.form.ISIN = null;
        $scope.pledge.form.issuerName = null;

      };


      $scope.findPledge = function () {

        var formValidationResult = validateForm(angular.copy($scope.pledge.form));
        if (formValidationResult.success) {
          if ($scope.pledge.searchResultGrid) {
            $scope.pledge.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.pledge.searchResultGrid) {
          $scope.pledge.searchResultGrid = widget;
        }
      });

      $scope.detailGrid = function (dataItem) {
        return {
          dataSource: {
            data: dataItem.recordSubjects && dataItem.recordSubjects.length ? dataItem.recordSubjects : [],
            pageSize: 5
          },
          scrollable: true,
          pageable: true,
          columns: [
            {
              field: "instrument.instrumentName",
              title: gettextCatalog.getString("Instrument Name"),
              width: "10rem"
            },
            {
              field: "instrument.ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "10rem"
            },
            {field: "instrument.issuerName", title: gettextCatalog.getString('Issuer'), width: "10rem"},
            {field: "quantity", title: gettextCatalog.getString('Quantity'), width: "10rem"}
          ]
        };
      };
    }]);
