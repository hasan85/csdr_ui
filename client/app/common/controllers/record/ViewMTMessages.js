'use strict';

angular.module('cmisApp')
  .controller('ViewMTMessagesController',
    ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
      '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
      function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
                $http, helperFunctionsService, recordsService, $rootScope) {

        var validateForm = function (formData) {
          var result = {
            success: false,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };

          result.success = true;

          return result;
        };

        var subscriptionOrdersSelected = function (data) {
          var sResult = angular.copy($scope.subscriptionOrders.searchResult.data.data);
          for (var i = 0; i < sResult.length; i++) {
            if (sResult[i].ID == data.ID) {
              $scope.subscriptionOrders.selectedSubscriptionOrdersIndex = i;
              break;
            }
          }
        };

        $scope.config = {};

        $scope.subscriptionOrdersSelected = subscriptionOrdersSelected;

        var _params = {
          searchUrl: "/api/common/getMTMessages/",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {},
            searchOnInit: true
          },
          criteriaEnabled: true
        };

        $scope.params = _params;

        $scope.params.config.splitterPanesConfig = [
          {
            size: "30%",
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _subscriptionOrders = {
          formName: 'subscriptionOrdersFindForm',
          form: {},
          data: {},

          searchResult: {
            data: null,
            isEmpty: false
          },

          selectedSubscriptionOrdersIndex: false,
          subscriptionOrdersSelected: subscriptionOrdersSelected,
          subscriptionOrdersDetails: false
        };

        $scope.subscriptionOrders = _subscriptionOrders;


        var findSubscriptionOrders = function (e) {

          //$scope.person.subscriptionOrdersDetails = false;

          if ($scope.subscriptionOrders.searchResultGrid !== undefined) {
            $scope.subscriptionOrders.searchResultGrid.refresh();
          }

          if ($scope.subscriptionOrders.formName.$dirty) {
            $scope.subscriptionOrders.formName.$submitted = true;
          }

          $scope.subscriptionOrders.searchResult.isEmpty = false;

          var formData = angular.copy($scope.subscriptionOrders.form);
          if ($scope.params.config.searchCriteria) {
            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          if(formData.creationDateEnd){
            formData.creationDateEnd = formData.creationDateEndObj
          }
          if(formData.creationDateStart){
            formData.creationDateStart = formData.creationDateStartObj
          }


          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log(e)

          //console.log('Request Data', requestData);
          Loader.show(true);
          $http({
            method: 'POST',
            url: $scope.params.searchUrl,
            data: {data: requestData}
          }).success(function (data, status, headers, config) {

            data.data = helperFunctionsService.convertObjectToArray(data.data);
            if (data) {
              $scope.subscriptionOrders.searchResult.data = data;
            } else {
              $scope.subscriptionOrders.searchResult.isEmpty = true;

            }

            //console.log('Response data', data);
            if (data['success'] === "true") {

              e.success({
                Data: data.data ? data.data : [], Total: data.total
              });
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }

            Loader.show(false);
          });

        };




        $scope.subscriptionOrders.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  netInputTime: {type: "date"},
                  netOutputDate: {type: "date"},
                  creationDate: {type: "date"},
                  responseTime: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findSubscriptionOrders(e);
              }
            },

            sort: {field: "creationDate", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            // {
            //   field: "ID",
            //   title: gettextCatalog.getString('ID'),
            //   width: "10rem"
            // },
            {
              field: "block4",
              title: gettextCatalog.getString("Block 4"),
              width: "10rem",
              template: function (dataItem) {
                return "<span><md-tooltip class='multiline' md-direction='left'><span style='white-space:pre'>" + dataItem.block4 + "</span></md-tooltip>" + dataItem.block4 + "</span>";
              }
            },
            {
              field: "msgType",
              title: gettextCatalog.getString("Message Type"),
              template:  "#= data.format + ' ' + data.msgType  #",
              width: '10rem'
            },
            {
              field: "creationDate",
              title: gettextCatalog.getString("Creation Date"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy HH:mm:ss}"
            },
            {
              field: "netInputTime",
              title: gettextCatalog.getString("Net Input Time"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy HH:mm:ss}"
            },
            {
              field: "netOutputDate",
              title: gettextCatalog.getString("Net Output Date"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy HH:mm:ss}"
            },
            {
              field: "netMir",
              title: gettextCatalog.getString("Net Mir"),
              width: '10rem'
            },
            {
              field: "userReference",
              title: gettextCatalog.getString("User Reference"),
              width: '10rem'
            },
            {
              field: "refMsgUserReference",
              title: gettextCatalog.getString("Reference Message User Reference"),
              width: '10rem'
            },
            {
              field: "sender",
              title: gettextCatalog.getString("Sender"),
              width: '10rem'
            },
            {
              field: "receiver",
              title: gettextCatalog.getString("Receiver"),
              width: '10rem'
            },
            {
              field: "session",
              title: gettextCatalog.getString("Session"),
              width: '10rem'
            },
            {
              field: "sequence",
              title: gettextCatalog.getString("Sequence"),
              width: '10rem'
            },
            {
              field: "ackNakType",
              title: gettextCatalog.getString("Ack Nak Type"),
              width: '10rem'
            },
            {
              field: "responseTime",
              title: gettextCatalog.getString("Response Time"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "errorCode",
              title: gettextCatalog.getString("Error Code"),
              width: '10rem'
            },
            {
              field: "errorDescription",
              title: gettextCatalog.getString("Error Description"),
              width: '10rem'
            },
            {
              field: "errorInfo",
              title: gettextCatalog.getString("Error Info"),
              width: '10rem'
            },


            {
              field: "copyServiceID",
              title: gettextCatalog.getString('Copy Service ID'),
              width: "10rem"
            },

            {
              field: "deliveryNotifRequest",
              title: gettextCatalog.getString("Delivery Notif Request"),
              width: '10rem',
            },
            {
              field: "finValidation",
              title: gettextCatalog.getString("Fin Validation"),
              width: '10rem'
            },
            {
              field: "macResult",
              title: gettextCatalog.getString("Mac Result"),
              width: '10rem'
            },
            {
              field: "pacResult",
              title: gettextCatalog.getString("Pac Result"),
              width: '10rem'
            },
            {
              field: "pde",
              title: gettextCatalog.getString("PDE"),
              width: '10rem'
            },
            {
              field: "pdm",
              title: gettextCatalog.getString("PDM"),
              width: '10rem'
            },
            {
              field: "priority",
              title: gettextCatalog.getString("Priority"),
              width: '10rem'
            },
            {
              field: "userPriority",
              title: gettextCatalog.getString("User Priority"),
              width: '10rem'
            },
            {
              field: "subFormat",
              title: gettextCatalog.getString("Sub Format"),
              width: '10rem'
            },
            {
              field: "messageFormat",
              title: gettextCatalog.getString("Message Format"),
              width: '10rem'
            }
          ]
        };


        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.subscriptionOrders.form.messageType = null;
          $scope.subscriptionOrders.form.creationDateStart = null;
          $scope.subscriptionOrders.form.creationDateEnd = null;
          $scope.subscriptionOrders.form.block4 = null;
          $scope.subscriptionOrders.form.in = 'ALL';
        };

        $scope.findSubscriptionOrders = function () {
          var formValidationResult = validateForm(angular.copy($scope.subscriptionOrders.form));
          if (formValidationResult.success) {
            if ($scope.subscriptionOrders.searchResultGrid) {
              $scope.subscriptionOrders.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.subscriptionOrders.searchResultGrid) {
            $scope.subscriptionOrders.searchResultGrid = widget;
          }
        });

      }]);
