'use strict';

angular.module('cmisApp')
  .controller('TitleTransferSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

        return result;
      };

      var titleTransferSelected = function (data) {
        var sResult = angular.copy($scope.titleTransfer.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.titleTransfer.selectedTitleTransferIndex = i;
            break;
          }
        }
      };

      $scope.titleTransferSelected = titleTransferSelected;

      var _params = {
        searchUrl: "/api/records/getTitleTransferRecords/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _titleTransfer = {
        formName: 'titleTransferFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedTitleTransferIndex: false,
        titleTransferSelected: titleTransferSelected,
        titleTransferDetails: false
      };

      $scope.titleTransfer = angular.merge($scope.searchConfig.titleTransfer, _titleTransfer);

      var findTitleTransfer = function (e) {

        //$scope.person.titleTransferDetails = false;

        if ($scope.titleTransfer.searchResultGrid !== undefined) {
          $scope.titleTransfer.searchResultGrid.refresh();
        }

        $scope.titleTransfer.selectedtitleTransferIndex = false;

        if ($scope.titleTransfer.formName.$dirty) {
          $scope.titleTransfer.formName.$submitted = true;
        }

        if ($scope.titleTransfer.formName.$valid || !$scope.titleTransfer.formName.$dirty) {

          $scope.titleTransfer.searchResult.isEmpty = false;

          var formData = angular.copy($scope.titleTransfer.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          formData.regDateStart = $scope.titleTransfer.form.regDateStart ? $scope.titleTransfer.form.regDateStartObj : null;
          formData.regDateFinish = $scope.titleTransfer.form.regDateFinish ? $scope.titleTransfer.form.regDateFinishObj : null;
          formData.settlementDateStart = $scope.titleTransfer.form.settlementDateStart ? $scope.titleTransfer.form.settlementDateStartObj : null;
          formData.settlementDateFinish = $scope.titleTransfer.form.settlementDateFinish ? $scope.titleTransfer.form.settlementDateFinishObj : null;
          formData.valueDateStart = $scope.titleTransfer.form.valueDateStart ? $scope.titleTransfer.form.valueDateStartObj : null;
          formData.valueDateFinish = $scope.titleTransfer.form.valueDateFinish ? $scope.titleTransfer.form.valueDateFinishObj : null;

          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.titleTransfer.searchResult.data = data;
              } else {
                $scope.titleTransfer.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };


      $scope.titleTransfer.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                regDate: {type: "date"},
                valueDate: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findTitleTransfer(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "number",
            title: gettextCatalog.getString("Registration Number"),
            width: '10rem'
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Registration Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "holder.name",
            title: gettextCatalog.getString("Transferee"),
            width: '10rem'
          },
          {
            field: "receiver.name",
            title: gettextCatalog.getString("Transferer"),
            width: '10rem'
          },
          {
            field: "holder.participantAccountNumber",
            title: "Mülkiyyət hüququnu ötürənin iştirakçı hesabı",
            width: '10rem'
          },
          {
            field: "receiver.participantAccountNumber",
            title: "Mülkiyyət hüququ ötürülənin iştirakçı hesabı",
            width: '10rem'
          },
          {
            field: "holder.pinCode",
            title: "Mülkiyyət hüququnu ötürənin pin kodu",
            width: '10rem'
          },
          {
            field: "receiver.pinCode",
            title: "Mülkiyyət hüququ ötürülənin pin kodu",
            width: '10rem'
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: '15rem'
          }
        ]
      };


      $scope.titleTransferSelected = titleTransferSelected;


      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {

        $scope.titleTransfer.form.destination = null;
        $scope.titleTransfer.form.referenceNumber = null;
        $scope.titleTransfer.form.regDateStart = null;
        $scope.titleTransfer.form.regDateFinish = null;
        $scope.titleTransfer.form.settlementDateStart = null;
        $scope.titleTransfer.form.settlementDateFinish = null;
        $scope.titleTransfer.form.valueDateStart = null;
        $scope.titleTransfer.form.valueDateFinish = null;
        $scope.titleTransfer.form.sellSideMemberName = null;
        $scope.titleTransfer.form.sellSideClientName = null;
        $scope.titleTransfer.form.buySideMemberName = null;
        $scope.titleTransfer.form.buySideClientName = null;
        $scope.titleTransfer.form.ISIN = null;
        $scope.titleTransfer.form.issuerName = null;

      };


      $scope.findTitleTransfer = function () {

        var formValidationResult = validateForm(angular.copy($scope.titleTransfer.form));
        if (formValidationResult.success) {
          if ($scope.titleTransfer.searchResultGrid) {
            $scope.titleTransfer.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.titleTransfer.searchResultGrid) {
          $scope.titleTransfer.searchResultGrid = widget;
        }
      });

      $scope.detailGrid = function (dataItem) {

        return {
          dataSource: {
            data: dataItem.subjects,
            pageSize: 5
          },
          scrollable: true,
          pageable: true,
          columns: [
            {
              field: "instrument.instrumentName",
              title: gettextCatalog.getString("Instrument Name"),
              width: "10rem"
            },
            {
              field: "instrument.ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "10rem"
            },
            {field: "instrument.issuerName", title: gettextCatalog.getString('Issuer'), width: "10rem"},
            {field: "quantity", title: gettextCatalog.getString('Quantity'), width: "10rem"}

          ]
        };
      };
    }]);
