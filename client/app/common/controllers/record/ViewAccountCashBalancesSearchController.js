'use strict';

angular.module('cmisApp')
  .controller('ViewAccountCashBalancesSearchController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        $scope.form = {}

        $scope.findData = function () {
          Loader.show(true);
          $scope.error = false;
          $http.post('/api/records/getAccountCashData/', {data: {accountNumber: $scope.form.accountNumber}}).then(function (response) {
            Loader.show(false);
            $scope.data = response.data.data;
            if($scope.data.personName){
              $scope.data.balances = $scope.data.balances instanceof Array ? $scope.data.balances : [$scope.data.balances]
            }else{
              $scope.error = true
            }

            console.log($scope.data);
          })

        };


      }]
  );
