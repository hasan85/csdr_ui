'use strict';

angular.module('cmisApp')
  .controller('IncomingPaymentRecordsSearchController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        var defaultParams = {
          searchUrl: "/api/persons/getUnderwriters/",
          showSearchCriteriaBlock: false,
          config: {
            searchOnInit: false
          }
        };

        $scope.params = angular.merge(defaultParams, $scope.searchConfig.params);


        console.log($scope.params)

        var validateForm = function (formData) {

          var result = {
            success: false,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };
          // if (formData.clientName || formData.brokerName || formData.finishDateStart || formData.finishDateFinish) {
          result.success = true;
          //}
          return result;
        };


        var findData = function (e) {


          if ($scope.search.searchResultGrid !== undefined) {
            $scope.search.searchResultGrid.refresh();
          }
          if ($scope.search.formName.$dirty) {
            $scope.search.formName.$submitted = true;
          }

          if ($scope.search.formName.$valid || !$scope.search.formName.$dirty) {

            $scope.search.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.search.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {

              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }


              formData.paymentDateStart = $scope.search.form.paymentDateStart ? $scope.search.form.paymentDateStartObj : null;
              formData.paymentDateFinish = $scope.search.form.paymentDateFinish ? $scope.search.form.paymentDateFinishObj : null;


              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take,
                sort: e.data.sort
              });



              console.log(requestData)
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).success(function (data) {

                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };




        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];


        $scope.search = angular.merge(
          $scope.searchConfig.accountPrescription,
          {
            formName: 'search',
            form: {},
            data: {},

            searchResult: {
              data: null,
              isEmpty: false
            }
          }
        );


        $scope.findData = function () {

          var formValidationResult = validateForm(angular.copy($scope.search.form));

          if (formValidationResult.success) {
            if ($scope.search.searchResultGrid) {
              $scope.search.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };


        $scope.toolTipOptions = {
          filter: "td:nth-child(9)",
          position: "top",
          content: function(e) {
            var grid = e.target.closest(".k-grid").getKendoGrid();
            var dataItem = grid.dataItem(e.target.closest("tr"));
            return dataItem.destination;

          },
          show: function(e) {
            this.popup.element[0].style.width = "300px";
          }
        }



        $scope.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  paymentDate: {type: "date"},
                  creationDate: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },
            sort: {field: "creationDate", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "creationDate",
              title: gettextCatalog.getString("Creation Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "refNumber",
              title: gettextCatalog.getString("Reference Number"),
              width: "10rem"
            },
            {
              field: "paymentDate",
              title: gettextCatalog.getString("Payment Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "currency.code",
              title: gettextCatalog.getString("Currency"),
              width: "5rem"
            },
            {
              field: "paidAmount",
              title: gettextCatalog.getString("Amount"),
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(paidAmount, 'n2') # </div>"
            },

            {
              field: "beneficiarAccount",
              template:  "#= data.payeeClient ? payeeClient.name : ''  #",
              title: "Benefisiarın adı",
              width: "15rem"
            },
            {
              field: "beneficiarAccount",
              template:  "#= data.payeeClient ? payeeClient.accountNumber  : ''  #",
              title: "Benefisiarın hesabı",
              width: "15rem"
            },
            {
              field: "serviceClass",
              template:  "#= data.serviceClass ? serviceClass.nameAz: ''  #",
              title: "Xidmət sinfi",
              width: "10rem"
            },
            {
              field: "destination",
              title: "Təyinat",
              width: "10rem"
            },
            {
              field: "status.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Status"),
              width: "6rem"
            },

            {
              field: "statusMessage",
              title: "Status mesajı",
              width: "20rem"
            },
            {
              field: "paymentSource.nameAz",
              template:  "#= data.paymentSource ? data.paymentSource.nameAz: ''  #",
              title: "Ödəniş mənbəyi",
              width: "10rem"
            },
            {
              field: "payerClientAccountNumber",
              title: "Ödəyici tərəfin hesab nömrəsi",
              width: "15rem"
            },

            {
              field: "payerClientName",
              title: "Ödəyici tərəfin adı",
              width: "10rem"
            },
            {
              field: "payerClientTin",
              title: "Ödəyici tərəfin VÖEN-i",
              width: "10rem"
            },
            {
              field: "payerBankTin",
              title: "Ödəyici bankın VÖEN-i",
              width: "10rem"
            },
            {
              field: "payerBankCode",
              title: "Ödəyici bankın kodu",
              width: "10rem"
            },
            {
              field: "payerBankBIC",
              title: "Ödəyici bankın BİC-i",
              width: "10rem"
            },
            {
              field: "payerBankTin",
              title: "Ödəyici bankın BİC-i",
              width: "10rem"
            },
            {
              field: "payerBankCorrespondentAccount",
              title: "Ödəyici bankın müxbir hesabı",
              width: "10rem"
            },
            {
              field: "payeeClientAccount",
              title: "Benefisiarın hesab nömrəsi",
              width: "10rem"
            }

          ]
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.search.form.paymentDateStart = null;
          $scope.search.form.paymentDateFinish = null;
          $scope.search.form.status = null;
          $scope.search.form.referenceNumber = null;
          $scope.search.form.currencyCode = null;
          $scope.search.form.payeeClientAccountNumber = null;
          $scope.search.form.payeeClientAccountName = null;
          $scope.search.form.serviceClass = null;
        };

        $scope.resetForm()

      }]
  );
