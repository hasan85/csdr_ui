'use strict';

angular.module('cmisApp')
  .controller('CorporateActionSearchTemplateController', ['$rootScope', '$scope', 'corporateActionFindService', 'Loader', 'referenceDataService', 'gettextCatalog', 'appConstants', 'SweetAlert','helperFunctionsService',
    function ($rootScope, $scope, corporateActionFindService, Loader, referenceDataService, gettextCatalog, appConstants, SweetAlert, helperFunctionsService) {

      $scope.labels = {
        lblUnderlyingSecurities: gettextCatalog.getString('Underlying Securities'),
        lblEntitlementSecurities: gettextCatalog.getString('Entitlement Securities')
      };

      $scope.gridColumns = [
        {
          field: "corporateActionClass.name" + $rootScope.lnC,
          title: gettextCatalog.getString("CA Class"),
          width: '34%'
        },
        {
          field: "registrationNumber",
          title: gettextCatalog.getString("Registration Number"),
          width: '33%'
        },
        {
          field: "registrationDate",
          title: gettextCatalog.getString("Registration Date"),
          width: '33%',
          format: "{0:dd-MMMM-yyyy}"
        }
      ];

      var isInit = true;
      var findCorporateAction = function () {

        $scope.corporateAction.corporateActionDetails = false;

        if (!isInit) {
          //  $scope.params.showSearchCriteriaBlock = false;
        }
        isInit = false;

        if ($scope.corporateAction.searchResultGrid !== undefined) {
          $scope.corporateAction.searchResultGrid.refresh();
        }

        $scope.corporateAction.selectedCorporateActionIndex = false;

        if ($scope.corporateAction.formName.$dirty) {
          $scope.corporateAction.formName.$submitted = true;
        }

        if ($scope.corporateAction.formName.$valid || !$scope.corporateAction.formName.$dirty) {

          $scope.corporateAction.searchResult.isEmpty = false;

          Loader.show(true);


          var searchCriteria = angular.copy($scope.corporateAction.form);

          if (searchCriteria && searchCriteria.registrationDate) {
            searchCriteria.registrationDate = searchCriteria.registrationDateObj;
          }

          corporateActionFindService.findCorporateActionByData($scope.params.searchUrl, searchCriteria).then(function (data) {
            data = {
              'data': data, pageSize: 20, schema: {
                model: {
                  fields: {
                    registrationDate: {type: "date"}
                  }
                }
              }
            };
            $scope.gridData = data;
            if (data) {
              $scope.corporateAction.searchResult.data = data;
            } else {
              $scope.corporateAction.searchResult.isEmpty = true;
            }
            Loader.show(false);
          });
        }
      };

      if ($scope.config) {
        $scope.config.buttons.refresh.click = function () {
          findCorporateAction();
        };
      }


      var corporateActionSelected = function (data) {


        var sResult = angular.copy($scope.corporateAction.searchResult.data.data);

        for (var i = 0; i < sResult.length; i++) {

          if (sResult[i].id == data.id) {

            $scope.corporateAction.selectedCorporateActionIndex = i;
            break;
          }
        }

      };

      $scope.corporateActionSelected = corporateActionSelected;

      var _corporateAction = {
        formName: 'corporateActionFindForm',
        form: {},
        data: {
          corporateActionClasses: []
        },
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedCorporateActionIndex: false,
        findCorporateAction: findCorporateAction,
        corporateActionSelected: corporateActionSelected,
        corporateActionDetails: false
      };

      var _params = {
        searchUrl: "/api/corporateActions/getCorporateActions",
        showSearchCriteriaBlock: false,
        formConfig: {
          caClass: {isVisible: true},
          caCategory: {isVisible: true}
        },
        config: {}
      };

      $scope.corporateAction = angular.merge($scope.searchConfig.corporateAction, _corporateAction);
      $scope.params = angular.merge(_params, $scope.searchConfig.params);
      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];
      Loader.show(true);

      referenceDataService.getCorporateActionCategories().then(function (data) {

        $scope.corporateAction.data.corporateActionCategories = data;
        // findCorporateAction();
        Loader.show(false);
      });

      $scope.findCorporateAction = findCorporateAction;

      $scope.corporateActionSelected = corporateActionSelected;

      $scope.$on('refreshGrid', function () {
        findCorporateAction();
      });

      // Get corporate action classes by category
      $scope.changeCorporateActionCategory = function () {

        var catId = parseInt($scope.corporateAction.form.corporateActionCategoryId);

        if (catId > 0) {

          Loader.show(true);

          referenceDataService.getCorporateActionClassesByCategory(catId).then(function (data) {
            $scope.corporateAction.data.corporateActionClasses = data;
            Loader.show(false);
          });
        }

      };

      $scope.detailInit = function (e) {
        Loader.show(true);
        corporateActionFindService.getCorporateActionById(e.data.id).then(function (res) {
          Loader.show(false);
          if (res && res.success === "true") {
            if (res.data) {
              e.data.underlyingSecurity = {data: res.data.underlyingSecurity, pageSize: 10}
              e.data.entitlementSecurity = {data: res.data.entitlementSecurity, pageSize: 10}
            }

          } else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
          }
        });
      };

      $scope.underlyingSecuritiesGrid = function (dataItem) {

        return {
          scrollable: false,
          pageable: true,
          columns: [
            {
              field: "name",
              title: gettextCatalog.getString("Instrument"),
              width: "33%",
              template: function (e) {
                return e.name ? e.name.nameAz : '';
              }
            },
            {
              field: "ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "33%"
            },
            {
              field: "issuer.name",
              title: gettextCatalog.getString('Issuer'),
              width: "33%"
            },
          ]
        };
      };

      $scope.entitlementSecuritiesGrid = function (dataItem) {

        return {
          scrollable: false,
          pageable: true,
          columns: [
            {
              field: "name",
              title: gettextCatalog.getString("Instrument"),
              width: "33%",
              template: function (e) {
                return e.name ? e.name.nameAz : '';
              }
            },
            {
              field: "ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "50%"
            },
            {
              field: "issuer.name",
              title: gettextCatalog.getString('Issuer'),
              width: "50%"
            },

          ]
        };
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };
      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };
      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.resetForm = function () {
        $scope.corporateAction.form = {
          corporateActionClassId: "",
          registrationNumber: null,
          registrationDate: null,
          ISIN: null,
        }
      };
      $scope._closeWindow = function () {
        $windowInstance.close();
      };

      findCorporateAction();
    }]);
