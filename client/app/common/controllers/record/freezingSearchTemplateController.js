'use strict';

angular.module('cmisApp')
  .controller('FreezingSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        result.success = true;

        return result;
      };

      var freezingSelected = function (data) {
        var sResult = angular.copy($scope.freezing.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.freezing.selectedFreezingIndex = i;
            break;
          }
        }
      };


      $scope.freezingSelected = freezingSelected;

      var _params = {
        searchUrl: "/api/records/getFreezingRecords/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _freezing = {
        formName: 'freezingFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedFreezingIndex: false,
        freezingSelected: freezingSelected,
        freezingDetails: false
      };

      $scope.freezing = angular.merge($scope.searchConfig.freezing, _freezing);

      var findFreezing = function (e) {

        //$scope.person.freezingDetails = false;

        if ($scope.freezing.searchResultGrid !== undefined) {
          $scope.freezing.searchResultGrid.refresh();
        }

        $scope.freezing.selectedFreezingIndex = false;

        if ($scope.freezing.formName.$dirty) {
          $scope.freezing.formName.$submitted = true;
        }

        if ($scope.freezing.formName.$valid || !$scope.freezing.formName.$dirty) {

          $scope.freezing.searchResult.isEmpty = false;

          var formData = angular.copy($scope.freezing.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          formData.regDateStart = $scope.freezing.form.regDateStart ? $scope.freezing.form.regDateStartObj : null;
          formData.regDateFinish = $scope.freezing.form.regDateFinish ? $scope.freezing.form.regDateFinishObj : null;
          formData.settlementDateStart = $scope.freezing.form.settlementDateStart ? $scope.freezing.form.settlementDateStartObj : null;
          formData.settlementDateFinish = $scope.freezing.form.settlementDateFinish ? $scope.freezing.form.settlementDateFinishObj : null;
          formData.valueDateStart = $scope.freezing.form.valueDateStart ? $scope.freezing.form.valueDateStartObj : null;
          formData.valueDateFinish = $scope.freezing.form.valueDateFinish ? $scope.freezing.form.valueDateFinishObj : null;

          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.freezing.searchResult.data = data;
              } else {
                $scope.freezing.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };


      $scope.freezing.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                regDate: {type: "date"},
                valueDate: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findFreezing(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "freezingClass.name" + $rootScope.lnC,
            title: gettextCatalog.getString('Record Type'),
            width: '10rem'
          },
          {
            field: "orderNumber",
            title: gettextCatalog.getString("Order Number"),
            width: '10rem'
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Registration Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "holder.name",
            title: gettextCatalog.getString("Holder"),
            width: '10rem'
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: '15rem'
          }
        ]
      };


      $scope.freezingSelected = freezingSelected;


      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {

        $scope.freezing.form.destination = null;
        $scope.freezing.form.referenceNumber = null;
        $scope.freezing.form.regDateStart = null;
        $scope.freezing.form.regDateFinish = null;
        $scope.freezing.form.settlementDateStart = null;
        $scope.freezing.form.settlementDateFinish = null;
        $scope.freezing.form.sellSideMemberName = null;
        $scope.freezing.form.sellSideClientName = null;
        $scope.freezing.form.buySideMemberName = null;
        $scope.freezing.form.buySideClientName = null;
        $scope.freezing.form.ISIN = null;
        $scope.freezing.form.issuerName = null;

      };


      $scope.findFreezing = function () {

        var formValidationResult = validateForm(angular.copy($scope.freezing.form));
        if (formValidationResult.success) {
          if ($scope.freezing.searchResultGrid) {
            $scope.freezing.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.freezing.searchResultGrid) {
          $scope.freezing.searchResultGrid = widget;
        }
      });

      $scope.detailGrid = function (dataItem) {

        return {
          dataSource: {
            data: dataItem.subjects,
            pageSize: 5
          },
          scrollable: true,
          pageable: true,
          columns: [
            {
              field: "instrument.instrumentName",
              title: gettextCatalog.getString("Instrument Name"),
              width: "10rem"
            },
            {
              field: "instrument.ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "10rem"
            },
            {field: "instrument.issuerName", title: gettextCatalog.getString('Issuer'), width: "10rem"},
            {field: "quantity", title: gettextCatalog.getString('Quantity'), width: "10rem"}

          ]
        };
      };
    }]);
