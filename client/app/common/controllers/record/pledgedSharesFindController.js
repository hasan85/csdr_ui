'use strict';

angular.module('cmisApp')
  .controller('PledgedSharesFindController', ['$scope', '$windowInstance', 'recordsService', 'searchCriteria',
    function ($scope, $windowInstance, recordsService, searchCriteria) {

      $scope.searchConfig = {
        pledgedShares: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPledgedSharesIndex: false
        },
        params: {
                 showSearchCriteriaBlock: true,
          config: {
            searchOnInit: true,
            searchCriteria:searchCriteria
          }
        }
      };

      $scope.selectPledgedShares = function () {
        recordsService.selectPledgedShares($scope.searchConfig.pledgedShares.searchResult.data.data[$scope.searchConfig.pledgedShares.selectedPledgedSharesIndex]);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
