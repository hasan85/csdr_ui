'use strict';

angular.module('cmisApp')
  .controller('ViewOutgoingPaymentRecordsSearchController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        var defaultParams = {
          searchUrl: "/api/persons/getUnderwriters/",
          showSearchCriteriaBlock: false,
          config: {
            searchOnInit: false
          }
        };


        $scope.status = [
          {code: 'STATUS_PENDING', name:'Gözləmədədir'},
          {code: 'STATUS_ERROR', name:'Xəta'},
          {code: 'STATUS_SENT', name: 'Göndərilmiş'},
          {code: 'STATUS_SUCCESS', name: 'Uğurla Tamamlandı'},
          {code: 'STATUS_OPERATION_APPROVAL_PENDING', name: 'Əməliyyatın Təsdiqlənməsi Gözləmədədir'},
          {code: 'STATUS_CHAIRMAN_APPROVAL_PENDING', name: 'Sədrin Təsdiqləməsi Gözləmədədir'},
          {code: 'STATUS_OPERATION_DEPARTMENT_REJECTED', name: 'Əməliyyat departamenti imtina edib'},
          {code: 'STATUS_CHAIRMAN_REJECTED', name:'Sədr İmtina Edib'}
        ]

        $scope.params = angular.merge(defaultParams, $scope.searchConfig.params);

        var validateForm = function (formData) {

          var result = {
            success: true,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };

          return result;
        };


        var findData = function (e) {

          console.log($scope.search.form)

          if ($scope.search.searchResultGrid !== undefined) {
            $scope.search.searchResultGrid.refresh();
          }
          if ($scope.search.formName.$dirty) {
            $scope.search.formName.$submitted = true;
          }

          if ($scope.search.formName.$valid || !$scope.search.formName.$dirty) {

            $scope.search.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.search.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {

              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }

              //
              formData.withdrawalDateStart = $scope.search.form.withdrawalDateStart ? $scope.search.form.withdrawalDateStartObj : null;
              formData.withdrawalDateFinish = $scope.search.form.withdrawalDateFinish ? $scope.search.form.withdrawalDateFinishObj : null;
              formData.creationDateStart = $scope.search.form.creationDateStart ? $scope.search.form.creationDateStartObj : null;
              formData.creationDateFinish = $scope.search.form.creationDateFinish ? $scope.search.form.creationDateFinishObj : null;

              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take,
                sort: e.data.sort
              });


              console.log(requestData)
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).success(function (data) {

                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };


        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];


        $scope.search = angular.merge(
          $scope.searchConfig.accountPrescription,
          {
            formName: 'search',
            form: {},
            data: {},

            searchResult: {
              data: null,
              isEmpty: false
            }
          }
        );


        $scope.findData = function () {

          var formValidationResult = validateForm(angular.copy($scope.search.form));

          if (formValidationResult.success) {
            if ($scope.search.searchResultGrid) {
              $scope.search.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };


        $scope.toolTipOptions = {
          filter: "td:nth-child(15)",
          position: "top",
          content: function(e) {
            var grid = e.target.closest(".k-grid").getKendoGrid();
            var dataItem = grid.dataItem(e.target.closest("tr"));
            return dataItem.destination;

          },
          show: function(e) {
            this.popup.element[0].style.width = "300px";
          }
        }


        $scope.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  withdrawalDate: {type: "date"},
                  creationDate: {type: "date"},
                  sendDate: {type: "date"},
                  amount: {type: "number"},
                  trdWithdrawalAmount: {type: "number"},
                  subWithdrawalAmount: {type: "number"},
                  frxWithdrawalAmount: {type: "number"},
                  disWithdrawalAmount: {type: "number"},
                  unkWithdrawalAmount: {type: "number"},
                  colWithdrawalAmount: {type: "number"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },
            sort: {field: "creationDate", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "payeeClientName",
              title: 'Müştərinin adı',
              width: "10rem"
            },
            {
              field: "account.accountNumber",
              template:  "#= data.account ? data.account.accountNumber: ''  #",
              title: gettextCatalog.getString("Account Number"),
              width: "10rem"
            },
            {
              field: "memberAccount.name",
              title: 'Üzvün adı',
              width: "10rem"
            },
            {
              field: "currency.code",
              title: gettextCatalog.getString("Currency"),
              width: "5rem"
            },
            {
              field: "amount",
              title: 'Çıxarış məbləği',
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(amount, 'n2') # </div>"
            },

            {
              field: "withdrawalClass.nameAz",
              title: 'Növ',
              width: "6rem"
            },
            {
              field: "trdWithdrawalAmount",
              title: 'Ticarət',
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(trdWithdrawalAmount, 'n2') # </div>"
            },

            {
              field: "subWithdrawalAmount",
              title: 'Abunə yazılışı',
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(subWithdrawalAmount, 'n2') # </div>"
            },

            {
              field: "frxWithdrawalAmount",
              title: 'FOREX',
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(frxWithdrawalAmount, 'n2') # </div>"
            },

            {
              field: "disWithdrawalAmount",
              title: 'Xidmət',
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(disWithdrawalAmount, 'n2') # </div>"
            },

            {
              field: "unkWithdrawalAmount",
              title: 'Naməlum',
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(unkWithdrawalAmount, 'n2') # </div>"
            },

            {
              field: "colWithdrawalAmount",
              title: 'Pul zəmanəti',
              width: "6rem",
              format: "{0:n2}",
              template: "<div style='text-align: right'>#= kendo.toString(colWithdrawalAmount, 'n2') # </div>"
            },
            {
              field: "payeeBankName",
              title: 'Bank',
              width: "10rem"
            },
            {
              field: "payeeClientAccountNumber",
              title: 'IBAN',
              width: "10rem"
            },
            {
              field: "payeeBankCorrespondentAccount",
              title: 'Müxbir hesabı',
              width: "10rem"
            },
            {
              field: "sessionNumber",
              title: 'Seans nömrəsi',
              width: "10rem"
            },
            {
              field: "destination",
              title: "Təyinat",
              width: "10rem"
            },
            {
              field: "refNumber",
              title: "İstinad Nömrəsi",
              width: "10rem"
            },
            {
              field: "status.code",
              title: "Status",
              template:  "#= data.status.nameAz  #",
              width: "10rem"
            },
            {
              field: "creationDate",
              title: gettextCatalog.getString("Creation Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "sendDate",
              title: "Göndərilmə vaxtı",
              width: "10rem",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "withdrawalDate",
              title: 'Çıxarış tarixi',
              width: "10rem",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "statusMessage",
              title: "Status mesajı",
              width: "10rem"
            },
            {
              field: "azipsActualCommission",
              title: "AZIPS faktiki komissiya",
              width: "10rem"
            },
            {
              field: "azipsSessionCommission",
              title: "Sessiya üçün Komissiya",
              width: "10rem"
            },
            {
              field: "azipsSessionCommissionVAT",
              title: "Sessiya üçün Komissiya ƏDV",
              width: "10rem"
            }

          ]
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.search.form.paymentDateStart = null;
          $scope.search.form.paymentDateFinish = null;
          $scope.search.form.status = null;
          $scope.search.form.referenceNumber = null;
          $scope.search.form.currencyCode = null;
          $scope.search.form.payeeClientAccountNumber = null;
          $scope.search.form.payeeClientAccountName = null;
          $scope.search.form.serviceClass = null;
        };

      }]
  );
