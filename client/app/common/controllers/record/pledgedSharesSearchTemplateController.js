'use strict';

angular.module('cmisApp')
  .controller('PledgedSharesSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        result.success = true;

        return result;
      };

      var pledgedSharesSelected = function (data) {
        var sResult = angular.copy($scope.pledgedShares.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].id == data.id) {
            $scope.pledgedShares.selectedPledgedSharesIndex = i;
            break;
          }
        }
      };


      $scope.pledgedSharesSelected = pledgedSharesSelected;

      var _params = {
        searchUrl: "/api/records/getPledgedBalances/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _pledgedShares = {
        formName: 'pledgedSharesFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedPledgedSharesIndex: false,
        pledgedSharesSelected: pledgedSharesSelected,
        pledgeDetails: false
      };

      console.log($scope.searchConfig.pledgedShares);

      $scope.pledgedShares = angular.merge( _pledgedShares, $scope.searchConfig.pledgedShares);



      var findPledgedShares = function (e) {

        //$scope.person.pledgeDetails = false;

        if ($scope.pledgedShares.searchResultGrid !== undefined) {
          $scope.pledgedShares.searchResultGrid.refresh();
        }

        $scope.pledgedShares.selectedPledgedSharesIndex = false;

        if ($scope.pledgedShares.formName.$dirty) {
          $scope.pledgedShares.formName.$submitted = true;
        }

        if ($scope.pledgedShares.formName.$valid || !$scope.pledgedShares.formName.$dirty) {

          $scope.pledgedShares.searchResult.isEmpty = false;

          var formData = angular.copy($scope.pledgedShares.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.pledgedShares.searchResult.data = data;
              } else {
                $scope.pledgedShares.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };


      $scope.pledgedShares.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total"
          },
          transport: {
            read: function (e) {
              findPledgedShares(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        //filterable:true,
        resizable: true,
        columns: [

          {
            field: "pledgor.name",
            title: gettextCatalog.getString('Pledgor'),
            width: '10rem'
          },
          {
            field: "pledgor.accountNumber",
            title: gettextCatalog.getString("Account"),
            width: '10rem'
          },

          {
            field: "pledgee.name",
            title: gettextCatalog.getString("Pledgee"),
            width: '10rem'
          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("ISIN"),
            width: "10rem"
          },
          {
            field: "instrument.issuerName",
            title: gettextCatalog.getString('Issuer'),
            width: "10rem"},
          {
            field: "quantity",
            title: gettextCatalog.getString('Quantity'),
            width: "7rem"},
          {
            field: "comment",
            title: gettextCatalog.getString('Comment'),
            width: "10rem"}
        ]
      };


      $scope.pledgedSharesSelected = pledgedSharesSelected;


      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.pledgedShares.form.pledgorName = null;
        $scope.pledgedShares.form.pledgorAccountNumber = null;
        $scope.pledgedShares.form.ISIN = null;
        $scope.pledgedShares.form.issuerName = null;
      };

      $scope.findPledgedShares = function () {

        var formValidationResult = validateForm(angular.copy($scope.pledgedShares.form));
        if (formValidationResult.success) {
          if ($scope.pledgedShares.searchResultGrid) {
            $scope.pledgedShares.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.pledgedShares.searchResultGrid) {
          $scope.pledgedShares.searchResultGrid = widget;
        }
      });

    }]);
