'use strict';

angular.module('cmisApp')
  .controller('SubscriptionOrdersSearchTemplateController',
    ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
      '$http', 'helperFunctionsService', 'recordsService', '$rootScope', "$filter",
      function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
                $http, helperFunctionsService, recordsService, $rootScope, $filter) {

        var validateForm = function (formData) {
          var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

          return result;
        };

        var subscriptionOrdersSelected = function (data) {
          var sResult = angular.copy($scope.subscriptionOrders.searchResult.data.data);
          for (var i = 0; i < sResult.length; i++) {
            if (sResult[i].ID == data.ID) {
              $scope.subscriptionOrders.selectedSubscriptionOrdersIndex = i;
              break;
            }
          }
        };

        $scope.subscriptionOrdersSelected = subscriptionOrdersSelected;

        var _params = {
          searchUrl: "/api/records/getSubscriptionInstruments/",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {},
            searchOnInit: true
          },
          criteriaEnabled: true
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _subscriptionOrders = {
          formName: 'subscriptionOrdersFindForm',
          form: {},
          data: {},

          searchResult: {
            data: null,
            isEmpty: false
          },

          selectedSubscriptionOrdersIndex: false,
          subscriptionOrdersSelected: subscriptionOrdersSelected,
          subscriptionOrdersDetails: false
        };

        $scope.subscriptionOrders = angular.merge($scope.searchConfig.subscriptionOrders, _subscriptionOrders);

        var findSubscriptionOrders = function (e) {

          //$scope.person.subscriptionOrdersDetails = false;

          if ($scope.subscriptionOrders.searchResultGrid !== undefined) {
            $scope.subscriptionOrders.searchResultGrid.refresh();
          }

          $scope.subscriptionOrders.selectedSubscriptionOrdersIndex = false;

          if ($scope.subscriptionOrders.formName.$dirty) {
            $scope.subscriptionOrders.formName.$submitted = true;
          }

          if ($scope.subscriptionOrders.formName.$valid || !$scope.subscriptionOrders.formName.$dirty) {

            $scope.subscriptionOrders.searchResult.isEmpty = false;

            var formData = angular.copy($scope.subscriptionOrders.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            //console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data) {
                $scope.subscriptionOrders.searchResult.data = data;
              } else {
                $scope.subscriptionOrders.searchResult.isEmpty = true;

              }

              //console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };

        $scope.subscriptionOrders.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  startDate: {type: "date"},
                  endDate: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findSubscriptionOrders(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "instrumentName",
              title: gettextCatalog.getString('Instrument Name'),
              width: "10rem"
            },
            {
              field: "ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "10rem"
            },
            {
              field: "issuerName",
              title: gettextCatalog.getString('Issuer Name'),
              width: "10rem"
            },

            {
              field: "startDate",
              title: gettextCatalog.getString("Subscription Start Date"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "endDate",
              title: gettextCatalog.getString("Subscription End Date"),
              width: '10rem',
              template: function (e) {
                return kendo.toString(e.endDate, 'dd-MMMM-yyyy');
              }
            }
          ]
        };

        $scope.subscriptionOrdersSelected = subscriptionOrdersSelected;

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.subscriptionOrders.form.buySideMemberName = null;
          $scope.subscriptionOrders.form.buySideClientName = null;
          $scope.subscriptionOrders.form.buySideClientAccountNumber = null;
          $scope.subscriptionOrders.form.regDateStart = null;
          $scope.subscriptionOrders.form.regDateFinish = null;
          $scope.subscriptionOrders.form.statusCode = null;
        };

        $scope.findSubscriptionOrders = function () {
          var formValidationResult = validateForm(angular.copy($scope.subscriptionOrders.form));
          if (formValidationResult.success) {
            if ($scope.subscriptionOrders.searchResultGrid) {
              $scope.subscriptionOrders.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.subscriptionOrders.searchResultGrid) {
            $scope.subscriptionOrders.searchResultGrid = widget;
          }
        });


        $scope.detailInit = function (dataItem) {
          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/records/getSubscriptionOrdersBySubscriptionID/',
            data: {
              instrumentID: dataItem.data.id
            }
          }).success(function (data) {
            Loader.show(false);
            if (data['success'] === "true") {
              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data.data) {
                for (var i = 0; i < data.data.length; i++) {
                  data.data[i].rejectionActionClasses = helperFunctionsService.convertObjectToArray(data.data[i].rejectionActionClasses);
                }
              }
              dataItem.data.subscriptionOrdersList = {
                data: data.data,
                pageSize: 10,
                schema: {
                  model: {
                    fields: {
                      subscriptionDate: {type: "date"}
                    }
                  }
                }
              };
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
          });
        };

        $scope.detailGrid = function () {
          return {
            scrollable: true,
            pageable: true,
            sortable:true,
            filterable:true,
            columns: [
              {
                field: "memberName",
                title: gettextCatalog.getString("Member Name"),
                width: "10rem"
              },
              {
                field: "accountNumber",
                title: gettextCatalog.getString("Account Number"),
                width: "10rem"
              },
              {
                field: "clientName",
                title: gettextCatalog.getString("Client Name"),
                width: "10rem"

              },
              {
                field: "quantity",
                title: gettextCatalog.getString("Quantity"),
                width: "10rem",
                template: function(dataItem) {
                  return dataItem.quantity ? $filter("formatNumber")(dataItem.quantity, false) : "";
                }
              },
              {
                field: "status.name" + $rootScope.lnC,
                title: gettextCatalog.getString("Status Code"),
                width: "10rem"

              },
              {
                field: "subscriptionDate",
                title: gettextCatalog.getString("Subscription Date"),
                type: "date",
                format: "{0:dd-MMMM-yyyy HH:mm:ss}",
                width: "12rem"
              }

            ]
          };
        };

      }]);
