'use strict';

angular.module('cmisApp')
  .controller('CorporateActionFindController', ['$scope', '$windowInstance', 'corporateActionFindService', 'searchUrl', 'formConfig',
    function ($scope, $windowInstance, corporateActionFindService, searchUrl, formConfig) {

      $scope.searchConfig = {
        corporateAction: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedCorporateActionIndex: false
        },
        params: {
          searchUrl: searchUrl ? searchUrl : '/api/corporateActions/getShareHolders',
          showSearchCriteriaBlock: true,
          formConfig:formConfig
        }
      };
      $scope.selectCorporateAction = function () {
        corporateActionFindService.selectCorporateAction($scope.searchConfig.corporateAction.searchResult.data.data[$scope.searchConfig.corporateAction.selectedCorporateActionIndex]);
        $windowInstance.close();
      };
      $scope.closeWindow = function () {
        $windowInstance.close();
      };

    }]);
