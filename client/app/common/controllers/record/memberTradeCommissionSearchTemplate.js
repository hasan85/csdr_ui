'use strict';

angular.module('cmisApp')
  .controller('MemberTradeCommissionSearchTemplateController',
  ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
    function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {


      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        result.success = true;

        return result;
      };

      var findData = function (e) {

        if ($scope.memberTradeCommission.searchResultGrid !== undefined) {
          $scope.memberTradeCommission.searchResultGrid.refresh();
        }
        if ($scope.memberTradeCommission.formName.$dirty) {
          $scope.memberTradeCommission.formName.$submitted = true;
        }

        if ($scope.memberTradeCommission.formName.$valid || !$scope.memberTradeCommission.formName.$dirty) {

          $scope.memberTradeCommission.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.memberTradeCommission.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }


            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            var lastDayWithSlashes = (lastDay.getDate()) + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getFullYear();
            var firstDayWithSlashes = (firstDay.getDate()) + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getFullYear();

            if (!$scope.memberTradeCommission.form.startObj) {
              $scope.memberTradeCommission.form.startObj = firstDay;
              $scope.memberTradeCommission.form.start = firstDayWithSlashes;
            }
            if (!$scope.memberTradeCommission.form.finishObj) {
              $scope.memberTradeCommission.form.finishObj = lastDay;
              $scope.memberTradeCommission.form.finish = lastDayWithSlashes;
            }


            formData.start = $scope.memberTradeCommission.form.startObj;
            formData.finish = $scope.memberTradeCommission.form.finishObj;

            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: requestData}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.memberTradeCommission.searchResult.data = data;
                } else {
                  $scope.memberTradeCommission.searchResult.isEmpty = true;
                }

                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


          } else {
            Loader.show(false);
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };

      var _params = {
        searchUrl: "/api/records/getMarketTradeCommission/",
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _memberTradeCommission = {
        formName: 'memberTradeCommissionFindForm',
        form: {

        },
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedMemberTradeCommissionIndex: false,
        findData: findData
      };

      $scope.memberTradeCommission = angular.merge($scope.searchConfig.memberTradeCommission, _memberTradeCommission);

      $scope.findData = function () {

        var formValidationResult = validateForm(angular.copy($scope.memberTradeCommission.form));
        if (formValidationResult.success) {
          if ($scope.memberTradeCommission.searchResultGrid) {
            $scope.memberTradeCommission.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      $scope.memberTradeCommission.mainGridOptions = {
        excel: {
          allPages: true
        },
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                start: {type: "date"},
                finish: {type: "date"},
              }
            }
          },
          transport: {
            read: function (e) {
              findData(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "member.name",
            title: gettextCatalog.getString("Member"),
            width: "10rem"
          },
          {
            field: "commission",
            title: gettextCatalog.getString("Commission"),
            width: "10rem"
          }
        ]
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.memberTradeCommission.form.start = null;
        $scope.memberTradeCommission.form.finish = null;

      };
    }]);
