'use strict';

angular.module('cmisApp')
  .controller('GrantSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        result.success = true;

        return result;
      };

      var grantSelected = function (data) {
        var sResult = angular.copy($scope.grant.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.grant.selectedGrantIndex = i;
            break;
          }
        }
      };


      $scope.grantSelected = grantSelected;

      var _params = {
        searchUrl: "/api/records/getGrantRecords/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _grant = {
        formName: 'grantFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedGrantIndex: false,
        grantSelected: grantSelected,
        grantDetails: false
      };

      $scope.grant = angular.merge($scope.searchConfig.grant, _grant);

      var findGrant = function (e) {

        //$scope.person.grantDetails = false;

        if ($scope.grant.searchResultGrid !== undefined) {
          $scope.grant.searchResultGrid.refresh();
        }

        $scope.grant.selectedgrantIndex = false;

        if ($scope.grant.formName.$dirty) {
          $scope.grant.formName.$submitted = true;
        }

        if ($scope.grant.formName.$valid || !$scope.grant.formName.$dirty) {

          $scope.grant.searchResult.isEmpty = false;

          var formData = angular.copy($scope.grant.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          formData.regDateStart = $scope.grant.form.regDateStart ? $scope.grant.form.regDateStartObj : null;
          formData.regDateFinish = $scope.grant.form.regDateFinish ? $scope.grant.form.regDateFinishObj : null;
          formData.settlementDateStart = $scope.grant.form.settlementDateStart ? $scope.grant.form.settlementDateStartObj : null;
          formData.settlementDateFinish = $scope.grant.form.settlementDateFinish ? $scope.grant.form.settlementDateFinishObj : null;
          formData.valueDateStart = $scope.grant.form.valueDateStart ? $scope.grant.form.valueDateStartObj : null;
          formData.valueDateFinish = $scope.grant.form.valueDateFinish ? $scope.grant.form.valueDateFinishObj : null;
          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.grant.searchResult.data = data;
              } else {
                $scope.grant.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };


      $scope.grant.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                regDate: {type: "date"},
                valueDate: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findGrant(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "number",
            title: gettextCatalog.getString("Registration Number"),
            width: '10rem'

          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Registration Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "holder.name",
            title: gettextCatalog.getString("Grantor"),
            width: '10rem'
          },
          {
            field: "receiver.name",
            title: gettextCatalog.getString("Grantee"),
            width: '10rem'
          },
          {
            field: "holder.participantAccountNumber",
            title: "Bağışlayanın iştirakçı hesabı",
            width: '10rem'
          },
          {
            field: "receiver.participantAccountNumber",
            title: "Bağışlananın iştirakçı hesabı",
            width: '10rem'
          },
          {
            field: "holder.pinCode",
            title: "Bağışlayanın pin kodu",
            width: '10rem'
          },
          {
            field: "receiver.pinCode",
            title: "Bağışlananın pin kodu",
            width: '10rem'
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: '15rem'
          }
        ]
      };


      $scope.grantSelected = grantSelected;


      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {

        $scope.grant.form.destination = null;
        $scope.grant.form.referenceNumber = null;
        $scope.grant.form.regDateStart = null;
        $scope.grant.form.regDateFinish = null;
        $scope.grant.form.valueDateStart = null;
        $scope.grant.form.valueDateFinish = null;
        $scope.grant.form.settlementDateStart = null;
        $scope.grant.form.settlementDateFinish = null;
        $scope.grant.form.sellSideMemberName = null;
        $scope.grant.form.sellSideClientName = null;
        $scope.grant.form.buySideMemberName = null;
        $scope.grant.form.buySideClientName = null;
        $scope.grant.form.ISIN = null;
        $scope.grant.form.issuerName = null;

      };


      $scope.findGrant = function () {

        var formValidationResult = validateForm(angular.copy($scope.grant.form));
        if (formValidationResult.success) {
          if ($scope.grant.searchResultGrid) {
            $scope.grant.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.grant.searchResultGrid) {
          $scope.grant.searchResultGrid = widget;
        }
      });

      $scope.detailGrid = function (dataItem) {

        return {
          dataSource: {
            data: dataItem.subjects,
            pageSize: 5
          },
          scrollable: true,
          pageable: true,
          columns: [
            {
              field: "instrument.instrumentName",
              title: gettextCatalog.getString("Instrument Name"),
              width: "10rem"
            },
            {
              field: "instrument.ISIN",
              title: gettextCatalog.getString("ISIN"),
              width: "10rem"
            },
            {field: "instrument.issuerName", title: gettextCatalog.getString('Issuer'), width: "10rem"},
            {field: "quantity", title: gettextCatalog.getString('Quantity'), width: "10rem"}

          ]
        };
      };
    }]);
