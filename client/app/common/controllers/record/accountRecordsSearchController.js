'use strict';

angular.module('cmisApp')
  .controller('AccountRecordsSearchController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        var defaultParams = {
          searchUrl: "/api/persons/getUnderwriters/",
          showSearchCriteriaBlock: false,
          config: {
            searchOnInit: false,
          }
        };

        $scope.serviceClasses = [
          {code: 'BALANCE_SERVICE_UNK', name: 'Naməlum ödənişlər'},
          {code: 'BALANCE_SERVICE_TRD', name: 'Ticarət bölməsi'},
          {code: 'BALANCE_SERVICE_REV', name: 'Gəlir hesabı'},
          {code: 'BALANCE_SERVICE_VAT', name: 'ƏDV hesabı'},
          {code: 'BALANCE_SERVICE_SUB', name: 'Abunə yazılışı bölməsi'},
          {code: 'BALANCE_SERVICE_FRX', name: 'FOREX bölməsi'},
          {code: 'BALANCE_SERVICE_COL', name: 'Pul zəmanəti bölməsi'},
          {code: 'BALANCE_SERVICE_CLR', name: 'Klirinq hesabı'},
          {code: 'BALANCE_SERVICE_CASH', name: 'Nağd pul hesabı'},
          {code: 'BALANCE_SERVICE_VATADV', name: 'Avans ƏDV'},
          {code: 'BALANCE_SERVICE_SRV', name: 'Xidmət hesabı'},
          {code: 'BALANCE_SERVICE_DIS', name: 'Ödəniş bölməsi'},
          {code: 'BALANCE_SERVICE_TAX', name: ' Mənbə vergisi'}
        ];


        $scope.operationClassCodes = [
          {code: 'COUPON_PAYMENT_RECORD', name: 'Kupon ödənişi'},
          {code: 'ISSUE_TITLE_ABSTRACT_RECORD', name: 'Hesabdan bir QK üzrə çıxarış'},
          {code: 'OUTGOING_PAYMENT_OPERATION', name: 'Hesabdan Məxaric'},
          {code: 'OTC_TRADE_RECORD', name: 'Birjadankənar Əqd'},
          {code: 'INCOMMING_PAYMENT_OPERATION', name: 'Hesaba mədaxil'},
          {code: 'ACCOUNT_OPENING_RECORD', name: 'Hesab açılışı'},
          {code: 'AZIPS_COMMISSION_OPERATION', name: 'AZIPS xidmət haqqının tutulması'},
          {code: 'AZIPS_RECORD', name: 'AZIPS xidmət haqqı'},
          {code: 'COUPON_PAYMENT_OPERATION', name: 'Kupon ödənişi əməliyyatı'},
          {code: 'MARKET_TRADE_RECORD ', name: 'Birja əqdi'}
        ];


        $scope.statusCodes = [
          {code: 'STATUS_PENDING', name: 'Gözləmədədir'},
          {code: 'STATUS_PROCESSED', name: 'Emal olunub'}
        ];


        $scope.params = angular.merge(defaultParams, $scope.searchConfig.params);

        var validateForm = function (formData) {

          var result = {
            success: false,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };
          // if (formData.clientName || formData.brokerName || formData.finishDateStart || formData.finishDateFinish) {
          result.success = true;
          //}
          return result;
        };

        $scope.test = {
          personSelected: function (data) {
            var sResult = angular.copy($scope.bla);

            for (var i = 0; i < sResult.length; i++) {

              if (sResult[i].ID == data.ID) {
                $scope.searchConfig.accountPrescription.selectedPersonIndex = sResult[i];

                break;
              }
            }

          }
        };


        var findData = function (e) {


          if ($scope.search.searchResultGrid !== undefined) {
            $scope.search.searchResultGrid.refresh();
          }
          if ($scope.search.formName.$dirty) {
            $scope.search.formName.$submitted = true;
          }

          if ($scope.search.formName.$valid || !$scope.search.formName.$dirty) {

            $scope.search.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.search.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {

              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }


              formData.creationDateStart = $scope.search.form.creationDateStart ? $scope.search.form.creationDateStartObj : null;
              formData.creationDateEnd = $scope.search.form.creationDateEnd ? $scope.search.form.creationDateEndObj : null;


              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take,
                sort: e.data.sort
              });


              console.log(requestData)
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).success(function (data) {
                $scope.bla = data.data
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };


        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];


        $scope.search = angular.merge(
          $scope.searchConfig.accountPrescription,
          {
            formName: 'search',
            form: {
              status: "STATUS_NOT_SETTLED"
            },
            data: {},

            searchResult: {
              data: null,
              isEmpty: false
            }
          }
        );


        $scope.findData = function () {

          var formValidationResult = validateForm(angular.copy($scope.search.form));

          if (formValidationResult.success) {
            if ($scope.search.searchResultGrid) {
              $scope.search.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };


        $scope.mainGridOptions = {
          excel: {
            allPages: true,
            fileName: "hesab_yazilari.xlsx",
            proxyURL: "/api/main/export",
            forceProxy: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  creationDate: {type: "date"},
                  paymentDate: {type: "date"},
                  amount: {type: "number"},
                  remainingAmount: {type: "number"},
                  ID: { type: 'number' }

                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },
            sort: {field: "creationDate", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "ID",
              title: "#",
              width: "10rem"
            },
            {
              field: "account.name",
              title: 'Ad',
              width: "10rem"
            },
            {
              field: "account.accountNumber",
              title: gettextCatalog.getString("Account Number"),
              width: "10rem"
            },
            {
              field: "serviceClass.nameAz",
              template:  "#= data.serviceClass ? data.serviceClass.nameAz: ''  #",
              title: "Xidmət sinfi",
              width: "10rem"
            },
            {
              field: "amount",
              title: gettextCatalog.getString("Amount"),
              width: "10rem",
              template: "<div style='text-align: right'>#= kendo.toString(data.amount, 'n2') #</div>",
              format: "{0:n2}"
            },
            {
              field: "remainingAmount",
              title: 'Qalıq',
              width: "10rem",
              template: "<div style='text-align: right'>#= kendo.toString(data.remainingAmount, 'n2') #</div>",
              format: "{0:n2}"
            },
            {
              field: "currency",
              title: gettextCatalog.getString("Currency"),
              width: "4rem"
            },
            {
              field: "creationDate",
              title: gettextCatalog.getString("Creation Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "operationClass.nameAz",
              title: 'Əməliyyat sinfi',
              width: "7rem",
              format: "{0:n2}"
            },
            {
              field: "operationNumber",
              title: 'Əməliyyat nömrəsi',
              width: "10rem"
            },
            {
              field: "status.nameAz",
              title: "Status",
              width: "15rem"
            }
          ]
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.search.form.serviceClassCode = null;
          $scope.search.form.currencyCode = null;
          $scope.search.form.accountNumber = null;
          $scope.search.form.creationDateStart = null;
          $scope.search.form.creationDateEnd = null;
          $scope.search.form.operationClassCode = null;
          $scope.search.form.operationID = null;
          $scope.search.form.statusCode = null;
          $scope.search.form.personName = null;
        };
      }]
  );



