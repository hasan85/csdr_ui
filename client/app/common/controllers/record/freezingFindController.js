'use strict';

angular.module('cmisApp')
  .controller('FreezingFindController', ['$scope', '$windowInstance', 'recordsService', 'searchCriteria',
    function ($scope, $windowInstance, recordsService, searchCriteria) {

      $scope.searchConfig = {
        freezing: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedFreezingIndex: false
        },
        params: {
                 showSearchCriteriaBlock: true,
          config: {
            searchOnInit: true,
            searchCriteria:searchCriteria
          }
        }
      };

      $scope.selectFreezing = function () {
        recordsService.selectFreezing($scope.searchConfig.freezing.searchResult.data.data[$scope.searchConfig.freezing.selectedFreezingIndex]);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
