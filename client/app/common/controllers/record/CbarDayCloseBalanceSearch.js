'use strict';

angular.module('cmisApp')
  .controller('CbarDayCloseBalanceSearchController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http', 'referenceDataService',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http, referenceDataService) {

        var defaultParams = {
          searchUrl: "/api/records/getCBARDayCloseBalances/",
          showSearchCriteriaBlock: false,
          config: {
            searchOnInit: false
          }
        };


        $scope.currencies = [
          {code: 'AZN'},
          {code: 'USD'},
        ]


        $scope.params = angular.merge(defaultParams, $scope.searchConfig.params);

        var validateForm = function (formData) {

          var result = {
            success: true,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };

          return result;
        };


        var findData = function (e) {

          if ($scope.search.searchResultGrid !== undefined) {
            $scope.search.searchResultGrid.refresh();
          }
          if ($scope.search.formName.$dirty) {
            $scope.search.formName.$submitted = true;
          }

          if ($scope.search.formName.$valid || !$scope.search.formName.$dirty) {

            $scope.search.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.search.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {

              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }

              formData.messageDateStart = $scope.search.form.messageDateStart ? $scope.search.form.messageDateStartObj : null;
              formData.messageDateFinish = $scope.search.form.messageDateFinish ? $scope.search.form.messageDateFinishObj : null;

              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take,
                sort: e.data.sort
              });


              console.log(requestData)
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).success(function (data) {

                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };


        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];


        $scope.search = angular.merge(
          $scope.searchConfig.accountPrescription,
          {
            formName: 'search',
            form: {},
            data: {},

            searchResult: {
              data: null,
              isEmpty: false
            }
          }
        );


        $scope.findData = function () {

          var formValidationResult = validateForm(angular.copy($scope.search.form));

          if (formValidationResult.success) {
            if ($scope.search.searchResultGrid) {
              $scope.search.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };


        $scope.toolTipOptions = {
          filter: "td:nth-child(15)",
          position: "top",
          content: function (e) {
            var grid = e.target.closest(".k-grid").getKendoGrid();
            var dataItem = grid.dataItem(e.target.closest("tr"));
            return dataItem.destination;

          },
          show: function (e) {
            this.popup.element[0].style.width = "300px";
          }
        }


        $scope.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  messageDate: {type: "date"},
                  creationDate: {type: "date"},
                  amount: {type: "number"},
                  systemBalance: {type: "number"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },
            sort: {field: "creationDate", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "messageDate",
              title: 'Mesaj tarixi',
              width: "10rem",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "creationDate",
              title: "Yaranma tarixi",
              width: "10rem",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "currencyCode",
              title: gettextCatalog.getString("Currency"),
              width: "10rem"
            },
            {
              field: "amount",
              title: "Mərkəzi bankdakı balans",
              width: "10rem",
              template: "<div style='text-align: right'>#= kendo.toString(amount, 'n2') # </div>"
            },
            {
              field: "systemBalance",
              title: 'Sistemdəki balans',
              width: "10rem",
              template: "<div style='text-align: right'>#= kendo.toString(systemBalance, 'n2') # </div>"
            },
            {
              field: "systemBalance",
              title: 'Fərq',
              width: "10rem",
              template:  "<div style='text-align: right'>#= kendo.toString(data.amount - data.systemBalance, 'n2') # </div>"
            }

          ]
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.search.form.messageDateStart = null;
          $scope.search.form.messageDateFinish = null;
          $scope.search.form.currencyCode = null;
        };

      }]
  );

