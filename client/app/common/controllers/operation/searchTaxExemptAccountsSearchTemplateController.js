'use strict';

angular.module('cmisApp')
  .controller('SearchTaxExemptAccountsSearchTemplateController',
    ['$scope', 'gettextCatalog', '$http', 'personFindService', 'Loader', 'appConstants', 'SweetAlert', 'referenceDataService',
      'helperFunctionsService', "$rootScope",
      function ($scope,gettextCatalog, $http, personFindService, Loader, appConstants, SweetAlert, referenceDataService,
                helperFunctionsService, $rootScope) {

        var validateForm = function (formData) {

          var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
          result.success = true;

          return result;
        };
        Loader.show(true);
        referenceDataService.getCouponPaymentMethodClasses().then(function (res) {
          $scope.distributionClasses = res;
          Loader.show(false);
        });

        var personSelected = function (data) {
          var sResult = angular.copy($scope.person.searchResult.data.data);

          for (var i = 0; i < sResult.length; i++) {

            if (sResult[i].processInstanceId == data.processInstanceId) {
              $scope.person.selectedPersonIndex = i;
              break;
            }
          }
        };

        $scope.personSelected = personSelected;

        var _params = {
          searchUrl: "/api/task-services/getMyFinishedTasks",
          showSearchCriteriaBlock: true,
          config: {
            criteriaForm: {
              personType: {
                isVisible: true
              },
              accountNumber: {
                isVisible: false
              }
            },
            searchOnInit: false
          },
          criteriaEnabled: true
        };


        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        // var _person = {
        //   formName: 'personFindForm',
        //   form: {},
        //   data: {
        //     personClasses: [],
        //     personClassCodes: {
        //     }
        //   },
        //
        //   searchResult: {
        //     data: null,
        //     isEmpty: false
        //   },
        //   selectedPersonType: $scope.params.config.criteriaForm.personType.default,
        //   selectedPersonIndex: false,
        //   personSelected: personSelected,
        //   personDetails: false
        // };
        var _person = {
          formName: 'personFindForm',
          form: {
            createdDate: {

            }
          },
          data: {},
          searchResult: {
            data: null,
            isEmpty: false
          },
          selectedPersonIndex: false,
          findPerson: findPerson,
          personSelected: personSelected
        };


        if ($scope.searchConfig.person) {
          $scope.person = angular.merge($scope.searchConfig.person, _person);
        } else {
          $scope.person = angular.copy(_person);
        }

        var findPerson = function (e) {

          if ($scope.person.searchResultGrid !== undefined) {
            $scope.person.searchResultGrid.refresh();
          }
          if ($scope.person.formName.$dirty) {
            $scope.person.formName.$submitted = true;
          }

          if ($scope.person.formName.$valid || !$scope.person.formName.$dirty) {

            $scope.person.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.person.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {


              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }

              formData.createdDate.first = $scope.person.form.createdDate.first ? $scope.person.form.createdDate.firstObj : null;
              formData.createdDate.last = $scope.person.form.createdDate.last ? $scope.person.form.createdDate.lastObj : null;
              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take
              });

              console.log('Request Data', requestData);
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.person.searchResult.data = data;
                } else {
                  $scope.person.searchResult.isEmpty = true;
                }
                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total ? data.total : 0
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });

            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };


        $scope.person.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                }
              }
            },
            transport: {
              read: function (e) {
                findPerson(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "id",
              title: "ID",
              width: "5rem"
            },
            {
              field: "name",
              title: "Təşkilatın adı"
            },
            {
              field: "distributionClass",
              title: "Paylanma Sinifi"
            }
          ]
        };

        $scope.personSelected = personSelected;

        $scope.closePersonDetails = function () {
          $scope.person.personDetails = false;
        };

        $scope.changePersonType = function () {

          var personClasses = angular.copy($scope.person.data.personClasses);
          for (var i = 0; i < personClasses.length; i++) {
            if (personClasses[i].id == $scope.person.form.personClassId) {
              $scope.person.selectedPersonType = personClasses[i].code;
            }
          }
          $scope.resetForm();
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.person.form.name = null;
          $scope.person.form.shortDescription = null;
          $scope.person.form.createdDate.first = null;
          $scope.person.form.createdDate.last = null;
          $scope.person.form.distributionClassId = null;
        };

        $scope.findPerson = function () {

          var formValidationResult = validateForm(angular.copy($scope.person.form));
          if (formValidationResult.success) {
            if ($scope.person.searchResultGrid) {
              $scope.person.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {

          if (widget === $scope.person.searchResultGrid) {
            $scope.person.searchResultGrid.element.on('dblclick', function () {
              var selectedData = $scope.person.searchResultGrid.dataItem($scope.person.searchResultGrid.select());
              if (!selectedData) {
                return;
              }
              if ($scope.selectPerson)
                $scope.selectPerson($scope.person.searchResultGrid);
            });
          }
        });
      }]);
