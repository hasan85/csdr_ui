'use strict';

angular.module('cmisApp')
  .controller('OperationSearchTemplateController',
    ['$scope', 'gettextCatalog', '$http', 'personFindService', 'Loader', 'appConstants', 'SweetAlert', 'referenceDataService',
      'helperFunctionsService', "$rootScope",
      function ($scope,gettextCatalog, $http, personFindService, Loader, appConstants, SweetAlert, referenceDataService,
                helperFunctionsService, $rootScope) {

        var validateForm = function (formData) {

          var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
          result.success = true;

          return result;
        };

        var personSelected = function (data) {
          var sResult = angular.copy($scope.person.searchResult.data.data);

          for (var i = 0; i < sResult.length; i++) {

            if (sResult[i].processInstanceId == data.processInstanceId) {
              $scope.person.selectedPersonIndex = i;
              break;
            }
          }
        };

        $scope.personSelected = personSelected;

        var _params = {
          searchUrl: "/api/task-services/getMyFinishedTasks",
          showSearchCriteriaBlock: true,
          config: {
            criteriaForm: {
              personType: {
                isVisible: true
              },
              accountNumber: {
                isVisible: false
              }
            },
            searchOnInit: false
          },
          criteriaEnabled: true
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);
        console.log("Params", $scope.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        // var _person = {
        //   formName: 'personFindForm',
        //   form: {},
        //   data: {
        //     personClasses: [],
        //     personClassCodes: {
        //     }
        //   },
        //
        //   searchResult: {
        //     data: null,
        //     isEmpty: false
        //   },
        //   selectedPersonType: $scope.params.config.criteriaForm.personType.default,
        //   selectedPersonIndex: false,
        //   personSelected: personSelected,
        //   personDetails: false
        // };
        var _person = {
          formName: 'personFindForm',
          form: {},
          data: {},
          searchResult: {
            data: null,
            isEmpty: false
          },
          selectedPersonIndex: false,
          findPerson: findPerson,
          personSelected: personSelected
        };


        $scope.person = angular.merge($scope.searchConfig.person, _person);

        var findPerson = function (e) {

          if ($scope.person.searchResultGrid !== undefined) {
            $scope.person.searchResultGrid.refresh();
          }
          if ($scope.person.formName.$dirty) {
            $scope.person.formName.$submitted = true;
          }

          if ($scope.person.formName.$valid || !$scope.person.formName.$dirty) {

            $scope.person.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.person.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {


              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }

              formData.startStartDate = $scope.person.form.startStartDate ? $scope.person.form.startStartDateObj : null;
              formData.startFinishDate = $scope.person.form.startFinishDate ? $scope.person.form.startFinishDateObj : null;
              var requestData = angular.extend(formData, {
                skip: e.data.skip,
                take: e.data.take,
              });

              console.log('Request Data', requestData);
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.person.searchResult.data = data;
                } else {
                  $scope.person.searchResult.isEmpty = true;
                }
                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total ? data.total : 0
                  });
                } else {
                  SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
                }

                Loader.show(false);
              });

            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };


        $scope.person.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                id: "id",
                fields: {
                  startDate: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findPerson(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "processInstanceId",
              title: '#',
              width: '7rem'
            },
            {
              field: "name" + $rootScope.lnC,
              title: gettextCatalog.getString("Name")
            },
            {
              field: "startDate",
              title: gettextCatalog.getString("Start Date"),
              type: "date",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "shortDescription",
              title: gettextCatalog.getString("Short Description"),
              template: "<div title='#= shortDescription #'>#= shortDescription # </div>"
            },
            {
              field: "username",
              title: "Əməliyyatı icra edən"
            }

          ]
        };

        $scope.personSelected = personSelected;

        $scope.showPersonDetails = function () {

          Loader.show(true);
          personFindService.findPersonById($scope.searchConfig.person.searchResult.data.data[
            $scope.searchConfig.person.selectedPersonIndex]['id']).then(function (data) {
            $scope.person.personDetails = data;
            Loader.show(false);
          });

        };

        $scope.closePersonDetails = function () {
          $scope.person.personDetails = false;
        };

        $scope.changePersonType = function () {

          var personClasses = angular.copy($scope.person.data.personClasses);
          for (var i = 0; i < personClasses.length; i++) {
            if (personClasses[i].id == $scope.person.form.personClassId) {
              $scope.person.selectedPersonType = personClasses[i].code;
            }
          }
          $scope.resetForm();
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.person.form.name = null;
          $scope.person.form.shortDescription = null;
          $scope.person.form.startStartDate = null;
          $scope.person.form.startFinishDate = null;
          $scope.person.form.processInstanceId = null;
        };

        $scope.detailInit = function (dataItem) {
          // Loader.show(true);
          return {
            selectable: true,
            dataSource: {
              data: dataItem.taskData,
              schema: {
                model: {
                  id: "id",
                  fields: {
                    name: {type: "string"},
                    createDate: {type: "date"},
                    endDate: {type: "date"},
                    assignee: {type: "string"}
                  }
                }
              },

              sort: {field: "createDate", dir: "desc"}
            },

            scrollable: true,
            pageable: false,
            columns: [
              {
                field: "id",
                title: '#',
                width: '7rem'
              },
              {
                field: "name" + $rootScope.lnC,
                title: gettextCatalog.getString("Task")
              },
              {
                field: "createDate",
                title: gettextCatalog.getString("Create Date"),
                type: "date",
                format: "{0:dd-MMMM-yyyy HH:mm}"
              },
              {
                field: "endDate",
                title: gettextCatalog.getString("End Date"),
                type: "date",
                format: "{0:dd-MMMM-yyyy HH:mm}"
              },
              {
                field: "assigne",
                title: gettextCatalog.getString("Assignee")
              }
            ]
          };
        };

        $scope.findPerson = function () {

          var formValidationResult = validateForm(angular.copy($scope.person.form));
          if (formValidationResult.success) {
            if ($scope.person.searchResultGrid) {
              $scope.person.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {

          if (widget === $scope.person.searchResultGrid) {
            $scope.person.searchResultGrid.element.on('dblclick', function () {
              var selectedData = $scope.person.searchResultGrid.dataItem($scope.person.searchResultGrid.select());
              if (!selectedData) {
                return;
              }
              $scope.selectPerson($scope.person.searchResultGrid);
            });
          }
        });
      }]);
