'use strict';

angular.module('cmisApp')
  .controller('LocalIssuersForeignTradersForStockSearchTemplateController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {


        var validateForm = function (formData) {

          var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};


          result.success = true;

          return result;
        };

        var findData = function (e) {

          if ($scope.trade.searchResultGrid !== undefined) {
            $scope.trade.searchResultGrid.refresh();
          }
          if ($scope.trade.formName.$dirty) {
            $scope.trade.formName.$submitted = true;
          }

          if ($scope.trade.formName.$valid || !$scope.trade.formName.$dirty) {

            $scope.trade.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.trade.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {


              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }


              var currentMonth=(new Date()).getMonth();
              var yyyy =(new Date()).getFullYear();
              var start = (Math.floor(currentMonth/3)*3)+1,
                  finish = start+3,
                  startDate = new Date(start+'-01-'+ yyyy),
                  finishDate = finish>12?new Date('01-01-'+ (yyyy+1)):new Date(finish+'-01-'+ (yyyy));
                  finishDate = new Date((finishDate.getTime())-1);

              console.log('startDate =', startDate,'finishDate =', finishDate);

              var lastDateWithSlashes = (finishDate.getDate()) + '-' + (finishDate.getMonth() + 1) + '-' + finishDate.getFullYear();
              var firstDateWithSlashes = (startDate.getDate()) + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();



              if (!$scope.trade.form.startDateObj) {
                $scope.trade.form.startDateObj = startDate;
                $scope.trade.form.startDate = firstDateWithSlashes;
              }
              if (!$scope.trade.form.finishDateObj) {
                $scope.trade.form.finishDateObj = finishDate;
                $scope.trade.form.finishDate = lastDateWithSlashes;
              }


              formData.startDate = $scope.trade.form.startDateObj;
              formData.finishDate = $scope.trade.form.finishDateObj;

              var requestData = angular.extend(formData, {

              });

              console.log('Request Data', requestData);
              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                data.data = helperFunctionsService.convertObjectToArray(data.data);
                if (data) {
                  $scope.trade.searchResult.data = data;
                } else {
                  $scope.trade.searchResult.isEmpty = true;
                }

                console.log('Response data', data);
                if (data['success'] === "true") {

                  e.success({
                    Data: data.data ? data.data : [], Total: data.total
                  });
                } else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }

                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };

        var _params = {
          searchUrl: "/api/reports/getLocalIssuersForeignTradersForStock/",
          showSearchCriteriaBlock: false,
          config: {
            searchOnInit: false
          }
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        var _trade = {
          formName: 'tradeFindForm',
          form: {},
          data: {},
          metaData: {
            marketTypes: ['',"STK", "BND", "PRM", "RPO"]
          },
          searchResult: {
            data: null,
            isEmpty: false
          },
          selectedtradeIndex: false,
          findData: findData
        };

        $scope.trade = angular.merge($scope.searchConfig.trade, _trade);

        $scope.findData = function () {

          var formValidationResult = validateForm(angular.copy($scope.trade.form));
          if (formValidationResult.success) {
            if ($scope.trade.searchResultGrid) {
              $scope.trade.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };
        $scope.trade.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  maturityDate: {type: "date"},
                  valueDate: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },

            serverPaging: false,
            serverSorting: false
          },
          selectable: true,
          scrollable: true,
          // pageable: {
          //   "pageSize": 20,
          //   "refresh": true,
          //   "pageSizes": true
          // },
          pageable: {
            input: false,
            numeric: true
          },
          sortable: true,
          resizable: true,
          columns: [

            // {
            //   field: "recordNumber",
            //   title: gettextCatalog.getString("Trade Number"),
            //   width: '10rem'
            // },
            {
              field: "valueDate",
              title: gettextCatalog.getString("Trade Date"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "issuer",
              title: gettextCatalog.getString("Issuer"),
              width: '10rem'
            },
            {
              field: "sellerCountry",
              title: gettextCatalog.getString("Seller Country"),
              width: '10rem'
            },
            {
              field: "buyerCountry",
              title: gettextCatalog.getString("Buyer Country"),
              width: '10rem'
            },
            {
              field: "startRemainder",
              title: gettextCatalog.getString("Start Remainder"),
              width: '10rem'
            },
            {
              title: gettextCatalog.getString("Operations"),
              columns: [
                {
                  field: "buyerRecordQuantity",
                  title: gettextCatalog.getString("Buyer Record Quantity"),
                  width: '10rem'
                },
                {
                  field: "buyerRecordPrice",
                  title: gettextCatalog.getString("Buyer Record Price"),
                  width: '10rem'
                },
                {
                  field: "sellerRecordQuantity",
                  title: gettextCatalog.getString("Seller Record Quantity"),
                  width: '10rem'
                },
                {
                  field: "sellerRecordPrice",
                  title: gettextCatalog.getString("Seller Record Price"),
                  width: '10rem'

                },
              ]
            },
            {
              field: "finishRemainder",
              title: gettextCatalog.getString("Finish Remainder"),
              width: '10rem'
            },
            {
              field: "currency",
              title: gettextCatalog.getString("Currency"),
              width: '10rem'
            }
          ]
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

      }]);
