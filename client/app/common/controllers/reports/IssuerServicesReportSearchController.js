'use strict';

angular.module('cmisApp')
  .controller('IssuerServicesReportSearchController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        var defaultParams = {
          showSearchCriteriaBlock: false,
          config: {
            searchOnInit: false
          }
        };

        $scope.params = angular.merge(defaultParams, $scope.searchConfig.params);
        $scope.form = {}


        var date = new Date();
        var start = new Date(date.getFullYear(), date.getMonth(), 1);
        var end = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        var lastDateWithSlashes = (end.getDate()) + '-' + (end.getMonth() + 1) + '-' + end.getFullYear();
        var firstDateWithSlashes = (start.getDate()) + '-' + (start.getMonth() + 1) + '-' + start.getFullYear();


        if (!$scope.form.startObj) {
          $scope.form.startObj = start;
          $scope.form.start = firstDateWithSlashes;
        }
        if (!$scope.form.endObj) {
          $scope.form.endObj = end;
          $scope.form.end = lastDateWithSlashes;
        }


        var findData = function (e) {


          Loader.show(true);

          var formData = angular.copy($scope.form);

          formData.start = formData.start ? formData.startObj : null;
          formData.end = formData.end ? formData.endObj : null;

          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: formData}}).success(function (data) {

            if (data['success'] === "true") {

              e.success({
                Data: data.data ? data.data : [], Total: data.data ? data.data.length : 0
              });
            } else {
              SweetAlert.swal("", (data.resultCode ? data.resultCode + " " : '') + data.message, 'error');
            }

            Loader.show(false);
          });


        }


        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];


        $scope.search = angular.merge(
          $scope.searchConfig.accountPrescription,
          {
            formName: 'search',
            form: {},
            data: {},

            searchResult: {
              data: null,
              isEmpty: false
            }
          }
        );


        $scope.findData = function () {

          if ($scope.search.searchResultGrid) {
            $scope.search.searchResultGrid.dataSource.page(1);
          }


        };


        $scope.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  paymentDate: {type: "date"},
                  creationDate: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },
            sort: {field: "creationDate", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "account.name",
              title: gettextCatalog.getString("Issuer"),
              width: "10rem"
            },
            {
              field: "startBalance",
              title: " İlkin qalıq",
              width: "10rem"
            },
            {
              field: "inAmount",
              title: " Mədaxil",
              width: "10rem"
            },
            {
              field: "outAmount",
              title: "Məxaric",
              width: "10rem"
            },
            {
              field: "endBalance",
              title: "Son qalıq",
              width: "5rem"
            }

          ]
        };


        $scope.detailGrid = function (dataItem) {
          return {
            excel: {
              allPages: true
            },
            dataSource: {
              schema: {
                data: "Data",
                total: "Total",
                model: {
                  fields: {
                    Date: {type: "date"}
                  }
                }
              },
              transport: {
                read: function (e) {

                  var requestData = {
                    accountID: dataItem.account.accountId,
                    start: $scope.form.start ? $scope.form.startObj : null,
                    end: $scope.form.end ? $scope.form.endObj : null

                  };
                  $http({
                    method: 'POST',
                    url: "/api/records/getOutgoingRecordsByAccountID/",
                    data: {data: requestData}
                  })
                    .success(function (data) {
                      data = helperFunctionsService.convertObjectToArray(data.data);
                      data = data ? data : [];
                      e.success({Data: data, Total: data.length});
                    });
                  // getGlobusGlobalOperationDetail(e, dataItem);
                }
              },
              serverPaging: false,
              serverSorting: false,
              pageSize: 15
            },
            scrollable: true,
            pageable: true,
            sortable: true,
            columns: [
              {
                field: "amount",
                title: gettextCatalog.getString("Amount"),
                width: "10rem"
              },
              {
                field: "operationClass.nameAz",
                title: "Xidmət növü",
                width: "10rem"
              }
            ]
          };
        };




        $scope.detailGrid2 = function (dataItem) {
          return {
            excel: {
              allPages: true
            },
            dataSource: {
              schema: {
                data: "Data",
                total: "Total",
                model: {
                  fields: {
                    date: {type: "date"}
                  }
                }
              },
              transport: {
                read: function (e) {

                  var requestData = {
                    accountID: dataItem.accountID,
                    operationClassID: dataItem.operationClass.id,
                    start: $scope.form.start ? $scope.form.startObj : null,
                    end: $scope.form.end ? $scope.form.endObj : null

                  };
                  $http({
                    method: 'POST',
                    url: "/api/records/getOutgoingRecordsByAccountAndOperationClass/",
                    data: {data: requestData}
                  })
                    .success(function (data) {
                      data = helperFunctionsService.convertObjectToArray(data.data);
                      data = data ? data : [];
                      e.success({Data: data, Total: data.length});
                    });
                  // getGlobusGlobalOperationDetail(e, dataItem);
                }
              },
              serverPaging: false,
              serverSorting: false,
              pageSize: 15
            },
            scrollable: true,
            pageable: true,
            sortable: true,
            columns: [
              {
                field: "date",
                title: gettextCatalog.getString("Date"),
                width: "10rem",
                format: "{0:dd-MMMM-yyyy HH:mm}"
              },
              {
                field: "amount",
                title: gettextCatalog.getString("Amount"),
                width: "10rem"
              },
              {
                field: "operationNumber",
                title: "Əməliyyatın nömrəsi",
                width: "10rem"
              }
            ]
          };
        };



        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.search.form.paymentDateStart = null;
          $scope.search.form.paymentDateFinish = null;
          $scope.search.form.status = null;
          $scope.search.form.referenceNumber = null;
          $scope.search.form.currencyCode = null;
          $scope.search.form.payeeClientAccountNumber = null;
          $scope.search.form.payeeClientAccountName = null;
          $scope.search.form.serviceClass = null;
        };

        $scope.resetForm()

      }]
  )
;
