'use strict';

angular.module('cmisApp')
  .controller('UndeliveredSMSReportSearchTemplateController',
    ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
      '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
      function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
                $http, helperFunctionsService, recordsService, $rootScope) {

        var validateForm = function (formData) {
          var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

          return result;
        };

        var subscriptionOrdersSelected = function (data) {
          var sResult = angular.copy($scope.subscriptionOrders.searchResult.data.data);
          for (var i = 0; i < sResult.length; i++) {
            if (sResult[i].ID == data.ID) {
              $scope.subscriptionOrders.selectedSubscriptionOrdersIndex = i;
              break;
            }
          }
        };

        $scope.subscriptionOrdersSelected = subscriptionOrdersSelected;

        var _params = {
          searchUrl: "/api/reports/getUndeliveredSMSRecords/",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {},
            searchOnInit: true
          },
          criteriaEnabled: true
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _subscriptionOrders = {
          formName: 'subscriptionOrdersFindForm',
          form: {},
          data: {},

          searchResult: {
            data: null,
            isEmpty: false
          },

          selectedSubscriptionOrdersIndex: false,
          subscriptionOrdersSelected: subscriptionOrdersSelected,
          subscriptionOrdersDetails: false
        };

        $scope.subscriptionOrders = angular.merge($scope.searchConfig.subscriptionOrders, _subscriptionOrders);

        var findSubscriptionOrders = function (e) {

          //$scope.person.subscriptionOrdersDetails = false;

          if ($scope.subscriptionOrders.searchResultGrid !== undefined) {
            $scope.subscriptionOrders.searchResultGrid.refresh();
          }

          $scope.subscriptionOrders.selectedSubscriptionOrdersIndex = false;

          if ($scope.subscriptionOrders.formName.$dirty) {
            $scope.subscriptionOrders.formName.$submitted = true;
          }

          if ($scope.subscriptionOrders.formName.$valid || !$scope.subscriptionOrders.formName.$dirty) {

            $scope.subscriptionOrders.searchResult.isEmpty = false;

            var formData = angular.copy($scope.subscriptionOrders.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            formData.sendTimeStart = $scope.subscriptionOrders.form.sendTimeStart ? $scope.subscriptionOrders.form.sendTimeStartObj : null;
            formData.sendTimeFinish = $scope.subscriptionOrders.form.sendTimeFinish ? $scope.subscriptionOrders.form.sendTimeFinishObj : null;

            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            //console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data) {
                $scope.subscriptionOrders.searchResult.data = data;
              } else {
                $scope.subscriptionOrders.searchResult.isEmpty = true;

              }

              //console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };

        $scope.subscriptionOrders.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  sendTime: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findSubscriptionOrders(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "sendTime",
              title: gettextCatalog.getString("Send Time"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "recordNumber",
              title: gettextCatalog.getString("Record Number"),
              width: "10rem"
            },
            {
              field: "smsText",
              title: gettextCatalog.getString("SMS Text"),
              width: "20rem",
              attributes: {
                style: "white-space: normal"
              }
            },
            {
              field: "personName",
              title: gettextCatalog.getString("Person Name"),
              width: "10rem"
            },
            {
              field: "phoneNumber",
              title: gettextCatalog.getString("Phone Number"),
              width: "10rem"
            },
            {
              field: "status",
              title: gettextCatalog.getString("Status"),
              width: "10rem"

            }
          ]
        };

        $scope.subscriptionOrdersSelected = subscriptionOrdersSelected;

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        $scope.resetForm = function () {
          $scope.subscriptionOrders.form.sendTimeStart = null;
          $scope.subscriptionOrders.form.sendTimeFinish = null;
          $scope.subscriptionOrders.form.status = null;
          $scope.subscriptionOrders.form.recordNumber = null;
          $scope.subscriptionOrders.form.smsText = null;
          $scope.subscriptionOrders.form.phoneNumber = null;
          $scope.subscriptionOrders.form.personName = null;
        };

        $scope.findSubscriptionOrders = function () {
          var formValidationResult = validateForm(angular.copy($scope.subscriptionOrders.form));
          if (formValidationResult.success) {
            if ($scope.subscriptionOrders.searchResultGrid) {
              $scope.subscriptionOrders.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.subscriptionOrders.searchResultGrid) {
            $scope.subscriptionOrders.searchResultGrid = widget;
          }
        });

      }]);

