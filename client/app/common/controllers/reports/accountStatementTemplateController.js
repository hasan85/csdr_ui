'use strict';

angular.module('cmisApp')
  .controller('AccountStatementSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        // if (formData.documentNumber || formData.accountNumber || formData.creationDateStart || formData.creationDateFinish || formData.personName) {
        result.success = true;
        //  }
        return result;
      };

      var accountStatementSelected = function (data) {
        var sResult = angular.copy($scope.accountStatement.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.accountStatement.selectedAccountStatementIndex = i;
            break;
          }
        }
      };


      $scope.accountStatementSelected = accountStatementSelected;

      var _params = {
        searchUrl: "/api/reports/getAccountStatementDocuments/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _accountStatement = {
        formName: 'accountStatementFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedAccountStatementIndex: false,
        accountStatementSelected: accountStatementSelected,
        accountStatementDetails: false
      };

      $scope.accountStatement = angular.merge($scope.searchConfig.accountStatement, _accountStatement);

      var findAccountStatement = function (e) {

        //$scope.person.accountStatementDetails = false;

        if ($scope.accountStatement.searchResultGrid !== undefined) {
          $scope.accountStatement.searchResultGrid.refresh();
        }

        $scope.accountStatement.selectedAccountStatementIndex = false;

        if ($scope.accountStatement.formName.$dirty) {
          $scope.accountStatement.formName.$submitted = true;
        }

        if ($scope.accountStatement.formName.$valid || !$scope.accountStatement.formName.$dirty) {

          $scope.accountStatement.searchResult.isEmpty = false;

          var formData = angular.copy($scope.accountStatement.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          formData.creationDateStart = $scope.accountStatement.form.creationDateStart ? $scope.accountStatement.form.creationDateStartObj : null;
          formData.creationDateFinish = $scope.accountStatement.form.creationDateFinish ? $scope.accountStatement.form.creationDateFinishObj : null;
          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data) {
                $scope.accountStatement.searchResult.data = data;
              } else {
                $scope.accountStatement.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total, schema: {
                    model: {
                      fields: {
                        creationDate: {type: "date"},
                        valueDate: {type: "date"}
                      }
                    }
                  }
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };


      $scope.accountStatement.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                creationDate: {type: "date"},
              }
            }
          },
          transport: {
            read: function (e) {
              findAccountStatement(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "personName",
            title: gettextCatalog.getString('Person'),
            width: '10rem'
          },
          {
            field: "accountNumber",
            title: gettextCatalog.getString("Account Number"),
            width: '10rem'
          },
          {
            field: "documentNumber",
            title: gettextCatalog.getString("Document Number"),
            width: '10rem'
          },
          {
            field: "creationDate",
            title: gettextCatalog.getString("Creation Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "issuerName",
            title: 'Emitent',
            width: '10rem',
            hidden: $scope.hideInstrumentsColumns
          },
          {
            field: "ISIN",
            title: 'ISIN',
            width: '10rem',
            hidden: $scope.hideInstrumentsColumns
          }
        ]
      };


      $scope.accountStatementSelected = accountStatementSelected;


      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.accountStatement.form.accountNumber = null;
        $scope.accountStatement.form.documentNumber = null;
        $scope.accountStatement.form.creationDateStart = null;
        $scope.accountStatement.form.creationDateFinish = null;
        $scope.accountStatement.form.personName = null;
        $scope.accountStatement.form.documentClassCode = null;

      };


      $scope.findAccountStatement = function () {

        var formValidationResult = validateForm(angular.copy($scope.accountStatement.form));
        if (formValidationResult.success) {
          if ($scope.accountStatement.searchResultGrid) {
            $scope.accountStatement.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.accountStatement.searchResultGrid) {
          $scope.accountStatement.searchResultGrid = widget;
        }
      });

      //$scope.detailGrid = function (dataItem) {
      //
      //  return {
      //    dataSource: {
      //      data: dataItem.subjects,
      //      pageSize: 5
      //    },
      //    scrollable: true,
      //    pageable: true,
      //    columns: [
      //      {
      //        field: "instrument.instrumentName",
      //        title: gettextCatalog.getString("Instrument Name"),
      //        width: "10rem"
      //      },
      //      {
      //        field: "instrument.ISIN",
      //        title: gettextCatalog.getString("ISIN"),
      //        width: "10rem"
      //      },
      //      {field: "instrument.issuerName", title: gettextCatalog.getString('Issuer'), width: "10rem"},
      //      {field: "quantity", title: gettextCatalog.getString('Quantity'), width: "10rem"}
      //
      //    ]
      //  };
      //};
    }]);
