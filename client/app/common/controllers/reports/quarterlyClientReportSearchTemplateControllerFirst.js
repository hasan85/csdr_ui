'use strict';

angular.module('cmisApp')
  .controller('QuarterlyClientReportSearchTemplateControllerFirst',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        var findData = function () {

          if ($scope.trade.formName.$valid || !$scope.trade.formName.$dirty) {

            $scope.trade.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.trade.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {


              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }



              var currentMonth=(new Date()).getMonth();
              var yyyy =(new Date()).getFullYear();
              var start = (Math.floor(currentMonth/3)*3)+1,
                finish = start+3,
               // startDate = new Date(start+'-01-'+ yyyy),
                finishDate = finish>12?new Date('01-01-'+ (yyyy+1)):new Date(finish+'-01-'+ (yyyy));
              finishDate = new Date((finishDate.getTime())-1);

              console.log('finishDate =', finishDate);


              var lastDateWithSlashes = (finishDate.getDate()) + '-' + (finishDate.getMonth() + 1) + '-' + finishDate.getFullYear();


              if (!$scope.trade.form.finishDateObj) {
                $scope.trade.form.finishDateObj = finishDate;
                $scope.trade.form.finishDate = lastDateWithSlashes;
              }


              formData.finishDate = $scope.trade.form.finishDateObj;


              var requestData = angular.extend(formData, {

              });

              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                // data.data = helperFunctionsService.convertObjectToArray(data.data);
                $scope.myData = data.data

                if (data) {
                  $scope.trade.searchResult.data = data;

                } else {
                  $scope.trade.searchResult.isEmpty = true;
                }


                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };

        $scope.params = angular.merge($scope.searchConfig.params);

        var _trade = {
          formName: 'tradeFindForm',
          form: {},
          data: {},
          searchResult: {
            data: null,
            isEmpty: false
          },
          findData: findData
        };

        $scope.trade = angular.merge($scope.searchConfig.trade, _trade);


        findData();

        $scope.refreshData = function () {
          findData();
        };


        $scope.printData = function (elementID) {
          Loader.show(true);
          
          var css = '<style>table{border-collapse: collapse;font-size: 20px;}table, td, th {border: 1px solid #999;}</style>';

          var divToPrint = document.getElementById(elementID);
          var newWin = window.open("");
          newWin.document.write(css + divToPrint.outerHTML);


          setTimeout(function() {
            newWin.print();
            newWin.close();
            Loader.show(false);
          }, 100);
        };

      }]);
