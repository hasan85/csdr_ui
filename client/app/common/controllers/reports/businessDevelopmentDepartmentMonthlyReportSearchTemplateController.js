'use strict';

angular.module('cmisApp')
  .controller('BusinessDevelopmentDepartmentMonthlyReportSearchTemplateController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

        var findData = function () {

          if ($scope.trade.formName.$valid || !$scope.trade.formName.$dirty) {

            $scope.trade.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.trade.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
              formValidationResult.success = true;
            } else {
              formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {


              if ($scope.params.config.searchCriteria) {
                formData = angular.merge(formData, $scope.params.config.searchCriteria);
              }


              var date = new Date();
              var startDate = new Date(date.getFullYear(), date.getMonth(), 1);
              var finishDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

              var lastDateWithSlashes = (finishDate.getDate()) + '-' + (finishDate.getMonth() + 1) + '-' + finishDate.getFullYear();
              var firstDateWithSlashes = (startDate.getDate()) + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();

              if (!$scope.trade.form.startDateObj) {
                $scope.trade.form.startDateObj = startDate;
                $scope.trade.form.startDate = firstDateWithSlashes;
              }
              if (!$scope.trade.form.finishDateObj) {
                $scope.trade.form.finishDateObj = finishDate;
                $scope.trade.form.finishDate = lastDateWithSlashes;
              }


              formData.startDate = $scope.trade.form.startDateObj;
              formData.finishDate = $scope.trade.form.finishDateObj;


              var requestData = angular.extend(formData, {

              });

              Loader.show(true);
              $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
              success(function (data) {

                //data.data = helperFunctionsService.convertObjectToArray(data.data);
                $scope.myData = data.data;

                if($scope.myData.ndcHoldingSecurityData){
                    $scope.myData.ndcHoldingSecurityData.noteSizes = angular.isObject($scope.myData.ndcHoldingSecurityData.noteSizes) ? [$scope.myData.ndcHoldingSecurityData.noteSizes]: $scope.myData.ndcHoldingSecurityData.noteSizes
                }

                console.log($scope.myData)

                if (data) {
                  $scope.trade.searchResult.data = data;

                } else {
                  $scope.trade.searchResult.isEmpty = true;
                }


                Loader.show(false);
              });


            } else {
              Loader.show(false);
              SweetAlert.swal("", formValidationResult.message, "error");
            }
          }
        };

        $scope.params = angular.merge($scope.searchConfig.params);

        var _trade = {
          formName: 'tradeFindForm',
          form: {},
          data: {},
          searchResult: {
            data: null,
            isEmpty: false
          },
          findData: findData
        };

        $scope.trade = angular.merge($scope.searchConfig.trade, _trade);


        findData();

        $scope.refreshData = function () {
          findData();
        };

        $scope.printData = function (elementID) {
          $scope.printClicked = true;
          Loader.show(true);

          var css = '<style>table{border-collapse: collapse;font-size: 20px;}table, td, th {border: 1px solid #999;}</style>';

          var divToPrint = document.getElementById(elementID);
          var newWin = window.open("");
          newWin.document.write(css + divToPrint.outerHTML);

          setTimeout(function() {
            newWin.print();
            newWin.close();
            $scope.printClicked = false;
            Loader.show(false);
          }, 100);
        };



      }]);
