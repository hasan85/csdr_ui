'use strict';

angular.module('cmisApp')
  .controller('LocalIssuersForeignTradersForBondSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        result.success = true;

        return result;
      };

      var freezingSelected = function (data) {
        var sResult = angular.copy($scope.freezing.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.freezing.selectedFreezingIndex = i;
            break;
          }
        }
      };


      $scope.freezingSelected = freezingSelected;

      var _params = {
        searchUrl: "/api/reports/getLocalIssuersForeignTradersForBond/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      var _freezing = {
        formName: 'freezingFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedFreezingIndex: false,
        freezingSelected: freezingSelected,
        freezingDetails: false
      };

      $scope.freezing = angular.merge($scope.searchConfig.freezing, _freezing);

      var findFreezing = function (e) {

        //$scope.person.freezingDetails = false;

        if ($scope.freezing.searchResultGrid !== undefined) {
          $scope.freezing.searchResultGrid.refresh();
        }

        $scope.freezing.selectedFreezingIndex = false;

        if ($scope.freezing.formName.$dirty) {
          $scope.freezing.formName.$submitted = true;
        }

        if ($scope.freezing.formName.$valid || !$scope.freezing.formName.$dirty) {

          $scope.freezing.searchResult.isEmpty = false;

          var formData = angular.copy($scope.freezing.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }


          var currentMonth=(new Date()).getMonth();
          var yyyy =(new Date()).getFullYear();
          var start = (Math.floor(currentMonth/3)*3)+1,
            finish = start+3,
            startDate = new Date(start+'-01-'+ yyyy),
            finishDate = finish>12?new Date('01-01-'+ (yyyy+1)):new Date(finish+'-01-'+ (yyyy));
          finishDate = new Date((finishDate.getTime())-1);

          console.log('startDate =', startDate,'finishDate =', finishDate);


          var lastDateWithSlashes = (finishDate.getDate()) + '-' + (finishDate.getMonth() + 1) + '-' + finishDate.getFullYear();
          var firstDateWithSlashes = (startDate.getDate()) + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();



          if (!$scope.freezing.form.startDateObj) {
            $scope.freezing.form.startDateObj = startDate;
            $scope.freezing.form.startDate = firstDateWithSlashes;
          }
          if (!$scope.freezing.form.finishDateObj) {
            $scope.freezing.form.finishDateObj = finishDate;
            $scope.freezing.form.finishDate = lastDateWithSlashes;
          }


          formData.startDate = $scope.freezing.form.startDateObj;
          formData.finishDate = $scope.freezing.form.finishDateObj;


          var requestData = angular.extend(formData, {

          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.freezing.searchResult.data = data;
              } else {
                $scope.freezing.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };


      $scope.freezing.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                maturityDate: {type: "date"},
                valueDate: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findFreezing(e);
            }
          },

          serverPaging: false,
          serverSorting: false
        },
        selectable: true,
        scrollable: true,
        pageable: {
          input: false,
          numeric: true
        },
        // pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "recordNumber",
            title: gettextCatalog.getString("Trade Number"),
            width: '10rem'
          },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Trade Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "issuer",
            title: gettextCatalog.getString("Issuer"),
            width: '10rem'
          },
          {
            field: "idNumber",
            title: gettextCatalog.getString("ID Number"),
            width: '10rem'
          },
          {
            field: "cfiCode",
            title: gettextCatalog.getString("CFI Code"),
            width: '10rem'
          },
          {
            field: "currency",
            title: gettextCatalog.getString("Currency"),
            width: '10rem'
          },
          {
            field: "maturityDate",
            title: gettextCatalog.getString("Maturity Date"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "parValue",
            title: gettextCatalog.getString("Par Value"),
            width: '10rem'
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: '10rem',

          },
          {
            field: "nonResidentCountry",
            title: gettextCatalog.getString("Non Resident Country"),
            width: '10rem'
          },
          {
            field: "couponRate",
            title: gettextCatalog.getString("Coupon Rate"),
            width: '10rem',

          },
          {
            field: "sellerCountry",
            title: gettextCatalog.getString("Seller Country"),
            width: '10rem'
          },
          {
            field: "startRemainder",
            title: gettextCatalog.getString("Start Remainder"),
            width: '10rem'
          },
          {
            title: gettextCatalog.getString("Operations"),
            columns: [
              {
                field: "sellerRecordQuantity",
                title: gettextCatalog.getString("Buyer / Seller Record Quantity"),
                width: '10rem'
              },
              {
                field: "sellerRecordPrice",
                title: gettextCatalog.getString("Seller Record Price"),
                width: '10rem'

              },

              {
                field: "buyerRecordPrice",
                title: gettextCatalog.getString("Buyer Record Price"),
                width: '10rem'
              }
            ]
          },
          {
            field: "finishRemainder",
            title: gettextCatalog.getString("Finish Remainder"),
            width: '10rem'
          },
          {
            field: "oppositeNonResidentCountry",
            title: gettextCatalog.getString("Opposite Non Resident Country"),
            width: '10rem'
          }
        ]
      };


      $scope.freezingSelected = freezingSelected;

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };


      $scope.findFreezing = function () {

        var formValidationResult = validateForm(angular.copy($scope.freezing.form));
        if (formValidationResult.success) {
          if ($scope.freezing.searchResultGrid) {
            $scope.freezing.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.freezing.searchResultGrid) {
          $scope.freezing.searchResultGrid = widget;
        }
      });

    }]);

