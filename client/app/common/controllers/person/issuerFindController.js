'use strict';

angular.module('cmisApp')
  .controller('IssuerFindController', ['$scope', '$windowInstance', 'personFindService', 'searchUrl', 'configParams',
    function ($scope, $windowInstance, personFindService, searchUrl, configParams) {

      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false
        },
        params: {
          searchUrl: searchUrl ? searchUrl : '/api/persons/getIssuers',
          showSearchCriteriaBlock: true,
          config: configParams
        }
      };

      $scope.selectPerson = function () {
        personFindService.selectPerson($scope.searchConfig.person.searchResult.data.data[$scope.searchConfig.person.selectedPersonIndex]);

        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
