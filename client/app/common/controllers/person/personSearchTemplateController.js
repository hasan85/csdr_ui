'use strict';

angular.module('cmisApp')
  .controller('PersonSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService) {

      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        result.success = true;
        // }
        return result;
      };

      var personSelected = function (data) {

        var sResult = angular.copy($scope.person.searchResult.data.data);

        for (var i = 0; i < sResult.length; i++) {

          if (sResult[i].id == data.id) {
            $scope.person.selectedPersonIndex = i;
            break;
          }
        }
      };

      $scope.personSelected = personSelected;

      var _params = {
        searchUrl: "/api/persons/getPersons",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {
            personType: {
              isVisible: true,
              default: appConstants.personClasses.juridicalPerson
            },
            accountNumber: {
              isVisible: false
            }
          },
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);


      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _person = {
        formName: 'personFindForm',
        form: {},
        data: {
          personClasses: [],
          personClassCodes: {
            natural_person: appConstants.personClasses.naturalPerson,
            juridical_person: appConstants.personClasses.juridicalPerson
          }
        },

        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedPersonType: $scope.params.config.criteriaForm.personType.default,
        selectedPersonIndex: false,
        personSelected: personSelected,
        personDetails: false
      };

      $scope.person = angular.merge($scope.searchConfig.person, _person);

      var findPerson = function (e) {

        $scope.person.personDetails = false;

        if ($scope.person.searchResultGrid !== undefined) {
          $scope.person.searchResultGrid.refresh();
        }

        $scope.person.selectedPersonIndex = false;

        if ($scope.person.formName.$dirty) {
          $scope.person.formName.$submitted = true;
        }

        if ($scope.person.formName.$valid || !$scope.person.formName.$dirty) {

          $scope.person.searchResult.isEmpty = false;

          var formData = angular.copy($scope.person.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {
            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('asd',$scope.params.config.criteriaForm);
          console.log('Request Data', requestData);

          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {
              if (data) {
                $scope.person.searchResult.data = data;
              } else {
                $scope.person.searchResult.isEmpty = true;
              }

              if (data['success'] === "true") {
                data.data = helperFunctionsService.convertObjectToArray(data.data);

                e.success({Data: data.data ? data.data : [], Total: data.total});
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };

      Loader.show(true);

      personFindService.getMetaDataForPersonSearch().then(function (data) {

        $scope.person.data.personClasses = data.personClasses;
        $scope.person.data.naturalPersonIdDocumentTypes = data.naturalPersonIdDocumentTypes;
        $scope.person.data.juridicalPersonIdDocumentTypes = data.juridicalPersonIdDocumentTypes;

        for (var i = 0; i < data.personClasses.length; i++) {
          if (data.personClasses[i].code == $scope.params.config.criteriaForm.personType.default) {
            $scope.person.form.personClassId = data.personClasses[i].id;
            $scope.person.selectedPersonType = data.personClasses[i].code;
          }
        }

        if (!$scope.person.selectedPersonTypee) {
          $scope.person.selectedPersonTyp = data.personClasses[0].code;
        }

        referenceDataService.addEmptyOption([
          $scope.person.data.naturalPersonIdDocumentTypes,
          $scope.person.data.juridicalPersonIdDocumentTypes,
          // $scope.person.data.accountRoles
        ]);


        $scope.person.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total"
            },
            transport: {
              read: function (e) {
                findPerson(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: $scope.params.config.criteriaForm.accountNumber.isVisible ?
            [
              {
                field: "name",
                title: gettextCatalog.getString("Name"),
                width: '30%'
              },
              {
                field: "idDocument",
                title: gettextCatalog.getString("ID Document"),
                width: '20%'
              },
              {
                field: "addressLine",
                title: gettextCatalog.getString("Address"),
                width: '30%'
              },
              {
                field: "uniqueCode",
                title: gettextCatalog.getString("Unique ID"),
                width: '20%'
              },

              {
                field: "accountNumber",
                title: gettextCatalog.getString("Account Number"),
                width: '20%'
              },
              {
                field: "azipsCode",
                title: gettextCatalog.getString("AZIPS Code"),
                width: '20%'
              }

            ] : [
            {
              field: "name",
              title: gettextCatalog.getString("Name"),
              width: '30%'
            },
            {
              field: "idDocument",
              title: gettextCatalog.getString("ID Document"),
              width: '20%'
            },
            {
              field: "addressLine",
              title: gettextCatalog.getString("Address"),
              width: '30%'
            },
            {
              field: "uniqueCode",
              title: gettextCatalog.getString("Unique ID"),
              width: '20%'
            }
          ]
        };


        Loader.show(false);
      });

      $scope.personSelected = personSelected;

      $scope.showPersonDetails = function () {

        Loader.show(true);
        personFindService.findPersonById($scope.searchConfig.person.searchResult.data.data[
          $scope.searchConfig.person.selectedPersonIndex]['id']).then(function (data) {
          $scope.person.personDetails = data;
          Loader.show(false);
        });

      };

      $scope.closePersonDetails = function () {
        $scope.person.personDetails = false;
      };

      $scope.changePersonType = function () {

        var personClasses = angular.copy($scope.person.data.personClasses);
        for (var i = 0; i < personClasses.length; i++) {
          if (personClasses[i].id == $scope.person.form.personClassId) {
            $scope.person.selectedPersonType = personClasses[i].code;
          }
        }
        $scope.resetForm();
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {

        $scope.person.form.name = null;
        $scope.person.form.fullName = null;
        $scope.person.form.firstName = null;
        $scope.person.form.uniqueCode = null;
        $scope.person.form.middleName = null;
        $scope.person.form.lastName = null;
        $scope.person.form.plainAddress = null;
        $scope.person.form.accountNumber = null;
        $scope.person.form.idDocumentSeries = null;
        $scope.person.form.idDocumentClass = "";
        $scope.person.form.idDocumentNumber = null;
        $scope.person.form.isSuspended = false;
        $scope.person.form.isDeleted = false;

      };

      $scope.detailInit = function (e) {
        Loader.show(true);
        personFindService.getPersonDetails(e.data.id).then(function (res) {

          Loader.show(false);
          if (res && res.success === "true") {
            e.data.details = res.data;
          } else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
          }
        });
      };

      $scope.findPerson = function () {

        var formValidationResult = validateForm(angular.copy($scope.person.form));
        if (formValidationResult.success) {
          if ($scope.person.searchResultGrid) {
            $scope.person.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.person.searchResultGrid) {
          $scope.person.searchResultGrid = widget;
        }
      });
    }]);
