'use strict';

angular.module('cmisApp')
  .controller('PersonFindController', ['$scope', '$windowInstance', 'personFindService', 'searchUrl', 'configParams',
    function ($scope, $windowInstance, personFindService, searchUrl, configParams) {

      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false
        },
        params: {
          searchUrl: searchUrl ? searchUrl : '/api/persons/getShareHolders',
          showSearchCriteriaBlock: true,
          config: configParams
        }
      };

      $scope.selectPerson = function (grid) {
        var selectedData = grid.dataItem(grid.select());
        personFindService.selectPerson(selectedData);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
