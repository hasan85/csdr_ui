'use strict';

angular.module('cmisApp')
  .controller('DeponentFindController', ['$scope', '$windowInstance', 'personFindService', 'searchUrl', 'configParams',
    function ($scope, $windowInstance, personFindService, searchUrl, configParams) {

      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false
        },
        params: {
          searchUrl: searchUrl ? searchUrl : '/api/reportsDataServices/getDeponents',
          showSearchCriteriaBlock: true,
          config: configParams
        }
      };

      $scope.selectPerson = function (grid) {
        var selectedData = grid.dataItem(grid.select());
        $windowInstance.close(selectedData);
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
