'use strict';

angular.module('cmisApp')
  .controller('PaymentSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog',
    function ($scope, personFindService, Loader, gettextCatalog) {


      var personSelected = function (data) {

        var sResult = angular.copy($scope.temenosPayments);

        for (var i = 0; i < sResult.length; i++) {

          if (sResult[i].paymentID == data.paymentID) {

            $scope.payment.selectedPaymentIndex = i;
            // $scope.payment.isMatched = true;
            // console.log($scope.payment.isMatched);
            break;
          }

        }


      };


      $scope.personSelected = personSelected;

        $scope.payment.mainGridOptions = {
          dataBound: function () {
            var rows = this.tbody.children();
            var dataItems = this.dataSource.view();
            for (var i = 0; i < dataItems.length; i++) {
              kendo.bind(rows[i], dataItems[i]);
            }
          },
          dataSource: {
            data: $scope.temenosPayments.slice().filter(function(item) {
              return item.isMatched != true;
            })
          },
          selectable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          personSelected: personSelected,
          columns: [
            {
              field: "paymentDate",
              title: gettextCatalog.getString("paymentDate"),
              width: '10rem',
              type: "date",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "destination",
              title: gettextCatalog.getString("Destination"),
              width: '8rem'
            },
            {
              field: "amount",
              title: gettextCatalog.getString("Amount"),
              width: '8rem'
            },
            {
              field: "currencyCode",
              title: gettextCatalog.getString("Currency"),
              width: '8rem'
            }
          ]
        };

        Loader.show(false);

        $scope.personSelected = personSelected;

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.payment.searchResultGrid) {
          $scope.payment.searchResultGrid = widget;
        }
      });

    }]);
