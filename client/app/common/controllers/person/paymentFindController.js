'use strict';

angular.module('cmisApp')
  .controller('PaymentFindController', ['$scope', '$windowInstance', 'personFindService', 'data',
    function ($scope, $windowInstance, personFindService,  data) {


        $scope.payment = {
            data: data,
            selectedPaymentIndex: false,
            //isMatched: false
        };

       $scope.temenosPayments = data.temenosPayments;

      $scope.selectPerson = function () {
        personFindService.selectPerson($scope.payment.data.temenosPayments[$scope.payment.selectedPaymentIndex]);

        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
