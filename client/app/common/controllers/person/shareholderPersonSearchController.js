'use strict';

angular.module('cmisApp')
  .controller('ShareholderPersonSearchController', ['$scope', '$windowInstance', 'personFindService', 'searchUrl', 'personForm',
    function ($scope, $windowInstance, personFindService, searchUrl, personForm) {
      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false,
          form: personForm
        },
        params: {
          searchUrl: searchUrl ? searchUrl : '/api/getShareholderPersons/',
          showSearchCriteriaBlock: true
        }
      };

      $scope.selectPerson = function (grid) {
        var selectedData = grid.dataItem(grid.select());
        $windowInstance.close(selectedData);
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
