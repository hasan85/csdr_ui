'use strict';

angular.module('cmisApp')
  .controller('ShareholderAccountsWithOperatorNameFindController', ['$scope', '$windowInstance', 'personFindService', 'searchUrl', 'configParams',
    function ($scope, $windowInstance, personFindService, searchUrl, configParams) {

      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false,
          selectedPersonData: null
        },
        params: {
          searchUrl: searchUrl ? searchUrl : '/api/persons/getShareholderAccountsWithOperatorName',
          showSearchCriteriaBlock: true,
          config: configParams
        }
      };

      $scope.selectPerson = function () {
        console.log($scope.searchConfig.person.selectedPersonData);
        personFindService.selectPerson($scope.searchConfig.person.selectedPersonData);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
