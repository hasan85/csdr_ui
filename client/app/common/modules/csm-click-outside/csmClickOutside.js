(function () {
  'use strict';

  angular
    .module('angular-click-outside', [])
    .directive('clickOutside', ['$document', '$parse', clickOutside]);

  function clickOutside($document, $parse) {
    return {

      require: '?ngModel',
      scope: { clickOutside: '&' },
      link: function postLink(scope, element, attrs) {

        var fn = $parse(attrs['clickOutside']);

        $document.bind('click', function (event) {
          var isClickedElementChildOfPopup = element
              .find(event.target)
              .length > 0;

          if (isClickedElementChildOfPopup)
            return;

          //scope.params.showSearchCriteriaBlock = false;
          ////scope.isPopupVisible = false;
         // scope.toggleSearchCriteriaBlock
         // scope.$apply();

          scope.clickOutside();
        //  scope.$apply();
         // console.log('geeting here');
         // fn();
          //return scope.$apply(function () {
          //  return fn(scope);
          //});
        });

        //var onClick = function (event) {
        //  var isChild = $(element).has(event.target).length > 0;
        //  var isSelf = element[0] == event.target;
        //  var isInside = isChild || isSelf;
        //  if (!isInside) {
        //    scope.$apply(attrs.clickOutside)
        //  }
        //}
        //scope.$watch(attrs.isActive, function (newValue, oldValue) {
        //  if (newValue !== oldValue && newValue == true) {
        //  $document.bind('click', onClick);
        //}
        //else if (newValue !== oldValue && newValue == false) {
        //  $document.unbind('click', onClick);
        //}
        //   });
      }
    };
  }
})();
