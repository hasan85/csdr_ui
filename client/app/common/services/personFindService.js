'use strict';

angular.module('cmisApp')
  .factory('personFindService',
    ["$http", "$q", "$kWindow", 'gettextCatalog', 'helperFunctionsService', 'appConstants', 'referenceDataService', "Loader",
      function ($http, $q, $kWindow, gettextCatalog, helperFunctionsService, appConstants, referenceDataService, Loader) {

        var deferred = $q.defer();
        var normalizePersonArrayFields = function (personData) {
          if (personData.otherDocuments) {
            personData.otherDocuments = helperFunctionsService.convertObjectToArray(personData.otherDocuments);
          }
          if (personData.memberRoles) {
            personData.memberRoles = helperFunctionsService.convertObjectToArray(personData.memberRoles);
          }
          if (personData.bankAccounts) {
            personData.bankAccounts = helperFunctionsService.convertObjectToArray(personData.bankAccounts);
          }
          if (personData.phoneNumbers) {
            personData.phoneNumbers = helperFunctionsService.convertObjectToArray(personData.phoneNumbers);
          }
          if (personData.emails) {
            personData.emails = helperFunctionsService.convertObjectToArray(personData.emails);
          }
          if (personData.representatives) {

            personData.representatives = helperFunctionsService.convertObjectToArray(personData.representatives);
            for (var i = 0; i < personData.representatives.length; i++) {
              personData.representatives[i] = normalizePersonArrayFields(personData.representatives[i]);
            }
          }
          if (personData.signers) {
            personData.signers = helperFunctionsService.convertObjectToArray(personData.signers);
            for (var j = 0; j < personData.signers.length; j++) {
              personData.signers[j] = normalizePersonArrayFields(personData.signers[j]);
            }
          }
          if (personData.userData) {
            personData.userData = normalizePersonArrayFields(personData.userData);
          }

          return personData;
        };
        var normalizeAccountArrayFields = function (accountData) {
          accountData = helperFunctionsService.convertObjectToArray(accountData);
          if (accountData) {
            for (var i = 0; i < accountData.length; i++) {
              accountData[i].functionalAccounts = helperFunctionsService.convertObjectToArray(accountData[i].functionalAccounts);
              accountData[i].owners = helperFunctionsService.convertObjectToArray(accountData[i].owners);

            }
          }
          return accountData;
        };
        var findPersonByData = function (url, formData) {

          return $http.post(url, {data: formData}).then(function (result) {
            if (result.data) {
              result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
              if (result.data.data) {
                for (var i = 0; i < result.data.data.length; i++) {
                  result.data.data[i].roles = helperFunctionsService.convertObjectToArray(result.data.data[i].roles);
                }
              }
            }
            return result.data;
          });
        };
        var findAccountByData = function (url, formData) {

          return $http.post(
            url,
            {data: formData}
          ).then(function (result) {
            if (result.data && result.data.data) {
              result.data.data = normalizeAccountArrayFields(result.data.data);
            }

            return result.data;
          });
        };
        var checkAccountStatusByRole = function (criteria, roleClassCode) {

          return $http.post(
            '/api/persons/checkAccountStatusByRole/',
            {
              criteria: criteria,
              roleClassCode: roleClassCode
            }
          ).then(function (result) {
            return result.data;
          });
        };
        var findPersonById = function (id) {
          return $http.get('/api/persons/getPersonById/' + id).then(function (result) {
            var resp = {};
            if (result.data) {
              resp = normalizePersonArrayFields(result.data);
            }
            return resp;
          })
        };
        var getPersonDetails = function (personId) {
          return $http.get('/api/persons/getPersonDetails/' + personId).then(function (result) {
            if (result.data && result.data.data) {
              result.data.data.participantAccounts = helperFunctionsService.convertObjectToArray(result.data.data.participantAccounts);
              result.data.data.representativeAccounts = helperFunctionsService.convertObjectToArray(result.data.data.representativeAccounts);
              if (result.data.data.participantAccounts && result.data.data.participantAccounts.length > 0) {
                for (var i = 0; i < result.data.data.participantAccounts.length; i++) {
                  result.data.data.participantAccounts[i].functionalAccounts =
                    helperFunctionsService.convertObjectToArray(result.data.data.participantAccounts[i].functionalAccounts);
                }
              }
            }
            return result.data;
          })
        };
        var getFirstPersonByCriteria = function (criteria) {
          return $http.post('/api/persons/getFirstPersonByCriteria/', {criteria: criteria}).then(function (result) {

            if (result.data && result.data.data) {
              result.data.data = normalizePersonArrayFields(result.data.data);
            }
            return result.data;
          })
        };
        var findShareholderById = function (id) {
          return $http.get('/api/persons/getShareholder/' + id).then(function (result) {
            return result.data;
          })
        };
        var findClientAccount = function (brokerId, searchUrl, personClassCode) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Client'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/clearing/templates/partials/search-templates/client-account-find.html',
            controller: 'ClientAccountFindController',
            resolve: {
              id: function () {
                return 0;
              },
              brokerId: function () {
                return brokerId ? brokerId : null
              },
              searchUrl: function () {
                return searchUrl ? searchUrl : null
              },
              personClassCode: function () {
                return personClassCode ? personClassCode : null;
              }
            }
          });

          return deferred.promise;
        };

        var findClientShareholder = function (brokerId, searchUrl, personClassCode) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Client'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/clearing/templates/partials/search-templates/client-shareholder-find.html',
            controller: 'ClientShareholderFindController',
            resolve: {
              id: function () {
                return 0;
              },
              brokerId: function () {
                return brokerId ? brokerId : null
              },
              searchUrl: function () {
                return searchUrl ? searchUrl : null
              },
              personClassCode: function () {
                return personClassCode ? personClassCode : null;
              }
            }
          });

          return deferred.promise;
        };

        var findAllClientAccounts = function (brokerId, searchUrl, personClassCode) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Client'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/clearing/templates/partials/search-templates/all-client-find.html',
            controller: 'AllClientFindController',
            resolve: {
              id: function () {
                return 0;
              },
              brokerId: function () {
                return brokerId ? brokerId : null
              },
              searchUrl: function () {
                return searchUrl ? searchUrl : null
              },
              personClassCode: function () {
                return personClassCode ? personClassCode : null;
              }
            }
          });

          return deferred.promise;
        };

        var findMoneyAccounts = function (brokerId, searchUrl, personClassCode) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Client'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/clearing/templates/partials/search-templates/money-account-find.html',
            controller: 'MoneyAccountFindController',
            resolve: {
              id: function () {
                return 0;
              },
              brokerId: function () {
                return brokerId ? brokerId : null
              },
              searchUrl: function () {
                return searchUrl ? searchUrl : null
              },
              personClassCode: function () {
                return personClassCode ? personClassCode : null;
              }
            }
          });

          return deferred.promise;
        };
        var findPerson = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Person'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getPersons';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: true,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                };
              }
            }
          });
          return deferred.promise;
        };
        var findShareholder = function (config, criteriaForm) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Shareholder'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/shareholder-find.html',
            controller: 'ShareholderFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getShareHolders';
              },
              configParams: function () {
                return {
                  criteriaForm: criteriaForm ? criteriaForm : {
                    personType: {
                      isVisible: true,
                      default: appConstants.personClasses.juridicalPerson
                    },
                    accountNumber: {
                      isVisible: true
                    }
                  },
                  searchCriteria: config ? config.searchCriteria : {}
                };
              }
            }
          });
          return deferred.promise;
        };

        var getMemberClientAccounts = function (config, criteriaForm) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Person'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/view-member-client-accounts.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getMemberClientAccounts';
              },
              configParams: function () {
                return {
                  criteriaForm: criteriaForm ? criteriaForm : {
                    accountNumber: {
                      isVisible: true
                    }
                  },
                  searchCriteria: config ? config.searchCriteria : {}
                };
              }
            }
          });
          return deferred.promise;
        };


        var findShareholderAccountsWithOperatorName = function (config, criteriaForm) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Shareholder'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/shareholder-accounts-with-operator-name-find.html',
            controller: 'ShareholderAccountsWithOperatorNameFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getShareholderAccountsWithOperatorName';
              },
              configParams: function () {
                return {
                  criteriaForm: criteriaForm ? criteriaForm : {
                    accountNumber: {
                      isVisible: true
                    }
                  },
                  searchCriteria: config ? config.searchCriteria : {},
                  searchOnInit: config ? config.searchOnInit || false : false,
                  filterDataByAccountId: config ? config.filterDataByAccountId || null : null
                };
              }
            }
          });
          return deferred.promise;
        };

        var getAnotherShareholderAccount = function (config, criteriaForm) {
          deferred = $q.defer();
          $kWindow.open({

            title: gettextCatalog.getString('Find Shareholder'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/shareholder-accounts-with-operator-name-find.html',
            controller: 'ShareholderAccountsWithOperatorNameFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getAnotherShareholderAccount';
              },
              configParams: function () {
                return {
                  criteriaForm: criteriaForm ? criteriaForm : {
                    accountNumber: {
                      isVisible: true
                    }
                  },
                  searchCriteria: config ? config.searchCriteria : {},
                  searchOnInit: config ? config.searchOnInit || false : false,
                  filterDataByAccountId: config ? config.filterDataByAccountId || null : null
                };
              }
            }
          });
          return deferred.promise;
        };

        var findIssuer = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Issuer'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/issuer-find.html',
            controller: 'IssuerFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getIssuers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var searchShareholders = function (obj) {
          deferred = $q.defer();
          $kWindow.open({
            title: obj.title ? obj.title : gettextCatalog.getString('İnvestoru tap'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/issuer-find.html',
            controller: 'IssuerFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return obj.searchUrl;
              },
              configParams: function () {
                return {
                  params: obj.params ? obj.params : {},
                  criteriaForm: obj.criteriaForm ? obj.criteriaForm : {}
                }
              }
            }
          });
          return deferred.promise;
        };
        var findPayment = function (data) {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Payment'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/payment-find.html',
            controller: 'PaymentFindController',
            resolve: {
              // id: function () {
              //   return 0;
              // },
              data: function () {
                return data;
              }
            }
          });
          return deferred.promise;
        };

        var findUnknownPayment = function (data) {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Payment'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/records/templates/unknown-payment-records.html',
            controller: 'ViewUnknownPaymentRecordsController',
            resolve: {
              id: function () {
                return 0;
              },
              data: function () {
                return data;
              }
            }
          });
          return deferred.promise;
        };


        var searchTaxBalances = function (data) {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Tax Balance'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/records/templates/tax-balance.html',
            controller: 'TaxBalanceController',
            resolve: {
              id: function () {
                return 0;
              },
              data: function () {
                return data;
              }
            }
          });
          return deferred.promise;
        };

        var searchClearingMembersAndIssuers = function (data) {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Search clearing members and issuers'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/records/templates/clearing-member-and-issuers.html',
            controller: 'ClearingMembersAndIssuersController',
            resolve: {
              id: function () {
                return 0;
              },
              data: function () {
                return data;
              }
            }
          });
          return deferred.promise;
        };


        var findTaxExchanges = function (data) {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find tax exchange'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/records/templates/find-tax-exchange.html',
            controller: 'TaxExchangeController',
            resolve: {
              id: function () {
                return 0;
              },
              data: function () {
                return data;
              }
            }
          });
          return deferred.promise;
        };



        var findPledgedShares = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Pledge Shares'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/pledged-shares-find.html',
            controller: 'PledgedSharesFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getPledgedBalances';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: true,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findPledgee = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Pledgee'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getPledgees';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: true,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findShareholderOrIssuer = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Shareholder/Issuer'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getShareholdersOrIssuers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    //personType: {
                    //  isVisible: false,
                    //  default: appConstants.personClasses.juridicalPerson
                    //}
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findMember = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Member'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getMembers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findDepoMember = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Depo Member'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            // appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getDepoMembers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findTradingMember = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Trading Member'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            // appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getTradingMembers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };

        var searchTradingMembersAndSO = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Trading Member'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            // appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/searchTradingMembersAndSO';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findCompletedOperation = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: 'Bitirilmiş İşlər',
            actions: ['Close'],
            isNotTile: true,
            width: '100%',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/operation-find.html',
            controller: 'OperationFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/task-services/getAllFinishedTasks';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };

        var findBroker = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Broker'),
            actions: ['Close'],
            isNotTile: true,
            width: '100%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getBrokers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findSettlementAgent = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Settlement Agent'),
            actions: ['Close'],
            isNotTile: true,
            width: '100%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getSettlementAgents';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findTradingOrDepoMember = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Trading/DEPO Member'),
            actions: ['Close'],
            isNotTile: true,
            width: '100%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getTradingOrDepoMembers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findClearingMember = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Clearing Member'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            // appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getClearingMembers';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findIamasPerson = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: 'Şəxsin əlavə edilməsi',
            actions: [],
            isNotTile: true,
            width: '60%',
            // appendTo: '#home',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.operation,
            templateUrl: 'app/person-account/templates/iamas-add-person-template.html',
            controller: 'IamasAddPersonTemplateController',
            resolve: {
              // handle promise resolving
              id: function () {
                return 0;
              },
              operationState: function () { return 'findoperation' },
              task: function () { return {} }
            }
          });
          return deferred.promise;
        };
        var findSettlementAgent = function () {
          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Clearing Member'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            // appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getSettlementAgents';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          });
          return deferred.promise;
        };
        var findTaxExempt = function (searchCriteria, criteriaForm) {

          deferred = $q.defer();
          $kWindow.open({
            title: "ÖVM-dən azad edilən təşkilatların siyahısı",
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/accounts/templates/search-tax-exempt-accounts-find.html',
            controller: 'SearchTaxExemptAccountsFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/common/searchTaxExemptAccounts';
              },
              configParams: function () {
                return {
                  criteriaForm: criteriaForm ? criteriaForm : {},
                  searchCriteria: searchCriteria
                };
              }
            }
          });

          return deferred.promise;

        };
        var findAccount = function (searchCriteria, criteriaForm) {

          deferred = $q.defer();
          $kWindow.open({
            title: gettextCatalog.getString('Find Account'),
            actions: ['Close'],
            isNotTile: true,
            width: '95%',
            //appendTo: '#home',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/person-find.html',
            controller: 'PersonFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return '/api/persons/getPersons';
              },
              configParams: function () {
                return {
                  criteriaForm: criteriaForm ? criteriaForm : {
                    personType: {
                      isVisible: true,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  },
                  searchCriteria: searchCriteria
                };
              }
            }
          });

          return deferred.promise;
        };
        var getMetaDataForPersonSearch = function () {


          var deferred = $q.defer();
          var metaData = {};

          var getJuridicalPersonIdDocumentTypes = function (value) {
            metaData.juridicalPersonIdDocumentTypes = value;
          };
          var getNaturalPersonIdDocumentTypes = function (value) {
            metaData.naturalPersonIdDocumentTypes = value;
          };
          var personClasses = function (value) {
            metaData.personClasses = value;
          };
          var accountRoles = function (value) {
            metaData.accountRoles = value;
          };

          $q.all([

            referenceDataService.getJuridicalPersonIdDocumentTypes().then(getJuridicalPersonIdDocumentTypes),
            referenceDataService.getNaturalPersonIdDocumentTypes().then(getNaturalPersonIdDocumentTypes),
            referenceDataService.getPersonClasses().then(personClasses),
            //referenceDataService.getAccountRoles().then(accountRoles),
          ])
            .then(function () {
              deferred.resolve(metaData);
            });

          return deferred.promise;
        };
        var resolveAccountStatusTextClass = function (e) {

          var txtClass = "";
          if (e.isSuspended == "true") {
            txtClass = "text-warning";
          }
          if (e.isDeleted == "true") {
            txtClass = "text-danger";
          }
          return txtClass;
        };

        var PersonalCardbyId = function (id) {
          Loader.show(true);
          return $http.post("api/records/PersonalCardbyId/", {
            SerialId: id.id,
            pinCode: id.pinCode,
            isDMX: id.isDMX
          }).then(function (result) {
            Loader.show(false);
            return result.data;
          });
        };






        var selectPerson = function (personData) {
          // Select person, resolve promise
          deferred.resolve(personData);
          deferred = $q.defer();
        };

        var endOperation = function (taskData) {
          deferred.resolve(taskData);
          deferred = $q.defer();
        };

        return {
          findPerson: findPerson,
          findPersonByData: findPersonByData,
          selectPerson: selectPerson,
          findPersonById: findPersonById,
          findShareholderById: findShareholderById,
          findIamasPerson: findIamasPerson,
          findShareholder: findShareholder,
          getMemberClientAccounts: getMemberClientAccounts,
          findShareholderAccountsWithOperatorName: findShareholderAccountsWithOperatorName,
          findMember: findMember,
          findDepoMember: findDepoMember,
          findTradingMember: findTradingMember,
          searchTradingMembersAndSO: searchTradingMembersAndSO,
          findClearingMember: findClearingMember,
          findSettlementAgent: findSettlementAgent,
          findBroker: findBroker,
          findIssuer: findIssuer,
          findPayment: findPayment,
          findAccount: findAccount,
          findShareholderOrIssuer: findShareholderOrIssuer,
          getMetaDataForPersonSearch: getMetaDataForPersonSearch,
          checkAccountStatusByRole: checkAccountStatusByRole,
          normalizePersonArrayFields: normalizePersonArrayFields,
          getFirstPersonByCriteria: getFirstPersonByCriteria,
          getPersonDetails: getPersonDetails,
          findAccountByData: findAccountByData,
          resolveAccountStatusTextClass: resolveAccountStatusTextClass,
          findTradingOrDepoMember: findTradingOrDepoMember,
          findPledgee: findPledgee,
          findPledgedShares: findPledgedShares,
          normalizeAccountArrayFields: normalizeAccountArrayFields,
          findClientAccount: findClientAccount,
          findClientShareholder:findClientShareholder,
          findAllClientAccounts:findAllClientAccounts,
          findMoneyAccounts:findMoneyAccounts,
          findUnknownPayment: findUnknownPayment,
          searchTaxBalances: searchTaxBalances,
          searchClearingMembersAndIssuers: searchClearingMembersAndIssuers,
          findTaxExchanges: findTaxExchanges,
          getAnotherShareholderAccount: getAnotherShareholderAccount,
          findCompletedOperation: findCompletedOperation,
          PersonalCardbyId: PersonalCardbyId,
          findTaxExempt: findTaxExempt,
          searchShareholders: searchShareholders

        };

      }]);
