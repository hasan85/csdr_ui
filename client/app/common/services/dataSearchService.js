'use strict';

angular.module('cmisApp')
  .factory('dataSearchService', ["$http", function ($http) {

    var apiUrlPartial = "/api/common/";


    var getAccountInfo = function (number) {
      return $http.get(apiUrlPartial + "getAccountInfo/" + number).then(function (result) {
        return result.data;
      })
    };

    var getAccountPrescription = function (depoAccountId, personId) {
      return $http.get(apiUrlPartial + "getAccountPrescription/" + depoAccountId + "/" + personId).then(function (result) {
        return result.data;
      })
    };

    return {
      getAccountInfo: getAccountInfo,
      getAccountPrescription:getAccountPrescription
    };

  }]);
