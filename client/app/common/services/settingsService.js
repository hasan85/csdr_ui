'use strict';

angular.module('cmisApp')
  .factory('settingsService', ["$http",'gettext', function ($http,gettext) {

    var settings = {
      "languages": [
        {'code': 'az', 'value': gettext("Azerbaijan")},
        {'code': 'en', 'value': gettext("English")}
      ],

      "search": {
        "options": [
          {'id': 1, 'value': gettext("Search Option1")},
          {'id': 2, 'value': gettext("Search Option2")}
        ]
      },
      "tiles": {
        "organizationOptions": [

          {'id': 1, 'value': gettext("Option Value1")},
          {'id': 2, 'value':  gettext("Option Value2")},
          {'id': 3, 'value': gettext("Option Value3")}
        ]
      }
    };

    return {
      getSettings: settings
    };

  }]);
