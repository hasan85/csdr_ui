'use strict';

angular.module('cmisApp')
  .factory('operationService', ['$http', 'Loader', '$kWindow', 'appConstants', 'SweetAlert', '$rootScope', 'helperFunctionsService',
    function ($http, Loader, $kWindow, appConstants, SweetAlert, $rootScope, helperFunctionsService) {
      var apiTaskServicesUrl = "/api/task-services/";
      var startProcess = function (key) {
        Loader.show(true);

        return $http.get(apiTaskServicesUrl + "startProcess/?taskKey=" + key).then(function successCallback(result) {

          if (result.data && result.data.success === "false") {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(result.data), 'error');
          } else {
            return createOperationData(result.data.data);
          }
          Loader.show(false);
        }, function errorCallback(err) {
          Loader.show(false);
        });
      };
      var startProcessByMessage = function (key, processInstanceId) {
        Loader.show(true);

        return $http.post(apiTaskServicesUrl + "startProcessByMessage/?messageName=" + key + "&senderId" + processInstanceId).then(function (result) {
          Loader.show(false);
          var data = createOperationData(result.data);
          return data;
        })
      };
      var getTask = function (id) {
        Loader.show(true);
        return $http.get(apiTaskServicesUrl + "openTask/?taskId=" + id).then(function (result) {
          Loader.show(false);
          var data = createOperationData(result.data);
          return data;
        })
      };
      var openFinishedTask = function (id) {
        Loader.show(true);
        return $http.get(apiTaskServicesUrl + "openFinishedTask/" + id).then(function (result) {
          console.log(result);
          Loader.show(false);
          var data = createOperationData(result.data);
          return data;
        })
      };
      var claimTask = function (id) {
        Loader.show(true);
        return $http.get(apiTaskServicesUrl + "claimTask/?taskId=" + id).then(function (result) {
          Loader.show(false);
          return result.data;
        })
      };
      var sendTaskComment = function (message) {
        Loader.show(true);
        return $http.post(apiTaskServicesUrl + "sendTaskComment/",
          {data: message}).then(function (result) {
            Loader.show(false);
            return result.data;
          })
      };
      var getMyActiveTasks = function () {
        Loader.show(true);

        return $http.get(apiTaskServicesUrl + "getMyActiveTasks/").then(function (result) {
          Loader.show(false);

          return result.data;
        })
      };
      var getMyUnclaimedTasks = function () {
        Loader.show(true);

        return $http.get(apiTaskServicesUrl + "getMyUnclaimedTasks/").then(function (result) {
          Loader.show(false);

          return result.data;
        })
      };
      var completeTask = function (data) {
        Loader.show(true);
        return $http(
          {
            method: 'POST',
            url: apiTaskServicesUrl + "completeTask/",
            data: data
          }
        ).then(function (result) {
            Loader.show(false);
            return result.data;
          })
      };
      var uploadTaskAttachment = function (data) {
        Loader.show(true);
        return $http(
          {
            method: 'POST',
            url: apiTaskServicesUrl + "uploadTaskAttachment/",
            headers: {'Content-Type': undefined},
            data: data
          }
        ).then(function (result) {
            Loader.show(false);
            return result.data;
          })
      };
      var removeDocument = function (id) {
        Loader.show(true);
        return $http.post(apiTaskServicesUrl + "removeDocument/" + id).then(function (result) {
          Loader.show(false);
          return result.data;
        })
      };

      var getDocumentByID = function (id) {
        Loader.show(true);
        return $http.get(apiTaskServicesUrl + "getDocumentByID/" + id).then(function (result) {
          Loader.show(false);
          return result.data;
        })
      };


      var getIncompletedOperationById = function (id) {
        Loader.show(true);
        return $http.get("/api/common/getIncompletedOperationById/" + id).then(function (result) {
          Loader.show(false);
          return result;
        })
      };

      var saveTask = function (data) {

        Loader.show(true);

        return $http.post(apiTaskServicesUrl + "saveTask/",
          {data: data}
        ).then(function (result) {
            Loader.show(false);

            return result.data;
          })
      };
      var getSingleOperation = function (key) {

        return $http.get(apiTaskServicesUrl + "getSingleOperation/?key=" + key).then(function (result) {
          return result.data;
        })
      };
      var openTask = function (taskData, state) {

        console.log('Open Task', taskData);
        if (taskData && taskData.config) {
          $kWindow.open({
            title: taskData['name' + $rootScope.lnC] ? taskData['name' + $rootScope.lnC] : taskData.name,
            actions: ['Minimize', 'Maximize', 'Close'],
            width: '100%',
            position: {
              top: 0,
              left: 0
            },
            taskId: taskData.id,
            screenId: taskData.screenId,
            screenType: taskData.config ? taskData.config.type : null,
            // appendTo: '#home',
            maximized: true,
            height: '100%',
            pinned: true,
            templateUrl: taskData.config.template,
            controller: taskData.config.controller,
            resolve: {
              id: function () {
                return taskData.key;
              },
              screenType: function () {
                return taskData.config.type;
              },
              taskId: function () {
                if (taskData.id) {
                  return taskData.id;
                }
                else {
                  return null;
                }
              },
              operationState: function () {
                return state;
              },
              task: function () {
                return taskData
              },
              configParams: function() {
                return {}
              }
            }
          });
        } else {

        }

      };
      var createWindow = function (screenId, taskId, title, template, controller, type, taskData, element, taskIdNum) {

        var winInst = $kWindow.open({
          title: title,
          actions: ['Minimize', 'Maximize', 'Close'],
          width: '100%',
          position: {
            top: 0,
            left: 0
          },
          screenType: type,
          screenId: screenId,
          // appendTo: '#home',
          maximized: true,
          height: '100%',
          pinned: true,
          taskId: taskIdNum,
          templateUrl: template,
          controller: controller,
          resolve: {
            id: function () {
              return taskId;
            },
            screenType: function () {
              return type;
            },
            operationState: function () {
              return null;
            },
            task: function () {
              return taskData;
            },
            configParams: function() {
              return {}
            }
          }
        });

        if (element) {
          element.attr('data-window-id', winInst.id);
        }

      };
      var startTask = function (attrs, element) {
        attrs.screenId = attrs.screenId || null;
        if (attrs.screenType == appConstants.screenTypes.operation) {
          Loader.show(true);

          startProcess(attrs.viewId).then(function (taskData) {
            if (taskData) {
              createWindow(attrs.screenId, taskData.key,
                taskData['name' + $rootScope.lnC] ? taskData['name' + $rootScope.lnC] : taskData.name, taskData.config.template,
                taskData.config.controller, taskData.config.type, taskData, element, taskData.id);
            }
            Loader.show(false);
          });
        }
        else {
          createWindow(attrs.screenId, attrs.viewId,
            attrs.windowTitle, attrs.windowTemplateUrl,
            attrs.windowController, attrs.screenType, null, null);
        }

      };
      var createOperationData = function (rawData) {

        var newData = rawData;
        var priorityData = {
          100: {
            level: "danger",
            nameEn: 'High',
            nameAz: 'Yüksək'
          },
          50: {
            level: "warning",
            nameEn: 'Medium',
            nameAz: 'Orta'
          },
          1: {
            level: "info",
            nameEn: 'Low',
            nameAz: 'Aşağı'
          }
        };

        if (rawData.priority) {

          newData['priorityData'] = priorityData[rawData.priority];
        }

        return newData;
      };
      var cancelTask = function (data) {

        Loader.show(true);
        return $http.post(apiTaskServicesUrl + "cancelTask/", {data: data}).then(function (result) {
          Loader.show(false);
          return result.data;
        });
      };
      var getDashBoardContents = function (key) {

        return $http.get(apiTaskServicesUrl + "getDashBoardContents/" + key).then(function (result) {
          var res = {};
          if (result.data && result.data.data) {
            res = angular.fromJson(result.data.data);
          }
          return res;
        });
      };
      return {
        createOperationData: createOperationData,
        startProcess: startProcess,
        startProcessByMessage: startProcessByMessage,
        getTask: getTask,
        openFinishedTask: openFinishedTask,
        getMyActiveTasks: getMyActiveTasks,
        getMyUnclaimedTasks: getMyUnclaimedTasks,
        completeTask: completeTask,
        uploadTaskAttachment: uploadTaskAttachment,
        removeDocument: removeDocument,
        getDocumentByID: getDocumentByID,
        getIncompletedOperationById: getIncompletedOperationById,
        getSingleOperation: getSingleOperation,
        openTask: openTask,
        claimTask: claimTask,
        saveTask: saveTask,
        cancelTask: cancelTask,
        startTask: startTask,
        getDashBoardContents: getDashBoardContents,
        sendTaskComment: sendTaskComment
      };
    }]);
