'use strict';

angular.module('cmisApp')
  .factory('operationRejectService', ["$http", "$q", "$kWindow", 'gettextCatalog',
    function ($http, $q, $kWindow, gettextCatalog) {

      var deferred = null;


      // Show cancel task window
      var showDialog = function (rejectionConfig) {
         deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Reject Task'),
          actions: ['Close'],
          isNotTile: true,
          //width: '500px',
          //minWidth:'50%',
        //  appendTo: '#home',
          //height: '300px',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/reject-operation.html',
          controller: 'RejectOperationController',
          resolve: {
            id: function () {
              return 0;
            },
            rejectionConfig: function() {
              return rejectionConfig;
            }
          }
        });

        return deferred.promise;
      };

      var sendFeedback = function (personData){

        deferred.resolve(personData);
        deferred = $q.defer();
      };

      var resetPromise = function () {
        deferred = $q.defer();
      };

      return {
        showDialog: showDialog,
        sendFeedback: sendFeedback,
        resetPromise:resetPromise

      };

    }]);
