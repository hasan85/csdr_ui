angular.module('kendo.window', [])
  .constant("kendoWindowConstants", {
    taskbarElementPrefix: 'taskbar-element-',
    taskbarPinnedElementPrefix: 'pw-taskbar-id',
    pinnedWindowsSplitterId: 'pinnedWindowsSplitter'
  })
/**
 * A helper, internal data structure that acts as a map but also allows getting / removing
 * elements in the LIFO order
 */
  .factory('$$stackedMap', function () {
    return {
      createNew: function () {
        var stack = [];

        return {
          add: function (key, value) {
            stack.push({
              key: key,
              value: value
            });
          },
          get: function (key) {
            for (var i = 0; i < stack.length; i++) {
              if (key == stack[i].key) {
                return stack[i];
              }
            }
          },
          keys: function () {
            var keys = [];
            for (var i = 0; i < stack.length; i++) {
              keys.push(stack[i].key);
            }
            return keys;
          },
          top: function () {
            return stack[stack.length - 1];
          },
          remove: function (key) {
            var idx = -1;
            for (var i = 0; i < stack.length; i++) {
              if (key == stack[i].key) {
                idx = i;
                break;
              }
            }
            return stack.splice(idx, 1)[0];
          },
          removeTop: function () {
            return stack.splice(stack.length - 1, 1)[0];
          },
          length: function () {
            return stack.length;
          }
        };
      }
    };
  })

  .factory('$windowStack', ['$document', '$compile', '$rootScope', '$$stackedMap', 'kendoWindowConstants', 'taskbarService', 'appConstants',
    function ($document, $compile, $rootScope, $$stackedMap, kendoWindowConstants, taskbarService, appConstants) {

      var body = $document.find('body').eq(0);
      var openedWindows = $$stackedMap.createNew();
      var $windowStack = {};

      function removeWindow(windowInstance) {


        var kendoWindow = openedWindows.get(windowInstance).value;

        $(kendoWindow.windowDomEl).data("kendoWindow").destroy();

        //clean up the stack
        openedWindows.remove(windowInstance);

        //remove window DOM element
        kendoWindow.windowDomEl.remove();

        //destroy scope
        kendoWindow.windowScope.$destroy();

      }

      $windowStack.open = function (windowInstance, kWindow) {

        openedWindows.add(windowInstance, {
          deferred: kWindow.deferred,
          windowScope: kWindow.scope,
          keyboard: kWindow.keyboard
        });


        var angularDomEl = angular.element('<div id="' + windowInstance.id + '"></div>');
        angularDomEl.attr('window-class', kWindow.windowClass);
        angularDomEl.attr('index', openedWindows.length() - 1);
        angularDomEl.html(kWindow.content);

        var windowDomEl = $compile(angularDomEl)(kWindow.scope);
        openedWindows.top().value.windowDomEl = windowDomEl;
        body.append(windowDomEl);
      };

      $windowStack.close = function (windowInstance, result, changeTabs) {

        var changeTabs = typeof  changeTabs === "boolean" ? changeTabs : true;

        var kendoWindow = openedWindows.get(windowInstance) ? openedWindows.get(windowInstance).value : null;

        if (kendoWindow) {
          kendoWindow.deferred.resolve(result);
          // taskbarService.removePinClickedClass();
          var windId = windowInstance.id;

          taskbarService.removeTaskbarElement(windId);

          delete $rootScope.activeWindows[windId];

          if (Object.keys($rootScope.activeWindows).length === 0 && changeTabs) {
            $rootScope.$broadcast('selectHomePageTab', 0);
          }
          removeWindow(windowInstance);

        }
      };

      $windowStack.dismiss = function (windowInstance, reason) {
        var kendoWindow = openedWindows.get(windowInstance).value;
        if (kendoWindow) {
          kendoWindow.deferred.reject(reason);
          removeWindow(windowInstance);
        }
      };

      $windowStack.getTop = function () {
        return openedWindows.top();
      };

      $windowStack.length = function () {
        return openedWindows.length();
      };

      return $windowStack;
    }])

  .provider('$kWindow', function () {

    var $windowProvider = {
      options: {
        keyboard: true
      },
      $get: ['$injector', '$rootScope', '$q', '$http', '$templateCache', '$controller',
        '$windowStack', 'kendoWindowConstants', 'taskbarService', 'screenSize', 'appConstants',
        function ($injector, $rootScope, $q, $http, $templateCache, $controller,
                  $windowStack, kendoWindowConstants, taskbarService, screenSize, appConstants) {

          var $kWindow = {};

          function getTemplatePromise(options) {
            return options.template ? $q.when(options.template) :
              $http.get(options.templateUrl, {cache: $templateCache}).then(function (result) {
                return result.data;
              });
          }

          function getResolvePromises(resolves) {
            var promisesArr = [];
            angular.forEach(resolves, function (value) {
              if (angular.isFunction(value) || angular.isArray(value)) {
                promisesArr.push($q.when($injector.invoke(value)));
              }
            });
            return promisesArr;
          }

          $kWindow.open = function (windowOptions) {

            var windowResultDeferred = $q.defer();
            var windowOpenedDeferred = $q.defer();

            //prepare an instance of a window to be injected into controllers and returned to a caller

            var windowInstance = {
              id: "kWindow" + $rootScope.createdWindowIndexes++,
              result: windowResultDeferred.promise,
              opened: windowOpenedDeferred.promise,
              close: function (result, changeTabs) {
                $windowStack.close(windowInstance, result, changeTabs);

              },
              dismiss: function (reason) {
                $windowStack.dismiss(windowInstance, reason);
              }

            };

            //merge and clean up options
            windowOptions = angular.extend({}, $windowProvider.options, windowOptions);
            windowOptions.resolve = windowOptions.resolve || {};

            //verify options
            if (!windowOptions.template && !windowOptions.templateUrl) {
              throw new Error('One of template or templateUrl options is required.');
            }

            var templateAndResolvePromise =
              $q.all([getTemplatePromise(windowOptions)].concat(getResolvePromises(windowOptions.resolve)));


            templateAndResolvePromise.then(function resolveSuccess(tplAndVars) {


              var kendoWindow;
              var windowScope = (windowOptions.scope || $rootScope).$new();
              windowScope.$close = windowInstance.close;
              windowScope.$dismiss = windowInstance.dismiss;

              var ctrlInstance, ctrlLocals = {};
              var resolveIter = 1;

              //controllers
              if (windowOptions.controller) {
                ctrlLocals.$scope = windowScope;
                ctrlLocals.$windowInstance = windowInstance;
                angular.forEach(windowOptions.resolve, function (value, key) {
                  ctrlLocals[key] = tplAndVars[resolveIter++];
                });

                ctrlInstance = $controller(windowOptions.controller, ctrlLocals);
              }


              $windowStack.open(windowInstance, {
                scope: windowScope,
                deferred: windowResultDeferred,
                content: tplAndVars[0],
                keyboard: windowOptions.keyboard,
                windowClass: windowOptions.windowClass
              });


            }, function resolveError(reason) {
              windowResultDeferred.reject(reason);
            });

            templateAndResolvePromise.then(function () {

              var opts = {
                title: (windowOptions.screenId ? ("[ " + windowOptions.screenId + "] ") : '') + windowOptions.title + (windowOptions.taskId ? (" - #" + windowOptions.taskId) : ''),
                modal: true,
                width: 500,
                // actions: ["Close"],
                visible: false,
                close: function () {
                  windowInstance.close(null);
                  $rootScope.customPinnedWindows = [];
                },

                open: function (e) {
                  // this.wrapper.css({top: 0});
                },
                activate: function () {
                  var autofocusElements = $(":input[autofocus]");
                  if (autofocusElements.length > 0) {
                    autofocusElements[0].focus();
                  }
                  windowOpenedDeferred.resolve(true);
                },
                //dragstart: function (e) {
                //
                //  $(e.sender.wrapper.context).css('display', 'none');
                //
                //},
                //dragend: function (e) {
                //  $(e.sender.wrapper.context).css('display', 'block');
                //},
              };

              if (windowOptions.maxWidth) {
                opts.maxWidth = windowOptions.maxWidth;
              }

              if (windowOptions.modal !== null) {
                opts.modal = windowOptions.modal;
              }
              if (windowOptions.actions) {
                opts.actions = windowOptions.actions;
              }
              if (windowOptions.position) {
                opts.position = windowOptions.position;
              }

              if (windowOptions.screenType) {
                opts.screenType = windowOptions.screenType;
              }

              if (windowOptions.isNotTile) {
                opts.isNotTile = windowOptions.isNotTile;
              }

              if (windowOptions.pinned) {
                opts.pinned = windowOptions.pinned;
              }

              var windowWidth = 100;

              if (screenSize.is('lg')) {
                windowWidth = 50;
              }
              else if (screenSize.is('md')) {
                windowWidth = 50;
              }
              if (windowOptions.screenType == appConstants.screenTypes.operation
                || windowOptions.screenType == appConstants.screenTypes.view) {


                if (windowOptions.screenType == appConstants.screenTypes.operation) {
                  opts.width = windowWidth + '%';
                  opts.height = '90%';
                } else {
                  opts.height = '90%';
                  opts.width = '90%';
                }
                //opts.maxHeight = '90%';
                opts.resizable = true;
                //opts.maxWidth = '100%';
                //opts.maxHeight = '100%';
                opts.appendTo = ".worker-area";

                $rootScope.activeWindows[windowInstance.id] = {
                  isOnTaskbar: false,
                  title: windowOptions.title,
                  screenId: windowOptions.screenId,
                  taskId: windowOptions.taskId
                };
              } else {

                if (windowOptions.resizable) {
                  opts.resizable = windowOptions.resizable;
                }
                if (windowOptions.width) {
                  opts.width = windowOptions.width;
                }
                if (windowOptions.height) {
                  opts.height = windowOptions.height;
                }
                if (windowOptions.minWidth) {
                  opts.minWidth = windowOptions.minWidth;
                }
                if (windowOptions.position) {
                  opts.position = windowOptions.position;
                }
                opts.appendTo = "body";
              }

              if (windowOptions.modal) {
                opts.position = {
                  top: "0px"
                };
                opts.appendTo = "body";
              }

              //Instantiate elements
              var wndId = $("#" + windowInstance.id);

              var wnd = wndId.kendoWindow(opts);

              var dialog = wndId.data("kendoWindow");

              var windowWrapper = wnd.closest(".k-window");

              if (windowOptions.screenType == appConstants.screenTypes.operation) {
                // All operations
                var leftPosition = (($windowStack.length() - 1) * 20) + 1 + "px";
                windowWrapper.css({
                  left: leftPosition,
                  top: 0
                });
              } else if (!windowOptions.modal) {
                // All views
                //dialog.maximize();
              }

              dialog.open();
              $rootScope.$broadcast('selectHomePageTab', 1);
              //$rootScope.homePageTabs.workerArea = true;
              //$rootScope.homePageTabs.dashboard = false;
              windowInstance.widget = dialog;

              if (opts.isNotTile) {
                dialog.center();
              }

              // Customize actions
              dialog.wrapper.find(".k-i-minimize").click(function () {
                taskbarService.addWindowToTaskbar(windowInstance.id, windowOptions.title, windowOptions.screenId, windowOptions.taskId);
                windowWrapper.css({
                  'display': 'none'
                });
              });

              dialog.wrapper.find(".k-i-restore").click(function () {

                windowWrapper.css({
                  'display': 'block'
                });

              });


            }, function () {
              windowOpenedDeferred.reject(false);
            });

            return windowInstance;
          };

          return $kWindow;
        }]
    };

    return $windowProvider;
  });
