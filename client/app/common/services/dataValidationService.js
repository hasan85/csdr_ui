'use strict';

angular.module('cmisApp')
  .factory('dataValidationService', ["$http", "$q", 'helperFunctionsService', function ($http, $q, helperFunctionsService) {

    var apiUrlPartial = "/api/data-validation/";

    var checkBalance = function (args) {
      var url = apiUrlPartial + 'checkBalance';
      return $http.post(url, {args: args}).then(function (result) {
        if (result['data']) {
          result.data['data'] = helperFunctionsService.convertObjectToArray(result.data['data']);
          if (result.data['data']) {
            for (var i = 0; i < result.data['data'].length; i++) {
              var obj = result.data['data'][i];
              if (!obj.currency) {
                obj.currency = {};
              }
              obj.operators = helperFunctionsService.convertObjectToArray(obj.operators);
            }
          }
        }
        return result.data;
      });
    };
    var validateSuccessionOperation = function (data) {

      var url = apiUrlPartial + 'validateSuccessionOperation';
      return $http.post(url, {data: data}).then(function (result) {

        return result.data;
      });
    };
    var sendBatchData = function (data) {

      var url = apiUrlPartial + 'sendBatchData';

      return $http.post(url, {data: data}).then(function (result) {
        return result.data;
      });
    };
    var sendActionFinish = function (data) {

      var url = apiUrlPartial + 'sendActionFinish';

      return $http.post(url).then(function (result) {
        return result.data;
      });
    };
    var sendTestData = function (data) {

      var url = apiUrlPartial + 'sendTestData';

      return $http.post(url, {data: data}).then(function (result) {
        return result.data;
      });
    };

    return {
      checkBalance: checkBalance,
      validateSuccessionOperation: validateSuccessionOperation,
      sendBatchData: sendBatchData,
      sendTestData: sendTestData,
      sendActionFinish: sendActionFinish

    };

  }]);
