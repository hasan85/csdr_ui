'use strict';

angular.module('cmisApp')
  .factory('instrumentFindService', ["$http", "$q", "$kWindow", "gettextCatalog", "helperFunctionsService", "appConstants", 'SweetAlert', function ($http, $q, $kWindow, gettextCatalog, helperFunctionsService, appConstants, SweetAlert) {

    var apiUrlPartial = "/api/common/";
    var deferred = $q.defer();

    //Find instrument
    var findInstrumentByData = function (formData, url) {

      return $http.post(
        apiUrlPartial + url,
        {
          data: formData
        }
      ).then(function (result) {
          if (result.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
    };
    var findInstrumentInBlackListByIssuer = function (issuerId) {
      return $http.post(
        '/api/instruments/getInstrumentsInBlackList/',
        {
          data: {
            issuerID: issuerId,
            isInBlackList: false
          }
        }
      ).then(function (result) {
          if (result.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
    };

    var findInstrumentInDebtorsListByIssuer = function (issuerId) {

      return $http.post(
        '/api/instruments/getInstrumentsInDebtorsList/',
        {
          data: {
            issuerID: issuerId,
            isInDebtorsList: false,

          }
        }
      ).then(function (result) {
          if (result.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
    };
    var findInstrumentByIssuer = function (criteria) {

      return $http.post(
        apiUrlPartial + "findInstrument",
        {
          data: criteria
        }
      ).then(function (result) {
          if (result.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
    };
    var findInstrumentsInCommissionExceptionListByIssuer = function (criteria) {

      return $http.post(
        "/api/persons/getInstrumentsInCommissionExceptionListByIssuer",
        {
          data: criteria
        }
      ).then(function (result) {
          if (result.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })

    };

    var findInstrumentsOutOfCommissionExceptionListByIssuer = function (criteria) {

      return $http.post(
        "/api/persons/getInstrumentsOutOfCommissionExceptionListByIssuer",
        {
          data: criteria
        }
      ).then(function (result) {
          if (result.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })

    };


    // Show instrument find window
    var findInstrument = function (params) {
      deferred = $q.defer();
      $kWindow.open({

        title: gettextCatalog.getString('Find Instrument'),
        actions: ['Close'],
        isNotTile: true,
        width: '90%',
        //appendTo: '#home',
        height: '90%',
        pinned: true,
        modal: true,
        screenType: appConstants.screenTypes.view,
        templateUrl: 'app/common/templates/instrument-find.html',
        controller: 'InstrumentFindController',
        resolve: {
          id: function () {
            return 0;
          },
          searchParams: function () {
            return params;
          }
        }
      });

      return deferred.promise;
    };

    var selectInstrument = function (instrumentData) {
      // Select instrument, resolve promise
      deferred.resolve(instrumentData);
      deferred = $q.defer();
    };

    var normalizeInstrumentArrayFields = function (instrumentData) {
      if (instrumentData.couponPayments) {
        instrumentData.couponPayments = helperFunctionsService.convertObjectToArray(instrumentData.couponPayments);
      }
      if (instrumentData.securityGroups) {
        instrumentData.securityGroups = helperFunctionsService.convertObjectToArray(instrumentData.securityGroups);
      }
      if (instrumentData.customIdentificators) {
        instrumentData.customIdentificators = helperFunctionsService.convertObjectToArray(instrumentData.customIdentificators);
      }
      if (instrumentData.fiscalDates) {
        instrumentData.fiscalDates = helperFunctionsService.convertObjectToArray(instrumentData.fiscalDates);
      }

      return instrumentData;
    };
    var findInstrumentById = function (id) {
      return $http.get('/api/instruments/getInstrumentById/' + id).then(function (result) {
        var resp = {};
        if (result.data) {
          if (result.data['success'] === "true") {
            resp = normalizeInstrumentArrayFields(result.data.data);
          } else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
          }
        }
        return resp;
      })
    };
    return {
      findInstrument: findInstrument,
      findInstrumentByData: findInstrumentByData,
      selectInstrument: selectInstrument,
      findInstrumentByIssuer: findInstrumentByIssuer,
      findInstrumentsInCommissionExceptionListByIssuer: findInstrumentsInCommissionExceptionListByIssuer,
      findInstrumentsOutOfCommissionExceptionListByIssuer: findInstrumentsOutOfCommissionExceptionListByIssuer,
      findInstrumentInBlackListByIssuer: findInstrumentInBlackListByIssuer,
      findInstrumentInDebtorsListByIssuer: findInstrumentInDebtorsListByIssuer,
      findInstrumentById: findInstrumentById
    };

  }]);
