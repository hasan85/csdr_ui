'use strict';

angular.module('cmisApp')
  .factory('taskbarService', ['$rootScope', 'kendoWindowConstants', '$filter', 'appConstants',
    function ($rootScope, kendoWindowConstants, $filter, appConstants) {

      var addWindowToTaskbar = function (windowId, title, screenId, taskId) {
        if (!$rootScope.activeWindows[windowId].isOnTaskbar) {

          $rootScope.activeWindows[windowId].isOnTaskbar = true;
          // console.log($rootScope.activeWindows);
          // Create new element on taskbar for new opened window
          var taskbarElementId = kendoWindowConstants.taskbarElementPrefix + windowId;

          var taskbarElement = "<md-list-item class='md-3-line' id='" + taskbarElementId + "' > " +
            " <div class='md-list-item-text'> " +
            " <md-button class='md-button-block md-button ' aria-label='icon button' >" +
            "  <i class='glyphicon glyphicon-pushpin'></i> " + "<span> [" + (screenId ? screenId : '') + "] " + title + (taskId ? (" #" + taskId) : '' ) + "</span>" +
            "  <md-tooltip md-direction='right'><span>" + title + "</span>  </md-tooltip>" +
            "</md-button>" +
            "</div> " +
            "</md-list-item>";

          $("#taskbar-elements").append(taskbarElement);
          var allActivePagesCollapsed = true;

          for (var pageKey in   $rootScope.activeWindows) {
            if ($rootScope.activeWindows.hasOwnProperty(pageKey)) {
              if (!$rootScope.activeWindows[pageKey].isOnTaskbar) {
                allActivePagesCollapsed = false;
              }
            }
          }
          if (allActivePagesCollapsed) {
            $rootScope.$broadcast('selectHomePageTab', 0);
          }
          // Taskbar element events
          $("#" + taskbarElementId).on('click', function () {
            $rootScope.$apply(function () {
              $rootScope.$broadcast('selectHomePageTab', 1);
              toggleSingleWindow(windowId);
            });

          });
          toggleSingleWindow(windowId);
          $rootScope.openedWindowsCount++;
        }
      };

      var toggleSingleWindow = function (windowId) {

        var winId = "#" + windowId;
        var window = $(winId).data('kendoWindow');
        var windowWrapper = $(winId).closest(".k-window");

        if (window) {
          if (window.options.isMinimized) {
            removeTaskbarElement(windowId);
            windowWrapper.css({
              'display': 'block'
            });
            window.restore();
            window.toFront();

          }
          else {
            windowWrapper.css({
              'display': 'none'
            });
            window.minimize();
          }
        }
      };

      var removeTaskbarElement = function (winId) {
        var element = kendoWindowConstants.taskbarElementPrefix + winId;
        if ($rootScope.activeWindows[winId] && $rootScope.activeWindows[winId].isOnTaskbar) {
          var elem = $("#" + element);
          if (elem.length) {
            elem.remove();
          }
          $rootScope.openedWindowsCount--;
          $rootScope.activeWindows[winId].isOnTaskbar = false;

        }
      };

      return {
        toggleSingleWindow: toggleSingleWindow,
        addWindowToTaskbar: addWindowToTaskbar,
        removeTaskbarElement: removeTaskbarElement
      }

    }]);
