'use strict';

angular.module('cmisApp')
  .factory('configurationService', ["$http", "$q", "helperFunctionsService", "SweetAlert", "gettextCatalog",
    function ($http, $q, helperFunctionsService, SweetAlert, gettextCatalog) {

      //Get tile configuration data
      var getTileConfiguration = function () {
        return $http.get('/api/task-services/getTileConfigurations/').then(function (result) {
          return result.data;
        });

      };

      //Return user data data
      var getUserInfo = function (userDataResult) {


        if (userDataResult.success === "false") {
          SweetAlert.swal("", helperFunctionsService.showErrorMessage(userDataResult), 'error');
          //window.location.replace('/logout');
        }

        var deferred = $q.defer();

        var userData = userDataResult.data;

        getTileConfiguration().then(function (configData) {


          var otherTiles = configData.otherTiles;
          var dashboardTiles = configData.dashboardTiles;

          if (userData) {
            userData.operations = helperFunctionsService.convertObjectToArray(userData.operations);
            if (userData.operations) {
              for (var k = 0; k < userData.operations.length; k++) {

                userData.operations[k].operations = helperFunctionsService.convertObjectToArray(userData.operations[k].operations);
                for (var i = 0; i < userData.operations[k].operations.length; i++) {
                  var screenId = userData.operations[k].operations[i].key;
                  userData.operations[k].operations[i] = angular.extend(userData.operations[k].operations[i], otherTiles[screenId]);
                }
              }
            }

            userData.views = helperFunctionsService.convertObjectToArray(userData.views);
            if (userData.views) {
              for (var k = 0; k < userData.views.length; k++) {
                userData.views[k].operations = helperFunctionsService.convertObjectToArray(userData.views[k].operations);
                for (var i = 0; i < userData.views[k].operations.length; i++) {
                  var screenId = userData.views[k].operations[i].key;
                  userData.views[k].operations[i] = angular.extend(userData.views[k].operations[i], otherTiles[screenId]);
                }
              }
            }

            if (userData.dashboards) {

              userData.dashboards = helperFunctionsService.convertObjectToArray(userData.dashboards);

              var permittedDashboards = [];

              for (var key in dashboardTiles) {
                if (dashboardTiles.hasOwnProperty(key)) {

                  for (var i = 0; i < userData.dashboards.length; i++) {
                    var screenId = userData.dashboards[i].key;
                    if (screenId === key) {
                      permittedDashboards.push(angular.extend(userData.dashboards[i], dashboardTiles[key]))
                    }
                  }
                }
              }
              userData.dashboards = permittedDashboards;
            }
          }

          deferred.resolve(userData);
        });

        return deferred.promise;
      };

      return {
        getUserInfo: getUserInfo
      };

    }]);
