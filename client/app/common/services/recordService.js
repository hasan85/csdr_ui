'use strict';

angular.module('cmisApp')
  .factory('recordService',
  ["$http", "$q", "$kWindow", 'gettextCatalog', 'helperFunctionsService', 'appConstants', 'referenceDataService',
    function ($http, $q, $kWindow, gettextCatalog, helperFunctionsService, appConstants, referenceDataService) {


      var getTrades = function (criteria) {
        return $http.post('/api/records/getTrades/', {criteria: criteria}).then(function (result) {

          if (result.data && result.data.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
      };

      var getOrders = function (criteria) {
        return $http.post('/api/records/getOrders/', {criteria: criteria}).then(function (result) {

          if (result.data && result.data.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
      };

       var getMyShareholderAccounts = function (criteria) {
              return $http.get('/api/records/getMyShareholderAccounts/').then(function (result) {
                if (result.data && result.data.data) {
                  result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
                }
                return result.data;
              })
            };

      return {
        getTrades: getTrades,
        getOrders:getOrders,
        getMyShareholderAccounts:getMyShareholderAccounts
      };

    }]);
