/* global io */
'use strict';

angular.module('cmisApp').factory('$socket', ['$rootScope', function ($rootScope) {

  return {
    connect: function () {
      var socket = io("", {
        query: 'token=' + ($rootScope.user ? $rootScope.user.token : null),
        path: '/socket.io-client'
      });

      return {
        on: function (eventName, callback) {
          socket.on(eventName, function () {
            var args = arguments;
            $rootScope.$apply(function () {
              callback.apply(socket, args);
            });
          });
        },
        emit: function (eventName, data, callback) {
          socket.emit(eventName, data, function () {
            var args = arguments;
            $rootScope.$apply(function () {
              if (callback) {
                callback.apply(socket, args);
              }
            });
          });
        },
        removeAllListeners: function (eventName, callback) {
          socket.removeAllListeners(eventName, function () {
            var args = arguments;
            $rootScope.$apply(function () {
              callback.apply(socket, args);
            });
          });
        }
      };
    }
  };

}]);
