'use strict';

angular.module('cmisApp')
  .factory('operationCancelService', ["$http", "$q", "$kWindow", 'gettextCatalog',
    function ($http, $q, $kWindow, gettextCatalog) {

      var deferred = null;


      // Show cancel task window
      var showDialog = function () {
         deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Cancel Task'),
          actions: ['Close'],
          isNotTile: true,
          width: '500px',
          //minWidth:'50%',
        //  appendTo: '#home',
          height: '185px',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/cancel-operation.html',
          controller: 'CancelOperationController',
          resolve: {
            id: function () {
              return 0;
            }
          }
        });

        return deferred.promise;
      };

      var sendFeedback = function (personData){

        deferred.resolve(personData);
        deferred = $q.defer();
      };

      var resetPromise = function () {
        deferred = $q.defer();
      };

      return {
        showDialog: showDialog,
        sendFeedback: sendFeedback,
        resetPromise:resetPromise

      };

    }]);
