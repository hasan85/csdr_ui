'use strict';

angular.module('cmisApp')
  .factory('reportService', ["$http", "$q", 'helperFunctionsService', function ($http, $q, helperFunctionsService) {

    var apiUrlPartial = "/api/reports/";


    var testPdf = function () {
      return $http.get(apiUrlPartial + 'testPdf', {contentType: 'application/pdf'})
        .then(function (res) {

         // console.log(data);
          //   var file = new Blob([data], {type: 'application/pdf'});
          // console.log(file);
          var fileURL = 'data:application/pdf;base64, ' + (escape(res.data));
          return fileURL;
          // window.open(fileURL);
        });
    };


    return {

      testPdf: testPdf,


    };

  }]);
