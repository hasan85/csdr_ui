'use strict';

angular.module('cmisApp')
  .factory('corporateActionFindService', ["$http", "$q", "$kWindow", 'gettextCatalog', 'helperFunctionsService',
    function ($http, $q, $kWindow, gettextCatalog, helperFunctionsService) {

      var deferred = $q.defer();

      var getByIdUrl = "/api/corporate-actions/getCorporateActionById/";
      var normalizeCorporateActionArrayFields = function (corporateActionData) {

        if (corporateActionData && corporateActionData.length > 0) {

          for (var i = 0; i < corporateActionData.length; i++) {
            corporateActionData[i].underlyingSecurity = corporateActionData[i].underlyingSecurity == "" ? null : corporateActionData[i].underlyingSecurity;

            corporateActionData[i].entitlementSecurity = corporateActionData[i].entitlementSecurity == "" ? null : corporateActionData[i].entitlementSecurity;

            if (corporateActionData[i].underlyingSecurity) {

              corporateActionData[i].underlyingSecurity = helperFunctionsService.convertObjectToArray(corporateActionData[i].underlyingSecurity);
            }
            if (corporateActionData[i].entitlementSecurity) {
              corporateActionData[i].entitlementSecurity = helperFunctionsService.convertObjectToArray(corporateActionData[i].entitlementSecurity);
            }
          }
        }
        return corporateActionData;
      };

      //Find corporateAction by data
      var findCorporateActionByData = function (url, formData) {

        return $http.post(
          url,
          {data: formData}
        ).then(function (result) {

            var res = helperFunctionsService.convertObjectToArray(result.data);


            res = normalizeCorporateActionArrayFields(res);

            return res;
          });
      };  //Find corporateAction by data

      //Find corporateAction by id
      var getCorporateActionById = function (id) {

        return $http.get(getByIdUrl + id).then(function (result) {
          var res = result.data;
          if (res) {
            res.data.entitlementSecurity = helperFunctionsService.convertObjectToArray(res.data.entitlementSecurity);
            res.data.underlyingSecurity = helperFunctionsService.convertObjectToArray(res.data.underlyingSecurity);
          }
          return res;
        });
      };  //Find corporateAction by data


      // Show corporateAction find window
      var findCorporateAction = function () {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Corporate Action'),
          actions: ['Close'],
          isNotTile: true,
          width: '80%',
          position: {
            left: "0px"
          },
          height: '80%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/corporate-action-find.html',
          controller: 'CorporateActionFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchUrl: function () {
              return '/api/corporate-actions/getCorporateActions';
            },
            formConfig: function () {
              return null;
            }
          }
        });

        return deferred.promise;
      };

      // Show corporateAction find window
      var findIssueRegistration = function () {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Corporate Action'),
          actions: ['Close'],
          isNotTile: true,
          width: '95%',
          //appendTo: '#home',
          height: '80%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/corporate-action-find.html',
          controller: 'CorporateActionFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchUrl: function () {
              return '/api/corporate-actions/getIssueRegistrations';
            },
            formConfig: function () {
              return {
                caCategory: {isVisible: false},
                caClass: {isVisible: false}
              }
            }
          }
        });

        return deferred.promise;
      };

      // Show corporateAction find window
      var findCashDividend = function () {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Cash Dividend'),
          actions: ['Close'],
          isNotTile: true,
          width: '95%',
          //appendTo: '#home',
          height: '80%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/corporate-action-find.html',
          controller: 'CorporateActionFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchUrl: function () {
              return '/api/corporate-actions/getCashDividends';
            },
            formConfig: function () {
              return {
                caCategory: {isVisible: false},
                caClass: {isVisible: false}
              }
            }
          }
        });

        return deferred.promise;
      };


      var selectCorporateAction = function (corporateActionData) {
        // Select corporateAction, resolve promise
        deferred.resolve(corporateActionData);
        deferred = $q.defer();
      };

      return {
        findCorporateAction: findCorporateAction,
        findCorporateActionByData: findCorporateActionByData,
        selectCorporateAction: selectCorporateAction,
        findIssueRegistration: findIssueRegistration,
        getCorporateActionById: getCorporateActionById,
        findCashDividend: findCashDividend

      };

    }]);
