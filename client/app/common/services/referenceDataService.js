'use strict';

angular.module('cmisApp')
  .factory('referenceDataService', ["$http", "$q", 'helperFunctionsService', '$filter', function ($http, $q, helperFunctionsService, $filter) {

    var apiUrlPartial = "/api/reference-data/";

    // Get service classes
    var getServiceClasses = function () {
      return $http.get(apiUrlPartial + "getServiceClasses/").then(function (result) {
        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };

    // Get all countries
    var getCountries = function () {
      return $http.get(apiUrlPartial + "getCountries/").then(function (result) {
        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get all currencies
    var getCurrencies = function () {
      return $http.get(apiUrlPartial + "getCurrencies/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // get AZIPS Session Info
    var getAZIPSSessionInfo = function () {
      return $http.get(apiUrlPartial + "getAZIPSSessionInfo/").then(function (result) {
        return result.data;
      })
    };
    // Get all phone number Types
    var getPhoneNumberTypes = function () {
      return $http.get(apiUrlPartial + "getPhoneNumberTypes/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get all  ID document Types
    var getIdDocumentTypes = function () {
      return $http.get(apiUrlPartial + "getIdDocumentTypes/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };

    // Get signer position types
    var getSignerPositions = function () {
      return $http.get(apiUrlPartial + "getSignerPositions/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get signer position types
    var getPersonClasses = function () {
      return $http.get(apiUrlPartial + "getPersonClasses/").then(function (result) {
        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get business classes
    var getBusinessClasses = function () {
      return $http.get(apiUrlPartial + "getBusinessClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get legal form classes
    var getLegalFormClasses = function () {
      return $http.get(apiUrlPartial + "getLegalFormClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get approval task decisions
    var getApprovalTaskDecisions = function () {
      return $http.get(apiUrlPartial + "getApprovalTaskDecisions/").then(function (result) {


        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get admin task decisions
    var getAdminTaskDecisions = function () {
      return $http.get(apiUrlPartial + "getAdminTaskDecisions/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };

    var getInstrumentIdentificatorTypes = function () {
      return $http.get(apiUrlPartial + "getInstrumentIdentificatorTypes/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get individual security classes
    var getIndividualSecurityGroups = function () {
      return $http.get(apiUrlPartial + "getIndividualSecurityGroups/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get CFI classes
    var getCFIClasses = function (parentId) {

      return $http.get(apiUrlPartial + "getCFIClasses/" + parentId).then(function (result) {


        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };
    var getcurrentCFIClasses = function (parentId) {
      return $http.get(apiUrlPartial + "getcurrentCFIClasses/" + parentId).then(function (result) {
        return helperFunctionsService.convertObjectToArray(result.data);
      });
    }
    // Get CFI category by pattern
    var getCFICategoryByPattern = function (pattern) {

      return $http.get(apiUrlPartial + "getCFICategoryByPattern/" + pattern).then(function (result) {
        return result.data;
      });
    };


    // Get registration Authority Classes
    var getRegistrationAuthorityClasses = function () {
      return $http.get(apiUrlPartial + "getRegistrationAuthorityClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };
    // Get Placement classes
    var getPlacementClasses = function () {
      return $http.get(apiUrlPartial + "getPlacementClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };

    // Get Corporate Action categories
    var getCorporateActionCategories = function () {
      return $http.get(apiUrlPartial + "getCorporateActionCategories/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };

    // Get Corporate Action classes
    var getCorporateActionClassesByCategory = function (categoryId) {
      return $http.get(apiUrlPartial + "getCorporateActionClassesByCategory/" + categoryId).then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };

    // Get coupon payment method classes
    var getCouponPaymentMethodClasses = function () {
      return $http.get(apiUrlPartial + "getCouponPaymentMethodClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };
    var getCAInstrumentDeletionClasses = function () {
      return $http.get(apiUrlPartial + "getCAInstrumentDeletionClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };
    var getFractionTreatmentClasses = function () {
      return $http.get(apiUrlPartial + "getFractionTreatmentClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };

    var getCABonusShareClasses = function () {
      return $http.get(apiUrlPartial + "getCABonusShareClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };
    var getActionClasses = function () {
      return $http.get(apiUrlPartial + "getActionClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };

    var getMemberRoles = function () {
      return $http.get(apiUrlPartial + "getMemberRoles/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      });
    };

    // Get Juridical Person ID document Types
    var getJuridicalPersonIdDocumentTypes = function () {
      return $http.get(apiUrlPartial + "getJuridicalPersonIdDocumentTypes/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    // Get Natural Person ID document Types
    var getNaturalPersonIdDocumentTypes = function () {
      return $http.get(apiUrlPartial + "getNaturalPersonIdDocumentTypes/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };

    var getSubjectContextClasses = function () {
      return $http.get(apiUrlPartial + "getSubjectContextClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getObjectRoleClasses = function () {
      return $http.get(apiUrlPartial + "getObjectRoleClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getOperationTypeClasses = function () {
      return $http.get(apiUrlPartial + "getOperationTypeClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getOrgNodeClasses = function () {
      return $http.get(apiUrlPartial + "getOrgNodeClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getTaskTypes = function () {
      return $http.get(apiUrlPartial + "getTaskTypes/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getAccountStatusCheckingClasses = function () {
      return $http.get(apiUrlPartial + "getAccountStatusCheckingClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getAccountRoles = function () {
      return $http.get(apiUrlPartial + "getAccountRoles/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getFunctionalAccountClasses = function () {
      return $http.get(apiUrlPartial + "getFunctionalAccountClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var getAuctionClasses = function () {
      return $http.get(apiUrlPartial + "getAuctionClasses/").then(function (result) {

        return helperFunctionsService.convertObjectToArray(result.data);
      })
    };
    var addEmptyOption = function (models) {
      for (var i = 0; i < models.length; i++) {
        if (models[i]) {
          models[i].unshift({
            id: '',
            code: '',
            nameAz: '---',
            nameEn: '---'
          });
        }
      }
    };
    var normalizeReturnedRecordTask = function (model, metaData) {
      if (model.registrationAuthorityClass && model.registrationAuthorityClass.id && metaData.registrationAuthorityClasses) {
        model.registrationAuthorityClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.registrationAuthorityClasses, model.registrationAuthorityClass['id']));
      }

      if (model.registrationAuthorityClass && model.registrationAuthorityClass.id && metaData.regAuthClasses) {
        model.registrationAuthorityClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.regAuthClasses, model.registrationAuthorityClass['id']));
      }

      if (model.fractionTreatmentClass && model.fractionTreatmentClass.id) {
        model.fractionTreatmentClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.fractionTreatmentClasses, model.fractionTreatmentClass['id']));
      }
      if (model.CAInstrumentDeletionClass && model.CAInstrumentDeletionClass.id) {
        model.CAInstrumentDeletionClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.CAInstrumentDeletionClasses, model.CAInstrumentDeletionClass['id']));
      }
      if (model.currency && model.currency.id) {
        model.currency = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.currencies, model.currency['id']));
      }

      if (model.entryType && model.entryType.id) {
        model.entryType = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.entryTypes, model.entryType['id']));
      }
      if (model.auctionClass && model.auctionClass.id) {
        model.auctionClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.auctionClasses, model.auctionClass['id']));
      }
      if (model.registrationDate && !model.registrationDateObj) {
        model.registrationDateObj = helperFunctionsService.parseDate(model.registrationDate);
        model.registrationDate = helperFunctionsService.generateDate(model.registrationDateObj);
      }
      if (model.valueDate && !model.valueDateObj) {
        model.valueDateObj = helperFunctionsService.parseDate(model.valueDate);
        model.valueDate = helperFunctionsService.generateDate(model.valueDateObj);
      }
      if (model.maturityDate && !model.maturityDateObj) {
        model.maturityDateObj = helperFunctionsService.parseDate(model.maturityDate);
        model.maturityDate = helperFunctionsService.generateDate(model.maturityDateObj);
      }
      if (model.recordDate && !model.recordDateObj) {
        model.recordDateObj = helperFunctionsService.parseDate(model.recordDate);
        model.recordDate = helperFunctionsService.generateDate(model.recordDateObj);
      }

      console.log('MODEL', model);
    };

    function generateStatisticsData(data) {
      var result = [];
      var parentFields = {
        financialSector: "Maliyyə sektoru",
        governmentManagementCompanies: "Dövlət idarəetmə orqanları",
        nonfinancialSector: "Qeyri-maliyyə sektoru",
        naturalPersons: "Ev təsərrüfati (fiziki şəxslər)",
        publicAndnoncommersionOrganizations: "İctimai və qeyri kommersiya təşkilatları",
        nonresidentPersons: "Qeyri-Rezident deponentlərin depo hesablarında olan həcm"
      };
      for (var k in data) {
        if (data.hasOwnProperty(k) && !(angular.isUndefined(data[k]) || data[k] === null)) {
          data[k].name = parentFields[k];
          result.push(data[k]);
        }
      }

      console.log("RESULT", result);
      return result;
    }


    return {
      getServiceClasses: getServiceClasses,
      getCountries: getCountries,
      getCurrencies: getCurrencies,
      getPhoneNumberTypes: getPhoneNumberTypes,
      getIdDocumentTypes: getIdDocumentTypes,
      getSignerPositions: getSignerPositions,
      getPersonClasses: getPersonClasses,
      getBusinessClasses: getBusinessClasses,
      getLegalFormClasses: getLegalFormClasses,
      getApprovalTaskDecisions: getApprovalTaskDecisions,
      getAdminTaskDecisions: getAdminTaskDecisions,
      getInstrumentIdentificatorTypes: getInstrumentIdentificatorTypes,
      getIndividualSecurityGroups: getIndividualSecurityGroups,
      getCFIClasses: getCFIClasses,
      getRegistrationAuthorityClasses: getRegistrationAuthorityClasses,
      getPlacementClasses: getPlacementClasses,
      getCorporateActionCategories: getCorporateActionCategories,
      getCorporateActionClassesByCategory: getCorporateActionClassesByCategory,
      getCouponPaymentMethodClasses: getCouponPaymentMethodClasses,
      getCAInstrumentDeletionClasses: getCAInstrumentDeletionClasses,
      getFractionTreatmentClasses: getFractionTreatmentClasses,
      getCABonusShareClasses: getCABonusShareClasses,
      getCFICategoryByPattern: getCFICategoryByPattern,
      getActionClasses: getActionClasses,
      getMemberRoles: getMemberRoles,
      getJuridicalPersonIdDocumentTypes: getJuridicalPersonIdDocumentTypes,
      getNaturalPersonIdDocumentTypes: getNaturalPersonIdDocumentTypes,
      getSubjectContextClasses: getSubjectContextClasses,
      getObjectRoleClasses: getObjectRoleClasses,
      getOperationTypeClasses: getOperationTypeClasses,
      getOrgNodeClasses: getOrgNodeClasses,
      getTaskTypes: getTaskTypes,
      getAccountStatusCheckingClasses: getAccountStatusCheckingClasses,
      addEmptyOption: addEmptyOption,
      getAccountRoles: getAccountRoles,
      getFunctionalAccountClasses: getFunctionalAccountClasses,
      getAuctionClasses: getAuctionClasses,
      normalizeReturnedRecordTask: normalizeReturnedRecordTask,
      getAZIPSSessionInfo: getAZIPSSessionInfo,
      getcurrentCFIClasses: getcurrentCFIClasses,
      generateStatisticsData: generateStatisticsData
    };

  }]);
