'use strict';

angular.module('cmisApp')
  .factory('dialogService', ["$http", "$q", "$kWindow", 'gettextCatalog',
    function ($http, $q, $kWindow, gettextCatalog) {

      var deferred = $q.defer();

      // Show cancel task window
      var showDialog = function (title, message, dialogConfiguration) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString(title),
          actions: ['Close'],
          isNotTile: true,
          width: 300,
          appendTo: '.client-area',
          //pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/dialog.html',
          controller: 'DialogController',
          resolve: {
            id: function () {
              return 0;
            },
            message: function () {
              return message
            },
            dialogConfiguration: function () {
              return dialogConfiguration
            }
          }

        });

        return deferred.promise;
      };

      var sendResponse = function (res) {
        // resolve promise
        deferred.resolve(res);
        deferred = $q.defer();
      };

      return {
        showDialog: showDialog,
        sendResponse: sendResponse
      };

    }]);
