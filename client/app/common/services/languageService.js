'use strict';

angular.module('cmisApp')
  .factory('languageService', ["$rootScope", "gettextCatalog", '$filter', function ($rootScope, gettextCatalog, $filter) {

    //Get tile configuration data
    var setLanguage = function (key) {
      gettextCatalog.setCurrentLanguage(key);
      $rootScope.language = gettextCatalog.getCurrentLanguage();
      $rootScope.ln = "_" + $rootScope.language;
    };
    var getCapitalizedLanguage = function (lang) {
      return $filter('capitalize')(lang);
    };

    return {
      setLanguage: setLanguage,
      getCapitalizedLanguage: getCapitalizedLanguage
    };

  }]);
