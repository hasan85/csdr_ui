'use strict';

angular.module('cmisApp')
  .factory('userService', ["$rootScope", "$http", "$q", "Loader", "languageService",
    function ($rootScope, $http, $q, Loader) {


      var getUserProfile = function () {
        return $http.get("/api/main/getUser/").then(function (result) {

          var profile = result.data;

         // languageService.setLanguage(profile.settings.language);

          return profile;
        })
      };

      var changeContext = function (val) {
        Loader.show(true);
        return $http.put("/api/main/changeContext", {context: val}).then(function (result) {
          Loader.show(false);
          return result.data;
        })
      };

      var changeSetting = function (key, value) {

        Loader.show(true);
        return $http.put("/api/main/changeSetting", {
          key: key,
          value: value
        }).then(function (result) {
          Loader.show(false);
          return result.data;
        })
      };

      var getUserContextNameById = function (contexts, id) {
        for (var i = 0; i < contexts.length; i++) {
          if (contexts[i].id === id) {
            return contexts[i]['name' + $rootScope.ln];
          }
        }

        throw "Couldn't find object with id: " + id;
      };

      return {
        getUserProfile: getUserProfile,
        changeContext: changeContext,
        changeSetting: changeSetting,
        getUserContextNameById: getUserContextNameById
      };

    }]);
