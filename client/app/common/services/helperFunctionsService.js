'use strict';

angular.module('cmisApp')
  .factory('helperFunctionsService', ['$window', 'gettextCatalog', '$filter', function ($window, gettextCatalog, $filter) {

    var dayOfWeeks = {
      1: gettextCatalog.getString('Monday'),
      2: gettextCatalog.getString('Tuesday'),
      3: gettextCatalog.getString('Wednesday'),
      4: gettextCatalog.getString('Thursday'),
      5: gettextCatalog.getString('Friday'),
      6: gettextCatalog.getString('Saturday'),
      7: gettextCatalog.getString('Sunday')
    };

    var getDayOfWeek = function (number) {
      return number >= 1 && number <= 7 ? dayOfWeeks[number] : null;
    };

    var isEmptyObject = function (obj) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
          return false;
      }

      return true;
    };

    var printHtml = function (data) {

      var windowObj = $window.open('', ' ', "width=600, height=600");
      windowObj.document.write(data);
      windowObj.document.close(); // necessary for IE >= 10
      windowObj.focus(); // necessary for IE >= 10

      setTimeout(function () {
        windowObj.print();
        windowObj.close();
      }, 500);
      return true;

    };

    var getStartOfDay = function () {
      var today = new Date();
      today.setHours(0);
      today.setMinutes(0);
      today.setSeconds(0);

      return today;
    };

    var getEndOfDay = function () {
      var today = new Date();
      today.setHours(23);
      today.setMinutes(59);
      today.setSeconds(59);
      return today;
    };


    var generateDate = function (dateObject) {
      var date = "";
      if (dateObject) {
        date = dateObject.getFullYear()
          + "-" + (parseInt(dateObject.getMonth()) + 1)
          + "-" + dateObject.getDate();
      }
      return date;
      //  return dateObject.getDate() + "-" + (parseInt(dateObject.getMonth()) + 1) + "-" + dateObject.getFullYear();
    };

    var generateDateFromFormat = function (dateObject, format) {
      var date = "";
      if (dateObject) {
        date = dateObject.getFullYear()
          + "-" + (parseInt(dateObject.getMonth()) + 1)
          + "-" + dateObject.getDate();
      }
      return date;
      //  return dateObject.getDate() + "-" + (parseInt(dateObject.getMonth()) + 1) + "-" + dateObject.getFullYear();
    };


    // dd-MM-yyyy HH:mm:ss
    var generateDateTime = function (dateObject) {

      var date = $filter('date')(dateObject, 'dd-MM-yyyy HH:mm:ss');
      return date;
    };

    var generateDateWithoutTime = function (dateObject) {

      var date = $filter('date')(dateObject, 'dd-MM-yyyy');
      return date;
    };


    var findByIdInsideArray = function (arr, id) {

      if (arr) {
        for (var i = 0; i < arr.length; i++) {

          if (arr[i].id == id) {
            return arr[i];
          }
        }
      }
      return null;
    };
    var findByIdInsideArrayByProperty = function (arr, id, prop) {

      if (arr) {
        for (var i = 0; i < arr.length; i++) {

          if (arr[i][prop] == id) {
            return arr[i];
          }
        }
      }
      return null;
    };

    var convertObjectToArray = function (field) {
      if (field) {
        if (typeof field.map === 'function') {
          return field;
        } else {
          field = [field];
        }
      }
      return field;
    };

    var startsWith = function (str, partial) {
      if (!str) {
        return false;
      }
      return str.indexOf(partial) === 0;
    };

    function endsWith(str, suffix) {
      if (!str) {
        return false;
      }
      return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    var convertArrayToObjectByKey = function (array, key) {
      var result = {};
      if (array && array.length > 0) {
        for (var i = 0; i < array.length; i++) {
          var keyD = array[i][key];
          result[keyD] = array[i];
        }
        return result;
      } else {

        return array;
      }

    };

    var convertArrayToObjectWithRandomKey = function (array) {
      var result = {};
      if (array) {
        for (var i = 0; i < array.length; i++) {
          var keyD = this.randomString(20, 'c');
          result[keyD] = array[i];
        }
      }
      return result;
    };

    var randomString = function (len, an) {
      an = an && an.toLowerCase();
      var str = "", i = 0, min = an == "a" ? 10 : 0, max = an == "n" ? 10 : 62;
      for (; i++ < len;) {
        var r = Math.random() * (max - min) + min << 0;
        str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
      }
      return str;
    };

    var replaceAt = function (str, index, character) {
      return str.substr(0, index) + character + str.substr(index + character.length);
    };

    var b64toBlob = function (b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };
    var saveBlobFile = function (url, name) {
      var a = document.createElement("a");
      a.style = "display: none";
      a.href = url;
      a.download = name;
      document.body.appendChild(a);

      a.onclick = function () {
        //(window.URL || window.webkitURL).revokeObjectURL(a.href);
        (document.body || document.documentElement).removeChild(a);
      };

      var mouseEvent = new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
      });

      setTimeout(function () {
        a.dispatchEvent(mouseEvent);

        // if you're writing cross-browser function:
        if (!navigator.mozGetUserMedia) { // i.e. if it is NOT Firefox
          window.URL.revokeObjectURL(a.href);
        }
      }, 200);

      // NEVER use "revoeObjectURL" here
      // you can use it inside "onclick" handler, though.
      // (window.URL || window.webkitURL).revokeObjectURL(hyperlink.href);


    };


    var saveBase64AsBlob = function (b64Data, contentType, extension) {
      console.log(b64Data);
      var blob = b64toBlob(b64Data, contentType);
      var blobUrl = URL.createObjectURL(blob);
      saveBlobFile(blobUrl, 'document.' + extension);
    };

    var saveBase64File = function printDocument(data) {
      window.open("data:application/pdf;base64," + data);
    };
    var openBase64File = function printDocument(data) {
      var iframe = "<iframe " +
        "frameborder='0' " +
        "style='border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;' " +
        "src='data:application/pdf;base64,"+data+"'" +
        "allowfullscreen></iframe>";
      var x = window.open();
      x.document.open();
      x.document.write(iframe);
      x.document.close();
    };

    var saveBase64FileByType = function printDocument(type, data) {
      window.open("data:" + type + ";base64," + data);
    };
    var showErrorMessage = function (res) {
      if (res) {
        return (res.message ? res.message : '');
      } else {
        return '';
      }
    };

    var parseDate = function (str) {
      return kendo.parseDate(str, 'dd-MM-yyyy HH:mm:ss');
    };
    var asbDateFormat = function (date) {
      return kendo.toString(date, "yyyyMMdd");
    };
    var generateAsbText = function (obj) {
      var res = '';
      var data = angular.copy(obj);

      // correcting data.
      data.instrumentName = data.instrumentName ? data.instrumentName : '';
      data.contractSize = data.contractSize ? data.contractSize : '';
      data.centralSecurityDepositoryName = data.centralSecurityDepositoryName ? data.centralSecurityDepositoryName : '';
      data.modifyDate = data.modifyDate ? data.modifyDate : "";
      data.maturityDate = data.maturityDate ? data.maturityDate : "";
      data.interestRate = data.interestRate ? data.interestRate : "";
      data.nominalValue = data.nominalValue ? data.nominalValue : "";


      res += data.isin + ';';
      res += data.status + ';';
      res += data.instrumentCategory + ';';
      res += data.CFI.description + ';';
      res += data.CFI.code + ';';
      res += data.instrumentName + ';';
      res += data.prospectus + ';';
      res += data.nominalValue + ';';
      res += data.currency + ';';
      res += data.smallestDenomination + ';';
      res += data.contractSize + ';';
      res += asbDateFormat(data.maturityDate) + ';';
      res += data.exercisePrice + ';';
      res += data.exercisePriceCurrency + ';';
      res += data.underlying + ';';
      res += data.typeofInterest + ';';
      res += data.interestRate + ';';
      res += data.interestPaymentDate + ';';
      res += data.interestFrequency + ';';
      res += asbDateFormat(data.firstinterestPaymentDate) + ';';
      res += data.additionalInformation + ';';
      res += data.MIC + ';';
      res += data.leadManagerName + ';';
      res += data.leadManagerLEI + ';';
      res += data.fundManagerName + ';';
      res += data.fundManagerLEI + ';';
      res += data.centralSecurityDepositoryName + ';';
      res += data.centralSecurityDepositoryNameLEI + ';';
      res += data.issuerName + ';';
      res += data.issuerLEI + ';';
      res += ';';
      res += data.issuerSupranational + ';';
      res += data.issuerHeadquartersAddress.plainAddress + ';';
      res += ';';
      res += data.issuerHeadquartersAddress.state + ';';
      res += data.issuerHeadquartersAddress.postalCode + ';';
      res += ';';
      res += data.issuerHeadquartersAddress.country2a + ';';

      res += data.issuerLegalAddress.plainAddress + ';';
      res += ';';
      res += data.issuerLegalAddress.state + ';';
      res += data.issuerLegalAddress.postalCode + ';';
      res += ';';
      res += data.issuerLegalAddress.country2a + ';';
      res += asbDateFormat(data.createdDate) + ';';
      res += asbDateFormat(data.modifyDate) + ';';
      return res;
    };

    return {
      generateDate: generateDate,
      convertObjectToArray: convertObjectToArray,
      findByIdInsideArray: findByIdInsideArray,
      startsWith: startsWith,
      endsWith: endsWith,
      printHtml: printHtml,
      convertArrayToObjectByKey: convertArrayToObjectByKey,
      randomString: randomString,
      convertArrayToObjectWithRandomKey: convertArrayToObjectWithRandomKey,
      replaceAt: replaceAt,
      generateDateTime: generateDateTime,
      generateDateWithoutTime: generateDateWithoutTime,
      saveBase64File: saveBase64File,
      getDayOfWeek: getDayOfWeek,
      getStartOfDay: getStartOfDay,
      getEndOfDay: getEndOfDay,
      isEmptyObject: isEmptyObject,
      showErrorMessage: showErrorMessage,
      parseDate: parseDate,
      saveBase64FileByType: saveBase64FileByType,
      saveBase64AsBlob: saveBase64AsBlob,
      findByIdInsideArrayByProperty: findByIdInsideArrayByProperty,
      generateAsbText: generateAsbText,
      saveBlobFile: saveBlobFile,
      openBase64File: openBase64File
    };


  }]);
