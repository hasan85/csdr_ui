'use strict';

angular.module('cmisApp')
  .factory('Loader', [function () {
    var instance = {};
    instance.show = function (on) {
      instance.visible = on;
    };
    return instance;
  }]);
