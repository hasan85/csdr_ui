'use strict';
angular.module('cmisApp').filter('capitalize', function() {
  return function(input, scope) {
    if (input!=null) {
      input = input.toLowerCase();
    }
    if(input) {
      return input.substring(0, 1).toUpperCase() + input.substring(1);
    }else{
      return input;
    }
  }
});
