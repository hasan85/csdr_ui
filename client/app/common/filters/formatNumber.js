/**
 * Created by maykinayki on 11/23/16.
 */

angular.module("cmisApp").filter('formatNumber', function () {
  return function (value, showReminder) {
    showReminder = showReminder == undefined ? true: showReminder;
    var n = Number(value),
      c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? " " : t,
      s = n < 0 ? "-" : "",
      i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
      j = (j = i.length) > 3 ? j % 3 : 0;
    var returnString = s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t);
    if(showReminder) {
      returnString += (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }
    return returnString;
  };
});
