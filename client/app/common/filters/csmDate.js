'use strict';
angular.module('cmisApp').filter('csmDate', ['$rootScope', function ($rootScope) {
  return function (input, format) {

    var formatToShow = "dd-MM-yyyy HH:mm:ss";

    if (format) {

      if (format == "localeDate") {

        if ($rootScope.lnC = "Az") {
          formatToShow = "dd-MM-yyyy";
        } else {
          formatToShow = "MM-dd-yyyy";
        }

      } else if (format == "localeTime") {
        if ($rootScope.lnC = "Az") {
          formatToShow = "dd-MM-yyyy HH:mm:ss";
        } else {
          formatToShow = "MM-dd-yyyy HH:mm:ss";
        }
      }
    }

    return kendo.parseDate(input, 'dd-MM-yyyy HH:mm:ss');

  }
}]);
