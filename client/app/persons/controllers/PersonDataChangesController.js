'use strict';

angular.module('cmisApp')
  .controller('PersonDataChangesController', ['$scope', 'appConstants',
    function ($scope, appConstants) {

      $scope.fieldChangeStatuses = appConstants.fieldChangeStatuses;

      $scope.subPersonBuffer = {};

      $scope.subPersonName = '';

      $scope.showSubPersonData = function (index, data, window) {

        $scope.subPersonBuffer = angular.copy(data[index]);

        $scope.subPersonName = '';

        if($scope.subPersonBuffer.name.newValue && $scope.subPersonBuffer.name.newValue.nameAz){
          $scope.subPersonName = $scope.subPersonBuffer.name.newValue.nameAz;
        }
        else{
          if ($scope.subPersonBuffer.lastName && $scope.subPersonBuffer.lastName.newValue) {
            $scope.subPersonName += $scope.subPersonBuffer.lastName.newValue.nameAz;
          }
          if($scope.subPersonBuffer.firstName && $scope.subPersonBuffer.firstName.newValue ){
            $scope.subPersonName += ' '+$scope.subPersonBuffer.firstName.newValue.nameAz;
          }
          if($scope.subPersonBuffer.middleName && $scope.subPersonBuffer.middleName.newValue){
            $scope.subPersonName += ' '+$scope.subPersonBuffer.middleName.newValue.nameAz;
          }
        }


        window.title($scope.subPersonName);
        window.open();
        window.center();
      };

      $scope.showUserData = function (data, window) {

        $scope.subPersonBuffer = angular.copy(data);

        $scope.subPersonName = $scope.subPersonBuffer.lastName.newValue.nameAz + ' '
          + $scope.subPersonBuffer.firstName.newValue.nameAz + ' '
          + $scope.subPersonBuffer.middleName.newValue.nameAz;

        window.title($scope.subPersonName);
        window.open();
        window.center();
      };

    }]);
