'use strict';

angular.module('cmisApp')
  .controller('MembersController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false
        },
        params: {
          searchUrl: '/api/persons/getMembers',
          config: {
            searchOnInit: true
          },
          criteriaEnabled:false
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view: {
            title: gettextCatalog.getString('View'),
            disabled: true,
            click: function () {
              $scope.$broadcast('showPersonDetails');
            }
          },
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        }
      };
      $scope.$watch('searchConfig.person.selectedPersonIndex', function (val) {

        if (val !== false) {
          $scope.config.buttons.view.disabled = false;
        }
        else {
          $scope.searchConfig.person.selectedPersonIndex = false;
          $scope.config.buttons.view.disabled = true;
        }

      });

    }]);
