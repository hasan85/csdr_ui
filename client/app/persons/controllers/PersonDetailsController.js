'use strict';

angular.module('cmisApp')
  .controller('PersonDetailsController', ['$scope', 'gettextCatalog',
    function ($scope, gettextCatalog) {


      $scope.subPersonBuffer = {};
      $scope.subPersonName = '';

      $scope.showSubPersonData = function (index, data, window) {


        console.log(arguments);
        $scope.subPersonBuffer = angular.copy(data[index]);

        if($scope.subPersonBuffer.name && $scope.subPersonBuffer.name.nameAz){
          $scope.subPersonName = $scope.subPersonBuffer.name.nameAz;
        }
        else{
          if ($scope.subPersonBuffer.lastName ) {
            $scope.subPersonName += $scope.subPersonBuffer.lastName.nameAz;
          }
          if($scope.subPersonBuffer.firstName ){
            $scope.subPersonName += ' '+$scope.subPersonBuffer.firstName.nameAz;
          }
          if($scope.subPersonBuffer.middleName ){
            $scope.subPersonName += ' '+$scope.subPersonBuffer.middleName.nameAz;
          }
        }
        window.title($scope.subPersonName);
        window.open();
        window.center();
      };

      $scope.labels = {
        personalInfo: gettextCatalog.getString('Personal Information'),
        idDocuments: gettextCatalog.getString('ID Documents'),
        actualAddress: gettextCatalog.getString('Actual Address'),
        legalAddress: gettextCatalog.getString('Legal Address'),
        contactInformation: gettextCatalog.getString('Contact Information'),
        bankAccounts: gettextCatalog.getString('Bank Accounts'),
        representatives: gettextCatalog.getString('Representatives')


      }

    }]);
