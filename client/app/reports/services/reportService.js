'use strict';

angular.module('cmisApp')
  .factory('reportService', ["$http", "$q", 'helperFunctionsService', function ($http, $q, helperFunctionsService) {

    var apiUrlPartial = "/api/reports/";


    var getAccountStatement = function (accountId, requestDate) {
      return $http.post(apiUrlPartial + 'getAccountStatement/', {
        data: {
          accountID: accountId,
          requestDate: requestDate
        }
      })
        .then(function (result) {
          if (result.data && result.data.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
            if (result.data.data) {
              for (var i = 0; i > result.data.data.length; i++) {
                result.data.data[i].operators = helperFunctionsService.convertObjectToArray(result.data.data[i].operators);
              }
            }
          }
          return result.data;
        });
    };

    var getHoldingReport = function (instrumentId, requestDate) {
      return $http.post(apiUrlPartial + 'getHoldingReport/', {
        data: {
          instrumentID: instrumentId,
          requestDate: requestDate
        }
      }).then(function (result) {
        if (result.data && result.data.data) {
          result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          if (result.data.data) {
            for (var i = 0; i > result.data.data.length; i++) {
              result.data.data[i].operators = helperFunctionsService.convertObjectToArray(result.data.data[i].operators);
            }
          }
        }
        return result.data;
      });
    };

    var testPdf = function () {
      return $http.get(apiUrlPartial + 'testPdf', {contentType: 'application/pdf'})
        .then(function (res) {

          // console.log(data);
          //   var file = new Blob([data], {type: 'application/pdf'});
          // console.log(file);
          var fileURL = 'data:application/pdf;base64, ' + (escape(res.data));
          return fileURL;
          // window.open(fileURL);
        });
    };

    var normalizeReturnedReportTask = function (model) {
      if (model.requestDate && !model.requestDateObj) {
        model.requestDateObj = helperFunctionsService.parseDate(model.requestDate);
        model.requestDate = helperFunctionsService.generateDateTime(model.requestDateObj);
      }
    };
    return {
      testPdf: testPdf,
      getAccountStatement: getAccountStatement,
      getHoldingReport: getHoldingReport,
      normalizeReturnedReportTask: normalizeReturnedReportTask
    };

  }]);
