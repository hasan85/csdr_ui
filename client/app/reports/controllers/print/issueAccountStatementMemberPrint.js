'use strict';

angular.module('cmisApp')
  .controller('IssueAccountStatementMemberPrintController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task','helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task,helperFunctionsService) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };


    }]);
