'use strict';

angular.module('cmisApp')
  .controller('ViewSecuritiesReportController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.data = {};
        $scope.metaData = {
          currencyClasses: [],
          instrumentTypes: [
            {data: 'ShortTermBond', name: "Qısamüddətli Istiqraz"},
            {data: 'LongTermBond', name: "Uzunmüddətli Istiqraz"},
            {data: 'Share', name: "Səhm"},
            {data: 'CbarNotes', name: "Mərkəzi bankın notları"}
          ]
        };
        $scope.CHILD_NAMES = [
          'detailedNonFinancialSector', 'detailedFinancialSector',
          'detailedGovernmentManagementCompanies', 'detailedNaturalPersons',
          'detailedPublicAndnoncommersionOrganizations', 'detailedNonResidentPersons'
        ];
        $scope.data.instrumentType = $scope.metaData.instrumentTypes[0].data;

        var date = new Date();
        var startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());

        var firstDateWithSlashes = (startDate.getDate()) + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();

        $scope.data.dateObj = startDate;
        $scope.data.date = firstDateWithSlashes;
        $scope.findData = function () {
          var formData = angular.copy($scope.data);


          formData.date = $scope.data.dateObj;

          delete formData.dateObj;
          if (formData.currencyId === "") return;
          if (angular.equals(formData, $scope.previousSearchData)) return;

          Loader.show(true);

          $http({
            method: 'POST',
            url: '/api/reportsDataServices/getStatisticsReports/',
            data: {data: formData}
          }).then(function (response) {
            delete $scope.records;
            var data = response.data;
            if (!data.exceptionModel) {
              $scope.records = referenceDataService.generateStatisticsData(data.data);
              $scope.previousSearchData = angular.copy(formData);
            } else {
              SweetAlert.swal("", data.exceptionModel.message, "error");
            }
            Loader.show(false);
          });
        };


        $scope.getDetail = function (obj) {
          obj.showDetail = !obj.showDetail;
        };


        $scope.printData = function (elementID) {
          $scope.printClicked = true;
          Loader.show(true);

          var css = '<style>table{border-collapse: collapse;font-size: 20px;}table, td, th {border: 1px solid #999;}</style>';

          var divToPrint = document.getElementById(elementID);
          var newWin = window.open("");
          newWin.document.write(css + divToPrint.outerHTML);

          setTimeout(function () {
            newWin.print();
            newWin.close();
            $scope.printClicked = false;
            Loader.show(false);
          }, 100);
        };

        $http({
          method: "GET",
          url: "api/reference-data/getCurrencies/"
        }).then(function (response) {
          if (response.data.sucess !== "false") {
            $scope.metaData.currencyClasses = response.data;
            $scope.findData();
          } else {
            SweetAlert.swal("", response.data.message, "error");
          }
        });

        $scope.closeWindow = function () {
          $windowInstance.close();
        };


      }])
;
