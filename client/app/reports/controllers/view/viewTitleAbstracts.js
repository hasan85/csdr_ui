'use strict';

angular.module('cmisApp')
  .controller('TitleAbstractsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        accountStatement: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/reports/getTitleAbstractDocuments',
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);


