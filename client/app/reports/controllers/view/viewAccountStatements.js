'use strict';

angular.module('cmisApp')
  .controller('AccountStatementsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      $scope.hideInstrumentsColumns = true;

      // Generic search configuration for person
      $scope.searchConfig = {
        accountStatement: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/reports/getAccountStatementDocuments',
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
