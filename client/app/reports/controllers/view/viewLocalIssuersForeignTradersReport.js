'use strict';

angular.module('cmisApp')
  .controller('LocalIssuersForeignTradersReportController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        freezing: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedFreezingIndex: false
        },
        trade: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };
      
      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
