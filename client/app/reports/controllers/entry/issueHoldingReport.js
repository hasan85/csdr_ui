'use strict';

angular.module('cmisApp')
  .controller('IssueHoldingReportController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService'
    , 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'SweetAlert', 'gettextCatalog','reportService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, SweetAlert, gettextCatalog,reportService) {

      var prepareFormData = function (formData) {
        if (formData.currentTimeSelected) {
          formData.requestDate = helperFunctionsService.generateDateTime(new Date());
        } else {
          if (formData.requestDateObj) {
            formData.requestDate = helperFunctionsService.generateDateTime(formData.requestDateObj);
          }
        }

        delete formData.requestDateObj;
        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.instrument === null || draft.instrument === undefined) {
          draft.instrument = {};
        }
        if (draft.currentTimeSelected === null || draft.currentTimeSelected === undefined) {
          draft.currentTimeSelected = true;
        }

          if(draft.selectedSigner) {
              $scope.selectedSignerID = draft.selectedSigner.id;
          }

        return draft;
      };

      $scope.issueHoldingReport = initializeFormData(angular.fromJson(task.draft));

        $scope.selectedSignerChange = function(selectedSignerID) {
            angular.forEach($scope.issueHoldingReport.signers, function(signer) {
                if(signer.id == selectedSignerID) {
                    $scope.issueHoldingReport.selectedSigner = signer;
                }
            });
        };
        if($scope.issueHoldingReport.selectedSigner) {
            $scope.selectedSignerChange($scope.issueHoldingReport.selectedSigner.id);
        }

      reportService.normalizeReturnedReportTask($scope.issueHoldingReport);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "issueHoldingReportForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.issueHoldingReport)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.issueHoldingReport);
      });

      $scope.$watch("issueHoldingReport.currentTimeSelected", function(newVal, oldVal) {
        if(newVal !== oldVal) {
          $scope.resetSelectedInsturments();
        }
      });

      $scope.$watch("issueHoldingReport.requestDateObj", function(newVal, oldVal) {
        if(newVal !== oldVal) {
          $scope.resetSelectedInsturments();
        }
      });

      $scope.resetSelectedInsturments = function() {
        $scope.issueHoldingReport.instrument = {};
      };

      // Search account
      $scope.findInstrument = function () {
        instrumentFindService.findInstrument({
          params: {
            date: $scope.issueHoldingReport.currentTimeSelected ? null : $scope.issueHoldingReport.requestDateObj
          }
        }).then(function (data) {
          $scope.issueHoldingReport.instrument = {};
          $scope.issueHoldingReport.instrument.ISIN = data.isin;
          $scope.issueHoldingReport.instrument.id = data.id;
          $scope.issueHoldingReport.instrument.issuerName = data.issuerName;
        });
      };
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.timePicker) {
          $scope.config.timePicker = widget;
        }
      });
    }]);
