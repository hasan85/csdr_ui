'use strict';

angular.module('cmisApp')
  .controller('IssueAccountStatementMember',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'helperFunctionsService', 'operationState', 'task', 'SweetAlert', 'gettextCatalog', 'reportService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, helperFunctionsService, operationState, task, SweetAlert, gettextCatalog, reportService) {

      var prepareFormData = function (formData) {
        if (formData.currentTimeSelected) {
          formData.requestDate = helperFunctionsService.generateDateTime(new Date());
        } else {
          if (formData.requestDateObj) {
            formData.requestDate = helperFunctionsService.generateDateTime(formData.requestDateObj);
          }
        }

        delete formData.requestDateObj;
        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.account === null || draft.account === undefined) {
          draft.account = {};
        }
        if (draft.currentTimeSelected === null || draft.currentTimeSelected === undefined) {
          draft.currentTimeSelected = true;
        }

        if(draft.selectedSigner) {
          $scope.selectedSignerID = draft.selectedSigner.id;
        }
        if(draft.selectedOperator) {
          $scope.selectedOperatorID = draft.selectedOperator.id;
        }

        return draft;
      };

      $scope.issueAccountStatement = initializeFormData(angular.fromJson(task.draft));

      $scope.selectedSignerChange = function(selectedSignerID) {
        angular.forEach($scope.issueAccountStatement.signers, function(signer) {
          if(signer.id == selectedSignerID) {
            $scope.issueAccountStatement.selectedSigner = signer;
          }
        });
      };
      if($scope.issueAccountStatement.selectedSigner) {
        $scope.selectedSignerChange($scope.issueAccountStatement.selectedSigner.id);
      }

      $scope.selectedOperatorChange = function(selectedOperatorID) {
        angular.forEach($scope.issueAccountStatement.operators, function(operator) {
          if(operator.id == selectedOperatorID) {
            $scope.issueAccountStatement.selectedOperator = operator;
          }
        });
      };
      if($scope.issueAccountStatement.selectedOperator) {
        $scope.selectedOperatorChange($scope.issueAccountStatement.selectedOperator.id);
      }


      reportService.normalizeReturnedReportTask($scope.issueAccountStatement);
      //$scope.issueAccountStatement = {
      //  account: {},
      //  currentTimeSelected: true,
      //};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "issueAccountStatementForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.issueAccountStatement)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.issueAccountStatement);
      });

      // Search account
      $scope.findAccount = function () {

        personFindService.findClientShareholder().then(function (data) {
          $scope.issueAccountStatement.account = {};
          $scope.issueAccountStatement.account.name = data.name;
          $scope.issueAccountStatement.account.accountId = data.accountId;
          $scope.issueAccountStatement.account.id = data.id;
          $scope.issueAccountStatement.account.accountNumber = data.accountNumber;
        });
      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.timePicker) {
          $scope.config.timePicker = widget;
          //$scope.currentTimeSelection(true);
        }
      });

      //$scope.currentTimeSelection = function (val) {
      //  if (val) {
      //    var time = new Date();
      //    $scope.issueAccountStatement.requestDate = helperFunctionsService.generateDateTime(time);
      //    $scope.config.timePicker.value(time);
      //    $scope.issueAccountStatement.requestDateObj = time;
      //
      //  } else {
      //    $scope.config.timePicker.value(null);
      //    $scope.issueAccountStatement.requestDate = null;
      //  }
      //};


    }]);
