'use strict';

angular.module('cmisApp')
  .controller('IssueTitleAbstractMemberController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'helperFunctionsService', 'operationState', 'task', 'instrumentFindService', 'SweetAlert', 'gettextCatalog', 'reportService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, helperFunctionsService, operationState, task, instrumentFindService, SweetAlert, gettextCatalog, reportService) {

      var prepareFormData = function (formData) {

        if (formData.currentTimeSelected) {
          formData.requestDate = helperFunctionsService.generateDateTime(new Date());
        } else {
          if (formData.requestDateObj) {
            formData.requestDate = helperFunctionsService.generateDateTime(formData.requestDateObj);
          }
        }

        delete formData.requestDateObj;
        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.account === null || draft.account === undefined) {
          draft.account = {};
        }
        if (draft.instrument === null || draft.instrument === undefined) {
          draft.instrument = {};
        }
        if (draft.currentTimeSelected === null || draft.currentTimeSelected === undefined) {
          draft.currentTimeSelected = true;
        }

        if(draft.selectedSigner) {
          $scope.selectedSignerID = draft.selectedSigner.id;
        }
        if(draft.selectedOperator) {
          $scope.selectedOperatorID = draft.selectedOperator.id;
        }

        return draft;
      };

      $scope.issueTitleAbstract = initializeFormData(angular.fromJson(task.draft));


      $scope.selectedSignerChange = function(selectedSignerID) {
        angular.forEach($scope.issueTitleAbstract.signers, function(signer) {
          if(signer.id == selectedSignerID) {
            $scope.issueTitleAbstract.selectedSigner = signer;
          }
        });
      };
      if($scope.issueTitleAbstract.selectedSigner) {
        $scope.selectedSignerChange($scope.issueTitleAbstract.selectedSigner.id);
      }

      $scope.selectedOperatorChange = function(selectedOperatorID) {
        angular.forEach($scope.issueTitleAbstract.operators, function(operator) {
          if(operator.id == selectedOperatorID) {
            $scope.issueTitleAbstract.selectedOperator = operator;
          }
        });
      };
      if($scope.issueTitleAbstract.selectedOperator) {
        $scope.selectedOperatorChange($scope.issueTitleAbstract.selectedOperator.id);
      }

      reportService.normalizeReturnedReportTask($scope.issueTitleAbstract);


      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "issueTitleAbstractForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.issueTitleAbstract)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.issueTitleAbstract);
      });

      // Search account
      $scope.findAccount = function () {

        personFindService.findClientShareholder().then(function (data) {
          $scope.issueTitleAbstract.account = {};
          $scope.issueTitleAbstract.account.name = data.name;
          $scope.issueTitleAbstract.account.accountId = data.accountId;
          $scope.issueTitleAbstract.account.id = data.id;
          $scope.issueTitleAbstract.account.accountNumber = data.accountNumber;
        });
      };

      $scope.$watch("issueTitleAbstract.currentTimeSelected", function(newVal, oldVal) {
        if(newVal !== oldVal) {
          $scope.resetSelectedInsturments();
        }
      });

      $scope.$watch("issueTitleAbstract.requestDateObj", function(newVal, oldVal) {
        if(newVal !== oldVal) {
          $scope.resetSelectedInsturments();
        }
      });

      $scope.resetSelectedInsturments = function() {
        $scope.issueTitleAbstract.instrument = {};
      };

      $scope.findInstrument = function () {
        instrumentFindService.findInstrument({
          params: {
            date: $scope.issueTitleAbstract.currentTimeSelected ? null : $scope.issueTitleAbstract.requestDateObj
          }
        }).then(function (data) {
          $scope.issueTitleAbstract.instrument = {};
          $scope.issueTitleAbstract.instrument.ISIN = data.isin;
          $scope.issueTitleAbstract.instrument.id = data.id;
          $scope.issueTitleAbstract.instrument.issuerName = data.issuerName;

        });
      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.timePicker) {
          $scope.config.timePicker = widget;
        }
      });

    }]);
