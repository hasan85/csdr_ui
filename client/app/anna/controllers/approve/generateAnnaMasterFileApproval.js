'use strict';

angular.module('cmisApp')
  .controller('GenerateAnnaMasterFileApproveController',
    ['$scope', 'id', 'SweetAlert', 'helperFunctionsService', 'Loader', 'appConstants', 'task', 'operationState', '$windowInstance', "taskId"
      ,function ($scope, id, SweetAlert, helperFunctionsService, Loader, appConstants, task, operationState, $windowInstance, taskId) {
          $scope.data = angular.fromJson(task.draft);

          $scope.config = {
            screenId: id,
            taskId: taskId,
            task: task,
            operationType: appConstants.operationTypes.approval,
            window: $windowInstance,
            state: operationState
          };
       }]);
