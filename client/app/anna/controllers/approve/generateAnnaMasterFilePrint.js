'use strict';

angular.module('cmisApp')
  .controller('GenerateAnnaMasterFilePrintController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', "helperFunctionsService",
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, helperFunctionsService) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };
      $scope.data = angular.fromJson(task.draft);
        $scope.exportToTxt = function () {
          var checkedItems = [];
          angular.forEach($scope.data.masterFileData.dataEntry, function (val) {
            if (val.isChecked) {
              checkedItems.push(val.asbFormat);
            }
          });
          var textfile = 'data:text/plain;charset=utf-8,' + encodeURIComponent(checkedItems.join("\r\n"));
          helperFunctionsService.saveBlobFile(textfile, "Asb" + +(new Date()));
        };

    }]);
