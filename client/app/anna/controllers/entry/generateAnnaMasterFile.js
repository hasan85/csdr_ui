'use strict';

angular.module('cmisApp')
  .controller('GenerateAnnaMasterFileController',
    ['$scope', 'id', 'SweetAlert', 'helperFunctionsService', 'Loader', 'appConstants', 'task', 'operationState', '$windowInstance',
      '$http'
      ,function ($scope, id, SweetAlert, helperFunctionsService, Loader, appConstants, task, operationState, $windowInstance,
                 $http) {
        $scope.metaData = {

        };
        var prepareFormData = function (data) {
          var result = [];
          angular.forEach(data, function (val) {
            if (val.isChecked) {
              result.push(val);
            }
          });
          return {
            masterFileData: {
              fileName: "Asb" + +(new Date()),
              dataEntry: result
            }
          };
        };
        var searchUrl = '/api/common/searchASBGenerator';
        $scope.searchData = {
          createdDate: {

          },
          isEN: true
        };
        // 1605
        $scope.data = angular.fromJson(task.draft);
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "annaMasterFileForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.completeTask(prepareFormData($scope.mainGrid.dataSource.view()));
              }
            }
          }
        };
        $scope.checkedIds = {
          length: 0
        };
        $scope.exportToTxt = function () {
          var checkedItems = [];
          angular.forEach($scope.mainGrid.dataSource.view(), function (val) {
            if (val.isChecked) {
              checkedItems.push(val.asbFormat);
            }
          });
          var textfile = 'data:text/plain;charset=utf-8,' + encodeURIComponent(checkedItems.join("\r\n"));
          helperFunctionsService.saveBlobFile(textfile, "Asb" + +(new Date()));
        };
        $scope.onSelection = function (dataItem) {
          $scope.checkedIds.isCheckAll = false;
          if (dataItem.isChecked) {
            dataItem.asbFormat = helperFunctionsService.generateAsbText(dataItem);
            $scope.checkedIds.length++;
          } else {
            $scope.checkedIds.length--;
          }
        };
        $scope.checkAll = function (isCheckAll) {
          var data = $scope.mainGrid.dataSource.view();
          if (!data.length) {
            return;
          }
          if (isCheckAll) {
            $scope.checkedIds.length = data.length;
            angular.forEach(data,function (val) {
              val.asbFormat = helperFunctionsService.generateAsbText(val);
              val.isChecked = true;
            });
          } else {
            $scope.checkedIds.length = 0;
            angular.forEach(data, function (val) {
              val.isChecked = false;
            });
          }
        };
        $scope.gridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                id: "id",
                fields: {
                  maturityDate: {type: "date"},
                  createdDate: {type: "date"},
                  modifyDate: {type: "date"}
                }
              }
            },
            transport: {
              read: function (e) {
                findAsb(e);
              }
            },
            serverPaging: true
          },
          scrollable: true,
          sortable: true,
          resizable: true,
          autoBind: false,
          columns: [
            {
              headerTemplate: "<input style='margin-top:13px !important;' ng-model='checkedIds.isCheckAll' ng-click='checkAll(checkedIds.isCheckAll)' type='checkbox' />",
              template: "<div><input type='checkbox' ng-model='dataItem.isChecked' ng-click='onSelection(dataItem)' /></div>",
              width:"40px"
            },
            {
              field: "isin",
              width: "8rem",
              title: "Isin nomre"
            },
            {
              field: "status",
              width: "80px",
              title: "Status"
            },
            {
              field: "instrumentCategory",
              title: "Q/K-ın növü",
              width: "80px"
            },
            {
              field: "CFI", //.description
              title: "CFI",
              width: "8rem",
              template: function (data) {
                var description = data.CFI.description;
                return '<div>' +
                    '<span>' +
                      description +
                    '</span>' +
                    '<md-tooltip md-direction="down">' +
                      '<span style="font-size:18px">' +
                        description +
                      '</span>' +
                    '</md-tooltip>' +
                  '</div>';
              }
            },
            {
              field: "currency",
              title: "Məzənnə",
              width: "80px"
            },
            {
              field: "maturityDate",
              title: "Son ödəmə tarixi",
              width: "9rem",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "typeofInterest",
              title: "Faizin Növü",
              width: "80px"
            },
            {
              field: "issuerName",
              width: "8rem",
              title: "Emitentin Adı",
              template: function (data) {
                var issuerName = '';
                if (data.issuerName) issuerName = data.issuerName;
                return '<div>' +
                  '<div>' +
                    issuerName +
                  '</div>' +
                  '<md-tooltip>' +
                    '<span style="font-size:18px;">'+issuerName+'</span>' +
                  '</md-tooltip>' +
                  '</div>';
              }
            },
            {
              field: "issuerLegalAddress",
              width: "80px",
              title: "Ölkə",
              template: function (data) {
                var country = data.issuerLegalAddress.country2a ? data.issuerLegalAddress.country2a : "";
                return '<span>' +
                  country +
                  '</span>';
              }
            },
            {
              field: "createdDate",
              title: "Yaranma tarixi",
              width: "9rem",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "modifyDate",
              title: "Dəyişdirilmə tarixi",
              width: "9em",
              format: "{0:dd-MMMM-yyyy}"
            },
            {
              field: "instrumentName",
              title: "FISN",
              width: "9em"
            }
          ]
        };
        $scope.findAsb = function() {
          $scope.mainGrid.dataSource.page(1);
        };
        function findAsb(e) {
          $scope.searchData.createdDate.first = $scope.searchData.createdDate.first ? $scope.searchData.createdDate.firstObj : null;
          $scope.searchData.createdDate.last = $scope.searchData.createdDate.last ? $scope.searchData.createdDate.lastObj : null;
          var searchData = angular.extend($scope.searchData, {
            sort: e.data.sort
          });
          Loader.show(true);
          $http({method: "POST", url: searchUrl, data: searchData}).success(function (data) {
            console.log("Response Data", data);
            data.data = helperFunctionsService.convertObjectToArray(data.data);
            if (data) {
              $scope.resultData = data.data;
            }
            if (data["success"] === "true") {
              $scope.checkedIds.isCheckAll = false;
              $scope.checkedIds.length = 0;
              e.success({
                Data: data.data ? data.data : [],
                Total: data.total
              });
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), "error");
            }
            Loader.show(false);
          });
        }
        $scope.$on("kendoWidgetCreated", function(event, widget) {
          $scope.mainGrid = widget;
        });

        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });
       }]);
