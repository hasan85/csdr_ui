'use strict';

angular.module('cmisApp')
  .controller('BusinessDayOpeningApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };

      var today = new Date();
      var day = today.getDate();
      var month = today.getMonth()+1;
      var year = today.getFullYear();
      $scope.currentDate = day + "-" + month + "-" + year;

      console.log($scope.currentDate);

      $scope.comingData = angular.fromJson(task.draft);

    }]);
