'use strict';

angular.module('cmisApp')
  .controller('MergerApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog, helperFunctionsService) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        labels: {
          lblOldInstruments: gettextCatalog.getString('Old Instruments'),
          lblNewInstrument: gettextCatalog.getString('New Instrument')
        }
      };

      $scope.startsWith = helperFunctionsService.startsWith;

    }]);
