'use strict';

angular.module('cmisApp')
  .controller('WithdrawalSubscriptionMoneyApprovalController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId','operationService','helperFunctionsService', 'operationState', 'task',
      'referenceDataService', 'Loader', 'gettextCatalog', 'recordsService', 'SweetAlert', '$http',
      function ($scope, $windowInstance, id, appConstants, taskId, operationService, helperFunctionsService, operationState, task,
                referenceDataService, Loader, gettextCatalog, recordsService, SweetAlert, $http) {




        // Generic search configuration for person
        $scope.searchConfig = {
          withdrawalSubscriptionMoney: {
            searchResult: {
              data: {},
              isEmpty: false
            },
            selectedWithdrawalSubscriptionMoneyIndex: false
          },
          params: {
            showSearchCriteriaBlock: true,

            config: {
              searchOnInit: true
            }
          }
        };



        var initializeFormData = function (draft) {
          return draft;
        };
        $scope.withdrawalSubscriptionMoneyData = initializeFormData(angular.fromJson(task.draft));
        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskId: taskId,
          task: task,
          operationType: appConstants.operationTypes.approval,
          window: $windowInstance,
          state: operationState
        };


        var validateForm = function () {
          var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

          return result;
        };

        var withdrawalSubscriptionMoneySelected = function (data) {
          var sResult = angular.copy($scope.withdrawalSubscriptionMoney.searchResult.data.data);
          for (var i = 0; i < sResult.length; i++) {
            if (sResult[i].ID == data.ID) {
              $scope.withdrawalSubscriptionMoney.selectedWithdrawalSubscriptionMoneyIndex = i;
              break;
            }
          }
        };


        $scope.withdrawalSubscriptionMoneySelected = withdrawalSubscriptionMoneySelected;

        var _params = {
          searchUrl: "/api/corporate-actions/getIssuerPayments/",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {},
            searchOnInit: false
          },
          criteriaEnabled: true
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _withdrawalSubscriptionMoney = {
          formName: 'withdrawalSubscriptionMoneyFindForm',
          form: {},
          data: {},

          searchResult: {
            data: null,
            isEmpty: false
          },

          selectedWithdrawalSubscriptionMoneyIndex: false,
          withdrawalSubscriptionMoneySelected: withdrawalSubscriptionMoneySelected,
          withdrawalSubscriptionMoneyDetails: false
        };

        $scope.withdrawalSubscriptionMoney = angular.merge($scope.searchConfig.withdrawalSubscriptionMoney, _withdrawalSubscriptionMoney);

        var findWithdrawalSubscriptionMoney = function (e) {

          //$scope.person.withdrawalSubscriptionMoneyDetails = false;

          if ($scope.withdrawalSubscriptionMoney.searchResultGrid !== undefined) {
            $scope.withdrawalSubscriptionMoney.searchResultGrid.refresh();
          }

          $scope.withdrawalSubscriptionMoney.selectedWithdrawalSubscriptionMoneyIndex = false;

          if ($scope.withdrawalSubscriptionMoney.formName.$dirty) {
            $scope.withdrawalSubscriptionMoney.formName.$submitted = true;
          }

          if ($scope.withdrawalSubscriptionMoney.formName.$valid || !$scope.withdrawalSubscriptionMoney.formName.$dirty) {

            $scope.withdrawalSubscriptionMoney.searchResult.isEmpty = false;

            var formData = angular.copy($scope.withdrawalSubscriptionMoney.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }

            var requestData = angular.extend(formData, {
              //instrumentID: $scope.withdrawalSubscriptionMoneyData.value
              instrumentID: 3815
            });

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.withdrawalSubscriptionMoney.searchResult.data = data;
              } else {
                $scope.withdrawalSubscriptionMoney.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };


        $scope.withdrawalSubscriptionMoney.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total"
            },
            transport: {
              read: function (e) {
                findWithdrawalSubscriptionMoney(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          filterable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [

            {
              field: "name",
              title: gettextCatalog.getString("Name"),
              width: '10rem'
            },
            {
              field: "accountNumber",
              title: gettextCatalog.getString("Account Number"),
              width: '10rem'

            },
            {
              field: "withdrawalAmount",
              title: gettextCatalog.getString("Withdrawal Amount"),
              width: '10rem'
            },
            {
              field: "tradeAmount",
              title: gettextCatalog.getString("Trade Amount"),
              width: '10rem'
            },
            {
              field: "SubscriptionAmount",
              title: gettextCatalog.getString("Subscription Amount"),
              width: '10rem'
            },

            {
              field: "iban",
              title: gettextCatalog.getString("IBAN"),
              width: '10rem'
            }
          ]
        };


        $scope.withdrawalSubscriptionMoneySelected = withdrawalSubscriptionMoneySelected;


        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };

        // $scope.resetForm = function () {
        //
        //   $scope.withdrawalSubscriptionMoney.form.name = null;
        //   $scope.withdrawalSubscriptionMoney.form.accountNumber = null;
        //   $scope.withdrawalSubscriptionMoney.form.amount = null;
        //   $scope.withdrawalSubscriptionMoney.form.iban = null;
        // };


        $scope.findWithdrawalSubscriptionMoney = function () {

          var formValidationResult = validateForm(angular.copy($scope.withdrawalSubscriptionMoney.form));
          if (formValidationResult.success) {
            if ($scope.withdrawalSubscriptionMoney.searchResultGrid) {
              $scope.withdrawalSubscriptionMoney.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.withdrawalSubscriptionMoney.searchResultGrid) {
            $scope.withdrawalSubscriptionMoney.searchResultGrid = widget;
          }
        });



        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask($scope.deMergerData);
        });


        // If task saved as draft get saved data
        if (operationState == appConstants.operationStates.active) {
          if (task.draft) {
            //$scope.deMergerData = angular.fromJson(task.draft);
          }
        }


      }]);
