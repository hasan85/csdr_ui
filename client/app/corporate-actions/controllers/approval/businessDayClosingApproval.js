'use strict';

angular.module('cmisApp')
  .controller('BusinessDayClosingApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };

      $scope.comingData = angular.fromJson(task.draft);

    }]);
