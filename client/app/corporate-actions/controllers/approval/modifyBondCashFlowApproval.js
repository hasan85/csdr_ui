'use strict';

angular.module('cmisApp')
  .controller('ModifyBondCashFlowApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog, helperFunctionsService) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };

      $scope.startsWith = helperFunctionsService.startsWith;

    }]);
