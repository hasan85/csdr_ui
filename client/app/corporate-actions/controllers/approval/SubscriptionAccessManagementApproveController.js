'use strict';

angular.module('cmisApp').controller('SubscriptionAccessManagementApprovalController', ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {

    $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
    };

    $scope.taskData = angular.copy(angular.fromJson(task.draft));

    $scope.selectedSubscription = $scope.taskData.subscriptions ? $scope.taskData.subscriptions[0] : {};

    $scope.changeSelectedSubscription = function() {
        $scope.selectedSubscriptionModel = $scope.selectedSubscription;
    };
    $scope.changeSelectedSubscription();

}]);
