'use strict';

angular.module('cmisApp')
  .controller('ReturnOfCapitalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task','SweetAlert','gettextCatalog',
    'referenceDataService', 'Loader', 'instrumentDeletionService', 'personFindService', 'manageInstrumentService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task,SweetAlert,gettextCatalog,
              referenceDataService, Loader, instrumentDeletionService, personFindService, manageInstrumentService) {

      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }

        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }

        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }

        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }

        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        return formData;
      };

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit :true,

      };


        var initializeFormData = function (draft) {
            if (!draft) {
                draft = {}
            }
            if (draft.issuer === null || draft.issuer === undefined) {
                draft.issuer = {};
            }
            if (draft.instrument === null || draft.instrument === undefined) {
                draft.instrument = {};
            }
            if (draft.inTreasureQuantity === null || draft.inTreasureQuantity === undefined) {
                draft.inTreasureQuantity = null;
            }
            return draft;
        };

        $scope.returnOfCapitalData = initializeFormData(angular.fromJson(task.draft));

      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "returnOfCapitalDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = prepareFormData(angular.copy($scope.returnOfCapitalData));
                $scope.config.completeTask(formData);
              }
              else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.returnOfCapitalData);
      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
          //$scope.returnOfCapitalData = angular.fromJson(task.draft);
        }
      }

      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data
        };

          referenceDataService.normalizeReturnedRecordTask($scope.returnOfCapitalData, $scope.metaData);

        Loader.show(false);

      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {

          $scope.returnOfCapitalData.issuer.name = data.name;
          $scope.returnOfCapitalData.issuer.accountId = data.accountId;
          $scope.returnOfCapitalData.issuer.id = data.id;
          $scope.returnOfCapitalData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
        });
      };

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.returnOfCapitalData.instrument.ISIN = data.isin;
          $scope.returnOfCapitalData.instrument.id = data.id;
          $scope.returnOfCapitalData.instrument.issuerName = data.issuerName;
          $scope.returnOfCapitalData.instrument.CFI = data.CFI;

          Loader.show(true);
          manageInstrumentService.getInTreasureQuantity(data.id).then(function (data) {
            $scope.returnOfCapitalData.inTreasureQuantity = parseInt(data);
            Loader.show(false);
          });


        });
      };

      $scope.compareQuantities = function (val) {

        if (val > $scope.returnOfCapitalData.inTreasureQuantity) {
          return false;
        }
        return true;
      }

    }]);
