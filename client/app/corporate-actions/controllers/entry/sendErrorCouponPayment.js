'use strict';

angular.module('cmisApp')
  .controller('SendErrorCouponPaymentController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, $http) {

        $scope.metaData = {
          currencies: []
        };


        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });

        var prepareFormData = function (viewModel) {
          viewModel.currency = angular.fromJson(viewModel.currency);
          viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);
          return viewModel;
        };


        $scope.data = angular.fromJson(task.draft);

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "enterBankStatementDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {

                  var data = angular.copy($scope.data)

                  data.recordDate = helperFunctionsService.generateDateTime(data.recordDate);
                  data.valueDate = helperFunctionsService.generateDateTime(data.valueDate);


                  $scope.config.completeTask(prepareFormData(data));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        $scope.findPaidCouponPayment = function () {

          $kWindow.open({
            title: gettextCatalog.getString('Find Instrument'),
            actions: ['Close'],
            isNotTile: true,
            width: '90%',
            height: '90%',
            pinned: true,
            modal: true,
            templateUrl: 'app/corporate-actions/templates/search-paid-coupon-payment.html',
            controller: 'SearchPaidCouponPaymentController',
            resolve: {
              data: function () {
                return {
                  onselect: function (data) {

                    $scope.data = data

                    Loader.show(true);
                    $http({
                      method: 'POST',
                      url: '/api/instruments/getCouponPaymentRecords/',
                      data: {couponPaymentID: data.id}
                    }).success(function (data) {

                      $scope.selectedRecords = data.data
                      Loader.show(false)

                    });

                  }
                }
              }
            }
          });

        };

        var emptyCouponPaymentRow = {
          amount: 0,
          recordDate: '',
          valueDate: ''
        }

        $scope.addNewCouponPayment = function () {
          $scope.data.vmCouponPayment.push(angular.copy(emptyCouponPaymentRow))
        }

        $scope.removePayment = function (index) {
          $scope.data.vmCouponPayment.splice(index, 1)
        }

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
