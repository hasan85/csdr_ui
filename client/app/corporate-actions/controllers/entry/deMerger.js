'use strict';

angular.module('cmisApp')
  .controller('DeMergerController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'Loader', 'demergerService', 'manageInstrumentService', 'gettextCatalog', 'personFindService', 'SweetAlert', 'issueRegistrationService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task,
              referenceDataService, Loader, demergerService, manageInstrumentService, gettextCatalog, personFindService, SweetAlert, issueRegistrationService) {

      var instrumentFormTemplate = manageInstrumentService.getInstrumentFormTemplate();

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit :true,

      };

      // metadata
      $scope.metaData = {
        sel1: [],
        sel2: [],
        sel3: [],
        sel4: [],
        sel5: [],
        sel6: [],
        convertableStockes: null
      };

      // Initialize local variables
      var commonStockConfig, convertibleStockConfig, bondConfig, mutualFundConfig, subscriptionConfig, warrantConfig = {};


      $scope.deMergerData = {
        oldSecurity: {},
        issuer: {},
        instruments: null

      };

      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "deMergerDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;

              var isPartialFormsValid = true;
              var selectedNewInstrumentCount = 0;

              if ($scope.deMergerData.instruments) {
                for (var i = 0; i < $scope.deMergerData.instruments.length; i++) {

                  var newInstruments = $scope.deMergerData.instruments[i].newInstruments;
                  if (newInstruments) {
                    for (var j = 0; j < newInstruments.length; j++) {
                      selectedNewInstrumentCount++;
                      newInstruments[j].partialFormName.$submitted = true;
                      if (newInstruments[j].partialFormName.$invalid) {
                        isPartialFormsValid = false;
                      }
                    }
                  }
                }
              }

              if ($scope.config.form.name.$valid && isPartialFormsValid) {

                if (!$scope.deMergerData.instruments) {
                  SweetAlert.swal('', gettextCatalog.getString('You have to select at leas one instrument!'), 'error');

                }
                else if (selectedNewInstrumentCount == 0) {
                  SweetAlert.swal('', gettextCatalog.getString('You have to de-merge at leas one instrument!'), 'error');
                }
                else {
                  var formData = demergerService.prepareFormData($scope.deMergerData);
                  $scope.config.completeTask(formData);
                }
              }
            }
          }
        },
        labels: {
          lblCFIWindowTitle: gettextCatalog.getString('CFI Generate'),
          lblCategory: gettextCatalog.getString('Category'),
          lblGroup: gettextCatalog.getString('Group'),
          lblAttribute: gettextCatalog.getString('Attribute')
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.deMergerData);
      });

      //Get metadata
      Loader.show(true);

      demergerService.getMetaData().then(function (data) {

        $scope.metaData = {
          fractionTreatmentClasses: data.fractionTreatmentClasses,
          registrationAuthorityClasses: data.registrationAuthorityClasses,
          placementClasses: data.placementClasses,
          identificatorClasses: data.identificatorClasses,
          currencies: data.currencies,
          securityGroups: data.securityGroups,
          sel1: data.CFIcategories,
          couponPaymentMethodClasses: data.couponPaymentMethodClasses,
          convertableStockes: null
        };

        var payload = angular.fromJson(task.draft).config;

        if (payload.haircutRatio) {
          instrumentFormTemplate.haircutRatio = payload.haircutRatio * 100;
        }
        if (payload.conventionalDayCount) {
          instrumentFormTemplate.conventionalDayCount = payload.conventionalDayCount;
        }

        // Get config data
        var commonStockConfigRaw = payload['commonStockConfiguration'] ? angular.fromJson(payload['commonStockConfiguration']) : null;
        var convertibleStockConfigRaw = payload['convertibleStockConfiguration'] ? angular.fromJson(payload['convertibleStockConfiguration']) : null;
        var bondConfigRaw = payload['bondConfiguration'] ? angular.fromJson(payload['bondConfiguration']) : null;

        var mutualFundConfigurationRaw = payload['mutualFundConfiguration'] ? angular.fromJson(payload['mutualFundConfiguration']) : null;
        var warrantConfigurationRaw = payload['warrantConfiguration'] ? angular.fromJson(payload['warrantConfiguration']) : null;
        var subscriptionConfigurationRaw = payload['subscriptionConfiguration'] ? angular.fromJson(payload['subscriptionConfiguration']) : null;

        // Prepare config data
        commonStockConfig = commonStockConfigRaw ? manageInstrumentService.generateFormConfig(commonStockConfigRaw) : null;
        convertibleStockConfig = convertibleStockConfigRaw ? manageInstrumentService.generateFormConfig(convertibleStockConfigRaw) : null;
        bondConfig = bondConfigRaw ? manageInstrumentService.generateFormConfig(bondConfigRaw) : null;
        mutualFundConfig = mutualFundConfigurationRaw ? manageInstrumentService.generateFormConfig(mutualFundConfigurationRaw) : null;
        warrantConfig = warrantConfigurationRaw ? manageInstrumentService.generateFormConfig(warrantConfigurationRaw) : null;
        subscriptionConfig = subscriptionConfigurationRaw ? manageInstrumentService.generateFormConfig(subscriptionConfigurationRaw) : null;

        instrumentFormTemplate.config.fields = commonStockConfig;

        Loader.show(false);

      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
          //$scope.deMergerData = angular.fromJson(task.draft);
        }
      }

      // Search issuer
      $scope.findCurrentIssuer = function () {
        personFindService.findIssuer().then(function (data) {

          $scope.deMergerData.issuer.name = data.name;
          $scope.deMergerData.issuer.accountId = data.accountId;
          $scope.deMergerData.issuer.id = data.id;
          $scope.deMergerData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
          instrumentFindService.findInstrumentByData({'issuerId': data.id}, 'findInstrument/').then(function (data) {

            if (data) {

              $scope.deMergerData.allInstruments = data;

              $scope.deMergerData.instruments = [];

              for (var i = 0; i < data.length; i++) {

                data[i].ISIN = data[i].isin;
                delete data[i].isin;
                $scope.deMergerData.instruments.push({
                  oldInstrument: data[i],
                  newInstruments: [],
                  selectSelfAlso: false
                });

              }
            }
            else {

              SweetAlert.swal('', gettextCatalog.getString('There is not any instrument of this issuer'), 'error');
            }

          });

        });

      };


      // Find issuer
      $scope.findIssuer = function (instrument) {

        personFindService.findIssuer().then(function (data) {

          instrument.issuer.name = data.name;
          instrument.issuer.id = data.id;
          instrument.issuer.accountId = data.accountId;
          instrument.issuer.accountNumber = data.accountNumber;

          if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_W') || helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_S')) {
            issueRegistrationService.getIssuerEquities(data.id).then(function (data1) {
              instrument.metaData.convertableStockes = data1;
            });
          }
        });


      };

      $scope.currentInstrumentSelectionChange = function (dm) {

        if (dm.selectSelfAlso == true) {

          dm.newInstruments.push({
            id: dm.oldInstrument.id,
            ISIN: dm.oldInstrument.ISIN,
            CFI:dm.oldInstrument.CFI,
            isCurrent: true
          });

          dm.indexOfSelectedCurrentInstrument = dm.newInstruments.length - 1;
        }
        else {

          dm.newInstruments.splice(dm.indexOfSelectedCurrentInstrument, 1);
        }

      };

      $scope.addNewInstrument = function (model) {

        var newInstrumentForm = angular.copy(instrumentFormTemplate);
        newInstrumentForm.partialFormName = model.oldInstrument.ISIN + "_" + (model.newInstruments.length);
        model.newInstruments.push(newInstrumentForm);
      };

      $scope.removeNewInstrument = function (model) {
        model.splice(model.length - 1, 1);
      };


      // Query CFI hierarchy
      $scope.getChildOptions = function (classId, index) {

        if (index != 6) {
          var startIndex;

          if (classId == '') {
            startIndex = index + 1;
          } else {

            startIndex = index + 2;
            classId = parseInt(classId);

            if (classId > 0) {

              Loader.show(true);

              referenceDataService.getCFIClasses(classId).then(function (data) {

                var nextClassesIndex = index + 1;
                $scope.metaData['sel' + nextClassesIndex] = data;

                Loader.show(false);

              });
            }
          }
          if (startIndex <= 6) {
            for (var i = startIndex; i <= 6; i++) {
              $scope.metaData['sel' + i] = null;
            }
          }
        }

      };

      // Get generated CFI
      $scope.generateCFI = function (window, instrument) {

        instrument.cfiForm.name.$submitted = true;

        if (instrument.cfiForm.name.$valid) {

          for (var i = 0; i < $scope.metaData.sel6.length; i++) {

            var searchedID = parseInt(instrument.cfiForm.sel6);
            var currentID = parseInt($scope.metaData.sel6[i]['id']);

            if (searchedID === currentID) {

              instrument.CFI = $scope.metaData.sel6[i];
              window.close();

              instrument.cfiForm.sel6 =
                instrument.cfiForm.sel5 =
                  instrument.cfiForm.sel4 =
                    instrument.cfiForm.sel3 =
                      instrument.cfiForm.sel2 = '';


              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_D')) {
                instrument.config.fields = bondConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_W')) {


                instrument.config.fields = warrantConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_S')) {


                instrument.config.fields = subscriptionConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_S')) {

                instrument.config.fields = commonStockConfig;
              }

              if (
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_P') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_R') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_C') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_F') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_V')

              ) {
                instrument.config.fields = convertibleStockConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_U')) {
                instrument.config.fields = mutualFundConfig;
              }

              break;
            }
          }

        }
      };

    }]);
