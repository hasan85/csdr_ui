'use strict';

angular.module('cmisApp')
  .controller('DecreaseCapitalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'FileUploader',
    'SweetAlert', 'gettextCatalog', 'Loader', '$http', 'batchEntryOfAllotmentService', '$filter', 'managePersonService', 'referenceDataService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, FileUploader,
              SweetAlert, gettextCatalog, Loader, $http, batchEntryOfAllotmentService, $filter, managePersonService, referenceDataService) {

      $scope.operationData = angular.fromJson(task.draft);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "operationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.form.data = angular.copy($scope.operationData);
                $scope.config.completeTask($scope.config.form.data);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.batchEntryData);
      });

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findStock',
        searchOnInit: true

      };
      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.operationData.instrumentID = data.id;
          $scope.operationData.instrument = {};
          $scope.operationData.instrument.ISIN = data.isin;
          $scope.operationData.instrument.issuerName = data.issuerName;

          $http.get('api/corporate-actions/getDecreasedCapitalData/' + data.id).then(function (result) {
            $scope.operationData.currentBuyBackQty = result.data.data.currentBuyBackQty;
            $scope.operationData.currentPlacedQty = result.data.data.currentPlacedQty;
            $scope.operationData.decreasedPlacedItems = result.data.data.decreasedPlacedItems;
          });

        });

      };

    }]);
