'use strict';

angular.module('cmisApp')
  .controller('CouponPaymentScheduleManagerController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'manageInstrumentService',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, manageInstrumentService) {

        $scope.metaData = {
          currencies: []
        };


        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });

        var prepareFormData = function (viewModel) {
          viewModel.currency = angular.fromJson(viewModel.currency);
          viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);
          return viewModel;
        };


        $scope.data = angular.fromJson(task.draft);

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "enterBankStatementDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {

                  var data = angular.copy($scope.data)
                  if (data.vmCouponPayment.length) {

                    data.vmCouponPayment.forEach(function (row) {
                      row.recordDate = helperFunctionsService.generateDateTime(row.recordDate);
                      row.valueDate = helperFunctionsService.generateDateTime(row.valueDate);

                      delete row.recordDateObj
                      delete row.valueDateObj
                    })
                  }

                  console.log(data)

                  $scope.config.completeTask(prepareFormData(data));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        $scope.findBondStatement = function () {

          $kWindow.open({
            title: gettextCatalog.getString('Find Instrument'),
            actions: ['Close'],
            isNotTile: true,
            width: '90%',
            height: '90%',
            pinned: true,
            modal: true,
            templateUrl: 'app/corporate-actions/templates/search-bond.html',
            controller: 'SearchBondController',
            resolve: {
              data: function () {
                return {
                  onselect: function (data) {

                    $scope.data.instrument = data;

                    manageInstrumentService.getBondCashFlow(data.id).then(function (cashFlow) {

                      if (cashFlow && cashFlow.success == "true" && cashFlow.data && cashFlow.data.length > 0) {
                        $scope.data.vmCouponPayment = []
                        cashFlow.data.forEach(function (obj) {
                            $scope.data.vmCouponPayment.push(obj)
                        })

                      } else {
                        $scope.data.vmCouponPayment = []
                        $scope.addNewCouponPayment()
                      }

                      Loader.show(false);
                    });

                  }
                }
              }
            }
          });

        };

        var emptyCouponPaymentRow = {
          interestAmount: 0,
          principalAmount: 0,
          recordDate: '',
          valueDate: ''
        }

        $scope.addNewCouponPayment = function () {
          $scope.data.vmCouponPayment.push(angular.copy(emptyCouponPaymentRow))
        }

        $scope.removePayment = function (index) {
          $scope.data.vmCouponPayment.splice(index, 1)
        }

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
