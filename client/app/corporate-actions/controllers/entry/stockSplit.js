'use strict';

angular.module('cmisApp')
  .controller('StockSplitController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', "referenceDataService", 'SweetAlert',
    'Loader', 'gettextCatalog', 'personFindService', 'stockSplitService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, SweetAlert,
              Loader, gettextCatalog, personFindService, stockSplitService) {

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.issuer === null || draft.issuer === undefined) {
          draft.issuer = {};
        }
        if (draft.instrument === null || draft.instrument === undefined) {
          draft.instrument = {};
        }
        return draft;
      };

      $scope.stockSplitData = initializeFormData(angular.fromJson(task.draft));

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit: true
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "stockSplitDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;

              if ($scope.config.form.name.$valid) {
                var formData = stockSplitService.prepareFormData(angular.copy($scope.stockSplitData));
                $scope.config.completeTask(formData);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.stockSplitData);
      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
          //$scope.stockSplitData = angular.fromJson(task.draft);
        }
      }

      Loader.show(true);
      stockSplitService.getMetaData().then(function (data) {

        $scope.metaData = {
          fractionTreatmentClasses: data.fractionTreatmentClasses,
          registrationAuthorityClasses: data.registrationAuthorityClasses
        };
        referenceDataService.normalizeReturnedRecordTask($scope.stockSplitData, $scope.metaData);

        Loader.show(false);
      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {

          $scope.stockSplitData.issuer.name = data.name;
          $scope.stockSplitData.issuer.accountId = data.accountId;
          $scope.stockSplitData.issuer.id = data.id;
          $scope.stockSplitData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
        });
      };

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.stockSplitData.instrument.ISIN = data.isin;
          $scope.stockSplitData.instrument.id = data.id;
          $scope.stockSplitData.instrument.issuerName = data.issuerName;
          $scope.stockSplitData.instrument.CFI = data.CFI;

        });
      };

    }]);
