'use strict';

angular.module('cmisApp')
  .controller('SubscriptionOrderController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
      'personFindService', 'helperFunctionsService', 'operationState', 'task', 'SweetAlert', 'gettextCatalog', 'reportService',
      function ($scope, $windowInstance, id, appConstants, operationService,
                personFindService, helperFunctionsService, operationState, task, SweetAlert, gettextCatalog, reportService) {

        var prepareFormData = function (formData) {
          if (formData.currentTimeSelected) {
            formData.requestDate = helperFunctionsService.generateDateTime(new Date());
          } else {
            if (formData.requestDateObj) {
              formData.requestDate = helperFunctionsService.generateDateTime(formData.requestDateObj);
            }
          }

          delete formData.requestDateObj;
          return formData;
        };

        var initializeFormData = function (draft) {
          if (!draft) {
            draft = {}
          }
          if (draft.clientAccount === null || draft.clientAccount === undefined) {
            draft.clientAccount = {};
          }
          if (draft.currentTimeSelected === null || draft.currentTimeSelected === undefined) {
            draft.currentTimeSelected = true;
          }

          if(draft.subscription) {
            $scope.subscriptionID = draft.subscription.id;
          }

          return draft;
        };

        $scope.subscription = initializeFormData(angular.fromJson(task.draft));

        $scope.subscriptionChange = function(subscriptionID) {
          angular.forEach($scope.subscription.subscriptions, function(subscription) {
            if(subscription.id == subscriptionID) {
              $scope.subscription.subscription = subscription;
            }
          });
        };
        if($scope.subscription.subscription) {
          $scope.subscriptionChange($scope.subscription.subscription.id);
        }

        reportService.normalizeReturnedReportTask($scope.subscription);

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "subscriptionForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  $scope.config.completeTask(prepareFormData(angular.copy($scope.subscription)));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };

        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask($scope.subscription);
        });

        // Search depo client ccount
        $scope.findAccount = function () {

          personFindService.findClientShareholder().then(function (data) {
            $scope.subscription.clientAccount = {};
            $scope.subscription.clientAccount.name = data.name;
            $scope.subscription.clientAccount.accountId = data.accountId;
            $scope.subscription.clientAccount.id = data.id;
            $scope.subscription.clientAccount.accountNumber = data.accountNumber;
          });
        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.config.timePicker) {
            $scope.config.timePicker = widget;
            //$scope.currentTimeSelection(true);
          }
        });

      }]);
