'use strict';

angular.module('cmisApp')
  .controller('CashDividendPaymentController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'corporateActionFindService', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, corporateActionFindService, SweetAlert) {

        var initializeFormData = function (draft) {
            if (!draft) {
                draft = {};
            }
            if (draft.cashDividend === null) {
                draft.cashDividend = {};
            }

            return draft;
        };
      var prepareFormData = function (formData) {

        if (formData.cashDividend.registrationDate) {
          formData.cashDividend.registrationDate = helperFunctionsService.generateDateTime(formData.cashDividend.registrationDate);
        }

        if (formData.cashDividend.entitlementSecurity) {
          for (var i = 0; i < formData.cashDividend.entitlementSecurity.length; i++) {
            var obj = formData.cashDividend.entitlementSecurity[i];
            var instrumentBuf = {
              name: obj.name,
              ISIN: obj.ISIN,
              issuer: {
                name: obj.issuer.name,
              }
            };
            formData.cashDividend.entitlementSecurity[i] = instrumentBuf;
          }
        }

        if (formData.cashDividend.underlyingSecurity) {
          for (var i = 0; i < formData.cashDividend.underlyingSecurity.length; i++) {
            var obj = formData.cashDividend.underlyingSecurity[i];
            var instrumentBuf = {
              name: obj.name,
              ISIN: obj.ISIN,
              issuer: {
                name: obj.issuer.name,
              }
            };
            formData.cashDividend.underlyingSecurity[i] = instrumentBuf;

          }
        }
        return formData;
      };
      $scope.cashDividendPaymentData = initializeFormData(angular.fromJson(task.draft));

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "cashDividendPaymentDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = prepareFormData(angular.copy($scope.cashDividendPaymentData));
                $scope.config.completeTask(formData);
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.cashDividendPaymentData);
      });

        referenceDataService.normalizeReturnedRecordTask($scope.cashDividendPaymentData);

      // Search corporate action
      $scope.findCashDividend = function () {
        corporateActionFindService.findCashDividend().then(function (data) {

          $scope.cashDividendPaymentData.cashDividend = data;
          Loader.show(true);
          corporateActionFindService.getCorporateActionById(data.id).then(function (res) {

            Loader.show(false);
            if (res && res.success === "true") {
              $scope.cashDividendPaymentData.cashDividend.underlyingSecurity = res.data.underlyingSecurity;
              $scope.cashDividendPaymentData.cashDividend.entitlementSecurity = res.data.entitlementSecurity;
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
            }
          });
        });
      };
    }]);
