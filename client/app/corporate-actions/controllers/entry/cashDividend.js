'use strict';

angular.module('cmisApp')
  .controller('CashDividendController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task','SweetAlert','gettextCatalog',
    'referenceDataService', 'Loader', 'instrumentDeletionService', 'personFindService', 'cashDividendService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task,SweetAlert ,gettextCatalog,
              referenceDataService, Loader, instrumentDeletionService, personFindService, cashDividendService) {

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findStock',
        searchOnInit: true

      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.issuer === null || draft.issuer === undefined) {
          draft.issuer = {};
        }
        if (draft.instrument === null || draft.instrument === undefined) {
          draft.instrument = {};
        }
        return draft;
      };

      $scope.cashDividendData = initializeFormData(angular.fromJson(task.draft));


      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "cashDividendDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = cashDividendService.prepareFormData(angular.copy($scope.cashDividendData));
                $scope.config.completeTask(formData);
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.cashDividendData);
      });

      Loader.show(true);
      cashDividendService.getMetaData().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data.registrationAuthorityClasses
        };
        referenceDataService.normalizeReturnedRecordTask($scope.cashDividendData, data);
        Loader.show(false);
      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {
          $scope.cashDividendData.issuer.name = data.name;
          $scope.cashDividendData.issuer.accountId = data.accountId;
          $scope.cashDividendData.issuer.id = data.id;
          $scope.cashDividendData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
        });
      };

      // Search instrument
      $scope.findInstrument = function () {
        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.cashDividendData.instrument.ISIN = data.isin;
          $scope.cashDividendData.instrument.id = data.id;
          $scope.cashDividendData.instrument.issuerName = data.issuerName;
          $scope.cashDividendData.instrument.instrumentName = data.instrumentName;
          $scope.cashDividendData.instrument.CFI = data.CFI;
        });
      };

    }]);
