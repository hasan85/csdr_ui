'use strict';

angular.module('cmisApp')
  .controller('WithdrawalSubscriptionMoneyController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService','helperFunctionsService', 'operationState', 'task',
      'referenceDataService', 'Loader', 'gettextCatalog', '$rootScope', 'recordsService', 'SweetAlert', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, helperFunctionsService, operationState, task,
                referenceDataService, Loader, gettextCatalog, $rootScope, recordsService, SweetAlert, $http) {

          // Generic search configuration for person
          $scope.searchConfig = {
              withdrawalSubscriptionMoney: {
                  searchResult: {
                      data: {},
                      isEmpty: false
                  }
              },
              issuerPayments: {
                  searchResult: {
                      data: {},
                      isEmpty: false
                  }
              },
              params: {
                  showSearchCriteriaBlock: true,

                  config: {
                      searchOnInit: true
                  }
              }
          };

          var initializeFormData = function (draft) {
              if (!draft) {
                  draft = {};
              }

              return draft;
          };

          $scope.withdrawalSubscriptionMoneyData = initializeFormData(angular.fromJson(task.draft));
          $scope.issuerPaymentsData = initializeFormData(angular.fromJson(task.draft));

          //Initialize scope variables [[
          $scope.config = {
              screenId: id,
              taskKey: id,
              task: task,
              form: {
                  name: "withdrawalSubscriptionMoneyDataForm",
                  data: {}
              },
              operationType: appConstants.operationTypes.entry,
              window: $windowInstance,
              state: operationState,
              buttons: {
                  complete: {
                      click: function () {
                          $scope.config.form.name.$submitted = true;
                          if ($scope.config.form.name.$valid) {
                              var formBuf = angular.extend($scope.withdrawalSubscriptionMoneyData, $scope.issuerPaymentsData);
                              $scope.config.completeTask(formBuf);
                          }
                      }
                  }
              }
          };


          var _params = {
              searchUrl: "/api/corporate-actions/getRemainingPayments/",
              searchUrl1: "/api/corporate-actions/getIssuerPayments/",
              showSearchCriteriaBlock: false,
              config: {
                  criteriaForm: {},
                  searchOnInit: false
              },
              criteriaEnabled: true
          };

          $scope.params = angular.merge(_params, $scope.searchConfig.params);


          var _withdrawalSubscriptionMoney = {
              formName: 'withdrawalSubscriptionMoneyFindForm',
              form: {},
              data: {},

              searchResult: {
                  data: null,
                  isEmpty: false
              }
          };

          var _issuerPayments = {
              formName: 'isuuerPaymentsForm',
              form: {},
              data: {},

              searchResult: {
                  data: null,
                  isEmpty: false
              }
          };

          $scope.withdrawalSubscriptionMoney = angular.merge($scope.searchConfig.withdrawalSubscriptionMoney, _withdrawalSubscriptionMoney);
          $scope.issuerPayments = angular.merge($scope.searchConfig.issuerPayments, _issuerPayments);

        var findWithdrawalSubscriptionMoney = function (e) {

          if ($scope.withdrawalSubscriptionMoney.searchResultGrid !== undefined) {
            $scope.withdrawalSubscriptionMoney.searchResultGrid.refresh();
          }

          if ($scope.withdrawalSubscriptionMoney.formName.$dirty) {
            $scope.withdrawalSubscriptionMoney.formName.$submitted = true;
          }

          if ($scope.withdrawalSubscriptionMoney.formName.$valid || !$scope.withdrawalSubscriptionMoney.formName.$dirty) {

            $scope.withdrawalSubscriptionMoney.searchResult.isEmpty = false;

            var formData = angular.copy($scope.withdrawalSubscriptionMoney.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }


            var requestData = angular.extend(formData, {
              //instrumentID: $scope.withdrawalSubscriptionMoneyData.value
              instrumentID: 3939
            });
            console.log($scope.withdrawalSubscriptionMoneyData);

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST',
              url: $scope.params.searchUrl,
              data: {data: requestData}}).success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.withdrawalSubscriptionMoney.searchResult.data = data;
              } else {
                $scope.withdrawalSubscriptionMoney.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };

        var findIssuerPayments = function (e) {

          if ($scope.issuerPayments.searchResultGrid !== undefined) {
            $scope.issuerPayments.searchResultGrid.refresh();
          }

          // if ($scope.issuerPayments.formName.$dirty) {
          //   $scope.issuerPayments.formName.$submitted = true;
          // }

          if (true) {

            $scope.issuerPayments.searchResult.isEmpty = false;

            var formData = angular.copy($scope.issuerPayments.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }


            var requestData = angular.extend(formData, {
              //instrumentID: $scope.withdrawalSubscriptionMoneyData.value
              instrumentID: 3815
            });
            console.log($scope.issuerPaymentsData);

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST',
              url: $scope.params.searchUrl1,
              data: {data: requestData}}).success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.issuerPayments.searchResult.data = data;
              } else {
                $scope.issuerPayments.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };


        $scope.withdrawalSubscriptionMoney.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  paymentDate: {type: "date"}
                }
              }
            },
            sort: { field: "paymentDate", dir: "desc" },
            transport: {
              read: function (e) {
                findWithdrawalSubscriptionMoney(e);
              }
            },

            serverPaging: true,
            serverSorting: true

          },
          selectable: true,
          filterable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "paymentDate",
              title: gettextCatalog.getString("Payment Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy}",
            },
            {
              field: "referenceNumber",
              title: gettextCatalog.getString("Reference Number"),
              width: "10rem"
            },
            {
              field: "destination",
              title: gettextCatalog.getString("Destination"),
              width: "10rem"
            },
            {
              field: "amount",
              title: gettextCatalog.getString("Amount"),
              width: "10rem"
            },
            {
              field: "paymentClass.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Payment Class"),
              width: "10rem"

            },
            {
              field: "currency",
              title: gettextCatalog.getString("Currency"),
              width: "10rem"
            },
            {
              field: "status.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Status"),
              width: "10rem"

            },
          ]
        };


        $scope.issuerPayments.mainGridOptions = {
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  paymentDate: {type: "date"}
                }
              }
            },
            sort: { field: "paymentDate", dir: "desc" },
            transport: {
              read: function (e) {
                findIssuerPayments(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          filterable: true,
          scrollable: true,
          pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "paymentDate",
              title: gettextCatalog.getString("Payment Date"),
              width: "10rem",
              format: "{0:dd-MMMM-yyyy}",
            },
            {
              field: "referenceNumber",
              title: gettextCatalog.getString("Reference Number"),
              width: "10rem"
            },
            {
              field: "destination",
              title: gettextCatalog.getString("Destination"),
              width: "10rem"
            },
            {
              field: "amount",
              title: gettextCatalog.getString("Amount"),
              width: "10rem"
            },
            {
              field: "paymentClass.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Payment Class"),
              width: "10rem"

            },
            {
              field: "currency",
              title: gettextCatalog.getString("Currency"),
              width: "10rem"
            },
            {
              field: "status.name" + $rootScope.lnC,
              title: gettextCatalog.getString("Status"),
              width: "10rem"

            },
          ]
        };


        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.withdrawalSubscriptionMoney.searchResultGrid) {
            $scope.withdrawalSubscriptionMoney.searchResultGrid = widget;
          }
        });

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.issuerPayments.searchResultGrid) {
            $scope.issuerPayments.searchResultGrid = widget;
          }
        });



        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask($scope.deMergerData);
        });


        // // If task saved as draft get saved data
        // if (operationState == appConstants.operationStates.active) {
        //   if (task.draft) {
        //     //$scope.deMergerData = angular.fromJson(task.draft);
        //   }
        // }


        $scope.pay = function () {

          if (true) {

            $scope.issuerPayments.searchResult.isEmpty = false;

            var formData = angular.copy($scope.issuerPayments.form);


            if ($scope.params.config.searchCriteria) {

              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }


            var requestData = angular.extend(formData, {
              instrumentID: 3939
              
            });
            console.log($scope.issuerPaymentsData);

            console.log('Request Data', requestData);
            Loader.show(true);
            $http({method: 'POST',
              url: $scope.params.searchUrl1,
              data: {data: requestData}}).success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.issuerPayments.searchResult.data = data;
              } else {
                $scope.issuerPayments.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }

          var requestData = angular.extend(formData, {

          });
          $http({method: 'POST',
            url: "/api/corporate-actions/sendSubscriptionPayment/",
            data: {data: requestData}}).success(function (data, status, headers, config) {

            var data = recordsService.normalizeRecordFields(data);
            if (data) {
              $scope.issuerPayments.searchResult.data = data;
            } else {
              $scope.issuerPayments.searchResult.isEmpty = true;
            }

            console.log('Response data', data);
            if (data['success'] === "true") {

              e.success({
                Data: data.data ? data.data : [], Total: data.total
              });
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }

            Loader.show(false);
          });
        }


      }]);
