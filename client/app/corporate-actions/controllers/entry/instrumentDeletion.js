'use strict';

angular.module('cmisApp')
  .controller('InstrumentDeletionController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'SweetAlert', 'gettextCatalog',
    'referenceDataService', 'Loader', 'instrumentDeletionService', 'personFindService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, SweetAlert, gettextCatalog,
              referenceDataService, Loader, instrumentDeletionService, personFindService) {

      var instrumentFormPartialTemplate = {
        id: null,
        ISIN: null,
        issuerName: null,
      };
      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit: true

      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.issuer === null || draft.issuer === undefined) {
          draft.issuer = {};
        }
        if (draft.instruments === null || draft.instruments === undefined) {
          draft.instruments = [
            angular.copy(instrumentFormPartialTemplate)
          ];
        }
        if (draft.config === null || draft.config === undefined) {
          draft.config = {
            instrument: {
              showInstrumentFindBlock: false,
              showInstrumentAddBtn: false
            }
          };
        }
        return draft;
      };

      $scope.instrumentDeletionData = initializeFormData(angular.fromJson(task.draft));

      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "instrumentDeletionDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = instrumentDeletionService.prepareFormData(angular.copy($scope.instrumentDeletionData));
                $scope.config.completeTask(formData);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.instrumentDeletionData);
      });

      //Get metadata
      Loader.show(true);

      instrumentDeletionService.getMetaData().then(function (data) {
        $scope.metaData = {
          CAInstrumentDeletionClasses: data.CAInstrumentDeletionClasses,
          registrationAuthorityClasses: data.registrationAuthorityClasses
        };
        referenceDataService.normalizeReturnedRecordTask($scope.instrumentDeletionData, $scope.metaData);
        Loader.show(false);
      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {
          $scope.instrumentDeletionData.issuer.name = data.name;
          $scope.instrumentDeletionData.issuer.accountId = data.accountId;
          $scope.instrumentDeletionData.issuer.id = data.id;
          $scope.instrumentDeletionData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
        });
      };

      // Search instrument
      $scope.findInstrument = function (index) {
        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.instrumentDeletionData.instruments[index].ISIN = data.isin;
          $scope.instrumentDeletionData.instruments[index].id = data.id;
          $scope.instrumentDeletionData.instruments[index].issuerName = data.issuerName;
          $scope.instrumentDeletionData.instruments[index].CFI = data.CFI;
        });
      };

      // Add new instrument
      $scope.addNewInstrument = function (model) {
        model.push(angular.copy(instrumentFormPartialTemplate));
      };

      // Remove instrument
      $scope.removeInstrument = function (index, model) {
        model.splice(index, 1);
      };

      $scope.instrumentDeletionClassChange = function () {

        var deletionClasses = {
          bankruptcy: 'CA_CLASS_BANKRUPTCY',
          liquidation: 'CA_CLASS_LIQUIDATION',
          redemption: 'CA_CLASS_REDEMPTION'

        };
        var selectedDeletionClassCode = angular.fromJson($scope.instrumentDeletionData.instrumentDeletionClass).code;

        $scope.instrumentSearchParams.params.searchUrl = null;
        $scope.instrumentSearchParams.searchUrl = 'findInstrument/';
        $scope.instrumentDeletionData.config.instrument.showInstrumentFindBlock = false;
        $scope.instrumentDeletionData.config.instrument.showInstrumentAddBtn = false;


        if (selectedDeletionClassCode == deletionClasses.liquidation) {
          $scope.instrumentDeletionData.config.instrument.showInstrumentFindBlock = true;
          $scope.instrumentDeletionData.config.instrument.showInstrumentAddBtn = true;
        }
        else if (selectedDeletionClassCode == deletionClasses.redemption) {

          $scope.instrumentSearchParams.searchUrl = 'findBond/';
          $scope.instrumentDeletionData.config.instrument.showInstrumentFindBlock = true;
          $scope.instrumentDeletionData.config.instrument.showInstrumentAddBtn = false;

        }
      };

    }]);
