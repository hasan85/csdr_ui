'use strict';

angular.module( 'cmisApp' )
  .controller( 'IssueRegistrationController',
    [ '$scope', 'gettextCatalog', '$windowInstance', 'id', 'appConstants', 'operationService',
      'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService',
      'Loader', 'personFindService', 'issueRegistrationService', 'manageInstrumentService', 'SweetAlert', '$filter',
      function ( $scope, gettextCatalog, $windowInstance, id, appConstants, operationService,
        instrumentFindService, helperFunctionsService, operationState, task, referenceDataService,
        Loader, personFindService, issueRegistrationService, manageInstrumentService, SweetAlert, $filter ) {

        // metadata
        $scope.metaData = {
          sel1: [],
          sel2: [],
          sel3: [],
          sel4: [],
          sel5: [],
          sel6: [],
          convertableStockes: null
        };

        // Initialize local variables
        var commonStockConfig, convertibleStockConfig, bondConfig, mutualFundConfig, subscriptionConfig, warrantConfig = {};

        var customIdentificatorsTemplate = {
          type: null,
          number: null
        };
        var fiscalDatesTemplate = {
          value: null
        };
        var couponPaymentTemplate = {
          paymentDate: null,
          amount: null
        };
        var securityGroupTemplate = {
          nameAz: null,
          nameEn: null,
          id: null,
          code: null
        };
        var underwriterTemplate = {
          id: null,
          name: null
        };

        var initializeFormData = function ( draft ) {
          debugger;
          if ( !draft ) {
            draft = {
              isSubscription: null
            }
          } else {
            if ( draft.isSubscription === undefined || draft.isSubscription === null ) {
              draft.isSubscription = null;
            }
          }
          if ( !draft.security ) {
            draft.security = {};
          }
          if ( !draft.underwriters || ( draft.underwriters && draft.underwriters.length < 1 ) ) {
            draft.underwriters = [
              angular.copy( underwriterTemplate )
            ];
          }
          if ( draft.preemptive === undefined || draft.preemptive === null ) {
            draft.preemptive = false;
          }
          if ( draft.sendToSecondaryMarket === undefined || draft.sendToSecondaryMarket === null ) {
            draft.sendToSecondaryMarket = false;
          }
          if ( !draft.instrument ) {
            draft.instrument = {
              customIdentificators: [
                angular.copy( customIdentificatorsTemplate )
              ],
              fiscalDates: [
                angular.copy( fiscalDatesTemplate )
              ],
              issuer: {},
              foreign: false,
              CFI: {},
              couponPayments: [
                angular.copy( couponPaymentTemplate )
              ],
              securityGroups: [
                angular.copy( securityGroupTemplate )
              ],
              cfiForm: {
                name: 'cfiGenerateForm',
                generatedCFI: 'EDPEKDE',
                sel1: '',
                sel2: '',
                sel3: '',
                sel4: '',
                sel5: '',
                sel6: '',
              },
              config: {
                fields: null
              },
              haircutRatio: 0,
              conventionalDayCount: 0
            };
          }
          if ( !draft.config ) {
            draft.config = {
              placement: {
                classCodes: {
                  public: 'PLACEMENT_CLASS_PUBLIC',
                  private: 'PLACEMENT_CLASS_PRIVATE',
                  auction: 'PLACEMENT_CLASS_AUCTION'
                },

                currentCode: null
              },
              security: {
                securityTypes: {
                  equity: 'Equity',
                  bond: 'Bond'
                },
                currentSecurity: null,
                isEquity: false,
                isBond: false,
                isIPO: true,
                isRS: false,
                isRW: false,
                isEP: false,
                isEU: false
              },
              labels: {
                periodStartDate: null,
                periodEndDate: null
              }
            };
          }
          return draft;
        };
        $scope.issueRegistration = initializeFormData( angular.fromJson( task.draft ) );
        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "issueRegistrationOperationForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.form.name.$submitted = true;
                if ( $scope.config.form.name.$valid ) {
                  var formData = issueRegistrationService.prepareFormData( $scope.issueRegistration );
                  formData.isSubscription = $scope.issueRegistration.isSubscription;
                  $scope.config.completeTask( formData );
                } else {
                  SweetAlert.swal( '', gettextCatalog.getString( 'Form Validation Error! \n Please check inputted fields' ), 'error' );
                }
              }
            }
          }
        };

        if ( operationState == appConstants.operationStates.active ) {
          if ( $scope.issueRegistration.placementClass ) $scope.issueRegistration.placementClass = $filter( 'json' )( $scope.issueRegistration.placementClass );
          if ( $scope.issueRegistration.registrationAuthorityClass ) $scope.issueRegistration.registrationAuthorityClass = $filter( 'json' )( $scope.issueRegistration.registrationAuthorityClass );

          if ( $scope.issueRegistration.issue ) {
            if ( $scope.issueRegistration.issue.bondConfiguration ) $scope.issueRegistration.issue.bondConfiguration = $filter( 'json' )( $scope.issueRegistration.issue.bondConfiguration );
            if ( $scope.issueRegistration.issue.commonStockConfiguration ) $scope.issueRegistration.issue.commonStockConfiguration = $filter( 'json' )( $scope.issueRegistration.issue.commonStockConfiguration );
            if ( $scope.issueRegistration.issue.convertibleStockConfiguration ) $scope.issueRegistration.issue.convertibleStockConfiguration = $filter( 'json' )( $scope.issueRegistration.issue.convertibleStockConfiguration );
            if ( $scope.issueRegistration.issue.mutualFundConfiguration ) $scope.issueRegistration.issue.mutualFundConfiguration = $filter( 'json' )( $scope.issueRegistration.issue.mutualFundConfiguration );
            if ( $scope.issueRegistration.issue.subscriptionConfiguration ) $scope.issueRegistration.issue.subscriptionConfiguration = $filter( 'json' )( $scope.issueRegistration.issue.subscriptionConfiguration );
            if ( $scope.issueRegistration.issue.warrantConfiguration ) $scope.issueRegistration.issue.warrantConfiguration = $filter( 'json' )( $scope.issueRegistration.issue.warrantConfiguration );
          }

          if ( $scope.issueRegistration.instrument ) {
            if ( $scope.issueRegistration.instrument.bondConfiguration ) $scope.issueRegistration.instrument.bondConfiguration = $filter( 'json' )( $scope.issueRegistration.instrument.bondConfiguration );
            if ( $scope.issueRegistration.instrument.commonStockConfiguration ) $scope.issueRegistration.instrument.commonStockConfiguration = $filter( 'json' )( $scope.issueRegistration.instrument.commonStockConfiguration );
            if ( $scope.issueRegistration.instrument.convertibleStockConfiguration ) $scope.issueRegistration.instrument.convertibleStockConfiguration = $filter( 'json' )( $scope.issueRegistration.instrument.convertibleStockConfiguration );
            if ( $scope.issueRegistration.instrument.mutualFundConfiguration ) $scope.issueRegistration.instrument.mutualFundConfiguration = $filter( 'json' )( $scope.issueRegistration.instrument.mutualFundConfiguration );
            if ( $scope.issueRegistration.instrument.subscriptionConfiguration ) $scope.issueRegistration.instrument.subscriptionConfiguration = $filter( 'json' )( $scope.issueRegistration.instrument.subscriptionConfiguration );
            if ( $scope.issueRegistration.instrument.warrantConfiguration ) $scope.issueRegistration.instrument.warrantConfiguration = $filter( 'json' )( $scope.issueRegistration.instrument.warrantConfiguration );
            if ( $scope.issueRegistration.instrument.currency ) $scope.issueRegistration.instrument.currency = $filter( 'json' )( $scope.issueRegistration.instrument.currency );

            if ( $scope.issueRegistration.instrument.securityGroups && angular.isArray( $scope.issueRegistration.instrument.securityGroups ) ) {
              var result = [];

              $scope.issueRegistration.instrument.securityGroups.forEach( function ( item ) {
                result.push( $filter( 'json' )( item ) );
              } );
              $scope.issueRegistration.instrument.securityGroups = result;
            }
          }

        }

        // Check if form is dirty
        $scope.$on( 'closeTask', function () {
          $scope.config.showTaskSavePrompt( false );
        } );

        function prepareForSave( data ) {

          if ( data.placementClass ) data.placementClass = angular.fromJson( data.placementClass );
          if ( data.registrationAuthorityClass ) data.registrationAuthorityClass = angular.fromJson( data.registrationAuthorityClass );

          if ( data.issue ) {
            if ( data.issue.bondConfiguration ) data.issue.bondConfiguration = angular.fromJson( data.issue.bondConfiguration );
            if ( data.issue.commonStockConfiguration ) data.issue.commonStockConfiguration = angular.fromJson( data.issue.commonStockConfiguration );
            if ( data.issue.convertibleStockConfiguration ) data.issue.convertibleStockConfiguration = angular.fromJson( data.issue.convertibleStockConfiguration );
            if ( data.issue.mutualFundConfiguration ) data.issue.mutualFundConfiguration = angular.fromJson( data.issue.mutualFundConfiguration );
            if ( data.issue.subscriptionConfiguration ) data.issue.subscriptionConfiguration = angular.fromJson( data.issue.subscriptionConfiguration );
            if ( data.issue.warrantConfiguration ) data.issue.warrantConfiguration = angular.fromJson( data.issue.warrantConfiguration );
          }

          if ( data.instrument ) {
            if ( data.instrument.bondConfiguration ) data.instrument.bondConfiguration = angular.fromJson( data.instrument.bondConfiguration );
            if ( data.instrument.commonStockConfiguration ) data.instrument.commonStockConfiguration = angular.fromJson( data.instrument.commonStockConfiguration );
            if ( data.instrument.convertibleStockConfiguration ) data.instrument.convertibleStockConfiguration = angular.fromJson( data.instrument.convertibleStockConfiguration );
            if ( data.instrument.mutualFundConfiguration ) data.instrument.mutualFundConfiguration = angular.fromJson( data.instrument.mutualFundConfiguration );
            if ( data.instrument.subscriptionConfiguration ) data.instrument.subscriptionConfiguration = angular.fromJson( data.instrument.subscriptionConfiguration );
            if ( data.instrument.warrantConfiguration ) data.instrument.warrantConfiguration = angular.fromJson( data.instrument.warrantConfiguration );
            if ( data.instrument.currency ) data.instrument.currency = angular.fromJson( data.instrument.currency );

            if ( data.instrument.securityGroups && angular.isArray( data.instrument.securityGroups ) ) {
              var result = [];

              data.instrument.securityGroups.forEach( function ( item ) {
                result.push( angular.fromJson( item ) );
              } );
              data.instrument.securityGroups = result;
            }
          }
          return data;
        }

        window.$scope = $scope;

        // Save task as draft
        $scope.$on( 'saveTask', function () {


          var buf = {
            cfiForm: $scope.issueRegistration.instrument.cfiForm,
            cfiWindow: $scope.issueRegistration.instrument.cfiGenerateWindow,
            partialFormName: $scope.issueRegistration.instrument.partialFormName
          };

          $scope.issueRegistration.instrument.cfiForm = null;
          $scope.issueRegistration.instrument.cfiGenerateWindow = null;
          $scope.issueRegistration.instrument.partialFormName = null;
          var savedData = prepareForSave( angular.copy( $scope.issueRegistration ) );

          $scope.config.saveTask( savedData );

          console.log( "Saved Task", savedData );

          $scope.issueRegistration.instrument.cfiForm = buf.cfiForm;
          $scope.issueRegistration.instrument.cfiGenerateWindow = buf.cfiWindow;
          $scope.issueRegistration.instrument.partialFormName = buf.partialFormName;
        } );

        //Get metadata
        issueRegistrationService.getMetaData().then( function ( data ) {

          $scope.metaData = {
            regAuthClasses: data.registrationAuthorityClasses,
            placementClasses: data.placementClasses,
            identificatorClasses: data.identificatorClasses,
            currencies: data.currencies,
            securityGroups: data.securityGroups,
            sel1: data.CFIcategories,
            couponPaymentMethodClasses: data.couponPaymentMethodClasses,
            convertableStockes: null
          };

          issueRegistrationService.normalizeReturnedTask( $scope.issueRegistration, data );

          var payload = angular.fromJson( task.draft ).issue;

          if ( payload ) {
            if ( payload.haircutRatio ) {
              $scope.issueRegistration.issue[ 'haircutRatio' ] = payload.haircutRatio;
              $scope.issueRegistration.instrument.haircutRatio = payload.haircutRatio * 100;
            }
            if ( payload.conventionalDayCount ) {
              $scope.issueRegistration.issue[ 'conventionalDayCount' ] = payload.conventionalDayCount;
              $scope.issueRegistration.instrument.conventionalDayCount = payload.conventionalDayCount;
            }

            // Get config data
            var commonStockConfigRaw = payload[ 'commonStockConfiguration' ] ? angular.fromJson( payload[ 'commonStockConfiguration' ] ) : null;
            var convertibleStockConfigRaw = payload[ 'convertibleStockConfiguration' ] ? angular.fromJson( payload[ 'convertibleStockConfiguration' ] ) : null;
            var bondConfigRaw = payload[ 'bondConfiguration' ] ? angular.fromJson( payload[ 'bondConfiguration' ] ) : null;
            var mutualFundConfigurationRaw = payload[ 'mutualFundConfiguration' ] ? angular.fromJson( payload[ 'mutualFundConfiguration' ] ) : null;
            var warrantConfigurationRaw = payload[ 'warrantConfiguration' ] ? angular.fromJson( payload[ 'warrantConfiguration' ] ) : null;
            var subscriptionConfigurationRaw = payload[ 'subscriptionConfiguration' ] ? angular.fromJson( payload[ 'subscriptionConfiguration' ] ) : null;

            // For save as draft
            $scope.issueRegistration.issue[ 'commonStockConfiguration' ] = payload[ 'commonStockConfiguration' ];
            $scope.issueRegistration.issue[ 'convertibleStockConfiguration' ] = payload[ 'convertibleStockConfiguration' ];
            $scope.issueRegistration.issue[ 'bondConfiguration' ] = payload[ 'bondConfiguration' ];
            $scope.issueRegistration.issue[ 'mutualFundConfiguration' ] = payload[ 'mutualFundConfiguration' ];
            $scope.issueRegistration.issue[ 'warrantConfiguration' ] = payload[ 'warrantConfiguration' ];
            $scope.issueRegistration.issue[ 'subscriptionConfiguration' ] = payload[ 'subscriptionConfiguration' ];
            $scope.issueRegistration.instrument[ 'commonStockConfiguration' ] = payload[ 'commonStockConfiguration' ];
            $scope.issueRegistration.instrument[ 'convertibleStockConfiguration' ] = payload[ 'convertibleStockConfiguration' ];
            $scope.issueRegistration.instrument[ 'bondConfiguration' ] = payload[ 'bondConfiguration' ];
            $scope.issueRegistration.instrument[ 'mutualFundConfiguration' ] = payload[ 'mutualFundConfiguration' ];
            $scope.issueRegistration.instrument[ 'warrantConfiguration' ] = payload[ 'warrantConfiguration' ];
            $scope.issueRegistration.instrument[ 'subscriptionConfiguration' ] = payload[ 'subscriptionConfiguration' ];

            // Prepare config data
            commonStockConfig = commonStockConfigRaw ? manageInstrumentService.generateFormConfig( commonStockConfigRaw ) : null;
            convertibleStockConfig = convertibleStockConfigRaw ? manageInstrumentService.generateFormConfig( convertibleStockConfigRaw ) : null;
            bondConfig = bondConfigRaw ? manageInstrumentService.generateFormConfig( bondConfigRaw ) : null;
            mutualFundConfig = mutualFundConfigurationRaw ? manageInstrumentService.generateFormConfig( mutualFundConfigurationRaw ) : null;
            warrantConfig = warrantConfigurationRaw ? manageInstrumentService.generateFormConfig( warrantConfigurationRaw ) : null;
            subscriptionConfig = subscriptionConfigurationRaw ? manageInstrumentService.generateFormConfig( subscriptionConfigurationRaw ) : null;

            if ( !$scope.issueRegistration.instrument.cfiForm ) {
              $scope.issueRegistration.instrument.cfiForm = {
                name: 'cfiGenerateForm',
                generatedCFI: 'EDPEKDE',
                sel1: '',
                sel2: '',
                sel3: '',
                sel4: '',
                sel5: '',
                sel6: '',
              };
            }
            if ( !$scope.issueRegistration.instrument.config ) {
              $scope.issueRegistration.instrument.config = {
                fields: null
              };
            }
            if ( helperFunctionsService.isEmptyObject( $scope.issueRegistration.instrument.config.fields ) ) {
              if ( $scope.issueRegistration.instrument.CFI && $scope.issueRegistration.instrument.CFI.code ) {
                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_D' ) ) {
                  $scope.issueRegistration.instrument.config.fields = bondConfig;
                }
                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_R_W' ) ) {
                  $scope.issueRegistration.config.security.isRW = true;
                  $scope.issueRegistration.instrument.config.fields = warrantConfig;
                }
                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_R_S' ) ) {
                  $scope.issueRegistration.config.security.isRS = true;
                  $scope.issueRegistration.instrument.config.fields = subscriptionConfig;
                }
                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_S' ) ) {
                  $scope.issueRegistration.instrument.config.fields = commonStockConfig;
                }
                if (
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_P' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_R' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_C' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_F' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_V' )

                ) {
                  $scope.issueRegistration.instrument.config.fields = convertibleStockConfig;
                }

                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_U' ) ) {
                  $scope.issueRegistration.instrument.config.fields = mutualFundConfig;
                }

              } else {
                $scope.issueRegistration.instrument.config.fields = commonStockConfig;
              }

            }
            $scope.issueRegistration.instrument.partialFormName = $scope.config.form.name;
          }


        } ) ;


        // Search instrument
        $scope.findInstrument = function () {
          instrumentFindService.findInstrument( {
            searchUrl: 'findIssuedInstruments'
          } ).then( function ( data ) {
            $scope.issueRegistration.security.ISIN = data.isin;
            $scope.issueRegistration.security.id = data.id;
            $scope.issueRegistration.security.issuerName = data.issuerName;
            $scope.issueRegistration.security.CFI = data.CFI;

            if ( helperFunctionsService.startsWith( data.CFI.code, appConstants.CFICodeTemplates.equity ) ) {
              $scope.issueRegistration.config.security.isEquity = true;
              $scope.issueRegistration.config.security.isBond = false;
              $scope.issueRegistration.config.security.currentSecurity = $scope.issueRegistration.config.security.securityTypes.equity;
            } else if ( helperFunctionsService.startsWith( data.CFI.code, appConstants.CFICodeTemplates.bond ) ) {
              $scope.issueRegistration.config.security.isEquity = false;
              $scope.issueRegistration.config.security.isBond = true;
              $scope.issueRegistration.config.security.currentSecurity = $scope.issueRegistration.config.security.securityTypes.bond;
            }
          } );
        };

        // Search under writer
        $scope.findUnderwriter = function ( index ) {
          personFindService.findBroker().then( function ( data ) {
            $scope.issueRegistration.underwriters[ index ].id = data.id;
            $scope.issueRegistration.underwriters[ index ].name = data.name;
            $scope.issueRegistration.underwriters[ index ].identificator = data.uniqueCode;
            $scope.issueRegistration.underwriters[ index ].accountId = data.accountId;
          } );
        };

        // Add new Under writer
        $scope.addNewUnderwriter = function ( model ) {
          model.push( angular.copy( underwriterTemplate ) );
        };

        // Remove Under writer
        $scope.removeUnderwriter = function ( index, model ) {
          model.splice( index, 1 );
        };

        $scope.placementClassChange = function () {
          if ( $scope.issueRegistration.placementClass ) {
            var currentPlacementClass = angular.fromJson( angular.copy( $scope.issueRegistration.placementClass ) );
            $scope.issueRegistration.config.placement.currentCode = currentPlacementClass.code;
          }
        };
        $scope.placementClassChange();

        //Add/remove custom Identificator
        $scope.addNewIdentificator = function ( model ) {
          model.push( angular.copy( customIdentificatorsTemplate ) );
        };

        $scope.removeIdentificator = function ( index, model ) {
          model.splice( index, 1 );
        };

        //Add/remove coupon payments
        $scope.addNewCouponPayment = function ( model ) {
          model.push( angular.copy( couponPaymentTemplate ) );
        };

        $scope.removeCouponPayment = function ( index, model ) {
          model.splice( index, 1 );
        };

        //Add/remove fiscal date
        $scope.addNewFiscalDate = function ( model ) {
          model.push( angular.copy( fiscalDatesTemplate ) );
        };

        $scope.removeFiscalDate = function ( index, model ) {
          model.splice( index, 1 );
        };

        //Add/remove security groups
        $scope.addNewSecurityGroup = function ( model ) {
          model.push( angular.copy( securityGroupTemplate ) );
        };

        $scope.removeSecurityGroup = function ( index, model ) {
          model.splice( index, 1 );
        };

        // Find issuer
        $scope.findIssuer = function () {

          personFindService.findIssuer().then( function ( data ) {
            $scope.issueRegistration.instrument.issuer.name = data.name;
            $scope.issueRegistration.instrument.issuer.id = data.id;
            $scope.issueRegistration.instrument.issuer.accountId = data.accountId;
            $scope.issueRegistration.instrument.issuer.accountNumber = data.accountNumber;
            if ( $scope.issueRegistration.config.security.isRS || $scope.issueRegistration.config.security.isRW ) {
              issueRegistrationService.getIssuerEquities( data.id ).then( function ( data ) {
                $scope.metaData.convertableStockes = data;
              } );
            }
          } );
        };

        // Query CFI hierarchy
        $scope.getChildOptions = function ( classId, index ) {
          if ( index != 6 ) {
            var startIndex;

            if ( classId == '' ) {
              startIndex = index + 1;
            } else {
              startIndex = index + 2;
              classId = parseInt( classId );

              if ( classId > 0 ) {

                Loader.show( true );

                referenceDataService.getCFIClasses( classId ).then( function ( data ) {

                  var nextClassesIndex = index + 1;
                  $scope.metaData[ 'sel' + nextClassesIndex ] = data;

                  Loader.show( false );

                } );
              }
            }
            if ( startIndex <= 6 ) {
              for ( var i = startIndex; i <= 6; i++ ) {
                $scope.metaData[ 'sel' + i ] = null;
              }
            }
          }

        };

        // Get generated CFI
        $scope.generateCFI = function ( window ) {

          console.log( $scope.issueRegistration.instrument.cfiForm.name.$submitted );
          $scope.issueRegistration.instrument.cfiForm.name.$submitted = true;
          if ( $scope.issueRegistration.instrument.cfiForm.name.$valid ) {
            for ( var i = 0; i < $scope.metaData.sel6.length; i++ ) {
              var searchedID = parseInt( $scope.issueRegistration.instrument.cfiForm.sel6 );
              var currentID = parseInt( $scope.metaData.sel6[ i ][ 'id' ] );
              if ( searchedID === currentID ) {

                $scope.issueRegistration.instrument.CFI = $scope.metaData.sel6[ i ];
                window.close();

                $scope.issueRegistration.instrument.cfiForm.sel6 =
                  $scope.issueRegistration.instrument.cfiForm.sel5 =
                  $scope.issueRegistration.instrument.cfiForm.sel4 =
                  $scope.issueRegistration.instrument.cfiForm.sel3 =
                  $scope.issueRegistration.instrument.cfiForm.sel2 =
                  $scope.issueRegistration.instrument.cfiForm.sel1 = '';


                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_D' ) ) {
                  $scope.issueRegistration.instrument.config.fields = bondConfig;
                }

                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_R_W' ) ) {

                  $scope.issueRegistration.config.security.isRW = true;
                  $scope.issueRegistration.instrument.config.fields = warrantConfig;
                }

                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_R_S' ) ) {

                  $scope.issueRegistration.config.security.isRS = true;
                  $scope.issueRegistration.instrument.config.fields = subscriptionConfig;
                }

                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_S' ) ) {

                  $scope.issueRegistration.instrument.config.fields = commonStockConfig;
                }

                if (
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_P' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_R' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_C' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_F' ) ||
                  helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_V' )

                ) {
                  $scope.issueRegistration.instrument.config.fields = convertibleStockConfig;
                }

                if ( helperFunctionsService.startsWith( $scope.issueRegistration.instrument.CFI.code, 'CFI_E_U' ) ) {
                  $scope.issueRegistration.instrument.config.fields = mutualFundConfig;
                }

                break;
              }
            }

          }
        };

        $scope.onlyWeekendsPredicate = function ( date ) {
          var day = date.getDay();
          return day === 0 || day === 6;
        }
      }
    ] );
