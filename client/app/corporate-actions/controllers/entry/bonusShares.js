'use strict';

angular.module('cmisApp')
  .controller('BonusSharesController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'Loader', 'bonusSharesService', 'manageInstrumentService', 'gettextCatalog', 'personFindService', 'issueRegistrationService','SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task,
              referenceDataService, Loader, bonusSharesService, manageInstrumentService, gettextCatalog, personFindService, issueRegistrationService,SweetAlert) {

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit: true,

      };

      // metadata
      $scope.metaData = {
        sel1: [],
        sel2: [],
        sel3: [],
        sel4: [],
        sel5: [],
        sel6: [],
        convertableStockes: null
      };

      // Initialize local variables

      var subscriptionConfig = {};
      var customIdentificatorsTemplate = {
        type: null,
        number: null
      };
      var fiscalDatesTemplate = {
        value: null
      };
      var couponPaymentTemplate = {
        paymentDate: null,
        amount: null
      };
      var securityGroupTemplate = {
        nameAz: null,
        nameEn: null,
        id: null,
        code: null
      };
      var instrumentFormTemplate = {
        customIdentificators: [
          angular.copy(customIdentificatorsTemplate)
        ],
        fiscalDates: [
          angular.copy(fiscalDatesTemplate)
        ],
        issuer: {},
        foreign: false,
        CFI: {},
        couponPayments: [
          angular.copy(couponPaymentTemplate)
        ],
        securityGroups: [
          angular.copy(securityGroupTemplate)
        ],
        cfiForm: {
          name: 'cfiGenerateForm',
          generatedCFI: 'EDPEKDE',
          sel1: '',
          sel2: '',
          sel3: '',
          sel4: '',
          sel5: '',
          sel6: ''
        },
        config: {
          fields: {},

        },
        haircutRatio: 0,
        conventionalDayCount: 0
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.issuer === null || draft.issuer === undefined) {
          draft.issuer = {};
        }
        if (draft.oldSecurity === null || draft.oldSecurity === undefined) {
          draft.oldSecurity = {};
        }
        if (!draft.config) {
          draft.config = {
            showNewSecurity: false
          };
        }
        if (draft.newSecurity && ((draft.newSecurity.config === null || draft.newSecurity.config === undefined) && !draft.newSecurity.ISIN )) {
          draft.newSecurity = angular.copy(instrumentFormTemplate);
        }
        return draft;
      };
      $scope.bonusShareData = initializeFormData(angular.fromJson(task.draft));
      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "bonusShareDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = bonusSharesService.prepareFormData($scope.bonusShareData);
                $scope.config.completeTask(formData);
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        labels: {
          lblCFIWindowTitle: gettextCatalog.getString('CFI Generate'),
          lblCategory: gettextCatalog.getString('Category'),
          lblGroup: gettextCatalog.getString('Group'),
          lblAttribute: gettextCatalog.getString('Attribute')
        }
      };
      //Get metadata
      Loader.show(true);

      bonusSharesService.getMetaData().then(function (data) {

        $scope.metaData = {
          fractionTreatmentClasses: data.fractionTreatmentClasses,
          registrationAuthorityClasses: data.registrationAuthorityClasses,
          placementClasses: data.placementClasses,
          identificatorClasses: data.identificatorClasses,
          currencies: data.currencies,
          securityGroups: data.securityGroups,
          sel1: [data.CFICategoryRight],
          couponPaymentMethodClasses: data.couponPaymentMethodClasses,
          CABonusShareClasses: data.CABonusShareClasses,
          convertableStockes: null

        };
        bonusSharesService.normalizeReturnedRecordTask($scope.bonusShareData, data);

        var payload = angular.fromJson(task.draft).newSecurity;

        if (payload.haircutRatio) {
          $scope.bonusShareData.newSecurity.haircutRatio = payload.haircutRatio * 100;
        }
        if (payload.conventionalDayCount) {
          $scope.bonusShareData.newSecurity.conventionalDayCount = payload.conventionalDayCount;
        }
        var subscriptionConfigurationRaw = payload['subscriptionConfiguration'] ? angular.fromJson(payload['subscriptionConfiguration']) : null;

        // For save as draft
        $scope.bonusShareData.newSecurity['subscriptionConfiguration'] = subscriptionConfigurationRaw;

        // Prepare config data
        subscriptionConfig = subscriptionConfigurationRaw ? manageInstrumentService.generateFormConfig(subscriptionConfigurationRaw) : null;

        subscriptionConfig.issuer = false;

        if (!$scope.bonusShareData.newSecurity.config) {
          $scope.bonusShareData.newSecurity.config = {
            fields: null
          };
        }
        $scope.bonusShareData.newSecurity.config.fields = subscriptionConfig;

        if (!$scope.bonusShareData.newSecurity.cfiForm) {
          $scope.bonusShareData.newSecurity.cfiForm = {
            name: 'cfiGenerateForm',
            generatedCFI: 'EDPEKDE',
            sel1: '',
            sel2: '',
            sel3: '',
            sel4: '',
            sel5: '',
            sel6: '',
          };
        }
        if (!$scope.bonusShareData.newSecurity.config) {
          $scope.bonusShareData.newSecurity.config = {
            fields: null
          };
        }

        if ($scope.bonusShareData && $scope.bonusShareData.issuer && $scope.bonusShareData.issuer.id) {
          issueRegistrationService.getIssuerEquities($scope.bonusShareData.issuer.id).then(function (data) {
            $scope.metaData.convertableStockes = data;
          });
        }
        $scope.bonusShareData.newSecurity.partialFormName = $scope.config.form.name;

        $scope.CATypeChange();
        Loader.show(false);

      });


      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {

        var buf = {
          cfiForm: $scope.bonusShareData.newSecurity.cfiForm,
          cfiWindow: $scope.bonusShareData.newSecurity.cfiGenerateWindow,
        };

        $scope.bonusShareData.newSecurity.cfiForm = null;
        $scope.bonusShareData.newSecurity.cfiGenerateWindow = null;
        $scope.config.saveTask($scope.bonusShareData);
        $scope.bonusShareData.newSecurity.cfiForm = buf.cfiForm;
        $scope.bonusShareData.newSecurity.cfiGenerateWindow = buf.cfiWindow;

      });

      // Search issuer
      $scope.findCurrentIssuer = function () {
        personFindService.findIssuer().then(function (data) {
          $scope.bonusShareData.issuer.name = data.name;
          $scope.bonusShareData.issuer.accountId = data.accountId;
          $scope.bonusShareData.issuer.id = data.id;
          $scope.bonusShareData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
          $scope.bonusShareData.newSecurity.issuer = {
            name: data.name,
            id: data.id
          };
          issueRegistrationService.getIssuerEquities(data.id).then(function (data) {
            $scope.metaData.convertableStockes = data;
          });
        });
      };

      // Search instrument
      $scope.findOldSecurity = function () {
        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.bonusShareData.oldSecurity.ISIN = data.isin;
          $scope.bonusShareData.oldSecurity.id = data.id;
          $scope.bonusShareData.oldSecurity.issuerName = data.issuerName;
          $scope.bonusShareData.oldSecurity.issuerId = data.issuerId;
          $scope.bonusShareData.oldSecurity.CFI = data.CFI;
        });
      };

      $scope.CATypeChange = function () {
        var bonusShareClasses = {
          bonusIssue: 'CA_CLASS_BONUS_ISSUE',
          stockDividend: 'CA_CLASS_STOCK_DIVIDEND',
          bonusRight: 'CA_CLASS_BONUS_RIGHT'
        };
        var selectedCAClassCode = $scope.bonusShareData && $scope.bonusShareData.CAType ? angular.fromJson($scope.bonusShareData.CAType).code : null;
        $scope.bonusShareData.config.showNewSecurity = false;
        if (selectedCAClassCode == bonusShareClasses.bonusRight) {
          $scope.bonusShareData.config.showNewSecurity = true;
        }

      };
      // Instrument data actions [[
      // Find issuer
      //Add/remove custom Identificator
      $scope.addNewIdentificator = function (model) {
        model.push(angular.copy(customIdentificatorsTemplate));
      };

      $scope.removeIdentificator = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove coupon payments
      $scope.addNewCouponPayment = function (model) {
        model.push(angular.copy(couponPaymentTemplate));
      };

      $scope.removeCouponPayment = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove fiscal date
      $scope.addNewFiscalDate = function (model) {

        model.push(angular.copy(fiscalDatesTemplate));
      };

      $scope.removeFiscalDate = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove security groups
      $scope.addNewSecurityGroup = function (model) {
        model.push(angular.copy(securityGroupTemplate));
      };

      $scope.removeSecurityGroup = function (index, model) {
        model.splice(index, 1);
      };

      // Query CFI hierarchy
      $scope.getChildOptions = function (classId, index) {
        if (index != 6) {
          var startIndex;

          if (classId == '') {
            startIndex = index + 1;
          } else {

            startIndex = index + 2;
            classId = parseInt(classId);

            if (classId > 0) {

              Loader.show(true);

              referenceDataService.getCFIClasses(classId).then(function (data) {

                var nextClassesIndex = index + 1;
                $scope.metaData['sel' + nextClassesIndex] = data;

                Loader.show(false);

              });
            }
          }
          if (startIndex <= 6) {
            for (var i = startIndex; i <= 6; i++) {
              $scope.metaData['sel' + i] = null;
            }
          }
        }

      };

      // Get generated CFI
      $scope.generateCFI = function (window) {

        $scope.bonusShareData.newSecurity.cfiForm.name.$submitted = true;

        if ($scope.bonusShareData.newSecurity.cfiForm.name.$valid) {

          for (var i = 0; i < $scope.metaData.sel6.length; i++) {

            var searchedID = parseInt($scope.bonusShareData.newSecurity.cfiForm.sel6);
            var currentID = parseInt($scope.metaData.sel6[i]['id']);

            if (searchedID === currentID) {

              $scope.bonusShareData.newSecurity.CFI = $scope.metaData.sel6[i];
              window.close();

              $scope.bonusShareData.newSecurity.cfiForm.sel6 =
                $scope.bonusShareData.newSecurity.cfiForm.sel5 =
                  $scope.bonusShareData.newSecurity.cfiForm.sel4 =
                    $scope.bonusShareData.newSecurity.cfiForm.sel3 =
                      $scope.bonusShareData.newSecurity.cfiForm.sel2 = '';

              break;
            }
          }

        }
      };

    }]);
