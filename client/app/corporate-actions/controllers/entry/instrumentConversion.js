'use strict';

angular.module('cmisApp')
  .controller('InstrumentConversionController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'Loader', 'instrumentConversionService', 'manageInstrumentService', 'gettextCatalog', 'personFindService', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task,
              referenceDataService, Loader, instrumentConversionService, manageInstrumentService, gettextCatalog, personFindService, SweetAlert) {
      // metadata
      $scope.metaData = {
        sel1: [],
        sel2: [],
        sel3: [],
        sel4: [],
        sel5: [],
        sel6: [],
        convertableStockes: null
      };

      // Initialize local variables
      var commonStockConfig, convertibleStockConfig, bondConfig, mutualFundConfig, subscriptionConfig, warrantConfig = {};

      var customIdentificatorsTemplate = {
        type: null,
        number: null
      };
      var fiscalDatesTemplate = {
        value: null
      };
      var couponPaymentTemplate = {
        paymentDate: null,
        amount: null
      };
      var securityGroupTemplate = {
        nameAz: null,
        nameEn: null,
        id: null,
        code: null
      };
      var underwriterTemplate = {
        id: null,
        name: null
      };
      var instrumentFormTemplate = {
        customIdentificators: [
          angular.copy(customIdentificatorsTemplate)
        ],
        fiscalDates: [
          angular.copy(fiscalDatesTemplate)
        ],
        issuer: {},
        foreign: false,
        CFI: {},

        couponPayments: [
          angular.copy(couponPaymentTemplate)
        ],
        securityGroups: [
          angular.copy(securityGroupTemplate)
        ],
        cfiForm: {
          name: 'cfiGenerateForm',
          generatedCFI: 'EDPEKDE',
          sel1: '',
          sel2: '',
          sel3: '',
          sel4: '',
          sel5: '',
          sel6: ''
        },
        config: {
          fields: {},

        },
        haircutRatio: 0,
        conventionalDayCount: 0
      };
      //$scope.instrumentConversionData = {
      //  oldSecurity: {},
      //  config: {
      //    newSecurity: {
      //      isNew: false
      //    }
      //  },
      //  newSecurity: angular.copy(instrumentFormTemplate)
      //};

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {};
        }

        if (!draft.oldSecurity) {
          draft.oldSecurity = {};
        }

        if (!draft.config) {
          draft.config = {
            newSecurity: {
              isNew: false
            }
          }
        }

        if (draft.newSecurity && ((draft.newSecurity.config === null || draft.newSecurity.config === undefined) && !draft.newSecurity.ISIN )) {
          draft.newSecurity = angular.copy(instrumentFormTemplate);
        }

        if (draft.newSecurity && draft.newSecurity.ISIN && !draft.newSecurity.id) {
          draft.config = {
            newSecurity: {
              isNew: true
            }
          }
        }
        return draft;
      };
      $scope.instrumentConversionData = initializeFormData(angular.fromJson(task.draft));

      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "instrumentConversionDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = instrumentConversionService.prepareFormData($scope.instrumentConversionData);
                $scope.config.completeTask(formData);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        labels: {
          lblCFIWindowTitle: gettextCatalog.getString('CFI Generate'),
          lblCategory: gettextCatalog.getString('Category'),
          lblGroup: gettextCatalog.getString('Group'),
          lblAttribute: gettextCatalog.getString('Attribute')
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        var buf = {
          cfiForm: $scope.instrumentConversionData.newSecurity.cfiForm,
          cfiWindow: $scope.instrumentConversionData.newSecurity.cfiGenerateWindow,
        };

        $scope.instrumentConversionData.newSecurity.cfiForm = null;
        $scope.instrumentConversionData.newSecurity.cfiGenerateWindow = null;
        $scope.config.saveTask($scope.instrumentConversionData);
        $scope.instrumentConversionData.newSecurity.cfiForm = buf.cfiForm;
        $scope.instrumentConversionData.newSecurity.cfiGenerateWindow = buf.cfiWindow;

      });

      //Get metadata
      Loader.show(true);

      instrumentConversionService.getMetaData().then(function (data) {
        $scope.metaData = {
          fractionTreatmentClasses: data.fractionTreatmentClasses,
          registrationAuthorityClasses: data.registrationAuthorityClasses,
          placementClasses: data.placementClasses,
          identificatorClasses: data.identificatorClasses,
          currencies: data.currencies,
          securityGroups: data.securityGroups,
          sel1: data.CFIcategories,
          couponPaymentMethodClasses: data.couponPaymentMethodClasses,
          convertableStockes: null
        };

        instrumentConversionService.normalizeReturnedTask($scope.instrumentConversionData, data);


        var payload = angular.fromJson(task.draft).newSecurity;
        if (payload.haircutRatio) {
          $scope.instrumentConversionData.newSecurity.haircutRatio = payload.haircutRatio * 100;
        }
        if (payload.conventionalDayCount) {
          $scope.instrumentConversionData.newSecurity.conventionalDayCount = payload.conventionalDayCount;
        }

        // Get config data
        var commonStockConfigRaw = payload['commonStockConfiguration'] ? angular.fromJson(payload['commonStockConfiguration']) : null;
        var convertibleStockConfigRaw = payload['convertibleStockConfiguration'] ? angular.fromJson(payload['convertibleStockConfiguration']) : null;
        var bondConfigRaw = payload['bondConfiguration'] ? angular.fromJson(payload['bondConfiguration']) : null;
        var mutualFundConfigurationRaw = payload['mutualFundConfiguration'] ? angular.fromJson(payload['mutualFundConfiguration']) : null;
        var warrantConfigurationRaw = payload['warrantConfiguration'] ? angular.fromJson(payload['warrantConfiguration']) : null;
        var subscriptionConfigurationRaw = payload['subscriptionConfiguration'] ? angular.fromJson(payload['subscriptionConfiguration']) : null;


        // For save as draft
        $scope.instrumentConversionData.newSecurity['commonStockConfiguration'] = commonStockConfigRaw;
        $scope.instrumentConversionData.newSecurity['convertibleStockConfiguration'] = convertibleStockConfigRaw;
        $scope.instrumentConversionData.newSecurity['bondConfiguration'] = bondConfigRaw;
        $scope.instrumentConversionData.newSecurity['mutualFundConfiguration'] = mutualFundConfigurationRaw;
        $scope.instrumentConversionData.newSecurity['warrantConfiguration'] = warrantConfigurationRaw;
        $scope.instrumentConversionData.newSecurity['subscriptionConfiguration'] = subscriptionConfigurationRaw;

        // Prepare config data
        commonStockConfig = commonStockConfigRaw ? manageInstrumentService.generateFormConfig(commonStockConfigRaw) : null;
        convertibleStockConfig = convertibleStockConfigRaw ? manageInstrumentService.generateFormConfig(convertibleStockConfigRaw) : null;
        bondConfig = bondConfigRaw ? manageInstrumentService.generateFormConfig(bondConfigRaw) : null;
        mutualFundConfig = mutualFundConfigurationRaw ? manageInstrumentService.generateFormConfig(mutualFundConfigurationRaw) : null;
        warrantConfig = warrantConfigurationRaw ? manageInstrumentService.generateFormConfig(warrantConfigurationRaw) : null;
        subscriptionConfig = subscriptionConfigurationRaw ? manageInstrumentService.generateFormConfig(subscriptionConfigurationRaw) : null;

        commonStockConfig.issuer = false;
        convertibleStockConfig.issuer = false;
        bondConfig.issuer = false;
        mutualFundConfig.issuer = false;
        warrantConfig.issuer = false;
        subscriptionConfig.issuer = false;

        if (!$scope.instrumentConversionData.newSecurity.cfiForm) {
          $scope.instrumentConversionData.newSecurity.cfiForm = {
            name: 'cfiGenerateForm',
            generatedCFI: 'EDPEKDE',
            sel1: '',
            sel2: '',
            sel3: '',
            sel4: '',
            sel5: '',
            sel6: ''
          };
        }
        if (!$scope.instrumentConversionData.newSecurity.config) {
          $scope.instrumentConversionData.newSecurity.config = {
            fields: null
          };
        }
        if (helperFunctionsService.isEmptyObject($scope.instrumentConversionData.newSecurity.config.fields)) {
          if ($scope.instrumentConversionData.newSecurity.CFI && $scope.instrumentConversionData.newSecurity.CFI.code) {
            if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_D')) {
              $scope.instrumentConversionData.newSecurity.config.fields = bondConfig;
            }
            if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_R_W')) {
              $scope.issueRegistration.config.security.isRW = true;
              $scope.instrumentConversionData.newSecurity.config.fields = warrantConfig;
            }
            if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_R_S')) {
              $scope.issueRegistration.config.security.isRS = true;
              $scope.instrumentConversionData.newSecurity.config.fields = subscriptionConfig;
            }
            if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_S')) {
              $scope.instrumentConversionData.newSecurity.config.fields = commonStockConfig;
            }
            if (
              helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_P') ||
              helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_R') ||
              helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_C') ||
              helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_F') ||
              helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_V')

            ) {
              $scope.instrumentConversionData.newSecurity.config.fields = convertibleStockConfig;
            }

            if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_U')) {
              $scope.instrumentConversionData.newSecurity.config.fields = mutualFundConfig;
            }

          } else {
            $scope.instrumentConversionData.newSecurity.config.fields = commonStockConfig;
          }

        }

        $scope.instrumentConversionData.newSecurity.config.fields = commonStockConfig;
        $scope.instrumentConversionData.newSecurity.partialFormName = $scope.config.form.name;
        Loader.show(false);

      });

      // Search instrument
      $scope.findOldSecurity = function () {

        instrumentFindService.findInstrument({
          searchUrl: 'findStockOrBond'
        }).then(function (data) {
          $scope.instrumentConversionData.oldSecurity.ISIN = data.isin;
          $scope.instrumentConversionData.oldSecurity.id = data.id;
          $scope.instrumentConversionData.oldSecurity.issuerName = data.issuerName;
          $scope.instrumentConversionData.oldSecurity.issuerId = data.issuerId;
          $scope.instrumentConversionData.oldSecurity.CFI = data.CFI;
          $scope.instrumentConversionData.newSecurity.id = null;
          $scope.instrumentConversionData.newSecurity.ISIN = null;
          $scope.instrumentConversionData.newSecurity.CFI = null;
          $scope.instrumentConversionData.newSecurity.issuer = {
            name: data.issuerName,
            id: data.issuerId
          };
        });
      };

      // Search instrument
      $scope.findNewSecurity = function () {

        var url = null;
        if (helperFunctionsService.startsWith($scope.instrumentConversionData.oldSecurity.CFI.code, appConstants.CFICodeTemplates.bond)) {
          url = 'findStock/'
        } else if (helperFunctionsService.startsWith($scope.instrumentConversionData.oldSecurity.CFI.code, appConstants.CFICodeTemplates.equity)) {
          url = 'findBond/';
        }
        instrumentFindService.findInstrument({
          searchUrl: url,
          params: {
            issuerId: $scope.instrumentConversionData.oldSecurity.issuerId
          },
          config: {
            issuer: {
              isVisible: false
            }
          }
        }).then(function (data) {
          $scope.instrumentConversionData.newSecurity.ISIN = data.isin;
          $scope.instrumentConversionData.newSecurity.id = data.id;
          $scope.instrumentConversionData.newSecurity.CFI = data.CFI;
        });
      };
      $scope.changeNewSecurityType = function () {
        $scope.instrumentConversionData.newSecurity.id = null;
        $scope.instrumentConversionData.newSecurity.ISIN = null;
        $scope.instrumentConversionData.newSecurity.CFI = null;
      };
      // Search under writer
      $scope.findUnderwriter = function (index) {
        personFindService.findBroker().then(function (data) {
          $scope.instrumentConversionData.underwriters[index].id = data.id;
          $scope.instrumentConversionData.underwriters[index].name = data.name;
        });
      };
      // Add new Under writer
      $scope.addNewUnderwriter = function (model) {
        model.push(angular.copy(underwriterTemplate));
      };

      // Remove Under writer
      $scope.removeUnderwriter = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove custom Identificator
      $scope.addNewIdentificator = function (model) {
        model.push(angular.copy(customIdentificatorsTemplate));
      };

      $scope.removeIdentificator = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove coupon payments
      $scope.addNewCouponPayment = function (model) {
        model.push(angular.copy(couponPaymentTemplate));
      };

      $scope.removeCouponPayment = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove fiscal date
      $scope.addNewFiscalDate = function (model) {

        model.push(angular.copy(fiscalDatesTemplate));
      };

      $scope.removeFiscalDate = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove security groups
      $scope.addNewSecurityGroup = function (model) {
        model.push(angular.copy(securityGroupTemplate));
      };

      $scope.removeSecurityGroup = function (index, model) {
        model.splice(index, 1);
      };

      // Query CFI hierarchy
      $scope.getChildOptions = function (classId, index) {
        if (index != 6) {
          var startIndex;

          if (classId == '') {
            startIndex = index + 1;
          } else {

            startIndex = index + 2;
            classId = parseInt(classId);

            if (classId > 0) {

              Loader.show(true);

              referenceDataService.getCFIClasses(classId).then(function (data) {

                var nextClassesIndex = index + 1;
                $scope.metaData['sel' + nextClassesIndex] = data;

                Loader.show(false);

              });
            }
          }
          if (startIndex <= 6) {
            for (var i = startIndex; i <= 6; i++) {
              $scope.metaData['sel' + i] = null;
            }
          }
        }

      };

      // Get generated CFI
      $scope.generateCFI = function (window) {

        $scope.instrumentConversionData.newSecurity.cfiForm.name.$submitted = true;

        if ($scope.instrumentConversionData.newSecurity.cfiForm.name.$valid) {

          for (var i = 0; i < $scope.metaData.sel6.length; i++) {

            var searchedID = parseInt($scope.instrumentConversionData.newSecurity.cfiForm.sel6);
            var currentID = parseInt($scope.metaData.sel6[i]['id']);

            if (searchedID === currentID) {

              $scope.instrumentConversionData.newSecurity.CFI = $scope.metaData.sel6[i];
              window.close();

              $scope.instrumentConversionData.newSecurity.cfiForm.sel6 =
                $scope.instrumentConversionData.newSecurity.cfiForm.sel5 =
                  $scope.instrumentConversionData.newSecurity.cfiForm.sel4 =
                    $scope.instrumentConversionData.newSecurity.cfiForm.sel3 =
                      $scope.instrumentConversionData.newSecurity.cfiForm.sel2 = '';


              if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_D')) {
                $scope.instrumentConversionData.newSecurity.config.fields = bondConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_R_W')) {

                $scope.instrumentConversionData.config.security.isRW = true;
                $scope.instrumentConversionData.newSecurity.config.fields = warrantConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_R_S')) {

                $scope.instrumentConversionData.config.security.isRS = true;
                $scope.instrumentConversionData.newSecurity.config.fields = subscriptionConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_S')) {

                $scope.instrumentConversionData.newSecurity.config.fields = commonStockConfig;
              }

              if (
                helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_P') ||
                helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_R') ||
                helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_C') ||
                helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_F') ||
                helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_V')

              ) {
                $scope.instrumentConversionData.newSecurity.config.fields = convertibleStockConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrumentConversionData.newSecurity.CFI.code, 'CFI_E_U')) {
                $scope.instrumentConversionData.newSecurity.config.fields = mutualFundConfig;
              }

              break;
            }
          }

        }
      };

    }]);
