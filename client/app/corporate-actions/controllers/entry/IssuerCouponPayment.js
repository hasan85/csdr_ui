'use strict';

angular.module('cmisApp')
  .controller('IssuerCouponPaymentController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', 'personFindService',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, dataSearchService, gettextCatalog, SweetAlert, personFindService) {

        $scope.metaData = {
          currencies: []
        };


        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });

        var prepareFormData = function (viewModel) {
          viewModel.currency = angular.fromJson(viewModel.currency).code;
          viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);
          return viewModel;
        };

        $scope.data = angular.fromJson(task.draft);

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "enterBankStatementDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  $scope.config.completeTask(prepareFormData(angular.copy($scope.data)));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        $scope.findPersonStatement = function () {

          personFindService.findAccount({
            roleCodes: ["ROLE_ISSUER"],
            accountClasses: ["ACCOUNT_ISSUER"]
          }).then(function (data) {
            $scope.data.issuerAccount = data
          });

        };

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
