'use strict';

angular.module('cmisApp')
  .controller('BusinessDayOpeningController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationState', 'task',
    function ($scope, $windowInstance, id, appConstants, operationState, task) {


      var today = new Date();
      var day = today.getDate();
      var month = today.getMonth()+1;
      var year = today.getFullYear();
      $scope.currentDate = day + "-" + month + "-" + year;

      console.log("jrevbr4ii", $scope.currentDate);

      $scope.comingData = angular.fromJson(task.draft);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "parValueChangeDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.completeTask(angular.copy($scope.comingData));
            }
          }
        }
      };
      
      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.parValueChangeData);
      });
      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

    }]);
