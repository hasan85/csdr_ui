'use strict';

angular.module('cmisApp')
  .controller('IssueFinalizationController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState',
    'task', 'referenceDataService', 'Loader', 'corporateActionFindService',
    'issueRegistrationService', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState,
              task, referenceDataService, Loader, corporateActionFindService,
              issueRegistrationService, SweetAlert, gettextCatalog) {

      var validateFormData = function () {
        if ($scope.issueFinalizationData.config.sendToTreasure.visible) {
          if ($scope.issueFinalizationData.sendToTreasure === "default") {
            SweetAlert.swal('', gettextCatalog.getString('Please select and option for Send To Treasure input'), 'error');
            return false;
          }
        }
        return true;
      };
      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }
        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }
        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }
        if (formData.corporateAction.registrationDate) {
          formData.corporateAction.registrationDate = helperFunctionsService.generateDateTime(formData.corporateAction.registrationDate);
        }
        if (formData.registrationAuthorityClass && formData.registrationAuthorityClass != '') {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        if (formData.corporateAction.entitlementSecurity) {
          for (var i = 0; i < formData.corporateAction.entitlementSecurity.length; i++) {
            var obj = formData.corporateAction.entitlementSecurity[i];
            var instrumentBuf = {
              name: obj.name,
              ISIN: obj.ISIN,
              issuer: {
                name: obj.issuer.name,
              }
            };
            formData.corporateAction.entitlementSecurity[i] = instrumentBuf;
          }
        }
        if (formData.corporateAction.underlyingSecurity) {
          for (var i = 0; i < formData.corporateAction.underlyingSecurity.length; i++) {
            var obj = formData.corporateAction.underlyingSecurity[i];
            var instrumentBuf = {
              name: obj.name,
              ISIN: obj.ISIN,
              issuer: {
                name: obj.issuer.name,
              }
            };
            formData.corporateAction.underlyingSecurity[i] = instrumentBuf;
          }
        }

        if (formData.sendToTreasure) {
          if (formData.sendToTreasure === "true") {
            formData.sendToTreasure = true;
          }
          if (formData.sendToTreasure === "false") {
            formData.sendToTreasure = false;
          }
          if (formData.sendToTreasure === "default") {
            formData.sendToTreasure = null;
          }
        }
        return formData;
      };


        var initializeFormData = function (draft) {
            if (!draft) {
                draft = {}
            }
            if (draft.issueSuccess === null || draft.issueSuccess === undefined) {
                draft.issueSuccess = true;
            }
            if (draft.corporateAction === null || draft.corporateAction === undefined) {
                draft.corporateAction = null;
            }
            if (draft.sendToTreasure === null || draft.sendToTreasure === undefined) {
                draft.sendToTreasure = "default";
            }
            if (draft.config === null || draft.config === undefined) {
                draft.config = {
                    sendToTreasure: {
                        visible: false
                    }
                };
            }
            return draft;
        };

        $scope.issueFinalizationData = initializeFormData(angular.fromJson(task.draft));


      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "issueFinalizationDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                if (validateFormData()) {
                  var formData = prepareFormData(angular.copy($scope.issueFinalizationData));
                  $scope.config.completeTask(formData);
                }
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.issueFinalizationData);
      });

      //Get metadata
      Loader.show(true);
      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
            regAuthClasses: data
        };
          referenceDataService.normalizeReturnedRecordTask($scope.issueFinalizationData, $scope.metaData);
        Loader.show(false);
      });

      // Search corporate action
      $scope.findCorporateAction = function () {

        corporateActionFindService.findIssueRegistration().then(function (data) {

          $scope.issueFinalizationData.corporateAction = data;

          issueRegistrationService.getIssueQuantities(data.id).then(function (iq) {

            $scope.issueFinalizationData.corporateAction.issueQuantity = iq;

            if (iq.placedQuantity < iq.issuedQuantity) {
              $scope.issueFinalizationData.config.sendToTreasure.visible = true;
            }
          });

          corporateActionFindService.getCorporateActionById(data.id).then(function (res) {

            Loader.show(false);
            if (res && res.success === "true") {
              $scope.issueFinalizationData.corporateAction.underlyingSecurity = res.data.underlyingSecurity;
              $scope.issueFinalizationData.corporateAction.entitlementSecurity = res.data.entitlementSecurity;
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
            }
          });
        });
      };

    }]);
