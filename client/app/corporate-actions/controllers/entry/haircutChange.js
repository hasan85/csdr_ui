'use strict';

angular.module('cmisApp')
  .controller('HaircutChangeController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'SweetAlert','gettextCatalog',
    'referenceDataService', 'Loader', 'instrumentDeletionService', 'personFindService', 'manageInstrumentService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, SweetAlert,gettextCatalog,
              referenceDataService, Loader, instrumentDeletionService, personFindService, manageInstrumentService) {

      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }
        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }

        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }
        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }
        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        if (formData.newHaircutValue) {
          formData.newHaircutValue = formData.newHaircutValue / 100;
        }
        if (formData.oldHaircutValue) {
          formData.oldHaircutValue = formData.oldHaircutValue / 100;
        }
        return formData;
      };
      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit :true,

      };


        var initializeFormData = function (draft) {
            if (!draft) {
                draft = {}
            }
            if (draft.issuer === null || draft.issuer === undefined) {
                draft.issuer = {};
            }
            if (draft.instrument === null || draft.instrument === undefined) {
                draft.instrument = {};
            }
            return draft;
        };

        $scope.haircutChangeData = initializeFormData(angular.fromJson(task.draft));

      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "haircutChangeDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = prepareFormData(angular.copy($scope.haircutChangeData));
                $scope.config.completeTask(formData);
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.haircutChangeData);
      });

      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data
        };
          referenceDataService.normalizeReturnedRecordTask($scope.haircutChangeData, $scope.metaData);
        Loader.show(false);
      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {

          $scope.haircutChangeData.issuer.name = data.name;
          $scope.haircutChangeData.issuer.accountId = data.accountId;
          $scope.haircutChangeData.issuer.id = data.id;
          $scope.haircutChangeData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
        });
      };

      // Search instrument
      $scope.findInstrument = function () {
        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.haircutChangeData.instrument.ISIN = data.isin;
          $scope.haircutChangeData.instrument.id = data.id;
          $scope.haircutChangeData.instrument.issuerName = data.issuerName;
          $scope.haircutChangeData.instrument.CFI = data.CFI;
          $scope.haircutChangeData.oldHaircutValue = parseFloat(data.haircut) * 100;
        });

      };

      $scope.compareQuantities = function (val) {

        if (val == $scope.haircutChangeData.oldHaircutValue) {
          return false;
        }

        return true;
      }

    }]);
