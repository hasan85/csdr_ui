/**
 * Created by maykinayki on 7/5/16.
 */
'use strict';

angular.module('cmisApp').controller('SubscriptionAccessManagementController', ['$scope', 'gettextCatalog', '$windowInstance', 'id', 'appConstants', 'operationService',     'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'personFindService', 'manageInstrumentService', 'SweetAlert',     function ($scope, gettextCatalog, $windowInstance, id, appConstants, operationService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, personFindService, manageInstrumentService, SweetAlert) {

    var memberTemplate = {
        id: null,
        name: null
    };

    var initializeFormData = function (draft) {
        if (!draft || typeof draft != "object") {
            draft = {};
        }
        return draft;
    };
    $scope.subscriptionAccess = initializeFormData(angular.fromJson(task.draft));
    $scope.selectedSubscription = $scope.subscriptionAccess.subscriptions ? $scope.subscriptionAccess.subscriptions[0] : {};

    $scope.changeSelectedSubscription = function() {
        $scope.selectedSubscriptionModel = $scope.selectedSubscription;
        if($scope.selectedSubscriptionModel.members.length == 0) {
            $scope.selectedSubscriptionModel.members.push(angular.copy(memberTemplate));
        }
    };
    $scope.changeSelectedSubscription();

    //Initialize scope variables [[
    $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
            name: "subscriptionAccessOperationForm",
            data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
            complete: {
                click: function () {
                    $scope.config.form.name.$submitted = true;
                    if ($scope.config.form.name.$valid) {
                        var formData = $scope.subscriptionAccess;
                        $scope.config.completeTask(formData);
                    } else {
                        SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                    }
                }
            }
        }
    };

    // Check if form is dirty
    $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
    });

    // Save task as draft
    $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.subscriptionAccess);
    });

    // Search under writer
    $scope.findMember = function (index, model) {
        personFindService.findBroker().then(function (data) {
            model[index].id = data.id;
            model[index].name = data.name;
        });
    };

    // Add new Under writer
    $scope.addNewMember = function (model) {
        model.push(angular.copy(memberTemplate));
    };

    // Remove Under writer
    $scope.removeMember = function (index, model) {
        model.splice(index, 1);
    };

}]);
