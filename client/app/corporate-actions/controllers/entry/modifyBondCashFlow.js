'use strict';

angular.module('cmisApp').controller('IssuerCouponController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'Loader', 'manageInstrumentService', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, Loader, manageInstrumentService, SweetAlert) {

      $scope.languages = appConstants.languages;
      var prepareFormData = function (formData) {
        for (var i = 0; i < formData.couponPayments.length; i++) {
          if (formData.couponPayments[i].paymentDate) {

            formData.couponPayments[i].paymentDate = helperFunctionsService.generateDateTime(formData.couponPayments[i].paymentDateObj);
          }
          if (formData.couponPayments[i].effectiveDate) {
            formData.couponPayments[i].effectiveDate = helperFunctionsService.generateDateTime(formData.couponPayments[i].effectiveDateObj);
          }

        }
        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.instrument === null || draft.instrument === undefined) {
          draft.instrument = {};
        }
        if (draft.couponPayments === null || draft.couponPayments === undefined) {
          draft.couponPayments = null;
        }
        if (draft.config === null || draft.config === undefined) {
          draft.config = {
            todayDate: new Date()
          };
        }
        return draft;
      };

      $scope.bondCashFlowData = initializeFormData(angular.fromJson(task.draft));

      $scope.bondCashFlowData.couponPayments.map(function (item) {
        if (item.paymentDate && item.paymentDateObj) {
          item.paymentDate = helperFunctionsService.generateDateTime(item.paymentDateObj);
        }
        if (item.effectiveDate && item.effectiveDateObj) {
          item.effectiveDate = helperFunctionsService.generateDateTime(item.effectiveDateObj);
        }
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "bondCashFlowDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid = true) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.bondCashFlowData)));
              }
            }
          }
        },


      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.bondCashFlowData);
      });

      // Search account
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument({
          searchUrl: 'findBond',
        }).then(function (data) {


          Loader.show(true);
          manageInstrumentService.getBondCashFlow(data.id).then(function (cashFlow) {
            console.log('cashflow', cashFlow);
            if (cashFlow && cashFlow.success == "true" && cashFlow.data && cashFlow.data.length > 0) {
              console.log(cashFlow);
              $scope.bondCashFlowData.instrument.ISIN = data.isin;
              $scope.bondCashFlowData.instrument.id = data.id;
              $scope.bondCashFlowData.instrument.CFI = data.CFI;
              $scope.bondCashFlowData.instrument.issuerName = data.issuerName;
              $scope.bondCashFlowData.couponPayments = cashFlow.data;
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(cashFlow), 'error');
            }
            Loader.show(false);
          });

        });
      };

    }]);
