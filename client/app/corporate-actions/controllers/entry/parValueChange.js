'use strict';

angular.module('cmisApp')
  .controller('ParValueChangeController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task','SweetAlert','gettextCatalog',
    'referenceDataService', 'Loader', 'instrumentDeletionService', 'personFindService', 'manageInstrumentService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, SweetAlert,gettextCatalog,
              referenceDataService, Loader, instrumentDeletionService, personFindService, manageInstrumentService) {


      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }

        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }

        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }

        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }

        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        return formData;
      };


      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null,
          isIssue: false
        },
        searchUrl: 'findInstrument',
        searchOnInit :true,

      };


        var initializeFormData = function (draft) {
            if (!draft) {
                draft = {}
            }
            if (draft.issuer === null || draft.issuer === undefined) {
                draft.issuer = {};
            }
            if (draft.instrument === null || draft.instrument === undefined) {
                draft.instrument = {};
            }
            if (draft.boughtBackQuantity === null || draft.boughtBackQuantity === undefined) {
                draft.boughtBackQuantity = null;
            }
            return draft;
        };

        $scope.parValueChangeData = initializeFormData(angular.fromJson(task.draft));

      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "parValueChangeDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = prepareFormData(angular.copy($scope.parValueChangeData));
                $scope.config.completeTask(formData);
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.parValueChangeData);
      });

      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {

        $scope.metaData = {
          registrationAuthorityClasses: data
        };
          referenceDataService.normalizeReturnedRecordTask($scope.parValueChangeData, $scope.metaData);

        Loader.show(false);

      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {

          $scope.parValueChangeData.issuer.name = data.name;
          $scope.parValueChangeData.issuer.accountId = data.accountId;
          $scope.parValueChangeData.issuer.id = data.id;
          $scope.parValueChangeData.issuer.accountNumber = data.accountNumber;
          $scope.instrumentSearchParams.params.issuerId = data.id;
        });
      };

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          $scope.parValueChangeData.instrument.ISIN = data.isin;
          $scope.parValueChangeData.instrument.id = data.id;
          $scope.parValueChangeData.instrument.issuerName = data.issuerName;
          $scope.parValueChangeData.instrument.CFI = data.CFI;
          //$scope.parValueChangeData.oldParValue = data.parValue;
          $scope.parValueChangeData.oldParValue = parseFloat(data.parValue);
        });
      };

      $scope.compareQuantities = function (val) {
        return val != $scope.parValueChangeData.oldParValue;
      }

    }]);
