'use strict';

angular.module('cmisApp')
  .controller('MergerController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'Loader', 'mergerService', 'manageInstrumentService',
    'gettextCatalog', 'personFindService', 'SweetAlert', 'issueRegistrationService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task,
              referenceDataService, Loader, mergerService, manageInstrumentService,
              gettextCatalog, personFindService, SweetAlert, issueRegistrationService) {

      var issuerTemplate = {
        id: null,
        name: null,
        accountId: null,
        accountNumber: null,
        instruments: null
      };

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument'
      };

      // metadata
      $scope.metaData = {
        sel1: [],
        sel2: [],
        sel3: [],
        sel4: [],
        sel5: [],
        sel6: [],
        convertableStockes: null
      };

      // Initialize local variables
      var commonStockConfig, convertibleStockConfig, bondConfig, mutualFundConfig, subscriptionConfig, warrantConfig = {};

      var instrumentFormTemplate = manageInstrumentService.getInstrumentFormTemplate();

      instrumentFormTemplate.partialFormName = "instrument_form_0";

      $scope.mergerData = {
        issuers: [
          angular.copy(issuerTemplate)
        ],
        mergedInstruments: [],

      };

      $scope.metaData = {};

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "mergerDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;

              var isPartialFormsValid = true;
              var newInstrumentSelected = true;

              if ($scope.mergerData.mergedInstruments) {
                for (var i = 0; i < $scope.mergerData.mergedInstruments.length; i++) {

                  if ($scope.mergerData.mergedInstruments[i].newInstrumentType == $scope.config.newInstrumentTypes.new) {
                    var newInstrument = $scope.mergerData.mergedInstruments[i].newInstrument;
                    newInstrument.partialFormName.$submitted = true;
                    if (newInstrument.partialFormName.$invalid) {
                      isPartialFormsValid = false;
                    }

                  } else {

                    if ($scope.mergerData.mergedInstruments[i].newInstrument && !!$scope.mergerData.mergedInstruments[i].newInstrument.id) {
                      newInstrumentSelected = false;
                    }
                  }


                }
              }

              if ($scope.config.form.name.$valid && isPartialFormsValid) {

                if ($scope.mergerData.mergedInstruments.length == 0) {
                  SweetAlert.swal('', gettextCatalog.getString('You have to merge at least two instrument!'), 'error');
                }
                else if (!newInstrumentSelected) {
                  SweetAlert.swal('', gettextCatalog.getString('You have to select new instrument!'), 'error');
                }
                else {

                  var formData = mergerService.prepareFormData($scope.mergerData);
                  $scope.config.completeTask(formData);

                }

              }

            }
          }
        },
        labels: {
          lblCFIWindowTitle: gettextCatalog.getString('CFI Generate'),
          lblCategory: gettextCatalog.getString('Category'),
          lblGroup: gettextCatalog.getString('Group'),
          lblAttribute: gettextCatalog.getString('Attribute'),
          lblOldInstruments: gettextCatalog.getString('Old Instruments'),
          lblNewInstrument: gettextCatalog.getString('New Instrument')
        },
        newInstrumentTypes: {
          new: 'new',
          existing: 'existing'
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.mergerData);
      });

      //Get metadata
      Loader.show(true);

      mergerService.getMetaData().then(function (data) {

        $scope.metaData = {
          fractionTreatmentClasses: data.fractionTreatmentClasses,
          registrationAuthorityClasses: data.registrationAuthorityClasses,
          placementClasses: data.placementClasses,
          identificatorClasses: data.identificatorClasses,
          currencies: data.currencies,
          securityGroups: data.securityGroups,
          sel1: data.CFIcategories,
          couponPaymentMethodClasses: data.couponPaymentMethodClasses,
          convertableStockes: null
        };

        var payload = angular.fromJson(task.draft).config;

        if (payload.haircutRatio) {
          instrumentFormTemplate.haircutRatio = payload.haircutRatio * 100;
        }
        if (payload.conventionalDayCount) {
          instrumentFormTemplate.conventionalDayCount = payload.conventionalDayCount;
        }

        // Get config data
        var commonStockConfigRaw = payload['commonStockConfiguration'] ? angular.fromJson(payload['commonStockConfiguration']) : null;
        var convertibleStockConfigRaw = payload['convertibleStockConfiguration'] ? angular.fromJson(payload['convertibleStockConfiguration']) : null;
        var bondConfigRaw = payload['bondConfiguration'] ? angular.fromJson(payload['bondConfiguration']) : null;

        var mutualFundConfigurationRaw = payload['mutualFundConfiguration'] ? angular.fromJson(payload['mutualFundConfiguration']) : null;
        var warrantConfigurationRaw = payload['warrantConfiguration'] ? angular.fromJson(payload['warrantConfiguration']) : null;
        var subscriptionConfigurationRaw = payload['subscriptionConfiguration'] ? angular.fromJson(payload['subscriptionConfiguration']) : null;


        // Prepare config data
        commonStockConfig = commonStockConfigRaw ? manageInstrumentService.generateFormConfig(commonStockConfigRaw) : null;
        convertibleStockConfig = convertibleStockConfigRaw ? manageInstrumentService.generateFormConfig(convertibleStockConfigRaw) : null;
        bondConfig = bondConfigRaw ? manageInstrumentService.generateFormConfig(bondConfigRaw) : null;
        mutualFundConfig = mutualFundConfigurationRaw ? manageInstrumentService.generateFormConfig(mutualFundConfigurationRaw) : null;
        warrantConfig = warrantConfigurationRaw ? manageInstrumentService.generateFormConfig(warrantConfigurationRaw) : null;
        subscriptionConfig = subscriptionConfigurationRaw ? manageInstrumentService.generateFormConfig(subscriptionConfigurationRaw) : null;

        instrumentFormTemplate.config.fields = commonStockConfig;

        Loader.show(false);

      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
          //$scope.mergerData = angular.fromJson(task.draft);
        }
      }

      // Search issuer
      $scope.findCurrentIssuer = function ($index) {
        personFindService.findIssuer().then(function (data) {

          $scope.mergerData.issuers[$index].name = data.name;
          $scope.mergerData.issuers[$index].accountId = data.accountId;
          $scope.mergerData.issuers[$index].id = data.id;
          $scope.mergerData.issuers[$index].accountNumber = data.accountNumber;
          //$scope.instrumentSearchParams.params.issuerId = data.id;
          instrumentFindService.findInstrumentByData({'issuerId': data.id}, 'findInstrument/').then(function (data) {

            if (data) {

              $scope.mergerData.issuers[$index].instruments = [];

              for (var i = 0; i < data.length; i++) {

                data[i].ISIN = data[i].isin;

                delete data[i].isin;
                delete data[i].currency;

                data[i]['issuer'] = {
                  id: data[i].issuerId,
                };

                data[i]['isSelected'] = false;

                $scope.mergerData.issuers[$index].instruments.push(data[i]);
              }
            }
            else {
              SweetAlert.swal('', gettextCatalog.getString('There is not any instrument of this issuer'), 'error');
            }

          });
        });

      };
      $scope.addNewIssuer = function (model) {
        model.push(angular.copy(issuerTemplate));
      };
      $scope.removeIssuer = function (index, model) {
        model.splice(index, 1);
      };


      // Add new instrument
      $scope.addNewInstrument = function (model) {

        var newInstrumentForm = angular.copy(instrumentFormTemplate);
        newInstrumentForm.partialFormName = 'instrument_form_' + (model.length + 1);
        model.push({
          newInstrument: newInstrumentForm,
          oldInstruments: angular.copy($scope.mergerData.issuers),
          newInstrumentType:  angular.copy($scope.config.newInstrumentTypes.new),
          newInstrumentBuff: {}
        });

      };


      $scope.newInstrumentSelectedFromExisting = function (merging) {

        //  merging.newInstrument = selectedInstrument;

      };
      $scope.newInstrumentTypeChange = function (mi) {
        mi.newInstrumentBuff = {};
      };

      $scope.removeNewInstrument = function (model) {
        model.splice(model.length - 1, 1);
      };

      // Find issuer
      $scope.findIssuer = function (instrument) {

        personFindService.findIssuer().then(function (data) {

          instrument.issuer.name = data.name;
          instrument.issuer.id = data.id;
          instrument.issuer.accountId = data.accountId;
          instrument.issuer.accountNumber = data.accountNumber;

          if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_W') || helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_S')) {
            issueRegistrationService.getIssuerEquities(data.id).then(function (data1) {
              instrument.metaData.convertableStockes = data1;
            });
          }
        });


      };
      // CFI generation

      // Query CFI hierarchy
      $scope.getChildOptions = function (classId, index) {

        if (index != 6) {
          var startIndex;

          if (classId == '') {
            startIndex = index + 1;
          } else {

            startIndex = index + 2;
            classId = parseInt(classId);

            if (classId > 0) {

              Loader.show(true);

              referenceDataService.getCFIClasses(classId).then(function (data) {

                var nextClassesIndex = index + 1;
                $scope.metaData['sel' + nextClassesIndex] = data;

                Loader.show(false);

              });
            }
          }
          if (startIndex <= 6) {
            for (var i = startIndex; i <= 6; i++) {
              $scope.metaData['sel' + i] = null;
            }
          }
        }

      };

      // Get generated CFI
      $scope.generateCFI = function (window, instrument) {

        instrument.cfiForm.name.$submitted = true;

        if (instrument.cfiForm.name.$valid) {

          for (var i = 0; i < $scope.metaData.sel6.length; i++) {

            var searchedID = parseInt(instrument.cfiForm.sel6);
            var currentID = parseInt($scope.metaData.sel6[i]['id']);

            if (searchedID === currentID) {

              instrument.CFI = $scope.metaData.sel6[i];
              window.close();

              instrument.cfiForm.sel6 =
                instrument.cfiForm.sel5 =
                  instrument.cfiForm.sel4 =
                    instrument.cfiForm.sel3 =
                      instrument.cfiForm.sel2 = '';


              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_D')) {
                instrument.config.fields = bondConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_W')) {


                instrument.config.fields = warrantConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_R_S')) {

                instrument.config.fields = subscriptionConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_S')) {

                instrument.config.fields = commonStockConfig;
              }

              if (
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_P') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_R') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_C') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_F') ||
                helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_V')

              ) {
                instrument.config.fields = convertibleStockConfig;
              }

              if (helperFunctionsService.startsWith(instrument.CFI.code, 'CFI_E_U')) {
                instrument.config.fields = mutualFundConfig;
              }

              break;
            }
          }

        }
      };

    }]);
