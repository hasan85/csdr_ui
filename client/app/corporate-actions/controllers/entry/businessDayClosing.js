'use strict';

angular.module('cmisApp')
  .controller('BusinessDayClosingController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
     'helperFunctionsService', 'operationState', 'task','SweetAlert','gettextCatalog',
    'referenceDataService', 'Loader',
    function ($scope, $windowInstance, id, appConstants, operationService,
               helperFunctionsService, operationState, task, SweetAlert,gettextCatalog,
              referenceDataService, Loader) {



      $scope.comingData = angular.fromJson(task.draft);

      console.log("cj",$scope.comingData);


      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "comingDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              //$scope.config.completeTask(angular.copy($scope.comingData));

              $scope.hasClosed = false;
              for( var i = 0; i < $scope.comingData.length; i++ ) {

                if($scope.comingData[i].closed == false) {
                  $scope.hasClosed = true;
                  break;
                }

                }
              
              if($scope.hasClosed) {
                SweetAlert.swal({
                    title: "Birjanın bazarları tam bağlanmayıbdır.",
                    text: "Davam etmək istəyirsiniz mi?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Bəli",
                    cancelButtonText: "Xeyr",
                    closeOnConfirm: true,
                    closeOnCancel: true
                  },
                  function(isConfirm){
                    if (isConfirm) {
                      $scope.config.completeTask(angular.copy($scope.comingData));
                    }
                  });
              } else {
                $scope.config.completeTask(angular.copy($scope.comingData));
              }


            }

          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.comingData);
      });


    }]);
