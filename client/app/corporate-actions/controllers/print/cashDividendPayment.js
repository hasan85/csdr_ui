'use strict';

angular.module('cmisApp')
  .controller('CashDividendPaymentPrintController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'reportService', 'Loader', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, reportService, Loader, SweetAlert, gettextCatalog) {
      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
      };
      $scope.config.gridData = {
        scrollable: true,
        pageable: true,
        columns: [
          {
            field: "account.name",
            title: gettextCatalog.getString("Shareholder")
          }, {
            field: "account.accountNumber",
            title: gettextCatalog.getString("Account Number")
          },
          {
            field: "bankAccount",
            title: gettextCatalog.getString("Bank Account")
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount")
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency")
          },


        ]
      };
      var gridData = angular.fromJson(task.draft);
      $scope.config.cashDividendPayments = {
        data: gridData ? gridData.cashDividendPayments : [],
        total: gridData ? gridData.length : 0,
        pageSize: 20
      };
    }]);
