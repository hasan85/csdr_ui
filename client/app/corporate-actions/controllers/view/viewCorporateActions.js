'use strict';

angular.module('cmisApp')
  .controller('ViewCorporateActionsController', ['$scope', '$windowInstance', 'corporateActionFindService', 'id',
    function ($scope, $windowInstance, corporateActionFindService, id) {

      // Generic search configuration for corporateAction
      $scope.searchConfig = {
        corporateAction: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedCorporateActionIndex: false
        },
        params: {
          searchUrl: '/api/corporate-actions/getCorporateActions',
          showSearchCriteriaBlock: false
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          print: false,
          view: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        }
      };

      $scope.$watch('searchConfig.corporateAction.selectedCorporateActionIndex', function (val) {

        if (val !== false) {
        }
        else {
          $scope.searchConfig.corporateAction.selectedCorporateActionIndex = false;

        }

      });

    }]);
