'use strict';

angular.module('cmisApp')
  .factory('mergerService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', 'manageInstrumentService', "$http",
    function (referenceDataService, $q, Loader, helperFunctionsService, manageInstrumentService, $http) {

      var prepareFormData = function (formData) {

        var result = {};

        result.registrationNumber = formData.registrationNumber;

        if (formData.registrationDateObj) {
          result.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }

        if (formData.valueDateObj) {
          result.valueDate =helperFunctionsService.generateDateTime( formData.valueDateObj);
        }

        if (formData.registrationAuthorityClass) {
          result.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        result.issuers = angular.copy(formData.issuers);
        if (result.issuers) {
          for (var i = 0; i < result.issuers.length; i++) {
            delete result.issuers[i].instruments;
          }
        }

        result.mergedInstruments = [];

        if (formData.mergedInstruments && formData.mergedInstruments.length > 0) {

          for (var i = 0; i < formData.mergedInstruments.length; i++) {

            result.mergedInstruments.push({newInstrument: null, oldInstruments: null});

            if (formData.mergedInstruments[i].newInstrumentBuff && formData.mergedInstruments[i].newInstrumentBuff.id) {
              result.mergedInstruments[result.mergedInstruments.length - 1].newInstrument = formData.mergedInstruments[i].newInstrumentBuff;
            }
            else if (formData.mergedInstruments[i].newInstrument) {

              result.mergedInstruments[result.mergedInstruments.length - 1].newInstrument = manageInstrumentService.prepareFormData(formData.mergedInstruments[i].newInstrument);
            }

            var oldInstruments = [];

            for (var j = 0; j < formData.mergedInstruments[i].oldInstruments.length; j++) {

              var issuer = formData.mergedInstruments[i].oldInstruments[j];
              for (var k = 0; k < issuer.instruments.length; k++) {
                var instrument = issuer.instruments[k];
                if (instrument.isSelected) {
                  instrument['fractionTreatmentClass'] = angular.fromJson(instrument['fractionTreatmentClass']);
                  oldInstruments.push(instrument);
                }
              }
            }

            if (oldInstruments.length > 0) {
              result.mergedInstruments[result.mergedInstruments.length - 1]['oldInstruments'] = oldInstruments;
            } else {
              result.mergedInstruments.splice(result.mergedInstruments.length - 1, 1);
            }


          }
        }
        return result;
      };

      //Return meta data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};

        var getCouponPaymentMethodClasses = function (value) {
          metaData.couponPaymentMethodClasses = value;
        };

        var getIdentificatorClasses = function (value) {
          metaData.identificatorClasses = value;
        };
        var getIndividualSecurityGroups = function (value) {
          metaData.securityGroups = value;
        };
        var getCFICategories = function (value) {
          metaData.CFIcategories = value;
        };
        var getCurrencies = function (value) {
          metaData.currencies = value;
        };

        var getRegAuthorityClasses = function (value) {
          metaData.registrationAuthorityClasses = value;
        };
        var getFractionTreatmentClasses = function (value) {
          metaData.fractionTreatmentClasses = value;
        };

        $q.all([
          referenceDataService.getInstrumentIdentificatorTypes().then(getIdentificatorClasses),
          referenceDataService.getCurrencies().then(getCurrencies),
          referenceDataService.getIndividualSecurityGroups().then(getIndividualSecurityGroups),
          referenceDataService.getCFIClasses(0).then(getCFICategories),
          referenceDataService.getCouponPaymentMethodClasses().then(getCouponPaymentMethodClasses),
          referenceDataService.getRegistrationAuthorityClasses().then(getRegAuthorityClasses),
          referenceDataService.getFractionTreatmentClasses().then(getFractionTreatmentClasses)
        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };

      return {
        getMetaData: getMetaData,
        prepareFormData: prepareFormData,
      };

    }]);
