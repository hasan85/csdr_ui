'use strict';

angular.module('cmisApp')
  .factory('demergerService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', 'manageInstrumentService', "$http",
    function (referenceDataService, $q, Loader, helperFunctionsService, manageInstrumentService, $http) {


      var prepareFormData = function (formData) {

        var result = {};

        result.registrationNumber = formData.registrationNumber;
        result.issuer = formData.issuer;
        if (formData.registrationDateObj) {
          result.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }

        if (formData.valueDateObj) {
          result.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }

        if (formData.registrationAuthorityClass) {
          result.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }


        result.instruments = [];

        if (formData.instruments && formData.instruments.length > 0) {

          for (var i = 0; i < formData.instruments.length; i++) {

            result.instruments.push({newInstruments: null, oldInstrument: null});
            if (formData.instruments[i].oldInstrument) {

              result.instruments[result.instruments.length-1].oldInstrument = formData.instruments[i].oldInstrument;
            }
            var newInstruments = [];

            if (formData.instruments[i].newInstruments) {
              for (var j = 0; j < formData.instruments[i].newInstruments.length; j++) {

                newInstruments.push(manageInstrumentService.prepareFormData(formData.instruments[i].newInstruments[j]));
              }
            }
            if (newInstruments.length > 0) {
              result.instruments[result.instruments.length-1]['newInstruments'] = newInstruments;
            } else {
              delete result.instruments.splice(result.instruments.length-1, 1);
            }
          }

        }

        return result;
      };

      //Return meta data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};

        var getCouponPaymentMethodClasses = function (value) {
          metaData.couponPaymentMethodClasses = value;
        };

        var getIdentificatorClasses = function (value) {
          metaData.identificatorClasses = value;
        };
        var getIndividualSecurityGroups = function (value) {
          metaData.securityGroups = value;
        };
        var getCFICategories = function (value) {
          metaData.CFIcategories = value;
        };
        var getCurrencies = function (value) {
          metaData.currencies = value;
        };

        var getRegAuthorityClasses = function (value) {
          metaData.registrationAuthorityClasses = value;
        };
        var getFractionTreatmentClasses = function (value) {
          metaData.fractionTreatmentClasses = value;
        };

        $q.all([
          referenceDataService.getInstrumentIdentificatorTypes().then(getIdentificatorClasses),
          referenceDataService.getCurrencies().then(getCurrencies),
          referenceDataService.getIndividualSecurityGroups().then(getIndividualSecurityGroups),
          referenceDataService.getCFIClasses(0).then(getCFICategories),
          referenceDataService.getCouponPaymentMethodClasses().then(getCouponPaymentMethodClasses),
          referenceDataService.getRegistrationAuthorityClasses().then(getRegAuthorityClasses),
          referenceDataService.getFractionTreatmentClasses().then(getFractionTreatmentClasses)
        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };

      return {
        getMetaData: getMetaData,
        prepareFormData: prepareFormData,


      };


    }])
;
