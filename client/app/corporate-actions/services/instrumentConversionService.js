'use strict';

angular.module('cmisApp')
  .factory('instrumentConversionService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', 'manageInstrumentService', "$filter",
    function (referenceDataService, $q, Loader, helperFunctionsService, manageInstrumentService, $filter) {


      var prepareFormData = function (formData) {

        var result = {};

        result.registrationNumber = formData.registrationNumber;
        result.nominator = formData.nominator;
        result.discriminator = formData.discriminator;
        result.oldSecurity = formData.oldSecurity;

        if (formData.registrationDateObj) {
          result.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }

        if (formData.valueDateObj) {
          result.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }

        if (formData.registrationAuthorityClass) {
          result.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        if (formData.recordDateObj) {

          result.recordDate = helperFunctionsService.generateDateTime(formData.recordDateObj);
        }

        if (formData.fractionTreatmentClass) {
          result.fractionTreatmentClass = angular.fromJson(formData.fractionTreatmentClass);
        }

        result.newSecurity = manageInstrumentService.prepareFormData(formData.newSecurity);

        return result;
      };

      //Return meta data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};

        var getCouponPaymentMethodClasses = function (value) {
          metaData.couponPaymentMethodClasses = value;
        };

        var getIdentificatorClasses = function (value) {
          metaData.identificatorClasses = value;
        };
        var getIndividualSecurityGroups = function (value) {
          metaData.securityGroups = value;
        };
        var getCFICategories = function (value) {
          metaData.CFIcategories = value;
        };
        var getCurrencies = function (value) {
          metaData.currencies = value;
        };

        var getRegAuthorityClasses = function (value) {
          metaData.registrationAuthorityClasses = value;
        };
        var getFractionTreatmentClasses = function (value) {
          metaData.fractionTreatmentClasses = value;
        };

        $q.all([
          referenceDataService.getInstrumentIdentificatorTypes().then(getIdentificatorClasses),
          referenceDataService.getCurrencies().then(getCurrencies),
          referenceDataService.getIndividualSecurityGroups().then(getIndividualSecurityGroups),
          referenceDataService.getCFIClasses(0).then(getCFICategories),
          referenceDataService.getCouponPaymentMethodClasses().then(getCouponPaymentMethodClasses),
          referenceDataService.getRegistrationAuthorityClasses().then(getRegAuthorityClasses),
          referenceDataService.getFractionTreatmentClasses().then(getFractionTreatmentClasses)
        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };


      var normalizeReturnedTask = function (model, metaData) {
        if (model.registrationAuthorityClass && model.registrationAuthorityClass.id) {
          model.registrationAuthorityClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.registrationAuthorityClasses, model.registrationAuthorityClass['id']));
        }
        if (model.fractionTreatmentClass && model.fractionTreatmentClass.id) {
          model.fractionTreatmentClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.fractionTreatmentClasses, model.fractionTreatmentClass['id']));
        }

        if (model.registrationDate && !model.registrationDateObj) {
          model.registrationDateObj = helperFunctionsService.parseDate(model.registrationDate);
          model.registrationDate = helperFunctionsService.generateDate(model.registrationDateObj);
        }
        if (model.valueDate && !model.valueDateObj) {
          model.valueDateObj = helperFunctionsService.parseDate(model.valueDate);
          model.valueDate = helperFunctionsService.generateDate(model.valueDateObj);
        }
        if (model.maturityDate && !model.maturityDateObj) {
          model.maturityDateObj = helperFunctionsService.parseDate(model.maturityDate);
          model.maturityDate = helperFunctionsService.generateDate(model.maturityDateObj);
        }

        if (model.recordDate && !model.recordDateObj) {
          model.recordDateObj = helperFunctionsService.parseDate(model.recordDate);
          model.recordDate = helperFunctionsService.generateDate(model.recordDateObj);
        }
        if (model.finishDate && !model.finishDateObj) {
          model.finishDateObj = helperFunctionsService.parseDate(model.finishDate);
          model.finishDate = helperFunctionsService.generateDate(model.finishDateObj);
        }
        if (model.newSecurity && model.newSecurity.ISIN) {
         // model.newSecurity = model.issue;
          manageInstrumentService.normalizeReturnedTaskData(model.newSecurity, metaData);
          // model.finishDateObj = helperFunctionsService.parseDate(model.finishDate);
          // model.finishDate = helperFunctionsService.generateDate(model.finishDateObj);
        }

      };

      return {
        getMetaData: getMetaData,
        prepareFormData: prepareFormData,
        normalizeReturnedTask: normalizeReturnedTask
      };


    }])
;
