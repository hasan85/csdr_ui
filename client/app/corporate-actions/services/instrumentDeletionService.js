'use strict';

angular.module('cmisApp')
  .factory('instrumentDeletionService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', 'manageInstrumentService', "$http",
    function (referenceDataService, $q, Loader, helperFunctionsService, manageInstrumentService, $http) {


      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {

          formData.registrationDate =helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }


        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {

          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }

        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }

        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        if (formData.instrumentDeletionClass) {
          formData.instrumentDeletionClass = angular.fromJson(formData.instrumentDeletionClass);
        }


        return formData;
      };

      //Return meta data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};


        var getRegAuthorityClasses = function (value) {
          metaData.registrationAuthorityClasses = value;
        };
        var getCAInstrumentDeletionClasses = function (value) {
          metaData.CAInstrumentDeletionClasses = value;
        };


        $q.all([
          referenceDataService.getRegistrationAuthorityClasses().then(getRegAuthorityClasses),
          referenceDataService.getCAInstrumentDeletionClasses().then(getCAInstrumentDeletionClasses)
        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };

      return {
        getMetaData: getMetaData,
        prepareFormData: prepareFormData,


      };


    }])
;
