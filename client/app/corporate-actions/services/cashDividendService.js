'use strict';

angular.module('cmisApp')
  .factory('cashDividendService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', 'manageInstrumentService', "$http",
    function (referenceDataService, $q, Loader, helperFunctionsService, manageInstrumentService, $http) {

      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }

        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }

        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }

        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }

        if (formData.recordDateObj) {
          formData.recordDate = helperFunctionsService.generateDateTime(formData.recordDateObj);
        }

        if (formData.recordDateObj) {
          delete formData.recordDateObj;
        }

        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        return formData;
      };

      //Return meta data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};


        var getRegAuthorityClasses = function (value) {
          metaData.registrationAuthorityClasses = value;
        };
        var getFractionTreatmentClasses = function (value) {
          metaData.fractionTreatmentClasses = value;
        };


        $q.all([

          referenceDataService.getRegistrationAuthorityClasses().then(getRegAuthorityClasses)

        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };

      return {
        getMetaData: getMetaData,
        prepareFormData: prepareFormData


      };


    }])
;
