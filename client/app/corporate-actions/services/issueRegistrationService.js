'use strict';

angular.module( 'cmisApp' )
  .factory( 'issueRegistrationService', [ "referenceDataService", "$q", "Loader", 'helperFunctionsService', 'manageInstrumentService', "$http", '$filter',
    function ( referenceDataService, $q, Loader, helperFunctionsService, manageInstrumentService, $http, $filter ) {


      var apiUrlPartial = '/api/corporate-actions/';

      var getIssuerEquities = function ( issuerId ) {
        return $http.get( apiUrlPartial + "getIssuerEquities/" + issuerId ).then( function ( result ) {
          var res = helperFunctionsService.convertObjectToArray( result.data );
          return res;
        } );
      };

      var getIssueQuantities = function ( corporateActionId ) {
        return $http.get( apiUrlPartial + "getIssueQuantities/" + corporateActionId ).then( function ( result ) {
          return result.data;
        } );
      };

      var prepareFormData = function ( formData ) {
        debugger;

        var result = {};

        result.registrationNumber = formData.registrationNumber;
        result.issueQuantity = formData.issueQuantity;
        result.issueCode = formData.issueCode;
        result.preemptive = formData.preemptive;
        result.sendToSecondaryMarket = formData.sendToSecondaryMarket;
        result.isSubscription = formData.isSubscription;

        if ( formData.registrationDateObj ) {
          result.registrationDate = helperFunctionsService.generateDateTime( formData.registrationDateObj );
        }
        if ( formData.periodStartDateObj ) {
          result.periodStartDate = helperFunctionsService.generateDateTime( formData.periodStartDateObj );
        }
        if ( formData.periodEndDateObj ) {
          result.periodEndDate = helperFunctionsService.generateDateTime( formData.periodEndDateObj );
        }
        if ( formData.recordDateObj ) {
          result.recordDate = helperFunctionsService.generateDateTime( formData.registrationDateObj );
        }

        if ( formData.valueDateObj ) {
          result.valueDate = helperFunctionsService.generateDateTime( formData.valueDateObj );
        }

        if ( formData.registrationAuthorityClass && formData.registrationAuthorityClass != '' ) {
          result.registrationAuthorityClass = angular.fromJson( formData.registrationAuthorityClass );
        }

        if ( formData.finishDateObj ) {
          result.finishDate = helperFunctionsService.generateDateTime( formData.finishDateObj );
        }

        if ( formData.placementClass && formData.placementClass != '' ) {
          result.placementClass = angular.fromJson( formData.placementClass );
        }

        result.underwriters = formData.underwriters;

        if ( formData.underwriters && formData.underwriters.length == 1 && !formData.underwriters[ 0 ][ 'id' ] ) {
          result.underwriters = null;
        }

        if ( formData.config.security.isIPO ) {
          result.security = null;
          if ( formData.instrument ) {
            console.log( formData.instrument )
            result.issue = manageInstrumentService.prepareFormData( formData.instrument );

            console.log( result.issue )


          }
        } else {
          result.security = formData.security;
          result.issueName = formData.issueName;
          result.issueAbbreviation = formData.issueAbbreviation;
          result.issue = null;
        }

        return result;
      };

      var prepareFormDataForSave = function ( formData ) {

        if ( formData && formData.instrument ) {
          delete formData.instrument.cfiForm;
          delete formData.instrument.cfiGenerateWindow;
        }
        return formData;
      };
      //Return meta data
      var getMetaData = function () {

        Loader.show( true );
        var deferred = $q.defer();
        var metaData = {};

        var getCouponPaymentMethodClasses = function ( value ) {
          metaData.couponPaymentMethodClasses = value;
        };
        var getRegAuthorityClasses = function ( value ) {
          metaData.registrationAuthorityClasses = value;
        };
        var getPlacementClasses = function ( value ) {
          metaData.placementClasses = value;
        };

        var getIdentificatorClasses = function ( value ) {
          metaData.identificatorClasses = value;
        };
        var getIndividualSecurityGroups = function ( value ) {
          metaData.securityGroups = value;
        };
        var getCFICategories = function ( value ) {
          metaData.CFIcategories = value;
        };
        var getCurrencies = function ( value ) {
          metaData.currencies = value;
        };

        $q.all( [
          referenceDataService.getRegistrationAuthorityClasses().then( getRegAuthorityClasses ),
          referenceDataService.getPlacementClasses().then( getPlacementClasses ),
          referenceDataService.getInstrumentIdentificatorTypes().then( getIdentificatorClasses ),
          referenceDataService.getCurrencies().then( getCurrencies ),
          referenceDataService.getIndividualSecurityGroups().then( getIndividualSecurityGroups ),
          referenceDataService.getCFIClasses( 0 ).then( getCFICategories ),
          referenceDataService.getCouponPaymentMethodClasses().then( getCouponPaymentMethodClasses )
        ] ).then( function () {
          deferred.resolve( metaData );
          Loader.show( false );
        } );

        return deferred.promise;
      };

      var normalizeReturnedTask = function ( model, metaData ) {
        if ( model.registrationAuthorityClass && model.registrationAuthorityClass.id ) {
          model.registrationAuthorityClass = $filter( 'json' )( helperFunctionsService.findByIdInsideArray( metaData.registrationAuthorityClasses, model.registrationAuthorityClass[ 'id' ] ) );
        }
        if ( model.placementClass && model.placementClass.id ) {
          model.placementClass = $filter( 'json' )( helperFunctionsService.findByIdInsideArray( metaData.placementClasses, model.placementClass[ 'id' ] ) );
        }
        if ( model.currency && model.currency.id ) {
          model.currency = $filter( 'json' )( helperFunctionsService.findByIdInsideArray( metaData.currencies, model.currency[ 'id' ] ) );
        }
        if ( model.registrationDate && !model.registrationDateObj ) {
          model.registrationDateObj = helperFunctionsService.parseDate( model.registrationDate );
          model.registrationDate = helperFunctionsService.generateDate( model.registrationDateObj );
        }
        if ( model.valueDate && !model.valueDateObj ) {
          model.valueDateObj = helperFunctionsService.parseDate( model.valueDate );
          model.valueDate = helperFunctionsService.generateDate( model.valueDateObj );
        }
        if ( model.maturityDate && !model.maturityDateObj ) {
          model.maturityDateObj = helperFunctionsService.parseDate( model.maturityDate );
          model.maturityDate = helperFunctionsService.generateDate( model.maturityDateObj );
        }
        if ( model.finishDate && !model.finishDateObj ) {
          model.finishDateObj = helperFunctionsService.parseDate( model.finishDate );
          model.finishDate = helperFunctionsService.generateDate( model.finishDateObj );
        }
        if ( model.issue && model.issue.ISIN ) {
          model.instrument = model.issue;
          manageInstrumentService.normalizeReturnedTaskData( model.instrument, metaData );
          // model.finishDateObj = helperFunctionsService.parseDate(model.finishDate);
          // model.finishDate = helperFunctionsService.generateDate(model.finishDateObj);
        }

      };
      return {
        getMetaData: getMetaData,
        prepareFormData: prepareFormData,
        getIssuerEquities: getIssuerEquities,
        getIssueQuantities: getIssueQuantities,
        prepareFormDataForSave: prepareFormDataForSave,
        normalizeReturnedTask: normalizeReturnedTask

      };


    }
  ] );
