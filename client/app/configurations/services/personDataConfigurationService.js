'use strict';

angular.module('cmisApp')
  .factory('personDataConfigurationService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', "$http",
    function (referenceDataService, $q, Loader, helperFunctionsService, $http) {


      var configurationDataUrl = '/api/configurations/';

      var getPersonConfigurationData = function (subjectContextCode, objectRoleCode, operationTypeCode) {


        return $http.get(configurationDataUrl + "getPersonConfigurationData/" +
          subjectContextCode + "/" + objectRoleCode + "/" + operationTypeCode)
          .then(function (result) {

          return result.data;
        })

      };

      //Return meta data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};


        var getSubjectContextClasses = function (value) {
          metaData.subjectContextClasses = value;
        };
        var getObjectRoleClasses = function (value) {
          metaData.objectRoleClasses = value;
        };
        var getOperationTypeClasses = function (value) {
          metaData.operationTypeClasses = value;
        };

        $q.all([

          referenceDataService.getSubjectContextClasses().then(getSubjectContextClasses),
          referenceDataService.getObjectRoleClasses().then(getObjectRoleClasses),
          referenceDataService.getOperationTypeClasses().then(getOperationTypeClasses)

        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };

      return {
        getMetaData: getMetaData,
        getPersonConfigurationData: getPersonConfigurationData,
      };


    }]);
