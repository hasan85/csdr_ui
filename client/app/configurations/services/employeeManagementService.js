'use strict';

angular.module('cmisApp')
  .factory('employeeManagementService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', "$http",
    function (referenceDataService, $q, Loader, helperFunctionsService, $http) {


      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};

        var getPhoneNumberTypes = function (value) {
          metaData.phoneNumberTypes = value;
        };

        var getCountries = function (value) {
          metaData.countries = value;
        };

        var getCurrencies = function (value) {
          metaData.currencies = value;
        };

        var businessClasses = function (value) {
          metaData.businessClasses = value;
        };
        var legalFormClasses = function (value) {
          metaData.legalFormClasses = value;
        };

        var getNaturalPersonIdDocumentTypes = function (value) {
          metaData.naturalPersonIdDocumentTypes = value;
        };
        var getSignerPositions = function (value) {
          metaData.signerPositions = value;
        };
        var getPersonClasses = function (value) {
          metaData.personClasses = value;
        };

        $q.all([
          referenceDataService.getPhoneNumberTypes().then(getPhoneNumberTypes),
          referenceDataService.getCountries().then(getCountries),
          referenceDataService.getCurrencies().then(getCurrencies),
          referenceDataService.getBusinessClasses().then(businessClasses),
          referenceDataService.getLegalFormClasses().then(legalFormClasses),
          referenceDataService.getNaturalPersonIdDocumentTypes().then(getNaturalPersonIdDocumentTypes),
          referenceDataService.getSignerPositions().then(getSignerPositions),
          referenceDataService.getPersonClasses().then(getPersonClasses)
        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };

      return {
        getMetaData: getMetaData,
      };


    }]);
