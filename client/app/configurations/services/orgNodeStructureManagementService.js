'use strict';

angular.module('cmisApp')
  .factory('orgNodeStructureManagementService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', "$http",
    function (referenceDataService, $q, Loader, helperFunctionsService, $http) {


      var configurationDataUrl = '/api/configurations/';


        var getOrganizationStructure = function() {
            return $http.get(configurationDataUrl + "getOrganizationStructure").then(function (result) {
                return result.data;
            });
        };

        var getAuthorizationByOrgNodeId = function (id) {
            return $http.get(configurationDataUrl + "getAuthorizationByOrgNodeId" + "/" + id).then(function (result) {
                return result.data;
            });
        };

      var getPersonConfigurationData = function (subjectContextCode, objectRoleCode) {


        return $http.get(configurationDataUrl + "getPersonConfigurationData/" + subjectContextCode + "/" + objectRoleCode).then(function (result) {

          return result.data;
        })

      };
      var expandAllNodesOnInit = function (data) {

        data.expanded = true;
        for (var child in data.items) {
          expandAllNodesOnInit(data.items[child]);
        }
      };
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};


        var getPositionClasses = function (value) {
          metaData.positionClasses = value;
        };
        var getOrgNodeClasses = function (value) {
          metaData.orgNodeClasses = value;
        };

        $q.all([

          referenceDataService.getSignerPositions().then(getPositionClasses),
          referenceDataService.getOrgNodeClasses().then(getOrgNodeClasses)

        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };
      var getDataItemsByNode = function (node, tree) {
        return {
          source: node.sourceNode ? tree.dataItem(node.sourceNode) : null,
          destination: node.destinationNode ? tree.dataItem(node.destinationNode) : null
        }
      };
      var findByIdInsideTree = function (node, id) {
        if (node.ID == id) {
          return node;
        }
        for (var item in node.items) {
          findByIdInsideTree(node.items[item], node.items[item].ID);
        }
      };
      var checkIfNodeIsInItems = function (parent, uid) {

        if (!parent.items || parent.items.length < 1) {
          return false;
        }
        for (var key in parent.items) {

          if (parent.items[key].ID === uid) {
            return true;
          } else {
            return false;
          }

        }
      };

      return {
          getOrganizationStructure: getOrganizationStructure,
          getAuthorizationByOrgNodeId: getAuthorizationByOrgNodeId,
        getMetaData: getMetaData,
        getPersonConfigurationData: getPersonConfigurationData,
        expandAllNodesOnInit: expandAllNodesOnInit,
        getDataItemsByNode: getDataItemsByNode,
        findByIdInsideTree: findByIdInsideTree,
        checkIfNodeIsInItems: checkIfNodeIsInItems
      };
    }]);
