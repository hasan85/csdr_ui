'use strict';

angular.module('cmisApp').controller('ViewAuthorizationsController', ['$scope', '$windowInstance', 'id', "orgNodeStructureManagementService", "helperFunctionsService", 'gettextCatalog', "Loader", "$http", function ($scope, $windowInstance, id, orgNodeStructureManagementService, helperFunctionsService, gettextCatalog, Loader, $http) {

  $scope.config = {
    screenId: id,
    window: $windowInstance,
    tree: {},
    mainGrid: {},
    buttons: {
    },
    labels: [
      gettextCatalog.getString("Operations"),
      gettextCatalog.getString("Views"),
      gettextCatalog.getString("Dashboards"),
      gettextCatalog.getString("Controlled Processes")
    ],
    labelsByIndex: {
      views: gettextCatalog.getString("Views"),
      operations: gettextCatalog.getString("Operations"),
      dashboards: gettextCatalog.getString("Dashboards"),
      controlledProcesses: gettextCatalog.getString("Controlled Processes")
    }
  };


    $scope.orgNodeItem = null;

    orgNodeStructureManagementService.getOrganizationStructure().then(function(data) {

        var treeData = [data.data];

        var formatData = function(data) {
            data.map(function(item) {
                item.expanded = true;
                if(item.items) {
                    item.items = helperFunctionsService.convertObjectToArray(item.items);
                    formatData(item.items);
                }
            });
        };

        formatData(treeData);

        $scope.treeData = new kendo.data.HierarchicalDataSource({
            data: treeData
        });
    });

  $scope.exportAllAuthorizations = function () {
    Loader.show(true);
    $http({
      method: "GET",
      url: "/api/configurations/getAllAuthorizations"
    }).then(function (response) {
      Loader.show(false);
      if(response && response.data && response.data.data) {

        console.log(response.data);

        $scope.exportDataToExcel(response.data.data, undefined, true);
      }
    });
  };

  $scope.exportDataToExcel = function(data, key, all) {

    var VMOrgNodeAuthorization = angular.copy(data);

    if(angular.isArray(VMOrgNodeAuthorization) && key) {
      data = {};
      data[key] = VMOrgNodeAuthorization;
    }

    var sheets = [];

    angular.forEach(data, function(item, key) {

      var sheet = {
        // Column settings (width)
        columns: [
          { autoWidth: true },
          { autoWidth: true },
          { autoWidth: true },
          { autoWidth: true }
        ],
        // Title of the sheet
        title: $scope.config.labelsByIndex[key],
        // Rows of the sheet
        rows: []
      };

      if(angular.isArray(item)) {

        var rows = [
          {
            cells: []
          }
        ];

        if(all == true) {
          rows[0].cells.push({value: "Təşkilati strukturun elementi"});
        }
        rows[0].cells.push({value: "Ad"});
        if(key != 'dashboards') {
          rows[0].cells.push({value: "Pəncərə nömrəsi"});
        }
        rows[0].cells.push({value: "Fərdi avtorizasiya"});

        item.map(function(item) {

          var cells = [];
          
          if(all == true) {
            cells.push({value: item.orgNodeName});
          }
          
          cells.push({value: item.name});

          if(key != 'dashboards') {
            cells.push({value: item.screenID});
          }
          cells.push({value: item.personalAuth == "true" ? "Bəli": "Xeyr"});

          rows.push({
            cells: cells
          });

        });

        sheet.rows = rows;

        sheets.push(sheet);

      }

    });


    var workbook = new kendo.ooxml.Workbook({
      sheets: sheets
    });
    kendo.saveAs({
      dataURI: workbook.toDataURL(),
      fileName: "authorizations.xlsx"
    });

  };

    $scope.viewOrgNode = function(orgNodeItem, window) {
        orgNodeStructureManagementService.getAuthorizationByOrgNodeId(orgNodeItem.ID).then(function(data) {
            $scope.orgNodeItem = {
                operations: helperFunctionsService.convertObjectToArray(data.data.operations),
                views: helperFunctionsService.convertObjectToArray(data.data.views),
                dashboards: helperFunctionsService.convertObjectToArray(data.data.dashboards),
                controlledProcesses: helperFunctionsService.convertObjectToArray(data.data.controlledProcesses)
            };
            window.open();
            window.center();
        });
    };

}]);
