'use strict';

angular.module('cmisApp')
  .controller('OrgNodeStructureManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'orgNodeStructureManagementService', 'operationState', 'task', 'gettextCatalog', 'SweetAlert', '$q', '$filter', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              orgNodeStructureManagementService, operationState, task, gettextCatalog, SweetAlert, $q, $filter, helperFunctionsService) {

      var orgNodeClassConstants = {
        context: 'ORGNODE_CONTEXT',
        unit: 'ORGNODE_UNIT',
        group: 'ORGNODE_GROUP',
        position: 'ORGNODE_POSITION'
      };

      var deferred = $q.defer();

      $scope.orgNodeWindowInstance = {};

      $scope.orgNodeStructureData = angular.fromJson(task.draft);

      $scope.metaData = {};

      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "orgNodeStructureDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(angular.copy($scope.treeInstance.dataSource.view()[0]));
              }
            }
          }
        },
        labels: {
          orgNode: gettextCatalog.getString('Organizational Node')
        },
        orgNodeFormName: "orgNodeAddForm"

      };

      $scope.buffers = {
        orgNode: {
          form: {
            orgNodeClass: null
          },
          metaData: {},
          isPosition: false
        }
      };

      var setContextChildClasses = function () {

        $scope.buffers.orgNode.metaData.orgNodeClasses = {};
        $scope.buffers.orgNode.metaData.orgNodeClasses[orgNodeClassConstants.group] =
          $scope.metaData.orgNodeClasses[orgNodeClassConstants.group];
        $scope.buffers.orgNode.metaData.orgNodeClasses[orgNodeClassConstants.unit] =
          $scope.metaData.orgNodeClasses[orgNodeClassConstants.unit];
        $scope.buffers.orgNode.metaData.orgNodeClasses[orgNodeClassConstants.position] =
          $scope.metaData.orgNodeClasses[orgNodeClassConstants.position];
      };
      var setUnitChildClasses = function () {
        $scope.buffers.orgNode.metaData.orgNodeClasses = {};
        $scope.buffers.orgNode.metaData.orgNodeClasses[orgNodeClassConstants.unit] =
          $scope.metaData.orgNodeClasses[orgNodeClassConstants.unit];
        $scope.buffers.orgNode.metaData.orgNodeClasses[orgNodeClassConstants.position] =
          $scope.metaData.orgNodeClasses[orgNodeClassConstants.position];
      };
      var setGroupChildClasses = function () {
        $scope.buffers.orgNode.metaData.orgNodeClasses = {};
        $scope.buffers.orgNode.metaData.orgNodeClasses[orgNodeClassConstants.position] =
          $scope.metaData.orgNodeClasses[orgNodeClassConstants.position];
      };
      var resolveOrgNodeBufferClasses = function (classCode, operation) {

        if (classCode == orgNodeClassConstants.context) {
          setContextChildClasses(operation);
        } else if (classCode == orgNodeClassConstants.unit) {
          setUnitChildClasses(operation);

        } else if (classCode == orgNodeClassConstants.group) {
          setGroupChildClasses(operation);
        }

      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        //var taskPayload = task && task.draft ? task.draft : null;
        //var currentTask = angular.toJson(prepareFormData($scope.orgNodeStructureData));
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.treeInstance.dataSource.view()[0]));
      });

      orgNodeStructureManagementService.getMetaData().then(function (data) {
        var orgNodeClasses = helperFunctionsService.convertArrayToObjectByKey(data.orgNodeClasses, 'code');
        var positionClasses = helperFunctionsService.convertArrayToObjectByKey(data.positionClasses, 'code');
        $scope.metaData = {
          orgNodeClasses: orgNodeClasses,
          positionClasses: positionClasses,
          orgNodeClassConstants: orgNodeClassConstants
        };
      });

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.tree) {
          $scope.treeInstance = widget;
          $scope.treeInstance.expand(".k-item");
        }
      });

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.orgNodeAddWindow) {
          $scope.orgNodeWindowInstance = widget;
        }
      });

      $scope.treeInstance = null;

      orgNodeStructureManagementService.expandAllNodesOnInit($scope.orgNodeStructureData);

      $scope.treeData = new kendo.data.HierarchicalDataSource({
        data: [$scope.orgNodeStructureData]
      });

      var showOrgNodeAddWindow = function () {
        $scope.orgNodeWindowInstance.open();
        $scope.orgNodeWindowInstance.center();
        return deferred.promise;
      };

      // Add new child to the tree
      $scope.addNewChild = function (item) {

        resolveOrgNodeBufferClasses(item.orgNodeClass.code, 'insert');
        var selectedItem = $scope.treeInstance.findByUid(item.uid);
        $scope.treeInstance.select(selectedItem);
        showOrgNodeAddWindow().then(function (newData) {
          $scope.treeInstance.append(angular.copy(newData), $scope.treeInstance.select());
          $scope.orgNodeWindowInstance.close();
          $scope.resetOrgNodeBuffer();
        });
      };

      $scope.showOrgNodeUpdateForm = function (dataItem) {

        if (dataItem.orgNodeClass.code == orgNodeClassConstants.position) {
          $scope.buffers.orgNode.isPosition = true;
        }
        else {
          $scope.buffers.orgNode.isPosition = false;
        }
        $scope.orgNodeWindowInstance.title(gettextCatalog.getString('Update Organizational Node'));

        $scope.buffers.orgNode.isUpdate = true;

        $scope.buffers.orgNode.form = angular.copy(dataItem);

        if (dataItem.orgNodeClass.code == orgNodeClassConstants.position) {
          for (var pc in $scope.metaData.positionClasses) {
            if ($scope.metaData.positionClasses[pc].nameAz == dataItem.name.nameAz) {
              $scope.buffers.orgNode.form.positionClass = $filter('json')($scope.metaData.positionClasses[pc]);
            }
          }
        }

        showOrgNodeAddWindow().then(function (data) {

          if (data.orgNodeClass.code == orgNodeClassConstants.position) {
            data.positionClass = angular.fromJson(data.positionClass);
            var positionNames = {
              nameAz: data.positionClass.nameAz,
              nameEn: data.positionClass.nameEn
            };
            data.name = positionNames;
            data.abbreviation = positionNames;

          }
          dataItem = angular.extend(dataItem, angular.copy(data));
          $scope.orgNodeWindowInstance.close();
          $scope.resetOrgNodeBuffer();
        });

      };

      //Add new Org Node to the tree
      $scope.addNewOrgNode = function () {

        var newOrgNode = angular.copy($scope.buffers.orgNode.form);

        newOrgNode.orgNodeClass = angular.fromJson(newOrgNode.orgNodeClass);
        newOrgNode.positionClass = angular.fromJson(newOrgNode.positionClass);
        if (newOrgNode.orgNodeClass.code == orgNodeClassConstants.position) {
          var positionNames = {
            nameAz: newOrgNode.positionClass.nameAz,
            nameEn: newOrgNode.positionClass.nameEn
          };
          newOrgNode.name = positionNames;
          newOrgNode.abbreviation = positionNames;

        }
        deferred.resolve(newOrgNode);
        deferred = $q.defer();
      };

      // Update Org Node details
      $scope.updateOrgNode = function () {
        $scope.config.orgNodeFormName.$submitted = true;
        deferred.resolve($scope.buffers.orgNode.form);
        deferred = $q.defer();
      };

      // Remove Org Node from tree
      $scope.remove = function (item) {
        var array = item.parent();
        var index = array.indexOf(item);
        array.splice(index, 1);
      };

      // Resetting all buffer variables (unbinding)
      $scope.resetOrgNodeBuffer = function () {

        $scope.buffers.orgNode.isUpdate = false;
        $scope.buffers.orgNode.isPosition = false;

        // $scope.buffers.orgNode.form.positionClass = null;
        $scope.buffers.orgNode.form = {};
        if ($scope.config.orgNodeFormName) {
          $scope.config.orgNodeFormName.$setPristine();
          $scope.config.orgNodeFormName.$setUntouched();
        }
        $scope.orgNodeWindowInstance.title(gettextCatalog.getString('Add New Organizational Node'));
        deferred = $q.defer();
      };

      // For detecting if OrgNode class  whether is position. If it is show Position Class selection.
      $scope.orgNodeClassChange = function (orgNodeClass) {
        var orgNodeClass = angular.fromJson(orgNodeClass);
        if (orgNodeClass) {
          if (orgNodeClass.code == orgNodeClassConstants.position) {
            $scope.buffers.orgNode.isPosition = true;
          } else {
            $scope.buffers.orgNode.isPosition = false;
          }
        }
      };

      $scope.nodeDragStart = function (node) {
        var dataItems = orgNodeStructureManagementService.getDataItemsByNode(node, $scope.treeInstance);

        if (dataItems.source.isSystem) {
          node.preventDefault();
        }
      };
    }
  ])
;
