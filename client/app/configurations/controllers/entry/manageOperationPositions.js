'use strict';

angular.module('cmisApp')
  .controller('ManageOperationPositionsController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', 'reportService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, reportService) {

      var instrumentFormPartialTemplate = {
        id: null,
        ISIN: null,
        issuerName: null,
        instrumentName: null
      };
      var accountFormPartialTemplate = {
        id: null,
        accountId: null,
        name: null,
        accountNumber: null
      };
      var prepareFormData = function (formData) {
        return formData;
      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        return draft;
      };
      $scope.manageOperationPositionData = initializeFormData(angular.fromJson(task.draft));
      reportService.normalizeReturnedReportTask($scope.manageOperationPositionData);
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "manageOperationPositionDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {

                $scope.config.completeTask(prepareFormData(angular.copy($scope.manageOperationPositionData)));

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.manageOperationPositionData);
      });



    }]);
