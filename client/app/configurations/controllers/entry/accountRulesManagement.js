'use strict';

angular.module('cmisApp')
  .controller('AccountRulesManagementEntryController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'SweetAlert', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, SweetAlert, helperFunctionsService) {

      var initializeViewModel = function () {
        var draft = angular.fromJson(task.draft);
        if (draft.accountNumberingParameters && draft.accountNumberingParameters.length > 0) {
          for (var i = 0; i < draft.accountNumberingParameters.length; i++) {
            draft.accountNumberingParameters[i].numberLength = Array(draft.accountNumberingParameters[i].numberLength + 1).join("X");
          }
        }
        return draft;
      };

      $scope.accountRulesManagementData = initializeViewModel();


      var prepareFormData = function (formData) {

        if (formData.accountNumberingParameters && formData.accountNumberingParameters.length > 0) {
          for (var i = 0; i < formData.accountNumberingParameters.length; i++) {
            formData.accountNumberingParameters[i].numberLength = formData.accountNumberingParameters[i].numberLength.length;
          }
        }
        return formData;
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "accountRulesManagementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;

              var formValid = true;
              if ($scope.accountRulesManagementData.isCollectiveAccountEnabled) {

                if (!($scope.accountRulesManagementData.isJointAccountEnabled
                  || $scope.accountRulesManagementData.isCoOwnerAccountEnabled)) {
                  formValid = false;
                  SweetAlert.swal('', gettextCatalog.getString('You enabled Collective Account.\n ' +
                    'You have to enable either Joint Account or Coowner Account also.'), 'error');
                }
              }
              if ($scope.config.form.name.$valid && formValid) {

                var formBuf = prepareFormData(angular.copy($scope.accountRulesManagementData));
                $scope.config.completeTask(formBuf);
              }else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        labels: {
          accounts: gettextCatalog.getString('Accounts'),
          accountNumberingRules: gettextCatalog.getString('Account Numbering Rules')
        }
      };

      $scope.accountNumberChange = function (model) {
        model.numberLength = model.numberLength.toString();
        for (var i = 0; i < model.numberLength.length; i++) {
          model.numberLength = helperFunctionsService.replaceAt(model.numberLength.toString(), i, 'X');
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(prepareFormData(angular.copy($scope.accountRulesManagementData)));
      });

      // If task saved as draft get saved data
      //if (operationState == appConstants.operationStates.active) {
      //  if (task.draft) {
      //    $scope.accountRulesManagementData = angular.fromJson(task.draft);
      //  }
      //}

    }]);
