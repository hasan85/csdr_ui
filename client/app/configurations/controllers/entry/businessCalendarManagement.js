'use strict';

angular.module('cmisApp')
  .controller('BusinessCalendarManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'helperFunctionsService','$filter','SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, helperFunctionsService,$filter,SweetAlert) {

      var initializeDraft = function () {
        var draft = angular.fromJson(task.draft);
        draft.weeklyWorkingDays = helperFunctionsService.convertObjectToArray(draft.weeklyWorkingDays);
        draft.nonWorkingDays = helperFunctionsService.convertObjectToArray(draft.nonWorkingDays);
        draft.exceptionalNonWorkingDates = helperFunctionsService.convertObjectToArray(draft.exceptionalNonWorkingDates);
        draft.exceptionalWorkingDates = helperFunctionsService.convertObjectToArray(draft.exceptionalWorkingDates);

        for (var i = 0; i < draft.exceptionalNonWorkingDates.length; i++) {
          draft.exceptionalNonWorkingDates[i].date = kendo.parseDate(draft.exceptionalNonWorkingDates[i]['date'],'dd-MM-yyyy HH:mm:ss');
        }
        for (var i = 0; i < draft.exceptionalWorkingDates.length; i++) {
          draft.exceptionalWorkingDates[i].date =  kendo.parseDate(draft.exceptionalWorkingDates[i]['date'],'dd-MM-yyyy HH:mm:ss');
        }

        return draft;
      };
      $scope.businessCalendarManagementData = initializeDraft();

      var prepareFormData = function (formData) {

        for (var i = 0; i < formData.exceptionalNonWorkingDates.length; i++) {
          formData.exceptionalNonWorkingDates[i].date = helperFunctionsService.generateDateTime(formData.exceptionalNonWorkingDates[i].date);
        }
        for (var i = 0; i < formData.exceptionalWorkingDates.length; i++) {
          formData.exceptionalWorkingDates[i].date = helperFunctionsService.generateDateTime(formData.exceptionalWorkingDates[i].date);
        }
        return formData;
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "businessCalendarManagementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formBuf = prepareFormData(angular.copy($scope.businessCalendarManagementData));
                $scope.config.completeTask(formBuf);
              }else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        labels: {
          weeklyWorkingDays: gettextCatalog.getString('Working Days Of Week'),
          nonWorkingDays: gettextCatalog.getString('Yearly Routine Non Working Days'),
          exceptionalNonWorkingDates: gettextCatalog.getString('Yearly Exceptional Non Working Days'),
          exceptionalWorkingDates: gettextCatalog.getString('Yearly Exceptional Working Days')
        }

      };

      $scope.YRNWDSelected = function (date) {
        if ($scope.businessCalendarManagementData.nonWorkingDays.indexOf(date) < 0) {
          $scope.businessCalendarManagementData.nonWorkingDays.push(date);
        }
        $scope.config.YRNWDDateStr = null;
        $scope.config.YRNWDDateObj = null;
      };

      $scope.YENWDSelected = function (date) {
        if ($scope.businessCalendarManagementData.exceptionalNonWorkingDates.indexOf(date) < 0) {
          $scope.businessCalendarManagementData.exceptionalNonWorkingDates.push({date: date});
        }
        $scope.config.YENWDDateStr = null;
        $scope.config.YENWDDateObj = null;
      };

      $scope.YEWDSelected = function (date) {
        if ($scope.businessCalendarManagementData.exceptionalWorkingDates.indexOf(date) < 0) {
          $scope.businessCalendarManagementData.exceptionalWorkingDates.push({date: date});
        }
        $scope.config.YEWDDateStr = null;
        $scope.config.YEWDDateObj = null;
      };

      $scope.removeElementFromArray = function (model, index) {
        model.splice(index, 1);
      };
      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(prepareFormData(angular.copy($scope.businessCalendarManagementData)));
      });

      // If task saved as draft get saved data
      //if (operationState == appConstants.operationStates.active) {
      //  if (task.draft) {
      //    $scope.businessCalendarManagementData = angular.fromJson(task.draft);
      //  }
      //}

    }]);
