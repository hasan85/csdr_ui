'use strict';

angular.module('cmisApp')
  .controller('OrderWhitelistManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'instrumentFindService', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, instrumentFindService, helperFunctionsService) {

      $scope.whitelistItemsData = angular.fromJson(task.draft);

        $scope.whitelistItemsData.vmOrderWhiteLists.map(function(item) {
            if (item.startDateObj) {
                item.startDate = helperFunctionsService.generateDateTime(item.startDateObj);
                delete item.startDateObj;
            }
            if (item.finishDateObj) {
                item.finishDate = helperFunctionsService.generateDateTime(item.finishDateObj);
                delete item.finishDateObj;
            } else {
                item.finishDate = item.startDate;
            }
        });

      $scope.metaData = {
        sides: ['BUY', 'SELL'],
        markets: ['STK', 'BND', 'PRM', 'RPO', 'DRV']
      };
      var prepareFormData = function (formData) {
        formData.vmOrderWhiteLists.forEach(function (item) {
          if (item.startDateObj) {
            item.startDate = helperFunctionsService.generateDateTime(item.startDateObj);
            delete item.startDateObj;
          }
          if (item.finishDateObj) {
            item.finishDate = helperFunctionsService.generateDateTime(item.finishDateObj);
            delete item.finishDateObj;
          } else {
            item.finishDate = item.startDate;
          }

        });
        return formData;
      };
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "employeesDataForm",
          data: {},
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy(
                  $scope.whitelistItemsData
                )));
              }
            }
          }
        },
        labels: {
          whitelistItemManageWindowTitle: gettextCatalog.getString('Add New Item')
        }
      };
      $scope.buffers = {
        whitelistItem: {
          currentIndex: -1,
          form: {},
          config: {
            form: {
              name: "whitelistItemManageForm"
            }
          },
          isNewWhitelistItem: true,
          isReadOnlyView: false
        }
      };

      $scope.showWhitelistItemCreationForm = function (index) {
        $scope.buffers.whitelistItem.whitelistItemManageWindow.title(gettextCatalog.getString('Add New Item'));
        $scope.config.labels.whitelistItemManageWindowTitle = gettextCatalog.getString('Add New Item');
        $scope.buffers.whitelistItem.currentIndex = index;
        $scope.buffers.whitelistItem.isNewWhitelistItem = true;
        $scope.buffers.whitelistItem.whitelistItemManageWindow.open();
        $scope.buffers.whitelistItem.whitelistItemManageWindow.center();
      };
      $scope.addWhitelistItem = function () {

        $scope.buffers.whitelistItem.config.form.name.$submitted = true;
        if ($scope.buffers.whitelistItem.config.form.name.$valid) {
          if (!$scope.whitelistItemsData.vmOrderWhiteLists) {
            $scope.whitelistItemsData.vmOrderWhiteLists = [];
          }
          $scope.whitelistItemsData.vmOrderWhiteLists.push($scope.buffers.whitelistItem.form);

          $scope.resetWhitelistItemBuffer();
          $scope.buffers.whitelistItem.whitelistItemManageWindow.close();
        }

      };
      $scope.showWhitelistItemUpdateForm = function (index) {
        $scope.buffers.whitelistItem.isNewWhitelistItem = false;
        $scope.buffers.whitelistItem.whitelistItemManageWindow.title(gettextCatalog.getString('Update Item'));
        $scope.config.labels.whitelistItemManageWindowTitle = gettextCatalog.getString('Update Item');
        $scope.buffers.whitelistItem.form = $scope.whitelistItemsData.vmOrderWhiteLists[index];
        $scope.buffers.whitelistItem.currentIndex = index;
        $scope.buffers.whitelistItem.whitelistItemManageWindow.open();
        $scope.buffers.whitelistItem.whitelistItemManageWindow.center();
      };
      $scope.showReadOnlyView = function (index) {
        $scope.buffers.whitelistItem.isReadOnlyView = true;
        $scope.buffers.whitelistItem.form = $scope.whitelistItemsData.vmOrderWhiteLists[index];
        $scope.buffers.whitelistItem.whitelistItemManageWindow.title(gettextCatalog.getString('View Item'));
        $scope.config.labels.whitelistItemManageWindowTitle = gettextCatalog.getString('View Item');
        $scope.buffers.whitelistItem.whitelistItemManageWindow.open();
        $scope.buffers.whitelistItem.whitelistItemManageWindow.center();
      };
      $scope.updateWhitelistItem = function () {
        $scope.buffers.whitelistItem.config.form.name.$submitted = true;
        if ($scope.buffers.whitelistItem.config.form.name.$valid) {
          $scope.whitelistItemsData.vmOrderWhiteLists[$scope.buffers.whitelistItem.currentIndex] = $scope.buffers.whitelistItem.form;
          $scope.buffers.whitelistItem.whitelistItemManageWindow.close();
          $scope.resetWhitelistItemBuffer();
        }
      };
      $scope.removeWhitelistItem = function (index) {
        $scope.whitelistItemsData.vmOrderWhiteLists.splice(index, 1)

      };
      $scope.resetWhitelistItemBuffer = function () {
        $scope.buffers.whitelistItem.whitelistItemManageWindow.title(gettextCatalog.getString('Add New Item'));
        $scope.config.labels.whitelistItemManageWindowTitle = gettextCatalog.getString('Add New Item');
        $scope.buffers.whitelistItem.currentIndex = -1;
        $scope.buffers.whitelistItem.form = {};
        $scope.buffers.whitelistItem.config.form.name.$setPristine();
        $scope.buffers.whitelistItem.config.form.name.$setUntouched();
        $scope.buffers.whitelistItem.isNewWhitelistItem = true;
        $scope.buffers.whitelistItem.isReadOnlyView = false;
      };

      // Search buyer broker
      $scope.findBroker = function (model) {
        personFindService.findTradingMember().then(function (data) {
          model.client = {};
          if (!model.broker) {
            model.broker = {};
          }
          model.broker.name = data.name;
          model.broker.accountId = data.accountId;
          model.broker.id = data.id;
          model.broker.accountNumber = data.accountNumber;
        });
      };

      // Search buyer broker
      $scope.findClient = function (model) {
        var brokerId = $scope.buffers.whitelistItem.form.broker.id;
        personFindService.findClientAccount(brokerId,'/api/clearing/getTradingMemberAccountById').then(function (data) {
          if (!model.client) {
            model.client = {};
          }
          model.client.name = data.name;
          model.client.accountId = data.accountId;
          model.client.id = data.id;
          model.client.accountNumber = data.accountNumber;
          model.client.depoAccountId = data.depoAccountId;
        });
      };
      // Search instrument
      $scope.findInstrument = function (model) {
        instrumentFindService.findInstrument().then(function (data) {
          if (!model.instrument) {
            model.instrument = {};
          }
          model.instrument.ISIN = data.isin;
          model.instrument.id = data.id;
          model.instrument.issuerName = data.issuerName;
          model.instrument.instrumentName = data.instrumentName;
        });
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.whitelistItemsData);
      });

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.whitelistItem.whitelistItemManageWindow) {
          $scope.buffers.whitelistItem.whitelistItemManageWindow = widget;
        }
      });

    }]);
