'use strict';

angular.module('cmisApp')
  .controller('AuthorizationManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'orgNodeStructureManagementService', '$rootScope',
    'referenceDataService', 'helperFunctionsService', 'Loader', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, orgNodeStructureManagementService, $rootScope,
              referenceDataService, helperFunctionsService, Loader, SweetAlert) {

      var taskTypeConstants = {
        entry: 'ENTRY_TASK_CLASS',
        approvalTask: 'APPROVAL_TASK_CLASS',
        printTask: "PRINT_TASK_CLASS"
      };
      var validationErrors = {};

      var validateForm = function (formData) {


        var isValid = true;
        for (var i = 0; i < formData.processAuthorizations.length; i++) {
          var workflows = formData.processAuthorizations[i].groupAuthorizations;
          formData.processAuthorizations[i].hasError = false;
          for (var j = 0; j < workflows.length; j++) {
            var workflow = workflows[j];
            workflow.hasError = false;
            workflow.hasNoProcessStarter = false;
            if (workflow && workflow['groupKey']) {

              for (var k = 0; k < workflow.taskAuthorizations.length; k++) {
                var taskAuthorization = workflow.taskAuthorizations[k];
                taskAuthorization.hasError = false;
                if (taskAuthorization.taskType == taskTypeConstants.entry ||
                  taskAuthorization.taskType == taskTypeConstants.printTask ||
                  taskAuthorization.taskType == taskTypeConstants.approvalTask) {

                  if (!(taskAuthorization.taskOperators && taskAuthorization.taskOperators.length > 0 )) {
                    if (taskAuthorization.taskType == taskTypeConstants.approvalTask && !workflow.isApproveEnabled) {
                      continue;
                    }
                    formData.processAuthorizations[i].hasError = true;
                    taskAuthorization.hasError = true;
                    workflow.hasError = true;
                    isValid = false;
                  }
                  // console.log(taskAuthorization.taskKey);
                }
              }

            }
          }
        }
        if (!isValid) {
          SweetAlert.swal('', gettextCatalog.getString('There are validation error on process configurations.' +
            ' Please check all of the process and make sure that you hav filled all of the required fields.' +
            ' Processes with errors will be highlighted with red color.'), 'error');
        }
        return isValid;
      };
      // Loader.show(true);
      $scope.metaData = {};

      referenceDataService.getTaskTypes().then(function (data) {
        $scope.metaData = {
          taskTypes: helperFunctionsService.convertArrayToObjectByKey(data, 'code')
        };

      });

      var draft = angular.fromJson(task.draft);

      $scope.authorizationManagementData = draft;

      orgNodeStructureManagementService.expandAllNodesOnInit($scope.authorizationManagementData.contextOrgNode);

      $scope.orgNodeStructureTree = new kendo.data.HierarchicalDataSource({
        data: [$scope.authorizationManagementData.contextOrgNode]
      });

      $scope.buffers = {
        processAuthorization: {
          data: {},
          currentIndex: -1
        },
        viewAuthorization: {
          data: {},
          currentIndex: -1
        },
        dashboardAuthorization: {
          data: {},
          currentIndex: -1
        },
        wfAuthorization: {
          data: {},
          currentIndex: -1
        },
        orgNodeSelection: {
          destination: null
        }
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "authorizationManagementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;

              if ($scope.config.form.name.$valid && validateForm($scope.authorizationManagementData)) {
                $scope.config.completeTask(angular.copy($scope.authorizationManagementData));
              }
            }
          }
        },
        labels: {
          processAuthorization: gettextCatalog.getString('Process Authorization'),
          viewConfiguration: gettextCatalog.getString('View Authorization'),
          dashboardConfiguration: gettextCatalog.getString('Dashboard Authorization'),
          editProcessAuthorization: gettextCatalog.getString('Edit Process Authorization'),
          addNewOrgNode: gettextCatalog.getString('Select Organizational Node'),
          overallOperationAuth: gettextCatalog.getString('Overall Operation Authorization'),
          workflowAuth: gettextCatalog.getString('Workflow Authorization'),
          generalConfiguration: gettextCatalog.getString('General Configuration'),
          controllers: gettextCatalog.getString('Controllers'),
          operators: gettextCatalog.getString('Operators'),
          workflow: gettextCatalog.getString('Workflow'),
          processControllers: gettextCatalog.getString('Process Controllers'),
          processStarters: gettextCatalog.getString('Process Starters')
        },
        orgNodeSelectionDestinations: {
          processController: 1
        }
      };

      // Process Authorization [[
      $scope.showProcessAuthorizationForm = function (index) {
        $scope.buffers.processAuthorization.data = $scope.authorizationManagementData.processAuthorizations[index];
        $scope.buffers.processAuthorization.currentIndex = index;
        $scope.buffers.processAuthorization.window.title(
          $scope.buffers.processAuthorization.data.name['name' + $rootScope.lnC] + " [ " +
          $scope.buffers.processAuthorization.data.processKey + " ]");
        $scope.buffers.processAuthorization.window.open();
        $scope.buffers.processAuthorization.window.center();
      };
      $scope.resetProcessAuthorizationBuffer = function () {
        $scope.buffers.processAuthorization.currentIndex = -1;
        $scope.buffers.processAuthorization.window.title(null);
        $scope.buffers.processAuthorization.data = {};
      };
      $scope.removeProcessController = function (index) {
        $scope.buffers.processAuthorization.data.processControllers.splice(index, 1)
      };
      $scope.removeTaskController = function (model, index) {
        model.splice(index, 1)
      };
      // Process Authorization ]]

      // View Authorization [[
      $scope.showViewAuthorizationForm = function (index) {
        $scope.buffers.viewAuthorization.data = $scope.authorizationManagementData.viewAuthorizations[index];
        $scope.buffers.viewAuthorization.currentIndex = index;
        $scope.buffers.viewAuthorization.window.title($scope.buffers.viewAuthorization.data.name['name' + $rootScope.lnC]);
        $scope.buffers.viewAuthorization.window.open();
        $scope.buffers.viewAuthorization.window.center();
      };
      $scope.resetViewAuthorizationBuffer = function () {
        $scope.buffers.viewAuthorization.currentIndex = -1;
        $scope.buffers.viewAuthorization.window.title(null);
        $scope.buffers.viewAuthorization.data = {};
      };
      $scope.removeViewController = function (index) {
        $scope.buffers.viewAuthorization.data.operators.splice(index, 1)
      };
      // View Authorization ]]

      // Dashboard Authorization [[
      $scope.showDashboardAuthorizationForm = function (index) {
        $scope.buffers.dashboardAuthorization.data = $scope.authorizationManagementData.dashboardAuthorizations[index];
        $scope.buffers.dashboardAuthorization.currentIndex = index;
        $scope.buffers.dashboardAuthorization.window.title($scope.buffers.dashboardAuthorization.data.name['name' + $rootScope.lnC]);
        $scope.buffers.dashboardAuthorization.window.open();
        $scope.buffers.dashboardAuthorization.window.center();
      };
      $scope.resetDashboardAuthorizationBuffer = function () {
        $scope.buffers.dashboardAuthorization.currentIndex = -1;
        $scope.buffers.dashboardAuthorization.window.title(null);
        $scope.buffers.dashboardAuthorization.data = {};
      };
      $scope.removeDashboardController = function (index) {
        $scope.buffers.dashboardAuthorization.data.operators.splice(index, 1)
      };
      // Dashboard Authorization ]]

      // WF Authorization [[
      $scope.showWfAuthorizationForm = function (index) {

        $scope.buffers.wfAuthorization.data = $scope.buffers.processAuthorization.data.groupAuthorizations[index];
        $scope.buffers.wfAuthorization.currentIndex = index;
        $scope.buffers.wfAuthorization.window.title($scope.buffers.wfAuthorization.data.groupKey);
        $scope.buffers.wfAuthorization.window.open();
        $scope.buffers.wfAuthorization.window.center();
      };
      $scope.resetWfAuthorizationBuffer = function () {
        $scope.buffers.wfAuthorization.currentIndex = -1;
        $scope.buffers.wfAuthorization.window.title(null);
        $scope.buffers.wfAuthorization.data = {};
      };
      $scope.deleteWfAuthorization = function (index) {
        $scope.buffers.processAuthorization.data.groupAuthorizations.splice(index, 1);
      };
      $scope.addNewWfAuthorization = function () {

        var wfGroupBuffer = {
          "groupKey": "Workflow1",
          "processControllers": [],
          "processStarters": [],
          "isApproveEnabled": null,
          "isVisaEnabled": null,
          "isDoubleEntryEnabled": null,
          "taskAuthorizations": angular.copy($scope.buffers.processAuthorization.data.taskAuthorizations)
        };

        var lastWfSequence = 1;

        if ($scope.buffers.processAuthorization.data.groupAuthorizations) {
          var wfGroupLength = $scope.buffers.processAuthorization.data.groupAuthorizations.length;
          if (wfGroupLength > 0) {
            var lastWfGroup = $scope.buffers.processAuthorization.data.groupAuthorizations[wfGroupLength - 1];
            var wfKeyArr = lastWfGroup.groupKey.split('-');
            lastWfSequence = parseInt(wfKeyArr[wfKeyArr.length - 1]) + 1;
          }
        }
        wfGroupBuffer.groupKey = "WF-" + $scope.buffers.processAuthorization.data.processKey + "-" + lastWfSequence;
        for (var i = 0; i < wfGroupBuffer.taskAuthorizations.length; i++) {
          wfGroupBuffer.taskAuthorizations[i].controllers = [];
          wfGroupBuffer.taskAuthorizations[i].operators = [];
        }
        $scope.buffers.processAuthorization.data.groupAuthorizations.push(wfGroupBuffer);

      };
      // WF Authorization ]]

      // OrgNode Selection [[
      $scope.showOrgNodeSelectionWindow = function (destination) {
        $scope.buffers.orgNodeSelection.window.open();
        $scope.buffers.orgNodeSelection.window.center();
        $scope.buffers.orgNodeSelection.destination = destination;
      };
      $scope.orgNodeSelected = function (dataItem) {
        $scope.buffers.orgNodeSelection.selectedDataItem = dataItem;
      };
      $scope.selectOrgNode = function () {
        // Push new node to Process Controllers List
        $scope.buffers.orgNodeSelection.destination.push($scope.buffers.orgNodeSelection.selectedDataItem);

        // Reset buffer and close window
        $scope.resetOrgNodeSelectionBuffer();
        $scope.buffers.orgNodeSelection.window.close();
      };
      $scope.resetOrgNodeSelectionBuffer = function () {
        $scope.buffers.orgNodeSelection.selectedDataItem = null;
        $scope.buffers.orgNodeSelection.destination = null;
        $scope.config.orgNodeStructureTree.select($());
      };
      // OrgNode Selection ]]

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        //  var taskPayload = task && task.draft ? task.draft : null;
        //  var currentTask = angular.toJson(prepareFormData($scope.authorizationManagementData));
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.authorizationManagementData);
      });



      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.processAuthorization.window) {
          $scope.buffers.processAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.viewAuthorization.window) {
          $scope.buffers.viewAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.dashboardAuthorization.window) {
          $scope.buffers.dashboardAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.wfAuthorization.window) {
          $scope.buffers.wfAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.orgNodeSelection.window) {
          $scope.buffers.orgNodeSelection.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.orgNodeStructureTree) {
          $scope.config.orgNodeStructureTree = widget;
          // $scope.treeInstance.expand(".k-item");
        }
      });

    }])
;
