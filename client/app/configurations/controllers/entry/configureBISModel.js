'use strict';

angular.module('cmisApp')
  .controller('ConfigureBISModelController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, SweetAlert) {

      //$scope.var
      $scope.BISConfigurationData = angular.fromJson(task.draft);

      $scope.BISCodes = {
        BIS1: "BIS1",
        BIS2: "BIS2",
        BIS3: "BIS3",
      };
      var prepareFormData = function (formData) {
        return formData;
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "BISConfigurationDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.BISConfigurationData)));
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.BISConfigurationData);
      });


    }]);
