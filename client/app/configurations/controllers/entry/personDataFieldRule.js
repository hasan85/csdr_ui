'use strict';

angular.module('cmisApp')
  .controller('PersonDataFieldRuleEntryController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personDataConfigurationService', 'operationState', 'task', 'Loader', 'helperFunctionsService', 'referenceDataService', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personDataConfigurationService, operationState, task, Loader, helperFunctionsService, referenceDataService, gettextCatalog) {


      $scope.metaData = {};
      Loader.show(true);
      referenceDataService.getOperationTypeClasses().then(function (data) {
        $scope.metaData.operationTypeClasses = helperFunctionsService.convertArrayToObjectByKey(data, 'code');
        Loader.show(false);
      });
      $scope.personFieldConfigurationData = angular.fromJson(task.draft);

      var localViewModel = angular.copy($scope.personFieldConfigurationData);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "personFieldConfigurationDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(angular.copy(localViewModel));
              }
            }
          }
        },
        showConfigurationDetails: false,
        labels: {
          naturalPerson: gettextCatalog.getString('Natural Person'),
          juridicalPerson: gettextCatalog.getString('Juridical Person'),
          insert: gettextCatalog.getString("Insert"),
          update: gettextCatalog.getString("Update")
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy(localViewModel));
      });

      $scope.buffer = {
        configurationData: null,
        subjectClass: null
      };

      $scope.toggleConfigurationDetailsView = function (subjectKey, objectKey, objectData, subjectClass, operationTypeCode) {
        if (!$scope.config.showConfigurationDetails) {
          Loader.show(true);
          $scope.buffer = {
            subjectKey: angular.copy(subjectKey),
            objectKey: angular.copy(objectKey),
            subjectClass: angular.copy(subjectClass),
            operationTypeCode: angular.copy(operationTypeCode)
          };

          var loadedConfig = null;
          if (operationTypeCode === $scope.metaData.operationTypeClasses.OPERATION_TYPE_INSERT.code) {
            loadedConfig = localViewModel.insertConfiguration.subjects[subjectKey].objects[objectKey].personConfigData;
          } else {
            loadedConfig = localViewModel.updateConfiguration.subjects[subjectKey].objects[objectKey].personConfigData;
          }
          if (loadedConfig) {
            objectData.personConfigData = loadedConfig;
            $scope.buffer.configurationData = angular.copy(objectData);
            Loader.show(false);

          } else {
            personDataConfigurationService.getPersonConfigurationData(subjectClass.code, objectData.objectClass.code, operationTypeCode).then(function (data) {

              if (data) {
                if (data.personConfigData) {
                  objectData.personConfigData = data.personConfigData;
                }
              }
              $scope.buffer.configurationData = angular.copy(objectData);
              Loader.show(false);
            });
          }
        }
        else {

          if ($scope.buffer.subjectKey > -1 && $scope.buffer.objectKey > -1) {
            if ($scope.buffer.operationTypeCode == $scope.metaData.operationTypeClasses.OPERATION_TYPE_INSERT.code) {
              localViewModel.insertConfiguration.subjects[$scope.buffer.subjectKey].objects[$scope.buffer.objectKey].personConfigData =
                $scope.buffer.configurationData.personConfigData;
            }
            else {
              localViewModel.updateConfiguration.subjects[$scope.buffer.subjectKey].objects[$scope.buffer.objectKey].personConfigData =
                $scope.buffer.configurationData.personConfigData;

            }
          }
          $scope.buffer = {
            subjectKey: null,
            objectKey: null,
            configurationData: null,
            subjectClass: null,
            operationTypeCode: operationTypeCode
          };
        }
        $scope.config.showConfigurationDetails = !$scope.config.showConfigurationDetails;
      };

      $scope.changeAvailability = function (subjectKey, objectKey, value, operationTypeCode) {
        if (operationTypeCode === $scope.metaData.operationTypeClasses.OPERATION_TYPE_INSERT.code) {
          localViewModel.insertConfiguration.subjects[subjectKey].objects[objectKey].enabled = value;
        } else {
          localViewModel.updateConfiguration.subjects[subjectKey].objects[objectKey].enabled = value;
        }
      };

      $scope.parseFieldValues = function (field) {

        if (field.visible === "true") {
          field.visible = true;
        }
        if (field.visible === "false") {
          field.visible = false;
        }

        if (field.mandatory === "true") {
          field.mandatory = true;
        }
        if (field.mandatory === "false") {
          field.mandatory = false;
        }


      };

    }]);
