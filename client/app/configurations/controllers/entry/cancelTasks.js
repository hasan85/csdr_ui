'use strict';

angular.module('cmisApp')
  .controller('CancelTasksController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', '$kWindow',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, dataSearchService, gettextCatalog, SweetAlert, $kWindow) {

        $scope.metaData = {
          currencies: []
        };


        var prepareFormData = function (viewModel) {
          viewModel.currency = angular.fromJson(viewModel.currency);
          viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);
          return viewModel;
        };

        $scope.data = angular.fromJson(task.draft);

        
        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "enterBankStatementDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  $scope.config.completeTask(prepareFormData(angular.copy($scope.data)));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        var _taskSelected = function (data) {
          if (data) {
            $scope.data = {
              nameAz: data.nameAz,
              operationID: data.processInstanceId,
              shortDescription: data.shortDescription
            }
          }
        };

        $scope.findTaskStatement = function () {
          $kWindow.open({
            title: gettextCatalog.getString('Find Tasks Statements'),
            actions: ['Close'],
            isNotTile: true,
            width: '90%',
            height: '90%',
            pinned: true,
            modal: true,
            templateUrl: 'app/main/templates/all-completed-tasks.html',
            controller: 'AllCompletedTasksController',
            resolve: {
              data: function () {
                return {
                  onselect: _taskSelected
                }
              }
            }
          });
        };

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
