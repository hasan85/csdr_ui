'use strict';

angular.module('cmisApp')
  .controller('RoleRulesManagementEntryController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'Loader',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, Loader) {

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "roleRulesManagementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(angular.copy($scope.roleRulesManagementData));
              }
            }
          }
        },
        labels: {
          availabilityParameters: gettextCatalog.getString('Availability Parameters'),
          combinationOfTheRoles: gettextCatalog.getString('Combination of the Roles')
        }
      };

      $scope.roleRulesManagementData = angular.fromJson(task.draft);

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.roleRulesManagementData);
      });


    }]);
