'use strict';

angular.module('cmisApp')
  .controller('EmployeeManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'orgNodeStructureManagementService', '$q',
    'helperFunctionsService', 'managePersonService', 'employeeManagementService', 'Loader', 'SweetAlert', 'dialogService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, orgNodeStructureManagementService, $q,
              helperFunctionsService, managePersonService, employeeManagementService, Loader, SweetAlert, dialogService) {


      var orgNodeClassConstants = {
        position: 'ORGNODE_POSITION',
        group: 'ORGNODE_GROUP',
        person: 'ORGNODE_PERSON',
        positionInternSuffix: '_INTERN'
      };
      var idDocumentClassCodes = {
        uniqueCode: 'UNIQUE_CODE',
        idCardClassCode: "ID_CARD",
      };
      var personDataEntryConfigs = {
        forInsert: {},
        forUpdate: {}
      };

      var prepareFormData = function () {
        var result = {
          employees: angular.copy($scope.employeesData.employees),
          contextOrgNode: angular.copy($scope.treeInstance.dataSource.view()[0]),
          representativeEntryConfigForInsert: $scope.employeesData.representativeEntryConfigForInsert,
          representativeEntryConfigForUpdate: $scope.employeesData.representativeEntryConfigForUpdate
        };
        for (var i = 0; i < result.employees.length; i++) {
          if (result.employees[i]['personData'].ID < 0) {
            result.employees[i]['personData'].ID = null;
          }
          result.employees[i]['personData'] = managePersonService.prepareFormData(result.employees[i]['personData'], $scope.metaData);
        }
        return result;
      };
      // Buffers
      $scope.buffers = angular.copy(managePersonService.getPersonDataEntryFormBuffers());

      $scope.orgNodeWindowInstance = {};

      $scope.employeesData = angular.fromJson(task.draft);

      var getPositionInternCode = function () {

        return $scope.employeesData.contextOrgNode.code.toUpperCase() + orgNodeClassConstants.positionInternSuffix;
      };

      $scope.metaData = {};

      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "employeesDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(angular.copy(
                  prepareFormData()
                ));
              }
            }
          }
        },
        labels: {
          orgNodeStructure: gettextCatalog.getString('Organizational Structure'),
          allEmployees: gettextCatalog.getString('All Employees'),
          actualAddress: gettextCatalog.getString('Actual Address'),
          legalAddress: gettextCatalog.getString('Legal Address')
        },
        selectedTab: 0,
        orgNodeFormName: "orgNodeAddForm"

      };

      $scope.buffers = angular.copy(managePersonService.getPersonDataEntryFormBuffers());

      $scope.data = {
        config: {
          formName: "representativeForm"
        }
      };

      Loader.show(true);


      $scope.jurisdictionCountryChange = function (person) {
        var model = person.form.jurisdictionCountry ? angular.fromJson(person.form.jurisdictionCountry) : null;
        if (model) {
          person.config.isTINCodeMandatory = model.code == "AZ" ? true : false;
        }
      };

      $scope.currencyChange = function (bankAccountBuffer) {
        var model = bankAccountBuffer.form.currency ? angular.fromJson(bankAccountBuffer.form.currency) : null;
        if (model) {
          bankAccountBuffer.isBankCodeMandatory = model.code == "AZN" ? true : false;
        }
      };

      employeeManagementService.getMetaData().then(function (data) {

        var naturalPersonIdDocumentTypes = [];
        if (data.naturalPersonIdDocumentTypes) {
          angular.forEach(data.naturalPersonIdDocumentTypes, function (value, key) {
            if (value['code'] != idDocumentClassCodes.idCardClassCode) {
              naturalPersonIdDocumentTypes.push(value);
            }
          });
        }

        // Bind metadata
        $scope.metaData = {
          personClasses: data.personClasses,
          phoneNumberTypes: data.phoneNumberTypes,
          countries: data.countries,
          currencies: data.currencies,
          businessClasses: data.businessClasses,
          legalFormClasses: data.legalFormClasses,
          allIdDocumentClasses: naturalPersonIdDocumentTypes,
          naturalPersonIdDocumentTypes: naturalPersonIdDocumentTypes,
          signerPositions: data.signerPositions,
        };

        var configForInsertRaw = $scope.employeesData['representativeEntryConfigForInsert'] ?
          angular.fromJson($scope.employeesData['representativeEntryConfigForInsert']) : null;

        var configForUpdateRaw = $scope.employeesData['representativeEntryConfigForUpdate'] ?
          angular.fromJson($scope.employeesData['representativeEntryConfigForUpdate']) : null;

        // Prepare config data
        personDataEntryConfigs.forInsert = configForInsertRaw ? managePersonService.generateFormConfig(configForInsertRaw) : null;
        personDataEntryConfigs.forUpdate = configForUpdateRaw ? managePersonService.generateFormConfig(configForUpdateRaw) : null;

        $scope.buffers.representative.data.config.fields = personDataEntryConfigs.forInsert;

        if ($scope.employeesData && $scope.employeesData.employees) {
          for (var i = 0; i < $scope.employeesData.employees.length; i++) {
            $scope.employeesData.employees[i]['personData'] = managePersonService.convertViewModelToForm($scope.employeesData.employees[i]['personData'], $scope.metaData);
          }
        }

        $scope.buffers.selectedEmployee = {};

        for (var key in $scope.employeesData.contextOrgNode.items) {
          if ($scope.employeesData.contextOrgNode.items[key].code && $scope.employeesData.contextOrgNode.items[key].code == getPositionInternCode()) {

            $scope.buffers.defaultPositionClass = angular.copy($scope.employeesData.contextOrgNode.items[key]);
            Loader.show(false);
            return;
          }

        }

        for (var key in $scope.employeesData.contextOrgNode.items) {
          if ($scope.employeesData.contextOrgNode.items[key].code
            && (helperFunctionsService.startsWith($scope.employeesData.contextOrgNode.items[key].code, 'ADMIN_')
            || helperFunctionsService.endsWith($scope.employeesData.contextOrgNode.items[key].code, '_ADMIN'))
          ) {
            $scope.buffers.defaultPositionClass = angular.copy($scope.employeesData.contextOrgNode.items[key]);
          }
        }

        Loader.show(false);

      });

      //Manage Person Documents
      $scope.addNewDocument = function (window, model, form) {
        managePersonService.addNewDocument(window, model, form, $scope.buffers.document);
      };
      $scope.removeDocument = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };
      $scope.showDocumentUpdateForm = function (index, model, window) {
        managePersonService.showDocumentUpdateForm(index, model, window, $scope.buffers.document);
      };
      $scope.updateDocument = function (window, model, form) {
        managePersonService.updateDocument(window, model, form, $scope.buffers.document);
      };
      $scope.resetDocumentBuffer = function (window, form) {
        managePersonService.resetDocumentBuffer(window, form, $scope.buffers.document);
      };

      //Manage Person Bank Accounts
      $scope.addNewBankAccount = function (window, model, form) {
        managePersonService.addNewBankAccount(window, model, form, $scope.buffers.bankAccount);
      };
      $scope.removeBankAccount = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };
      $scope.showBankAccountUpdateForm = function (index, model, window) {

        managePersonService.showBankAccountUpdateForm(index, model, window, $scope.buffers.bankAccount);
      };
      $scope.updateBankAccount = function (window, model, form) {
        managePersonService.updateBankAccount(window, model, form, $scope.buffers.bankAccount);
      };
      $scope.resetBankAccountBuffer = function (window, form) {
        managePersonService.resetBankAccountBuffer(window, form, $scope.buffers.bankAccount);
      };

      //Manage Person Phone Numbers
      $scope.addNewPhoneNumber = function (model) {
        managePersonService.addNewPhoneNumber(model);
      };
      $scope.removePhoneNumber = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };

      //Manage Person Emails
      $scope.addNewEmail = function (model) {
        managePersonService.addNewEmail(model)
      };
      $scope.removeEmail = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };

      var getDefaultSignerPositionInsideTree = function () {

        var items = $scope.treeInstance.dataSource.view()[0].items;
        var id = $scope.buffers.defaultPositionClass.ID;

        for (var item in items) {
          if (parseInt(items[item].ID) === parseInt(id)) {
            return items[item];
          }
        }

      };
      var updateEmployeeNameInsideTree = function (id, name, node) {

        if (node.personID === id) {
          node = angular.extend(node, {
            name: {
              nameAz: name,
              nameEn: name
            }
          });
        }
        else {
          if (node.items && node.items.length > 0) {
            for (var i = 0; i < node.items.length; i++) {
              updateEmployeeNameInsideTree(id, name, node.items[i]);
            }
          }
        }
      };
      var removeEmployeeFromTree = function (id, node) {

        if (node.personID == id) {
          var array = node.parent();
          var index = array.indexOf(node);
          array.splice(index, 1);
        }
        else {
          if (node.items && node.items.length > 0) {
            for (var i = 0; i < node.items.length; i++) {
              removeEmployeeFromTree(id, node.items[i]);
            }
          }
        }
      };

      var updateEmployeeParentNode = function (employeeId, sourceNodeParentId, destinationNode, isCopied) {

        for (var i = 0; i < $scope.employeesData.employees.length; i++) {

          if ($scope.employeesData.employees[i]['personData'].ID == employeeId) {

            if (!$scope.employeesData.employees[i]['parentOrgNodes']) {
              $scope.employeesData.employees[i]['parentOrgNodes'] = [];
            }

            $scope.employeesData.employees[i]['parentOrgNodes'].push(destinationNode);
            if (!isCopied) {
              for (var j = 0; j < $scope.employeesData.employees[i]['parentOrgNodes'].length; j++) {
                if ($scope.employeesData.employees[i]['parentOrgNodes'][j].ID == sourceNodeParentId) {
                  $scope.employeesData.employees[i]['parentOrgNodes'].splice(j, 1);
                }
              }
            }
          }
        }
      };
      var deleteEmployeeParentNode = function (employeeId, sourceNodeParentId) {
        for (var i = 0; i < $scope.employeesData.employees.length; i++) {
          if ($scope.employeesData.employees[i]['personData'].ID == employeeId) {
            for (var j = 0; j < $scope.employeesData.employees[i]['parentOrgNodes'].length; j++) {
              if ($scope.employeesData.employees[i]['parentOrgNodes'][j].ID == sourceNodeParentId) {
                $scope.employeesData.employees[i]['parentOrgNodes'].splice(j, 1);
              }
            }
          }
        }
      };

      //Manage Representatives
      $scope.addNewRepresentative = function (model, window) {

        $scope.buffers.representative.data.config.formName.$submitted = true;
        if ($scope.buffers.representative.data.config.formName.$valid) {
          window.close();

          model.ID = -(helperFunctionsService.randomString(10, "N"));
          $scope.employeesData.employees.push({
            personData: model,
            parentOrgNodes: [$scope.buffers.defaultPositionClass]
          });

          var parentItem = getDefaultSignerPositionInsideTree();
          // Append New Person to OrgNode[[
          var selectedItem = $scope.treeInstance.findByUid(parentItem.uid);
          $scope.treeInstance.select(selectedItem);

          var names = managePersonService.generateFullNameOfPerson(model);

          $scope.treeInstance.append({
            personID: model.ID,
            name: {
              nameAz: names,
              nameEn: names
            },
            orgNodeClass: {
              code: orgNodeClassConstants.person
            }
          }, $scope.treeInstance.select());
          $scope.buffers.representative.data.config.formName.$setPristine();
          $scope.buffers.representative.data.config.formName.$setUntouched();
        }
      };
      $scope.showRepresentativeUpdateForm = function (index, model, window) {

        $scope.buffers.representative.data.config.fields = personDataEntryConfigs.forUpdate;
        window.title(gettextCatalog.getString('Update Representative'));
        var representative = angular.copy(model[index]);
        $scope.buffers.representative.data.form = representative['personData'];

        $scope.buffers.representative.data.currentIndex = index;

        window.open();
        window.center();
      };
      $scope.updateRepresentative = function (model, window) {

        $scope.buffers.representative.data.config.formName.$submitted = true;
        if ($scope.buffers.representative.data.config.formName.$valid) {

          var representative = angular.copy($scope.buffers.representative.data.form);
          var updatedName = managePersonService.generateFullNameOfPerson(representative);

          updateEmployeeNameInsideTree(representative.ID, updatedName, $scope.treeInstance.dataSource.view()[0]);
          var index = $scope.buffers.representative.data.currentIndex;
          $scope.employeesData.employees[index]['personData'] = representative;
          $scope.resetRepresentativesBuffer(window);
          window.close();
        }

      };
      $scope.removeRepresentative = function (index, model) {

        removeEmployeeFromTree(model[index].personData.ID, $scope.treeInstance.dataSource.view()[0]);
        model.splice(index, 1);
      };
      $scope.resetRepresentativesBuffer = function (window) {

        $scope.buffers.selectedEmployee = {};
        $scope.buffers.representative.data.config.fields = personDataEntryConfigs.forInsert;
        window.title(gettextCatalog.getString('Add New Representative'));
        $scope.buffers.representative.data.currentIndex = -1;
        $scope.buffers.representative.data.form = {
          documents: [],
          bankAccounts: [],
          phoneNumbers: [
            {
              type: null,
              number: ''
            }
          ],
          emails: [
            {value: ''}
          ]
        };

        $scope.buffers.representative.data.config.formName.$setPristine();
        $scope.buffers.representative.data.config.formName.$setUntouched();

      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        //var taskPayload = task && task.draft ? task.draft : null;
        //var currentTask = angular.toJson(prepareFormData($scope.employeesData));
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(prepareFormData(angular.copy($scope.employeesData)));
      });


      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.tree) {
          $scope.treeInstance = widget;
          $scope.treeInstance.expand(".k-item");
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.orgNodeAddWindow) {
          $scope.orgNodeWindowInstance = widget;
        }
      });


      // Org Node Tree operations
      $scope.treeInstance = null;
      orgNodeStructureManagementService.expandAllNodesOnInit($scope.employeesData.contextOrgNode);
      $scope.treeData = new kendo.data.HierarchicalDataSource({
        data: [$scope.employeesData.contextOrgNode]
      });
      $scope.employeeDragStart = function (node) {
        var dataItems = orgNodeStructureManagementService.getDataItemsByNode(node, $scope.treeInstance);
        if (orgNodeClassConstants.person !== dataItems.source.orgNodeClass.code) {
          node.preventDefault();
        }
      };
      $scope.employeeDrop = function (node) {

        var dataItems = orgNodeStructureManagementService.getDataItemsByNode(node, $scope.treeInstance);
        if ((dataItems.destination.orgNodeClass.code == orgNodeClassConstants.position || dataItems.destination.orgNodeClass.code == orgNodeClassConstants.group ) && node.dropPosition == "over" && !orgNodeStructureManagementService.checkIfNodeIsInItems(dataItems.destination, dataItems.source.ID)
        ) {

          var sourceParentNode = $scope.treeInstance.dataItem($scope.treeInstance.parent(node.sourceNode));
          if (sourceParentNode.code !== getPositionInternCode()) {
            node.setValid(false);
            dialogService.showDialog(gettextCatalog.getString('Copy'),
              gettextCatalog.getString('Dou you want to copy or move this employee ?'),
              {
                buttons: [
                  {value: 'copy', text: gettextCatalog.getString('Copy')},
                  {value: 'move', text: gettextCatalog.getString('Move')}
                ]
              }
            ).then(function (data) {

                var sourceNode = null;
                var destinationNode = $scope.treeInstance.findByUid(dataItems.destination.uid);
                if (data === "move") {
                  sourceNode = $scope.treeInstance.findByUid(dataItems.source.uid);
                } else {
                  sourceNode = $scope.treeInstance.dataItem(node.sourceNode).toJSON();
                }
                $scope.treeInstance.append(sourceNode, destinationNode);

                updateEmployeeParentNode(dataItems.source.personID,
                  sourceParentNode.ID,
                  dataItems.destination,
                  data === "move" ? false : true);
              });
          } else {

            updateEmployeeParentNode(dataItems.source.personID, sourceParentNode.ID, dataItems.destination, false);
          }
        } else {
          node.preventDefault();
        }
      };
      $scope.removeFromTree = function (item) {
        var array = item.parent();
        var index = array.indexOf(item);
        var node = $scope.treeInstance.findByUid(item.uid);
        var parentNode = $scope.treeInstance.dataItem($scope.treeInstance.parent(node));
        deleteEmployeeParentNode(item.personID, parentNode.ID);
        array.splice(index, 1);
      };

      //Watched variables
      $scope.$watch("buffers.document.currentIndex", function (newVal) {
        $scope.config.documentManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Document') :
          gettextCatalog.getString('Add New Document');

      });
      $scope.$watch("buffers.bankAccount.currentIndex", function (newVal) {
        $scope.config.bankAccountManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Bank Account') : gettextCatalog.getString('Add New Bank Account');

      });
      $scope.$watch("buffers.representative.data.currentIndex", function (newVal) {
        $scope.config.representativeManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Employee') : gettextCatalog.getString('Add New Employee');

      });
    }])
;
