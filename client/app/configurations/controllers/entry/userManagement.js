'use strict';

angular.module('cmisApp')
  .controller('UserManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog
               ) {

      $scope.usersData = angular.fromJson(task.draft);

      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "employeesDataForm",
          data: {},

        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(angular.copy(
                  $scope.usersData
                ));
              }
            }
          }
        },
        labels: {
          userManageWindowTitle: gettextCatalog.getString('Add New User')
        }

      };

      $scope.buffers = {
        user: {
          currentIndex: -1,
          form: {},
          config: {
            form: {
              name: "userManageForm"
            }
          }
        }

      };

      $scope.showUserCreationForm = function (index) {
        $scope.buffers.user.userManageWindow.title(gettextCatalog.getString('Add New User'));
        $scope.config.labels.userManageWindowTitle = gettextCatalog.getString('Add New User');
        $scope.buffers.user.currentIndex = index;
          $scope.buffers.user.emails = $scope.usersData.employees[index].emails;
        $scope.buffers.user.isNewUser = true;
        $scope.buffers.user.userManageWindow.open();
        $scope.buffers.user.userManageWindow.center();
      };

      //Manage Representatives
      $scope.addUser = function () {

        $scope.buffers.user.config.form.name.$submitted = true;
        if ($scope.buffers.user.config.form.name.$valid) {

          $scope.usersData.employees[$scope.buffers.user.currentIndex].username = $scope.buffers.user.form.username;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].password = $scope.buffers.user.form.password;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].email = $scope.buffers.user.form.email;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].pinCode = $scope.buffers.user.form.pinCode;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].strongAuthRequired = $scope.buffers.user.form.strongAuthRequired;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].suspended = $scope.buffers.user.form.suspended;

          $scope.resetUserBuffer();

            $scope.buffers.user.userManageWindow.close();
        }
      };
      $scope.showUserUpdateForm = function (index) {
        $scope.buffers.user.isNewUser = false;
        $scope.buffers.user.userManageWindow.title(gettextCatalog.getString('Update User'));
        $scope.config.labels.userManageWindowTitle = gettextCatalog.getString('Update User');
        $scope.buffers.user.form = {
          userID: $scope.usersData.employees[index].userID,
          username: $scope.usersData.employees[index].username,
          password: $scope.usersData.employees[index].password,
          email: $scope.usersData.employees[index].email,
          pinCode: $scope.usersData.employees[index].pinCode,
          strongAuthRequired: $scope.usersData.employees[index].strongAuthRequired,
          suspended: $scope.usersData.employees[index].suspended
        };
        $scope.buffers.user.currentIndex = index;
          $scope.buffers.user.emails = $scope.usersData.employees[index].emails;
        $scope.buffers.user.userManageWindow.open();
        $scope.buffers.user.userManageWindow.center();
      };
      $scope.updateUser = function () {

        $scope.buffers.user.config.form.name.$submitted = true;
        if ($scope.buffers.user.config.form.name.$valid) {

          $scope.usersData.employees[$scope.buffers.user.currentIndex].username = $scope.buffers.user.form.username;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].password = $scope.buffers.user.form.password;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].email = $scope.buffers.user.form.email;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].pinCode = $scope.buffers.user.form.pinCode;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].strongAuthRequired = $scope.buffers.user.form.strongAuthRequired;
          $scope.usersData.employees[$scope.buffers.user.currentIndex].suspended = $scope.buffers.user.form.suspended;
          $scope.buffers.user.userManageWindow.close();
          $scope.resetUserBuffer();
        }

      };
      $scope.removeUser = function (index) {
        $scope.usersData.employees[index].username = null;
        $scope.usersData.employees[index].password = null;
        $scope.usersData.employees[index].email = null;
        $scope.usersData.employees[index].pinCode = null;
        $scope.usersData.employees[index].strongAuthRequired = null;
        $scope.usersData.employees[index].suspended = null;
       // $scope.usersData.employees[index].userID = null;
      };
      $scope.resetUserBuffer = function () {
        $scope.buffers.user.userManageWindow.title(gettextCatalog.getString('Add New User'));
        $scope.config.labels.userManageWindowTitle = gettextCatalog.getString('Add New User');
        $scope.buffers.user.currentIndex = -1;
        $scope.buffers.user.form = {};
        $scope.buffers.user.config.form.name.$setPristine();
        $scope.buffers.user.config.form.name.$setUntouched();
        $scope.buffers.user.isNewUser = false;
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        //var taskPayload = task && task.draft ? task.draft : null;
        //var currentTask = angular.toJson(prepareFormData($scope.employeesData));
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.employeesData);
      });

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.user.userManageWindow) {
          $scope.buffers.user.userManageWindow = widget;
        }
      });

    }]);
