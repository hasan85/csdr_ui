'use strict';

angular.module('cmisApp')
  .controller('GlobalConfigurationsEntryController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, SweetAlert) {

      $scope.globalConfigurationData = angular.fromJson(task.draft);
      var prepareFormData = function (formData) {
        var viewModel = {};
        viewModel.account = formData.account && formData.account.accountId ? formData.account : null;
        return viewModel;
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "globalConfigurationDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;

              var formValid = true;
              if ($scope.globalConfigurationData.accountConfiguration.depoAccountConfiguration.collectiveAccount.isEnabled) {

                if (!($scope.globalConfigurationData.accountConfiguration.depoAccountConfiguration.jointAccount.isEnabled
                  || $scope.globalConfigurationData.accountConfiguration.depoAccountConfiguration.coownerAccount.isEnabled)) {
                  formValid = false;
                  SweetAlert.swal('', gettextCatalog.getString('You enabled Collective Account.\n ' +
                    'You have to enable either Joint Account or Coowner Account also.'), 'error');
                }
              } else {
                // $scope.globalConfigurationData.accountConfiguration.depoAccountConfiguration.jointAccount.isEnabled = false;
                //  $scope.globalConfigurationData.accountConfiguration.depoAccountConfiguration.coownerAccount.isEnabled = false;
              }
              if ($scope.config.form.name.$valid && formValid) {
                $scope.config.completeTask(angular.copy($scope.globalConfigurationData));
              }
            }
          }
        },
        labels: {
          accountParameters: gettextCatalog.getString('Account Parameters'),
          csdConfigParameters: gettextCatalog.getString('CSD Account Parameters')
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.globalConfigurationData);
      });

    }]);
