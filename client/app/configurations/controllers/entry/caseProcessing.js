'use strict';

angular.module('cmisApp')
  .controller('CaseProcessingController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', 'reportService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, reportService) {

      var instrumentFormPartialTemplate = {
        id: null,
        ISIN: null,
        issuerName: null,
        instrumentName: null
      };
      var accountFormPartialTemplate = {
        id: null,
        accountId: null,
        name: null,
        accountNumber: null
      };
      var prepareFormData = function (formData) {

        if (formData.currentTimeSelected) {
          formData.requestDate = helperFunctionsService.generateDateTime(new Date());
        } else {
          if (formData.requestDateObj) {
            formData.requestDate = helperFunctionsService.generateDateTime(formData.requestDateObj);
          }
        }
        delete formData.requestDateObj;
        return formData;
      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.accounts === null || draft.accounts === undefined) {
          draft.accounts = [
            angular.copy(accountFormPartialTemplate)
          ];
        }
        if (draft.instruments === null || draft.instruments === undefined) {
          draft.instruments = [
            angular.copy(instrumentFormPartialTemplate)
          ];
        }
        if (draft.currentTimeSelected === null || draft.currentTimeSelected === undefined) {
          draft.currentTimeSelected = true;
        }
        return draft;
      };
      $scope.caseProcessingData = initializeFormData(angular.fromJson(task.draft));
      reportService.normalizeReturnedReportTask($scope.caseProcessingData);
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "caseProcessingDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {

                if (($scope.caseProcessingData.accounts && $scope.caseProcessingData.accounts[0].id > 0)
                  || ( $scope.caseProcessingData.instruments && $scope.caseProcessingData.instruments[0].id > 0)
                ) {

                  $scope.config.completeTask(prepareFormData(angular.copy($scope.caseProcessingData)));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! You have to select at least one account or instrument'), 'error');
                }

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.caseProcessingData);
      });

      $scope.$watch("caseProcessingData.currentTimeSelected", function(newVal, oldVal) {
        if(newVal !== oldVal) {
          $scope.resetSelectedInsturments();
        }
      });

      $scope.$watch("caseProcessingData.requestDateObj", function(newVal, oldVal) {
        if(newVal !== oldVal) {
          $scope.resetSelectedInsturments();
        }
      });

      $scope.resetSelectedInsturments = function() {
        $scope.caseProcessingData.instruments = [
          angular.copy(instrumentFormPartialTemplate)
        ];
      };

      // Search instrument
      $scope.findInstrument = function (index) {
        instrumentFindService.findInstrument({
          params: {
            date: $scope.caseProcessingData.currentTimeSelected ? null : $scope.caseProcessingData.requestDateObj
          }
        }).then(function (data) {
          $scope.caseProcessingData.instruments[index].ISIN = data.isin;
          $scope.caseProcessingData.instruments[index].id = data.id;
          $scope.caseProcessingData.instruments[index].issuerName = data.issuerName;
          $scope.caseProcessingData.instruments[index].instrumentName = data.instrumentName;
        });
      };

      // Add new instrument
      $scope.addNewInstrument = function (model) {
        model.push(angular.copy(instrumentFormPartialTemplate));
      };

      // Remove instrument
      $scope.removeInstrument = function (index, model) {
        model.splice(index, 1);
      };

      $scope.findAccount = function (index) {
        personFindService.findShareholderAccountsWithOperatorName().then(function (data) {
          $scope.caseProcessingData.accounts[index].accountId = data.accountId;
          $scope.caseProcessingData.accounts[index].id = data.id;
          $scope.caseProcessingData.accounts[index].accountNumber = data.accountNumber;
          $scope.caseProcessingData.accounts[index].name = data.name;
          $scope.caseProcessingData.accounts[index].idDocument = data.idDocument;
          $scope.caseProcessingData.accounts[index].addressLine = data.addressLine;
        });
      };

      $scope.addNewAccount = function (model) {
        model.push(angular.copy(accountFormPartialTemplate));
      };

      $scope.removeAccount = function (index, model) {
        model.splice(index, 1);
      };


    }]);
