'use strict';

angular.module('cmisApp')
  .controller('OperationParametersManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', '$rootScope',
    'referenceDataService', 'helperFunctionsService', 'Loader', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, $rootScope,
              referenceDataService, helperFunctionsService, Loader, SweetAlert) {

      $scope.readonly = false;
      var taskTypeConstants = {
        entry: 'ENTRY_TASK_CLASS',
        approvalTask: 'APPROVAL_TASK_CLASS',
        printTask: "PRINT_TASK_CLASS"
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "operationParamDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;

              angular.forEach($scope.operationParamData.operationParameterDataGroup, function (value) {
                value.operationParam = angular.toJson(value.operationParam);
              });
              $scope.config.completeTask(angular.copy($scope.operationParamData));

            }
          }
        },
        labels: {
          operationParam: gettextCatalog.getString('Process Updating Operation Parameters'),
        }
      };

      $scope.operationParamData = angular.fromJson(task.draft);

      var signerOperatorTemplate = {
        name: "",
        position: "",
        isDefault: false
      };

      angular.forEach($scope.operationParamData.operationParameterDataGroup, function (value) {
        if(value.operationParam == null) {
          value.operationParam = {
            signers: [
              angular.copy(signerOperatorTemplate)
            ],
            operators: [
              angular.copy(signerOperatorTemplate)
            ]
          };
        } else {
          value.operationParam = angular.fromJson(value.operationParam);
        }
      });

      $scope.buffers = {
        operationParam: {
          data: {},
          currentIndex: -1
        }
      };

      // Add/Remove Signer

      $scope.signers = [
        angular.copy(signerOperatorTemplate)
      ];

      $scope.addNewSigner = function () {
        $scope.operationParam.signers.push(angular.copy(signerOperatorTemplate));
      };

      $scope.removeSigner = function (index) {
        $scope.operationParam.signers.splice(index, 1);
      };

      // Add/Remove Operator

      $scope.operators = [
        angular.copy(signerOperatorTemplate)
      ];

      $scope.addNewOperator = function () {

        $scope.operationParam.operators.push(angular.copy(signerOperatorTemplate));
      };

      $scope.removeOperator = function (index) {
        $scope.operationParam.operators.splice(index, 1);
      };

      // Process Authorization [[

      $scope.showOperationParamForm = function (index) {
        $scope.buffers.operationParam.data = $scope.operationParamData.operationParameterDataGroup[index];
        $scope.buffers.operationParam.window.title(
          $scope.buffers.operationParam.data.operationName['name' + $rootScope.lnC] + " [ " +
          $scope.buffers.operationParam.data.operationKey + " ]");
        $scope.buffers.operationParam.currentIndex = index;
        $scope.buffers.operationParam.window.open();
        $scope.buffers.operationParam.window.center();
        $scope.operationKey = $scope.buffers.operationParam.data.operationKey;
        $scope.operationParam = $scope.operationParamData
          .operationParameterDataGroup[$scope.buffers.operationParam.currentIndex].operationParam;

      };
      $scope.isDefaultChange = function(signer,signers) {
        angular.forEach(signers,function (value) {
          value.isDefault = false;
        });
        signer.isDefault = true;
        console.log(signer);
      };

      $scope.resetOperationParamBuffer = function () {
        $scope.buffers.operationParam.currentIndex = -1;
        $scope.buffers.operationParam.window.title(null);
        $scope.buffers.operationParam.data = {};
      };
      $scope.removeProcessController = function (index) {
        $scope.buffers.operationParam.data.processControllers.splice(index, 1)
      };
      $scope.removeTaskController = function (model, index) {
        model.splice(index, 1)
      };
      // Process Authorization ]]

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        //  var taskPayload = task && task.draft ? task.draft : null;
        //  var currentTask = angular.toJson(prepareFormData($scope.updatingOperationParamsData));
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.operationParamData);
      });

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.operationParam.window) {
          $scope.buffers.operationParam.window = widget;
        }
      });

    }]);
