'use strict';

angular.module('cmisApp')
  .controller('RoleRulesManagementApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        labels: {
          availabilityParameters: gettextCatalog.getString('Availability Parameters'),
          combinationOfTheRoles: gettextCatalog.getString('Combination of the Roles')
        }
      };

    }]);
