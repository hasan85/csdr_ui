'use strict';

angular.module('cmisApp')
  .controller('EmployeeManagementApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        labels: {
          accountParameters: gettextCatalog.getString('Account Parameters'),
          csdConfigParameters: gettextCatalog.getString('CSD Account Parameters')
        }
      };

      $scope.employeesData = angular.fromJson(task.draft);

      $scope.treeData = new kendo.data.ObservableArray(
        [$scope.employeesData.contextOrgNode]
      );
    }]);
