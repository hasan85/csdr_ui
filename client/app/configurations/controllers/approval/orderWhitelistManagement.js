'use strict';

angular.module('cmisApp')
  .controller('OrderWhitelistManagementApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog) {

      $scope.whitelistItemsData = angular.fromJson(task.draft);
      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        labels: {
          whitelistItemManageWindowTitle: gettextCatalog.getString('View Item')
        }
      };

      $scope.buffers = {
        whitelistItem: {
          currentIndex: -1,
          form: {},
          config: {
            form: {
              name: "whitelistItemManageForm"
            }
          },
          isNewWhitelistItem: true,
          isReadOnlyView: false
        }

      };
      $scope.showReadOnlyView = function (index) {
        $scope.buffers.whitelistItem.form = $scope.whitelistItemsData.vmOrderWhiteLists[index];
        $scope.buffers.whitelistItem.whitelistItemManageWindow.title(gettextCatalog.getString('View Item'));
        $scope.buffers.whitelistItem.whitelistItemManageWindow.open();
        $scope.buffers.whitelistItem.whitelistItemManageWindow.center();
      };

      $scope.resetWhitelistItemBuffer = function () {
        $scope.buffers.whitelistItem.currentIndex = -1;
        $scope.buffers.whitelistItem.form = {};
      };
    }]);
