'use strict';

angular.module('cmisApp')
  .controller('CancelOperationReviewController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task',
      function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {

        $scope.data = angular.fromJson(task.draft)


        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "enterBankStatementDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  $scope.config.completeTask(angular.copy($scope.data));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };

      }]);
