'use strict';

angular.module('cmisApp')
  .controller('CancelTasksApprovalController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task',
      function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {

        $scope.data = angular.fromJson(task.draft)
        $scope.config = {
          screenId: id,
          taskId: taskId,
          task: task,
          operationType: appConstants.operationTypes.approval,
          window: $windowInstance,
          state: operationState
        };

      }]);
