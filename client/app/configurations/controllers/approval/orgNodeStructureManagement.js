'use strict';

angular.module('cmisApp')
  .controller('OrgNodeStructureManagementApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
      };

      $scope.treeData = new kendo.data.ObservableArray(
        [angular.fromJson(task.draft)]
      );

    }]);
