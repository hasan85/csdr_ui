'use strict';

angular.module('cmisApp')
  .controller('AccountRulesManagementApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        labels: {
          accounts: gettextCatalog.getString('Accounts'),
          accountNumberingRules: gettextCatalog.getString('Account Numbering Rules')
        }
      };

    }]);
