'use strict';

angular.module('cmisApp')
  .controller('AuthorizationManagementApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'gettextCatalog', 'orgNodeStructureManagementService', '$rootScope',
    'referenceDataService', 'helperFunctionsService', 'Loader', 'SweetAlert','taskId',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, gettextCatalog, orgNodeStructureManagementService, $rootScope,
              referenceDataService, helperFunctionsService, Loader, SweetAlert,taskId) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        labels: {
          accountParameters: gettextCatalog.getString('Account Parameters'),
          csdConfigParameters: gettextCatalog.getString('CSD Account Parameters'),
          processAuthorization: gettextCatalog.getString('Process Authorization'),
          viewConfiguration: gettextCatalog.getString('View Authorization'),
          dashboardConfiguration: gettextCatalog.getString('Dashboard Authorization'),
          editProcessAuthorization: gettextCatalog.getString('Edit Process Authorization'),
          addNewOrgNode: gettextCatalog.getString('Select Organizational Node'),
          overallOperationAuth: gettextCatalog.getString('Overall Operation Authorization'),
          workflowAuth: gettextCatalog.getString('Workflow Authorization'),
          generalConfiguration: gettextCatalog.getString('General Configuration'),
          controllers: gettextCatalog.getString('Controllers'),
          operators: gettextCatalog.getString('Operators'),
          workflow: gettextCatalog.getString('Workflow'),
          processControllers: gettextCatalog.getString('Process Controllers'),
          processStarters: gettextCatalog.getString('Process Starters')
        },
        orgNodeSelectionDestinations: {
          processController: 1
        }
      };


      $scope.authorizationManagementData = angular.fromJson(task.draft);

      $scope.buffers = {
        processAuthorization: {
          data: {},
          currentIndex: -1
        },
        viewAuthorization: {
          data: {},
          currentIndex: -1
        },
        dashboardAuthorization: {
          data: {},
          currentIndex: -1
        },
        wfAuthorization: {
          data: {},
          currentIndex: -1
        },
        orgNodeSelection: {
          destination: null
        }
      };

      // Process Authorization [[
      $scope.showProcessAuthorizationForm = function (index) {
        $scope.buffers.processAuthorization.data = $scope.authorizationManagementData.processAuthorizations[index];
        $scope.buffers.processAuthorization.currentIndex = index;
        $scope.buffers.processAuthorization.window.title(
          $scope.buffers.processAuthorization.data.name['name' + $rootScope.lnC] + " [ " +
          $scope.buffers.processAuthorization.data.processKey + " ]");
        $scope.buffers.processAuthorization.window.open();
        $scope.buffers.processAuthorization.window.center();
      };

      // Process Authorization ]]

      // View Authorization [[
      $scope.showViewAuthorizationForm = function (index) {
        $scope.buffers.viewAuthorization.data = $scope.authorizationManagementData.viewAuthorizations[index];
        $scope.buffers.viewAuthorization.currentIndex = index;
        $scope.buffers.viewAuthorization.window.title($scope.buffers.viewAuthorization.data.name['name' + $rootScope.lnC]);
        $scope.buffers.viewAuthorization.window.open();
        $scope.buffers.viewAuthorization.window.center();
      };

      // View Authorization ]]

      // Dashboard Authorization [[
      $scope.showDashboardAuthorizationForm = function (index) {
        $scope.buffers.dashboardAuthorization.data = $scope.authorizationManagementData.dashboardAuthorizations[index];
        $scope.buffers.dashboardAuthorization.currentIndex = index;
        $scope.buffers.dashboardAuthorization.window.title($scope.buffers.dashboardAuthorization.data.name['name' + $rootScope.lnC]);
        $scope.buffers.dashboardAuthorization.window.open();
        $scope.buffers.dashboardAuthorization.window.center();
      };

      // Dashboard Authorization ]]

      // WF Authorization [[
      $scope.showWfAuthorizationForm = function (index) {

        $scope.buffers.wfAuthorization.data = $scope.buffers.processAuthorization.data.groupAuthorizations[index];
        $scope.buffers.wfAuthorization.currentIndex = index;
        $scope.buffers.wfAuthorization.window.title($scope.buffers.wfAuthorization.data.groupKey);
        $scope.buffers.wfAuthorization.window.open();
        $scope.buffers.wfAuthorization.window.center();
      };
      // WF Authorization ]]

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.processAuthorization.window) {
          $scope.buffers.processAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.viewAuthorization.window) {
          $scope.buffers.viewAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.dashboardAuthorization.window) {
          $scope.buffers.dashboardAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.wfAuthorization.window) {
          $scope.buffers.wfAuthorization.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.buffers.orgNodeSelection.window) {
          $scope.buffers.orgNodeSelection.window = widget;
        }
      });
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.orgNodeStructureTree) {
          $scope.config.orgNodeStructureTree = widget;
          // $scope.treeInstance.expand(".k-item");
        }
      });
    }]);
