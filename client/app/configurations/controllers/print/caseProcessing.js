'use strict';

angular.module('cmisApp')
  .controller('CaseProcessingPrintController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'reportService', 'Loader',
    'SweetAlert', 'gettextCatalog', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, reportService, Loader,
              SweetAlert, gettextCatalog, helperFunctionsService) {

      var taskData = angular.fromJson(task.draft);
      var requestDate = kendo.parseDate(taskData.requestDate, 'dd-MM-yyyy HH:mm:ss');

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        accountStatementGrid: {},
        holdingReportGrid: {},
      };
      var showAccountStatementData = function (window, data, account) {
        if (data.data && data.data.length) {
          for (var i = 0; i < data.data.length; i++) {
            var obj = data.data[i];
            obj.operators = helperFunctionsService.convertObjectToArray(obj.operators);
          }
        }
        $scope.config.accountStatementGrid.options.dataSource = {
          data: data.data ? data.data : [],
          total: data.data ? data.data.length : 0,
          pageSize: 20,
          group: {field: "instrument.instrumentName"}
        };
        window.title(gettextCatalog.getString('Account Statement') + " - " + account.name);
        window.open();
        window.center();
      };
      var showHoldingReportData = function (window, data, instrument) {
        $scope.config.holdingReportGrid.options.dataSource = {
          data: data.data ? data.data : [],
          total: data.data ? data.data.length : 0,
          pageSize: 20
        };
        window.title(gettextCatalog.getString('Register') + " - " + instrument.instrumentName);
        window.open();
        window.center();

      };
      $scope.config.accountStatementGrid.options = {
        columns: [
          {
            field: "instrument.instrumentName",
            title: gettextCatalog.getString('Instrument'),
            width: "20rem",
            template: function (e) {
              return e.instrument.instrumentName + " | " + e.instrument.issuerName + " | " + e.instrument.ISIN;
            }

          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("ISIN"),
            width: "6rem"
          },
          {
            field: "instrument.issuerName",
            title: gettextCatalog.getString("Issuer"),
            width: "6rem"
          },
          {
            field: "ID",
            title: gettextCatalog.getString("Share ID"),
            width: "6rem"
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "6rem"
          },
          {
            field: "free",
            title: gettextCatalog.getString("Free"),
            width: "5rem",
            template: function (e) {
              if (e.free == "true") {
                return "<i class='glyphicon glyphicon-ok text-success'></i>";
              } else {
                return "<i class='glyphicon glyphicon-remove text-danger'></i>"
              }
            }
          },
          {
            field: "frozen",
            title: gettextCatalog.getString("Frozen"),
            width: "7rem",
            template: function (e) {
              if (e.frozen == "true") {
                return "<i class='glyphicon glyphicon-ok text-success'></i>";
              } else {
                return "<i class='glyphicon glyphicon-remove text-danger'></i>"
              }
            }
          },
          {
            field: "repo",
            title: gettextCatalog.getString("Repo"),
            width: "5rem",
            template: function (e) {
              if (e.repo == "true") {
                return "<i class='glyphicon glyphicon-ok text-success'></i>";
              } else {
                return "<i class='glyphicon glyphicon-remove text-danger'></i>"
              }
            }
          },
          {
            title: gettextCatalog.getString("Pledge"),
            columns: [
              {
                field: "pledged",
                width: "5rem",
                title: gettextCatalog.getString("Pledged"),
                template: function (e) {
                  if (e.pledged == "true") {
                    return "<i class='glyphicon glyphicon-ok text-success'></i>";
                  } else {
                    return "<i class='glyphicon glyphicon-remove text-danger'></i>"
                  }
                }
              },
              {
                field: "operators",
                title: gettextCatalog.getString("Pledgees"),
                width: "8rem",
                template: function (e) {
                  var t = "<ul class=\"list-no-style\">";
                  if (e.operators) {

                    for (var i = 0; i < e.operators.length; i++) {
                      t += "<li > " + e.operators[i] + "</li>";
                    }
                  }
                  t += "</ul>";
                  return t;
                }
              }
            ]
          },
          {
            title: gettextCatalog.getString("Blocks"),
            columns: [
              {
                field: "operationBlockQuantity",
                width: "6rem",
                title: gettextCatalog.getString("Operation")
              },
              {
                field: "orderBlockQuantity",
                width: "6rem",
                title: gettextCatalog.getString("Order")
              },
              {
                field: "tradeBlockQuantity",
                width: "6rem",
                title: gettextCatalog.getString("Trade")
              },
              {
                field: "settlementBlockQuantity",
                width: "6rem",
                title: gettextCatalog.getString("Settlement")
              }
            ]
          }
        ]
      };
      $scope.config.holdingReportGrid.options = {
        columns: [
          {
            field: "account.accountNumber",
            title: gettextCatalog.getString('Account No.'),
            width: "2rem"
          },
          {
            field: "account.name",
            title: gettextCatalog.getString("Person"),
            width: "2rem"
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "2rem"
          },

          {
            title: gettextCatalog.getString("Encumberence"),
            columns: [

              {
                field: "pledgeQuantity",
                width: "2rem",
                title: gettextCatalog.getString("Pledged"),
              },
              {
                field: "repoQuantity",
                title: gettextCatalog.getString("Repo"),
                width: "2rem"
              },
            ]
          },
          {
            field: "frozenQuantity",
            title: gettextCatalog.getString("Frozen"),
            width: "2rem"
          }
        ]
      };

      var loadedAccountStatements = {}, loadedHoldingReports = {};

      $scope.getAccountStatement = function (window, account) {
        if (account) {
          if (loadedAccountStatements.hasOwnProperty(account.accountId)) {
            showAccountStatementData(window, loadedAccountStatements[account.accountId], account);
          } else {
            Loader.show(true);
            reportService.getAccountStatement(account.accountId, requestDate).then(function (data) {
              if (data) {
                if (data.success == "true") {
                  loadedAccountStatements[account.accountId] = data;
                  showAccountStatementData(window, data, account);
                }
                else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }
              }
              Loader.show(false);
            });
          }
        }
      };

      $scope.getHoldingReport = function (window, instrument) {

        if (instrument.id) {

          if (loadedHoldingReports.hasOwnProperty(instrument.id)) {

            showHoldingReportData(window, loadedHoldingReports[instrument.id], instrument);

          } else {
            Loader.show(true);
            reportService.getHoldingReport(instrument.id, requestDate).then(function (data) {
              if (data) {
                if (data.success == "true") {

                  loadedHoldingReports[instrument.id] = data;
                  showHoldingReportData(window, data, instrument);
                }
                else {
                  SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }
              }
              Loader.show(false);
            });
          }
        }
      };

    }]);
