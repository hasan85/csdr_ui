'use strict';

angular.module('cmisApp').controller('ViewAccountConsolidationsSearchTemplateController', ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http', function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {

    var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        // if (formData.clientName || formData.brokerName || formData.finishDateStart || formData.finishDateFinish) {
        result.success = true;
        //}
        return result;
    };

    var _accountConsolidations = {
        formName: 'accountConsolidationsFindForm',
        form: {},
        data: {},
        searchResult: {
            data: null,
            isEmpty: false
        },
        selectedaccountConsolidationsIndex: false,
        findData: findData
    };

    $scope.accountConsolidations = _accountConsolidations;

    var findData = function (e) {

        if ($scope.accountConsolidations.searchResultGrid !== undefined) {
            $scope.accountConsolidations.searchResultGrid.refresh();
        }
        if ($scope.accountConsolidations.formName.$dirty) {
            $scope.accountConsolidations.formName.$submitted = true;
        }

        if ($scope.accountConsolidations.formName.$valid || !$scope.accountConsolidations.formName.$dirty) {

            $scope.accountConsolidations.searchResult.isEmpty = false;

            Loader.show(true);

            var formData = angular.copy($scope.accountConsolidations.form);

            var formValidationResult = {success: false};

            if ($scope.params.config.searchOnInit == true) {
                formValidationResult.success = true;
            } else {
                formValidationResult = validateForm(formData);
            }
            if (formValidationResult.success) {


                if ($scope.params.config.searchCriteria) {
                    formData = angular.merge(formData, $scope.params.config.searchCriteria);
                }
                formData.consolidatedDateStart = $scope.accountConsolidations.form.consolidatedDateStart ? $scope.accountConsolidations.form.consolidatedDateStartObj : null;
                formData.consolidatedDateFinish = $scope.accountConsolidations.form.consolidatedDateFinish ? $scope.accountConsolidations.form.consolidatedDateFinishObj : null;
                var requestData = angular.extend(formData, {
                    skip: e.data.skip,
                    take: e.data.take,
                    sort: e.data.sort
                });

                console.log('Request Data', requestData);
                Loader.show(true);
                $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
                    success(function (data) {

                        data.data = helperFunctionsService.convertObjectToArray(data.data);
                        if (data) {
                            $scope.accountConsolidations.searchResult.data = data;
                        } else {
                            $scope.accountConsolidations.searchResult.isEmpty = true;
                        }

                        console.log('Response data', data);
                        if (data['success'] === "true") {

                            e.success({
                                Data: data.data ? data.data : [], Total: data.total
                            });
                        } else {
                            SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                        }

                        Loader.show(false);
                    });


            } else {
                Loader.show(false);
                SweetAlert.swal("", formValidationResult.message, "error");
            }
        }
    };

    $scope.findData = function () {

        var formValidationResult = validateForm(angular.copy($scope.accountConsolidations.form));
        if (formValidationResult.success) {
            if ($scope.accountConsolidations.searchResultGrid) {
                $scope.accountConsolidations.searchResultGrid.dataSource.page(1);
            }
        }
        else {
            SweetAlert.swal("", formValidationResult.message, "error");
        }

    };

    var _params = {
        searchUrl: "/api/common/getAccountConsolidations/",
        showSearchCriteriaBlock: false,
        config: {
            searchOnInit: false
        }
    };

    $scope.params = angular.merge(_params, $scope.searchConfig.params);

    $scope.params.config.splitterPanesConfig = [
        {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
            size: '70%',
            collapsible: false
        }
    ];

    $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
        } else {
            splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
    };

    $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
        });
    };

    $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
        });
    };

    $scope.accountConsolidations.mainGridOptions = {
        dataSource: {
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    fields: {
                        consolidatedDate: {type: "date"}
                    }
                }
            },
            transport: {
                read: function (e) {
                    findData(e);
                }
            },

            serverPaging: true,
            serverSorting: true
        },
        selectable: false,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
            {
                title: gettextCatalog.getString("Old Account"),
                columns: [
                    {
                        field: "oldAccount.name",
                        title: gettextCatalog.getString("Name"),
                        width: "10rem",
                        attributes: { style: "white-space: normal"}
                    },
                    {
                        field: "oldAccount.accountNumber",
                        title: gettextCatalog.getString("Number"),
                        width: "7rem"
                    },
                    {
                        field: "oldAccount.addressLine",
                        title: gettextCatalog.getString("Address"),
                        width: "10rem",
                        attributes: { style: "white-space: normal"}
                    },
                    {
                        field: "oldAccount.identificator",
                        title: gettextCatalog.getString("Pin"),
                        width: "8rem"
                    }
                ]
            },
            {
                title: gettextCatalog.getString("New Account"),
                columns: [
                    {
                        field: "newAccount.name",
                        title: gettextCatalog.getString("Name"),
                        width: "10rem",
                        attributes: { style: "white-space: normal"}
                    },
                    {
                        field: "newAccount.accountNumber",
                        title: gettextCatalog.getString("Number"),
                        width: "7rem"
                    },
                    {
                        field: "newAccount.addressLine",
                        title: gettextCatalog.getString("Address"),
                        width: "10rem",
                        attributes: { style: "white-space: normal"}
                    },
                    {
                        field: "newAccount.identificator",
                        title: gettextCatalog.getString("Pin"),
                        width: "8rem"
                    }
                ]
            },
          {
            field: "consolidatedDate",
            title: gettextCatalog.getString("Consolidated Date"),
            width: '10rem',
              format: "{0:dd-MMMM-yyyy HH:mm:ss}",
            attributes: { style: "white-space: normal"}
          }
        ]
    };

    $scope.resetForm = function () {
        $scope.accountConsolidations.form.consolidatedDateStart = null;
        $scope.accountConsolidations.form.consolidatedDateFinish = null;
        $scope.accountConsolidations.form.oldAccountName = null;
        $scope.accountConsolidations.form.oldAccountNumber = null;
        $scope.accountConsolidations.form.newAccountName = null;
        $scope.accountConsolidations.form.newAccountNumber = null;
    };

}]);
