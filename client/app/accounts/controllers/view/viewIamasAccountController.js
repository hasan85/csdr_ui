'use strict';

angular.module("cmisApp")
  .controller('ViewIamasAccountController', ['$scope', 'personFindService', 'id', '$windowInstance', function ($scope, personFindService, id, $windowInstance) {
    $scope.config = {
      screenId: id,
      window: $windowInstance,
      mainGrid: {}
    };
  }]);
