'use strict';

angular.module('cmisApp')
  .controller('AccountPrescriptionsController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      $scope.searchConfig = {
        accountPrescription: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/persons/getAccountPrescriptions/',
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
