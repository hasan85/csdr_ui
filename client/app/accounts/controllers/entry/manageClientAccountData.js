'use strict';

angular.module('cmisApp').controller('ManageClientAccountDataController', ['$scope', '$windowInstance', 'id', 'task', 'appConstants', 'managePersonService', '$filter', 'gettext', 'gettextCatalog', 'operationState', 'Loader', 'personFindService', 'referenceDataService', 'instrumentFindService', 'SweetAlert', 'helperFunctionsService', 'dataSearchService', "operationService", "$http", "$timeout", function ($scope, $windowInstance, id, task, appConstants, managePersonService, $filter, gettext, gettextCatalog, operationState, Loader, personFindService, referenceDataService, instrumentFindService, SweetAlert, helperFunctionsService, dataSearchService, operationService, $http, $timeout) {
  window.$scope = $scope;
  $scope.metaData = {
    idDocumentTypes: []
  };
  var operationClasses = {
    systemOwnerDataChange: "system_owner_data_change_entry",
  };
  var idDocumentClassCodes = {
    uniqueCode: 'UNIQUE_CODE',
    idCardClassCode: "ID_CARD",
    TIN: "TIN",
    registerCertificate: 'REGISTER_CERTIFICATE'
  };
  var roleClassConstants = {
    tradingMember: "ROLE_TRADING_MEMBER",
    depoMember: "ROLE_DEPO_MEMBER"
  };
  var itemTemplate = {
    instrument: {
      id: null,
      ISIN: null,
      issuerName: null,
    },
    allInstrumentSelected: true,
    hasRestrictionToQuantity: false,
    quantity: null,
  };


  // Initialize variables
  var npConfig = {};
  var jpConfig = {};
  var npRepresentativeConfig = {};
  var jpRepresentativeConfig = {};

  var npRepresentativeConfigUpdate = {};
  var jpRepresentativeConfigUpdate = {};

  var userConfig = {};
  var userConfigUpdate = {};

  var personClasses = {};

  // Shareholder form template
  var shareholderFormDataTemplate = {
    isRepresentative: false,
    isSigner: false,
    isUser: false,
    documents: [],
    bankAccounts: [],
    phoneNumbers: [
      {
        type: null,
        number: ''
      }
    ],
    pinCode: {},
    TIN: {},
    idCard: {},
    registrationCertificate: {},
    emails: [
      {value: ''}
    ],
    actualAddress: null,
    legalAddress: null,
    representatives: [],
    signers: [],
    users: []
  };
  var resolveActionClasses = function (draft) {
    var roleCodes = helperFunctionsService.convertObjectToArray(draft.broker.roles);
    var hasDepoMemberRole = false, hasTradingMemberRole = false;
    if (roleCodes) {
      for (var i = 0; i < roleCodes.length; i++) {
        if (roleCodes[i].code == roleClassConstants.depoMember) {
          hasDepoMemberRole = true;
        }
        if (roleCodes[i].code == roleClassConstants.tradingMember) {
          hasTradingMemberRole = true;
        }
      }
    }
    $scope.metaData.actionClasses = [];
    if (hasTradingMemberRole) {
      $scope.metaData.actionClasses = $scope.metaData.actionClasses.concat(draft.tradingMemberActionClasses)
    }
    if (hasDepoMemberRole) {
      $scope.metaData.actionClasses = $scope.metaData.actionClasses.concat(draft.depoMemberActionClasses)
    }
  };
  var mainPayload = angular.fromJson(task.draft);


  function resetShareholder() {
    // Shareholder model
    $scope.shareholder = {
      data: {
        form: angular.copy(shareholderFormDataTemplate),
        config: {
          formName: 'npMainForm',
          prefix: 'npMain',
          documentFormName: 'npDocumentForm',
          bankAccountFormName: 'npBankAccountForm',
          fields: {},
          test: {},
          isShareholder: true,
          isTINCodeMandatory: false
        },
        documentAddWindow: {},
        bankAccountAddWindow: {},
        personClasses: {}
      }
    };
  }

  resetShareholder();
  
  var payload = angular.fromJson(task.draft).personViewModel.dataEntry;

  var setConfigurations = function () {
    $scope.isInsert = payload.isInsert;

    $scope.shareholder.data.form.naturalPersonConfiguration = payload['naturalPersonConfiguration'];
    $scope.shareholder.data.form.juridicalPersonConfiguration = payload['juridicalPersonConfiguration'];
    $scope.shareholder.data.form.naturalRepresentativeInsertConfiguration = payload['naturalRepresentativeInsertConfiguration'];
    $scope.shareholder.data.form.juridicalRepresentativeInsertConfiguration = payload['juridicalRepresentativeInsertConfiguration'];
    $scope.shareholder.data.form.naturalRepresentativeUpdateConfiguration = payload['naturalRepresentativeUpdateConfiguration'];
    $scope.shareholder.data.form.juridicalRepresentativeUpdateConfiguration = payload['juridicalRepresentativeUpdateConfiguration'];
    $scope.shareholder.data.form.userInsertConfiguration = payload['userInsertConfiguration'];
    $scope.shareholder.data.form.userUpdateConfiguration = payload['userUpdateConfiguration'];
  };

  $timeout(function () {
    setConfigurations();
  }, 5000);

  //watch address fields to update plain adress on change
  $scope.$watch("shareholder.data.form.legalAddress", function (newValue) {
    if (newValue) {
      $scope.legalPlainAddressAZ = [];
      $scope.legalPlainAddressEN = [];
      if (newValue.stateAz) {
        $scope.legalPlainAddressAZ.push(newValue.stateAz);
      }
      if (newValue.locationsAz) {
        $scope.legalPlainAddressAZ.push(newValue.locationsAz)
      }
      if (newValue.addressLineAz) {
        $scope.legalPlainAddressAZ.push(newValue.addressLineAz)
      }
      $scope.legalPlainAddressAZ = $scope.legalPlainAddressAZ.join(", ");

      if (!$scope.legalPlainAddressAZ.length && newValue.plainAddressAz && newValue.plainAddressAz.length) {
        $scope.legalPlainAddressAZ = newValue.plainAddressAz
      }

      if (newValue.stateEn) {
        $scope.legalPlainAddressEN.push(newValue.stateEn);
      }
      if (newValue.locationsEn) {
        $scope.legalPlainAddressEN.push(newValue.locationsEn)
      }
      if (newValue.addressLineEn) {
        $scope.legalPlainAddressEN.push(newValue.addressLineEn)
      }

      $scope.legalPlainAddressEN = $scope.legalPlainAddressEN.join(", ");

      if (!$scope.legalPlainAddressEN.length && newValue.plainAddressEn && newValue.plainAddressEn.length) {
        $scope.legalPlainAddressEN = newValue.plainAddressEn
      }

    }

  }, true);


  $scope.$watch("shareholder.data.form.actualAddress", function (newValue) {
    if (newValue) {
      $scope.actualPlainAddressAZ = [];
      $scope.actualPlainAddressEN = [];
      if (newValue.stateAz) {
        $scope.actualPlainAddressAZ.push(newValue.stateAz);
      }
      if (newValue.locationsAz) {
        $scope.actualPlainAddressAZ.push(newValue.locationsAz)
      }
      if (newValue.addressLineAz) {
        $scope.actualPlainAddressAZ.push(newValue.addressLineAz)
      }

      $scope.actualPlainAddressAZ = $scope.actualPlainAddressAZ.join(", ");

      if (newValue.stateEn) {
        $scope.actualPlainAddressEN.push(newValue.stateEn);
      }
      if (newValue.locationsEn) {
        $scope.actualPlainAddressEN.push(newValue.locationsEn)
      }
      if (newValue.addressLineEn) {
        $scope.actualPlainAddressEN.push(newValue.addressLineEn)
      }

      $scope.actualPlainAddressEN = $scope.actualPlainAddressEN.join(", ");
    }

  }, true);


  $scope.$watch("buffers.representative.data.form.legalAddress", function (newValue) {
    if (newValue) {
      $scope._rep_legalPlainAddressAZ = [];
      $scope._rep_legalPlainAddressEN = [];
      if (newValue.stateAz) {
        $scope._rep_legalPlainAddressAZ.push(newValue.stateAz);
      }
      if (newValue.locationsAz) {
        $scope._rep_legalPlainAddressAZ.push(newValue.locationsAz)
      }
      if (newValue.addressLineAz) {
        $scope._rep_legalPlainAddressAZ.push(newValue.addressLineAz)
      }

      $scope._rep_legalPlainAddressAZ = $scope._rep_legalPlainAddressAZ.join(", ");

      if (newValue.stateEn) {
        $scope._rep_legalPlainAddressEN.push(newValue.stateEn);
      }
      if (newValue.locationsEn) {
        $scope._rep_legalPlainAddressEN.push(newValue.locationsEn)
      }
      if (newValue.addressLineEn) {
        $scope._rep_legalPlainAddressEN.push(newValue.addressLineEn)
      }

      $scope._rep_legalPlainAddressEN = $scope._rep_legalPlainAddressEN.join(", ");
    }

  }, true);
  $scope.$watch("buffers.representative.data.form.actualAddress", function (newValue) {
    if (newValue) {
      $scope._rep_actualPlainAddressAZ = [];
      $scope._rep_actualPlainAddressEN = [];
      if (newValue.stateAz) {
        $scope._rep_actualPlainAddressAZ.push(newValue.stateAz);
      }
      if (newValue.locationsAz) {
        $scope._rep_actualPlainAddressAZ.push(newValue.locationsAz)
      }
      if (newValue.addressLineAz) {
        $scope._rep_actualPlainAddressAZ.push(newValue.addressLineAz)
      }

      $scope._rep_actualPlainAddressAZ = $scope._rep_actualPlainAddressAZ.join(", ");

      if (newValue.stateEn) {
        $scope._rep_actualPlainAddressEN.push(newValue.stateEn);
      }
      if (newValue.locationsEn) {
        $scope._rep_actualPlainAddressEN.push(newValue.locationsEn)
      }
      if (newValue.addressLineEn) {
        $scope._rep_actualPlainAddressEN.push(newValue.addressLineEn)
      }

      $scope._rep_actualPlainAddressEN = $scope._rep_actualPlainAddressEN.join(", ");
    }

  }, true);

  $scope.getFormBuffer = function () {
    return managePersonService.prepareFormData(angular.copy($scope.shareholder.data.form), angular.copy($scope.metaData));
  };


  $scope.validateForm = function () {
    var success = true;
    if ($scope.config.isUpdateOperation && !($scope.shareholder.data.form.ID > 0)) {
      SweetAlert.swal('', gettextCatalog.getString('You have to select account for changing!'), 'error');
      success = false;
    }
    else {
      $scope.shareholder.data.config.formName.$submitted = true;


      if ($scope.shareholder.data.config.formName.$valid && !$scope.config.duplicateUserDetected) {
        if ($scope.config.userAccountEnabled) {
          $scope.buffers.user.data.config.formName.$submitted = true;
          if ($scope.buffers.user.data.config.formName.$valid) {
            success = true;
          }
        } else {
          success = true;
        }
      } else if ($scope.config.duplicateUserDetected) {
        SweetAlert.swal('', gettextCatalog.getString('', 'Duplicate person!.', 'error'));
        success = false;
      } else {
        SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
        success = false;
      }
    }
    return success;
  };
  var generateAccountOpeningDocumentsURL = "/api/common/generateAccountOpeningDocuments";
  $scope.generatedDocuments = [];
  $scope.generateAccountOpeningDocuments = function () {
    var isFormValid = $scope.validateForm();
    if (isFormValid) {
      Loader.show(true);
      $http.post(
        generateAccountOpeningDocumentsURL,
        $scope.getFormBuffer()
      ).then(function (response) {
        Loader.show(false);
        if (response.data.success == "true") {
          var documentsData = response.data.data;
          angular.forEach(documentsData, function (value, key) {

            var label = "";
            if (key == "brokerInvestorContractData") {
              label = "Müqavilə"
            }
            if (key == "naturalPersonFatcaFormData") {
              label = "FATCA"
            }
            if (key == "personFormData") {
              label = "Anket"
            }

            $scope.generatedDocuments.push({
              name: label,
              file: value
            });
          });
        }
      });
    }
  };
  $scope.downloadDocument = function (base64FileURI) {
    window.open("data:application/pdf;base64, " + base64FileURI);
  };

  $scope.jurisdictionCountryChange = function (person) {
    $scope.isTINCodeMandatory = false;
    var model = person.form.jurisdictionCountry ? angular.fromJson(person.form.jurisdictionCountry) : null;
    if (model) {
      $scope.isTINCodeMandatory = model.code == "AZ";
    }
  };
  $scope.currencyChange = function (bankAccountBuffer) {
    var model = bankAccountBuffer.form.currency ? angular.fromJson(bankAccountBuffer.form.currency) : null;
    if (model) {
      bankAccountBuffer.isBankCodeMandatory = model.code == "AZN";
    }
  };
  // set pin code mandatory based on country
  $scope.citizenshipCountryChange = function (model) {
    $scope.isPinCodeMandatory = false;
    $scope.isIDCardSeriesMandatory = false;
    $scope.isIDCardNumberMandatory = false;
    console.log(model);
    model = angular.fromJson(model);
    if (model) {
      $scope.isPinCodeMandatory = model.code == "AZ";
      $scope.isIDCardSeriesMandatory = model.code == "AZ";
      $scope.isIDCardNumberMandatory = model.code == "AZ";
    }
  };

  // Initialize scope variables [[
  $scope.config = {
    screenId: id,
    taskKey: id,
    task: task,
    operationType: appConstants.operationTypes.entry,
    form: {
      name: "shareholderDataForm",
      data: {}
    },
    state: operationState,
    buttons: {
      complete: {
        click: function () {

          if ($scope.config.isUpdateOperation && !($scope.shareholder.data.form.ID > 0)) {
            SweetAlert.swal('', gettextCatalog.getString('You have to select account for changing!'), 'error');
          }
          else {
            $scope.shareholder.data.config.formName.$submitted = true;
            if ($scope.shareholder.data.config.formName.$valid && !$scope.config.duplicateUserDetected) {

              if ($scope.config.userAccountEnabled) {
                $scope.buffers.user.data.config.formName.$submitted = true;

                if ($scope.buffers.user.data.config.formName.$valid) {

                  $scope.shareholder.data.form.userData = angular.copy($scope.buffers.user.data.form);
                  $scope.shareholder.data.form.userData.isUser = true;
                  var formBuf = managePersonService.prepareFormData(angular.copy($scope.shareholder.data.form), angular.copy($scope.metaData));

                  console.log("draft on completeTask", {
                    personViewModel: {
                      dataEntry: $scope.shareholder.data.form
                    }
                  });
                  $scope.config.completeTask({
                    personViewModel: {dataEntry: formBuf},
                      brokerAccount: angular.fromJson(task.draft).brokerAccount
                  });
                }
              } else {
                var buf = angular.copy($scope.shareholder.data.form);
                buf.userData = null;

                console.log("draft on completeTask", {
                  personViewModel: {
                    dataEntry: $scope.shareholder.data.form
                  },
                    brokerAccount: angular.fromJson(task.draft).brokerAccount,
                });

                $scope.config.completeTask({
                  personViewModel: {
                    dataEntry: managePersonService.prepareFormData(buf, angular.copy($scope.metaData))
                  },
                    brokerAccount: angular.fromJson(task.draft).brokerAccount,
                });
              }

            } else if ($scope.config.duplicateUserDetected) {
              SweetAlert.swal('', gettextCatalog.getString('', 'Duplicate person!.', 'error'));
            } else {
              SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
            }
          }
        }
      }
    },
    window: $windowInstance,
    labels: {
      lblSelectRoles: gettextCatalog.getString('Select Roles'),
      actualAddress: gettextCatalog.getString('Actual Address'),
      legalAddress: gettextCatalog.getString('Legal Address'),
      accountInformation: gettextCatalog.getString('Account Information'),
      administratorInformation: gettextCatalog.getString('Administrator Information'),
    },
    userAccountEnabled: false,
    isUpdateOperation: task.key.indexOf('_data_change') > -1 ? true : false,
    isSystemOwnerDataChangeOperation: task.key == operationClasses.systemOwnerDataChange ? true : false,
    duplicateUserDetected: false
  };

  // Buffers
  $scope.buffers = angular.copy(managePersonService.getPersonDataEntryFormBuffers());


  // Check if form is dirty
  $scope.$on('closeTask', function () {
    $scope.config.showTaskSavePrompt(false);
  });
  // Listen task save event
  $scope.$on('saveTask', function () {
    Loader.show(true);
    setConfigurations();
    // $scope.config.saveTask({
    //   dataEntry: $scope.shareholder.data.form
    // });
    var res = managePersonService.prepareFormData(angular.copy($scope.shareholder.data.form), angular.copy($scope.metaData));
    res = angular.merge({}, $scope.shareholder.data.form, res);
    console.log(res);
    $scope.config.saveTask({
      personViewModel: {
        dataEntry: res
      },
      brokerAccount: angular.fromJson(task.draft).brokerAccount
    });

    Loader.show(false);
  });


  //Get metadata
  Loader.show(true);

  function _updateConfiguration(payload) {
    $scope.shareholder.data.form.naturalPersonConfiguration = payload['naturalPersonConfiguration'];
    $scope.shareholder.data.form.juridicalPersonConfiguration = payload['juridicalPersonConfiguration'];
    $scope.shareholder.data.form.naturalRepresentativeInsertConfiguration = payload['naturalRepresentativeInsertConfiguration'];
    $scope.shareholder.data.form.juridicalRepresentativeInsertConfiguration = payload['juridicalRepresentativeInsertConfiguration'];
    $scope.shareholder.data.form.naturalRepresentativeUpdateConfiguration = payload['naturalRepresentativeUpdateConfiguration'];
    $scope.shareholder.data.form.juridicalRepresentativeUpdateConfiguration = payload['juridicalRepresentativeUpdateConfiguration'];
    $scope.shareholder.data.form.userInsertConfiguration = payload['userInsertConfiguration'];
    $scope.shareholder.data.form.userUpdateConfiguration = payload['userUpdateConfiguration'];


    // Prepare config data [[[
    var npConfigRaw = payload['naturalPersonConfiguration'] ?
      angular.fromJson(payload['naturalPersonConfiguration']) : null;

    var jpConfigRaw = payload['juridicalPersonConfiguration'] ?
      angular.fromJson(payload['juridicalPersonConfiguration']) : null;

    var npRepresentativeConfigRaw = payload['naturalRepresentativeInsertConfiguration'] ?
      angular.fromJson(payload['naturalRepresentativeInsertConfiguration']) : null;

    var jpRepresentativeConfigRaw = payload['juridicalRepresentativeInsertConfiguration'] ?
      angular.fromJson(payload['juridicalRepresentativeInsertConfiguration']) : null;

    var npRepresentativeConfigUpdateRaw = payload['naturalRepresentativeUpdateConfiguration'] ?
      angular.fromJson(payload['naturalRepresentativeUpdateConfiguration']) : null;
    var jpRepresentativeConfigUpdateRaw = payload['juridicalRepresentativeUpdateConfiguration'] ?
      angular.fromJson(payload['juridicalRepresentativeUpdateConfiguration']) : null;

    var userConfigRaw = payload['userInsertConfiguration'] ? angular.fromJson(payload['userInsertConfiguration']) : null;
    var userConfigUpdateRaw = payload['userUpdateConfiguration'] ? angular.fromJson(payload['userUpdateConfiguration']) : null;

    npConfig = npConfigRaw ? managePersonService.generateFormConfig(npConfigRaw) : null;
    jpConfig = jpConfigRaw ? managePersonService.generateFormConfig(jpConfigRaw) : null;
    npRepresentativeConfig = npRepresentativeConfigRaw ? managePersonService.generateFormConfig(npRepresentativeConfigRaw) : null;
    jpRepresentativeConfig = jpRepresentativeConfigRaw ? managePersonService.generateFormConfig(jpRepresentativeConfigRaw) : null;
    npRepresentativeConfigUpdate = npRepresentativeConfigUpdateRaw ? managePersonService.generateFormConfig(npRepresentativeConfigUpdateRaw) : null;
    jpRepresentativeConfigUpdate = jpRepresentativeConfigUpdateRaw ? managePersonService.generateFormConfig(jpRepresentativeConfigUpdateRaw) : null;

    userConfig = userConfigRaw ? managePersonService.generateFormConfig(userConfigRaw) : null;
    userConfigUpdate = userConfigUpdateRaw ? managePersonService.generateFormConfig(userConfigUpdateRaw) : null;
    // Prepare config data ]]]

    // Normalize person type classes [[[
    for (var i = 0; i < $scope.personClasses.length; i++) {
      if ($scope.personClasses[i].code === appConstants.personClasses.juridicalPerson) {
        personClasses['juridical'] = $scope.personClasses[i];
        personClasses['juridical']['indexInObject'] = i;

      } else {
        personClasses['natural'] = $scope.personClasses[i];
        personClasses['natural']['indexInObject'] = i;
      }
    }
    // Normalize person type classes ]]]


    // Set Default Person Type [[[
    var personType = angular.fromJson(payload.personType);
    if (payload.isNaturalPersonEnabled || (personType && personType.code == appConstants.personClasses.naturalPerson)) {
      $scope.shareholder.data.form.personType = $filter('json')($scope.personClasses[personClasses.natural.indexInObject]);
      $scope.shareholder.data.config.fields = npConfig;
      $scope.metaData.idDocumentTypes = $scope.metaData.naturalPersonIdDocumentTypes;
    }
    else {
      $scope.shareholder.data.form.personType = $filter('json')($scope.personClasses[personClasses.juridical.indexInObject]);
      $scope.shareholder.data.config.fields = jpConfig;
      $scope.metaData.idDocumentTypes = $scope.metaData.juridicalPersonIdDocumentTypes;
    }


    if (payload.isNaturalRepresentativeEnabled) {
      $scope.buffers.representative.data.config.fields = npRepresentativeConfig;
      $scope.buffers.representative.data.form.personType = $filter('json')($scope.personClasses[personClasses.natural.indexInObject]);
    }
    else {
      $scope.buffers.representative.data.config.fields = jpRepresentativeConfig;
      $scope.buffers.representative.data.form.personType = $filter('json')($scope.personClasses[personClasses.juridical.indexInObject]);
    }
    // Set Default Person Type ]]]

    $scope.shareholder.data.config.currentRole = $scope.shareholder.data.form.currentRole = payload.currentRole;
    $scope.shareholder.data.config.accountClassCode = $scope.shareholder.data.form.accountClassCode = payload.accountClassCode;
    $scope.shareholder.data.config.isUserEnabled = $scope.shareholder.data.form.isUserEnabled = payload.isUserEnabled;
    $scope.shareholder.data.config.isNaturalPersonEnabled = $scope.shareholder.data.form.isNaturalPersonEnabled = payload.isNaturalPersonEnabled;
    $scope.shareholder.data.config.isJuridicalPersonEnabled = $scope.shareholder.data.form.isJuridicalPersonEnabled = payload.isJuridicalPersonEnabled;
    $scope.shareholder.data.config.isRepresentativeEnabled = $scope.shareholder.data.form.isRepresentativeEnabled = payload.isRepresentativeEnabled;
    $scope.shareholder.data.config.isNaturalRepresentativeEnabled = $scope.shareholder.data.form.isNaturalRepresentativeEnabled = payload.isNaturalRepresentativeEnabled;
    $scope.shareholder.data.config.isJuridicalRepresentativeEnabled = $scope.shareholder.data.form.isJuridicalRepresentativeEnabled = payload.isJuridicalRepresentativeEnabled;

    $scope.buffers.representative.data.config.isNaturalPersonEnabled = payload.isNaturalRepresentativeEnabled;
    $scope.buffers.representative.data.config.isJuridicalPersonEnabled = payload.isJuridicalRepresentativeEnabled;

    $scope.shareholder.data.config.isDocumentGenerationEnabled = $scope.shareholder.data.form.isDocumentGenerationEnabled = payload.isDocumentGenerationEnabled;
    $scope.shareholder.data.config.isIAMASEnabled = $scope.shareholder.data.form.isIAMASEnabled = payload.isIAMASEnabled;

    if ($scope.buffers.user.data.form.ID > 0) {
      $scope.buffers.user.data.config.fields = userConfigUpdate;
    } else {
      $scope.buffers.user.data.config.fields = userConfig;
    }

    $scope.config.tabs = [{label: gettextCatalog.getString('Account Information')}];

    if ($scope.shareholder.data.config.isUserEnabled) {
      $scope.config.userAccountEnabled = true;
      $scope.config.tabs.push({
        label: gettextCatalog.getString('Administrator Information')
      });
    }


    // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
          $scope.shareholder.data.form = mainPayload.personViewModel.dataEntry;
      }
      if ($scope.config.isSystemOwnerDataChangeOperation || (payload.personType && payload.personType.id)) {
          $scope.updatePersonData($scope.shareholder.data, personFindService.normalizePersonArrayFields(payload), $scope.metaData);
      }


    if (!$scope.shareholder.data.form.phoneNumbers || (angular.isArray($scope.shareholder.data.form.phoneNumbers) && $scope.shareholder.data.form.phoneNumbers.length == 0)) {
      $scope.shareholder.data.form.phoneNumbers = [{
        type: null,
        number: ""
      }];
    }


  }


  managePersonService.getMetaData().then(function (data) {

    // DETERMINE DOCUMENT TYPES FOR PERSON [[[
    var naturalPersonIdDocumentTypes = juridicalPersonIdDocumentTypes = [];
    if (data.naturalPersonIdDocumentTypes) {
      angular.forEach(data.naturalPersonIdDocumentTypes, function (value) {
        if (value['code'] != idDocumentClassCodes.idCardClassCode) {
          naturalPersonIdDocumentTypes.push(value);
        }
      });
    }
    var juridicalPersonIdDocumentTypes = [];
    if (data.juridicalPersonIdDocumentTypes) {
      angular.forEach(data.juridicalPersonIdDocumentTypes, function (value) {
        if (value['code'] != idDocumentClassCodes.TIN && value['code'] != idDocumentClassCodes.registerCertificate) {
          juridicalPersonIdDocumentTypes.push(value);
        }
      });
    }


    // Bind metadata [[[
    $scope.metaData = {
      phoneNumberTypes: data.phoneNumberTypes,
      allIdDocumentClasses: angular.extend(data.juridicalPersonIdDocumentTypes, data.naturalPersonIdDocumentTypes),
      countries: data.countries,
      currencies: data.currencies,
      userPositions: data.userPositions,
      signerPositions: data.signerPositions,
      personClasses: data.personClasses,
      businessClasses: data.businessClasses,
      legalFormClasses: data.legalFormClasses,
      naturalPersonIdDocumentTypes: naturalPersonIdDocumentTypes,
      juridicalPersonIdDocumentTypes: juridicalPersonIdDocumentTypes,
      accountStatusCheckingClasses: helperFunctionsService.convertArrayToObjectByKey(
        data.accountStatusCheckingClasses, 'code')
    };


    var payload = angular.fromJson(task.draft).personViewModel.dataEntry;
    // var payload = managePersonService.saveViewModel(angular.fromJson(task.draft).personViewModel.dataEntry, $scope.metaData, true);
    $scope.isInsert = payload.isInsert;

    $scope.personClasses = data.personClasses;

    _updateConfiguration(payload);

    Loader.show(false);

  });

  // Change person type
  $scope.changePersonType = function (personType, isRepresentative, personId) {

    var personClassCode = angular.fromJson(personType)['code'];
    if (personClassCode == appConstants.personClasses.naturalPerson) {
      if (isRepresentative) {
        if (personId > 0) {
          $scope.buffers.representative.data.config.fields = npRepresentativeConfigUpdate;
        } else {
          $scope.buffers.representative.data.config.fields = npRepresentativeConfig;
        }
      }
      else {
        $scope.shareholder.data.config.fields = npConfig;
      }
      $scope.metaData.idDocumentTypes = $scope.metaData.naturalPersonIdDocumentTypes;
    }
    else {
      if (isRepresentative) {
        if (personId > 0) {
          $scope.buffers.representative.data.config.fields = jpRepresentativeConfigUpdate;
        } else {
          $scope.buffers.representative.data.config.fields = jpRepresentativeConfig;
        }
      }
      else {
        $scope.shareholder.data.config.fields = jpConfig;
      }
      $scope.metaData.idDocumentTypes = $scope.metaData.juridicalPersonIdDocumentTypes;
    }
  };

  // Reset form
  $scope.resetForm = function () {
    var personType = $scope.shareholder.data.form.personType;
    $scope.shareholder.data.form = angular.copy(managePersonService.getFormModel());
    $scope.buffers.user.data.form = angular.copy(managePersonService.getFormModel());
    $scope.shareholder.data.form.personType = personType;
  };

  //Manage Person Documents
  $scope.addNewDocument = function (window, model, form) {

    managePersonService.addNewDocument(window, model, form, $scope.buffers.document);
  };
  $scope.removeDocument = function (index, model) {
    managePersonService.removeFromArray(index, model);
  };
  $scope.showDocumentUpdateForm = function (index, model, window) {
    managePersonService.showDocumentUpdateForm(index, model, window, $scope.buffers.document);
  };
  $scope.updateDocument = function (window, model, form) {
    managePersonService.updateDocument(window, model, form, $scope.buffers.document);
  };
  $scope.resetDocumentBuffer = function (window, form) {
    managePersonService.resetDocumentBuffer(window, form, $scope.buffers.document);
  };

  //Manage Person Bank Accounts
  $scope.addNewBankAccount = function (window, model, form) {
    managePersonService.addNewBankAccount(window, model, form, $scope.buffers.bankAccount);
  };
  $scope.removeBankAccount = function (index, model) {
    managePersonService.removeFromArray(index, model);
  };
  $scope.showBankAccountUpdateForm = function (index, model, window) {

    managePersonService.showBankAccountUpdateForm(index, model, window, $scope.buffers.bankAccount);
  };
  $scope.updateBankAccount = function (window, model, form) {
    managePersonService.updateBankAccount(window, model, form, $scope.buffers.bankAccount);
  };
  $scope.resetBankAccountBuffer = function (window, form) {

    managePersonService.resetBankAccountBuffer(window, form, $scope.buffers.bankAccount);
  };

  //Manage Person Phone Numbers
  $scope.addNewPhoneNumber = function (model) {
    managePersonService.addNewPhoneNumber(model);
  };
  $scope.removePhoneNumber = function (index, model) {
    managePersonService.removeFromArray(index, model);
  };

  //Manage Person Emails
  $scope.addNewEmail = function (model) {
    managePersonService.addNewEmail(model)
  };
  $scope.removeEmail = function (index, model) {
    managePersonService.removeFromArray(index, model);
  };

  //Manage Representatives
  $scope.addNewRepresentative = function (model, window, isSigner) {
    $scope.buffers.representative.data.config.formName.$submitted = true;
    if ($scope.buffers.representative.data.config.formName.$valid) {
      window.close();
      if (angular.fromJson(model.personType).code === appConstants.personClasses.naturalPerson) {
        model.name = {
          nameAz: managePersonService.generateFullNameOfPerson(model)
        };
      }
      if (isSigner) {
        $scope.buffers.representative.data.config.fields = npRepresentativeConfig;
        $scope.shareholder.data.form.signers.push(model);

      } else {
        $scope.shareholder.data.form.representatives.push(model);
      }

      $scope.buffers.representative.data.config.formName.$setPristine();
      $scope.buffers.representative.data.config.formName.$setUntouched();
    }

  };
  $scope.showRepresentativeUpdateForm = function (index, model, window, isSigner) {
    window.title(gettextCatalog.getString('Update Representative'));
    $scope.buffers.representative.data.form = angular.copy(model[index]);
    $scope.buffers.representative.data.currentIndex = index;
    if (angular.fromJson(model[index].personType).code === appConstants.personClasses.juridicalPerson) {
      if (model[index].ID > 0) {
        $scope.buffers.representative.data.config.fields = jpRepresentativeConfigUpdate;
      } else {
        $scope.buffers.representative.data.config.fields = jpRepresentativeConfig;
      }
    } else {

      if (model[index].ID > 0) {
        $scope.buffers.representative.data.config.fields = npRepresentativeConfigUpdate;
      } else {
        $scope.buffers.representative.data.config.fields = npRepresentativeConfig;
      }
    }
    if (isSigner) {
      $scope.buffers.representative.data.config.fields = npRepresentativeConfig;
      $scope.buffers.representative.data.config.isSignerForm = true;
    } else {
      $scope.buffers.representative.data.config.isSignerForm = false;
    }
    window.open();
    window.center();
  };

  $scope.showRepresentativeAddForm = function (window, isSigner) {
    window.open();
    window.center();

    if (isSigner) {
      $scope.buffers.representative.data.config.isSignerForm = true;
    } else {
      $scope.buffers.representative.data.config.isSignerForm = false;
    }

    $scope.buffers.representative.data.config.disableAll = false;
  };


  $scope.updateRepresentative = function (model, window) {

    $scope.buffers.representative.data.config.formName.$submitted = true;
    if ($scope.buffers.representative.data.config.formName.$valid) {


      var representative = angular.copy($scope.buffers.representative.data.form);
      if (angular.fromJson(representative.personType).code === appConstants.personClasses.naturalPerson) {
        representative.name = {
          nameAz: managePersonService.generateFullNameOfNaturalPerson(representative)
        };
      }
      var index = $scope.buffers.representative.data.currentIndex;

      model[index] = representative;
      $scope.resetRepresentativesBuffer(window);
      window.close();
    }

  };
  $scope.removeRepresentative = function (index, model) {
    model.splice(index, 1);
  };
  $scope.resetRepresentativesBuffer = function (window) {

    window.title(gettextCatalog.getString('Add New Representative'));
    $scope.buffers.representative.data.currentIndex = -1;
    $scope.buffers.representative.data.config.fields = npRepresentativeConfig;
    var personType = $scope.buffers.representative.data.form.personType;
    $scope.buffers.representative.data.form = {
      isRepresentative: true,
      personType: $filter('json')($scope.metaData.personClasses[personClasses.natural.indexInObject]),
      documents: [],
      bankAccounts: [],
      phoneNumbers: [
        {
          type: null,
          number: ''
        }
      ],
      emails: [
        {
          value: ''
        }
      ]
    };
    $scope.buffers.representative.data.config.formName.$setPristine();
    $scope.buffers.representative.data.config.formName.$setUntouched();
  };

  var jurisdictionCountryConditions = {
    isAZ: "Jurisdiction == AZ",
    notAZ: "Jurisdiction != AZ"
  };
  $scope.$watch("shareholder.data.form.jurisdictionCountry", function (newVal) {
    if (newVal) {
      var country = angular.fromJson(newVal);
      var countryCode = country.code;
      var condition = countryCode == "AZ" ? jurisdictionCountryConditions.isAZ : jurisdictionCountryConditions.notAZ;
      angular.forEach(task.document, function (document) {
        if (document.condition) {
          var documents = document.documents;
          document.disabled = document.condition.toLowerCase() != condition.toLowerCase();
          if (document.disabled == true) {
            angular.forEach(documents, function (item, $index) {
              $scope._removeDocumentOnCountryChange(documents, $index, item.ID);
            });
          }
        }
      });
    }
  });

  $scope._removeDocumentOnCountryChange = function (model, $index, id) {
    var swalOptions = {
      text: gettextCatalog.getString('Uploaded documents will be deleted if you change country')
    };
    $scope._removeDocument(model, $index, id, swalOptions);
  };

  $scope._removeDocument = function (model, $index, id, swalOptions) {
    swalOptions = angular.extend({}, {
      title: gettextCatalog.getString('Warning'),
      text: gettextCatalog.getString('Are you sure to delete this document?'),
      type: "warning",
      showCancelButton: false,
      confirmButtonText: gettextCatalog.getString('Yes'),
      cancelButtonText: gettextCatalog.getString('Cancel'),
      closeOnConfirm: true,
      closeOnCancel: true
    }, swalOptions);
    SweetAlert.swal(swalOptions,
      function (isConfirm) {
        if (isConfirm) {
          operationService.removeDocument(id).then(function (res) {
            if (res.success == "true") {
              model.splice($index, 1);
            }
            else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
            }
          });
        }
      });
  };

  //Watched variables
  $scope.$watch("buffers.document.currentIndex", function (newVal) {
    $scope.config.documentManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Document') : gettextCatalog.getString('Add New Document');

  });
  $scope.$watch("buffers.bankAccount.currentIndex", function (newVal) {
    $scope.config.bankAccountManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Bank Account') : gettextCatalog.getString('Add New Bank Account');

  });
  $scope.$watch("buffers.user.data.currentIndex", function (newVal) {
    $scope.config.userManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update User') : gettextCatalog.getString('Add New User');

  });
  $scope.$watch("buffers.representative.data.currentIndex", function (newVal) {
    $scope.config.representativeManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Representative') : gettextCatalog.getString('Add New Representative');
  });

  $scope.updatePersonData = function (oldData, newData) {
    Loader.show(false);

    oldData.form = managePersonService.convertViewModelToForm(newData, $scope.metaData);

    if (oldData.form.userData) {
      $scope.buffers.user.data.form = oldData.form.userData;
    }
    $scope.changePersonType(oldData.form.personType, false, oldData.form.ID);
  };
  $scope.updateRepresentativeData = function (oldData, newData) {
    oldData.form = managePersonService.convertViewModelToForm(newData, $scope.metaData);
    $scope.changePersonType(oldData.form.personType, true, oldData.form.ID);
  };

  // Find person
  // This method will be called on update
  $scope.findShareholder = function () {

    var criteriaForm = null;
    if (!$scope.shareholder.data.config.isNaturalPersonEnabled) {
      criteriaForm = {
        personType: {
          isVisible: false,
          default: appConstants.personClasses.juridicalPerson
        }
      };
    } else if (!$scope.shareholder.data.config.isJuridicalPersonEnabled) {

      criteriaForm = {
        personType: {
          isVisible: false,
          default: appConstants.personClasses.naturalPerson
        }
      };
    }

    personFindService.findClientShareholder(null, null, criteriaForm.personType.default).then(function (data) {
      if (data['id']) {
        Loader.show(true);
        $scope.config.clientAccountId = data['depoAccountId'];
        personFindService.findPersonById(data['id']).then(function (foundPerson) {

          $scope.updatePersonData($scope.shareholder.data, foundPerson);

        });
      }
    });
  };

  // Find representative
  $scope.findRepresentative = function (isSigner) {

    var criteriaForm = null;
    if (!$scope.shareholder.data.config.isNaturalRepresentativeEnabled) {
      criteriaForm = {
        personType: {
          isVisible: false,
          default: appConstants.personClasses.juridicalPerson
        }
      };
    } else if (!$scope.shareholder.data.config.isJuridicalRepresentativeEnabled) {

      criteriaForm = {
        personType: {
          isVisible: false,
          default: appConstants.personClasses.naturalPerson
        }
      };
    }
    personFindService.findAccount(
      {
        //  roleCodes: [appConstants.roleClasses.roleRepresentative],
        accountClasses: [appConstants.accountClasses.accountPersonal]
      }, criteriaForm
    ).then(function (data) {

      if (data['id']) {

        Loader.show(true);

        personFindService.findPersonById(data['id']).then(function (shareholder) {

          Loader.show(false);
          if (isSigner) {
            $scope.shareholder.data.form.signers.push(managePersonService.convertViewModelToForm(shareholder, $scope.metaData));
          } else {
            $scope.shareholder.data.form.representatives.push(managePersonService.convertViewModelToForm(shareholder, $scope.metaData));
          }
        });
      }

    });

  };

  // Find user
  $scope.findUser = function () {

    personFindService.findPerson().then(function (data) {

      if (data['id']) {

        Loader.show(true);

        personFindService.findPersonById(data['id']).then(function (user) {

          Loader.show(false);
          $scope.shareholder.data.form.users.push(managePersonService.convertViewModelToForm(user, $scope.metaData));

        });
      }

    });

  };

  $scope.userAccountAvailabilityChange = function (enabled, model) {
    if (enabled) {
      $scope.config.tabs.push({
        label: gettextCatalog.getString('Administrator Information')
      });
    } else {
      $scope.config.tabs.pop();
    }
  };

  $scope.updatePersonFromSearch = function (person, searchData) {
    Loader.show(true);
    $scope.config.duplicateUserDetected = false;
    if (person.config.isRepresentative || person.config.isUser) {

      personFindService.getFirstPersonByCriteria(searchData).then(function (resp) {
        if (resp.success === "true" && resp.data) {

          if (person.config.isRepresentative) {

            $scope.updateRepresentativeData($scope.buffers.representative.data, resp.data);

          } else {
            $scope.buffers.user.data.form = managePersonService.convertViewModelToForm(resp.data, $scope.metaData);
            $scope.buffers.user.data.config.fields = userConfigUpdate;
          }

          SweetAlert.swal("", resp.message, 'success');

        } else if (resp.success === "false") {
          SweetAlert.swal("", helperFunctionsService.showErrorMessage(resp), 'error');
        }

        Loader.show(false);
      });
    }
    else {
      personFindService.checkAccountStatusByRole(searchData, $scope.shareholder.data.config.currentRole).then(function (data) {
        console.log("account status checked", data);
        if (data && data.success === "true") {

          if ($scope.metaData.accountStatusCheckingClasses.ACCOUNT_NOT_EXIST.code == data.data.code) {

          } else if (($scope.metaData.accountStatusCheckingClasses.ACCOUNT_EXIST.code == data.data.code)
            || ($scope.metaData.accountStatusCheckingClasses.ACCOUNT_DUPLICATED.code == data.data.code )) {
            SweetAlert.swal("", data.message, 'success');

            // $scope.updatePersonData(person, personFindService.normalizePersonArrayFields(data.data.personData));


            resetShareholder()
            _updateConfiguration(data.data.personData)

          }
        } else {
          SweetAlert.swal('', gettextCatalog.getString("Internal Server Error"), 'error');
        }
        Loader.show(false);
      });
    }
  };

  $scope.fromJson = angular.formJson;

  function isOpeningTask() {
    return [
      "issuer_account_opening_entry",
      "pledgee_account_opening_entry",
      "investor_account_opening_entry",
      "depository_member_account_opening_entry",
      "trading_member_account_opening_entry",
      "clearing_member_account_opening_entry",
      "natural_client_account_opening_entry",
      "juridical_client_account_opening_entry"
    ].indexOf(task.key) > -1;
  }


  $scope.findPersonByIdCard = function (model, idCard, person) {

    // Pin Code validation
    if (idCard && idCard.series && idCard.number) {

      if (!(model.form.ID )
        && model.form.idCard.series == idCard.series
        && model.form.idCard.number == idCard.number
      ) {
        var searchData = {
          idDocumentSeries: idCard.series,
          idDocumentNumber: idCard.number,
          idDocumentClass: idDocumentClassCodes.idCardClassCode,
          personClassId: personClasses.natural.id
        };
        $scope.updatePersonFromSearch(model, searchData);
      }
    }
  };

  $scope.findPersonByRegistrationCertificate = function (model, regCert) {

    if (regCert && regCert.series && regCert.number) {

      if (!(model.form.ID )
        && model.form.registrationCertificate.series == regCert.series
        && model.form.registrationCertificate.number == regCert.number
      ) {
        var searchData = {
          idDocumentSeries: regCert.series,
          idDocumentNumber: regCert.number,
          idDocumentClass: idDocumentClassCodes.registerCertificate,
          personClassId: personClasses.juridical.id
        };
        $scope.updatePersonFromSearch(model, searchData);
      }
    }
  };

  $scope.findPersonByPinCode = function (model, identificator) {

    if (identificator) {
      if (!(model.form.ID && model.form.pinCode.value == identificator)) {
        var searchData = {
          identificatorClassCode: idDocumentClassCodes.uniqueCode,
          uniqueCode: identificator,
          personClassId: personClasses.natural.id
        };
        $scope.updatePersonFromSearch(model, searchData);
      }
    }
  };


  $scope.findPersonByTINCode = function (model, identificator) {

    if (identificator) {
      if (!(model.form.ID && model.form.TIN.value == identificator)) {
        var searchData = {
          identificatorClassCode: idDocumentClassCodes.uniqueCode,
          uniqueCode: identificator,
          personClassId: personClasses.juridical.id
        };
        $scope.updatePersonFromSearch(model, searchData);
      }
    }
  };
  $scope.legalFormClassCodeChange = function (newVal, form) {
    if (newVal) {
      var model = angular.fromJson(newVal);
      console.log(model);
      if (angular.isArray(model.children) && model.children.length > 1) {
        form.childLegalClasses = model.children;
        form.legalFormClass = null;
      } else {
        form.legalFormClass = newVal;
        form.childLegalClasses = [];
      }
    }
  };
  $scope.businessClassCodeChange = function (newVal, form) {
    console.log(1);
    if (newVal) {
      var model = angular.fromJson(newVal);
      console.log(model);
      if (angular.isArray(model.children) && model.children.length > 1) {
        form.childBusinessClasses = model.children;
        form.businessClass = null;
      } else {
        form.businessClass = newVal;
        form.childBusinessClasses = [];
      }
    }
  };


}]);
