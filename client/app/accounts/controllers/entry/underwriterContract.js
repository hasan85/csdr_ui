'use strict';

angular.module('cmisApp')
  .controller('UnderwriterContractController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'helperFunctionsService', 'operationState', 'task',
    'instrumentFindService', 'SweetAlert', 'gettextCatalog', '$rootScope',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, helperFunctionsService, operationState, task,
              instrumentFindService, SweetAlert, gettextCatalog, $rootScope) {

      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit: true

      };
      var payload = angular.fromJson(task.draft);
      var itemTemplate = {
        id: null,
        ISIN: null,
        issuerName: null,
      };

      $scope.metaData = {
        actionClasses: []
      };

      var prepareFormData = function (viewModel) {

        if (viewModel.finishDateObj) {
          viewModel.finishDate = helperFunctionsService.generateDateTime(viewModel.finishDateObj);
        }

        if (!viewModel.instruments) {
          viewModel.instruments = null;
        }

        return viewModel;
      };
      var initializeFormData = function (payload) {
        if (!payload) {
          payload = {};
        }
        if (payload.allInstruments === null || payload.allInstruments === undefined) {
          payload.allInstruments = true;
        }
        if (!payload.instruments) {
          payload.instruments = [angular.copy(itemTemplate)];
        }
        if (payload.finishDate && !payload.finishDateObj) {
          payload.finishDateObj = helperFunctionsService.parseDate(payload.finishDate);
          payload.finishDate = helperFunctionsService.generateDateTime(payload.finishDateObj);
        }

        return payload;
      };
      $scope.accountPrescriptionData = initializeFormData(angular.copy(payload));


      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "accountPrescriptionDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        isBrokerSearchEnabled: $scope.accountPrescriptionData.broker == null ? true : false,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.accountPrescriptionData)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.accountPrescriptionData);
      });


      // Search account
      $scope.findAccount = function () {
        personFindService.findIssuer().then(function (data) {
          $scope.accountPrescriptionData.clientAccount = {};
          $scope.accountPrescriptionData.clientAccount.id = data.id;
          $scope.accountPrescriptionData.clientAccount.accountId = data.accountId;
          $scope.accountPrescriptionData.clientAccount.name = data.name;
          $scope.instrumentSearchParams.params.issuerId = data.id;
        });
      };

      // Search broker
      $scope.findBroker = function () {
        personFindService.findTradingOrDepoMember().then(function (data) {
          $scope.accountPrescriptionData.broker = {};
          $scope.accountPrescriptionData.broker.name = data.name;
          $scope.accountPrescriptionData.broker.accountId = data.accountId;
          $scope.accountPrescriptionData.broker.id = data.id;
          $scope.accountPrescriptionData.broker.accountNumber = data.accountNumber;
          $scope.accountPrescriptionData.broker.roles = helperFunctionsService.convertObjectToArray(data.roles);
        });
      };

      // Search instrument
      $scope.findInstrument = function (index) {
        instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {
          var isDuplicate = false;
          if ($scope.accountPrescriptionData.instruments) {
            for (var i = 0; i < $scope.accountPrescriptionData.instruments.length; i++) {
              var obj = $scope.accountPrescriptionData.instruments[i];
              if (obj.id == data.id) {
                isDuplicate = true;
                $scope.accountPrescriptionData.instruments.pop();
                break;
              }
            }
          }

          if (isDuplicate) {
            SweetAlert.swal('', gettextCatalog.getString('Duplicate instrument!'), 'error');
          } else {
            $scope.accountPrescriptionData.instruments[index].ISIN = data.isin;
            $scope.accountPrescriptionData.instruments[index].id = data.id;
            $scope.accountPrescriptionData.instruments[index].issuerName = data.issuerName;
          }

        });
      };

      // Add new item
      $scope.addNewItem = function (model) {
        model.push(angular.copy(itemTemplate));
      };

      // Remove item
      $scope.removeItem = function (index, model) {
        model.splice(index, 1);
      };

    }]);
