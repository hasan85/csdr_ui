'use strict';

angular.module('cmisApp')
  .controller('TaxExemptAccountController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'Loader', 'referenceDataService', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, Loader, referenceDataService, gettextCatalog) {

      $scope.data = angular.fromJson(task.draft);
      $scope.metaData = {
        couponPaymentMethodClasses: []
      };
      function resetData() {
        $scope.data = angular.fromJson(task.draft);
      }

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              var data = prepareFormData(angular.copy($scope.data));
              $scope.config.completeTask(data);
            }
          }
        }
      };
      function prepareFormData(data) {
        var result = {
        };
        if (data.personID) {
          result.personID = data.personID;
        }
        if (data.distributionClass) {
          var model = angular.fromJson(data.distributionClass);
          result.distributionClassID = model.id;
          result.distribution = model.nameAz;
          result.distributionClass = data.distributionClass;
        }
        if (data.name) {
          result.name = data.name;
        }
        if (data.distribution) {
          result.distribution = data.distribution;
        }
        return result;
      }
      $scope.distributionChange = function (cls) {
        var clsObj = null;
        if (cls) {
          clsObj = angular.fromJson(cls);
        }
        $scope.data.distribution = clsObj ? clsObj.nameAz : '';
      };
      Loader.show(true);
      referenceDataService.getCouponPaymentMethodClasses().then(function (res) {
        $scope.metaData.couponPaymentMethodClasses = res;
        Loader.show(false);
      });

      $scope.findShareholder = function () {
        // findShareholder, searchShareholders
        personFindService.searchShareholders({
          criteriaForm: {
            personType: {
              isVisible: false,
              default: appConstants.personClasses.juridicalPerson
            }
          },
          title: " Təşkilatı Tap",
          searchUrl: "/api/reportsDataServices/searchJuridicalPersons",
          params: {
            columns: [
                    {
                      field: "name",
                      title: gettextCatalog.getString("Təşkilatın Adı"),
                      width: '8rem'
                    },
                    {
                      field: "createdDate",
                      title: gettextCatalog.getString("Created Date"),
                      width: '8rem',
                      format: '{0:dd-MMMM-yyyy}'
                    },
                    {
                      field: "isCoupon",
                      title: gettextCatalog.getString("Kupon"),
                      width: '5rem',
                      template: function (val) {
                        var logo = 'glyphicon';
                        if (val.isCoupon === "true") {
                          logo += ' glyphicon-ok'
                        } else {
                          logo += ' glyphicon-remove';
                        }
                        return '<div style="text-align:center;font-size:1.3em;">' +
                          '<i class="'+logo+'"></i>' +
                          '</div>';
                      }
                    },
                    {
                      field: "isDiscount",
                      title: "Kupon Discount",
                      width: "5rem",
                      template: function (val) {
                        var logo = 'glyphicon';
                        if (val.isDiscount === "true") {
                          logo += ' glyphicon-ok'
                        } else {
                          logo += ' glyphicon-remove';
                        }
                        return '<div style="text-align:center;font-size:1.3em;">' +
                          '<i class="'+logo+'"></i>' +
                          '</div>';

                      }
                    }
                  ],
            personSelected: true
          }
        }).then(function (data) {
            resetData();
            $scope.data.personID = data.id;
            $scope.data.name = data.name;
            $scope.data[$scope.metaData.couponPaymentMethodClasses[0].id] = data.isCoupon == "true";
            $scope.data[$scope.metaData.couponPaymentMethodClasses[1].id] = data.isDiscount == "true";
        });
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.data);
      });


    }]);
