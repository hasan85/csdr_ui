'use strict';

angular.module('cmisApp')
  .controller('AccountConsolidationController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'dataSearchService', 'Loader', 'SweetAlert', 'gettextCatalog', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, dataSearchService, Loader, SweetAlert, gettextCatalog, helperFunctionsService) {

      var validateForm = function (formData) {

        var similarAccountSelected = false;

        if (!formData.accounts) {
          return false;
        } else if (formData.accounts.length < 2) {
          SweetAlert.swal('', gettextCatalog.getString('You have to select at least two accounts!'), 'error');
          return false;
        } else if (!formData.accounts[1].personID) {
          SweetAlert.swal('', gettextCatalog.getString('You have to select at least two accounts!'), 'error');
          return false;
        }

        formData.accounts.forEach(function (account) {
          if (account.consolidateTo) {
            similarAccountSelected = true;
          }
        });

        if (!similarAccountSelected) {
          SweetAlert.swal('', gettextCatalog.getString('You have to select similar account!'), 'error');
          return false;
        }
        return true;
      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {};
        }
        if (!draft.accounts || (draft.accounts && !draft.accounts.length )) {
          draft.accounts = [
            {
              consolidateTo: false
            }
          ]
        }
        return draft;
      };
      $scope.accountConsolidationData = initializeFormData(angular.fromJson(task.draft));
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "accountConsolidationDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid && validateForm($scope.accountConsolidationData)) {
                $scope.config.completeTask(angular.copy($scope.accountConsolidationData));
              }
            }
          }
        }
      };
      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });
      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.accountConsolidationData);
      });
      $scope.changeAccountStatus = function () {
        if ($scope.accountConsolidationData.isDeleted) {
          if ($scope.accountConsolidationData.functionalAccounts) {
            for (var i = 0; i < $scope.accountConsolidationData.functionalAccounts.length; i++) {
              $scope.accountConsolidationData.functionalAccounts[i].isDeleted = true;
            }
          }
        }
      };
      var isDuplicateAccount = function (newAccount) {

        var isDuplicate = false;

        $scope.accountConsolidationData.accounts.forEach(function (account) {
          if (account.personID == newAccount.id) {
            SweetAlert.swal('', gettextCatalog.getString('You cannot consolidate same accounts!'), 'error');
            isDuplicate = true;
          }
        });
        return isDuplicate;

      };
      // Search First account
      $scope.findAccount = function ($index) {
        Loader.show(true);
        personFindService.findPerson().then(function (data) {

          if (!isDuplicateAccount(data)) {
            $scope.accountConsolidationData.accounts[$index] = {
              personID: data.id,
              name: data.name,
              uniqueCode: data.uniqueCode,
              document: data.idDocument,
              address: data.addressLine,
              consolidateTo: false
            };
          }

          Loader.show(false);
        });
      };
      $scope.addNewAccount = function (model) {
        model.push({});
      };
      $scope.similarAccountSelected = function (currentVal, $index) {

        if (currentVal) {
          for (var i = 0; i < $scope.accountConsolidationData.accounts.length; i++) {
            var obj = $scope.accountConsolidationData.accounts[i];

            if (i != $index && obj.consolidateTo) {
              obj.consolidateTo = false;
            }

          }
        }
      };
      $scope.removeAccount = function (model, $index) {
        model.splice($index, 1);
      };
    }]);
