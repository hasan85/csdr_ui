'use strict';

angular.module('cmisApp')
  .controller('DuplicateAccountReportController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task) {

      $scope.duplicateAccountData = angular.fromJson(task.draft);

      $scope.panelBarOptions = {
        dataSource: $scope.albums
      };
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.completeTask(angular.copy($scope.duplicateAccountData));
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.duplicateAccountData);
      });


    }]);
