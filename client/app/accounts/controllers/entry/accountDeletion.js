'use strict';

angular.module('cmisApp')
  .controller('AccountDeletionController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task', 'dataSearchService', 'Loader', 'SweetAlert', 'gettextCatalog', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task, dataSearchService, Loader, SweetAlert, gettextCatalog, helperFunctionsService) {

      $scope.accountSuspensionData = angular.fromJson(task.draft);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "accountSuspensionDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(angular.copy($scope.accountSuspensionData));
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.accountSuspensionData);
      });

      $scope.changeAccountStatus = function () {
        if ($scope.accountSuspensionData.isDeleted) {
          if ($scope.accountSuspensionData.functionalAccounts) {
            for (var i = 0; i < $scope.accountSuspensionData.functionalAccounts.length; i++) {
              $scope.accountSuspensionData.functionalAccounts[i].isDeleted = true;
            }
          }
        }
      };
      // Search account
      $scope.findAccount = function () {
      if (!$scope.config.form.name.$valid) {
              SweetAlert.swal('', 'Hesab nömrəsini daxil edin', 'error');
              return;
          }
        Loader.show(true);
        dataSearchService.getAccountInfo($scope.accountSuspensionData.number).then(function (data) {
          var errorMessage = null;
          if (data && data.success == "true") {
            var result = data.data;
            result.isSuspended = result.isSuspended == "false" ? false : true;
            result.isDeleted = result.isDeleted == "false" ? false : true;
            if (result.owners) {
              result.owners = helperFunctionsService.convertObjectToArray(result.owners)
            }
            if (result.functionalAccounts) {
              result.functionalAccounts = helperFunctionsService.convertObjectToArray(result.functionalAccounts);
              for (var i = 0; i < result.functionalAccounts.length; i++) {
                result.functionalAccounts[i].isSuspended = result.functionalAccounts[i].isSuspended == "false" ? false : true;
                result.functionalAccounts[i].isDeleted = result.functionalAccounts[i].isDeleted == "false" ? false : true;
                result.functionalAccounts[i].owners =  helperFunctionsService.convertObjectToArray( result.functionalAccounts[i].owners )
              }
            }

            $scope.accountSuspensionData = result;
          } else if (data && data.success == "false") {
            errorMessage = data.message;
          } else {
            errorMessage = gettextCatalog.getString('Internal Server Error')
          }
          if (errorMessage) {
            SweetAlert.swal('', errorMessage, 'error')
          }
          Loader.show(false);
        });
      };

    }]);
