'use strict';

angular.module('cmisApp')
  .controller('ClientAccountOpeningController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'helperFunctionsService', 'operationState', 'task',
    'instrumentFindService', 'SweetAlert', 'gettextCatalog','$filter',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, helperFunctionsService, operationState, task,
              instrumentFindService, SweetAlert, gettextCatalog,$filter) {
      var roleClassConstants = {
        tradingMember: "ROLE_TRADING_MEMBER",
        depoMember: "ROLE_DEPO_MEMBER"
      };
      var itemTemplate = {
        instrument: {
          id: null,
          ISIN: null,
          issuerName: null,
        },
        allInstrumentSelected: true,
        hasRestrictionToQuantity: false,
        quantity: null,
      };
      $scope.metaData = {
        actionClasses: []
      };
      var resolveActionClasses = function (draft) {
        var roleCodes = helperFunctionsService.convertObjectToArray(draft.broker.roles);
        var hasDepoMemberRole = false, hasTradingMemberRole = false;
        if (roleCodes) {
          for (var i = 0; i < roleCodes.length; i++) {
            if (roleCodes[i].code == roleClassConstants.depoMember) {
              hasDepoMemberRole = true;
            }
            if (roleCodes[i].code == roleClassConstants.tradingMember) {
              hasTradingMemberRole = true;
            }
          }
        }
        $scope.metaData.actionClasses = [];
        if (hasTradingMemberRole) {
          $scope.metaData.actionClasses = $scope.metaData.actionClasses.concat(draft.tradingMemberActionClasses)
        }
        if (hasDepoMemberRole) {
          $scope.metaData.actionClasses = $scope.metaData.actionClasses.concat(draft.depoMemberActionClasses)
        }
      };
      var prepareFormData = function (viewModel) {

        if (viewModel.finishDateObj) {
          viewModel.finishDate = helperFunctionsService.generateDateTime(viewModel.finishDateObj);
        }

        if (viewModel.items) {

          for (var i = 0; i < viewModel.items.length; i++) {
            viewModel.items[i]['actionClass'] = angular.fromJson(viewModel.items[i]['actionClass']);
          }
        } else {
          viewModel.items = null;
        }

        return viewModel;
      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {};
        }

        if (draft.broker != null) {
          resolveActionClasses(draft);
        }

        if (!draft.items || (draft.items && draft.items.length < 1)) {

          draft.items = [
            angular.copy(itemTemplate)
          ];
        } else if (draft.items && draft.items.length > 0) {

          for (var i = 0; i < draft.items.length; i++) {
            var obj = draft.items[i];
            if (obj.actionClass && obj.actionClass.id) {
              obj.actionClass = $filter('json')(helperFunctionsService.findByIdInsideArray($scope.metaData.actionClasses, obj.actionClass['id']));
            }

          }
        }

        if (draft.finishDate && !draft.finishDateObj) {
          draft.finishDateObj = helperFunctionsService.parseDate(draft.finishDate);
          draft.finishDate = helperFunctionsService.generateDateTime(draft.finishDateObj);
        }

        return draft;
      };
      $scope.accountPrescriptionData = initializeFormData(angular.fromJson(task.draft));

      if ($scope.accountPrescriptionData.broker != null) {
        resolveActionClasses($scope.accountPrescriptionData);
      }

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "accountPrescriptionDataForm",
          data: {}
        },
        isBrokerSearchEnabled: $scope.accountPrescriptionData.broker == null ? true : false,
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.accountPrescriptionData)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };
      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.accountPrescriptionData));
      });

      // Search account
      $scope.findAccount = function () {
        personFindService.findShareholderAccountsWithOperatorName().then(function (data) {
          $scope.accountPrescriptionData.clientAccount = {};
          $scope.accountPrescriptionData.clientAccount.id = data.id;
          $scope.accountPrescriptionData.clientAccount.accountId = data.accountId;
          $scope.accountPrescriptionData.clientAccount.name = data.name;
          $scope.accountPrescriptionData.clientAccount.accountNumber = data.accountNumber;
        });
      };

      // Search broker
      $scope.findBroker = function () {
        personFindService.findTradingOrDepoMember().then(function (data) {
          $scope.accountPrescriptionData.broker = {};
          $scope.accountPrescriptionData.broker.name = data.name;
          $scope.accountPrescriptionData.broker.accountId = data.accountId;
          $scope.accountPrescriptionData.broker.id = data.id;
          $scope.accountPrescriptionData.broker.accountNumber = data.accountNumber;
          $scope.accountPrescriptionData.broker.roles = helperFunctionsService.convertObjectToArray(data.roles);
          resolveActionClasses($scope.accountPrescriptionData);
        });
      };

      // Search instrument
      $scope.findInstrument = function (index) {
        instrumentFindService.findInstrument().then(function (data) {
          $scope.accountPrescriptionData.items[index].instrument.ISIN = data.isin;
          $scope.accountPrescriptionData.items[index].instrument.id = data.id;
          $scope.accountPrescriptionData.items[index].instrument.issuerName = data.issuerName;
        });
      };
      $scope.changeInstrumentSelection = function (item) {
        item.instrument.ISIN = null;
        item.instrument.id = null;
        item.instrument.issuerName = null;
      };
      $scope.quantityRestrictionChange = function (item) {
        item.quantity = null;
      };
      $scope.addNewItem = function (model) {
        model.push(angular.copy(itemTemplate));
      };
      $scope.removeItem = function (index, model) {
        model.splice(index, 1);
      };

    }]);
