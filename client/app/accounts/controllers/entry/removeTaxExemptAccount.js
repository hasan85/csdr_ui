'use strict';

angular.module('cmisApp')
  .controller('RemoveTaxExemptAccountController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'operationState', 'task',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, operationState, task) {

      $scope.data = angular.fromJson(task.draft);
      $scope.data.form = {

      };
      $scope.metaData = {

      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              var result = prepareFormData(angular.copy($scope.data));
              $scope.config.completeTask(result);
            }
          }
        }
      };
      function prepareFormData(data) {
        var result = {};
        if (data.id) result.id = data.id;
        if (data.name) result.name = data.name;
        if (data.distribution) result.distribution = data.distribution;
        return result;
      }
      $scope.findTaxExempt = function () {
        personFindService.findTaxExempt().then(function (obj) {
          $scope.data.id = obj.id;
          $scope.data.name = obj.name;
          $scope.data.distribution = obj.distributionClass;
        });
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.duplicateAccountData);
      });


    }]);
