'use strict';

angular.module('cmisApp')
  .controller('SendBatchDataController', ['$scope', 'operationService', '$windowInstance', 'id', 'Loader',
    'dataValidationService', 'SweetAlert', 'helperFunctionsService', '$http',
    function ($scope, operationService, $windowInstance, id, Loader, dataValidationService, SweetAlert, helperFunctionsService, $http) {


      var prepareFormData = function (formData) {

        // formData.date = helperFunctionsService.generateDateTime(formData.date);

        return formData;
      };
      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid1: {},
        mainGrid2: {},
        mainGrid3: {},
        mainGrid4: {},
        buttons: {
          view: false,
          print: false,
          refresh: false,
          export: false
        }
      };
      $scope.batchData = {
        formName: 'batchDataFormName',
        form: {
          sendTo: 'FOS'
        }
      };

      $scope.corporateAction = {
        formName: 'corporateActionFormName',
        form: {}
      };

      $scope.proceedTRSEndofDateOperations = function () {
        Loader.show(true);
        $http.get("/api/reportsDataServices/proceedTRSBatchMessages")
          .then(function (data) {
            var d = data.data;
            if (d.isSuccess) {
              SweetAlert.swal("", "Əməliyyat uğurla başa çatdı", "success");
            } else {
              SweetAlert.swal("", d.message, "error");
            }

            Loader.show(false);
          });
      };

        $scope.proceedTRSProcess = function () {
            Loader.show(true);
            $http.get("/api/reportsDataServices/proceedTRSBatchProcess")
                .then(function (data) {
                    var d = data.data;
                    if (d.isSuccess) {
                        SweetAlert.swal("", "Əməliyyat uğurla başa çatdı", "success");
                    } else {
                        SweetAlert.swal("", d.message, "error");
                    }
                    Loader.show(false);
                });
        };
      $scope.sendBatchData = function () {

        Loader.show(true);

        var formBuf = prepareFormData(angular.copy($scope.batchData.form));

        dataValidationService.sendBatchData(formBuf).then(function (data) {

          Loader.show(false);
          if (data) {

            if (data.success == 'true') {
              SweetAlert.swal('', 'Successfully sent.', 'success');
            } else {
              SweetAlert.swal('', 'Can\'t send!', 'error');
            }
          } else {
            SweetAlert.swal('', 'Something  happened ...', 'error');
          }
        });
      };


      $scope.sendTest = function () {

        Loader.show(true);
        var formBuf = prepareFormData(angular.copy($scope.corporateAction.form));
        dataValidationService.sendTestData(formBuf).then(function (data) {

          Loader.show(false);
          if (data) {

            if (data.success == 'true') {
              SweetAlert.swal('', 'Successfully sent.', 'success');
            } else {
              SweetAlert.swal('', 'Can\'t send!', 'error');
            }
          } else {
            SweetAlert.swal('', 'Something  happened ...', 'error');
          }
        });
      };
      $scope.sendActionFinish = function () {

        Loader.show(true);

        dataValidationService.sendActionFinish().then(function (data) {

          Loader.show(false);
          if (data) {

            if (data.success == 'true') {
              SweetAlert.swal('', 'Successfully sent.', 'success');
            } else {
              SweetAlert.swal('', 'Can\'t send!', 'error');
            }
          } else {
            SweetAlert.swal('', 'Something  happened ...', 'error');
          }
        });
      };
    }]);
