'use strict';

angular.module('cmisApp').factory('managePersonService', [
  "referenceDataService", "$q", "Loader", "$http",
  'appConstants', 'helperFunctionsService', '$filter', 'gettextCatalog',
  function (referenceDataService, $q, Loader, $http,
            appConstants, helperFunctionsService, $filter, gettextCatalog) {

    var shareHolderViewModel = {
      personType: null,
      naturalPersonConfiguration: null,
      juridicalPersonConfiguration: null,
      naturalPersonShareholderConfiguration: null,
      juridicalPersonShareholderConfiguration: null,
      representativeConfiguration: null,
      signerConfiguration: null,
      subjectToFatca: null,
      noteOnFatca: null,
      pinCode: null,
      TIN: null,
      azipsCode: null,
      bankCode: null,
      issuerCode: null,
      settlementAgent: null,
      memberRoles: null,
      BSEBrokerCode: null,
      BSEIssuerID: null,
      name: null,
      firstName: null,
      lastName: null,
      middleName: null,
      abbreviation: null,
      citizenshipCountry: null,
      jurisdictionCountry: null,
      position: null,
      birthday: null,
      legalFormClass: null,
      businessClass: null,
      warrantStartDate: null,
      warrantEndDate: null,
      creationDate: null,
      legalAddress: null,
      actualAddress: null,
      otherDocuments: [],
      bankAccounts: [],
      phoneNumbers: [],
      emails: [],
      representatives: [],
      users: [],
      userData: null,
      isRepresentative: null,
      isSigner: null,
      functionalAccountNumber: null
    };
    var shareHolderFormModel = {
      ID: null,
      personType: null,
      naturalPersonConfiguration: null,
      juridicalPersonConfiguration: null,
      naturalPersonShareholderConfiguration: null,
      juridicalPersonShareholderConfiguration: null,
      representativeConfiguration: null,
      signerConfiguration: null,
      subjectToFatca: null,
      noteOnFatca: null,
      pinCode: null,
      TIN: null,
      azipsCode: null,
      bankCode: null,
      issuerCode: null,
      settlementAgent: null,
      idCard: null,
      registrationCertificate: null,
      memberRoles: [],
      BSEBrokerCode: null,
      BSEIssuerID: null,
      name: null,
      firstName: null,
      lastName: null,
      middleName: null,
      abbreviation: null,
      citizenshipCountry: null,
      jurisdictionCountry: null,
      position: null,
      birthdayObj: null,
      birthdayStr: null,
      legalFormClass: null,
      businessClass: null,
      warrantStartDateStr: null,
      warrantStartDateObj: null,
      warrantEndDateStr: null,
      warrantEndDateObj: null,
      creationDateObj: null,
      creationDateStr: null,
      legalAddress: null,
      actualAddress: null,
      documents: [],
      bankAccounts: [],
      phoneNumbers: [],
      emails: [],
      representatives: [],
      signers: [],
      users: [],
      userData: null,
      isRepresentative: null,
      isSigner: null

    };

    // Prepare form data for save
    var prepareFormData = function (formData, metaData) {
      var naturalPerson = null;
      var personClasses = metaData.personClasses;
      for (var m = 0; m < personClasses.length; m++) {
        if (personClasses[m].code === appConstants.personClasses.naturalPerson) {
          naturalPerson = personClasses[m];
        }
      }
      var result = angular.copy(shareHolderViewModel);


      result.naturalPersonConfiguration = formData.naturalPersonConfiguration;
      result.juridicalPersonConfiguration = formData.juridicalPersonConfiguration;

      console.log(formData);

      if (formData.personType) {
        result.personType = angular.fromJson(formData.personType);
      }
      else {
        result.personType = naturalPerson;
      }

      result.ID = formData.ID;

      if (formData.azipsCode && formData.azipsCode.value) {
        result.azipsCode = formData.azipsCode;
      }
      if (formData.bankCode && formData.bankCode.value) {
        result.bankCode = formData.bankCode;
      }
      if (formData.azipsCode && formData.azipsCode.value) {
        result.azipsCode = formData.azipsCode;
      }
      if (formData.issuerCode && formData.issuerCode.value) {
        result.issuerCode = formData.issuerCode;
      }
      if (formData.settlementAgent) {
        result.settlementAgent = formData.settlementAgent;
      }
      if (formData.BSEBrokerCode && formData.BSEBrokerCode.value) {
        result.BSEBrokerCode = formData.BSEBrokerCode;
      }
      if (formData.functionalAccountNumber) {
        result.functionalAccountNumber = formData.functionalAccountNumber;
      }
      result.BSEIssuerID = formData.BSEIssuerID;
      result.abbreviation = formData.abbreviation;
      result.isSigner = formData.isSigner;
      result.isRepresentative = formData.isRepresentative;
      result.legalAddress = formData.legalAddress ? formData.legalAddress : null;

      if (result.legalAddress && result.legalAddress.country && result.legalAddress.country != '') {
        result.legalAddress.country = angular.fromJson(result.legalAddress.country);
      } else if (result.legalAddress) {
        result.legalAddress.country = null;
      }
      result.actualAddress = formData.actualAddress ? formData.actualAddress : null;

      if (result.actualAddress && result.actualAddress.country && result.actualAddress.country != '') {
        result.actualAddress.country = angular.fromJson(result.actualAddress.country);
      } else if (result.actualAddress) {
        result.actualAddress.country = null;
      }

      result.subjectToFatca = formData.subjectToFatca;
      result.noteOnFatca = formData.noteOnFatca;

      if (formData.emails && formData.emails.length > 0) {
        for (var i = 0; i < formData.emails.length; i++) {
          if (formData.emails[i].value != '') {
            result.emails.push(formData.emails[i]);
          }
        }
      }

      if (formData.phoneNumbers && formData.phoneNumbers.length > 0) {
        for (var i = 0; i < formData.phoneNumbers.length; i++) {

          if (formData.phoneNumbers[i]['type']) {
            formData.phoneNumbers[i]['type'] = angular.fromJson(formData.phoneNumbers[i]['type']);
          } else {
            formData.phoneNumbers[i]['type'] = null;
          }
          if (formData.phoneNumbers[i]['number']) {
            result.phoneNumbers.push(formData.phoneNumbers[i]);
          }

        }

      }

      if (formData.bankAccounts && formData.bankAccounts.length > 0) {
        for (var i = 0; i < formData.bankAccounts.length; i++) {
          formData.bankAccounts[i]['currency'] = angular.fromJson(formData.bankAccounts[i]['currency']);
        }
        result.bankAccounts = formData.bankAccounts;

      }

      result.otherDocuments = [];
      if (formData.documents && formData.documents.length > 0) {
        for (var i = 0; i < formData.documents.length; i++) {
          if (formData.documents[i]['type']) {
            formData.documents[i]['type'] = angular.fromJson(formData.documents[i]['type']);
            result.otherDocuments.push(formData.documents[i]);
          }
        }
      }


      if (formData.representatives && formData.representatives.length > 0) {

        for (var i = 0; i < formData.representatives.length; i++) {
          formData.representatives[i] = prepareFormData(formData.representatives[i], metaData);
        }
        result.representatives = formData.representatives;
      }
      if (formData.signers && formData.signers.length > 0) {

        for (var i = 0; i < formData.signers.length; i++) {
          formData.signers[i] = prepareFormData(formData.signers[i], metaData);
        }
        result.signers = formData.signers;
      }

      if (formData.userData) {
        result.userData = prepareFormData(formData.userData, metaData);
      }

      console.log(result);
      var personClassCode = result.personType.code;

      if (personClassCode == appConstants.personClasses.naturalPerson) {

        result.firstName = formData.firstName;
        result.middleName = formData.middleName;
        result.lastName = formData.lastName;

        if (formData.pinCode && formData.pinCode.value) {
          result.pinCode = formData.pinCode;
        }
        if (formData.idCard) {
          result.idCard = formData.idCard;
        }
        if (formData.warrantStartDateObj) {
          result.warrantStartDate = helperFunctionsService.generateDateTime(formData.warrantStartDateObj);
        }

        if (formData.warrantEndDateObj) {
          result.warrantEndDate = helperFunctionsService.generateDateTime(formData.warrantEndDateObj);
        }

        if (formData.citizenshipCountry) {
          result.citizenshipCountry = angular.fromJson(formData.citizenshipCountry);
        }

        if (formData.birthdayObj) {
          result.birthday = helperFunctionsService.generateDateTime(formData.birthdayObj);
        }

        result.name = formData.name;
      }
      else {

        result.name = formData.name;
        if (formData.registrationCertificate && formData.registrationCertificate.number) {
          result.registrationCertificate = formData.registrationCertificate;
        }

        if (formData.TIN && formData.TIN.value) {
          result.TIN = formData.TIN;
        }
        if (formData.jurisdictionCountry) {
          result.jurisdictionCountry = angular.fromJson(formData.jurisdictionCountry);
        }
        if (formData.legalFormClass) {
          result.legalFormClass = angular.fromJson(formData.legalFormClass);
        }
        if (formData.legalFormClassCode) {
          result.legalFormClassCode = angular.fromJson(formData.legalFormClassCode);
        }


        if (formData.businessClass) {
          result.businessClass = angular.fromJson(formData.businessClass);
          result.businessClass.children = null;
        }
        if (formData.businessClassCode) {
          result.businessClassCode = angular.fromJson(formData.businessClassCode);
        }


        if (formData.memberRoles && formData.memberRoles.roleClassObj && formData.memberRoles.roleClassObj.length > 0) {
          result.memberRoles = [];
          for (var k = 0; k < formData.memberRoles.roleClassObj.length; k++) {
            result.memberRoles.push(
              {
                id: formData.memberRoles.roleClassOldObj ?
                  formData.memberRoles.roleClassOldObj[formData.memberRoles.roleClassObj[k].id]
                  : null,
                roleClass: {
                  id: formData.memberRoles.roleClassObj[k].id,
                  code: formData.memberRoles.roleClassObj[k].code,
                  nameAz: formData.memberRoles.roleClassObj[k].nameAz,
                  nameEn: formData.memberRoles.roleClassObj[k].nameEn
                }
              });
          }
        } else {
          result.memberRoles = null;
        }
        if (formData.creationDateObj) {
          result.creationDate = helperFunctionsService.generateDateTime(formData.creationDateObj);
        }

      }
      if (formData.position) {
        result.position = angular.fromJson(formData.position);
      }
      return result;
    };

    // [[


    var convertViewModelToForm = function (model, metaData) {

      var result = angular.copy(shareHolderFormModel);

      result.ID = model.ID;
      result.pinCode = model.pinCode;
      result.TIN = model.TIN;
      result.azipsCode = model.azipsCode;
      result.issuerCode = model.issuerCode;
      result.settlementAgent = model.settlementAgent;
      result.BSEBrokerCode = model.BSEBrokerCode;
      result.BSEIssuerID = model.BSEIssuerID;
      result.abbreviation = model.abbreviation;
      result.isSigner = model.isSigner;
      result.isRepresentative = model.isRepresentative;
      result.legalAddress = model.legalAddress ? model.legalAddress : null;
      result.actualAddress = model.actualAddress ? model.actualAddress : null;
      result.subjectToFatca = model.subjectToFatca === 'true' ? true : false;
      result.isJuridicalPersonEnabled = model.isJuridicalPersonEnabled === 'true' ? true : false;
      result.isJuridicalRepresentativeEnabled = model.isJuridicalRepresentativeEnabled === 'true' ? true : false;
      result.isNaturalPersonEnabled = model.isNaturalPersonEnabled === 'true' ? true : false;
      result.isNaturalRepresentativeEnabled = model.isNaturalRepresentativeEnabled === 'true' ? true : false;
      result.isRepresentative = model.isRepresentative === 'true' ? true : false;
      result.isRepresentativeEnabled = model.isRepresentativeEnabled === 'true' ? true : false;
      result.isSigner = model.isSigner === 'true' ? true : false;
      result.isUser = model.isUser === 'true' ? true : false;
      result.isUserEnabled = model.isUserEnabled === 'true' ? true : false;
      result.isUserEnabled = model.isUserEnabled === 'true' ? true : false;


      console.log(model.naturalPersonConfiguration, model.juridicalPersonConfiguration);

      result.naturalPersonConfiguration = model.naturalPersonConfiguration;
      result.juridicalPersonConfiguration = model.juridicalPersonConfiguration;

      result.noteOnFatca = model.noteOnFatca;
      result.emails = model.emails && model.emails.length > 0 ? model.emails : [{value: ''}];
      //result.phoneNumbers = model.phoneNumbers && model.phoneNumbers.length > 0 ? model.phoneNumbers : [{type: "", number: ''}];

      if (model.legalAddress && model.legalAddress.country) {
        result.legalAddress.country = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.countries, model.legalAddress.country['id']));
      }

      if (model.actualAddress && model.actualAddress.country) {
        result.actualAddress.country = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.countries, model.actualAddress.country['id']));
      }

      if (model.phoneNumbers && model.phoneNumbers.length > 0) {

        for (var i = 0; i < model.phoneNumbers.length; i++) {

          if (model.phoneNumbers[i]['type']) {
            model.phoneNumbers[i]['type'] = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.phoneNumberTypes, model.phoneNumbers[i]['type']['id']));
          } else {
            model.phoneNumbers[i]['type'] = null;
          }
          result.phoneNumbers.push(model.phoneNumbers[i]);
        }
      }
      else {
        result.phoneNumbers = [
          {
            type: null,
            number: ''
          }
        ];
      }


      if (model.otherDocuments && model.otherDocuments.length > 0) {

        for (var i = 0; i < model.otherDocuments.length; i++) {

          if (model.otherDocuments[i]['type']) {

            model.otherDocuments[i]['type'] = helperFunctionsService.findByIdInsideArray(metaData.allIdDocumentClasses, model.otherDocuments[i]['type']['id']);

          }

        }

      } else {
        model.otherDocuments = [];
      }

      result.documents = model.otherDocuments;

      if (model.bankAccounts && model.bankAccounts.length > 0) {

        for (var i = 0; i < model.bankAccounts.length; i++) {

          if (model.bankAccounts[i]['currency']) {

            model.bankAccounts[i]['currency'] = helperFunctionsService.findByIdInsideArray(metaData.currencies, model.bankAccounts[i]['currency']['id']);

          }
          result.bankAccounts.push(model.bankAccounts[i]);
        }
      } else {
        result.bankAccounts = [];
      }

      if (model.representatives && model.representatives.length > 0) {
        for (var i = 0; i < model.representatives.length; i++) {
          model.representatives[i] = convertViewModelToForm(model.representatives[i], metaData);

        }
        result.representatives = model.representatives;
      } else {
        result.representatives = [];
      }

      if (model.signers && model.signers.length > 0) {
        for (var i = 0; i < model.signers.length; i++) {
          model.signers[i] = convertViewModelToForm(model.signers[i], metaData);
        }
        result.signers = model.signers;
      } else {
        result.signers = [];
      }

      if (model.userData) {
        result.userData = convertViewModelToForm(model.userData, metaData);
      }

      var personClassCode = model.personType ? model.personType.code : appConstants.personClasses.naturalPerson;


      result.personType = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.personClasses, model['personType']['id']));

      result.name = model.name;

      if (model.position) {
        result.position = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.signerPositions, model.position.id));
      }
      if (personClassCode == appConstants.personClasses.naturalPerson) {

        result.firstName = model.firstName;
        result.firstName = model.firstName;
        result.middleName = model.middleName;
        result.lastName = model.lastName;


        if (model.warrantStartDate) {
          result.warrantStartDateObj = model.warrantStartDate;
          result.warrantStartDateStr = helperFunctionsService.generateDate(new Date(model.warrantStartDate));
        }
        if (model.warrantEndDate) {

          result.warrantEndDateObj = model.warrantEndDate;
          result.warrantEndDateStr = helperFunctionsService.generateDate(new Date(model.warrantEndDate));
        }
        if (model.citizenshipCountry) {
          result.citizenshipCountry = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.countries, model['citizenshipCountry']['id']));
        }

        if (model.birthday) {
          result.birthdayObj = model.birthday;
          result.birthdayStr = helperFunctionsService.generateDate(new Date(model.birthday));
        }


        if (model.idCard) {
          result.idCard = {
            id: model.idCard.id,
            number: model.idCard.number,
            series: model.idCard.series
          }
        }

      }
      else {

        if (model.businessClass) { // [[ BusinessClass
          var parent = helperFunctionsService.findByIdInsideArrayByProperty(metaData.businessClasses, model.businessClass.code, 'code');
          if (parent) {
            result.businessClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.businessClasses, parent.id));
            result.businessClassCode = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.businessClasses, parent.id));
          } else {
            for (var i = 0; i < metaData.businessClasses.length; i++) {
              if (metaData.businessClasses[i]['children']) {
                var child = helperFunctionsService.findByIdInsideArray(metaData.businessClasses[i]['children'], model.businessClass.id);
                if (child) {
                  result.businessClassCode = $filter('json')(metaData.businessClasses[i]);
                  result.businessClass = $filter('json')(child);
                  result.childBusinessClasses = metaData.businessClasses[i]['children'];
                  break;
                }
              }
            }
          }
        }


        if (model.jurisdictionCountry) {
          result.jurisdictionCountry = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.countries, model['jurisdictionCountry']['id']));
        }

        if (model.legalFormClass) {
          var parent = helperFunctionsService.findByIdInsideArray(metaData.legalFormClasses, model.legalFormClass.id);
          if (parent) {
            result.legalFormClassCode = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.legalFormClasses, parent.id));
            result.legalFormClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.legalFormClasses, parent.id));
          } else {
            for (var i = 0; i < metaData.legalFormClasses.length; i++) {
              if (metaData.legalFormClasses[i]['children']) {
                var child = helperFunctionsService.findByIdInsideArray(metaData.legalFormClasses[i]['children'], model.legalFormClass.id);
                if (child) {
                  result.legalFormClassCode = $filter('json')(metaData.legalFormClasses[i]);
                  result.legalFormClass = $filter('json')(child);
                  result.childLegalClasses = metaData.legalFormClasses[i]['children'];
                  break;
                }
              }
            }
          }
        }

        if (model.memberRoles && model.memberRoles.length > 0) {

          var roleClassObj = [];
          var roleClassOldObj = {};

          for (var i = 0; i < model.memberRoles.length; i++) {

            var role = model.memberRoles[i];
            roleClassObj.push({
              id: role.roleClass.id,
              nameAz: role.roleClass.nameAz,
              nameEn: role.roleClass.nameEn,
              code: role.roleClass.code
            });
            roleClassOldObj[role.roleClass.id] = role.id;
          }
          result.memberRoles = {
            roleClassObj: roleClassObj,
            roleClassOldObj: roleClassOldObj
          };

        }
        if (model.creationDate) {
          if (model.creationDate.indexOf('+') > -1) {
            result.creationDateObj = model.creationDate;
            result.creationDateStr = helperFunctionsService.generateDate(new Date(model.creationDate));

          } else {
            result.creationDateObj = helperFunctionsService.parseDate(model.creationDate);
            result.creationDateStr = helperFunctionsService.generateDate(result.creationDateObj);
          }
        }

        if (model.registrationCertificate) {
          result.registrationCertificate = {
            id: model.registrationCertificate.id,
            number: model.registrationCertificate.number,
            series: model.registrationCertificate.series
          }
        }
      }

      // console.log(model);
      // console.log(angular.copy(result));

      return result;
    };

    //Return full tile data
    var getMetaData = function () {

      Loader.show(true);
      var deferred = $q.defer();
      var metaData = {};

      var getPhoneNumberTypes = function (value) {
        metaData.phoneNumberTypes = value;
      };

      var getCountries = function (value) {
        metaData.countries = value;
      };

      var getCurrencies = function (value) {
        metaData.currencies = value;
      };

      var getSignerPositions = function (value) {
        metaData.signerPositions = value;
      };
      var personClasses = function (value) {
        metaData.personClasses = value;
      };
      var businessClasses = function (value) {
        metaData.businessClasses = value;
      };
      var legalFormClasses = function (value) {
        metaData.legalFormClasses = value;
      };
      var getJuridicalPersonIdDocumentTypes = function (value) {
        metaData.juridicalPersonIdDocumentTypes = value;
      };
      var getNaturalPersonIdDocumentTypes = function (value) {
        metaData.naturalPersonIdDocumentTypes = value;
      };
      var getIdDocumentTypes = function (value) {
        metaData.idDocumentTypes = value;
      };

      var getAccountStatusCheckingClasses = function (value) {
        metaData.accountStatusCheckingClasses = value;
      };
      $q.all([
        referenceDataService.getPhoneNumberTypes().then(getPhoneNumberTypes),
        referenceDataService.getCountries().then(getCountries),
        referenceDataService.getCurrencies().then(getCurrencies),
        referenceDataService.getSignerPositions().then(getSignerPositions),
        referenceDataService.getPersonClasses().then(personClasses),
        referenceDataService.getBusinessClasses().then(businessClasses),
        referenceDataService.getLegalFormClasses().then(legalFormClasses),
        referenceDataService.getJuridicalPersonIdDocumentTypes().then(getJuridicalPersonIdDocumentTypes),
        referenceDataService.getNaturalPersonIdDocumentTypes().then(getNaturalPersonIdDocumentTypes),
        referenceDataService.getAccountStatusCheckingClasses().then(getAccountStatusCheckingClasses),
        referenceDataService.getIdDocumentTypes().then(getIdDocumentTypes)
      ])
        .then(function () {
          deferred.resolve(metaData);
          Loader.show(false);
        });

      return deferred.promise;
    };

    // Generate form configuration
    var generateFormConfig = function (rawData) {

      var configModel = {
        "isEnRequired": {
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "actual_address": {
          "fields": {
            "country": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "address_line": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "locations": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "postal_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "state": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            }
          },
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "bank_accounts": {
          "fields": {
            "account": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "bank_name": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "branch_name": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "correspondent_account": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "currency": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "iban": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "current_account": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "swiftbic": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "tin": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "bank_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "correspondent_bank_name": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "correspondent_bank_swift_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            }

          },
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "contact_information": {
          "fields": {
            "emails": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "phone_numbers": {
              "fields": {
                "number": {
                  "is_mandatory": false,
                  "is_visible": false,
                  "is_editable": false
                },
                "type": {
                  "is_mandatory": false,
                  "is_visible": false,
                  "is_editable": false
                }
              },
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            }
          },
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "documents": {
          "fields": {
            "document_type": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "doc_number": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "series": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            }
          },
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "legal_address": {
          "fields": {
            "country": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "address_line": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "locations": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "postal_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "state": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            }
          },
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "personal_information": {
          "fields": {
            "pin_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "TIN": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "azips_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },

            "issuer_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "member_roles": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "BSE_broker_code": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "BSE_issuer_id": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "position": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },

            "name": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "birthday": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },

            "creation_date": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "citizenship_country": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },

            "jurisdiction_country": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "first_name": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "last_name": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "middle_name": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "abbreviation": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },

            "note_on_fatca": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "subject_to_fatca": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "warrant_end_date": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "warrant_start_date": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "person_type": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "business_class": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "legal_form_class": {
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "id_card": {
              "fields": {
                "doc_number": {
                  "is_mandatory": false,
                  "is_visible": false,
                  "is_editable": false
                },
                "series": {
                  "is_mandatory": false,
                  "is_visible": false,
                  "is_editable": false
                }
              },
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            },
            "registration_certificate": {
              "fields": {
                "doc_number": {
                  "is_mandatory": false,
                  "is_visible": false,
                  "is_editable": false
                },
                "series": {
                  "is_mandatory": false,
                  "is_visible": false,
                  "is_editable": false
                }
              },
              "is_mandatory": false,
              "is_visible": false,
              "is_editable": false
            }

          },
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "settlement_agent": {
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "representatives": {
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        },
        "signers": {
          "is_mandatory": false,
          "is_visible": false,
          "is_editable": false
        }
      };
      for (var i = 0; i < rawData.length; i++) {

        var panel = rawData[i];
        var panelName = panel['name'];

        configModel[panelName]['is_mandatory'] = panel['mandatory'] && panel['mandatory'] !== 'false';
        configModel[panelName]['is_visible'] = panel['visible'] && panel['visible'] !== 'false';
        configModel[panelName]['is_editable'] = panel['editable'] && panel['editable'] !== 'false';

        if (panel.childs) {
          for (var j = 0; j < panel.childs.length; j++) {
            var field = panel.childs[j];
            var fieldName = field['name'];

            if (configModel[panelName].fields[fieldName]) {

              configModel[panelName].fields[fieldName]['is_mandatory'] = field['mandatory'] && field['mandatory'] !== 'false';

              configModel[panelName].fields[fieldName]['is_visible'] = field['visible'] && field['visible'] !== 'false';
              configModel[panelName].fields[fieldName]['is_editable'] = field['editable'] && field['editable'] !== 'false';

              if (field.childs) {
                for (var k = 0; k < field.childs.length; k++) {
                  var subField = field.childs[k];
                  var subFieldName = subField['name'];
                  if (configModel[panelName].fields[fieldName].fields[subFieldName]) {
                    configModel[panelName].fields[fieldName].fields[subFieldName]['is_mandatory'] = subField['mandatory'] && subField['mandatory'] !== 'false';
                    configModel[panelName].fields[fieldName].fields[subFieldName]['is_visible'] = subField['visible'] && subField['visible'] !== 'false';
                    configModel[panelName].fields[fieldName].fields[subFieldName]['is_editable'] = subField['editable'] && subField['editable'] !== 'false';
                  }
                }
              }
            }
          }
        }
      }

      return configModel;

    };

    var getPersonDataEntryFormBuffers = function () {
      return {
        document: {
          currentIndex: -1,
          form: {
            type: 0
          }
        },
        bankAccount: {
          currentIndex: -1,
          form: {},
          isBankCodeMandatory: false
        },
        signer: {
          data: {
            form: {
              isSigner: true,
              documents: [],
              bankAccounts: [],
              pinCode: {},
              TIN: {},
              idCard: {},
              phoneNumbers: [
                {
                  type: null,
                  number: ''
                }

              ],
              emails: [
                {value: ''}
              ]
            },
            config: {
              formName: 'npSignerForm',
              prefix: 'npSigner',
              documentFormName: 'npSignerDocumentForm',
              bankAccountFormName: 'npSignerBankAccountForm',
              fields: {},
              isSigner: true,
              isTINCodeMandatory: false
            },
            currentIndex: -1,
            documentAddWindow: {}
          }
        },
        user: {
          data: {
            form: {
              isUser: true,
              documents: [],
              bankAccounts: [],
              pinCode: {},
              TIN: {},
              idCard: {},
              phoneNumbers: [
                {
                  type: null,
                  number: ''
                }

              ],
              emails: [
                {value: ''}
              ]
            },
            config: {
              formName: 'npUserForm',
              prefix: 'npUser',
              documentFormName: 'npUserDocumentForm',
              bankAccountFormName: 'npUserBankAccountForm',
              fields: {},
              isUser: true,
              isTINCodeMandatory: false
            },
            currentIndex: -1,
            documentAddWindow: {}
          }
        },
        representative: {
          data: {
            showRepresentative: false,
            form: {
              isRepresentative: true,
              documents: [],
              bankAccounts: [],
              pinCode: {},
              TIN: {},
              idCard: {},
              phoneNumbers: [
                {
                  type: null,
                  number: ''
                }

              ],
              azipsCode: {id: null, value: null},
              settlementAgent: null,
              issuerCode: {id: null, value: null},
              emails: [
                {value: ''}
              ]
            },
            config: {
              formName: 'npRepresentativeForm',
              prefix: 'npRepresentative',
              documentFormName: 'npRepresentativeDocumentForm',
              bankAccountFormName: 'npRepresentativeBankAccountForm',
              fields: {},
              isRepresentative: true,
              isTINCodeMandatory: false,

            },
            currentIndex: -1,
            documentAddWindow: {}
          }
        }
      };
    };

    var removeFromArray = function (index, model) {
      model.splice(index, 1);
    };

    var addNewDocument = function (window, model, form, buffer) {

      var newDoc = angular.copy(buffer.form);
      newDoc.type = angular.fromJson(newDoc.type);
      if (!model) {
        model = [];
      }
      model.push(newDoc);
      buffer.form = {};
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
      window.close();
    };
    var showDocumentUpdateForm = function (index, model, window, buffer) {

      window.title(gettextCatalog.getString('Update Document'));
      var document = angular.copy(model[index]);
      document.type = $filter('json')(document.type) || null;
      buffer.form = document;
      buffer.currentIndex = index;
      window.open();
      window.center();
    };
    var updateDocument = function (window, model, form, buffer) {
      var document = angular.copy(buffer.form);
      document.type = angular.fromJson(document.type) || null;
      var index = buffer.currentIndex;
      model[index] = document;
      resetDocumentBuffer(window, form, buffer);

      window.close();

    };
    var resetDocumentBuffer = function (window, form, buffer) {
      buffer.currentIndex = -1;
      buffer.form = {};
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
      window.title(gettextCatalog.getString('Add New Document'));
    };

    var addNewEmail = function (model) {
      if (!model) {
        model = [];
      }
      model.push({
        value: ''
      });
    };
    var addNewPhoneNumber = function (model) {
      if (!model) {
        model = [];
      }
      model.push({
        type: null,
        number: ''
      });
    };

    //Manage Person Bank Accounts
    var addNewBankAccount = function (window, model, form, buffer) {
      var newAccount = angular.copy(buffer.form);
      newAccount.currency = angular.fromJson(newAccount.currency);
      if (!model) {
        model = [];
      }
      model.push(newAccount);
      buffer.form = {};
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
      window.title(gettextCatalog.getString('Add New Bank Account'));
      window.close();
    };
    var showBankAccountUpdateForm = function (index, model, window, buffer) {

      window.title(gettextCatalog.getString('Update Bank Account'));
      var document = angular.copy(model[index]);
      document.currency = $filter('json')(document.currency);
      buffer.form = document;
      buffer.currentIndex = index;


      window.open();
      window.center();
    };
    var updateBankAccount = function (window, model, form, buffer) {
      var document = angular.copy(buffer.form);
      document.currency = angular.fromJson(document.currency);
      var index = buffer.currentIndex;
      model[index] = document;
      resetBankAccountBuffer(window, form, buffer);
      window.close();
    };
    var resetBankAccountBuffer = function (window, form, buffer) {
      buffer.currentIndex = -1;
      buffer.form = {};
      window.title(gettextCatalog.getString('Add New Bank Account'));
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
    };


    var generateFullName = function (last, first, middle) {
      return last + " " + first + " " + middle;
    };
    var generateFullNameOfPerson = function (model) {
      return model.name && model.name.nameAz ? model.name.nameAz : generateFullName(
        model.lastName ? model.lastName.nameAz : "",
        model.firstName ? model.firstName.nameAz : "",
        model.middleName ? model.middleName.nameAz : ""
      );
    };
    var generateFullNameOfNaturalPerson = function (model) {
      return generateFullName(
        model.lastName ? model.lastName.nameAz : "",
        model.firstName ? model.firstName.nameAz : "",
        model.middleName ? model.middleName.nameAz : ""
      );
    };
    return {
      getMetaData: getMetaData,
      generateFormConfig: generateFormConfig,
      prepareFormData: prepareFormData,
      convertViewModelToForm: convertViewModelToForm,
      getFormModel: function () {
        return shareHolderFormModel;
      },
      getPersonDataEntryFormBuffers: getPersonDataEntryFormBuffers,

      // Document management
      addNewDocument: addNewDocument,
      removeFromArray: removeFromArray,
      showDocumentUpdateForm: showDocumentUpdateForm,
      updateDocument: updateDocument,
      resetDocumentBuffer: resetDocumentBuffer,

      addNewEmail: addNewEmail,
      addNewPhoneNumber: addNewPhoneNumber,

      // Bank account management
      addNewBankAccount: addNewBankAccount,
      showBankAccountUpdateForm: showBankAccountUpdateForm,
      updateBankAccount: updateBankAccount,
      resetBankAccountBuffer: resetBankAccountBuffer,
      generateFullName: generateFullName,
      generateFullNameOfPerson: generateFullNameOfPerson,
      generateFullNameOfNaturalPerson: generateFullNameOfNaturalPerson


    };


  }]);
