'use strict';

angular.module('cmisApp')
  .controller('PersonDataPrintController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'referenceDataService', 'Loader',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, referenceDataService, Loader) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
      };


    }]);
