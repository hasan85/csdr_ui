'use strict';

angular.module('cmisApp')
  .controller('ManageShareholderDataApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'referenceDataService', 'Loader', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, referenceDataService, Loader, gettextCatalog) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        labels: {
          accountInformation: gettextCatalog.getString('Account Information'),
          accountPrescription: gettextCatalog.getString('Account Prescription')
        }
      };
      Loader.show(true);
      referenceDataService.getApprovalTaskDecisions().then(function (data) {
        $scope.config.approvalTaskDecisions = data;
        Loader.show(false);
      });

    }]);
