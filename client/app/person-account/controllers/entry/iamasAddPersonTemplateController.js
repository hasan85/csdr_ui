'use strict';

angular.module('cmisApp')
  .controller('IamasAddPersonTemplateController', ['$scope', '$windowInstance', 'personFindService', 'id', 'operationState', 'appConstants',
    'task', 'SweetAlert', '$rootScope',
    function ($scope, $windowInstance, personFindService, id, operationState, appConstants, task, SweetAlert, $rootScope) {
    window.$scope = $scope;
      var buttons = {
        complete: {
          click: function () {
            $scope.config.completeTask($scope.data);
          }
        }
      };
      if (operationState === 'findoperation') {
          buttons = {
            unclaim: null,
            saveAsDraft: null,
            complete: {
              disabled: true,
              click: function () {
                if ($scope.data.searchResult) {
                  personFindService.selectPerson($scope.data);
                  $windowInstance.close();
                }
              }
            }
          };
        }

      $scope.data = {
      };

      $scope.getPerson = function () {
        $scope.config.form.name.$submitted = true;
        if ($scope.config.form.name.$valid) {
          // your code
          console.log('Form is valid');
          personFindService.PersonalCardbyId($scope.data).then(function (person) {
            console.log(person);
            if (person.success !== 'false') {
              $scope.data.searchResult = person.data;
              $scope.config._buttons.complete.disabled = false;
            } else {
              SweetAlert.swal('', person.message,'error');
            }
          });
        }
      };


      // Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "iamasForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: buttons
        };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask({});
      });

    }]);
