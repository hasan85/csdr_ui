'use strict';

angular.module('cmisApp')
  .controller('naturalPersonManagementEntryController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
      'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState',
      'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',
      function ($scope, $windowInstance, id, appConstants, operationService,
                personFindService, instrumentFindService, helperFunctionsService,
                operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog) {
        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                $scope.config.completeTask({});
              }
            }
          }
        };
        $scope.findIamasPerson = function () {
          personFindService.findIamasPerson().then(function (data) {
            $scope.personData = data.searchResult;
            $scope.errorLog = false
          });
        };


        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask({});
        });

        // wizard configuration
        var vm = {};

        // $scope.activeTabStyle = {
        //   'border-color': '5px solid #ad102a'
        // };
        vm.currentStep = 1;
        vm.user = {};
        vm.steps = [
          {
            step: 1,
            name: "1",
            template: "app/person-account/templates/partials/np/panels/step1.html"
          },
          {
            step: 2,
            name: "2",
            template: "app/person-account/templates/partials/np/panels/step2.html"
          },
          {
            step: 3,
            name: "3",
            template: "app/person-account/templates/partials/np/panels/step3.html"
          }
        ];

        $scope.isValid = function (step_num) {
          var step_num_int = parseInt(step_num, 10);
          if (!$scope.personData) $scope.errorLog = true;
          switch (step_num_int) {
            case 1:
              return !!$scope.personData;
            default:
              return false
          }
        };

        vm.gotoStep = function (newStep) {
          if ($scope.isValid(vm.currentStep)) {
            vm.currentStep = newStep;
          }
        };

        vm.getStepTemplate = function () {
          for (var i = 0; i < vm.steps.length; i++) {
            if (vm.currentStep === vm.steps[i].step) {
              return vm.steps[i].template;
            }
          }
        };

        vm.save = function () {
        };

        $scope.vm = vm
      }]);
