'use strict';

angular.module('cmisApp')
  .controller('ManageExchangeRateApprovalController',
  ['$scope', '$windowInstance', 'id', 'task', 'appConstants', 'taskId', 'operationState',
    function ($scope, $windowInstance, id, task, appConstants, taskId, operationState) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };

    }]);
