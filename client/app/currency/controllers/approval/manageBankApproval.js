'use strict';

angular.module('cmisApp')
  .controller('ManageBankInfoApprovalController',
    ['$scope', '$windowInstance', 'id', 'taskId', 'appConstants', 'operationState', 'task',
      function ($scope, $windowInstance, id, taskId, appConstants, operationState, task) {

        $scope.config = {
          screenId: id,
          taskId: taskId,
          task: task,
          operationType: appConstants.operationTypes.approval,
          window: $windowInstance,
          state: operationState
        };

      }]);
