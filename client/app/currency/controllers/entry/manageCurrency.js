'use strict';

angular.module('cmisApp')
  .controller('ManageCurrencyController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationState', 'task', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationState, task, SweetAlert, gettextCatalog) {

      $scope.config = {
        screenId: id,
        operationType: appConstants.operationTypes.entry,
        taskKey: id,
        task: task,
        window: $windowInstance,
        form: {
          name: "currencyManageForm",
          data: {
            isBase: false
          }
        },
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask($scope.config.form.data);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      $scope.config.form.data = angular.fromJson(task.draft);
      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.config.form.data);
      });

    }]);
