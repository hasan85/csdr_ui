'use strict';

angular.module('cmisApp')
  .controller('ManageBankInfoController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.metaData = {};
        $scope.data = angular.fromJson(task.draft);



        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });

        $scope.data.bankInfos.forEach(function(obj){
          obj.correspondentAccounts = angular.isArray(obj.correspondentAccounts) ? obj.correspondentAccounts: [obj.correspondentAccounts]
        })

        console.log($scope.data)
        $scope.prepareData = function (data) {
          return data
        }

        setTimeout(function () {
          $windowInstance.widget.setOptions({width: '80%'})
        }, 200)


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.formValid()) {
                //
                var data = $scope.prepareData(angular.copy($scope.data));
                $scope.config.completeTask(data);
                //
                // } else {
                //   console.log($scope)
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };




        $scope.removeBankInfo = function(index){
          $scope.data.bankInfos.splice(index,1)
        };




        function openForm(fn) {
          $kWindow.open({
            title: '',
            actions: ['Close'],
            isNotTile: true,
            maxWidth: 600,
            height: '400px',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/currency/templates/manage-bank-info-form.html',
            controller: 'ManageBankInfoFormController',
            resolve: {
              bla: function () {
                return $scope.dataForm
              },
              submit: function(){
                return fn
              }
            }
          });
        }


        $scope.editBankInfo = function(index){
          $scope.dataForm = $scope.data.bankInfos[index];
          openForm(function (data) {
            $scope.data.bankInfos[index] = data
          })
        };


        $scope.addBankInfo = function(){
          $scope.dataForm = {
            name: "",
            SWIFTBIC: "",
            Code: "",
            TIN: "",
            correspondentAccounts: [
              {
                correspondentAccount: "",
                currencyCode: ""
              }
            ]
          }

          openForm(function (data) {
            $scope.data.bankInfos.push(data)
          })
        };


        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;

angular.module('cmisApp')
  .controller('ManageBankInfoFormController',
    ['$scope', '$windowInstance', 'bla', 'personFindService', 'submit', 'referenceDataService', function ($scope, $windowInstance, data, personFindService, submit, referenceDataService) {

      $scope.data = angular.copy(data);

      $scope.addCorrespondentAccount = function(){
        $scope.data.correspondentAccounts.push({
          correspondentAccount: "",
          currencyCode: ""
        })
      };

      $scope.removeCorrespondentAccount = function(index){
        $scope.data.correspondentAccounts.splice(index,1)
      };


      $scope.submit = function(){
        submit($scope.data);
        $windowInstance.close()
      };


      $scope.close = function(){
        $windowInstance.close()
      }

    }]);


