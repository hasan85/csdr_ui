'use strict';

angular.module('cmisApp')
  .controller('ManageExchangeRateController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'referenceDataService', 'operationState', '$filter',
    'task', 'SweetAlert', 'gettextCatalog', 'helperFunctionsService',
    function ($scope, $windowInstance, id, appConstants, operationService, referenceDataService, operationState, $filter,
              task, SweetAlert, gettextCatalog, helperFunctionsService) {

      var formTemplate = {
        currency: null,
        rateUnit: null,
        rateValue: null,
        rateDateTimeStr: null,
        rateDateTimeObj: null
      };

      var initializeFormData = function (draft) {
        if (!angular.isArray(draft)) {
          draft = [
            angular.copy(formTemplate)
          ]
        }
        return draft;
      };

      $scope.exchangeRates = initializeFormData(angular.fromJson(task.draft));

      // Prepare form data before send to backend
      var prepareFormData = function (formData) {

        var processedFormData = [];

        for (var i = 0; i < formData.length; i++) {

          var buff = {};

          buff['rateDateTime'] = helperFunctionsService.generateDateTime(formData[i].rateDateTimeObj);


          if (formData[i].currency) {
            buff['currency'] = angular.fromJson(formData[i].currency);
          }

          buff['rateUnit'] = formData[i].rateUnit;
          buff['rateValue'] = formData[i].rateValue;

          processedFormData.push(buff);
        }
        return processedFormData;

      };

      // Get metadata
      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData = {
          currencies: data
        };

        if (angular.isArray($scope.exchangeRates)) {
          $scope.exchangeRates.forEach(function (item) {

            if (item.currency && item.currency.id) {
              item.currency = $filter('json')(helperFunctionsService.findByIdInsideArray( $scope.metaData.currencies, item.currency['id']));
            }
            if (item.rateDateTime && !item.rateDateTimeObj) {
              item.rateDateTimeObj = helperFunctionsService.parseDate(item.rateDateTime);
              item.rateDateTimeStr = helperFunctionsService.generateDateTime(item.rateDateTimeObj);
            }
          })
        }
      });

      // Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        window: $windowInstance,
        operationType: appConstants.operationTypes.entry,
        form: {
          name: "exchangeRateManageForm",
          data: {}
        },
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var buf = prepareFormData(angular.copy($scope.exchangeRates));
                $scope.config.completeTask(buf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
      });

      // Listen task's save event
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.exchangeRates);
      });

      $scope.addNewExchangeRate = function (item) {
        item.push(angular.copy(formTemplate));
      };

      $scope.removeExchangeRate = function (index) {
        $scope.exchangeRates.splice(index, 1);
      };
    }]);
