"use strict";


angular.module("cmisApp")
  .factory('managementService',
    ["$http", "$q", "$kWindow", 'gettextCatalog', 'helperFunctionsService', 'appConstants', 'referenceDataService', "Loader", "$filter",
      function ($http, $q, $kWindow, gettextCatalog, helperFunctionsService, appConstants, referenceDataService, Loader, $filter) {
        var deferred = $q.defer();

        function uPreparePerson(person, metaData) { // utility function
          if (!person.name) person.name = {};
          if (!person.address) person.address = {};
          if (!person.country) {
            person.country = {};
          } else {
            person.citizenshipCountry = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.countries, person.country['id']));
          }
          if (!person.email) person.email = {};
          if (!person.emails) person.emails = [{value: ''}];
          if (!person.phoneNumbers) {
            person.phoneNumbers = [{type: null, number: ''}];
          } else if (person.phoneNumbers && person.phoneNumbers.length > 0) {
            for (var i = 0 ; i < person.phoneNumbers.length; i++) {
              if (person.phoneNumbers[i]['type']) {
                person.phoneNumbers[i]['type'] = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.phoneNumberTypes, person.phoneNumbers[i]['type']['id']));
              } else {
                person.phoneNumbers[i]['type'] = null;
              }
            }
          }
          if (!person.fathername) person.fathername = {};
          if (!person.idCard) person.idCard = {};
          if (!person.pinCode) person.pinCode = {};
          if (!person.surname) person.surname = {};
          if (!person.user) person.user = {};
          if (!person.actualAddress) person.actualAddress = {};
          if (!person.legalAddress) person.legalAddress = {};
          return person;
        }

        var fixPersonData = function (data, metaData) {
          var foundPersons = [];
          var iamasPerson = data.fromIAMAS;
          var dataBasePerson = data.fromCSDR;

          // address, country, email, fathername, idCard, name, pinCode, surname, user

          if (data.fromIAMASByPin) {
            data.fromIAMASByPin.forEach(function (item) {
              uPreparePerson(item, metaData);
              foundPersons.push(item);
            });
          }

          if (iamasPerson) {
            uPreparePerson(iamasPerson, metaData);
          }
          ;
          if (dataBasePerson) {
            uPreparePerson(dataBasePerson, metaData);
          }


          return {
            foundPersons: foundPersons,
            iamasPerson: iamasPerson,
            dataBasePerson: dataBasePerson
          };
        };

        var findPersonFromIamas = function (idCard) {
          return $http.post('/api/reportsDataServices/findPersonFromIamas/', {
            idCard: idCard
          }).then(function (response) {
            return response.data;
          });
        };
        var findPersonFromCSDR = function (idCard) {
          return $http.post("/api/reportsDataServices/findPersonFromCSDR", {
            idCard: idCard
          }).then(function (response) {
            return response.data;
          });
        };
        var getPersonSettings = function () {
          return $http.post("/api/records/getPersonSettings")
            .then(function (response) {
              return response.data;
            });
        };
        var personTemplate = {
          address: {},
          country: {},
          email: {},
          fathername: {},
          idCard: {},
          name: {},
          pinCode: {},
          surname: {},
          user: {}
        };

        var userTemplate = {
          person: {},
          authMethod: "WithPassword", // WithAsanImza
          isSuspended: false,
          operationType: "Create", // Create, Edit, Delete
          password: null,
          username: null
        };

        var deponentTemplate = {
          serviceConsumerId: null, // getDeponent -> ID
          contractDate: null,
          contractNumber: null,
          isOnGoing: false,
          isPaymentPending: false,
          isSuspended: false,
          isTerminated: false,
          operationType: null,
          paidTillDate: null,
          serviceConsumerName: null,
          users: []
        };


        var getMetaData = function () {
          deferred = $q.defer();

          var metaData = {};

          function getCountries(data) {
            metaData.countries = data;
          }

          function getNaturalPersonIdDocumentTypes(data) {
            metaData.documentTypes = data;
          }

          $q.all([
            referenceDataService.getCountries().then(getCountries),
            referenceDataService.getNaturalPersonIdDocumentTypes().then(getNaturalPersonIdDocumentTypes)
          ])
            .then(function () {
              deferred.resolve(metaData);
            });

          return deferred.promise;
        };


        var isUserExist = function (userData) {
          return $http.get('/api/reportsDataServices/findPersonFromDataBase', userData).then(function (response) {
            return response.data;
          });
        };
        var findDeponent = function (isIssuer) {
          return $kWindow.open({
            title: 'Axtar',
            actions: ['Close'],
            isNotTile: true,
            width: '100%',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/deponent-search.html',
            controller: 'DeponentFindController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return isIssuer ? '/api/reportsDataServices/getIssuers' : '/api/reportsDataServices/getDeponents';
              },
              configParams: function () {
                return {
                  criteriaForm: {
                    personType: {
                      isVisible: false,
                      default: appConstants.personClasses.juridicalPerson
                    }
                  }
                }
              }
            }
          }).result;
        };

        var findPerson = function (deponent, url) {
          return $kWindow.open({
            title: "Şəxsi seç",
            actions: ['Close'],
            isNotTile: true,
            width: '100%',
            height: '80%',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/common/templates/shareholder-person-search.html',
            controller: 'ShareholderPersonSearchController',
            resolve: {
              id: function () {
                return 0;
              },
              searchUrl: function () {
                return url ? url : 'api/reportsDataServices/getShareholderPersons';
              },
              personForm: function () {
                return {
                  contractID: deponent.ID ? String(deponent.ID) : null
                }
              }
            }
          }).result;
        };
        var editUser = function (form) {
          deferred = $q.defer();
          $kWindow.open({
            title: form.title,
            actions: [],
            isNotTile: true,
            width: form.width ? form.width : "50%",
            height: form.height ? form.height : "50%",
            modal: true,
            templateUrl: 'app/management/templates/partials/user_edit.html',
            controller: 'EditUserController',
            resolve: {
              id: function () {
                return 0;
              },
              form: function () {
                return form;
              }
            }
          });
          return deferred.promise;
        };

        var selectData = function (data) {
          deferred.resolve(data);
          deferred = $q.defer();
        };

        return {
          findPersonFromIamas: findPersonFromIamas,
          getMetaData: getMetaData,
          userTemplate: angular.copy(userTemplate),
          deponentTemplate: angular.copy(deponentTemplate),
          isUserExist: isUserExist,
          personTemplate: angular.copy(personTemplate),
          fixPersonData: fixPersonData,
          findPerson: findPerson,
          selectData: selectData,
          findDeponent: findDeponent,
          editUser: editUser,
          getPersonSettings: getPersonSettings,
          findPersonFromCSDR: findPersonFromCSDR,
          uPreparePerson: uPreparePerson
        };
      }]);
