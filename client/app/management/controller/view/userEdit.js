'use strict';

angular.module('cmisApp')
  .controller('EditUserController', ['$scope', '$windowInstance', 'personFindService',
    "form", "$http", "SweetAlert", "Loader", "managementService",
    function ($scope, $windowInstance, personFindService, form, $http, SweetAlert, Loader, managementService) {
      $scope.minDate = new Date();

      $scope.buffers = {
        editDeponent: {
          config: {
            form: {
              name: "editDeponent"
            }
          }
        },
        addDeponent: {
          config: {
            form: {
              name: "addDeponent"
            }
          }
        },
        addUser: {
          config: {
            form: {
              name: "addUser"
            }
          }
        }
      };
      var person = {
        isUserSelected: false
      };
      $scope.data = {
        deponent: {},
        person: angular.copy(person),
        edit: {}
      };
      $scope.forms = {
        editDeponent: "editDeponent",
        addUser: "addUser",
        addDeponent: "addDeponent"
      };

      $scope.form = form;
      if (form.deponent) {
        $scope.data.edit.isTerminated = form.deponent.isTerminated;
        $scope.data.edit.isSuspended = form.deponent.isSuspended;
      }

      $scope.findDeponent = function () {
        managementService.findDeponent(form.isIssuer).then(function (data) {
          if (!data) return;
          if (data.ID) {
            $scope.data.deponent.foundDeponent = data;
            $scope.data.deponent.isFound = true;
          }
        });
      };


      $scope.updatePerson = function () {
        $scope.buffers[form.whichForm].config.form.name.$submitted = true;
        if ($scope.buffers[form.whichForm].config.form.name.$valid) {
          managementService.selectData($scope.data);
          $windowInstance.close();
        }

      };
      $scope.findPerson = function (deponent) {
        managementService.findPerson(deponent, form.isIssuer ? 'api/reportsDataServices/getIssuerPersons' : undefined).then(function (val) {
          console.log(val);
          $scope.data.person = angular.copy(person);
          if (val) {

            $scope.data.person.isUserSelected = true;

            if (!val.email) val.email = {};
            if (!val.user) val.user = {};
            if (!val.pinCode) val.pinCode = {};
            if (!val.idCard) val.idCard = {};
            if (!val.fathername) val.fathername = {};
            if (!val.name) val.name = {};
            if (!val.address) val.address = {};
            if (!val.surname) val.surname = {};

            $scope.data.person.selected = val;
          }
        });
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
