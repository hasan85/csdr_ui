'use strict';

angular.module('cmisApp')
  .controller('ShareholderUiManagementPrintController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', "helperFunctionsService",
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, helperFunctionsService) {

      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };

      $scope.passwords = angular.fromJson(task.draft);

      $scope.print = function (user) {
        var password = user.password ? user.password : "İstifadəçi artıq var";
        var template = "<table style='border:1px solid black;border-collapse:collapse'>" +
          "<tr style='border:1px solid black;'>" +
          "<th style='border:1px solid black;'>İstifadəçi adı</th>" +
          "<th style='border:1px solid black;'>Parol</th>" +
          "</tr>" +
          "<tr style='border:1px solid black;'>" +
          "<td style='border:1px solid black;padding:5px;'>"+ user.username +"</td>" +
          "<td style='border:1px solid black;padding:5px;'>"+ password +"</td>" +
          "</tr>" +
          "</table>";
        helperFunctionsService.printHtml(template);
      };

    }]);
