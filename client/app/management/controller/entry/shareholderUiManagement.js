"use strict";

angular.module("cmisApp")
  .controller('ShareholderUiManagmentController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'task', 'Loader', 'SweetAlert', 'gettextCatalog', 'operationState',
      "personFindService", "referenceDataService", "managementService", "$rootScope",
      function ($scope, $windowInstance, id, appConstants, task, Loader, SweetAlert, gettextCatalog, operationState, personFindService,
                referenceDataService, managementService, $rootScope) {

        // initialize scope variables. [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          form: {
            name: "AccountForm"
          },
          buttons: {
            complete: {
              click: function () {
                var dat = prepare($scope.unTouchedDeponentData, angular.copy($scope.deponentData));
                $scope.config.completeTask(dat);
              }
            }
          }
        };

        function prepare(unTouchedDeponentData, deponentData) {
          var dat = angular.copy(unTouchedDeponentData);
          dat.dataChange = [];
          deponentData.forEach(function (item) {
            if (item.isTerminated) {
              if (!item.isManuallyAdded) {
                item.operationType = "Delete";
                item.users = [];
                dat.dataChange.push(item);
              }
            } else if (item.operationType === "Create") {
              dat.dataChange.push(item);
            } else {
              checkEquality(item, dat);
            }
          });
          return dat;
        }

        function checkEquality(item, dat) {
          var oldItem = null;
          dat.dataEntry.forEach(function (old) {
            if (item.ID === old.ID) {
              oldItem = old;
            }
          });

          if (!oldItem) return;

          function edited(field) {
            if (oldItem[field] !== item[field]) {
              item.operationType = "Edit";
              item[field + "Changed"] = {
                edited: true,
                oldValue: oldItem[field],
                newValue: item[field]
              };
            }
          }


          edited("paidTillDate");
          edited("isSuspended");
          edited("isPaymentPending");
          edited("isOnGoing");

          getState(item, oldItem, true);

          if (item.operationType) {
            dat.dataChange.push(item);
          } else {
            var pushDeponent = false;
            item.users.forEach(function (user) {
              if (user.operationType === "Create") {
                user.userCreated = true;
                pushDeponent = true;
              } else if (user.isTerminated) {
                user.operationType = "Delete";
                pushDeponent = true;
              } else {
                oldItem.users.forEach(function (oldUser) {
                  if (user.ID === oldUser.ID) {
                    if (user.isSuspended !== oldUser.isSuspended) {
                      user.operationType = "Edit";
                      user.suspendedChanged = {
                        oldValue: oldUser.isSuspended ? "Dondurulub" : "Aktiv",
                        newValue: user.isSuspended ? "Dondurulub" : "Aktif"
                      };
                      pushDeponent = true;
                    }
                    return;
                  }
                });
              }
            });
            if (pushDeponent) {
              item.userWasChanged = true;
              dat.dataChange.push(item);
            }

          }


        }


        $scope.metaData = {};
        $scope.buffers = {
          windows: {},
          editForm: { // buffers.editForm.config.form.name
            config: {
              form: {
                name: "editForm"
              }
            }
          },
          deponentAddForm: { // buffers.deponentAddForm.config.form.name
            config: {
              form: {
                name: "deponentAddForm"
              }
            }
          },
          userAddForm: {  // buffers.userAddForm.config.form.name
            config: {
              form: {
                name: "userAddForm"
              }
            }
          }
        };
        $scope.constants = {
          suspended: "Suspended",
          ongoing: "OnGoing"
        };
        $scope.stateLabel = {
          isSuspended: "Suspended",
          isTerminated: "Terminated",
          isPaymentPending: "Payment Pending",
          isOnGoing: "On going"
        };

        function getState(deponent, oldItem, isConfirming) {
          var isSuspended = deponent.isSuspended;
          var isTerminated = deponent.isTerminated;
          var isPaymentPending = deponent.isPaymentPending;
          var isOnGoing = deponent.isOnGoing;
          var state = $scope.stateLabel[isTerminated ? 'isTerminated' : isSuspended ? 'isSuspended' : isPaymentPending ? 'isPaymentPending' : 'isOnGoing'];

          if (isConfirming) {
            if (deponent.initialState !== state) {
              deponent.stateChanged = true;
              deponent.newState = state;
            }
          } else {
            if (!deponent.initialState) {
              deponent.initialState = state;
            }
          }
          return state;
        }

        $scope.getState = getState;


        $scope.unTouchedDeponentData = angular.fromJson(task.draft);
        $scope.deponentData = angular.copy($scope.unTouchedDeponentData.dataEntry);
        $scope.deponentData.forEach(function (item) {
          getState(item);
        });


        // [[ pagination
        var viewBy = 10;
        $scope.paginationOptions = {
          viewBy: viewBy,
          itemsPerPage: viewBy,
          currentPage: 1,
          totalItems: $scope.deponentData.length,
          maxSize: 5
        };
        // ]]


        $scope.showDetail = function (obj) {
          if (obj.isTerminated) return;
          if (obj.users.length) obj.showDetail = !obj.showDetail;
        };
        // edit deponent
        $scope.editDeponent = function (deponent) {
          managementService.editUser({
            whichForm: "editDeponent",
            title: "Müqavilənin redakte edilməsi",
            deponent: deponent
          }).then(function (data) {
            if (data.edit.startDateObj) {
              deponent.paidTillDate = data.edit.startDateObj;
              deponent.isOnGoing = true;
              deponent.isPaymentPending = false;
            }

            if (data.edit.isTerminated) {
              deponent.isTerminated = true;
              deponent.isPaymentPending = false;
              deponent.isOnGoing = false;
              deponent.isSuspended = false;
            } else {
              if (data.edit.isSuspended) {
                deponent.isSuspended = true;
                deponent.isTerminated = false;
                deponent.isPaymentPending = false;
                deponent.isOnGoing = false;
              } else {
                deponent.isSuspended = false;
                deponent.isTerminated = false;
                deponent.isPaymentPending = false;
                deponent.isOnGoing = false;
                if (deponent.paidTillDate) {
                  deponent.isOnGoing = true;
                } else {
                  deponent.isPaymentPending = true;
                }


              }
            }
          });
        };

        //suspend user
        $scope.suspendUser = function (user, isSuspended) {
          console.log("SUSPEND_USER", user, isSuspended);
          if (isSuspended) {
            user.isTerminated = false;
          }
        };

        // remove user
        $scope.removeUser = function (user, deponent, isRemoved) {
          console.log("REMOVE_USER", user, isRemoved);
          if (user.isManuallyAdded) {
            deponent.users.splice(deponent.users.indexOf(user), 1);
            return;
          }
          if (isRemoved) {
            user.isSuspended = false;
          }
        };

        // add deponent
        $scope.addDeponent = function () {
          managementService.editUser({
            whichForm: "addDeponent",
            title: "Müqavilənin əlavə edilməsi",
            height: "80%",
            isIssuer: task.processKey === "issuer-ui-management"
          }).then(function (data) {
            var isDuplicateFound = false;
            $scope.deponentData.forEach(function (item) {
              if (data.deponent.foundDeponent.ID === (item.serviceConsumerId.toString())) {
                isDuplicateFound = true;
                SweetAlert.swal("", "Müqavilə mövcuddur.", "warning");
                return;
              }
            });
            if (isDuplicateFound) return;
            // data bind
            var newDeponent = angular.copy(managementService.deponentTemplate);
            createNewContract(newDeponent, data);
            if (data.deponent.isPaid) {
              newDeponent.paidTillDate = data.deponent.paidUntilDateObj;
              newDeponent.isOnGoing = true;
            } else {
              newDeponent.isPaymentPending = true;
            }
            newDeponent.isManuallyAdded = true;
            $scope.deponentData.unshift(newDeponent);
          });
        };

        function createNewContract(newDeponent, data) {
          newDeponent.contractDate = data.deponent.contractDateObj;
          newDeponent.serviceConsumerName = data.deponent.foundDeponent.name;
          newDeponent.serviceConsumerId = +data.deponent.foundDeponent.ID;
          newDeponent.contractNumber = data.deponent.contractNumber;
          newDeponent.isJuridical = data.deponent.foundDeponent.isJuridical;
          newDeponent.isIamas = data.deponent.foundDeponent.isIamas;
          newDeponent.countryId = data.deponent.foundDeponent.countryId;
          newDeponent.isManuallyAdded = true;
          newDeponent.operationType = "Create";
        } // helper reference function. binging.


        // add user.
        $scope.addUser = function (deponent) {
          managementService.editUser({
            whichForm: "addUser",
            title: "İstifadəçinin əlavə edilməsi",
            height: "80%",
            isJuridical: deponent.isJuridical,
            metaData: $scope.metaData,
            deponent: deponent,
            task: task,
            isIssuer: task.processKey === "issuer-ui-management"
          }).then(function (data) {
            var selectedData = data.person;
            var newUser = angular.copy(managementService.userTemplate);
            newUser.isManuallyAdded = true;
            newUser.person = selectedData.selected;
            var userExists;
            deponent.users.forEach(function (item) {
              if (item.person.ID == newUser.person.ID) {
                userExists = true;
              }
            });

            if (userExists) {
              SweetAlert.swal("", "Bu deponent üçün artıq hesab yaranıb", "error");
              return;
            }
            if (selectedData.isAsan) {
              newUser.authMethod = "WithAsanImza";
            } else {
              newUser.email = selectedData.selected.email;
              newUser.username = selectedData.selected.user.value;

            }
            deponent.users.unshift(newUser);
            console.log("ADD_USER", data, deponent, newUser);

          });
        };

        $scope.getName = function (child) {
          var surname = child.person.surname;
          var name = child.person.name;
          var surn = null;
          if (surname['name' + $rootScope.lnC.toUpperCase()] || name['name' + $rootScope.lnC.toUpperCase()]) {
            surn = surname['name' + $rootScope.lnC.toUpperCase()] + " " + name['name' + $rootScope.lnC.toUpperCase()];
          } else {
            surn = surname['name' + $rootScope.lnC] + " " + name['name' + $rootScope.lnC];
          }
          child.fullName = surn;
          return surn;
        };


        // close window.
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

        $scope.$watch("deponentData.length", function (newVal) {
          $scope.paginationOptions.totalItems = newVal;
        });


        $scope.$on("saveTask", function () {
          $scope.config.saveTask({
            dataChange: [],
            dataEntry: angular.copy($scope.deponentData)
          });
        });

      }
    ]);
