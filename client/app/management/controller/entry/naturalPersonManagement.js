"use strict";

angular.module("cmisApp")
  .controller('NaturalPersonManagementController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'task', 'Loader', 'SweetAlert', 'gettextCatalog', 'operationState',
      "personFindService", "referenceDataService", "managementService", "$rootScope", "managePersonService", 'helperFunctionsService',
      function ($scope, $windowInstance, id, appConstants, task, Loader, SweetAlert, gettextCatalog, operationState, personFindService,
                referenceDataService, managementService, $rootScope, managePersonService, helperFunctionsService) {
        window.$scope = $scope;
        // initialize scope variables. [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          form: {},
          buttons: {
            complete: {
              click: function () {
                $scope.data.config.formName.$submitted = true;
                if ($scope.data.config.formName.$invalid) {
                  SweetAlert.swal("", "Forma doğrulama xətası", "error");
                  return;
                }
                var completeData = null;

                if ($scope.data.form.isNoMVDOcument || $scope.data.manualInputEnabled) {
                  if (!$scope.data.form.actualAddress.value) {
                    $scope.data.form.actualAddress = null;
                  }
                  if (!$scope.data.form.legalAddress.value) {
                    $scope.data.form.legalAddress = null;
                  }
                  completeData = $scope.data.form;
                } else {
                  $scope.data.form.iamas.iamasPerson.phoneNumbers = $scope.data.form.phoneNumbers;
                  $scope.data.form.iamas.iamasPerson.emails = $scope.data.form.emails;
                  $scope.data.form.iamas.iamasPerson.countryId = $scope.data.form.countryId;
                  $scope.data.form.iamas.iamasPerson.actualAddress = $scope.data.form.actualAddress;
                  if ($scope.data.form.iamas.dataBasePerson) {
                    $scope.data.form.iamas.iamasPerson.ID = $scope.data.form.iamas.dataBasePerson.ID;
                    $scope.data.form.iamas.iamasPerson.fathername.id = $scope.data.form.iamas.dataBasePerson.fathername.id;
                    $scope.data.form.iamas.iamasPerson.idCard.id = $scope.data.form.iamas.dataBasePerson.idCard.id;
                    $scope.data.form.iamas.iamasPerson.legalAddress.id = $scope.data.form.iamas.dataBasePerson.legalAddress.id;
                    $scope.data.form.iamas.iamasPerson.name.id = $scope.data.form.iamas.dataBasePerson.name.id;
                    $scope.data.form.iamas.iamasPerson.pinCode.id = $scope.data.form.iamas.dataBasePerson.pinCode.id;
                    $scope.data.form.iamas.iamasPerson.surname.id = $scope.data.form.iamas.dataBasePerson.surname.id;
                    $scope.data.form.iamas.iamasPerson.country.id = $scope.data.form.iamas.dataBasePerson.country.id;
                  }
                  if ($scope.data.form.iamas.iamasPerson.actualAddress.value === null) {
                    $scope.data.form.iamas.iamasPerson.actualAddress = null;
                  }
                  completeData = $scope.data.form.iamas.iamasPerson;
                }
                if (completeData.phoneNumbers) {
                  var arr = [];
                  completeData.phoneNumbers.forEach(function (phonenumber) {
                    if (phonenumber.number !== "" && phonenumber.type) {
                      phonenumber.type = angular.fromJson(phonenumber.type);
                      arr.push(phonenumber);
                    }
                  });
                  completeData.phoneNumbers = arr;
                }
                if (completeData.emails) {
                  var arr = [];
                  completeData.emails.forEach(function (email) {
                    if (email.value !== "") {
                      arr.push(email);
                    }
                  });
                  completeData.emails = arr;
                }

                if (completeData.birthDateObj) {
                  completeData.birthDate = helperFunctionsService.generateDateTime(completeData.birthDateObj);
                }

                $scope.config.completeTask({
                  dataEntry: completeData
                });
              }
            }
          }
        };


        var personDataTemplate = {
          name: {nameAz: null},
          surname: {nameAz: null},
          fathername: {nameAz: null},
          birthDate: null,
          idCard: {
            series: "AZE",
            number: null
          },
          pinCode: {value: null},
          passport: {},
          countryId: null,
          legalAddress: {value: null},
          actualAddress: {value: null},
          bankAccounts: [],
          emails: [
            {value: ''}
          ],
          phoneNumbers: [
            {
              type: null,
              number: ''
            }
          ],
          isIamas: null
        };

        function setConfig() {
          $scope.data = {
            form: angular.copy(personDataTemplate),
            bankAccountWindow: {},
            config: {
              formName: "npMainForm",
              labels: {
                actualAddress: gettextCatalog.getString('Actual Address'),
                legalAddress: gettextCatalog.getString('Legal Address')
              }
            }
          };
        }

        setConfig();

        $scope.metaData = {};
        $scope.buffers = {};

        Loader.show(true);
        referenceDataService.getCountries().then(function (countries) {
          referenceDataService.getCurrencies().then(function (currencies) {
            referenceDataService.getPhoneNumberTypes().then(function (phones) {
              $scope.metaData.countries = countries;
              $scope.metaData.currencies = currencies;
              $scope.metaData.phoneNumberTypes = phones;
              countries.forEach(function (item) {
                if (item.code == "AZ") {
                  $scope.data.form.countryId = item.id;
                  return;
                }
              });
              managementService.getPersonSettings().then(function (data) {
                if (data.success === "true") {
                  $scope.data.manualInputEnabled = data.data.manualInputEnabled === "true" ? true : false;
                }
                Loader.show(false);
              });
            });
          });
        });


        $scope.residencyChange = function (isNotResident) {
          $scope.data.form = angular.copy(personDataTemplate);
          $scope.data.config.formName.$setPristine();
          $scope.data.config.formName.$setUntouched();
          if (!isNotResident) {
            $scope.metaData.countries.forEach(function (item) {
              if (item.code == "AZ") {
                $scope.data.form.countryId = item.id;
                return;
              }
            });
          } else {
            $scope.data.form.idCard.series = null;
          }
        };

        $scope.citizenshipCountryChange = function (model) {
          if (!model) {
            $scope.data.form.countryId = null;
          } else {
            $scope.data.form.countryId = angular.fromJson(model).id;
          }
        };
        $scope.loadIamas = function (persons) {
          if (persons.dataBasePerson) {
            if (angular.isArray(persons.dataBasePerson.emails)) {
              $scope.data.form.emails = persons.dataBasePerson.emails;
            }
            if (angular.isArray(persons.dataBasePerson.phoneNumbers)) {
              $scope.data.form.phoneNumbers = persons.dataBasePerson.phoneNumbers;
            }
            $scope.data.form.actualAddress.value = persons.dataBasePerson.actualAddress ? persons.dataBasePerson.actualAddress.value : $scope.data.form.actualAddress.value;
          }
          $scope.data.form.iamasSuccess = true;
          $scope.buffers.addUser.compareWindow.close();
        };
        $scope.showSelectedPerson = function (win) {
          win.open();
          win.center();
        };
        $scope.findPersonIfNoDocument = function (idCard) {
          if ($scope.data.form.isNoMVDOcument) {

            if ($scope.data.form.lastPassport && ((idCard.series === $scope.data.form.lastPassport.series) && (idCard.number === $scope.data.form.lastPassport.number))) {
              return;
            }
            $scope.data.form.lastPassport = angular.copy(idCard);
            Loader.show(true);
            managementService.findPersonFromCSDR({
              number: idCard.number,
              series: idCard.series,
              processInstanceId: task.processInstanceId
            }).then(function (model) {
              Loader.show(false);
              if (model.isSuccess) {
                if (!model.data) return;
                model.data.isIamas = false;
                model.data.isNoMVDOcument = $scope.data.form.isNoMVDOcument;
                model.data.lastPassport = $scope.data.form.lastPassport;
                $scope.data.form = managementService.uPreparePerson(model.data, $scope.metaData);
              } else {
                SweetAlert.swal("", model.message, "error");
              }

            });
          }
        };
        $scope.findPersonByPin = function (pinCode, number, series) {

          if ($scope.data.manualInputEnabled) {
            if (!pinCode) return;
            if (pinCode === $scope.data.form.lastPin) {
              return;
            }
            Loader.show(true);
            managementService.findPersonFromCSDR({
              pinCode: pinCode,
              processInstanceId: task.processInstanceId
            })
              .then(function (model) {
                Loader.show(false);
                if (!model.isSuccess) {
                  SweetAlert.swal("", model.message, "error");
                } else {
                  if (!model.data) return;
                  model.data.isIamas = false;
                  model.data.isNoMVDOcument = $scope.data.form.isNoMVDOcument;
                  $scope.data.form = managementService.uPreparePerson(model.data, $scope.metaData);

                  $scope.data.form.lastPin = pinCode;
                }
              });
          }
        };
        $scope.findPerson = function () {
          var idCard = {
            number: $scope.data.form.idCard.number,
            pinCode: $scope.data.form.pinCode.value,
            series: $scope.data.form.idCard.series,
            isResident: $scope.data.isResident
          };
          $scope.data.config.formName.$submitted = true;
          if ($scope.data.config.formName.$invalid) {
            SweetAlert.swal("", "Forma doğrulama xətası", "error");
            return;
          }


          Loader.show(true);
          managementService.findPersonFromIamas({
            number: idCard.number,
            pinCode: idCard.pinCode,
            isDMX: idCard.isResident,
            processInstanceId: task.processInstanceId,
            activityInstanceId: task.processKey
          })
            .then(function (person) {
              Loader.show(false);
              if (!person.isSuccess) {
                SweetAlert.swal("", person.message, "error");
              } else {
                $scope.data.form.iamas = managementService.fixPersonData(person.data, $scope.metaData);
                $scope.buffers.addUser.compareWindow.open();
                $scope.buffers.addUser.compareWindow.center();
              }
            });
        };


        //Manage Person Phone Numbers
        $scope.addNewPhoneNumber = function (model) {
          managePersonService.addNewPhoneNumber(model);
        };
        $scope.removePhoneNumber = function (index, model) {
          managePersonService.removeFromArray(index, model);
        };

        //Manage Person Emails
        $scope.addNewEmail = function (model) {
          managePersonService.addNewEmail(model)
        };
        $scope.removeEmail = function (index, model) {
          managePersonService.removeFromArray(index, model);
        };

        // close window.
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

      }
    ]);
