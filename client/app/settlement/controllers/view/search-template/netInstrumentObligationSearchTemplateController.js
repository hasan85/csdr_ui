'use strict';

angular.module('cmisApp')
  .controller('NetInstrumentObligationSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope', "$filter",
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope, $filter) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

        return result;
      };

      var netObligationSelected = function (data) {
        var sResult = angular.copy($scope.netObligation.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.netObligation.selectedNetObligationIndex = i;
            break;
          }
        }
      };

      $scope.netObligationSelected = netObligationSelected;

      var _params = {
        searchUrl: "/api/settlement/getInstrumentObligations/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _netObligation = {
        formName: 'netObligationFindForm',
        form: {
          settlementTimeStart: helperFunctionsService.getStartOfDay(),
          settlementTimeFinish: helperFunctionsService.getEndOfDay()
        },
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedNetObligationIndex: false,
        netObligationSelected: netObligationSelected,
        netObligationDetails: false
      };

      $scope.netObligation = angular.merge($scope.searchConfig.netObligation, _netObligation);

      var findNetObligation = function (e) {

        //$scope.person.netObligationDetails = false;

        if ($scope.netObligation.searchResultGrid !== undefined) {
          $scope.netObligation.searchResultGrid.refresh();
        }

        $scope.netObligation.selectednetObligationIndex = false;

        if ($scope.netObligation.formName.$dirty) {
          $scope.netObligation.formName.$submitted = true;
        }

        if ($scope.netObligation.formName.$valid || !$scope.netObligation.formName.$dirty) {

          $scope.netObligation.searchResult.isEmpty = false;

          console.log($scope.netObligation.form);
          var formData = angular.copy($scope.netObligation.form);

          if (formData.idDocumentClass == "") {
            formData.idDocumentClass = null;
          }
          if ($scope.params.config.searchCriteria) {

            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }

          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          console.log('Request Data', requestData);
          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {

              var data = recordsService.normalizeRecordFields(data);
              if (data) {
                $scope.netObligation.searchResult.data = data;
              } else {
                $scope.netObligation.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };

      $scope.netObligation.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {
                settlementTime: {type: "date"},
                start: {type: "date"}
              }
            }
          },
          transport: {
            read: function (e) {
              findNetObligation(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "clearingMember.name",
            title: gettextCatalog.getString("Clearing Member"),
            width: '10rem'
          },
          {
            field: "start",
            title: gettextCatalog.getString("Start"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "settlementTime",
            title: gettextCatalog.getString("Settlement Time"),
            width: '10rem',
            format: "{0:dd-MMMM-yyyy HH:mm}"
          },
          {
            field: "cycleClass.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Cycle Class"),
            width: '10rem'
          },
          {
            field: "instrument.instrumentName",
            title: gettextCatalog.getString("Instrument"),
            width: '10rem'
          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("ISIN"),
            width: '10rem'
          },
          {
            field: "instrument.issuerName",
            title: gettextCatalog.getString("Issuer"),
            width: '10rem'
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: '10rem',
            template: function (e) {

              var quantity;

              if (e.quantity > 0) {
                quantity = -e.quantity;
              } else if (e.quantity < 0) {
                quantity = Math.abs(e.quantity);
              }

              return $filter("formatNumber")(quantity, false);

            }
          },
          {
            field: "status.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Status"),
            width: '10rem'
          },
        ]
      };

      $scope.netObligationSelected = netObligationSelected;

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.netObligation.form.settlementTimeStart = null;
        $scope.netObligation.form.settlementTimeFinish = null;
      };

      $scope.findNetObligation = function () {
        var formValidationResult = validateForm(angular.copy($scope.netObligation.form));
        if (formValidationResult.success) {
          if ($scope.netObligation.searchResultGrid) {
            $scope.netObligation.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }
      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.netObligation.searchResultGrid) {
          $scope.netObligation.searchResultGrid = widget;
        }


      });

    }]);
