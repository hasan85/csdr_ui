'use strict';

angular.module('cmisApp')
  .controller('NetObligationsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        netObligation: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedNetObligationIndex: false
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view:false,
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        }
      };
      $scope.$watch('searchConfig.netObligation.selectedNetObligationIndex', function (val) {

        if (val !== false) {
         // $scope.config.buttons.view.disabled = false;
        }
        else {
          $scope.searchConfig.netObligation.selectedPersonIndex = false;
         // $scope.config.buttons.view.disabled = true;
        }

      });

    }]);
