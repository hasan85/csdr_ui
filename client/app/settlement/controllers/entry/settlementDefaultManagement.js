'use strict';

angular.module('cmisApp')
  .controller('SettlementDefaultManagementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', '$rootScope',
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, dataSearchService, gettextCatalog, SweetAlert, $rootScope) {

      var prepareFormData = function (formData) {
        if (formData.defaultDatas) {
          for (var i = 0; i < formData.defaultDatas.length; i++) {
            var obj = formData.defaultDatas[i];
            if (formData.boundGridItems.hasOwnProperty(obj.ID)) {
              formData.defaultDatas[i] = angular.merge(obj, formData.boundGridItems[obj.ID]);

              if (formData.defaultDatas[i].regDate) {
                formData.defaultDatas[i].regDate = helperFunctionsService.generateDateTime(formData.defaultDatas[i].regDate);
              }
              if (formData.defaultDatas[i].settlementDate) {
                formData.defaultDatas[i].settlementDate = helperFunctionsService.generateDateTime(formData.defaultDatas[i].settlementDate);
              }
              if (formData.defaultDatas[i].valueDate) {
                formData.defaultDatas[i].valueDate = helperFunctionsService.generateDateTime(formData.defaultDatas[i].valueDate);
              }
              if (formData.defaultDatas[i].maturityDate) {
                formData.defaultDatas[i].maturityDate = helperFunctionsService.generateDateTime(formData.defaultDatas[i].maturityDate);
              }
            }
          }
        }
        return formData;
      };

      $scope.settlementDefaultManagementData = angular.merge({
        cancelAll: false,
        rolloverAll: false,
        boundGridItems: {}
      }, angular.fromJson(task.draft));

      if ($scope.settlementDefaultManagementData.defaultDatas) {
        for (var i = 0; i < $scope.settlementDefaultManagementData.defaultDatas.length; i++) {
          var elem = $scope.settlementDefaultManagementData.defaultDatas[i];
          $scope.settlementDefaultManagementData.boundGridItems[elem.ID] = {
            isCancelled: false,
            isRollovered: false
          }
        }
      }

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        // var taskPayload = task && task.draft ? angular.toJson(prepareFormData(angular.fromJson(task.draft))) : null;
        // var currentTask = angular.toJson(prepareFormData($scope.settlementDefaultManagementData));
        $scope.config.showTaskSavePrompt(false);

      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.settlementDefaultManagementData));
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "settlementDefaultManagementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              var formBuf = prepareFormData(angular.copy($scope.settlementDefaultManagementData));
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(formBuf);
              }
            }
          }
        }
      };

      $scope.mainGridOptions = {
        columns: [
          {
            field: 'isCancelled',
            width: "8rem",
            sortable: false,
            template: function (e) {
              return '<md-checkbox ng-change="changeCancelValue(' + e.ID + ')" class="early-settlement-checkbox" ' +
                'ng-model="settlementDefaultManagementData.boundGridItems[' + e.ID + '].isCancelled" ' +
                ' ng-disabled="settlementDefaultManagementData.rolloverAll || settlementDefaultManagementData.cancelAll" ' +
                ' aria-label="."></md-checkbox>';
            },
            headerTemplate: '<md-checkbox  class="early-settlement-checkbox settlement-table-header" ' +
            ' ng-model="settlementDefaultManagementData.cancelAll" ng-disabled="settlementDefaultManagementData.rolloverAll" ' +
            ' aria-label=".">' + gettextCatalog.getString("Cancel") + '</md-checkbox>',
          },
          {
            field: 'isRollovered',
            title: gettextCatalog.getString("Rollover"),
            width: "11rem",
            sortable: false,
            template: function (e) {
              return '<md-checkbox class="settlement-table-header" ng-change="changeRolloverValue(' + e.ID + ')" ' +
                ' ng-model="settlementDefaultManagementData.boundGridItems[' + e.ID + '].isRollovered"  ' +
                ' ng-disabled="settlementDefaultManagementData.rolloverAll || settlementDefaultManagementData.cancelAll" aria-label="."></md-checkbox>';
            },
            headerTemplate: '<md-checkbox class="settlement-table-header" ng-model="settlementDefaultManagementData.rolloverAll" ' +
            '  ng-disabled="settlementDefaultManagementData.cancelAll" aria-label=".">' + gettextCatalog.getString("Rollover") + '</md-checkbox>',
          },
          {
            field: "recordNumber",
            title: gettextCatalog.getString("Trade Number"),
            width: "10rem"
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Trade Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "settlementDate",
            title: gettextCatalog.getString("Settlement Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "recordStatus.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Trade Status"),
            width: "10rem"
          },
          {
            field: "buySideMember.name",
            title: gettextCatalog.getString("Buy Side Member"),
            width: "10rem"
          },
          {
            field: "buySideClient.name",
            title: gettextCatalog.getString("Buy Side Client"),
            width: "10rem"
          },
          {
            field: "sellSideMember.name",
            title: gettextCatalog.getString("Sell Side Member"),
            width: "10rem"
          },
          {
            field: "sellSideClient.name",
            title: gettextCatalog.getString("Sell Side Client"),
            width: "10rem"
          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("Instrument"),
            width: "10rem"
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "10rem"
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount"),
            width: "10rem"
          },
          {
            field: "price",
            title: gettextCatalog.getString("Price"),
            width: "10rem"
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency"),
            width: "10rem"
          },

          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "maturityDate",
            title: gettextCatalog.getString("Maturity Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: "10rem"
          }
        ],
        dataBound: function () {
          var rows = this.tbody.children();
          var dataItems = this.dataSource.view();
          for (var i = 0; i < dataItems.length; i++) {
            kendo.bind(rows[i], dataItems[i]);
          }
        },
        dataSource: {
          data: $scope.settlementDefaultManagementData.defaultDatas,
          schema: {
            model: {
              fields: {
                maturityDate: {type: "date"},
                valueDate: {type: "date"},
                settlementDate: {type: "date"},
                regDate: {type: "date"}
              }
            }
          }
        }
      };

      $scope.changeCancelValue = function (val) {
        if ($scope.settlementDefaultManagementData.boundGridItems[val].isCancelled) {
          $scope.settlementDefaultManagementData.boundGridItems[val].isRollovered = false;
        }
      };

      $scope.changeRolloverValue = function (val) {
        if ($scope.settlementDefaultManagementData.boundGridItems[val].isRollovered) {
          $scope.settlementDefaultManagementData.boundGridItems[val].isCancelled = false;
        }
      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.grid) {
          $scope.$watch('settlementDefaultManagementData.cancelAll', function (newVal) {
            for (var obj in $scope.settlementDefaultManagementData.boundGridItems) {
              if ($scope.settlementDefaultManagementData.boundGridItems.hasOwnProperty(obj)) {
                $scope.settlementDefaultManagementData.boundGridItems[obj].isCancelled = newVal ? true : false;
                $scope.settlementDefaultManagementData.boundGridItems[obj].isRollovered = false;
              }
            }
          });

          $scope.$watch('settlementDefaultManagementData.rolloverAll', function (newVal) {
            for (var obj in $scope.settlementDefaultManagementData.boundGridItems) {
              if ($scope.settlementDefaultManagementData.boundGridItems.hasOwnProperty(obj)) {
                $scope.settlementDefaultManagementData.boundGridItems[obj].isRollovered = newVal ? true : false;
                $scope.settlementDefaultManagementData.boundGridItems[obj].isCancelled = false;
              }
            }
          });
        }
      });

    }]);
