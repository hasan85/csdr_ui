'use strict';

angular.module('cmisApp')
  .controller('SettlementDefaultManagementApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'gettextCatalog', '$rootScope',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog, $rootScope) {
      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };

      $scope.settlementDefaultManagementData = angular.fromJson(task.draft);
      //if ($scope.settlementDefaultManagementData.defaultDatas) {
      //  for (var i = 0; i < $scope.settlementDefaultManagementData.defaultDatas.length; i++) {
      //    var elem = $scope.settlementDefaultManagementData.defaultDatas[i];
      //    $scope.settlementDefaultManagementData.boundGridItems[elem.ID] = {
      //      isCancelled: false,
      //      isRollovered: false
      //    }
      //  }
      //}
      $scope.mainGridOptions = {
        columns: [
          {
            field: 'isCancelled',
            width: "8rem",
            template: function (e) {
              return '<md-checkbox class="early-settlement-checkbox" ng-model="settlementDefaultManagementData.boundGridItems[' + e.ID + '].isCancelled"  ng-disabled="true" aria-label="."></md-checkbox>';
            },
            headerTemplate: '<md-checkbox class="early-settlement-checkbox settlement-table-header"  ng-model="settlementDefaultManagementData.cancelAll" ng-disabled="true" aria-label=".">' + gettextCatalog.getString("Cancel") + '</md-checkbox>',
          },
          {
            field: 'isRollovered',
            title: gettextCatalog.getString("Rollover"),
            width: "12rem",
            template: function (e) {
              return '<md-checkbox class="settlement-table-header" ng-model="settlementDefaultManagementData.boundGridItems[' + e.ID + '].isRollovered"  ng-disabled="true" aria-label="."></md-checkbox>';
            },
            headerTemplate: '<md-checkbox class="settlement-table-header" ng-model="settlementDefaultManagementData.rolloverAll"  ng-disabled="true" aria-label=".">' + gettextCatalog.getString("Rollover") + '</md-checkbox>',
          },
          {
            field: "recordNumber",
            title: gettextCatalog.getString("Trade Number"),
            width: "10rem"
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Trade Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "settlementDate",
            title: gettextCatalog.getString("Settlement Date"),
            type: "date",
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "buySideMember.name",
            title: gettextCatalog.getString("Buy Side Member"),
            width: "10rem"
          },
          {
            field: "buySideClient.name",
            title: gettextCatalog.getString("Buy Side Client"),
            width: "10rem"
          },
          {
            field: "sellSideMember.name",
            title: gettextCatalog.getString("Sell Side Member"),
            width: "10rem"
          },
          {
            field: "sellSideClient.name",
            title: gettextCatalog.getString("Sell Side Client"),
            width: "10rem"
          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("Instrument"),
            width: "10rem"
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "10rem"
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount"),
            width: "10rem"
          },
          {
            field: "price",
            title: gettextCatalog.getString("Price"),
            width: "10rem"
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency"),
            width: "10rem"
          },
          {
            field: "recordStatus.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Trade Status"),
            width: "10rem"
          },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "maturityDate",
            title: gettextCatalog.getString("Maturity Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: "10rem"
          }
        ],
        dataSource: {
          data: angular.fromJson(task.draft).defaultDatas,
          schema: {
            model: {
              fields: {
                maturityDate: {type: "date"},
                valueDate: {type: "date"},
                settlementDate: {type: "date"},
                regDate: {type: "date"}
              }
            }
          }
        }
      };
    }]);
