'use strict';

angular.module('cmisApp')
  .factory('pledgeService', ["$q", "Loader", "$http", 'helperFunctionsService', 'referenceDataService',
    function ($q, Loader, $http, helperFunctionsService, referenceDataService) {


      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};

        var getRegistrationAuthorityClasses = function (value) {
          metaData.registrationAuthorityClasses = value;
        };

        var getCurrencies = function (value) {
          metaData.currencies = value;
        };

        $q.all([
          referenceDataService.getRegistrationAuthorityClasses().then(getRegistrationAuthorityClasses),
          referenceDataService.getCurrencies().then(getCurrencies),

        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };
      var getPledgeRegistrations = function (pledgorId, pledgeeId) {

        return $http.get('/api/pledge/getPledgeRegistrations/?pledgorId='
          + pledgorId + "&pledgeeId=" + pledgeeId)
          .then(function (result) {
            var res = helperFunctionsService.convertObjectToArray(result.data);

            if (res && res.length > 0) {
              for (var i = 0; i < res.length; i++) {
                res[i]['subjects'] = helperFunctionsService.convertObjectToArray(res[i]['subjects']);
              }
            }
            return res;
          });
      };

      return {
        getPledgeRegistrations: getPledgeRegistrations,
        getMetaData: getMetaData
      };

    }]);
