'use strict';

angular.module('cmisApp')
  .factory('batchEntryOfAllotmentService', ["referenceDataService", "$q", "Loader", "$http", 'appConstants', 'helperFunctionsService', '$filter',
    function (referenceDataService, $q, Loader, $http, appConstants, helperFunctionsService, $filter) {

      //Return full tile data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};

        var getAccountStatusCheckingClasses = function (value) {
          metaData.accountStatusCheckingClasses = value;
        };
        var personClasses = function (value) {
          metaData.personClasses = value;
        };

        var registrationAuthorityClasses = function (value) {
          metaData.registrationAuthorityClasses = value;
        };
        var auctionClasses = function (value) {
          metaData.auctionClasses = value;
        };
        var getPhoneNumberTypes = function (value) {
          metaData.phoneNumberTypes = value;
        };

        var getCountries = function (value) {
          metaData.countries = value;
        };

        var getCurrencies = function (value) {
          metaData.currencies = value;
        };

        var businessClasses = function (value) {
          metaData.businessClasses = value;
        };
        var legalFormClasses = function (value) {
          metaData.legalFormClasses = value;
        };

        var getNaturalPersonIdDocumentTypes = function (value) {
          metaData.naturalPersonIdDocumentTypes = value;
        };
        var getSignerPositions = function (value) {
          metaData.signerPositions = value;
        };


        $q.all([
          referenceDataService.getPersonClasses().then(personClasses),
          referenceDataService.getPhoneNumberTypes().then(getPhoneNumberTypes),
          referenceDataService.getCountries().then(getCountries),
          referenceDataService.getCurrencies().then(getCurrencies),
          referenceDataService.getBusinessClasses().then(businessClasses),
          referenceDataService.getLegalFormClasses().then(legalFormClasses),
          referenceDataService.getNaturalPersonIdDocumentTypes().then(getNaturalPersonIdDocumentTypes),
          referenceDataService.getSignerPositions().then(getSignerPositions),
          referenceDataService.getRegistrationAuthorityClasses().then(registrationAuthorityClasses),
          referenceDataService.getAccountStatusCheckingClasses().then(getAccountStatusCheckingClasses),
          referenceDataService.getAuctionClasses().then(auctionClasses),
        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };


      return {
        getMetaData: getMetaData,

      };

    }]);
