'use strict';

angular.module('cmisApp')
  .factory('recordsService', ["$q", "Loader", "$http", 'helperFunctionsService', 'gettextCatalog', '$kWindow',
    function ($q, Loader, $http, helperFunctionsService, gettextCatalog, $kWindow) {
      var deferred = $q.defer();
      var findFreezing = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Freezing Record'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/freezing-find.html',
          controller: 'FreezingFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };
      var findWithdrawalSubscriptionMoney = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Withdrawal Subscription Money'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/corporate-actions/templates/withdrawal-subscription-money.html',
          controller: 'WithdrawalSubscriptionMoneyController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };
      var findMatchedTrade = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Matched Trade'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/matched-trade-find.html',
          controller: 'MatchedTradeFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };
      var findPledge = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Pledge Record'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/pledge-find.html',
          controller: 'PledgeFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };
      var findPledgedShares = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Pledge Shares'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/pledged-shares-find.html',
          controller: 'PledgedSharesFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };
      var selectFreezing = function (data) {
        // Select Bank Statement, resolve promise
        deferred.resolve(data);
        deferred = $q.defer();
      };
      var selectWithdrawalSubscriptionMoney = function (data) {
        deferred.resolve(data);
        deferred = $q.defer();
      };
      var selectMatchedTrade = function (data) {
        // Select Bank Statement, resolve promise
        deferred.resolve(data);
        deferred = $q.defer();
      };
      var selectPledge = function (data) {
        // Select Bank Statement, resolve promise
        deferred.resolve(data);
        deferred = $q.defer();
      };
      var selectPledgedShares = function (data) {
        // Select Bank Statement, resolve promise
        deferred.resolve(data);
        deferred = $q.defer();
      };
      var normalizeRecordFields = function (data) {

        if (data) {
          data.data = helperFunctionsService.convertObjectToArray(data.data);
          if (data.data) {
            for (var i = 0; i < data.data.length; i++) {
              data.data[i].recordSubjects = helperFunctionsService.convertObjectToArray(data.data[i].recordSubjects);
              data.data[i].subjects = helperFunctionsService.convertObjectToArray(data.data[i].subjects);

            }
          }
        }
        return data;
      };
      var getShareSelectionGridOptions = function () {
        return {
          columns: [
            {
              field: "instrument.instrumentName",
              title: gettextCatalog.getString('Instrument'),
              width: "10rem",
              editable: false
            },
            {
              field: "ID",
              title: gettextCatalog.getString("Share ID"),
              width: "6rem",
              editable: false
            },
            {
              field: "quantity",
              title: gettextCatalog.getString("Quantity"),
              width: "12rem",
              editable: false
            },
            {
              field: "selectedQuantity",
              title: gettextCatalog.getString("Selected Quantity"),
              width: "140px",
              template: function (e) {
                var t = "<input type='number' data-bind='value: selectedQuantity'   class='k-textbox inline-grid-qty-input'> ";
                return t;
              }
            },
            {
              field: "free",
              title: gettextCatalog.getString("Free"),
              width: "5rem",
              editable: false,
              template: function (e) {
                if (e.free == "true") {
                  return "<i class='glyphicon glyphicon-ok text-success'></i>";
                } else {
                  return "<i class='glyphicon glyphicon-remove text-danger'></i>"
                }
              }
            },
            {
              field: "frozen",
              title: gettextCatalog.getString("Frozen"),
              width: "5rem",
              editable: false,
              template: function (e) {
                if (e.frozen == "true") {
                  return "<i class='glyphicon glyphicon-ok text-success'></i>";
                } else {
                  return "<i class='glyphicon glyphicon-remove text-danger'></i>"
                }
              }
            },
            {
              field: "repo",
              title: gettextCatalog.getString("Repo"),
              width: "5rem",
              editable: false,
              template: function (e) {
                if (e.repo == "true") {
                  return "<i class='glyphicon glyphicon-ok text-success'></i>";
                } else {
                  return "<i class='glyphicon glyphicon-remove text-danger'></i>"
                }
              }
            },
            {
              title: gettextCatalog.getString("Pledge"),
              columns: [
                {
                  field: "pledged",
                  width: "5rem",
                  title: gettextCatalog.getString("Pledged"),
                  editable: false,
                  template: function (e) {
                    if (e.pledged == "true") {
                      return "<i class='glyphicon glyphicon-ok text-success'></i>";
                    } else {
                      return "<i class='glyphicon glyphicon-remove text-danger'></i>"
                    }
                  }
                },
                {
                  field: "operators",
                  title: gettextCatalog.getString("Pledgees"),
                  width: "6rem",
                  editable: false,
                  template: function (e) {
                    var t = "<ul class=\"list-no-style\">";
                    if (e.operators) {
                      //e.operators = helperFunctionsService.convertObjectToArray(e.operators);
                      for (var i = 0; i < e.operators.length; i++) {
                        t += "<li > " + e.operators[i] + "</li>";
                      }
                    }
                    t += "</ul>";
                    return t;
                  }
                }
              ]
            },
            {
              title: gettextCatalog.getString("Blocks"),
              columns: [
                {
                  field: "operationBlockQuantity",
                  width: "6rem",
                  editable: false,
                  title: gettextCatalog.getString("Operation")
                },
                {
                  field: "orderBlockQuantity",
                  width: "6rem",
                  editable: false,
                  title: gettextCatalog.getString("Order")
                },
                {
                  field: "tradeBlockQuantity",
                  width: "6rem",
                  editable: false,
                  title: gettextCatalog.getString("Trade")
                },
                {
                  field: "settlementBlockQuantity",
                  width: "6rem",
                  editable: false,
                  title: gettextCatalog.getString("Settlement")
                }
              ]
            }
          ], dataBound: function () {
            var rows = this.tbody.children();
            var dataItems = this.dataSource.view();
            for (var i = 0; i < dataItems.length; i++) {
              kendo.bind(rows[i], dataItems[i]);
            }
          }
        };

      };
      var getAuctionRecords = function (instrumentId) {
        return $http.post('/api/records/getAuctionRecords/' + instrumentId)
          .then(function (result) {
            if (result.data) {
              result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
            }
            return result.data;
          });
      };

      return {
        findFreezing: findFreezing,
        findWithdrawalSubscriptionMoney: findWithdrawalSubscriptionMoney,
        selectWithdrawalSubscriptionMoney: selectWithdrawalSubscriptionMoney,
        selectFreezing: selectFreezing,
        findPledge: findPledge,
        selectPledge: selectPledge,
        findPledgedShares: findPledgedShares,
        selectPledgedShares: selectPledgedShares,
        getShareSelectionGridOptions: getShareSelectionGridOptions,
        normalizeRecordFields: normalizeRecordFields,
        findMatchedTrade: findMatchedTrade,
        selectMatchedTrade: selectMatchedTrade,
        getAuctionRecords: getAuctionRecords
      };

    }]);
