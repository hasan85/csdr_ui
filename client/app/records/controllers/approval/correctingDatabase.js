'use strict';

angular.module('cmisApp').controller('CorrectingDatabaseApprovalController', ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {

    var titleTransferOperations = {
        grantOperation: "grantoperation_approve",
        titleTransfer: "titleTransfer_approve"
    };

    $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
    };

}]);
