'use strict';

angular.module('cmisApp')
  .controller('ShareTransferBetweenIntraAccountsApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task',
    function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {

      var titleTransferOperations = {
        grantOperation: "grantoperation_approve",
        titleTransfer: "titleTransfer_approve"
      };


      $scope.isGrantOperation = task.key == titleTransferOperations.grantOperation ? true : false;
      $scope.config = {
        screenId: id,
        taskId: taskId,
        task: task,
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState
      };

    }]);
