'use strict';

angular.module('cmisApp')
  .controller('TaxExchangeController', ['$scope', '$windowInstance', 'personFindService', 'id','data',
    function ($scope, $windowInstance, personFindService, id, data) {

      $scope.searchConfig = {
        accountPrescription: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/records/getTaxExchanges/',
          form: data,
          config: {
            searchOnInit: true,
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

      $scope.selectPerson = function () {
        personFindService.selectPerson($scope.searchConfig.accountPrescription.selectedPersonIndex );

        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };


    }]);
