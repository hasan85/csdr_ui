'use strict';

angular.module('cmisApp').controller('ViewPaymentsController', ['$scope', '$windowInstance', 'personFindService', 'id', "$q", "configParams", function ($scope, $windowInstance, personFindService, id, $q, configParams) {

  $scope.searchConfig = {
    OTCTrade: {
      searchResultGrid: undefined,
      searchResult: {
        data: {},
        isEmpty: false
      }
    },
    params: {
      searchUrl: '/api/records/getPayments/',
      config: {
        searchOnInit: true,
        searchCriteria: configParams.searchCriteria
      }
    }
  };

  // View configuration
  $scope.config = {
    screenId: id,
    window: $windowInstance,
    mainGrid: {}
  };

  $scope.selectData = function () {
    $windowInstance.close(selectedData);
  };

}]);
