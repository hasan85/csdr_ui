'use strict';

angular.module('cmisApp')
  .controller('ViewAzipsViewController',
    ['$scope', 'recordService', 'Loader', 'gettextCatalog', 'appConstants', '$rootScope', 'SweetAlert', 'helperFunctionsService', '$http',
      function ($scope, recordService, Loader, gettextCatalog, appConstants, $rootScope, SweetAlert, helperFunctionsService, $http) {


        $scope.services = [
          {name: 'resetBalances'},
          {name: 'sendUnknownPayments'},
          {name: 'sendPaymentWithWrongDestination'},
          {name: 'sendMemberTradingPayment'},
          {name: 'sendAdvancePayment'},
          {name: 'sendMemberTBDPayment'},
          {name: 'sendFOREXPayment'},
          {name: 'getMarketValuePriceList'}
        ]


        $scope.submit = function (service) {
          if (service === 'getMarketValuePriceList') {
            $http.post('/api/configurations/getMarketValuePriceList')
              .then(function (data) {
                console.log('getMarketValuePriceList: ' + data.data);
              });
          } else {
            $http.post('/api/records/azipsTestServices/', {data: {service: service.name}})
          }
        }


      }]
  );
