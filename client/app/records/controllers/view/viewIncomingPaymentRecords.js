'use strict';

angular.module('cmisApp')
  .controller('ViewIncomingPaymentRecordsController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      $scope.searchConfig = {
        accountPrescription: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/records/getIncomingPaymentRecords/',
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
