'use strict';

angular.module('cmisApp')
  .controller('FreezingRecordsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        freezing: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedFreezingIndex: false
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view:false,
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        },
      };
      $scope.$watch('searchConfig.freezing.selectedFreezingIndex', function (val) {

        if (val !== false) {
         // $scope.config.buttons.view.disabled = false;
        }
        else {
          $scope.searchConfig.freezing.selectedPersonIndex = false;
         // $scope.config.buttons.view.disabled = true;
        }

      });

    }]);
