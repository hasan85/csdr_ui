'use strict';

angular.module('cmisApp')
  .controller('PledgeRecordsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        pledge: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPledgeIndex: false
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view:false,
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        },
      };
      $scope.$watch('searchConfig.pledge.selectedPledgeIndex', function (val) {

        if (val !== false) {
         // $scope.config.buttons.view.disabled = false;
        }
        else {
          $scope.searchConfig.pledge.selectedPersonIndex = false;
         // $scope.config.buttons.view.disabled = true;
        }

      });

    }]);
