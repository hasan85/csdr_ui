'use strict';

angular.module('cmisApp')
  .controller('TitleTransferRecordsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        titleTransfer: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedTitleTransferIndex: false
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view:false,
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        },
      };
      $scope.$watch('searchConfig.titleTransfer.selectedTitleTransferIndex', function (val) {

        if (val !== false) {
         // $scope.config.buttons.view.disabled = false;
        }
        else {
          $scope.searchConfig.titleTransfer.selectedPersonIndex = false;
         // $scope.config.buttons.view.disabled = true;
        }

      });

    }]);
