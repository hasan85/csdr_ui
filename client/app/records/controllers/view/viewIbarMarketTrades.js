'use strict';

angular.module('cmisApp')
  .controller('IbarMarketTradesController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      $scope.searchConfig = {
        trade: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/records/getTrades/',
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
