'use strict';

angular.module('cmisApp')
  .controller('ViewAccountRecordsController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      $scope.searchConfig = {
        accountPrescription: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          form: {
            creationDateStart: kendo.toString(new Date(), "dd-M-yyyy"),
            creationDateStartObj: new Date()
          }
        },
        params: {
          searchUrl: '/api/records/getAccountRecords/',
          config: {
            searchOnInit: true
          },
          showSearchCriteriaBlock: true
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

      $scope.selectPerson = function () {
        personFindService.selectPerson($scope.searchConfig.accountPrescription.selectedPersonIndex );

        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };


    }]);
