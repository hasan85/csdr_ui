'use strict';

angular.module('cmisApp')
  .controller('GrantRecordsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        grant: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedGrantIndex: false
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view:false,
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        },
      };
      $scope.$watch('searchConfig.grant.selectedGrantIndex', function (val) {

        if (val !== false) {
         // $scope.config.buttons.view.disabled = false;
        }
        else {
          $scope.searchConfig.grant.selectedPersonIndex = false;
         // $scope.config.buttons.view.disabled = true;
        }

      });

    }]);
