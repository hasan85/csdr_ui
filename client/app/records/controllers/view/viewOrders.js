'use strict';

angular.module('cmisApp')
  .controller('OrdersController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      // Generic search configuration for trade
      $scope.searchConfig = {
        order: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
