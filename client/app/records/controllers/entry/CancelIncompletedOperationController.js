'use strict';

angular.module('cmisApp')
  .controller('CancelIncompletedOperationController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', '$kWindow', "personFindService", "moment",
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, dataSearchService, gettextCatalog, SweetAlert, $kWindow, personFindService, moment) {
        $scope.metaData = {
          currencies: []
        };


        var prepareFormData = function (viewModel) {
          viewModel.currency = angular.fromJson(viewModel.currency);
          viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);
          return viewModel;
        };

        $scope.data = angular.fromJson(task.draft);

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "enterBankStatementDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  $scope.config.completeTask(prepareFormData(angular.copy($scope.data)));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        var _taskSelected = function (data) {
          if (data) {
            $scope.data = {
              nameAz: data.nameAz,
              operationID: data.processInstanceId,
              shortDescription: data.shortDescription
            }
          }
        };
        var searchForOperation = function (response) {
          $scope.operationInfo = null;
          $scope.data = null;
          if(response.data.data){
            $scope.operationInfo = response.data.data;
            $scope.data = response.data.data;
          }else{
            if ($scope.operationInfo)
              $scope.operationInfo.creationDate = null;

            $scope.errorMessage =  response.message ? response.message : 'Əməliyyat tapılmadı.';
          }
        }


        $scope.searchOperation = function(){
          $scope.errorMessage = false;
          if (!$scope.data.operationID) {
            return;
          }
          operationService.getIncompletedOperationById($scope.data.operationID).then(function (response) {
            console.log('response', response);
            searchForOperation(response);

          });
        };
        $scope.searchIncompletedOperation = function () {
          Loader.show(true);
          personFindService.findCompletedOperation().then(function (data) {
            if (!data) {
              return;
            }
            operationService.getIncompletedOperationById(data.processInstanceId).then(function (response) {
              searchForOperation(response);
            });
            Loader.show(false);
          });
        };



// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
