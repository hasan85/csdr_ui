'use strict';

angular.module('cmisApp')
  .controller('NotaryTradeController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
      'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',
      function ($scope, $windowInstance, id, appConstants, operationService,
                personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog) {

        var initializeFormData = function (draft) {

          if (!draft) {
            draft = {}
          }
          if (draft.isConfirmed === null || draft.isConfirmed === undefined) {
            draft.isConfirmed = false;
          }


          return draft;
        };

        $scope.metaData = {};

        var prepareFormData = function (formData) {
          formData.errResults = formData.errResults.filter(function (item) {
            return item.code === $scope.reason_code;
          });
          return formData;
        };

        $scope.otcTrade = initializeFormData(angular.fromJson(task.draft));
        $scope.taskData = $scope.otcTrade;

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "otcTradeOperationForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  var buf = prepareFormData(angular.copy($scope.otcTrade));
                  $scope.config.completeTask(buf);

                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };
        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {

          $scope.config.saveTask($scope.otcTrade);
        });


        $scope.selectChange = function () {
          console.log("scope.isConfirmed", $scope.isConfirmed);
          console.log("scope.reason", $scope.reason);
        }

      }]);
