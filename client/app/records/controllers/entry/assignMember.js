'use strict';

angular.module('cmisApp')
  .controller('AssignMemberController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService) {


        $scope.data = angular.fromJson(task.draft);

        $scope.data.canDetectMember = $scope.data.canDetectMember || false;
        $scope.data.payerIsIssuer = null;


        $scope.serviceClasses = [
          {code: "PAYMENT_SERVICE_DIS", name: "Kupon/Dividend"},
          {code: "PAYMENT_SERVICE_SRV", name: "Xidmət haqqı"}
        ]

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "enterBankStatementDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.config.form.name.$valid) {

                var data = angular.copy($scope.data)
                // if (data.vmCouponPayment.length) {
                //
                //   data.vmCouponPayment.forEach(function (row) {
                //     row.recordDate = helperFunctionsService.generateDateTime(row.recordDate);
                //     row.valueDate = helperFunctionsService.generateDateTime(row.valueDate);
                //
                //     delete row.recordDateObj
                //     delete row.valueDateObj
                //   })
                // }
                if(data.beneficiarAccount){
                  if(data.beneficiarAccount.creationDate) delete data.beneficiarAccount.creationDate
                  if(data.beneficiarAccount.identificatorFinishDate) delete data.beneficiarAccount.identificatorFinishDate
                }

                console.log(data)

                $scope.config.completeTask(data);
                // } else {
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };


        $scope.$watch('data.payerType', function(){
            setTimeout(function () {
              $('.window-main-content').scrollTop(2123423)
            },200)
        })


        $scope.findMember = function () {
          personFindService.findClearingMember().then(function (data) {
            $scope.data.payeeClient = data
            console.log(data)
          })
        };



        $scope.findIssuer = function () {
          personFindService.findIssuer().then(function (data) {
            $scope.data.payeeClient = data
            console.log(data)
          })
        };

        $scope.findTaxExchanges = function () {
          personFindService.findTaxExchanges({amount: $scope.data.paidAmount}).then(function (data) {
            $scope.data.items = angular.isArray(data.exchangeItems) ? data.exchangeItems: [data.exchangeItems]
          })
        };


        var emptyCouponPaymentRow = {
          amount: 0,
          recordDate: '',
          valueDate: ''
        }


// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
