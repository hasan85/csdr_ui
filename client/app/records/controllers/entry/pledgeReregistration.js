'use strict';

angular.module('cmisApp')
  .controller('PledgeReRegistrationController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService',
    'operationState', 'task', 'referenceDataService', "Loader", 'dataValidationService', 'SweetAlert', 'gettextCatalog', 'recordsService', 'pledgeService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService,
              operationState, task, referenceDataService, Loader, dataValidationService, SweetAlert, gettextCatalog, recordsService, pledgeService) {

      var instrumentFormPartialTemplate = {
        instrument: {
          id: null,
          ISIN: null,
          issuerName: null,
          instrumentName: null
        },
        allQuantity: true,
        shares: [],
        balance: [],
        validation: {
          errors: {
            pledgable: false,
            amountSplit: false
          }
        }
      };
      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.maturityDateObj) {
          formData.maturityDate = helperFunctionsService.generateDateTime(formData.maturityDateObj);
          delete formData.maturityDateObj;
        }

        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }
        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        if (formData.currency) {
          formData.currency = angular.fromJson(formData.currency);
        }
        if (formData.pledgor && formData.pledgor.identificatorFinishDate) {
          formData.pledgor.identificatorFinishDate = helperFunctionsService.generateDateTime(formData.pledgor.identificatorFinishDate);
        }

        if (formData.pledgee && formData.pledgee.identificatorFinishDate) {
          formData.pledgee.identificatorFinishDate = helperFunctionsService.generateDateTime(formData.pledgee.identificatorFinishDate);
        }
        if (formData.referredPledgeRecord && formData.referredPledgeRecord.recordDate) {
          formData.referredPledgeRecord.recordDate = helperFunctionsService.generateDateTime(formData.referredPledgeRecord.recordDate);
        }

        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.pledgor === null || draft.pledgor === undefined) {
          draft.pledgor = {};
        }
        if (draft.pledgee === null || draft.pledgee === undefined) {
          draft.pledgee = {};
        }
        if (draft.referredPledgeRecord === null || draft.referredPledgeRecord === undefined) {
          draft.referredPledgeRecord = {};
        }
        if (draft.subjects === null || draft.subjects === undefined) {
          draft.subjects = [angular.copy(instrumentFormPartialTemplate)];
        }
        return draft;
      };
      $scope.pledge = initializeFormData(angular.fromJson(task.draft));

      //$scope.pledge = {
      //  pledgor: {},
      //  pledgee: {},
      //  subjects: [
      //    angular.copy(instrumentFormPartialTemplate)
      //  ],
      //  referredPledgeRecord: {}
      //};
      $scope.metaData = {};
      Loader.show(true);
      pledgeService.getMetaData().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data.registrationAuthorityClasses,
          currencies: data.currencies
        };
        referenceDataService.normalizeReturnedRecordTask($scope.pledge, data);
        Loader.show(false);
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "pledgeOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              var formBuf = prepareFormData(angular.copy($scope.pledge));
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(formBuf);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        balanceGrid: {},
        selectedSubjectIndexForBalanceSelection: -1
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.pledge);
      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
          //$scope.pledge = angular.fromJson(task.draft);
        }
      }

      $scope.findOrder = function () {
        recordsService.findPledge({
          statusCode: "STATUS_PROCESSED"
        }).then(function (order) {

          $scope.resetData();
          $scope.pledge.referredPledgeRecord = {
            id: order.id,
            recordDate: order.regDate,
            recordNumber: order.recordNumber,
            pledgeClass: order.pledgeClass
          };
          $scope.pledge.pledgor = order.pledgor;
          $scope.pledge.pledgee = order.pledgee;

          $scope.pledge.subjects = order.recordSubjects;
          if ($scope.pledge.subjects) {
            for (var i = 0; i < $scope.pledge.subjects.length; i++) {
              var buf = angular.copy(instrumentFormPartialTemplate);
              buf.instrument = $scope.pledge.subjects[i].instrument;
              $scope.pledge.subjects[i] = angular.merge($scope.pledge.subjects[i], angular.copy(buf));
            }
          }
        });
      };

      $scope.resetData = function () {
        $scope.pledge.pledgor = {};
        $scope.pledge.referredPledgeRecord = {};
        $scope.pledge.pledgee = {};
        $scope.pledge.subjects = [
          angular.copy(instrumentFormPartialTemplate)
        ];
      };

      // Search pledgor
      $scope.findPledgor = function () {

        personFindService.findShareholder().then(function (data) {
          $scope.pledge.pledgor.name = data.name;
          $scope.pledge.pledgor.accountId = data.accountId;
          $scope.pledge.pledgor.id = data.id;
          $scope.pledge.pledgor.accountNumber = data.accountNumber;

        });
      };

      $scope.findPledgee = function () {

        personFindService.findPledgee().then(function (data) {
          $scope.pledge.pledgee.name = data.name;
          $scope.pledge.pledgee.accountId = data.accountId;
          $scope.pledge.pledgee.id = data.id;
          $scope.pledge.pledgee.accountNumber = data.accountNumber;
        });
      };

      // Subscribe for grid double click event
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.balanceGrid) {
          $scope.config.balanceGrid = widget;
        }
        if (widget === $scope.config.accountStatementWindow) {
          $scope.config.accountStatementWindow = widget;
        }
      });
    }]);
