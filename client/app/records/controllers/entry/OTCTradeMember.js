'use strict';

angular.module('cmisApp').controller('OTCTradeMemberController', ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',   'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',   function ($scope, $windowInstance, id, appConstants, operationService, personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog) {

    var initializeFormData = function (draft) {

        if (!draft) {
            draft = {}
        }
        if (draft.isSellAction === null || draft.isSellAction === undefined) {
            draft.isSellAction = false;
        }
        if (draft.thisClient === null || draft.thisClient === undefined) {
            draft.thisClient = {};
        }
        if (draft.counterPartyClient === null || draft.counterPartyClient === undefined) {
            draft.counterPartyClient = {
                accountNumber: null
            };
        }
        if (draft.buyer === null || draft.buyer === undefined) {
            draft.buyer = {};
        }
        if (draft.counterPartyBroker === null || draft.counterPartyBroker === undefined) {
            draft.counterPartyBroker = {};
        }
        if (draft.subject === null || draft.subject === undefined) {
            draft.subject = {
                instrument: {}
            };
        }

        return draft;
    };

    $scope.metaData = {};

    var prepareFormData = function (formData) {

        if (formData.contractDateObj) {
            formData.contractDate = helperFunctionsService.generateDateTime(formData.contractDateObj);
            delete formData.contractDateObj;
        }
        if (formData.venue) {
            formData.venue = angular.fromJson(formData.venue);
        }
        return formData;
    };

    $scope.COMMISSION_PAYMENT_METHODS = [
        {code: "COMMISSION_PAID_BY_SELLER", nameAz: "Satıcı"},
        {code: "COMMISSION_PAID_BY_BUYER", nameAz: "Alıcı"},
        {code: "COMMISSION_PAID_BOTH_SIDES", nameAz: "Hər iki tərəf"}
    ];

    $scope.otcTrade = initializeFormData(angular.fromJson(task.draft));

    referenceDataService.normalizeReturnedRecordTask($scope.otcTrade, null);

    //Initialize scope variables [[
    $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
            name: "otcTradeOperationForm",
            data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
            complete: {
                click: function () {

                    $scope.config.form.name.$submitted = true;
                    if ($scope.config.form.name.$valid) {
                        var buf = prepareFormData(angular.copy($scope.otcTrade));
                        $scope.config.completeTask(buf);

                    } else {
                        SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                    }
                }
            }
        }
    };

    // Check if form is dirty
    $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
    });

    // Save task as draft
    $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.otcTrade);
    });

    // Search buyer broker
    $scope.findCounterPartyBroker = function () {
        console.log("isOperatorTradingMember", $scope.otcTrade.isOperatorTradingMember);
        if($scope.otcTrade.isOperatorTradingMember) {
            personFindService.searchTradingMembersAndSO().then(function (data) {
                $scope.otcTrade.counterPartyBroker.name = data.name;
                $scope.otcTrade.counterPartyBroker.accountId = data.accountId;
                $scope.otcTrade.counterPartyBroker.id = data.id;
                $scope.otcTrade.counterPartyBroker.accountNumber = data.accountNumber;
            });
        } else {
            personFindService.findTradingMember().then(function (data) {
                $scope.otcTrade.counterPartyBroker.name = data.name;
                $scope.otcTrade.counterPartyBroker.accountId = data.accountId;
                $scope.otcTrade.counterPartyBroker.id = data.id;
                $scope.otcTrade.counterPartyBroker.accountNumber = data.accountNumber;
            });
        }
    };

    $scope.otcTrade.isIssuer = false;
    // Search client
    $scope.findClient = function () {
        Loader.show(true);
        console.log("isOperatorTradingMember", $scope.otcTrade.isOperatorTradingMember);
        // if($scope.otcTrade.isOperatorTradingMember) {
        //     personFindService.findAllClientAccounts().then(function (data) {
        //         $scope.otcTrade.thisClient.name = data.name;
        //         $scope.otcTrade.thisClient.accountId = data.accountId;
        //         $scope.otcTrade.thisClient.id = data.id;
        //         $scope.otcTrade.thisClient.accountNumber = data.accountNumber;
        //         $scope.otcTrade.thisClient.depoAccountId = data.depoAccountId;
        //     });
        // } else {
        //     personFindService.findShareholder().then(function (data) {
        //         $scope.otcTrade.thisClient.name = data.name;
        //         $scope.otcTrade.thisClient.accountId = data.accountId;
        //         $scope.otcTrade.thisClient.id = data.id;
        //         $scope.otcTrade.thisClient.accountNumber = data.accountNumber;
        //         $scope.otcTrade.thisClient.depoAccountId = data.depoAccountId;
        //         $scope.otcTrade.thisClient.accountClassCode = data.accountType;
        //     });
        // }
            if(!$scope.otcTrade.isOperatorTradingMember && !$scope.otcTrade.isIssuer) {
              personFindService.findShareholder().then(function (data) {
                        $scope.otcTrade.thisClient.name = data.name;
                        $scope.otcTrade.thisClient.accountId = data.accountId;
                        $scope.otcTrade.thisClient.id = data.id;
                        $scope.otcTrade.thisClient.accountNumber = data.accountNumber;
                        $scope.otcTrade.thisClient.depoAccountId = data.depoAccountId;
                        $scope.otcTrade.thisClient.accountClassCode = data.accountType;
                    });
            } else if(!$scope.otcTrade.isOperatorTradingMember && $scope.otcTrade.isIssuer) {
              personFindService.findIssuer().then(function (data) {
                        $scope.otcTrade.thisClient.name = data.name;
                        $scope.otcTrade.thisClient.accountId = data.accountId;
                        $scope.otcTrade.thisClient.id = data.id;
                        $scope.otcTrade.thisClient.accountNumber = data.accountNumber;
                        $scope.otcTrade.thisClient.depoAccountId = data.depoAccountId;
                        $scope.otcTrade.thisClient.accountClassCode = data.accountType;

                        $scope.issuer = $scope.otcTrade.thisClient.id;
              });
            } else {
              personFindService.findAllClientAccounts().then(function (data) {
                        $scope.otcTrade.thisClient.name = data.name;
                        $scope.otcTrade.thisClient.accountId = data.accountId;
                        $scope.otcTrade.thisClient.id = data.id;
                        $scope.otcTrade.thisClient.accountNumber = data.accountNumber;
                        $scope.otcTrade.thisClient.depoAccountId = data.depoAccountId;
                     });
            }
    };

    // Search instrument
    $scope.findInstrument = function () {
      if($scope.issuer) {
        instrumentFindService.findInstrument({
          params: {
            issuerId:  $scope.issuer
          }
        }).then(function (data) {
          $scope.otcTrade.subject.instrument.ISIN = data.isin;
          $scope.otcTrade.subject.instrument.id = data.id;
          $scope.otcTrade.subject.instrument.issuerName = data.issuerName;
          $scope.otcTrade.subject.instrument.instrumentName = data.instrumentName;
        });
      } else {
        instrumentFindService.findInstrument().then(function (data) {
          $scope.otcTrade.subject.instrument.ISIN = data.isin;
          $scope.otcTrade.subject.instrument.id = data.id;
          $scope.otcTrade.subject.instrument.issuerName = data.issuerName;
          $scope.otcTrade.subject.instrument.instrumentName = data.instrumentName;
        });
      }

    };

}]);
