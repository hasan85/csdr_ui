'use strict';

angular.module('cmisApp')
  .controller('TransferToIssuerAccountController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'FileUploader',
    'SweetAlert', 'gettextCatalog', 'Loader', '$http', 'batchEntryOfAllotmentService', '$filter', 'managePersonService', 'referenceDataService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, FileUploader,
              SweetAlert, gettextCatalog, Loader, $http, batchEntryOfAllotmentService, $filter, managePersonService, referenceDataService) {

      var initializeFormData = function (draft) {
        if (draft.items === null) {
          draft.items = [];
        }
        if (draft.instrument === null) {
          draft.instrument = {};
        }
        return draft;
      };

      $scope.allotmentData = initializeFormData(angular.fromJson(task.draft));

      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }
        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }
        if (formData.valueDate) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }
        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }

        return formData;
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "transferToIssuerAccountOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.form.data = prepareFormData(angular.copy($scope.allotmentData));
                $scope.config.completeTask($scope.config.form.data);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }, labels: {
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.allotmentData);
      });


       $scope.total_quantity = 0;
       $scope.$watch("allotmentData.items", function(newValue) {
           if(newValue) {
               $scope.total_quantity = 0;
               newValue.map(function(item) {
                   $scope.total_quantity += item.quantity;
               });
           }
       }, true);

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument().then(function (data) {
          $scope.allotmentData.instrument.ISIN = data.isin;
          $scope.allotmentData.instrument.id = data.id;
          $scope.allotmentData.instrument.issuerName = data.issuerName;
          $scope.allotmentData.instrument.instrumentName = data.instrumentName;
          $scope.allotmentData.instrument.CFI = data.CFI;
        });

      };

      $scope.findShareholder = function () {

        personFindService.findShareholderAccountsWithOperatorName().then(function (data) {

          $scope.allotmentData.items.push({
            accountID: data.accountId,
            personData: {
              name: {
                nameAz: data.name
              }
            },
            quantity: null
          });

        });

      };

      $scope.removeRepresentative = function (index, model) {
        model.splice(index, 1);
      };

    }]);
