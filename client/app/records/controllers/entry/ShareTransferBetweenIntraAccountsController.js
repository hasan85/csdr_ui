'use strict';

angular.module('cmisApp')
  .controller('ShareTransferBetweenIntraAccountsController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog) {


      $scope.metaData = {};
      Loader.show(true);
      Loader.show(false);


      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }

        return formData;
      };


      var subjectModel = {
        instrument: null,
        quantity: null
      };
      var taskDataModel = {
        owner: null,
        receiver: null,
        subjects: [
          angular.copy(subjectModel)
        ]
      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = taskDataModel
        } else {

          if(!draft.subjects || !angular.isArray(draft.subjects) || (angular.isArray(draft.subjects) && draft.subjects.length == 0)) {
            draft.subjects = [
              angular.copy(subjectModel)
            ];
          }

        }
        return draft;
      };

      $scope.taskData = initializeFormData(angular.fromJson(task.draft));


      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "taskForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.taskData)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.taskData);
      });

      // Search owner
      $scope.findOwner = function () {
        Loader.show(true);
        personFindService.findShareholderAccountsWithOperatorName().then(function (data) {
          console.log("owner", data);
          $scope.taskData.owner = data;
        });

      };

      // Search receiver
      $scope.findReceiver = function () {
        console.log("owner id", $scope.taskData.owner.id);
        console.log($scope.taskData.owner)
        personFindService.getAnotherShareholderAccount({
          searchCriteria: {
            personId: $scope.taskData.owner.id,
            accountId: $scope.taskData.owner.accountId
          },
          searchOnInit: true,
          filterDataByAccountId: $scope.taskData.owner.accountId
        }).then(function (data) {
          console.log("receiver", data);
          $scope.taskData.receiver = data;
        });
      };

      // Search instrument
      $scope.findInstrument = function (index) {
        instrumentFindService.findInstrument().then(function (data) {
          console.log("instrument", data);
          $scope.taskData.subjects[index].instrument = data;
        });
      };

      // Add new instrument
      $scope.addNewInstrument = function (model) {
        model.push(angular.copy(subjectModel));
      };

      // Remove instrument
      $scope.removeInstrument = function (index, model) {
        model.splice(index, 1);
      };

    }]);
