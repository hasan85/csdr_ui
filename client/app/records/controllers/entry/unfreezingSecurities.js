'use strict';

angular.module('cmisApp')
  .controller('UnfreezingSecuritiesController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'recordsService',
    'dataValidationService', 'Loader', 'referenceDataService', 'gettextCatalog', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task,
              recordsService, dataValidationService, Loader, referenceDataService, gettextCatalog, SweetAlert) {

      $scope.metaData = {
        shareClassCodes: {
          freezingShareholderAccount: 'FREEZING_SHAREHOLDER_ACCOUNT_RECORD',
          freezingShareholderSecurities: 'FREEZING_SHAREHOLDER_SECURITIES_RECORD',
          freezingIssuerAccount: 'FREEZING_ISSUER_ACCOUNT_RECORD',
          freezingIssuerSecurities: 'FREEZING_ISSUER_SECURITIES_RECORD',
          freezingShareRecord: 'FREEZING_SHARE_RECORD'
        },
        shareClassCases: {
          issuer: 'issuer',
          shareholder: 'shareholder',
          share: 'share'
        },
        currentShareClassCase: ''
      };
      Loader.show(true);
      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }
        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        if (formData.holder && formData.holder.identificatorFinishDate) {
          formData.holder.identificatorFinishDate = helperFunctionsService.generateDateTime(formData.holder.identificatorFinishDate);
        }
        if (formData.unfreezeOrder) {
          formData.unfreezingOrder.recordDate = helperFunctionsService.generateDateTime(formData.unfreezingOrder.recordDate);

          var selectedSubjects = [];

          if (formData.subjects) {
            for (var i = 0; i < formData.subjects.length; i++) {
              if (formData.subjects[i].selected) {
                selectedSubjects.push(formData.subjects[i]);
              }
            }
            formData.subjects = selectedSubjects;
          }
        }
        else {
          if (formData.unfreezeAll) {
            formData.subjects = null;
          }
        }
        return formData;
      };
      var validateForm = function (formData) {
        var isValid = true;
        if (formData.subjects && formData.subjects.length > 0) {
          var subjects = formData.subjects;
          for (var i = 0; i < subjects.length; i++) {
            var subject = subjects[i];
          }
        }
        return isValid;
      };
      var instrumentFormPartialTemplate = {
        instrument: {
          id: null,

          ISIN: null,
          issuerName: null,
          instrumentName: null
        },
        allQuantity: true,
        shares: [],
        balance: [],
        validation: {
          errors: {
            freezable: false,
            amountSplit: false
          }
        }
      };
      $scope.instrumentSearchParams = {
        config: {
          issuer: {
            isVisible: false
          }
        },
        params: {
          issuerId: null
        },
        searchUrl: 'findInstrument',
        searchOnInit: true

      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.isHolderIssuer === null || draft.isHolderIssuer === undefined) {
          draft.isHolderIssuer = true;
        }
        if (draft.unfreezeAll === null || draft.unfreezeAll === undefined) {
          draft.unfreezeAll = true;
        }
        if (draft.unfreezeOrder === null || draft.unfreezeOrder === undefined) {
          draft.unfreezeOrder = true;
        }
        if (draft.selected === null || draft.selected === undefined) {
          draft.selected = true;
        }
        if (draft.holder === null || draft.holder === undefined) {
          draft.holder = {};
        }

        if (draft.subjects === null || draft.subjects === undefined) {
          draft.subjects = [angular.copy(instrumentFormPartialTemplate)];
        }

        return draft;
      };
      $scope.freezing = initializeFormData(angular.fromJson(task.draft));
      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData.registrationAuthorityClasses = data;
        referenceDataService.normalizeReturnedRecordTask($scope.freezing, {registrationAuthorityClasses: data});
        Loader.show(false);
      });
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "freezingOperationForm",
          data: {},
          customValidationErrors: {
            orderNotSelected: false,
            orderSecurityNotSelected: false
          }
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;

              if ($scope.config.form.name.$valid && validateForm($scope.freezing)) {
                var formBuf = prepareFormData(angular.copy($scope.freezing));
                $scope.config.completeTask(formBuf);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        balanceGrid: {},
        selectedSubjectIndexForBalanceSelection: -1
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.freezing);
      });

      $scope.resetData = function () {

        $scope.freezing.holder = {};
        $scope.freezing.subjects = [
          angular.copy(instrumentFormPartialTemplate)
        ];
        $scope.freezing.unfreezingOrder = {};
      };

      $scope.findHolder = function () {

        if ($scope.freezing.isHolderIssuer) {
          personFindService.findIssuer().then(function (data) {

            $scope.freezing.holder.name = data.name;
            $scope.freezing.holder.accountId = data.accountId;
            $scope.freezing.holder.id = data.id;
            $scope.freezing.holder.accountNumber = data.accountNumber;

            $scope.instrumentSearchParams.params.issuerId = data.id;

            $scope.freezing.subjects = [
              angular.copy(instrumentFormPartialTemplate)
            ];
            $scope.freezing.unfreezingOrder = {};
          });
        } else {
          // findShareholder, searchShareholders
          personFindService.searchShareholders({
            criteriaForm: {
              personType: {
                isVisible: true,
                default: appConstants.personClasses.juridicalPerson
              }
            },
            searchUrl: '/api/clearing/searchClientShareholder'
          }).then(function (data) {
            console.log(data);
            $scope.freezing.holder.name = data.name;
            $scope.freezing.holder.accountId = data.accountId;
            $scope.freezing.holder.id = data.id;
            $scope.freezing.holder.accountNumber = data.accountNumber;


            $scope.freezing.subjects = [
              angular.copy(instrumentFormPartialTemplate)
            ];
            $scope.freezing.unfreezingOrder = {};

          });
        }
      };


      // Search instrument
      $scope.findInstrument = function (index) {
        if ($scope.freezing.isHolderIssuer) {
          instrumentFindService.findInstrument(angular.copy($scope.instrumentSearchParams)).then(function (data) {

            $scope.freezing.subjects[index] = angular.copy(instrumentFormPartialTemplate);
            $scope.freezing.subjects[index].instrument.ISIN = data.isin;
            $scope.freezing.subjects[index].instrument.id = data.id;
            $scope.freezing.subjects[index].instrument.issuerName = data.issuerName;
            $scope.freezing.subjects[index].instrument.instrumentName = data.instrumentName;
          });
        } else {
          instrumentFindService.findInstrument().then(function (data) {
            $scope.freezing.subjects[index] = angular.copy(instrumentFormPartialTemplate);
            $scope.freezing.subjects[index].instrument.ISIN = data.isin;
            $scope.freezing.subjects[index].instrument.id = data.id;
            $scope.freezing.subjects[index].instrument.issuerName = data.issuerName;
            $scope.freezing.subjects[index].instrument.instrumentName = data.instrumentName;
          });
        }
      };

      // Add new instrument
      $scope.addNewInstrument = function (model) {
        model.push(angular.copy(instrumentFormPartialTemplate));
      };

      // Remove instrument
      $scope.removeInstrument = function (index, model) {
        model.splice(index, 1);
      };

      $scope.config.balanceGrid.options = recordsService.getShareSelectionGridOptions();

      $scope.showBalanceData = function (window, data, subject, index) {
        if (subject.shares) {
          subject.shares.length = 0;
        }
        if(!subject.validation){
          subject.validation = {
            errors: {
              freezable: false,
              amountSplit: false
            }
          }
        }
        subject.validation.errors.freezable = true;
        $scope.config.selectedSubjectIndexForBalanceSelection = index;
        $scope.config.balanceGrid.options.dataSource = {
          data: data ? data : [],
          total: data ? data.length : 0,
          pageSize: 20,
          schema: {
            schema: {
              model: {
                fields: {
                  selectedQuantity: {
                    type: "number"
                  }
                }
              }
            }
          }
        };
        window.title(gettextCatalog.getString('Shares') + " - " + subject.instrument.instrumentName ? subject.instrument.instrumentName : '');
        window.open();
        window.center();
      };

      $scope.checkBalance = function (subject, window, index, isShareCase) {

        if (!subject.allQuantity || (isShareCase && subject.selected)) {

          if (subject.shares) {
            subject.shares.length = 0;
          }
          Loader.show(true);
          var args = {
            accountId: $scope.freezing.holder.accountId,
            quantity: null,
            instrumentId: subject.instrument.id,
            type: 'CATEGORY_UNFREEZABLE_SHARES'
          };
          dataValidationService.checkBalance(args).then(function (data) {
            $scope.config.selectedSubjectIndexForBalanceSelection = -1;
            subject.balance = [];

            if (data) {

              if (data['success'] == 'true') {
                if (data['data'] && data['data'].length > 0) {
                  subject.balance = data['data'];
                  $scope.showBalanceData(window, subject.balance, subject, index);
                } else {
                  subject.validation.errors.freezable = false;
                }

              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

            } else {
              SweetAlert.swal('', gettextCatalog.getString('Internal Server Error'), 'error');
              subject.validation.errors.freezable = false;
            }

            Loader.show(false);
          });
        }

      };

      $scope.selectShares = function () {

        var gridData = $scope.config.balanceGrid.dataSource.data();

        if (gridData) {
          for (var i = 0; i < gridData.length; i++) {

            var qty = parseInt(gridData[i].selectedQuantity);
            if (qty > 0) {
              $scope.freezing.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].shares.push({
                ID: gridData[i].ID,
                quantity: qty
              });
              $scope.freezing.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].balance[i].selectedQuantity = qty;
            }
          }
        }
        $scope.freezing.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].validation.errors.freezable = false;

        console.log($scope.freezing);
        $scope.config.accountStatementWindow.close();
      };

      // Subscribe for grid double click event
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.balanceGrid) {
          $scope.config.balanceGrid = widget;
        }
        if (widget === $scope.config.accountStatementWindow) {
          $scope.config.accountStatementWindo = widget;
        }
      });

      $scope.findOrder = function () {
        recordsService.findFreezing({
          statusCode: "STATUS_PROCESSED"
        }).then(function (order) {

          $scope.resetData();
          $scope.freezing.unfreezingOrder = {
            id: order.ID,
            recordDate: order.regDate,
            orderNumber: order.orderNumber,
            freezingClass: order.freezingClass.code
          };

          if (order.freezingClass.code == $scope.metaData.shareClassCodes.freezingIssuerAccount
            || order.freezingClass.code == $scope.metaData.shareClassCodes.freezingIssuerSecurities) {

            $scope.metaData.currentShareClassCase = $scope.metaData.shareClassCases.issuer;

          }
          else if (order.freezingClass.code == $scope.metaData.shareClassCodes.freezingShareholderAccount
            || order.freezingClass.code == $scope.metaData.shareClassCodes.freezingShareholderSecurities) {
            $scope.metaData.currentShareClassCase = $scope.metaData.shareClassCases.shareholder;
          }
          else {
            $scope.metaData.currentShareClassCase = $scope.metaData.shareClassCases.share;
          }

          $scope.freezing.holder = order.holder;
          $scope.freezing.subjects = order.subjects;
          if ($scope.freezing.subjects) {
            for (var i = 0; i < $scope.freezing.subjects.length; i++) {
              var buf = angular.copy(instrumentFormPartialTemplate);
              buf.instrument = $scope.freezing.subjects[i].instrument;
              $scope.freezing.subjects[i] = angular.merge($scope.freezing.subjects[i], angular.copy(buf));
            }
          }

        });
      };

      $scope.instrumentQtyChange = function (subject) {

        Loader.show(true);

        var params = {
          accountId: $scope.freezing.holder.accountId,
          instrumentId: subject.instrument.id,
          quantity: subject.quantity
        };

        dataValidationService.validateUnfreezableShares(params).then(function (data) {

          subject.chains = null;
          subject.validation.errors.freezable = false;
          subject.validation.errors.amountSplit = false;

          if (data) {

            if (data['success'] == 'true') {

              if (data['data'] && data['data'].length > 0) {
                subject.chains = data['data'];
              }
              subject.validation.errors.freezable = false;

            } else {

              subject.validation.errors.freezable = true;
            }

          } else {
            subject.validation.errors.freezable = false;
          }

          Loader.show(false);
        });
      }
    }]);
