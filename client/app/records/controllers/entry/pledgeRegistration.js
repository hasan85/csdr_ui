'use strict';

angular.module('cmisApp')
  .controller('PledgeRegistrationController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService',
    'operationState', 'task', 'referenceDataService', "Loader", 'dataValidationService', 'SweetAlert', 'gettextCatalog', 'recordsService', 'pledgeService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService,
              operationState, task, referenceDataService, Loader, dataValidationService, SweetAlert, gettextCatalog, recordsService, pledgeService) {

      var instrumentFormPartialTemplate = {
        instrument: {
          id: null,
          ISIN: null,
          issuerName: null,
          instrumentName: null
        },
        allQuantity: false,
        shares: [],
        balance: [],
        validation: {
          errors: {
            pledgable: false,
            amountSplit: false
          }
        }
      };
      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.maturityDateObj) {
          formData.maturityDate = helperFunctionsService.generateDateTime(formData.maturityDateObj);
          delete formData.maturityDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }
        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        if (formData.currency) {
          formData.currency = angular.fromJson(formData.currency);
        }
        if (formData.pledgeAll) {
          formData.subjects = null;
        }
        return formData;
      };
      var validateForm = function (formData) {

        if (formData.subjects && formData.subjects.length > 0) {

          var subjects = formData.subjects;

          for (var i = 0; i < subjects.length; i++) {

            var subject = subjects[i];

            if (subject.validation.errors.pledgable) {
              return false;
            }
          }
        }
        return true;
      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }

        if (draft.pledgeAll === null || draft.pledgeAll === undefined) {
          draft.pledgeAll = true;
        }
        if (draft.pledgor === null || draft.pledgor === undefined) {
          draft.pledgor = {};
        }
        if (draft.pledgee === null || draft.pledgee === undefined) {
          draft.pledgee = {};
        }
        if (draft.subjects === null || draft.subjects === undefined) {
          draft.subjects = [angular.copy(instrumentFormPartialTemplate)];
        }

        return draft;
      };
      $scope.pledge = initializeFormData(angular.fromJson(task.draft));

      $scope.metaData = {};
      Loader.show(true);
      pledgeService.getMetaData().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data.registrationAuthorityClasses,
          currencies: data.currencies
        };
        referenceDataService.normalizeReturnedRecordTask($scope.pledge, data);
        Loader.show(false);
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "pledgeOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              var formBuf = prepareFormData(angular.copy($scope.pledge));
              if ($scope.config.form.name.$valid && validateForm(formBuf)) {

                $scope.config.completeTask(formBuf);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        balanceGrid: {},
        selectedSubjectIndexForBalanceSelection: -1
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.pledge);
      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
         // $scope.pledge = angular.fromJson(task.draft);
        }
      }

      $scope.pledgorTypeChange = function () {
        $scope.pledge.pledgor = {};
      };

      // Search pledgor
      $scope.findPledgor = function () {

        personFindService.findShareholder().then(function (data) {
          $scope.pledge.pledgor.name = data.name;
          $scope.pledge.pledgor.accountId = data.accountId;
          $scope.pledge.pledgor.id = data.id;
          $scope.pledge.pledgor.accountNumber = data.accountNumber;

        });
      };
      $scope.findPledgee = function () {

        personFindService.findPledgee().then(function (data) {
          $scope.pledge.pledgee.name = data.name;
          $scope.pledge.pledgee.accountId = data.accountId;
          $scope.pledge.pledgee.id = data.id;
          $scope.pledge.pledgee.accountNumber = data.accountNumber;
        });
      };

      // Search instrument
      $scope.findInstrument = function (index) {
        instrumentFindService.findInstrument().then(function (data) {
          $scope.pledge.subjects[index] = angular.copy(instrumentFormPartialTemplate);
          $scope.pledge.subjects[index].instrument.ISIN = data.isin;
          $scope.pledge.subjects[index].instrument.id = data.id;
          $scope.pledge.subjects[index].instrument.issuerName = data.issuerName;
          $scope.pledge.subjects[index].instrument.instrumentName = data.instrumentName;
        });

      };
      // Add new instrument
      $scope.addNewInstrument = function (model) {
        model.push(angular.copy(instrumentFormPartialTemplate));
      };
      // Remove instrument
      $scope.removeInstrument = function (index, model) {
        model.splice(index, 1);
      };


      $scope.config.balanceGrid.options = recordsService.getShareSelectionGridOptions();
      $scope.showBalanceData = function (window, data, subject, index) {

        subject.shares.length = 0;
        if (!subject.validation) {
          subject.validation = {
            errors: {
              pledgable: false,
              amountSplit: false
            }
          }
        }
        subject.validation.errors.pledgable = true;
        $scope.config.selectedSubjectIndexForBalanceSelection = index;
        $scope.config.balanceGrid.options.dataSource = {
          data: data ? data : [],
          total: data ? data.length : 0,
          pageSize: 20,
          schema: {
            schema: {
              model: {
                fields: {
                  selectedQuantity: {
                    type: "number"
                  }
                }
              }
            }
          }
        };
        window.title(gettextCatalog.getString('Shares') + " - " + subject.instrument.instrumentName ? subject.instrument.instrumentName : '');
        window.open();
        window.center();
      };
      $scope.checkBalance = function (subject, window, index) {


        subject.shares.length = 0;
        Loader.show(true);
        var args = {
          accountId: $scope.pledge.pledgor.accountId,
          quantity: null,
          instrumentId: subject.instrument.id,
          type: 'CATEGORY_PLEDGABLE_SHARES'
        };
        dataValidationService.checkBalance(args).then(function (data) {
          $scope.config.selectedSubjectIndexForBalanceSelection = -1;
          subject.balance = [];

          if (data) {

            if (data['success'] == 'true') {
              if (data['data'] && data['data'].length > 0) {
                subject.balance = data['data'];
                $scope.showBalanceData(window, subject.balance, subject, index);
              } else {
                subject.validation.errors.pledgable = false;
              }

            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }

          } else {
            SweetAlert.swal('', gettextCatalog.getString('Internal Server Error'), 'error');
            subject.validation.errors.pledgable = false;
          }

          Loader.show(false);
        });


      };
      $scope.selectShares = function () {

        var gridData = $scope.config.balanceGrid.dataSource.data();

        if (gridData) {
          for (var i = 0; i < gridData.length; i++) {

            var qty = parseInt(gridData[i].selectedQuantity);
            if (qty > 0) {
              $scope.pledge.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].shares.push({
                ID: gridData[i].ID,
                quantity: qty
              });
              $scope.pledge.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].balance[i].selectedQuantity = qty;
            }
          }
        }
        $scope.pledge.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].validation.errors.pledgable = false;

        $scope.config.accountStatementWindow.close();

      };

      // Subscribe for grid double click event
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.balanceGrid) {
          $scope.config.balanceGrid = widget;
        }
        if (widget === $scope.config.accountStatementWindow) {
          $scope.config.accountStatementWindo = widget;
        }
      });
    }]);
