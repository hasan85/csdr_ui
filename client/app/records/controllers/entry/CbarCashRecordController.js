'use strict';

angular.module('cmisApp')
  .controller('CbarCashRecordController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.metaData = {};
        referenceDataService.getCurrencies().then(function (data) {

          $scope.metaData.currencies = data;
          $scope.data.currencyCode = 'AZN'
          referenceDataService.addEmptyOption([
            $scope.metaData.currencies
          ]);

          Loader.show(false);
        });

        $scope.data = {};

        var date = new Date();
        var startDate = new Date(date.getFullYear(), date.getMonth(), 1);
        var finishDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        var lastDateWithSlashes = (finishDate.getDate()) + '-' + (finishDate.getMonth() + 1) + '-' + finishDate.getFullYear();
        var firstDateWithSlashes = (startDate.getDate()) + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();

        $scope.data.startDateObj = startDate;
        $scope.data.startDate = firstDateWithSlashes;

        $scope.data.endDateObj = finishDate;
        $scope.data.endDate = lastDateWithSlashes;


        var findData = function (e) {

          var formData = angular.copy($scope.data);

          formData.startDate = $scope.data.startDateObj;
          formData.endDate = $scope.data.endDateObj;

          delete formData.startDateObj;
          delete formData.endDateObj;



          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          requestData.currencyCode = requestData.currencyCode || 'AZN'

          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/records/getCBARCashRecord/',
            data: {data: requestData}
          }).then(function (response) {

            var records = response.data;

            $scope.endBalance = records.data.endBalance;
            $scope.startBalance = records.data.startBalance;

            var recordData = records.data.accountRecordDatas.data ? records.data.accountRecordDatas.data : []

            recordData = angular.isArray(recordData) ? recordData : [recordData]

            console.log(recordData);

            e.success({
              Data: recordData , Total: records.data.accountRecordDatas.total
            });

            Loader.show(false);
          });

        };

        $scope.findData = function(){
          $scope.searchResultGrid.dataSource.page(1);
        }


        $scope.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  date: {type: "date"},
                  incomingAmount: {type: "number"},
                  outgoingAmount: {type: "number"},
                  remainingAmount: {type: "number"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },
            sort: {field: "date", dir: "desc"},
            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {
            input: false,
            numeric: true
          },
          // pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [

            {
              field: "ID",
              title: "ID",
              width: '10rem'
            },
            {
              field: "name",
              title: "Təşkilat",
              width: '10rem',
            },
            {
              field: "incomingAmount",
              title: "Mədaxil",
              width: '10rem',
              template: "<div style='text-align: right'>#= kendo.toString(incomingAmount, 'n2') # </div>"
            },
            {
              field: "outgoingAmount",
              title: "Məxaric",
              width: '10rem',
              template: "<div style='text-align: right'>#= kendo.toString(outgoingAmount, 'n2') # </div>"
            },
            {
              field: "destination",
              title: "Təyinat",
              width: '10rem'
            },
            {
              field: "date",
              title: gettextCatalog.getString("Date"),
              width: '10rem',
              format: "{0:dd-MMMM-yyyy HH:mm}"
            },
            {
              field: "remainingAmount",
              title: "Qalıq",
              width: '10rem',
              template: "<div style='text-align: right'>#= kendo.toString(remainingAmount, 'n2') # </div>"
            }
          ]
        };

        $scope.exportToExcel = function(){
          $scope.searchResultGrid.saveAsExcel();
        }




      }]);
