'use strict';

angular.module('cmisApp')
  .controller('AuctionFinalizationController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', 'recordsService', '$filter',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task,
              referenceDataService, Loader, SweetAlert, gettextCatalog, recordsService, $filter) {

      $scope.metaData = {};

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.instrument === null || draft.instrument === undefined) {
          draft.instrument = {};
        }
        return draft;
      };

      $scope.auctionFinalizationData = initializeFormData(angular.fromJson(task.draft));

      Loader.show(true);
      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data
        };

        // In case of task started over
        referenceDataService.normalizeReturnedRecordTask($scope.auctionFinalizationData, {registrationAuthorityClasses: data});
        Loader.show(false);
      });

      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }

        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        if (formData.auctions) {
          for (var i = 0; i < formData.auctions.length; i++) {
            var obj = formData.auctions[i];
            obj.registrationDate = helperFunctionsService.generateDateTime(obj.registrationDate);
            obj.valueDate = helperFunctionsService.generateDateTime(obj.valueDate);
          }
        }
        return formData;
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "auctionFinalizationDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var buf = prepareFormData(angular.copy($scope.auctionFinalizationData));
                $scope.config.completeTask(buf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.auctionFinalizationData);
      });


      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument().then(function (data) {
          $scope.auctionFinalizationData.instrument = {};
          $scope.auctionFinalizationData.instrument.ISIN = data.isin;
          $scope.auctionFinalizationData.instrument.id = data.id;
          $scope.auctionFinalizationData.instrument.issuerName = data.issuerName;
          $scope.auctionFinalizationData.instrument.instrumentName = data.instrumentName;

          recordsService.getAuctionRecords(data.id).then(function (res) {
            if (res.success == "true") {
              $scope.auctionFinalizationData.auctions = res.data;
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
            }
            Loader.show(false);

          });

        });
      };


    }]);
