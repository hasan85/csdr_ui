'use strict';

angular.module('cmisApp')
  .controller('TransferPledgedTitleController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService',
    'operationState', 'task', 'referenceDataService', "Loader", 'dataValidationService', 'SweetAlert', 'gettextCatalog', 'recordsService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService,
              operationState, task, referenceDataService, Loader, dataValidationService, SweetAlert, gettextCatalog, recordsService) {

      var instrumentFormPartialTemplate = {
        instrument: {
          id: null,
          ISIN: null,
          issuerName: null,
          instrumentName: null
        },
        shares: [],
        balance: []
      };
      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }
        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }

        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }

        return formData;

      };
      var validateForm = function (formData) {
        if (formData.subject && formData.subject.shares && formData.subject.shares.length) {
          return true;
        } else {
          return false;
        }
      };
      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }

        if (draft.seller === null || draft.seller === undefined) {
          draft.seller = true;
        }
        if (draft.buyer === null || draft.buyer === undefined) {
          draft.buyer = {};
        }
        if (draft.pledgee === null || draft.pledgee === undefined) {
          draft.pledgee = {};
        }

        if (draft.subject === null || draft.subject === undefined) {
          draft.subject = angular.copy(instrumentFormPartialTemplate)
        }

        return draft;
      };
      $scope.transferPledgedTitleData = initializeFormData(angular.fromJson(task.draft));

      $scope.metaData = {};
      Loader.show(true);
      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data
        };
        referenceDataService.normalizeReturnedRecordTask($scope.transferPledgedTitleData, {registrationAuthorityClasses: data});
        Loader.show(false);
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "transferPledgedTitleDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              var formBuf = prepareFormData(angular.copy($scope.transferPledgedTitleData));
              if ($scope.config.form.name.$valid) {

                if (validateForm(formBuf)) {
                  $scope.config.completeTask(formBuf);
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('You have to select at least one share'), 'error');
                }

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        balanceGrid: {}
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.transferPledgedTitleData);
      });

      // Search seller
      $scope.findSeller = function () {
        Loader.show(true);
        personFindService.findShareholder({
          searchCriteria: {
            isSuspended: false
          }
        }).then(function (data) {
          $scope.transferPledgedTitleData.seller = {};
          $scope.transferPledgedTitleData.seller.name = data.name;
          $scope.transferPledgedTitleData.seller.accountId = data.accountId;
          $scope.transferPledgedTitleData.seller.id = data.id;
          $scope.transferPledgedTitleData.seller.accountNumber = data.accountNumber;
        });

      };
      // Search Buyer
      $scope.findBuyer = function () {

        personFindService.findShareholder({
          searchCriteria: {
            isSuspended: false,
          }
        }).then(function (data) {

          $scope.transferPledgedTitleData.buyer = {};
          $scope.transferPledgedTitleData.buyer.name = data.name;
          $scope.transferPledgedTitleData.buyer.accountId = data.accountId;
          $scope.transferPledgedTitleData.buyer.id = data.id;
          $scope.transferPledgedTitleData.buyer.accountNumber = data.accountNumber;
        });
      };
      // Search Pledgee
      $scope.findPledgee = function () {

        personFindService.findPledgee({
          searchCriteria: {
            isSuspended: false,
          }
        }).then(function (data) {
          $scope.transferPledgedTitleData.pledgee = {};
          $scope.transferPledgedTitleData.pledgee.name = data.name;
          $scope.transferPledgedTitleData.pledgee.accountId = data.accountId;
          $scope.transferPledgedTitleData.pledgee.id = data.id;
          $scope.transferPledgedTitleData.pledgee.accountNumber = data.accountNumber;
        });
      };

      // Search Instruments
      $scope.findInstrument = function () {
        instrumentFindService.findInstrument().then(function (data) {
          $scope.transferPledgedTitleData.subject.instrument = {};
          $scope.transferPledgedTitleData.subject.instrument.ISIN = data.isin;
          $scope.transferPledgedTitleData.subject.instrument.id = data.id;
          $scope.transferPledgedTitleData.subject.instrument.issuerName = data.issuerName;
          $scope.transferPledgedTitleData.subject.instrument.instrumentName = data.instrumentName;
        });
      };
      $scope.config.balanceGrid.options = recordsService.getShareSelectionGridOptions();
      $scope.showBalanceData = function (window, data, subject) {
        subject.shares.length = 0;
        $scope.config.balanceGrid.options.dataSource = {
          data: data ? data : [],
          total: data ? data.length : 0,
          pageSize: 20,
          schema: {
            schema: {
              model: {
                fields: {
                  selectedQuantity: {
                    type: "number"
                  }
                }
              }
            }
          }
        };
        window.title(gettextCatalog.getString('Shares') + " - " + subject.instrument.instrumentName ? subject.instrument.instrumentName : '');
        window.open();
        window.center();
      };
      $scope.checkBalance = function (subject, window, index) {
        subject.shares.length = 0;
        Loader.show(true);
        var args = {
          accountId: $scope.transferPledgedTitleData.seller.accountId,
          operatorId: $scope.transferPledgedTitleData.pledgee.accountId,
          quantity: null,
          instrumentId: subject.instrument.id,
          type: 'CATEGORY_TRADABLE_PLEDGE_SHARES'
        };
        dataValidationService.checkBalance(args).then(function (data) {
          subject.balance = [];

          if (data) {

            if (data['success'] == 'true') {
              if (data['data'] && data['data'].length > 0) {
                subject.balance = data['data'];
                $scope.showBalanceData(window, subject.balance, subject, index);
              }
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }

          } else {
            SweetAlert.swal('', gettextCatalog.getString('Internal Server Error'), 'error');
          }

          Loader.show(false);
        });

      };
      $scope.selectShares = function () {
        var gridData = $scope.config.balanceGrid.dataSource.data();

        if (gridData) {
          for (var i = 0; i < gridData.length; i++) {

            var qty = parseInt(gridData[i].selectedQuantity);
            if (qty > 0) {
              $scope.transferPledgedTitleData.subject.shares.push({
                ID: gridData[i].ID,
                quantity: qty
              });
              $scope.transferPledgedTitleData.subject.balance[i].selectedQuantity = qty;
            }
          }
        }
        $scope.config.accountStatementWindow.close();
      };

      // Subscribe for grid double click event
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.balanceGrid) {
          $scope.config.balanceGrid = widget;
        }
        if (widget === $scope.config.accountStatementWindow) {
          $scope.config.accountStatementWindo = widget;
        }
      });

    }]);
