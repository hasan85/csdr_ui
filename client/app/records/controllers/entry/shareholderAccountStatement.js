'use strict';

angular.module('cmisApp')
  .controller('shareholderAccountStatementController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'recordService','$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, recordService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {
        $scope.metaData = {}
        Loader.show(true);
        recordService.getMyShareholderAccounts().then(function (data) {
          $scope.metaData.shareholderAccounts = data.data ? data.data : [];
          $scope.data.accountNumber = recordService.dontKnowHowToPassDataBetweenController ? recordService.dontKnowHowToPassDataBetweenController : $scope.metaData.shareholderAccounts[0];
          $scope.findData();
          Loader.show(false);
        });




        $scope.data = {};

        var findData = function (e) {

          var formData = angular.copy($scope.data);

          if(!formData.accountNumber) {
            e.success({
                        Data: [], Total: 0
                      });
          } else {
            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });

            Loader.show(true);
            $http({
              method: 'POST',
              url: '/api/records/getShareholderAccountStatement/',
              data: {data: requestData}
            }).then(function (response) {
              var balances = response.data;

              e.success({
                Data: balances.data ? balances.data : [], Total: balances.total
              });

              Loader.show(false);
            });
          }

      };

        $scope.findData = function () {
          $scope.searchResultGrid.dataSource.read();
        };

        $scope.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  parValue: {type: "number"},
                  instrumentQuantity: {type: "number"},
                  balanceQuantity: {type: "number"},
                  encumbranceQuantity: {type: "number"},
                  cfiName: {type: "string"},
                  serviceStatus: {type: "string"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {
            input: false,
            numeric: true
          },
          // pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              title:"Emitent",
              columns: [
                {
                  field: "issuer",
                  title: "Adı",
                  width: '10rem'
                },
                {
                  field: "issuerAddress",
                  title: "Hüquqi ünvanı",
                  width: '10rem'
                }
              ]
            },
            {
              title: "Qiymətli Kağız",
              columns: [
                {
                  title:"ISIN Nömrəsi",
                  field:"instrumentCode",
                  width:"7rem"
                },
                {
                  title: "Buraxılış forması və növü",
                  field: "cfiName",
                  sortable: false,
                  width: '10rem'
                },
                {
                  field: "parValue",
                  title: "Nominal Dəyəri",
                  width: '8rem',
                  template: "<div style='text-align: right'>#= kendo.toString(parValue, 'n2') # </div>"
                },
                {
                  field: "instrumentQuantity",
                  title: "Ümumi Məbləği",
                  width: '8rem',
                  template: "<div style='text-align: right'>#= kendo.toString(instrumentQuantity, 'n0') # </div>"
                },
                {
                  title: "Xidmət statusu <i class='glyphicon glyphicon-info-sign'><md-tooltip>(borclu deyilsə xidmət olunur, borcludursa xidmət olunmur)</md-tooltip></i>",
                  field: "serviceStatus",
                  width: '8rem',
                  sortable:false
                }
              ]
            },
            {
              field: "balanceQuantity",
              title: "Mülkiyyətində olan Kağızların sayı",
              width: '7rem',
              template: "<div style='text-align: right'>#= kendo.toString(balanceQuantity, 'n0') # </div>"
            },
            {
              field: "encumbranceQuantity",
              title: "Öhdəliklərlə yüklənmiş",
              width: '6rem',
              template: "<div style='text-align: right'>#= kendo.toString(encumbranceQuantity, 'n0') # </div>"
            },

          ]
        };

        $scope.exportToExcel = function () {
          $scope.searchResultGrid.saveAsExcel();
        }

      }])
;
