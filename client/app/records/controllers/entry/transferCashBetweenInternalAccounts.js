'use strict';

angular.module('cmisApp')
  .controller('TransferCashBetweenInternalAccountsController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {

        $scope.metaData = {};
        $scope.data = angular.fromJson(task.draft);


        $scope.fromAccountChooses = [
          {value: 'self', label: 'Öz hesabı'},
          {value: 'client', label: 'Müştəri hesabı'}
        ]

        $scope.toAccountChoose = {};

        var serviceClasses = [
          {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'},
          {code: 'BALANCE_SERVICE_SUB', label: 'Abunə yazılışı bölməsi'},
          {code: 'BALANCE_SERVICE_COL', label: 'Pul zəmanəti bölməsi'},
          {code: 'BALANCE_SERVICE_SRV', label: 'Xidmət bölməsi'},
          {code: 'BALANCE_SERVICE_DIS', label: 'Ödəniş bölməsi'}
        ]


        $scope.fromAccountChooseChanged = function (newVal) {
          if (newVal) {
            $scope.fromAccountChoose = newVal

            $scope.data.fromServiceClasses = [];
            $scope.data.toServiceClasses = [];
            $scope.toAccountChooses = [];
            $scope.toAccountChoose = false;
            $scope.data.fromServiceClass = false;
            $scope.data.toServiceClass = false;
            $scope.data.fromAccount = false;
            $scope.data.toAccount = false;


            if (newVal == 'self') {
              $scope.data.fromAccount = $scope.data.currentMemberAccount;
              $scope.fromServiceClasses = [
                // {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'},
                {code: 'BALANCE_SERVICE_DIS', label: 'Ödəniş bölməsi'},
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'}
              ]
            } else {
              $scope.fromServiceClasses = [
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'}
              ]
              $scope.data.fromServiceClass = $scope.fromServiceClasses[0];
              $scope.fromServiceClassChanged($scope.data.fromServiceClass)

            }
          }
        }


        $scope.fromServiceClassChanged = function (newVal) {
          if (newVal) {

            $scope.data.toServiceClasses = [];
            $scope.data.toServiceClass = false;
            $scope.toAccountChooses = [];
            $scope.toAccountChoose = false;
            $scope.data.toAccount = false;


            if ($scope.fromAccountChoose == 'self') {

              if (newVal.code == 'BALANCE_SERVICE_TRD') {
                $scope.toAccountChooses = [
                  {value: 'self', label: 'Öz hesabı'},
                  {value: 'client', label: 'Müştəri hesabı'},
                  {value: 'other', label: 'Digər üzv'}
                ]

              } else if (newVal.code == 'BALANCE_SERVICE_DIS') {

                $scope.toAccountChooses = [
                  {value: 'self', label: 'Öz hesabı'}
                ]
                $scope.data.toAccount = $scope.data.currentMemberAccount;
                $scope.toAccountChoose = 'self';
                $scope.toServiceClasses = [
                  {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'}
                ];
                $scope.data.toServiceClass = $scope.toServiceClasses[0];
              }
              else if (newVal.code == 'BALANCE_SERVICE_SRV') {

                $scope.toAccountChooses = [
                  {value: 'self', label: 'Öz hesabı'},
                  {value: 'client', label: 'Müştəri hesabı'}
                ]
              }
            } else {
              $scope.toAccountChooses = [];
              $scope.toServiceClasses = [
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'}
              ];

              $scope.toAccountChooses = [
                {value: 'n', label: 'Müştərin digər üzvdəki hesabı'}
              ]
              $scope.toAccountChoose = 'n'
              $scope.data.toServiceClass = $scope.toServiceClasses[0];
            }

          }

        }

        $scope.$watch('data.fromServiceClass', function (newVal) {
          $scope.fromServiceClassChanged(newVal)
          getAmount()
        });

        $scope.$watch('data.fromAccount', function (newVal) {
          getAmount()
        });


        $scope.toAccountChooseChanged = function (newVal) {
          $scope.data.toAccount = false;
          if (newVal) {
            $scope.data.toServiceClasses = [];
            $scope.data.toServiceClass = false;
            $scope.toAccountChoose = newVal;


            if ($scope.fromAccountChoose == 'self' && $scope.data.fromServiceClass.code == 'BALANCE_SERVICE_TRD' && newVal == 'self') {
              $scope.toServiceClasses = [
                {code: 'BALANCE_SERVICE_SUB', label: 'Abunə yazılışı bölməsi'},
                {code: 'BALANCE_SERVICE_COL', label: 'Pul zəmanəti bölməsi'},
                // {code: 'BALANCE_SERVICE_SRV', label: 'Xidmət bölməsi'}
              ];
              $scope.data.toAccount = $scope.data.currentMemberAccount
            }


            if ($scope.fromAccountChoose == 'self' && $scope.data.fromServiceClass.code == 'BALANCE_SERVICE_TRD' && newVal == 'client') {
              $scope.toServiceClasses = [
                {code: 'BALANCE_SERVICE_SUB', label: 'Abunə yazılışı bölməsi'},
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'}
              ];
            }


            if ($scope.fromAccountChoose == 'self' && $scope.data.fromServiceClass.code == 'BALANCE_SERVICE_TRD' && newVal == 'other') {
              $scope.toServiceClasses = [
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'}
              ];
            }

            if ($scope.fromAccountChoose == 'self' && $scope.data.fromServiceClass.code == 'BALANCE_SERVICE_SRV' && newVal == 'self') {
              $scope.toServiceClasses = [
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'},
                {code: 'BALANCE_SERVICE_SUB', label: 'Abunə yazılışı bölməsi'},
                {code: 'BALANCE_SERVICE_COL', label: 'Pul zəmanəti bölməsi'}
              ];
              $scope.data.toServiceClass = $scope.toServiceClasses[0];
              $scope.data.toAccount = $scope.data.currentMemberAccount

            }


            if ($scope.fromAccountChoose == 'self' && $scope.data.fromServiceClass.code == 'BALANCE_SERVICE_SRV' && newVal == 'client') {
              $scope.toServiceClasses = [
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'},
                {code: 'BALANCE_SERVICE_SUB', label: 'Abunə yazılışı bölməsi'}
              ];
            }

            if ($scope.fromAccountChoose == 'client' && $scope.data.fromServiceClass.code == 'BALANCE_SERVICE_TRD') {
              $scope.toServiceClasses = [
                {code: 'BALANCE_SERVICE_TRD', label: 'Ticarət bölməsi'}
              ];
              $scope.data.toServiceClass = $scope.toServiceClasses[0];
            }
          }
        };


        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });


        $scope.prepareData = function (data) {
          return data
        }

        setTimeout(function () {
          $windowInstance.widget.setOptions({width: '80%'})
        }, 200)


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.formValid()) {
                //
                var data = $scope.prepareData(angular.copy($scope.data));
                $scope.config.completeTask(data);
                //
                // } else {
                //   console.log($scope)
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };


        $scope.findFromAccount = function () {
          personFindService.findMoneyAccounts().then(function (data) {
              data.moneyPositions = null;
            $scope.data.fromAccount = data;
            if (data) {
              $scope.data.fromAccount.creationDate = null;
              $scope.data.fromAccount.identificatorFinishDate = null;
            }
            $http.post('/api/records/getOtherMemberClientShareholdersByAccountID/', {
              data: {
                accountID: data.accountId
              }
            }).then(function (response) {
              Loader.show(false);
              $scope.data.toAccount = response.data.data
            }, function (erro) {
              Loader.show(false)

            })
          })
        };

        $scope.parseFloat = parseFloat

        $scope.findToAccount = function () {
          var services = {
            client: 'findMoneyAccounts',
            other: 'findTradingMember'
          };

          console.log($scope.toAccountChoose)
          var service = services[$scope.toAccountChoose];

          personFindService[service]().then(function (data) {
            $scope.data.toAccount = data;
            if (data) {
              $scope.data.toAccount.creationDate = null;
              $scope.data.toAccount.identificatorFinishDate = null;
            }
          })

        };

        function getAmount() {
          $scope.amount = 0
          if ($scope.data.fromAccount && $scope.data.fromServiceClass && $scope.data.currencyCode) {
            $scope.amountLoading = true
            $http.post('/api/records/getFreeCashAmount/', {
              data: {
                accountID: $scope.data.fromAccount.accountId,
                serviceClassCode: $scope.data.fromServiceClass.code,
                currencyCode: $scope.data.currencyCode
              }
            }).then(function (response) {
              $scope.amountLoading = false
              $scope.amount = response.data.data
            }, function (erro) {
              Loader.show(false)

            })
          }
        }

        $scope.$watch('data.currencyCode', function (newVal) {
          if (newVal) getAmount()
        });

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
