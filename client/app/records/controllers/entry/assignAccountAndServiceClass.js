'use strict';

angular.module('cmisApp')
  .controller('AssignAccountAndServiceClassController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService) {


        $scope.data = angular.fromJson(task.draft);


        function getSum() {
          var sum  = 0
          if(!$scope.data.items) return 0;
          $scope.data.items.forEach(function(obj){
            sum += (parseFloat(obj.trdAmount || 0) + parseFloat(obj.subAmount || 0) + parseFloat(obj.colAmount || 0) + parseFloat(obj.frxAmount || 0) + parseFloat(obj.advAmount || 0))
          })

          console.log(sum);

          return sum
        }




        $scope.data.isHouseAccount = $scope.data.isHouseAccount || false
        $scope.data.correctAssignment = $scope.data.correctAssignment || true
        $scope.data.localeAmount = getSum()


        $scope.serviceClasses = [
          {code: 'PAYMENT_SERVICE_TRD', name: 'Ticarət'},
          {code: 'PAYMENT_SERVICE_SUB', name: 'Abunə'},
          {code: 'PAYMENT_SERVICE_FRX', name: 'Foreks'},
          {code: 'PAYMENT_SERVICE_COL', name: 'Kollateral'},
          {code: 'PAYMENT_SERVICE_ADV', name: 'Avans'},
          {code: 'PAYMENT_SERVICE_TBD', name: 'Təyin olunacaq'}

        ]

        setTimeout(function () {
          $windowInstance.widget.setOptions({width: '80%'})
        }, 200)

        $scope.addItem = function () {
          if (!$scope.data.items) $scope.data.items = [];
          $scope.data.items.push({
            isHouseAccount: false,
            beneficiarAccount: {},
            trdAmount: 0,
            subAmount: 0,
            colAmount: 0,
            frxAmount: 0,
            advAmount: 0
          })
        };


        $scope.removeItem = function (index) {
          $scope.data.items.splice(index, 1)
        };


        if(!$scope.data.items) $scope.addItem();

        console.log($scope.data)
        $scope.formValid = function(){
          var q = $scope.data.paidAmount - $scope.data.localeAmount
          if(!$scope.data.correctAssignment) return true;
          if(q > 0 || q < 0) return false

          return true
        }





        $scope.prepareData = function(data){
          data.items.forEach(function(obj){
            if (obj.beneficiarAccount) {
              if (obj.beneficiarAccount.creationDate) delete obj.beneficiarAccount.creationDate
              if (obj.beneficiarAccount.identificatorFinishDate) delete obj.beneficiarAccount.identificatorFinishDate
            }
          })

          return data
        }



        $scope.$watch('data.items', function(newVal){
              $scope.data.localeAmount = 0;
              $scope.data.localeAmount = getSum()
        },1);


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "assignAccountAndService",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                if ($scope.formValid()) {

                  var data = $scope.prepareData(angular.copy($scope.data))
                  $scope.config.completeTask(data);

                } else {
                  console.log($scope)
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        $scope.findMember = function (obj) {
          personFindService.findAllClientAccounts().then(function (data) {
            obj.beneficiarAccount = data
          })
        };


// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
