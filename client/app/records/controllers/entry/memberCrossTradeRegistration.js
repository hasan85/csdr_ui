'use strict';

angular.module('cmisApp')
  .controller('MemberCrossTradeRegistrationController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog) {


      var prepareFormData = function (formData) {
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }
        if (formData.contractDateObj) {
          formData.contractDate = helperFunctionsService.generateDateTime(formData.contractDateObj);
          delete formData.contractDateObj;
        }

        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.seller === null || draft.seller === undefined) {
          draft.seller = {};
        }
        if (draft.buyer === null || draft.buyer === undefined) {
          draft.buyer = {};
        }
        if (draft.subject === null || draft.subject === undefined) {
          draft.subject = {
            instrument: {
              instrumentId: null,
              ISIN: null,
              issuerName: null,
            },
            quantity: null,
            price: null,

          };
        }
        return draft;
      };

      $scope.otcTrade = initializeFormData(angular.fromJson(task.draft));

      referenceDataService.normalizeReturnedRecordTask($scope.otcTrade, null);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "otcTradeOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var buf = prepareFormData(angular.copy($scope.otcTrade));
                $scope.config.completeTask(buf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.otcTrade);
      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
          //$scope.otcTrade = angular.extend(angular.fromJson(task.draft), $scope.otcTrade);
        }
      }

      // Search seller

      // Search seller
      $scope.findSeller = function () {
        Loader.show(true);
        personFindService.findClientShareholder({
          searchCriteria: {
            isSuspended: false
          }
        }).then(function (data) {
          if ($scope.otcTrade.buyer.id && $scope.otcTrade.buyer.id == data.id) {
            SweetAlert.swal('', gettextCatalog.getString('You cannot select same person as a seller and buyer'), 'error');
          }
          else {
            $scope.otcTrade.seller.name = data.name;
            $scope.otcTrade.seller.accountId = data.accountId;
            $scope.otcTrade.seller.id = data.id;
            $scope.otcTrade.seller.accountNumber = data.accountNumber;
            $scope.otcTrade.seller.depoAccountId = data.depoAccountId;
          }
        });
      };

      // Search buyer
      $scope.findBuyer = function () {

        personFindService.findClientShareholder({
          searchCriteria: {
            isSuspended: false,
          }
        }).then(function (data) {

          if ($scope.otcTrade.seller.id && $scope.otcTrade.seller.id == data.id) {
            SweetAlert.swal('', gettextCatalog.getString('You cannot select same person as a seller and buyer'), 'error');
          } else {
            $scope.otcTrade.buyer.name = data.name;
            $scope.otcTrade.buyer.accountId = data.accountId;
            $scope.otcTrade.buyer.id = data.id;
            $scope.otcTrade.buyer.accountNumber = data.accountNumber;
            $scope.otcTrade.buyer.depoAccountId = data.depoAccountId;
          }

        });
      };

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument().then(function (data) {
          $scope.otcTrade.subject.instrument.ISIN = data.isin;
          $scope.otcTrade.subject.instrument.id = data.id;
          $scope.otcTrade.subject.instrument.issuerName = data.issuerName;
          $scope.otcTrade.subject.instrument.instrumentName = data.instrumentName;
        });
      };


    }]);
