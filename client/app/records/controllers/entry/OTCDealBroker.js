'use strict';

angular.module('cmisApp')
  .controller('OTCDealBrokerController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog) {

      var initializeFormData = function (draft) {

        if (!draft) {
          draft = {}
        }
        if (draft.isSellAction === null || draft.isSellAction === undefined) {
          draft.isSellAction = false;
        }
        if (draft.client === null || draft.client === undefined) {
          draft.client = {};
        }
        if (draft.buyer === null || draft.buyer === undefined) {
          draft.buyer = {};
        }
        if (draft.counterPartyBroker === null || draft.counterPartyBroker === undefined) {
          draft.counterPartyBroker = {};
        }
        if (draft.subject === null || draft.subject === undefined) {
          draft.subject = {
            instrument: {}
          };
        }

        return draft;
      };

      $scope.metaData = {};

      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }

        return formData;
      };

      $scope.otcTrade = initializeFormData(angular.fromJson(task.draft));

      referenceDataService.normalizeReturnedRecordTask($scope.otcTrade, null);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "otcTradeOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var buf = prepareFormData(angular.copy($scope.otcTrade));
                $scope.config.completeTask(buf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.otcTrade);
      });

      // Search client
      $scope.findClient = function () {
        Loader.show(true);
        personFindService.findAllClientAccounts().then(function (data) {
          $scope.otcTrade.client.name = data.name;
          $scope.otcTrade.client.accountId = data.accountId;
          $scope.otcTrade.client.id = data.id;
          $scope.otcTrade.client.accountNumber = data.accountNumber;
          $scope.otcTrade.client.depoAccountId = data.depoAccountId;
        });

      };

      // Search buyer broker
      $scope.findCounterPartyBroker = function () {
        personFindService.findTradingMember().then(function (data) {
          $scope.otcTrade.counterPartyBroker.name = data.name;
          $scope.otcTrade.counterPartyBroker.accountId = data.accountId;
          $scope.otcTrade.counterPartyBroker.id = data.id;
          $scope.otcTrade.counterPartyBroker.accountNumber = data.accountNumber;
        });
      };

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument().then(function (data) {
          $scope.otcTrade.subject.instrument.ISIN = data.isin;
          $scope.otcTrade.subject.instrument.id = data.id;
          $scope.otcTrade.subject.instrument.issuerName = data.issuerName;
          $scope.otcTrade.subject.instrument.instrumentName = data.instrumentName;
        });
      };

    }]);
