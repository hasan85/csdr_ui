'use strict';

angular.module('cmisApp')
  .controller('BatchEntryOfAllotmentController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'FileUploader',
    'SweetAlert', 'gettextCatalog', 'Loader', '$http', 'batchEntryOfAllotmentService', '$filter', 'managePersonService', 'referenceDataService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, FileUploader,
              SweetAlert, gettextCatalog, Loader, $http, batchEntryOfAllotmentService, $filter, managePersonService, referenceDataService) {


      $scope.forceDisablePincodeMandatory = true;

      var initializeFormData = function (draft) {
        if (draft.items === null) {
          draft.items = [];
        }
        if (draft.instrument === null) {
          draft.instrument = {};
        }
        return draft;
      };

      $scope.batchEntryData = initializeFormData(angular.fromJson(task.draft));

      var npConfig = {};
      var jpConfig = {};
      var idDocumentClassCodes = {
        uniqueCode: 'UNIQUE_CODE',
        idCardClassCode: "ID_CARD"
      };
      var personClasses = {};

      $scope.buffers = angular.copy(managePersonService.getPersonDataEntryFormBuffers());

      $scope.data = {
        config: {
          formName: "representativeForm"
        }
      };
      $scope.buffers = angular.copy(managePersonService.getPersonDataEntryFormBuffers());

      var prepareFormData = function (formData) {
        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
        }
        if (formData.registrationDateObj) {
          delete formData.registrationDateObj;
        }
        if (formData.valueDate) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
        }
        if (formData.valueDateObj) {
          delete formData.valueDateObj;
        }
        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        if (formData.auctionClass) {
          formData.auctionClass = angular.fromJson(formData.auctionClass);
        }
        if (formData.entryType) {
          formData.entryType = angular.fromJson(formData.entryType);
        }
        if (formData.personClassName) {
          formData.personClassName = angular.fromJson(formData.personClassName);
        }
        if (formData.personClassName) {
          formData.personClassName = angular.fromJson(formData.personClassName);
        }
        if (formData.docClassName) {
          formData.docClassName = angular.fromJson(formData.docClassName);
        }

        if (formData.entryTypeCode == $scope.metaData.entryTypeCodes.manual) {

          for (var i = 0; i < formData.items.length; i++) {
            if (formData.items[i]['personData'].ID < 0) {
              formData.items[i]['personData'].ID = null;
            }
            formData.items[i]['personData'] = managePersonService.prepareFormData(formData.items[i]['personData'], $scope.metaData);
          }
        }

        return formData;
      };

      var fileParseServiceUrl = '/api/records/upload/';
      var entryTypeCodes = {
        file: 'file',
        manual: 'manual'
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "batchEntryDataOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.form.data = prepareFormData(angular.copy($scope.batchEntryData));
                $scope.config.completeTask($scope.config.form.data);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }, labels: {
          lblSelectRoles: gettextCatalog.getString('Select Roles'),
          actualAddress: gettextCatalog.getString('Actual Address'),
          legalAddress: gettextCatalog.getString('Legal Address'),
          accountInformation: gettextCatalog.getString('Account Information'),
          administratorInformation: gettextCatalog.getString('Administrator Information')
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      function saveTaskData(draft) {
          var data = angular.copy(draft);
          if (data.entryType) data.entryType = angular.fromJson(data.entryType);
          if (data.registrationAuthorityClass) data.registrationAuthorityClass = angular.fromJson(data.registrationAuthorityClass);
          if (data.auctionClass) data.auctionClass = angular.fromJson(data.auctionClass);



          return data;
      }

      // Save task as draft
      $scope.$on('saveTask', function () {
          var saveData = saveTaskData($scope.batchEntryData);
          console.log(saveData);
          $scope.config.saveTask(saveData);
      });

      // If task saved as draft get saved data
      //if (operationState == appConstants.operationStates.active) {
      //  if (task.draft) {
      //    $scope.batchEntryData = angular.fromJson(task.draft);
      //  }
      //}

      var uploader = $scope.uploader = new FileUploader({
        url: '/api/records/upload/',
        queueLimit: 10,
        fileFormDataName: ['file[0]', 'file[1]']
      });
      uploader.filters.push({
        name: "fileSizeLimit",
        fn: function (item) {
          return item.size < appConstants.fileSizeLimit;
        }
      });
      uploader.onWhenAddingFileFailed = function (item, filter) {

        var errorMessages = {
          fileSizeLimit: gettextCatalog.getString('You cannot upload file larger than 5 mb'),
          queueLimit: gettextCatalog.getString('You cannot upload more than 10 file')
        };
        SweetAlert.swal('', errorMessages[filter.name], 'error');

      };
      uploader.onErrorItem = function () {
        Loader.show(false);
        SweetAlert.swal('', gettextCatalog.getString('Files did not uploaded, please try again!'), 'error');
      };

        $scope.total_quantity = 0;
        $scope.$watch("batchEntryData.items", function(newValue) {
            if(newValue) {
                $scope.total_quantity = 0;
                newValue.map(function(item) {
                    $scope.total_quantity += item.quantity;
                });
            }
        }, true);

      uploader.uploadAll = function () {

        Loader.show(true);
        var data = new FormData();

        for (var i = 0; i < uploader.queue.length; i++) {
          data.append("uploadedFile", uploader.queue[i]['_file']);
          uploader.queue[i]['file'].isSuccess = true;
        }
        $http({
          method: 'POST',
          url: fileParseServiceUrl,
          headers: {'Content-Type': undefined},
          data: data
        }).success(function (items) {
          if (items.success == "true") {

            if (items.data) {
              items.data = helperFunctionsService.convertObjectToArray(items.data);
            }
            for (var i = 0; i < items.data.length; i++) {
              var obj = items.data[i];
              if (obj.personData.otherDocuments) {
                obj.personData.otherDocuments = helperFunctionsService.convertObjectToArray(obj.personData.otherDocuments);
              }
              if (obj.personData.phoneNumbers) {
                obj.personData.phoneNumbers = helperFunctionsService.convertObjectToArray(obj.personData.phoneNumbers);
              }
            }
            $scope.batchEntryData.items = items.data;
            SweetAlert.swal('', gettextCatalog.getString('Files successfully uploaded and parsed!'), 'success');
          } else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(items), 'error');
          }

          Loader.show(false);

        });

      };



      $scope.jurisdictionCountryChange = function (person) {
        var model = person.form.jurisdictionCountry ? angular.fromJson(person.form.jurisdictionCountry) : null;
        if (model) {
          person.config.isTINCodeMandatory = model.code == "AZ" ? true : false;
        }
      };

      $scope.currencyChange = function (bankAccountBuffer) {
        var model = bankAccountBuffer.form.currency ? angular.fromJson(bankAccountBuffer.form.currency) : null;
        if (model) {
          bankAccountBuffer.isBankCodeMandatory = model.code == "AZN" ? true : false;
        }
      };

      batchEntryOfAllotmentService.getMetaData().then(function (data) {

        // DETERMINE DOCUMENT TYPES FOR PERSON [[[
        var naturalPersonIdDocumentTypes = juridicalPersonIdDocumentTypes = [];
        if (data.naturalPersonIdDocumentTypes) {
          angular.forEach(data.naturalPersonIdDocumentTypes, function (value) {
            if (value['code'] != idDocumentClassCodes.idCardClassCode) {
              naturalPersonIdDocumentTypes.push(value);
            }
          });
        }
        var juridicalPersonIdDocumentTypes = [];
        if (data.juridicalPersonIdDocumentTypes) {
          angular.forEach(data.juridicalPersonIdDocumentTypes, function (value) {
            if (value['code'] != idDocumentClassCodes.TIN && value['code'] != idDocumentClassCodes.registerCertificate) {
              juridicalPersonIdDocumentTypes.push(value);
            }
          });
        }

        var entryTypes = {};

        entryTypes[entryTypeCodes.manual] = {
          id: 1,
          nameAz: "Manual olaraq daxil et",
          nameEn: 'Manual entry',
          code: entryTypeCodes.manual
        };
        entryTypes[entryTypeCodes.file] = {
          id: 2,
          nameAz: "Fayldan daxil et",
          nameEn: 'File',
          code: entryTypeCodes.file
        };

        $scope.metaData = {
          registrationAuthorityClasses: data.registrationAuthorityClasses,
          personClasses: data.personClasses,
          entryTypes: [
            entryTypes[entryTypeCodes.manual],
            entryTypes[entryTypeCodes.file]
          ],
          entryTypeCodes: entryTypeCodes,
          phoneNumberTypes: data.phoneNumberTypes,
          countries: data.countries,
          currencies: data.currencies,
          businessClasses: data.businessClasses,
          legalFormClasses: data.legalFormClasses,
          allIdDocumentClasses: naturalPersonIdDocumentTypes,
          naturalPersonIdDocumentTypes: naturalPersonIdDocumentTypes,
          juridicalPersonIdDocumentTypes: juridicalPersonIdDocumentTypes,
          signerPositions: data.signerPositions,
          accountStatusCheckingClasses: helperFunctionsService.convertArrayToObjectByKey(
            data.accountStatusCheckingClasses, 'code'),
          auctionClasses: data.auctionClasses
        };


        var configJuridicalPersonRaw = $scope.batchEntryData['juridicalPersonConfiguration'] ?
          angular.fromJson($scope.batchEntryData['juridicalPersonConfiguration']) : null;

        var configNaturalPersonRaw = $scope.batchEntryData['naturalPersonConfiguration'] ?
          angular.fromJson($scope.batchEntryData['naturalPersonConfiguration']) : null;

        // Prepare config data
        jpConfig = configJuridicalPersonRaw ? managePersonService.generateFormConfig(configJuridicalPersonRaw) : null;
        npConfig = configNaturalPersonRaw ? managePersonService.generateFormConfig(configNaturalPersonRaw) : null;

        // Normalize person type classes [[[
        for (var i = 0; i < data.personClasses.length; i++) {
          if (data.personClasses[i].code === appConstants.personClasses.juridicalPerson) {
            personClasses['juridical'] = data.personClasses[i];
            personClasses['juridical']['indexInObject'] = i;

          } else {
            personClasses['natural'] = data.personClasses[i];
            personClasses['natural']['indexInObject'] = i;
          }
        }
        // Normalize person type classes ]]]
        $scope.buffers.representative.data.form.personType = $filter('json')(data.personClasses[personClasses.natural.indexInObject]);
        $scope.buffers.representative.data.config.fields = npConfig;
        $scope.buffers.representative.data.config.isNaturalPersonEnabled = true;
        $scope.buffers.representative.data.config.isJuridicalPersonEnabled = true;
        //$scope.buffers.representative.data.config.isRepresentativeEnabled = true;

        $scope.buffers.selectedEmployee = {};

        if ($scope.batchEntryData && $scope.batchEntryData.items) {
          $scope.batchEntryData.items.forEach(function (item) {
            if (item.personData && item.personData.personType && item.personData.personType.id) {
              item.personData = managePersonService.convertViewModelToForm(item.personData, $scope.metaData);
            }
          });
        }




        referenceDataService.normalizeReturnedRecordTask($scope.batchEntryData, data);

        if ($scope.batchEntryData.entryType && $scope.batchEntryData.entryType != "null") {
          $scope.batchEntryData.entryTypeCode = angular.isString($scope.batchEntryData.entryType) ?
            angular.fromJson($scope.batchEntryData.entryType).code : $scope.batchEntryData.entryType;
        } else {
          $scope.batchEntryData.entryType = $filter('json')(entryTypes[entryTypeCodes.manual]);
          $scope.batchEntryData['entryTypeCode'] = entryTypeCodes.manual;
        }

        Loader.show(false);

      });

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument().then(function (data) {
          $scope.batchEntryData.instrument.ISIN = data.isin;
          $scope.batchEntryData.instrument.id = data.id;
          $scope.batchEntryData.instrument.issuerName = data.issuerName;
          $scope.batchEntryData.instrument.instrumentName = data.instrumentName;
          $scope.batchEntryData.instrument.CFI = data.CFI;
        });

      };

      $scope.entryTypeChange = function () {

        var data = angular.fromJson($scope.batchEntryData.entryType);
        $scope.batchEntryData.entryTypeCode = data.code;
        $scope.batchEntryData.items = [];

      };

      // Search successor
      $scope.findShareholder = function () {

        personFindService.findShareholder().then(function (data) {
          $scope.batchEntryData.items.push({
            personData: {
              ID: data.id,
              name: {
                nameAz: data.name
              },
              functionalAccountNumber: data.accountNumber
            },
            quantity: null
          });

        });

      };

      $scope.addNewItem = function (model) {
        model.push({personData: null, quantity: null});
      };
      $scope.removeItem = function (index, model) {
        model.splice(index, 1);
      };

      $scope.changeShareholderEntryType = function (val, index) {

        $scope.batchEntryData['items'][index] = {isNewPerson: val};
      };

      //Watched variables
      $scope.$watch("buffers.document.currentIndex", function (newVal) {
        $scope.config.documentManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Document') :
          gettextCatalog.getString('Add New Document');

      });
      $scope.$watch("buffers.bankAccount.currentIndex", function (newVal) {
        $scope.config.bankAccountManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Bank Account') : gettextCatalog.getString('Add New Bank Account');

      });
      $scope.$watch("buffers.representative.data.currentIndex", function (newVal) {
        $scope.config.representativeManageWindowTitle = newVal > -1 ? gettextCatalog.getString('Update Shareholder') : gettextCatalog.getString('Add New Shareholder');

      });

      //Manage Person Documents
      $scope.addNewDocument = function (window, model, form) {
        managePersonService.addNewDocument(window, model, form, $scope.buffers.document);
      };
      $scope.removeDocument = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };
      $scope.showDocumentUpdateForm = function (index, model, window) {
        managePersonService.showDocumentUpdateForm(index, model, window, $scope.buffers.document);
      };
      $scope.updateDocument = function (window, model, form) {
        managePersonService.updateDocument(window, model, form, $scope.buffers.document);
      };
      $scope.resetDocumentBuffer = function (window, form) {
        managePersonService.resetDocumentBuffer(window, form, $scope.buffers.document);
      };

      //Manage Person Bank Accounts
      $scope.addNewBankAccount = function (window, model, form) {
        managePersonService.addNewBankAccount(window, model, form, $scope.buffers.bankAccount);
      };
      $scope.removeBankAccount = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };
      $scope.showBankAccountUpdateForm = function (index, model, window) {

        managePersonService.showBankAccountUpdateForm(index, model, window, $scope.buffers.bankAccount);
      };
      $scope.updateBankAccount = function (window, model, form) {
        managePersonService.updateBankAccount(window, model, form, $scope.buffers.bankAccount);
      };
      $scope.resetBankAccountBuffer = function (window, form) {
        managePersonService.resetBankAccountBuffer(window, form, $scope.buffers.bankAccount);
      };

      //Manage Person Phone Numbers
      $scope.addNewPhoneNumber = function (model) {
        managePersonService.addNewPhoneNumber(model);
      };
      $scope.removePhoneNumber = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };

      //Manage Person Emails
      $scope.addNewEmail = function (model) {
        managePersonService.addNewEmail(model)
      };
      $scope.removeEmail = function (index, model) {
        managePersonService.removeFromArray(index, model);
      };

      //Manage Representatives
      $scope.addNewRepresentative = function (model, window) {

        $scope.buffers.representative.data.config.formName.$submitted = true;
        if ($scope.buffers.representative.data.config.formName.$valid) {
          window.close();

          if (angular.fromJson(model.personType).code === appConstants.personClasses.naturalPerson) {
            model.name = {
              nameAz: managePersonService.generateFullNameOfPerson(model)
            };
          }
          $scope.batchEntryData.items.push({
            personData: model,
            quantity: null
          });
          $scope.buffers.representative.data.config.formName.$setPristine();
          $scope.buffers.representative.data.config.formName.$setUntouched();
        }
      };
      $scope.showRepresentativeUpdateForm = function (index, model, window) {

        window.title(gettextCatalog.getString('Update Shareholder'));
        var representative = angular.copy(model[index]);
        $scope.buffers.representative.data.form = representative['personData'];
        $scope.buffers.representative.data.currentIndex = index;
        if (angular.fromJson(model[index].personData.personType).code === appConstants.personClasses.juridicalPerson) {
          $scope.buffers.representative.data.config.fields = jpConfig;
        } else {
          $scope.buffers.representative.data.config.fields = npConfig;
        }

        window.open();
        window.center();
      };
      $scope.updateRepresentative = function (model, window) {

        $scope.buffers.representative.data.config.formName.$submitted = true;
        if ($scope.buffers.representative.data.config.formName.$valid) {

          var representative = angular.copy($scope.buffers.representative.data.form);
          if (angular.fromJson(representative.personType).code === appConstants.personClasses.naturalPerson) {
            representative.name = {
              nameAz: managePersonService.generateFullNameOfNaturalPerson(representative)
            };
          }
          var index = $scope.buffers.representative.data.currentIndex;
          $scope.batchEntryData.items[index]['personData'] = representative;
          $scope.resetRepresentativesBuffer(window);
          window.close();
        }

      };
      $scope.removeRepresentative = function (index, model) {
        model.splice(index, 1);
      };


      $scope.changePersonType = function (personType, isRepresentative, personId) {

        var personClassCode = angular.fromJson(personType)['code'];
        if (personClassCode == appConstants.personClasses.naturalPerson) {
          $scope.buffers.representative.data.config.fields = npConfig;
          $scope.metaData.idDocumentTypes = $scope.metaData.naturalPersonIdDocumentTypes;
        }
        else {

          $scope.buffers.representative.data.config.fields = jpConfig;
          $scope.metaData.idDocumentTypes = $scope.metaData.juridicalPersonIdDocumentTypes;
        }
      };
      $scope.resetRepresentativesBuffer = function (window) {
        $scope.buffers.selectedEmployee = {};
        $scope.buffers.representative.data.config.fields = jpConfig;
        window.title(gettextCatalog.getString('Add New Representative'));
        $scope.buffers.representative.data.currentIndex = -1;
        var personType = $scope.buffers.representative.data.form.personType;
        $scope.buffers.representative.data.config.fields = npConfig;
        $scope.buffers.representative.data.form = {
          personType: personType,
          documents: [],
          bankAccounts: [],
          phoneNumbers: [
            {
              type: null,
              number: ''
            }
          ],
          emails: [
            {value: ''}
          ]
        };
        $scope.buffers.representative.data.config.formName.$setPristine();
        $scope.buffers.representative.data.config.formName.$setUntouched();
      };
      $scope.findPersonByIdCard = function (model, idCard) {

        if (idCard && idCard.series && idCard.number) {

          if (!(model.form.ID )
            && model.form.idCard.series == idCard.series
            && model.form.idCard.number == idCard.number
          ) {
            var searchData = {
              idDocumentSeries: idCard.series,
              idDocumentNumber: idCard.number,
              idDocumentClass: idDocumentClassCodes.idCardClassCode,
              personClassId: personClasses.natural.id
            };
            $scope.updatePersonFromSearch(model, searchData);
          }
        }
      };
      $scope.findPersonByRegistrationCertificate = function (model, regCert) {

        if (regCert && regCert.series && regCert.number) {

          if (!(model.form.ID )
            && model.form.registrationCertificate.series == regCert.series
            && model.form.registrationCertificate.number == regCert.number
          ) {
            var searchData = {
              idDocumentSeries: regCert.series,
              idDocumentNumber: regCert.number,
              idDocumentClass: idDocumentClassCodes.registerCertificate,
              personClassId: personClasses.juridical.id
            };
            $scope.updatePersonFromSearch(model, searchData);
          }
        }
      };
      $scope.findPersonByPinCode = function (model, identificator) {

        if (identificator) {
          if (!(model.form.ID && model.form.pinCode.value == identificator)) {
            var searchData = {
              identificatorClassCode: idDocumentClassCodes.uniqueCode,
              uniqueCode: identificator,
              personClassId: personClasses.natural.id
            };
            $scope.updatePersonFromSearch(model, searchData);
          }
        }
      };
      $scope.findPersonByTINCode = function (model, identificator) {

        if (identificator) {
          if (!(model.form.ID && model.form.TIN.value == identificator)) {
            var searchData = {
              identificatorClassCode: idDocumentClassCodes.uniqueCode,
              uniqueCode: identificator,
              personClassId: personClasses.juridical.id
            };
            $scope.updatePersonFromSearch(model, searchData);
          }
        }
      };
      $scope.updatePersonFromSearch = function (person, searchData) {
        Loader.show(true);
        $scope.config.duplicateUserDetected = false;
        personFindService.checkAccountStatusByRole(searchData, 'ROLE_SHAREHOLDER').then(function (data) {

          if (data && data.success === "true") {

            if ($scope.metaData.accountStatusCheckingClasses.ACCOUNT_NOT_EXIST.code == data.data.code) {

            } else if ($scope.metaData.accountStatusCheckingClasses.ACCOUNT_EXIST.code == data.data.code) {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'success');
              $scope.updatePersonData(person, personFindService.normalizePersonArrayFields(data.data.personData));
            } else if ($scope.metaData.accountStatusCheckingClasses.ACCOUNT_DUPLICATED.code == data.data.code && !$scope.config.isUpdateOperation) {
              $scope.config.duplicateUserDetected = true;
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }

          } else {
            SweetAlert.swal('', gettextCatalog.getString("Internal Server Error"), 'error');
          }
          Loader.show(false);
        });
      };
      $scope.updatePersonData = function (oldData, newData) {
        Loader.show(false);
        oldData.form = managePersonService.convertViewModelToForm(newData, $scope.metaData);
        $scope.changePersonType(oldData.form.personType, false, oldData.form.ID);
      };
    }]);
