'use strict';

angular.module('cmisApp')
  .controller('UnknownWithdrawalOperationController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService) {


        $scope.init = function () {
          $scope.data = angular.fromJson(task.draft);

          angular.forEach($scope.data.items, function (item) {
                      angular.forEach($scope.withdrawalTypes, function (type) {
                                            if (type.value == item.withdrawalType) {
                                              item.searchVisible = type.searchVisible;
                                              item.hasFeeVisible = type.hasFeeVisible;
                                              item.findService = type.findService
                                            }
                                          });
                    });

          $scope.data.localeAmount = getSum();
          if (!$scope.data.items) $scope.addItem();
        }


        function getSum() {
          var sum = 0;
          if (!$scope.data.items) return 0;
          $scope.data.items.forEach(function (obj) {
            sum += parseFloat(obj.amount)
          });

          return sum
        }
        alert("!!!!!!");


        $scope.withdrawalTypes = [
          {
            value: 'TAX_CURR_EXCHANGE',
            name: ' Mənbə vergisinin konvertasiyası',
            searchVisible: true,
            hasFeeVisible: true,
            findService: personFindService.searchTaxBalances
          },
          {
            value: 'CURR_EXCHANGE_COMMISSION',
            name: 'Mənbə vergisi konversiyası x/h',
            searchVisible: false
          },
          {
            value: 'VAT',
            name: 'ƏDV',
            searchVisible: false
          },
          {
            value: 'XOHKS_COMMISSION',
            name: 'XÖHKS x/h',
            searchVisible: false
          },
          {
            value: 'UNKNOWN',
            name: 'Naməlum məxaric',
            searchVisible: false
          },
          {
            value: 'TAX',
            name: 'Mənbə vergisi',
            searchVisible: true,
            findService: personFindService.searchTaxBalances
          }
        ];


        $scope.onWithdrawalTypeChange = function (obj) {
          angular.forEach($scope.withdrawalTypes, function (type) {
            if (type.value == obj.withdrawalType) {
              obj.searchVisible = type.searchVisible;
              obj.hasFeeVisible = type.hasFeeVisible;
              obj.findService = type.findService
            }
          })

        };

        $scope.addItem = function () {
          if (!$scope.data.items) $scope.data.items = [];
          $scope.data.items.push({
            amount: 0,
            hasFee: null
          })
        };


        $scope.removeItem = function (index) {
          $scope.data.items.splice(index, 1)
        };


        $scope.formValid = function () {
          var q = $scope.data.paidAmount - $scope.data.localeAmount
          if (!$scope.data.correctAssignment) return true;
          if (q > 0 || q < 0) return false

          return true
        }


        $scope.prepareData = function (data) {
          data.items.forEach(function (obj) {

          })

          return data
        }


        $scope.$watch('data.items', function (newVal) {
          $scope.data.localeAmount = 0;
          $scope.data.localeAmount = getSum()
        }, 1);


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "assignAccountAndService",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                if ($scope.formValid()) {

                  var data = $scope.prepareData(angular.copy($scope.data))
                  $scope.config.completeTask(data);

                } else {
                  console.log($scope)
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        $scope.findAccount = function (obj) {
          obj.findService().then(function (data) {
            obj.account = data.account;
            obj.maxAmount = data.amount
            obj.accountName = data.account.name

            console.log(obj)
          })
        };


// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });


        $scope.init()

      }])
