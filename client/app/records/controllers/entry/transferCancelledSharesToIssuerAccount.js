'use strict';

angular.module('cmisApp')
  .controller('TransferCancelledSharesToIssuerAccountController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', '$filter',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $filter) {

      $scope.metaData = {};
      Loader.show(false);

      var prepareFormData = function (formData) {

        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        return draft;
      };

      $scope.taskData = initializeFormData(angular.fromJson(task.draft));


      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "taskForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var buf = prepareFormData(angular.copy($scope.taskData));
                $scope.config.completeTask(buf);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.otcTrade);
      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer({
          searchCriteria: {
            isSuspended: false
          }
        }).then(function (data) {
          $scope.issuer = {};
          $scope.issuer.name = data.name;
          $scope.issuer.accountId = data.accountId;
          $scope.issuer.id = data.id;
          $scope.issuer.accountNumber = data.accountNumber;

          $scope.taskData.issuer = $scope.issuer;
        });
      };

      // Search instrument
      $scope.findInstrument = function () {
        instrumentFindService.findInstrument({
          params: {
            issuerId: $scope.issuer.id
          }
        }).then(function (data) {
          $scope.instrument = {};
          $scope.instrument.ISIN = data.isin;
          $scope.instrument.instrumentID = data.id;
          $scope.instrument.issuerName = data.issuerName;
          $scope.instrument.instrumentName = data.instrumentName;

          $scope.taskData.instrument = $scope.instrument;
          $scope.taskData.instrumentID = data.id;
        });
      };



    }]);
