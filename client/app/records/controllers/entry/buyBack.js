'use strict';

angular.module('cmisApp')
  .controller('BuyBackController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', '$filter',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $filter) {

      $scope.metaData = {};
      Loader.show(true);

      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }


        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.seller === null || draft.seller === undefined) {
          draft.seller = {};
        }
        if (draft.brokerSeller === null || draft.brokerSeller === undefined) {
          draft.brokerSeller = {};
        }
        if (draft.buyer === null || draft.buyer === undefined) {
          draft.buyer = {};
        }
        if (draft.brokerBuyer === null || draft.brokerBuyer === undefined) {
          draft.brokerBuyer = {};
        }
        if (draft.subject === null || draft.subject === undefined) {
          draft.subject = {
            instrument: {
              instrumentId: null,
              ISIN: null,
              issuerName: null,
            },
            quantity: null,
            price: null,

          };
        }

        return draft;
      };

      $scope.otcTrade = initializeFormData(angular.fromJson(task.draft));

      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data
        };
        referenceDataService.normalizeReturnedRecordTask($scope.otcTrade, {registrationAuthorityClasses: data});
        Loader.show(false);
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "otcTradeOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var buf = prepareFormData(angular.copy($scope.otcTrade));
                $scope.config.completeTask(buf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.otcTrade);
      });

      // If task saved as draft get saved data
      if (operationState == appConstants.operationStates.active) {
        if (task.draft) {
          /// $scope.otcTrade = angular.fromJson(task.draft);
        }
      }

      // Search seller

      // Search seller
      $scope.findSeller = function () {

        Loader.show(true);
        personFindService.findShareholder({
          searchCriteria: {
            isSuspended: false
          }
        }).then(function (data) {
          $scope.otcTrade.seller = {};
          $scope.otcTrade.seller.name = data.name;
          $scope.otcTrade.seller.accountId = data.accountId;
          $scope.otcTrade.seller.id = data.id;
          $scope.otcTrade.seller.accountNumber = data.accountNumber;
        });

      };

      // Search buyer
      $scope.findBuyer = function () {

        personFindService.findIssuer({
          searchCriteria: {
            isSuspended: false,
          }
        }).then(function (data) {

          $scope.otcTrade.buyer = {};
          $scope.otcTrade.buyer.name = data.name;
          $scope.otcTrade.buyer.accountId = data.accountId;
          $scope.otcTrade.buyer.id = data.id;
          $scope.otcTrade.buyer.accountNumber = data.accountNumber;
        });
      };


      // Search seller broker
      $scope.findSellerBroker = function () {

        personFindService.findTradingMember().then(function (data) {

          $scope.otcTrade.brokerSeller = {};
          $scope.otcTrade.brokerSeller.name = data.name;
          $scope.otcTrade.brokerSeller.accountId = data.accountId;
          $scope.otcTrade.brokerSeller.id = data.id;
          $scope.otcTrade.brokerSeller.accountNumber = data.accountNumber;
        });

      };
      // Search buyer broker
      $scope.findBuyerBroker = function () {

        personFindService.findTradingMember().then(function (data) {

          $scope.otcTrade.brokerBuyer = {};
          $scope.otcTrade.brokerBuyer.name = data.name;
          $scope.otcTrade.brokerBuyer.accountId = data.accountId;
          $scope.otcTrade.brokerBuyer.id = data.id;
          $scope.otcTrade.brokerBuyer.accountNumber = data.accountNumber;
        });

      };

      // Search instrument
      $scope.findInstrument = function () {

        instrumentFindService.findInstrument().then(function (data) {
          $scope.otcTrade.subject.instrument = {};
          $scope.otcTrade.subject.instrument.ISIN = data.isin;
          $scope.otcTrade.subject.instrument.id = data.id;
          $scope.otcTrade.subject.instrument.issuerName = data.issuerName;
          $scope.otcTrade.subject.instrument.instrumentName = data.instrumentName;
        });
      };


    }]);
