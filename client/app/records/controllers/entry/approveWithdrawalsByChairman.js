'use strict';

angular.module('cmisApp')
  .controller('ApproveWithdrawalsByChairmanController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http', '$q',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http, $q) {


        Loader.show(true);
        $http.post('/api/records/getPendingWithdrawals/', {
          data: {
            status: 'STATUS_CHAIRMAN_APPROVAL_PENDING'
          }
        }).then(function (response) {
          Loader.show(false);
          $scope.data = {
            vmWithdrawals: angular.isArray(response.data.data)? response.data.data : [response.data.data]
          };
          console.log($scope.data);

          $scope.data.vmWithdrawals.forEach(function(obj){
            obj.chairmanApproved = null;
            obj.expanded = false;
          })

        }, function (error) {
          Loader.show(false)

        })

        $scope.expand = function() {
            console.log(this);
            console.log(this.$index);
            var obj = this.obj;
            if(!this.obj.details) {
                Loader.show(true);
                        $http.post('/api/clearing/getAccountRecordByWithdrawalID/', {
                            operationID: obj.ID
                        }).then(function (response) {
                          obj.details = angular.isArray(response.data.data)? response.data.data : [response.data.data]
                          console.log(response.data);

                          obj.expanded = !obj.expanded
                          Loader.show(false);

                        }, function (error) {
                          Loader.show(false)

                        })
            } else {
               obj.expanded = !obj.expanded
            }
        }



        $scope.tr = {
          selectAll: null
        };


        setTimeout(function () {
          $windowInstance.widget.setOptions({width: '80%'})
        }, 200)


        $scope.$watch('tr.selectAll', function (newVal, oldVal) {
          var val = oldVal === false ? null : newVal
          if (newVal != oldVal) {
            $scope.data.vmWithdrawals.forEach(function (obj) {
              obj.chairmanApproved = val
            })

            $scope.tr.selectAll = val
          }
        })

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                var signables = $scope.data.vmWithdrawals.filter(function (obj) {
                  return obj.chairmanApproved;
                });

                $http({
                  method: "GET",
                  url: "https://azipssigner:8099/TestService/sign?inputData=test"
                }).then(
                  function (response) {
                    if (response) {
                      var promises = signables.map(function (obj, key) {
                        return _sign(obj)
                      })

                      $q.all(promises).then(function () {
                        var data = angular.copy($scope.data)
                        data.vmWithdrawals.forEach(function(obj){
                          if(!angular.isObject(obj.memberAccount)) obj.memberAccount = null
                          if(!angular.isObject(obj.account)) obj.account = null
                          obj.creationDate = helperFunctionsService.generateDateTime(obj.creationDate)
                          obj.withdrawalDate = helperFunctionsService.generateDateTime(obj.withdrawalDate)
                          obj.sendDate = helperFunctionsService.generateDateTime(obj.sendDate)
                        })

                        console.log(data)
                        $scope.config.completeTask(data);
                      })
                    }
                  }
                );

                // //check service is working
                // _sign(signables[0]).then(
                //   function (success) {
                //     if(success){
                //       var promises = signables.map(function (obj, key) {
                //         if(key != 0) return _sign(obj)
                //       })
                //
                //       $q.all(promises).then(function () {
                //         $scope.config.completeTask($scope.data);
                //       })
                //     }else{
                //       // SweetAlert.error('Webservice not working')
                //     }
                //
                //   }
                // )
              }
            }
          }
        };

        var _sign = function (obj) {
          obj.loading = true;
          return $http({
            method: "GET",
            url: "https://azipssigner:8099/TestService/sign?inputData=" + obj.block4
          }).then(
            function (response) {
              if (response && response.data) {
                obj.msgMacResult = response.data;
                obj.loading = false
                obj.success = true
              } else {
                obj.loading = false
                obj.error = true
              }
            },
            function (error) {
              obj.loading = false
              obj.error = true
            }
          );
        }


        $scope.sign = function (obj) {
          if (obj) {
            _sign(obj)
          } else {
            $scope.data.vmWithdrawals.map(function (obj) {
              if (obj.chairmanApproved) _sign(obj)
            })
          }
        }

        $scope.isLoading = function (obj) {
          var loading = false
          $scope.data.vmWithdrawals.map(function (obj) {
            if (obj.loading) loading = true
          })

          return loading
        }


        $scope.chairmanApprovedChanged = function(obj){
          if(obj.chairmanApproved === true && obj.prevChairmanApproved === false){
            obj.chairmanApproved = null
          }
          obj.prevChairmanApproved = obj.chairmanApproved
        }

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
