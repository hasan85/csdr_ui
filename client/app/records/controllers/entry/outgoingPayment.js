'use strict';

angular.module('cmisApp')
  .controller('OutgoingPaymentController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.metaData = {}
        $scope.data = angular.fromJson(task.draft);
        $scope.data.isHouseAccount = null
        $scope.data.isMemberBankAccount = true


        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });

        referenceDataService.getAZIPSSessionInfo().then(function (data) {

          $scope.metaData.sessionInfo = data.data;
          $scope.sessionType = data.data.currentSessionNumber ?  'now': 'next' ;
          $scope.sessionTypeChange($scope.sessionType);
          Loader.show(false);
        });

        $scope.kendoDateOptions = {
            min: new Date()
        };


        $scope.sessionTypeChange = function(newVal){
          if (newVal) {
            if(newVal == 'now'){
              $scope.data.sessionNumber = $scope.metaData.sessionInfo.currentSessionNumber;
              $scope.data.withdrawalDateObj = new Date();
            }
            else if(newVal == 'next'){

              $scope.data.sessionNumber = $scope.metaData.sessionInfo.nextSessionNumber;
              $scope.data.withdrawalDateObj = new Date($scope.metaData.sessionInfo.nextSessionDate)
            }
            else {
              $scope.data.sessionNumber = null;
              $scope.data.withdrawalDate = null
            }
          }
        }


        $scope.memberAccount = angular.copy($scope.data.memberAccount);
        $scope.accountNumber = $scope.memberAccount.accountNumber
        $scope.accountName = $scope.memberAccount.name


        delete $scope.data.memberAccount;

        $scope.prepareData = function (data) {

          if (data.account) {
            if (data.account.identificatorFinishDate) delete data.account.identificatorFinishDate
            if (data.account.creationDate) delete data.account.creationDate
          }

          if (data.withdrawalDateObj) {
            console.log(data.withdrawalDateObj)
            data.withdrawalDate = helperFunctionsService.generateDateTime(data.withdrawalDateObj);
            delete data.withdrawalDateObj

          }

          return data
        }

        setTimeout(function () {
          $windowInstance.widget.setOptions({width: '80%'})
        }, 200)


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.formValid()) {
                //
                var data = $scope.prepareData(angular.copy($scope.data));
                $scope.config.completeTask(data);
                //
                // } else {
                //   console.log($scope)
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };

        $scope.$watch('data.currency', function (newVal) {
          if (newVal) {
            if ($scope.data.isHouseAccount && !$scope.data.isMemberBankAccount) {
              $scope.getMemberClientBankAccounts()
            } else {
              $scope.getWithdrawalData()
            }
          }
        });

        $scope.$watch('data.account', function (newVal) {
          if (newVal) {
            $scope.getWithdrawalData()
          }
        });

        $scope.$watch('data.isMemberBankAccount', function (newVal) {
          $scope.bankAccounts = []
          if ($scope.data.isHouseAccount && !$scope.data.isMemberBankAccount) {
            $scope.getMemberClientBankAccounts()
          } else {
            $scope.getWithdrawalData()
          }
        });


        $scope.setAccount = function () {
          if ($scope.data.isHouseAccount === false) {
            $scope.data.account = null;
            $scope.accountNumber = ''
            $scope.accountName = ''
          } else {
            $scope.data.account = $scope.memberAccount;
            $scope.accountNumber = $scope.memberAccount.accountNumber
            $scope.accountName = $scope.memberAccount.name
          }
        }


        $scope.findMember = function () {
          personFindService.findMoneyAccounts().then(function (data) {
            $scope.accountNumber = data.accountNumber
            $scope.accountName = data.name

            $scope.data.account = {
              accountId: data.accountId,
              name: data.name,
              id: data.id
            }
          })
        };


        $scope.findMemberClientAccount = function () {
          personFindService.findAllClientAccounts().then(function (data) {
            $scope.memberClientAccountNumber = data.accountNumber;
            $scope.memberClientAccountName = data.name;

            $scope.data.memberClientAccount = {
              accountId: data.accountId
            };
            $scope.getMemberClientBankAccounts()
          })
        };


        $scope.getWithdrawalData = function () {

          var account = $scope.data.account || $scope.memberAccount

          console.log(account)
          if ($scope.data.currency && account) {
            Loader.show(true)
            $http.post('/api/records/getWithdrawalData/', {
              data: {
                currencyID: $scope.data.currency.id,
                accountID: account.accountId
              }
            }).then(function (response) {
              Loader.show(false);
              var data = response.data.data;
              if (angular.isArray(data.bankAccounts)) {
                $scope.bankAccounts = data.bankAccounts
              } else if (data.bankAccounts && data.bankAccounts.id) {
                $scope.bankAccounts = [data.bankAccounts]
              } else {
                $scope.bankAccounts = []
              }

              $scope.data.trdAmount = data.trdAmount;
              $scope.data.subAmount = data.subAmount;
              $scope.data.frxAmount = data.frxAmount;
              $scope.data.disAmount = data.disAmount;
              $scope.data.colAmount = data.colAmount;
              $scope.data.trdWithdrawalAmount = data.trdWithdrawalAmount;
              $scope.data.subWithdrawalAmount = data.subWithdrawalAmount;
              $scope.data.frxWithdrawalAmount = data.frxWithdrawalAmount;
              $scope.data.disWithdrawalAmount = data.disWithdrawalAmount;
              $scope.data.colWithdrawalAmount = data.colWithdrawalAmount;

            }, function (erro) {
              Loader.show(false)

            })
          }

        }


        $scope.getMemberClientBankAccounts = function () {

          if ($scope.data.currency && $scope.data.memberClientAccount) {
            console.log($scope.data.memberClientAccount)

            Loader.show(true)

            $http.post('/api/records/getBankAccounts/', {
              data: {
                currencyID: $scope.data.currency.id,
                accountID: $scope.data.memberClientAccount.accountId
              }
            }).then(function (response) {
              Loader.show(false);
              var data = response.data.data;
              if (angular.isArray(data)) {
                $scope.bankAccounts = data
              } else if (data && data.bankNameAz) {
                $scope.bankAccounts = [data]

              } else {
                $scope.bankAccounts = []
              }

            }, function (erro) {
              Loader.show(false)

            })
          }
        }


// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
