'use strict';

angular.module('cmisApp')
  .controller('RemainingBalancesController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {

        $scope.metaData = {}
        referenceDataService.getCurrencies().then(function (data) {

          $scope.metaData.currencies = data;
          $scope.data.currencyCode = 'AZN';

          referenceDataService.addEmptyOption([
            $scope.metaData.currencies

          ]);

          Loader.show(false);
        });


        $scope.data = {};

        var today = new Date();

        var firstDateWithSlashes = (today.getDate()) + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();

        $scope.data.dateObj = today;
        $scope.data.date = firstDateWithSlashes;


        var findData = function (e) {


          var formData = angular.copy($scope.data);

          formData.date = $scope.data.dateObj;

          delete formData.dateObj;

          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          requestData.currencyCode = requestData.currencyCode || 'AZN';

          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/records/getRemainingBalances/',
            data: {data: formData}
          }).then(function (response) {

            var records = response.data;

            e.success({
              Data: records.data ? records.data : [], Total: records.total
            });

            Loader.show(false);
          });

        };


        $scope.findData = function () {
          $scope.searchResultGrid.dataSource.page(1);
        }


        $scope.mainGridOptions = {
          excel: {
            allPages: true
          },
          dataSource: {
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  date: {type: "date"},
                  remainingAmount: {type: "number"}
                }
              }
            },
            transport: {
              read: function (e) {
                findData(e);
              }
            },

            serverPaging: true,
            serverSorting: true
          },
          selectable: true,
          scrollable: true,
          pageable: {
            input: false,
            numeric: true
          },
          // pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
          sortable: true,
          resizable: true,
          columns: [
            {
              field: "name",
              title: "Şəxs (S.A.A.[F.Ş],tam adı [H.Ş])",
              width: '10rem'
            },
            {
              field: "accountNumber",
              title: "Daxili pul hesabının nömrəsi:",
              width: '10rem'
            },
            {
              field: "serviceClass.nameAz",
              title: "Pul hesabının bölməsi",
              width: '10rem'
            },
            {
              field: "accountManagerName",
              title: "Hesabı idarə edən",
              width: '10rem'
            },
            {
              field: "currency.code",
              title: "Valyuta",
              width: '10rem'
            },
            {
              field: "remainingAmount",
              title: "Qalıq",
              width: '10rem',
              template: "<div style='text-align: right'>#= kendo.toString(remainingAmount, 'n2') # </div>"
            }
          ]
        };

        $scope.exportToExcel = function () {
          $scope.searchResultGrid.saveAsExcel();
        }


      }])
;
