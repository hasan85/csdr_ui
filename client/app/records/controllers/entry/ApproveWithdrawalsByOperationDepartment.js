'use strict';

angular.module('cmisApp')
  .controller('ApproveWithdrawalsByOperationDepartmentController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService','$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        Loader.show(true);
        $http.post('/api/records/getPendingWithdrawals/', {
          data: {
            status: 'STATUS_OPERATION_APPROVAL_PENDING'
          }
        }).then(function (response) {
          Loader.show(false);
          $scope.data = {
            vmWithdrawals: angular.isArray(response.data.data)? response.data.data : [response.data.data]
          };

          $scope.data.vmWithdrawals.forEach(function(obj){
            if(!obj.hasOwnProperty('opDepApproved')) obj.opDepApproved = null
          })


        }, function (erro) {
          Loader.show(false)

        })



        $scope.tr = {
            selectAll: null
        }

        setTimeout(function () {
          $windowInstance.widget.setOptions({width: '80%'})
        }, 200)




        $scope.$watch('tr.selectAll', function(newVal, oldVal){
          console.log(newVal)
            if(newVal != oldVal){
               $scope.data.vmWithdrawals.forEach(function(obj){
                  obj.opDepApproved = newVal
               })
            }
        })


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.formValid()) {
                //
                var data = angular.copy($scope.data);

                data.vmWithdrawals.forEach(function(obj){
                   if(!angular.isObject(obj.memberAccount)) obj.memberAccount = null
                   if(!angular.isObject(obj.account)) obj.account = null
                   obj.creationDate = helperFunctionsService.generateDateTime(obj.creationDate)
                   obj.withdrawalDate = helperFunctionsService.generateDateTime(obj.withdrawalDate)
                })


                $scope.config.completeTask(data);
                //
                // } else {
                //   console.log($scope)
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };



// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
