'use strict';

angular.module('cmisApp').controller('CorrectingDatabaseController', ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', "$http", function ($scope, $windowInstance, id, appConstants, operationService, personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $http) {

  var titleTransferOperations = {
    grantOperation: "grantoperation_entry",
    titleTransfer: "titleTransfer_entry"
  };
  $scope.metaData = {};
  Loader.show(true);


  referenceDataService.getCouponPaymentMethodClasses().then(function (data) {

    $scope.metaData.couponPaymentMethods = data;

    referenceDataService.addEmptyOption([
      $scope.metaData.couponPaymentMethods
    ]);
  });


  $scope.isGrantOperation = task.key == titleTransferOperations.grantOperation ? true : false;
  //
  // var prepareFormData = function (formData) {
  //
  //     if (formData.registrationDateObj) {
  //         formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
  //         delete formData.registrationDateObj;
  //     }
  //     if (formData.valueDateObj) {
  //         formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
  //         delete formData.valueDateObj;
  //     } else {
  //         formData.valueDate = formData.registrationDate;
  //     }
  //
  //     for (var i = 0; i < formData.subjects.length; i++) {
  //         delete formData.subjects[i]['validation'];
  //     }
  //     if (formData.registrationAuthorityClass) {
  //         formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
  //     }
  //     return formData;
  // };

  var instrumentFormPartialTemplate = {
    instrument: {
      issuerID: '',
      ISIN: '',
      issuerName: '',
    },
    quantity: 0,
    validation: {
      isValid: false,
      message: ''
    }
  };

  var initializeFormData = function (draft) {
    if (!draft) {
      draft = {}
    }
    if (draft.owner === null || draft.owner === undefined) {
      draft.owner = {};
    }
    if (draft.receiver === null || draft.receiver === undefined) {
      draft.receiver = {};
    }
    if (draft.subjects === null || draft.subjects === undefined) {
      draft.subjects = [
        angular.copy(instrumentFormPartialTemplate)
      ]
    }
    return draft;
  };
  $scope.taskData = angular.fromJson(task.draft);
  $scope.grant = initializeFormData($scope.taskData.titleTransfer);
  if ($scope.taskData.instrumentData == null) {
    $scope.taskData.instrumentData = {}
  }
  if ($scope.taskData.correctionType == null) {
    $scope.taskData.correctionType = "titleTransferChange";
  }
  $scope.instrumentData = $scope.taskData.instrumentData;

  //referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
  //    $scope.metaData = {
  //        registrationAuthorityClasses: data
  //    };
  //    referenceDataService.normalizeReturnedRecordTask($scope.grant, {registrationAuthorityClasses: data});
  //    Loader.show(false);
  //});

  Loader.show(false);

  //Initialize scope variables [[
  $scope.config = {
    screenId: id,
    taskKey: id,
    task: task,
    form: {
      name: "",
      data: {}
    },
    operationType: appConstants.operationTypes.entry,
    window: $windowInstance,
    state: operationState,
    buttons: {
      complete: {
        click: function () {
          $scope.config.form.name.titleTransfer.$submitted = true;
          $scope.config.form.name.instrumentDataChange.$submitted = true;

          if ($scope.taskData.correctionType == "titleTransferChange" && $scope.config.form.name.titleTransfer.$valid) {

            var taskData = angular.copy($scope.taskData);
            taskData.titleTransfer = $scope.taskData.correctionType == "titleTransferChange" ? $scope.grant : null;
            taskData.instrumentData = $scope.taskData.correctionType != "titleTransferChange" ? $scope.instrumentData : null;

            $scope.config.completeTask(taskData);

          } else if ($scope.taskData.correctionType != "titleTransferChange" && $scope.config.form.name.instrumentDataChange != null) {

            var taskData = angular.copy($scope.taskData);
            if (taskData.instrumentData) {
              $scope.instrumentData.couponRate = $scope.instrumentData.couponRate || null;
            }

            if (taskData.instrumentData.maturityDateObj) {
              taskData.instrumentData.maturityDate = helperFunctionsService.generateDateTime(taskData.instrumentData.maturityDateObj);
              delete taskData.instrumentData.maturityDateObj;
            }
            if (taskData.instrumentData.regDateObj) { //AZ1001007884
              taskData.instrumentData.regDate = helperFunctionsService.generateDateTime(taskData.instrumentData.regDateObj);
              delete taskData.instrumentData.regDateObj;
            }

            $scope.config.completeTask(taskData);

          } else {
            SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
          }
        }
      }
    }
  };

  // Check if form is dirty
  $scope.$on('closeTask', function () {
    $scope.config.showTaskSavePrompt(false);
  });

  // Save task as draft
  $scope.$on('saveTask', function () {
    $scope.config.saveTask($scope.grant);
  });

  // Search owner
  $scope.findOwner = function () {
    Loader.show(true);
    personFindService.findShareholderOrIssuer({
      searchCriteria: {
        isSuspended: false
      }
    }).then(function (data) {
      $scope.grant.owner.name = data.name;
      $scope.grant.owner.accountId = data.accountId;
      $scope.grant.owner.id = data.id;
      $scope.grant.owner.accountNumber = data.accountNumber;
    });

  };

  // Search receiver
  $scope.findReceiver = function () {
    personFindService.findShareholderOrIssuer({
      searchCriteria: {
        isSuspended: false
      }
    }).then(function (data) {

      $scope.grant.receiver.name = data.name;
      $scope.grant.receiver.accountId = data.accountId;
      $scope.grant.receiver.id = data.id;
      $scope.grant.receiver.accountNumber = data.accountNumber;
    });
  };

  // Search instrument
  $scope.findInstrument = function (index) {
    instrumentFindService.findInstrument().then(function (data) {
      $scope.grant.subjects[index].instrument.ISIN = data.isin;
      $scope.grant.subjects[index].instrument.id = data.id;
      $scope.grant.subjects[index].instrument.issuerName = data.issuerName;
      $scope.grant.subjects[index].instrument.issuerID = data.issuerId;
      $scope.grant.subjects[index].instrument.instrumentName = data.instrumentName;
    });
  };

  $scope.findAllInstrument = function () {
    console.log($scope.instrumentData);
    instrumentFindService.findInstrument().then(function (instrument) {

      Loader.show(true);

      $http({
        url: "/api/instruments/getInstrumentById/" + instrument.id
      }).then(function (response) {
        var data = response.data ? response.data.data : {};
        console.log(data);
        $scope.instrumentData.instrumentID = data.id;
        $scope.instrumentData.oldConventionalDayCount = data.conventionalDayCount;
        $scope.instrumentData.oldQuantity = data.quantity;
        $scope.instrumentData.issuerName = data.issuer.name;
        $scope.instrumentData.oldRegistrar = data.registrar;
        $scope.instrumentData.oldCouponPaymentMethod = data.couponPaymentMethod ? data.couponPaymentMethod.nameAz: '';
        $scope.instrumentData.oldISIN = data.ISIN;
        // $scope.instrumentData.cfi = data.CFI;
        $scope.instrumentData.isin = data.ISIN;
        $scope.instrumentData.oldCouponRate = data.couponRate;
        $scope.instrumentData.oldMaturityDate = data.maturityDate;
        $scope.instrumentData.oldCFI = data.CFI;
        $scope.instrumentData.oldRegDate = data.regDate;

        Loader.show(false);

      });
    });
  };

  // Query CFI hierarchy
  $scope.getChildOptions = function (classId, index) {
    if (index != 6) {
      var startIndex;

      if (classId == '') {
        startIndex = index + 1;
      } else {
        startIndex = index + 2;
        classId = parseInt(classId);

        if (classId > 0) {

          Loader.show(true);

          referenceDataService.getcurrentCFIClasses(classId).then(function (data) {

            var nextClassesIndex = index + 1;
            $scope.metaData['sel' + nextClassesIndex] = data;

            Loader.show(false);

          });
        }
      }
      if (startIndex <= 6) {
        for (var i = startIndex; i <= 6; i++) {
          $scope.metaData['sel' + i] = null;
        }
      }
    }

  };

  referenceDataService.getcurrentCFIClasses(0).then(function (value) {
    $scope.metaData["sel1"] = value;
  });


  // Get generated CFI
  $scope.generateCFI = function (window) {
    for (var i = 0; i < $scope.metaData.sel6.length; i++) {
      var item = $scope.metaData.sel6[i];
      if ($scope.instrument.cfiForm.sel6 == item.id) {
        $scope.instrumentData.cfi = item;
        window.close();
        break;
      }
    }
  };

  $scope.isBond = function () {
    var isNewCFIBond = false;
    var isOldCFIBond = false;
    if ($scope.instrumentData.cfi) {
      isNewCFIBond = helperFunctionsService.startsWith($scope.instrumentData.cfi.code, 'CFI_D');
    }
    if ($scope.instrumentData.oldCFI) {
      isOldCFIBond = helperFunctionsService.startsWith($scope.instrumentData.oldCFI.code, 'CFI_D');
    }
    return isNewCFIBond || isOldCFIBond;
  };

  // Add new instrument

  $scope.addNewInstrument = function (model) {
    model.push(angular.copy(instrumentFormPartialTemplate));
  };

  // Remove instrument
  $scope.removeInstrument = function (index, model) {
    model.splice(index, 1);
  };

  $scope.titleTransfer = false;

}]);
