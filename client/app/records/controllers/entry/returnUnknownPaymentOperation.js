'use strict';

angular.module('cmisApp')
  .controller('ReturnUnknownPaymentOperationController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.metaData = {}
        $scope.data = angular.fromJson(task.draft);

        $scope.data.items.forEach(function(obj, key){
          obj.withdrawalDateObj = helperFunctionsService.parseDate(obj.withdrawalDate)
        })

        $scope.sessions = [
          {value: 1},
          {value: 2},
          {value: 3}
        ];

        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });

        $scope.memberAccount = angular.copy($scope.data.memberAccount)


        $scope.azipsFeePaymentTypes = [
          {value: 'PAID_FROM_AMOUNT', name: 'Xərc məbləğdən tutulur'},
          {value: 'PAID_BY_NDC', name: 'Xərci MDM ödəyir'},
          {value: 'PAID_BY_CLIENT', name: 'Xərci müştəri ödəyir'}
        ]


        delete $scope.data.memberAccount

        $scope.prepareData = function (data) {

          if (data.account) {
            if (data.account.identificatorFinishDate) delete data.account.identificatorFinishDate
            if (data.account.creationDate) delete data.account.creationDate
          }

          if (data.withdrawalDate) {
            data.withdrawalDate = helperFunctionsService.generateDateTime(data.withdrawalDateObj);
            delete data.withdrawalDateObj

          }
          return data
        }


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.formValid()) {
                //
                var data = $scope.prepareData(angular.copy($scope.data));

                data.items.forEach(function(obj, key){
                  if(obj.withdrawalDate)  obj.withdrawalDate = helperFunctionsService.generateDateTime(obj.withdrawalDateObj)
                  delete data.items[key].withdrawalDateObj

                  if(obj.azipsFeePayer){
                    obj.azipsFeePayer = {
                      accountId: obj.azipsFeePayer.accountId
                    }
                  }

                })

                $scope.config.completeTask(data);
                //
                // } else {
                //   console.log($scope)
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };

        $scope.$watch('data.currency', function (newVal) {
          if (newVal) {
            $scope.getWithdrawalData()
          }
        });

        $scope.$watch('data.account', function (newVal) {
          if (newVal) {
            $scope.getWithdrawalData()
          }
        });

        $scope.setAccount = function () {
          if ($scope.data.isHouseAccount === false) {
            $scope.data.account = null;
          } else {
            $scope.data.account = $scope.memberAccount;
          }
        }

        $scope.addItem = function () {
          if (!$scope.data.items) $scope.data.items = []
          $scope.data.items.push({
            isInternalAccount: false
            // payeeClientAccountNumber: '',
            // payeeClientName: '',
            // payeeClientTin: '',
            // payeeClientCode: '',
            // payeeBankTin: '',
            // payeeBankCode: '',
            // payeeBankBIC: '',
            // payeeBankCorrespondentAccount: '',
          })
        }

        $scope.removeItem = function (index) {

          $scope.data.items.splice(index, 1)
        }

         if($scope.data.items.length == 0){
             $scope.addItem()
           }


        $scope.findUnknownPayment = function () {

          personFindService.findUnknownPayment().then(function (data) {
            $scope.data.paymentID = data.ID;
            $scope.data.amount = data.paidAmount
          })
        };

        $scope.findInternalAccount = function (obj) {
          personFindService.findClearingMember().then(function (response) {
            console.log(response)
            obj.internalMemberAccount = response
          })
        }

        $scope.searchClearingMembersAndIssuers = function (obj) {
          personFindService.searchClearingMembersAndIssuers().then(function (data) {
            obj.azipsFeePayer = data
          })
        };


// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
