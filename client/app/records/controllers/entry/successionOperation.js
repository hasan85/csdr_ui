'use strict';

angular.module('cmisApp')
  .controller('SuccessionOperationController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState',
    'task', 'referenceDataService', 'Loader', 'dataValidationService', 'SweetAlert', 'gettextCatalog', 'recordsService',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState,
              task, referenceDataService, Loader, dataValidationService, SweetAlert, gettextCatalog, recordsService) {

      $scope.metaData = {
        maxLength: 100
      };
      Loader.show(true);

      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        }
        else {
          formData.valueDate = formData.registrationDate;
        }

        for (var i = 0; i < formData.subjects.length; i++) {
          var selectedShares = [];
          for (var key in formData.subjects[i].shares) {
            if (formData.subjects[i].shares.hasOwnProperty(key) && formData.subjects[i].shares[key].quantity > 0) {
              selectedShares.push({
                ID: parseInt(key),
                quantity: formData.subjects[i].shares[key].quantity,
                instrument: formData.subjects[i].shares[key].instrument
              });
            }
          }
          formData.subjects[i].shares = selectedShares;
        }

        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        return formData;
      };

      var subjectTemplate = {
        successor: {
          id: null,
          name: null,
          accountNumber: null
        },
        shares: {},
        validation: {
          isValid: false,
          message: ''
        }
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.holder === null || draft.holder === undefined) {
          draft.holder = {};
        }
        if (draft.subjects === null || draft.subjects === undefined) {
          draft.subjects = [angular.copy(subjectTemplate)];
        }
        return draft;
      };
      $scope.succession = initializeFormData(angular.fromJson(task.draft));

      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data
        };
        referenceDataService.normalizeReturnedRecordTask($scope.succession, {
          registrationAuthorityClasses: data
        });
        if ($scope.succession.rawBalanceData) {
          $scope.config.balanceGrid.options.dataSource = {
            data: $scope.succession.rawBalanceData ? $scope.succession.rawBalanceData : [],
            total: $scope.succession.rawBalanceData ? $scope.succession.rawBalanceData.length : 0,
            pageSize: 20,
            schema: {
              schema: {
                model: {
                  fields: {
                    selectedQuantity: {
                      type: "number"
                    }
                  }
                }
              }
            },
            group: {field: "instrument.instrumentName"}
          };
        }

        if ($scope.succession.subjects) {
          $scope.succession.subjects.forEach(function (subject) {

            if (subject.shares) {
              var buf = {};
              if (angular.isArray(subject.shares)) {
              subject.shares.forEach(function (share) {
                buf[share.ID] = share;
              });
              }
              subject.shares = buf;
            }
          });
        }
        Loader.show(false);
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "successionOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formBuf = prepareFormData(angular.copy($scope.succession));
                console.log('Sent Data', formBuf);
                Loader.show(true);
                dataValidationService.validateSuccessionOperation(formBuf).then(function (data) {

                  console.log('Succession Validation', data);
                  if (data) {
                    if (data.success == "true") {

                      if (data.data == "true") {

                        SweetAlert.swal({
                            title: gettextCatalog.getString('Do you want to complete this task?'),
                            text: gettextCatalog.getString('Not all shares have been transferred to successors. Continue?'),
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: gettextCatalog.getString('Yes'),
                            cancelButtonText: gettextCatalog.getString('Cancel'),
                            closeOnConfirm: true,
                            closeOnCancel: true
                          },
                          function (isConfirm) {
                            if (isConfirm) {
                              $scope.config.completeTask(formBuf);
                            }
                          });

                      } else {
                        $scope.config.completeTask(formBuf);
                      }

                    } else {
                      SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                    }
                  }
                  Loader.show(false);
                })


              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        balanceGrid: {},
        selectedSubjectIndexForBalanceSelection: -1
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.succession);
      });

      // Search holder
      $scope.findHolder = function () {
        $scope.succession.holder = {};
        $scope.succession.subjects = [angular.copy(subjectTemplate)];
        $scope.config.balanceGrid.options.dataSource = {};

        personFindService.findShareholder({
          isSuspended: false
        }, {
          personType: {
            isVisible: false,
            default: appConstants.personClasses.naturalPerson
          }
        }).then(function (person) {


          var args = {
            accountId: person.accountId,
            quantity: null,
            instrumentId: null,
            type: null
          };

          Loader.show(true);
          dataValidationService.checkBalance(args).then(function (data) {

            if (data) {
              if (data['success'] == 'true') {
                if (data['data'] && data['data'].length > 0) {

                  $scope.succession.rawBalanceData = data.data;
                  $scope.config.balanceGrid.options.dataSource = {
                    data: data.data ? data.data : [],
                    total: data.data ? data.data.length : 0,
                    pageSize: 20,
                    schema: {
                      schema: {
                        model: {
                          fields: {
                            selectedQuantity: {
                              type: "number"
                            }
                          }
                        }
                      }
                    },
                    group: {field: "instrument.instrumentName"}
                  };
                  $scope.succession.holder.name = person.name;
                  $scope.succession.holder.accountId = person.accountId;
                  $scope.succession.holder.id = person.id;
                  $scope.succession.holder.accountNumber = person.accountNumber;
                }

              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

            } else {
              SweetAlert.swal('', gettextCatalog.getString('Internal Server Error'), 'error');
              //subject.validation.errors.freezable = false;
            }
            Loader.show(false);
          });

        });

      };

      // Search successor
      $scope.findSuccessor = function (index) {

        personFindService.findShareholder().then(function (data) {
          $scope.succession.subjects[index].successor.name = data.name;
          $scope.succession.subjects[index].successor.accountId = data.accountId;
          $scope.succession.subjects[index].successor.id = data.id;
          $scope.succession.subjects[index].successor.accountNumber = data.accountNumber;
        });

      };

      // Search instrument
      $scope.findInstrument = function (index) {

        instrumentFindService.findInstrument().then(function (data) {
          $scope.succession.subjects[index].instrument.ISIN = data.isin;
          $scope.succession.subjects[index].instrument.id = data.id;
          $scope.succession.subjects[index].instrument.issuerName = data.issuerName;
          $scope.succession.subjects[index].instrument.instrumentName = data.instrumentName;
        });
      };

      // Add new instrument
      $scope.addNewSubject = function (model) {
        model.push(angular.copy(subjectTemplate));
      };

      // Remove instrument
      $scope.removeSubject = function (index, model) {
        model.splice(index, 1);
      };

      $scope.config.balanceGrid.options = {
        columns: [
          {
            field: "instrument.instrumentName",
            title: gettextCatalog.getString('Instrument'),
            width: "10rem",
            editable: false
          },
          {
            field: "ID",
            title: gettextCatalog.getString("Share ID"),
            width: "6rem",
            editable: false
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "6rem",
            editable: false
          },
          {
            field: "free",
            title: gettextCatalog.getString("Free"),
            width: "5rem",
            editable: false,
            template: function (e) {
              if (e.free == "true") {
                return "<i class='glyphicon glyphicon-ok text-success'></i>";
              } else {
                return "<i class='glyphicon glyphicon-remove text-danger'></i>"
              }
            }
          },
          {
            field: "frozen",
            title: gettextCatalog.getString("Frozen"),
            width: "5rem",
            editable: false,
            template: function (e) {
              if (e.frozen == "true") {
                return "<i class='glyphicon glyphicon-ok text-success'></i>";
              } else {
                return "<i class='glyphicon glyphicon-remove text-danger'></i>"
              }
            }
          },
          {
            field: "repo",
            title: gettextCatalog.getString("Repo"),
            width: "5rem",
            editable: false,
            template: function (e) {
              if (e.repo == "true") {
                return "<i class='glyphicon glyphicon-ok text-success'></i>";
              } else {
                return "<i class='glyphicon glyphicon-remove text-danger'></i>"
              }
            }
          },
          {
            title: gettextCatalog.getString("Pledge"),
            columns: [
              {
                field: "pledged",
                width: "5rem",
                title: gettextCatalog.getString("Pledged"),
                editable: false,
                template: function (e) {
                  if (e.pledged == "true") {
                    return "<i class='glyphicon glyphicon-ok text-success'></i>";
                  } else {
                    return "<i class='glyphicon glyphicon-remove text-danger'></i>"
                  }
                }
              },
              {
                field: "operators",
                title: gettextCatalog.getString("Pledgees"),
                width: "6rem",
                editable: false,
                template: function (e) {
                  var t = "<ul class=\"list-no-style\">";
                  if (e.operators) {
                    for (var i = 0; i < e.operators.length; i++) {
                      t += "<li > " + e.operators[i] + "</li>";
                    }
                  }
                  t += "</ul>";
                  return t;
                }
              }
            ]
          },
          {
            title: gettextCatalog.getString("Blocks"),
            columns: [
              {
                field: "operationBlockQuantity",
                width: "6rem",
                editable: false,
                title: gettextCatalog.getString("Operation")
              },
              {
                field: "orderBlockQuantity",
                width: "6rem",
                editable: false,
                title: gettextCatalog.getString("Order")
              },
              {
                field: "tradeBlockQuantity",
                width: "6rem",
                editable: false,
                title: gettextCatalog.getString("Trade")
              },
              {
                field: "settlementBlockQuantity",
                width: "6rem",
                editable: false,
                title: gettextCatalog.getString("Settlement")
              }
            ]
          }
        ],
        dataBound: function () {
          var rows = this.tbody.children();
          var dataItems = this.dataSource.view();
          for (var i = 0; i < dataItems.length; i++) {
            kendo.bind(rows[i], dataItems[i]);
            this.expandRow(this.tbody.find("tr.k-master-row").first());
          }
        }
      };

      $scope.selectShares = function () {
        var gridData = $scope.config.balanceGrid.dataSource.data();
        if (gridData) {
          for (var i = 0; i < gridData.length; i++) {
            var qty = parseInt(gridData[i].selectedQuantity);
            if (qty > 0) {
              $scope.succession.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].shares.push({
                ID: gridData[i].ID,
                quantity: qty
              });
              $scope.succession.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].balance[i].selectedQuantity = qty;
            }
          }
        }
        if (!$scope.succession.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].validation) {
          $scope.succession.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].validation = {
            errors: {
              freezable: false,
              amountSplit: false
            }
          }
        }
        $scope.succession.subjects[$scope.config.selectedSubjectIndexForBalanceSelection].validation.errors.freezable = false;
        $scope.config.accountStatementWindow.close();
      };

      // Subscribe for grid double click event
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.balanceGrid) {
          $scope.config.balanceGrid = widget;
        }
        if (widget === $scope.config.accountStatementWindow) {
          $scope.config.accountStatementWindo = widget;
        }
      });
    }]);
