'use strict';

angular.module('cmisApp')
  .controller('TitleTransferController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog) {

      var titleTransferOperations = {
        grantOperation: "grantoperation_entry",
        titleTransfer: "titleTransfer_entry"
      };
      $scope.metaData = {};
      Loader.show(true);


      $scope.isGrantOperation = task.key == titleTransferOperations.grantOperation;

      var prepareFormData = function (formData) {

        if (formData.registrationDateObj) {
          formData.registrationDate = helperFunctionsService.generateDateTime(formData.registrationDateObj);
          delete formData.registrationDateObj;
        }
        if (formData.valueDateObj) {
          formData.valueDate = helperFunctionsService.generateDateTime(formData.valueDateObj);
          delete formData.valueDateObj;
        } else {
          formData.valueDate = formData.registrationDate;
        }

        for (var i = 0; i < formData.subjects.length; i++) {
          delete formData.subjects[i]['validation'];
        }
        if (formData.registrationAuthorityClass) {
          formData.registrationAuthorityClass = angular.fromJson(formData.registrationAuthorityClass);
        }
        return formData;
      };

      var instrumentFormPartialTemplate = {
        instrument: {
          instrumentId: '',
          ISIN: '',
          issuerName: '',
        },
        quantity: '',
        validation: {
          isValid: false,
          message: ''
        }
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.owner === null || draft.owner === undefined) {
          draft.owner = {};
        }
        if (draft.receiver === null || draft.receiver === undefined) {
          draft.receiver = {};
        }
        if (draft.subjects === null || draft.subjects === undefined || draft.subjects.length == 0 ) {
          draft.subjects = [
            angular.copy(instrumentFormPartialTemplate)
          ]
        }
        return draft;
      };
      $scope.grant = initializeFormData(angular.fromJson(task.draft));

      referenceDataService.getRegistrationAuthorityClasses().then(function (data) {
        $scope.metaData = {
          registrationAuthorityClasses: data
        };
        referenceDataService.normalizeReturnedRecordTask($scope.grant, {registrationAuthorityClasses: data});
        Loader.show(false);
      });
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "grantOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.form.data = prepareFormData(angular.copy($scope.grant));
                $scope.config.completeTask($scope.config.form.data);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.grant);
      });

      // Search owner
      $scope.findOwner = function () {
        Loader.show(true);
        personFindService.findShareholder({
          searchCriteria: {
            isSuspended: false
          }
        }).then(function (data) {
          $scope.grant.owner.name = data.name;
          $scope.grant.owner.accountId = data.accountId;
          $scope.grant.owner.id = data.id;
          $scope.grant.owner.accountNumber = data.accountNumber;
        });

      };

      // Search receiver
      $scope.findReceiver = function () {
        personFindService.findShareholder({
          searchCriteria: {
            isSuspended: false,
          }
        }).then(function (data) {

          $scope.grant.receiver.name = data.name;
          $scope.grant.receiver.accountId = data.accountId;
          $scope.grant.receiver.id = data.id;
          $scope.grant.receiver.accountNumber = data.accountNumber;
        });
      };

      // Search instrument
      $scope.findInstrument = function (index) {
        instrumentFindService.findInstrument().then(function (data) {
          $scope.grant.subjects[index].instrument.ISIN = data.isin;
          $scope.grant.subjects[index].instrument.id = data.id;
          $scope.grant.subjects[index].instrument.issuerName = data.issuerName;
          $scope.grant.subjects[index].instrument.instrumentName = data.instrumentName;
        });
      };

      // Add new instrument
      $scope.addNewInstrument = function (model) {
        model.push(angular.copy(instrumentFormPartialTemplate));
      };

      // Remove instrument
      $scope.removeInstrument = function (index, model) {
        model.splice(index, 1);
      };

    }]);
