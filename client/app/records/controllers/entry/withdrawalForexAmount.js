'use strict';

angular.module('cmisApp')
  .controller('WithdrawalForexAmountController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {

        $scope.form = {};
        $scope.data = angular.fromJson(task.draft);


        $scope.sessions = [
          {value: 1},
          {value: 2},
          {value: 3}
        ];


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {


                if($scope.form.taskForm.$valid){
                  var data = angular.copy($scope.data);
                  if (data.withdrawalDate) {
                    data.withdrawalDate = helperFunctionsService.generateDateTime(data.withdrawalDateObj);
                    delete data.withdrawalDateObj
                  }

                  $scope.config.completeTask(data);

                }
                else{
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };

        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
