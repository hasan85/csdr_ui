'use strict';

angular.module('cmisApp')
  .controller('ServiceReportController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.data = {};

        var date = new Date();
        var startDate = new Date(date.getFullYear(), date.getMonth(), 1);
        var finishDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        var lastDateWithSlashes = (finishDate.getDate()) + '-' + (finishDate.getMonth() + 1) + '-' + finishDate.getFullYear();
        var firstDateWithSlashes = (startDate.getDate()) + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();

        $scope.data.startDateObj = startDate;
        $scope.data.startDate = firstDateWithSlashes;

        $scope.data.endDateObj = finishDate;
        $scope.data.endDate = lastDateWithSlashes;


        $scope.findData = function () {


          var formData = angular.copy($scope.data);

          formData.startDate = $scope.data.startDateObj;
          formData.endDate = $scope.data.endDateObj;

          delete formData.startDateObj;
          delete formData.endDateObj;

          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/reports/getServiceReport/',
            data: {data: formData}
          }).then(function (response) {

            $scope.records = response.data.data.childReports;

            console.log($scope.records);

            Loader.show(false);
          });

        };


        $scope.getDetail = function (child) {
          child.showDetail = !child.showDetail;

          console.log(child);
          if (!child.details) {
            $http({
              method: 'POST',
              url: '/api/reports/getServiceReportDetail/',
              data: {
                data: {
                  startDate: $scope.data.startDateObj,
                  endDate: $scope.data.endDateObj,
                  operationClass: child.operationClass.code,
                  accountClassCode: child.accountClassCode
                }
              }
            }).then(function (response) {

              child.details = angular.isArray(response.data.data) ? response.data.data: [response.data.data] ;

              Loader.show(false);
            });
          }
        }

        $scope.getDetailDatails = function (child, detail) {
          detail.showDetail = !detail.showDetail;

          if (!detail.details) {
            $http({
              method: 'POST',
              url: '/api/records/getOutgoingRecordsByAccountAndOperationClass/',
              data: {
                data: {
                  start: $scope.data.startDateObj,
                  end: $scope.data.endDateObj,
                  accountID: detail.accountID,
                  operationClassID: child.operationClass.id
                }
              }
            }).then(function (response) {

              detail.details = angular.isArray(response.data.data) ? response.data.data: [response.data.data] ;

              Loader.show(false);
            });
          }
        }


        $scope.printData = function (elementID) {
          $scope.printClicked = true;
          Loader.show(true);

          var css = '<style>table{border-collapse: collapse;font-size: 20px;}table, td, th {border: 1px solid #999;}</style>';

          var divToPrint = document.getElementById(elementID);
          var newWin = window.open("");
          newWin.document.write(css + divToPrint.outerHTML);

          setTimeout(function () {
            newWin.print();
            newWin.close();
            $scope.printClicked = false;
            Loader.show(false);
          }, 100);
        };

        $scope.findData()


      }])
;
