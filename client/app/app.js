'use strict';

var cmisApp = angular.module('cmisApp', [
  'ngCookies',
  'ngAnimate',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'kendo.directives',
  'kendo.window',
  'angular-blocks',
  'gettext',
  'once',
  'ui.bootstrap',
  'oitozero.ngSweetAlert',
  'ui.validate',
  'ngMaterial',
  'ngAnimate',
  'ngCookies',
  'angular-click-outside',
  'angularFileUpload',
  'vAccordion',
  'mc.resizer',
  'matchMedia',
  'ngMessages',
  'pasvaz.bindonce',
  "angularMoment"
]);
cmisApp.constant('appConstants', {
  screenTypes: {
    view: 'view',
    operation: 'operation'
  },
  operationStates: {
    active: 'active',
    unclaimed: 'unclaimed',
    completed: 'completed'
  },
  operationTypes: {
    entry: 'entry',

    approval: 'approval'
  },
  personClasses: {
    naturalPerson: 'NATURAL_PERSON',
    juridicalPerson: 'JURIDICAL_PERSON'
  },
  fieldChangeStatuses: {
    new: "NEW",
    changed: "CHANGED",
    unchanged: "UNCHANGED",
    deleted: "DELETED"
  },
  CFICodeTemplates: {
    equity: 'CFI_E',
    bond: 'CFI_D',
  },
  languages: [
    {
      code: 'AZ',
      nameAz: 'Azərbaycan',
      nameEn: "Azerbaijan"
    },
    {
      code: 'EN',
      nameAz: 'İngilis',
      nameEn: "English"
    }
  ],
  memberRoles: {
    roleBroker: "ROLE_BROKER"
  },
  accountClasses: {
    accountPersonal: "ACCOUNT_PERSONAL",
  },
  roleClasses: {
    roleRepresentative: "ROLE_REPRESENTATIVE",
  },
  fileSizeLimit: 5242880,
  homePageTabs: {
    dashboard: 0,
    workerArea: 1
  },
  dateTimeFormat: ""
});
cmisApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$mdThemingProvider', '$provide', '$httpProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider, $mdThemingProvider, $provide, $httpProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('pink')
      .accentPalette('orange');
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
    $provide.decorator("$exceptionHandler", ['$delegate', 'Loader', function ($delegate, Loader) {
      return function (exception, cause) {
        //$delegate(exception, cause);

        //if(exception.message.indexOf('[ngRepeat:dupes]')>-1){
        //  alert("Error! You cannot add duplicate items!");
        //  //console.error(exception);
        //} else{
        alert("Unexpected Error: " + exception.message);
        console.error(exception);
        // }

        Loader.show(false);
      };
    }]);

    $provide.factory('httpInterceptor', ['$q', 'Loader', function ($q, Loader) {
      return {
        response: function (response) {
          if(response && response.data && response.data.resultCode == "9998") {
            console.log("intercept respose",response);
            alert("Sessiya başa çatıb");
            setTimeout(function() {
              window.location.href = '/logout';
            }, 200);
          }
          return response || $q.when(response);
        },
        responseError: function (rejection) {
          Loader.show(false);
          alert('Internal Server Error (UI)' + rejection.statusText);
          return $q.reject(rejection);
        }
      };
    }]);
    $httpProvider.interceptors.push('httpInterceptor');

  }]);

angular.element(document).ready(function () {

  $.ajax('/api/task-services/getUserData/', {type: 'GET'}).done(function (userData) {

    console.log("User data", userData);

    window.userData = userData;
    window.showChangePasswordModal = false;
    window.showChangePasswordWarning = false;

    if(userData.resultCode == "9997" && userData.success === "false") {
      window.showChangePasswordModal = true;
    } else if(userData.resultCode == "9996")  {
      window.showChangePasswordWarning = true;
    } else if (userData.success === "false") {
      window.location.href = '/logout';
      throw new Error("login failed");
    }

    cmisApp.run(['$rootScope', 'gettextCatalog', 'languageService', 'userService', 'configurationService', 'Loader', 'appConstants', '$cookies', 'SweetAlert', 'amMoment',
      function ($rootScope, gettextCatalog, languageService, userService, configurationService, Loader, appConstants, $cookies, SweetAlert, amMoment) {
        amMoment.changeLocale("az");
        kendo.cultures["az-AZ"] = {
          //<language code>-<country/region code>
          name: "az-AZ",
          numberFormat: {
            pattern: ["-n"],
            decimals: 2,
            ",": " ",
            ".": ".",
            groupSize: [3],
            percent: {
              pattern: ["-n %", "n %"],
              decimals: 2,
              ",": ",",
              ".": ".",
              groupSize: [3],
              //percent symbol
              symbol: "%"
            },
            currency: {
              pattern: ["($n)", "$n"],
              decimals: 2,
              ",": ",",
              ".": ".",
              groupSize: [3],
              symbol: "$"
            }
          },
          calendars: {
            standard: {
              days: {
                names: ["Bazar", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"],
                namesAbbr: ["B.", "B.E.", "\u00c7.A.", "\u00c7.", "C.A.", "C.", "\u015e."],
                namesShort: ["B.", "B.E.", "\u00c7.A.", "\u00c7.", "C.A.", "C.", "\u015e."]
              },
              months: {
                names: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "İyun", "İyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
                namesAbbr: ["Yan", "Fev", "Mar", "Apr", "May", "İyn", "İyl", "Avq", "Sen", "Okt", "Noy", "Dek"]
              },
              AM: ["AM", "am", "AM"],
              PM: ["PM", "pm", "PM"],
              patterns: {
                d: "d-M-yyyy",
                D: "MMMM,dddd  dd, yyyy",
                F: "dddd, MMMM dd, yyyy HH:mm:ss",
                g: "d-M-yyyy HH:mm",
                G: "d-M-yyyy HH:mm:ss",
                m: "dd MMMM",
                M: "dd MMMM",
                s: "dd'-'MM'-'yyyyTHH':'mm':'ss",
                t: "HH:mm",
                T: "HH:mm:ss",
                u: "dd'-'MM'-'yyyy HH':'mm':'ss'Z'",
                y: "MMMM, yyyy",
                Y: "MMMM, yyyy"
              },
              firstDay: 1
            }
          }

        };
        kendo.culture("az-AZ");
        $rootScope.openedWindowsCount = 0;
        $rootScope.selectedHomePageTab = 0;

        var today = new Date();
        var yesterday = new Date(today);
        yesterday.setDate(today.getDate() - 1);
        var tomorrow = new Date(today);
        tomorrow.setHours(0);
        tomorrow.setMinutes(0);
        tomorrow.setDate(today.getDate() + 1);

        $rootScope.dateConstants = {
          today: today,
          yesterday: yesterday,
          tomorrow: tomorrow
        };
        $rootScope.customPinnedWindows = [];
        $rootScope.activeWindows = {};
        $rootScope.widgets = {};
        $rootScope.createdWindowIndexes = 0;


        gettextCatalog.setCurrentLanguage($cookies.get('locale') ? $cookies.get('locale') : 'az');
        gettextCatalog.debug = false;
        $rootScope.language = gettextCatalog.getCurrentLanguage();

        if ($rootScope.language == "az") {

          angular.module("ngLocale", [], ["$provide", function ($provide) {
            var PLURAL_CATEGORY = {ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other"};
            $provide.value("$locale", {
              "DATETIME_FORMATS": {
                "AMPMS": [
                  "AM",
                  "PM"
                ],
                "DAY": [
                  "bazar",
                  "bazar ert\u0259si",
                  "\u00e7\u0259r\u015f\u0259nb\u0259 ax\u015fam\u0131",
                  "\u00e7\u0259r\u015f\u0259nb\u0259",
                  "c\u00fcm\u0259 ax\u015fam\u0131",
                  "c\u00fcm\u0259",
                  "\u015f\u0259nb\u0259"
                ],
                "MONTH": [
                  "yanvar",
                  "fevral",
                  "mart",
                  "aprel",
                  "may",
                  "iyun",
                  "iyul",
                  "avqust",
                  "sentyabr",
                  "oktyabr",
                  "noyabr",
                  "dekabr"
                ],
                "SHORTDAY": [
                  "B.",
                  "B.E.",
                  "\u00c7.A.",
                  "\u00c7.",
                  "C.A.",
                  "C.",
                  "\u015e."
                ],
                "SHORTMONTH": [
                  "yan",
                  "fev",
                  "mar",
                  "apr",
                  "may",
                  "iyn",
                  "iyl",
                  "avq",
                  "sen",
                  "okt",
                  "noy",
                  "dek"
                ],
                "fullDate": "d MMMM y, EEEE",
                "longDate": "d MMMM y",
                "medium": "d MMM y HH:mm:ss",
                "mediumDate": "d MMM y",
                "mediumTime": "HH:mm:ss",
                "short": "dd/MM/yy HH:mm",
                "shortDate": "dd/MM/yy",
                "shortTime": "HH:mm"
              },
              "NUMBER_FORMATS": {
                "CURRENCY_SYM": "\u20ac",
                "DECIMAL_SEP": ",",
                "GROUP_SEP": ".",
                "PATTERNS": [
                  {
                    "gSize": 3,
                    "lgSize": 3,
                    "maxFrac": 3,
                    "minFrac": 0,
                    "minInt": 1,
                    "negPre": "-",
                    "negSuf": "",
                    "posPre": "",
                    "posSuf": ""
                  },
                  {
                    "gSize": 3,
                    "lgSize": 3,
                    "maxFrac": 2,
                    "minFrac": 2,
                    "minInt": 1,
                    "negPre": "\u00a4\u00a0-",
                    "negSuf": "",
                    "posPre": "\u00a4\u00a0",
                    "posSuf": ""
                  }
                ]
              },
              "id": "az-latn",
              "pluralCat": function (n, opt_precision) {
                if (n == 1) {
                  return PLURAL_CATEGORY.ONE;
                }
                return PLURAL_CATEGORY.OTHER;
              }
            });
          }]);
        }
        $rootScope.ln = "_" + $rootScope.language;

        $rootScope.lnC = languageService.getCapitalizedLanguage($rootScope.language);

        Loader.show(true);

        if (userData.success == false) {
          // SweetAlert.swal('', 'User not authenticated', 'error');
          // window.location.replace('/logout');
        }
        else {
          $rootScope.user = userData;
          configurationService.getUserInfo(userData).then(function (data) {
            $rootScope.user = data;

            $rootScope.flatOperatations = [];
            if($rootScope.user.operations){
              for (var i = 0; i < $rootScope.user.operations.length; i++) {
                if ($rootScope.user.operations[i].operations && $rootScope.user.operations[i].key != "RECENT_OPERATIONS" && $rootScope.user.operations[i].key != "TOP_OPERATIONS" ) {
                  for (var j = 0; j < $rootScope.user.operations[i].operations.length; j++) {
                    $rootScope.flatOperatations.push($rootScope.user.operations[i].operations[j]);
                  }
                }
              }
              for (var i = 0; i < $rootScope.user.views.length; i++) {
                if ($rootScope.user.views[i].operations) {
                  for (var j = 0; j < $rootScope.user.views[i].operations.length; j++) {
                    $rootScope.flatOperatations.push($rootScope.user.views[i].operations[j]);
                  }
                }
              }
            }


            $rootScope.getFilteredOperations = function (searchText) {
              searchText = String(searchText).toLocaleLowerCase();
              if (!isNaN(+searchText)) {
                return $rootScope.flatOperatations
                  .filter(function (operation) {
                    return new RegExp("^" + searchText, "gi").test(operation.screenID);
                  });
              }
              return $rootScope.flatOperatations.filter(function(operation) {
                return operation.nameAz.toLocaleLowerCase().indexOf(searchText) > -1 ||
                  operation.nameEn.toLocaleLowerCase().indexOf(searchText) > -1 ||
                  operation.key.toLocaleLowerCase().indexOf(searchText) > -1 ||
                  String(operation.screenID).indexOf(searchText) > -1
              });
            };

            Loader.show(false);
          });

          $rootScope.onBeforeUnloadHandler = function() {
            return gettextCatalog.getString('Are you sure to leave this window? Your unsaved data will be lost.');
          };
          window.onbeforeunload = $rootScope.onBeforeUnloadHandler;

        }


      }]);
    angular.bootstrap(document, ["cmisApp"]);
  });
});

