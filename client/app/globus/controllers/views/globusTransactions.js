/**
 * Created by maykinayki on 6/14/16.
 */

'use strict';

angular.module('cmisApp').controller('ViewGlobusTransactionsController', ['$scope', 'Loader', 'gettextCatalog', '$windowInstance', 'id', 'SweetAlert', '$http', "helperFunctionsService", function ($scope, Loader, gettextCatalog, $windowInstance, id, SweetAlert, $http, helperFunctionsService) {

    $scope.metaData = {};
    Loader.show(true);

    var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        result.success = true;
        return result;
    };
    var formDataTemplate = {
        minusAccountNumber: "",
        minusAccountName: "",
        plusAccountNumber: "",
        plusAccountName: "",
        ISIN: "",
        issuerName: "",
        orderDateStart: "",
        orderDateFinish: "",
        transactionType: ""
    };
    $scope.globusTransactions = {
        formName: 'globusTransactionsFindForm',
        resetForm: function() {
            this.form = angular.copy(formDataTemplate);
        },
        form: angular.copy(formDataTemplate),
        data: {},
        searchResult: {
            data: null,
            isEmpty: false
        }
    };
    $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
        }
    };

    var paramsLocal = {
        config: {
            issuer: {
                isVisible: true
            },
            ISIN: {
                isVisible: true
            }
        },
        params: {
            issuerId: null,
            ISIN: null
        },
        showSearchCriteriaBlock: false,
        searchUrl: 'getGlobusTransactions/',
        searchOnInit: false,
    };
    $scope.params = paramsLocal;
    $scope.params.config.splitterPanesConfig = [
        {
            size: '30%',
            collapsible: true,
            collapsed: true
        },
        {
            size: '70%',
            collapsible: false
        }
    ];

    var getGlobusTransactions = function (e) {
        if ($scope.globusTransactions.searchResultGrid !== undefined) {
            $scope.globusTransactions.searchResultGrid.refresh();
        }

        $scope.globusTransactions.searchResult.isEmpty = false;

        var requestData = angular.extend(angular.copy($scope.globusTransactions.form), {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
        });

        requestData.orderDateStart = $scope.globusTransactions.form.orderDateStart ? $scope.globusTransactions.form.orderDateStartObj : null;
        requestData.orderDateFinish = $scope.globusTransactions.form.orderDateFinish ? $scope.globusTransactions.form.orderDateFinishObj : null;

        Loader.show(true);
        $http({method: 'POST', url: "/api/common/" + $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {
                if (data) {
                    $scope.globusTransactions.searchResult.data = data;
                } else {
                    $scope.globusTransactions.searchResult.isEmpty = true;
                }
                if (data['success'] === "true") {
                    data.data = helperFunctionsService.convertObjectToArray(data.data);

                    console.log(data.data);
                    e.success({Data: data.data ? data.data : [], Total: data.total});
                } else {
                    SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }
                Loader.show(false);
            });
    };

    $scope.findData = function () {
        var formValidationResult = validateForm(angular.copy($scope.globusTransactions.form));
        if (formValidationResult.success) {
            if ($scope.globusTransactions.searchResultGrid) {
                $scope.globusTransactions.searchResultGrid.dataSource.page(1);
            }
        }
        else {
            SweetAlert.swal("", formValidationResult.message, "error");
        }
    };
    if ($scope.params.searchOnInit) {
        getGlobusTransactions();
    }

    $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
        } else {
            splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
    };

    $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
        });
    };

    $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
        });
    };


    $scope.globusTransactions.mainGridOptions = {
        excel: {
            allPages: true
        },
        dataSource: {
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    fields: {
                        orderDate: {type: "date"},
                        changeTime: {type: "date"}
                    }
                }
            },
            transport: {
                read: function (e) {
                    getGlobusTransactions(e);
                }
            },

            serverPaging: true,
            serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
            {
                field: "statusName",
                title: gettextCatalog.getString("Status"),
                width: "10rem"
            },
            {
                field: "minusAccount.name",
                title: gettextCatalog.getString("Minus Account Name"),
                width: "10rem"
            },
            {
                field: "minusAccount.accountNumber",
                title: gettextCatalog.getString("Minus Account Number"),
                width: "10rem"
            },
            {
                field: "plusAccount.name",
                title: gettextCatalog.getString("Plus Account Name"),
                width: "10rem"
            },
            {
                field: "plusAccount.accountNumber",
                title: gettextCatalog.getString("Plus Account Number"),
                width: "10rem"
            },
            {
                field: "minusSubDivName",
                title: gettextCatalog.getString("Minus Subdivision Name"),
                width: "10rem"
            },
            {
                field: "minusDivName",
                title: gettextCatalog.getString("Minus Division Name"),
                width: "10rem"
            },
            {
                field: "plusSubDivName",
                title: gettextCatalog.getString("Plus Subdivision Name"),
                width: "10rem"
            },
            {
                field: "plusDivName",
                title: gettextCatalog.getString("Plus Division Name"),
                width: "10rem"
            },
            {
                field: "instrument.ISIN",
                title: gettextCatalog.getString("ISIN"),
                width: "10rem"
            },
            {
                field: "instrument.issuerName",
                title: gettextCatalog.getString("Issuer"),
                width: "10rem"
            },
            {
                field: "quantity",
                title: gettextCatalog.getString("Quantity"),
                width: "10rem"
            },
            {
                field: "cost",
                title: gettextCatalog.getString("Cost"),
                width: "10rem"
            },
            {
                field: "orderDate",
                title: gettextCatalog.getString("Order Date"),
                width: "10rem",
                format: "{0:dd-MMMM-yyyy HH:mm:ss}"
            },
            {
                field: "comment",
                title: gettextCatalog.getString("Comment"),
                width: "10rem"
            },
            {
                field: "changeTime",
                title: gettextCatalog.getString("Change Time"),
                width: "10rem",
                format: "{0:dd-MMMM-yyyy HH:mm:ss}"
            },
            {
                field: "statusName",
                title: gettextCatalog.getString("Status"),
                width: "10rem"
            },
            {
                field: "transactionTypeName",
                title: gettextCatalog.getString("Transaction Type"),
                width: "10rem"
            },
            {
                field: "reasonName",
                title: gettextCatalog.getString("Reason"),
                width: "10rem"
            },
            {
                field: "currencyName",
                title: gettextCatalog.getString("Currency"),
                width: "10rem"
            },
            {
                field: "username",
                title: gettextCatalog.getString("Username"),
                width: "10rem"
            }
        ]
    };

    $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.globusTransactions.searchResultGrid) {
            $scope.globusTransactions.searchResultGrid = widget;
        }
    });
}]);
