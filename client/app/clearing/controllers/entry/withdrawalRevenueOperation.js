'use strict';

angular.module('cmisApp')
  .controller('WithdrawalRevenueOperationController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.metaData = {}
        $scope.data = angular.fromJson(task.draft);

        console.log($scope.data);

        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });


        referenceDataService.getAZIPSSessionInfo().then(function (data) {

          $scope.metaData.sessionInfo = data.data;
          $scope.sessionType = data.data.currentSessionNumber ?  'now': 'next' ;
          $scope.sessionTypeChange($scope.sessionType);
          Loader.show(false);
        });



        $scope.sessionTypeChange = function(newVal){
          if (newVal) {

            $scope.kendoDateOptions = {
              min: new Date()
            };

            if(newVal == 'now'){
              $scope.data.sessionNumber = $scope.metaData.sessionInfo.currentSessionNumber;
              $scope.data.withdrawalDateObj = new Date();
            }
            else if(newVal == 'next'){

              $scope.data.sessionNumber = $scope.metaData.sessionInfo.nextSessionNumber;
              $scope.data.withdrawalDateObj = new Date($scope.metaData.sessionInfo.nextSessionDate)
            }
            else {
              $scope.data.sessionNumber = null;
              $scope.data.withdrawalDate = null
            }
          }
        }

        $scope.prepareData = function (data) {
          //

          if (data.withdrawalDateObj) {
            data.withdrawalDate = helperFunctionsService.generateDateTime(data.withdrawalDateObj);
            delete data.withdrawalDateObj

          }

          return data
        }

        setTimeout(function () {
          $windowInstance.widget.setOptions({width: '80%'})
        }, 200)


        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.formValid()) {
                //
                var data = $scope.prepareData(angular.copy($scope.data));
                console.log(data);
                $scope.config.completeTask(data);
                //
                // } else {
                //   console.log($scope)
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };

        $scope.$watch('data.currency', function (newVal) {
          if (newVal) {
            if ($scope.data.isHouseAccount && !$scope.data.isMemberBankAccount) {
              $scope.getMemberClientBankAccounts()
            } else {
              $scope.getWithdrawalData()
            }
          }
        });

        $scope.$watch('data.withdrawalDate', function (newVal) {
          if (newVal) {
            console.log(newVal);
          }
        });



// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
