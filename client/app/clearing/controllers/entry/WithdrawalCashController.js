'use strict';

angular.module('cmisApp')
  .controller('WithdrawalCashController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, dataSearchService, gettextCatalog, SweetAlert) {


      $scope.metaData = {
        currencies: []
      };

      Loader.show(true);

      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;
        Loader.show(false);
      });

      var validateForm = function (viewModel) {
        for (var i = 0; i < viewModel.positions.length; i++) {
          if (viewModel.positions[i].withdrawalAmount > 0) {
            return true;
          }
        }
        SweetAlert.swal("", gettextCatalog.getString('You have to make at least one withdrawal!'), 'error');
        return false;
      };

      $scope.enterWithdrawalOrderData =angular.fromJson(task.draft);
      console.log("Task draft: ",$scope.enterWithdrawalOrderData);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "enterWithdrawalOrderDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              var taskData = angular.copy($scope.enterWithdrawalOrderData);
              $scope.taskData = taskData;
              taskData.positions.map(function(item) {
                item.selectedBank = angular.fromJson(item.selectedBank);
              });
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid && validateForm(taskData)) {
                $scope.config.completeTask(taskData);
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      $scope.checkAccount = function (accountNumber) {

        var errorMessage = null;
        var validAccountClassTypes = ['ACCOUNT_TRADING_MEMBER', 'ACCOUNT_CLEARING_MEMBER', 'ACCOUNT_PRESCRIPTION', 'ACCOUNT_CLIENT_SHAREHOLDER'];
        Loader.show(true);
        dataSearchService.getAccountInfo(accountNumber).then(function (data) {

          if (data && data.success == "false") {
            errorMessage = data.message;
            $scope.enterWithdrawalOrderData.accountNumber = null;
          } else if (data.success != "true") {
            errorMessage = gettextCatalog.getString('Internal Server Error');
          } else {

            if (validAccountClassTypes.indexOf(data.data.code) < 0) {
              errorMessage = gettextCatalog.getString('Invalid Account Type')
            }

          }

          if (errorMessage) {
            SweetAlert.swal('', errorMessage, 'error')
          }

          Loader.show(false);
        })
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.enterWithdrawalOrderData));
      });

    }]);
