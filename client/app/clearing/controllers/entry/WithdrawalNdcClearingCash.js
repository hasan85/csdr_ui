'use strict';

angular.module('cmisApp')
  .controller('WithdrawalNdcClearingCashController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
      'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog','$rootScope',
      function ($scope, $windowInstance, id, appConstants, operationService,
                personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $rootScope) {

        var initializeFormData = function (draft) {

          if (!draft) {
            draft = {}
          }
          if (draft.isConfirmed === null || draft.isConfirmed === undefined) {
            draft.isConfirmed = false;
          }


          return draft;
        };

        $scope.metaData = {};

        var prepareFormData = function (formData) {

          return formData;
        };

        $scope.data = initializeFormData(angular.fromJson(task.draft));

        $scope.taskData = $scope.data;

        $scope.bankAccounts = angular.fromJson(task.draft).bankAccounts

        $scope.bankAccounts.forEach(function(item){
            item.textField = item['bankName' + $rootScope.lnC] + " " + item.iban
        })

        $scope.bankAccountsOption = {
          dataSource: $scope.bankAccounts,
          dataTextField: "textField",
          dataValueField: "id",
          optionLabel: {
            textField: "",
            id: null
          }
        };



        console.log( $scope.taskData)

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "otcTradeOperationForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                console.log($scope.taskData)
                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  var buf = prepareFormData(angular.copy($scope.taskData));


                  $scope.config.completeTask(buf);

                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };
        // Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask($scope.taskData);
        });


      }]);
