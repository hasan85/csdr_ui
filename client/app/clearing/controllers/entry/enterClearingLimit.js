'use strict';

angular.module('cmisApp')
  .controller('EnterClearingLimitController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', '$filter', 'gettextCatalog', 'SweetAlert',
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, $filter, gettextCatalog, SweetAlert) {

      $scope.metaData = {
        currencies: []
      };
      $scope.clearingLimitData = {};

      var payload = angular.fromJson(task.draft).memberLimits;

      Loader.show(true);

      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;

        for (var i = 0; i < payload.length; i++) {
          if (payload[i].isSelected) {
            $scope.config.selectedMemberLimitIndex = i.toString();
          }

          if (payload[i].limits && payload[i].limits.length > 0) {

            for (var j = 0; j < payload[i].limits.length; j++) {
              if (payload[i].limits[j].currency) {
                payload[i].limits[j].currency = $filter('json')(helperFunctionsService.findByIdInsideArray(
                    $scope.metaData.currencies,
                    payload[i].limits[j].currency['id'])
                );
              }
            }
          }
          else {
            payload[i].limits = [];
            payload[i].limits.push(angular.copy(clearingLimitTemplate));
          }
        }

        $scope.clearingLimitData = angular.copy(payload);

        Loader.show(false);
      });

      var clearingLimitTemplate = {
        oldValue: null,
        newValue: null,
        currency: ""
      };

      var prepareFormData = function (viewModel) {
        for (var i = 0; i < viewModel.length; i++) {
          if (viewModel[i].limits && viewModel[i].limits.length > 0) {
            for (var j = 0; j < viewModel[i].limits.length; j++) {
              if (viewModel[i].limits[j].currency != '') {
                viewModel[i].limits[j].currency = angular.fromJson(viewModel[i].limits[j].currency);
              } else {
                viewModel[i].limits[j].currency = {};
              }
            }
          }
        }
        if ($scope.config.selectedMemberLimitIndex) {
          viewModel[$scope.config.selectedMemberLimitIndex].isSelected = true;
        }

        return {memberLimits: viewModel};
      };

      var validateForm = function () {
        var selectedCurrencyIds = [];
        for (var i = 0; i < $scope.clearingLimitData[$scope.config.selectedMemberLimitIndex].limits.length; i++) {
          if ($scope.clearingLimitData[$scope.config.selectedMemberLimitIndex].limits[i].currency != '') {
            var currency = angular.fromJson($scope.clearingLimitData[$scope.config.selectedMemberLimitIndex].limits[i].currency);

            if (selectedCurrencyIds.indexOf(currency.id) > -1) {

              SweetAlert.swal("", gettextCatalog.getString('You cannot add two clearing limit for the same currency'), 'error');
              return false;
            } else {
              selectedCurrencyIds.push(currency.id);
            }
          }
        }

        return true;
      };

      $scope.changeMember = function () {
        $scope.clearingLimitData = angular.copy(payload);
      };

      $scope.addNewClearingLimit = function () {
        $scope.clearingLimitData[$scope.config.selectedMemberLimitIndex].limits.push(angular.copy(clearingLimitTemplate));
      };

      $scope.removeClearingLimit = function (model, index) {
        model.splice(index, 1)
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "clearingLimitDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              console.log($scope.config.form.name);
              if ($scope.config.form.name.$valid && validateForm()) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.clearingLimitData)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        selectedMemberLimitIndex: ""
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);

      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(prepareFormData(angular.copy($scope.clearingLimitData)));
      });

    }]);
