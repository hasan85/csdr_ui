'use strict';

angular.module('cmisApp')
    .controller('ConfirmMatchedTradeController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
      'Loader', 'helperFunctionsService', 'operationState', 'task',
      'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', 'recordsService', "$filter",
      function ($scope, $windowInstance, id, appConstants, operationService,
                Loader, helperFunctionsService, operationState, task,
                referenceDataService, dataSearchService, gettextCatalog, SweetAlert, recordsService, $filter) {


        var initializeFormData = function (formData) {
          if (!formData) {
            formData = {
              isConfirmed: null
            }
          } else {
            if (formData.isConfirmed === undefined || formData.isConfirmed === null) {
              formData.isConfirmed = null;
            }
          }
          return formData;
        };

        $scope.matchedTradeData = initializeFormData(angular.fromJson(task.draft));

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "matchedTradeDataForm",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.form.name.$submitted = true;
                if ($scope.config.form.name.$valid) {
                  $scope.config.completeTask(angular.copy($scope.matchedTradeData));
                } else {
                  SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                }
              }
            }
          }
        };


        $scope.findMatchedTrade = function () {

          recordsService.findMatchedTrade().then(function (data) {

            if (data) {

              $scope.matchedTradeData.ID = data.ID;
              $scope.matchedTradeData.buySideClient = data.buySideClient;
              $scope.matchedTradeData.buySideMember = data.buySideMember;
              $scope.matchedTradeData.instrument = data.instrument;
              $scope.matchedTradeData.price = data.price;
              $scope.matchedTradeData.quantity = data.quantity;
              $scope.matchedTradeData.amount = data.amount;
              $scope.matchedTradeData.currency = data.currency;
              $scope.matchedTradeData.sellSideMember = data.sellSideMember;
              $scope.matchedTradeData.sellSideClient = data.sellSideClient;

            }
          });

        };

        // Check if form is dirty
        $scope.$on('closeTask', function () {
          //var taskPayload = task && task.draft ? angular.toJson(prepareFormData(angular.fromJson(task.draft))) : null;
          //var currentTask = angular.toJson(prepareFormData($scope.matchedTradeData));
          $scope.config.showTaskSavePrompt(false);
        });

        // Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.matchedTradeData));
        });


      }]);
