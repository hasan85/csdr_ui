'use strict';

angular.module('cmisApp')
  .controller('EnterMemberAdvanceController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', '$filter',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $filter) {

      $scope.metaData = {};
      Loader.show(false);

      var prepareFormData = function (formData) {
        if (formData.paymentDateObj) {
          formData.paymentDate = helperFunctionsService.generateDateTime(formData.paymentDateObj);
          delete formData.paymentDateObj;
        }

        formData.correspondentAccount = angular.fromJson(formData.correspondentAccount);
        return formData;
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {}
        }
        if (draft.member === null || draft.member === undefined) {
          draft.member = {};
        }
        if (draft.paymentDate && !draft.paymentDateObj) {
          draft.paymentDateObj = helperFunctionsService.parseDate(draft.paymentDate);
          draft.paymentDate = helperFunctionsService.generateDate(draft.paymentDateObj);
        }

        return draft;
      };

      $scope.enterMemberAdvance = initializeFormData(angular.fromJson(task.draft));

      if ($scope.enterMemberAdvance.correspondentAccount && $scope.enterMemberAdvance.correspondentAccount.accountId) {
        $scope.enterMemberAdvance.correspondentAccount = $filter('json')(
          helperFunctionsService.findByIdInsideArrayByProperty($scope.enterMemberAdvance.correspondentAccounts,
            $scope.enterMemberAdvance.correspondentAccount['accountId'], 'accountId'));
      }
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "enterMemberAdvanceOperationForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var buf = prepareFormData(angular.copy($scope.enterMemberAdvance));
                $scope.config.completeTask(buf);
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.enterMemberAdvance);
      });


      // Search seller broker
      $scope.findMember = function () {

        personFindService.findTradingMember().then(function (data) {

          $scope.enterMemberAdvance.member.name = data.name;
          $scope.enterMemberAdvance.member.accountId = data.accountId;
          $scope.enterMemberAdvance.member.id = data.id;
          $scope.enterMemberAdvance.member.accountNumber = data.accountNumber;
        });

      };


    }]);
