'use strict';

angular.module('cmisApp').controller('SubscriptionPaymentManualMatchingController', ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService','personFindService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', '$rootScope', function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService,personFindService, dataSearchService, gettextCatalog, SweetAlert, $rootScope) {


    var prepareFormData = function (viewModel) {

        viewModel.subscriptions.map(function(item) {
            item.regDate = helperFunctionsService.generateDateTime(item.regDate);
        });

        viewModel.temenosPayments.map(function(item) {
            item.paymentDate = helperFunctionsService.generateDateTime(item.paymentDate);
        });

        return viewModel;
    };

    $scope.earlySettlementData = angular.fromJson(task.draft);

    $scope.mainGridOptions = {
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        filterable: {
            mode: "row",
            extra: false
        },
        dataBound: function() {
            this.expandRow(this.tbody.find("tr.k-master-row"));
        },
        columns: [
            {
                field: "searchPayment",
                title: gettextCatalog.getString("Search Payment"),
                width: "8rem",
                template: function (e) {
                    var t = ' <md-button type="button" class="md-fab md-micro"aria-label="search button"ng-click="findPayment(dataItem)"> <i class="glyphicon glyphicon-search micro-glyphicon"></i></md-button>';
                    return t;
                },
                filterable: false
            },
            {
                field: "recordNumber",
                title: gettextCatalog.getString("Order Number"),
                width: "10rem",
                filterable: {
                    cell: {
                        showOperators: false,
                        operator: "contains"
                    },
                    ignoreCase: true
                }
            },
            {
                field: "memberName",
                title: gettextCatalog.getString("Member Name"),
                width: "10rem",
                filterable: {
                    cell: {
                        showOperators: false,
                        operator: "contains"
                    },
                    ignoreCase: true
                }
            },
            {
                field: "clientAccountNumber",
                title: gettextCatalog.getString("Account Number"),
                width: "10rem",
                filterable: {
                    cell: {
                        showOperators: false,
                        operator: "contains"
                    },
                    ignoreCase: true
                }
            },
            {
                field: "amount",
                title: gettextCatalog.getString("Amount"),
                width: "9rem",
                filterable: {
                    cell: {
                        showOperators: false
                    },
                    ignoreCase: true
                }
            },
          {
            field: "regDate",
            title: gettextCatalog.getString("Subscription Date"),
            width: "12rem",
            format: "{0:dd-MMMM-yyyy HH:mm:ss}",
            filterable: {
              ui: "datetimepicker" // use Kendo UI DateTimePicker
            }
          }
        ],
        dataSource: {
            transport: {
                read: function (e) {
                    e.success({Data: $scope.earlySettlementData.subscriptions, Total: $scope.earlySettlementData.subscriptions.length});
                }
            },
            schema: {
              data: "Data",
              total: "Total",
              model: {
                fields: {
                  regDate: {type: "date"},
                  amount: {type: "number"}
                }
              }
            },
        }
    };

    $scope.detailGridOptions = function(masterDataItem) {
        return {
            dataSource: {
                transport: {
                    read: function (e) {
                        e.success({Data: masterDataItem.matchedPayments, Total: masterDataItem.matchedPayments.length});
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total"
                }
            },
            columns: [
                {
                    field: "removePayment",
                    title: gettextCatalog.getString("Remove Payment"),
                    width: "8rem",
                    template: function (e) {
                        var t = ' <md-button type="button" class="md-fab md-micro" aria-label="remove button" ng-click="removePayment($event, dataItem)"> <i class="glyphicon glyphicon-remove micro-glyphicon"></i></md-button>';
                        return t;
                    }
                },
                {
                    field: "paymentDate",
                    title: gettextCatalog.getString("paymentDate"),
                    width: '10rem',
                    type: "date",
                    format: "{0:dd-MMMM-yyyy}"
                },
                {
                    field: "destination",
                    title: gettextCatalog.getString("Destination"),
                    width: '8rem'
                },
                {
                    field: "amount",
                    title: gettextCatalog.getString("Amount"),
                    width: '8rem'
                },
                {
                    field: "currencyCode",
                    title: gettextCatalog.getString("Currency"),
                    width: '8rem'
                }
            ]
        };
    };

    //Initialize scope variables [[
    $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
            complete: {
                click: function () {
                    var matchValidation = false;
                    $scope.earlySettlementData.temenosPayments.map(function(item) {
                        if(item.isMatched == true) {
                            matchValidation = true;
                        }
                    });

                    if(!matchValidation) {
                        SweetAlert.swal('', gettextCatalog.getString('You have to match at least a payment'), 'error');
                        return matchValidation;
                    }

                  var data = prepareFormData(angular.copy($scope.earlySettlementData));
                  $scope.config.completeTask(data);

                }
            }
        }
    };

    // Save task as draft
    $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.earlySettlementData);
    });

    $scope.selectedSubscription = null;

    $scope.subscriptionSelected = function (data) {
        var result = $scope.earlySettlementData.subscriptions;
        for (var i = 0; i < result.length; i++) {
            if (result[i].recordID == data.recordID) {
                $scope.selectedSubscription = result[i];
                break;
            }
        }
    };

    $scope.updateGrid = function() {
        $scope.config.grid.dataSource.read();
    };

    $scope.removePayment = function (e, dataItem) {

        var parentItem = dataItem.parent().parent();
        var recordID = parentItem.recordID;

        $scope.earlySettlementData.temenosPayments.map(function(item) {
            if(item.paymentID == dataItem.paymentID) {
                item.isMatched = false;
                $scope.earlySettlementData.subscriptions.map(function(subscription) {
                    if(subscription.recordID == recordID) {
                        subscription.matchedPayments.map(function(matchedPayment, index, matchedPayments) {
                            if(matchedPayment == item) {
                                matchedPayments.splice(index, 1);
                                $(e.currentTarget).closest("tr").remove();
                            }
                        });
                    }
                });
            }
        });
    };

    // Search payment
    $scope.findPayment = function (dataItem) {
        $scope.subscriptionSelected(dataItem);

        personFindService.findPayment($scope.earlySettlementData).then(function (data) {
            $scope.earlySettlementData.temenosPayments.map(function(item) {
                if(item.paymentID == data.paymentID) {
                    item.isMatched = true;
                    $scope.selectedSubscription.matchedPayments.push(item);
                }
            });
            $scope.updateGrid();
        });
    };

  // Check if form is dirty
  $scope.$on('closeTask', function () {
    //var taskPayload = task && task.draft ? angular.toJson(prepareFormData(angular.fromJson(task.draft))) : null;
    //var currentTask = angular.toJson(prepareFormData($scope.matchedTradeData));
    $scope.config.showTaskSavePrompt(false);
  });

}]);
