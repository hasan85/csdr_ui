'use strict';

angular.module('cmisApp').controller('CorrectPaymentEntryController', ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',   'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'FileUploader',   'SweetAlert', 'gettextCatalog', 'Loader', '$http', 'batchEntryOfAllotmentService', '$filter', 'managePersonService', 'referenceDataService', "$kWindow", "$q",  function ($scope, $windowInstance, id, appConstants, operationService, personFindService, instrumentFindService, helperFunctionsService, operationState, task, FileUploader, SweetAlert, gettextCatalog, Loader, $http, batchEntryOfAllotmentService, $filter, managePersonService, referenceDataService, $kWindow, $q) {


  var initializeFormData = function (draft) {
    return draft;
  };

  $scope.correctPaymentData = initializeFormData(angular.fromJson(task.draft));
  $scope.correctPaymentDataBuff = {};

  $scope.data = {
    config: {
      formName: "representativeForm"
    }
  };

  var prepareFormData = function (formData) {
    return formData;
  };

  $scope.findPayment = function() {
    var openPaymentView = function (config, criteriaForm) {
      return $kWindow.open({
        title: gettextCatalog.getString("Ödəniş axtar"),
        actions: ['Close'],
        isNotTile: true,
        width: '95%',
        //appendTo: '#home',
        height: '80%',
        pinned: true,
        modal: true,
        screenType: appConstants.screenTypes.view,
        templateUrl: 'app/records/templates/view-payments.html',
        controller: 'ViewPaymentsController',
        resolve: {
          id: function () {
            return 0;
          },
          configParams: function () {
            return {
              criteriaForm: criteriaForm ? criteriaForm : {},
              searchCriteria: config ? config.searchCriteria : {}
            };
          }
        }
      }).result;
    };

    openPaymentView({
      searchCriteria: {
        direction: "DIRECTION_IN",
        statusCode: "STATUS_ERROR"
      }
    }).then(function(data) {
      if(data) {
        $scope.payment = angular.copy(data);

        $scope.correctPaymentDataBuff.paymentID = data.ID;
        $scope.correctPaymentDataBuff.accountID = data.payeeClientAccountID || null;
        $scope.correctPaymentDataBuff.serviceClassID = data.serviceClass ? data.serviceClass.id : null;

        $scope.hasAccount = !!$scope.correctPaymentDataBuff.accountID;
        $scope.hasServiceClass = !!$scope.correctPaymentDataBuff.serviceClassID;

        if($scope.hasAccount) {
          $scope.account = {
            name: data.payeeClientName,
            accountNumber: data.payeeClientAccountNumber
          };
        } else {
          $scope.account = null;
        }

      }
    });
  };

  $scope.findAccount = function () {
    personFindService.getMemberClientAccounts().then(function (data) {
      $scope.account = data;
      $scope.correctPaymentDataBuff.accountID = data.accountId;
    });
  };


  Loader.show(true);
  $q.all([

    referenceDataService.getServiceClasses().then(function(data) {
      if(data) {
        $scope.serviceClasses = data;
      }
    })

  ]).then(function(data) {
    Loader.show(false);
  })

  //Initialize scope variables [[
  $scope.config = {
    screenId: id,
    taskKey: id,
    task: task,
    form: {
      name: "correctPaymentDataOperationForm",
      data: {}
    },
    operationType: appConstants.operationTypes.entry,
    window: $windowInstance,
    state: operationState,
    buttons: {
      complete: {
        click: function () {

          $scope.correctPaymentData.paymentID = $scope.payment.ID;
          if(!$scope.payment.serviceClass) {
            $scope.correctPaymentData.serviceClassID = $scope.correctPaymentDataBuff.serviceClassID;
          }
          if(!$scope.payment.payeeClientAccountID) {
            $scope.correctPaymentData.accountID = $scope.correctPaymentDataBuff.accountID;
          }

          $scope.correctPaymentData.approvalData = {
            payment: $scope.payment,
            account: $scope.account,
            serviceClass: $scope.serviceClasses.filter(function(item) {return item.id == $scope.correctPaymentDataBuff.serviceClassID})
          }

          $scope.config.form.name.$submitted = true;
          if ($scope.config.form.name.$valid) {
            $scope.config.form.data = prepareFormData(angular.copy($scope.correctPaymentData));
            $scope.config.completeTask($scope.config.form.data);
          } else {
            SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
          }
        }
      }
    }
  };

  // Check if form is dirty
  $scope.$on('closeTask', function () {
    $scope.config.showTaskSavePrompt(false);
  });

  // Save task as draft
  $scope.$on('saveTask', function () {
    $scope.config.saveTask($scope.correctPaymentData);
  });

}]);
