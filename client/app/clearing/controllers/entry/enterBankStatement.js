'use strict';

angular.module('cmisApp')
  .controller('EnterBankStatementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', '$filter',
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, dataSearchService, gettextCatalog, SweetAlert, $filter) {

      $scope.metaData = {
        currencies: []
      };

      Loader.show(true);

      var prepareFormData = function (viewModel) {
        viewModel.currency = angular.fromJson(viewModel.currency);
        viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);
        return viewModel;
      };

      $scope.enterBankStatementData = angular.fromJson(task.draft);

      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;
        if ($scope.enterBankStatementData.currency && $scope.enterBankStatementData.currency.id) {
          $scope.enterBankStatementData.currency = $filter('json')(helperFunctionsService.findByIdInsideArray(data,
            $scope.enterBankStatementData.currency['id']));
        }
        if ($scope.enterBankStatementData.correspondentAccount && $scope.enterBankStatementData.correspondentAccount.accountId) {
          $scope.enterBankStatementData.correspondentAccount = $filter('json')(
            helperFunctionsService.findByIdInsideArrayByProperty($scope.enterBankStatementData.correspondentAccounts,
              $scope.enterBankStatementData.correspondentAccount['accountId'], 'accountId'));
        }
        Loader.show(false);
      });


      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "enterBankStatementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.enterBankStatementData)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      $scope.checkAccount = function (accountNumber) {
        var errorMessage = null;
        var validAccountClassTypes = ['ACCOUNT_TRADING_MEMBER', 'ACCOUNT_CLEARING_MEMBER', 'ACCOUNT_PRESCRIPTION', 'ACCOUNT_CLIENT_SHAREHOLDER'];
        Loader.show(true);
        dataSearchService.getAccountInfo(accountNumber).then(function (data) {
          if (data && data.success == "false") {
            errorMessage = data.message;
            $scope.enterBankStatementData.accountNumber = null;
          } else if (data.success != "true") {
            errorMessage = gettextCatalog.getString('Internal Server Error');
          } else {
            if (validAccountClassTypes.indexOf(data.data.type.code) < 0) {
              errorMessage = gettextCatalog.getString('Invalid Account Type')
            }
          }
          if (errorMessage) {
            SweetAlert.swal('', errorMessage, 'error')
          }
          Loader.show(false);
        })
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.enterBankStatementData));
      });

    }]);
