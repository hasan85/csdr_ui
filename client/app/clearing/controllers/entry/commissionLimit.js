'use strict';

angular.module('cmisApp')
  .controller('CommissionLimitController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http', '$q',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http, $q) {


        $scope.data = angular.fromJson(task.draft);

        console.log($scope.data)
        $scope.data.commissionLimitItems  = angular.isArray($scope.data.commissionLimitItems ) ? $scope.data.commissionLimitItems : [$scope.data.commissionLimitItems ];

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {
                $scope.config.completeTask($scope.data);
              }
            }
          }
        };


        function openForm(fn) {
          $kWindow.open({
            title: '',
            actions: ['Close'],
            isNotTile: true,
            maxWidth: 400,
            height: '400px',
            pinned: true,
            modal: true,
            screenType: appConstants.screenTypes.view,
            templateUrl: 'app/clearing/templates/add-commission-limit-form.html',
            controller: 'AddCommissionLimitController',
            resolve: {
              bla: function () {
                return $scope.dataForm
              },
              submit: function(){
                return fn
              }
            }
          });
        }


        $scope.edit = function (index) {
          $scope.dataForm = $scope.data.commissionLimitItems [index];
          openForm(function (data) {
            $scope.data.commissionLimitItems [index] = data
          })
        };

        $scope.add = function () {
          $scope.dataForm = {}
          openForm(function (data) {
            $scope.data.commissionLimitItems .push(data)
          })
        }


// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
'use strict';

angular.module('cmisApp')
  .controller('AddCommissionLimitController',
    ['$scope', '$windowInstance', 'bla', 'personFindService', 'submit', 'referenceDataService', function ($scope, $windowInstance, data, personFindService, submit, referenceDataService) {


      referenceDataService.getCurrencies().then(function (data) {
        $scope.currencies = data;
      });

      $scope.data = angular.copy(data);
      delete $scope.data.config

      $scope.findMember = function () {
          personFindService.findTradingMember().then(function(data){
            console.log(data)
            $scope.data.account = data
          })
      }


      $scope.submit = function(){
        submit($scope.data)
        $windowInstance.close()
      }


      $scope.close = function(){
        $windowInstance.close()
      }

    }])
