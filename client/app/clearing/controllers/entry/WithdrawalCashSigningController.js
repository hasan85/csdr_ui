'use strict';

angular.module('cmisApp')
  .controller('WithdrawalCashSigningController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', "$http", "$q", "$timeout",
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, dataSearchService, gettextCatalog, SweetAlert, $http, $q, $timeout) {

      $scope.metaData = {
        currencies: []
      };

      var validateForm = function (viewModel) {
        for (var i = 0; i < viewModel.positions.length; i++) {
          if (viewModel.positions[i].withdrawalAmount > 0) {
            return true;
          }
        }
        SweetAlert.swal("", gettextCatalog.getString('You have to make at least one withdrawal!'), 'error');
        return false;
      };

      $scope.enterWithdrawalOrderData =angular.fromJson(task.draft);
      console.log("Task draft: ",$scope.enterWithdrawalOrderData);

      $scope.rejectionConfig = {
        cancelRejectionEnabled: false
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "enterWithdrawalOrderDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              var taskData = angular.copy($scope.enterWithdrawalOrderData);
              $scope.taskData = taskData;
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid && validateForm(taskData)) {
                $scope.config.completeTask(taskData);
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      $scope.signedSuccess = false;
      $scope.sign = function() {
        var signables = $scope.enterWithdrawalOrderData.positions.filter(function(position) {
          return !!position.block4String && position.approved === true;
        });

        var promises = signables.map(function(item) {
          return $http({
            method: "GET",
            url: "http://localhost:8099/TestService/sign?inputData="+item.block4String
          }).then(function(response) {
            if(response && response.data)
              item.msgMacResult = response.data;
          });
        });

        $scope.signedSuccess = false;
        $scope.startSigning = true;
        $q.all(promises).then(function(){
          $scope.signError = null;
          $timeout(function() {
            $scope.signedSuccess = true;
            $scope.startSigning = false;
          }, 1200);
        }).catch(function() {
          SweetAlert.swal('', "Xəta baş verdi", 'error');
          $scope.signedSuccess = false;
          $timeout(function() {
            $scope.startSigning = false;
          }, 1200);
        });

      }

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.enterWithdrawalOrderData));
      });

    }]);
