'use strict';

angular.module('cmisApp')
  .controller('PrintPaymentOrderController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http', '$q',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http, $q) {


        $scope.metaData = {};
        $scope.data = {};

        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });


        $scope.prepareData = function (data) {
          data.amount = parseInt(data.amount)
          return data
        };

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          window: $windowInstance,
          buttons: {
            complete: {
              click: function () {

                // if ($scope.formValid()) {
                //
                var data = $scope.prepareData(angular.copy($scope.data));

                $scope.config.completeTask(data);
                //
                // } else {
                //   console.log($scope)
                //   SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
                // }
              }
            }
          }
        };


        $scope.$watch('data.currencyID', function (newVal) {
            $scope.getMemberClientBankAccounts()
        });


        $scope.setAccount = function () {
          if ($scope.data.isHouseAccount === false) {
            $scope.data.account = null;
            $scope.accountNumber = '';
            $scope.accountName = ''
          } else {
            $scope.data.account = $scope.memberAccount;
            $scope.accountNumber = $scope.memberAccount.accountNumber;
            $scope.accountName = $scope.memberAccount.name
          }
        };


        $scope.findMember = function () {
          personFindService.findAllClientAccounts().then(function (data) {
            console.log(data)
            $scope.accountNumber = data.accountNumber;
            $scope.accountName = data.name;
            $scope.data.accountID = data.accountId;
            $scope.getMemberClientBankAccounts()
          })
        };



        $scope.getMemberClientBankAccounts = function () {

          console.log($scope.data)
          if ($scope.data.currencyID && $scope.data.accountID) {
            Loader.show(true);

            $http.post('/api/records/getBankAccounts/', {
              data: {
                currencyID: $scope.data.currencyID,
                accountID: $scope.data.accountID
              }
            }).then(function (response) {
              Loader.show(false);
              var data = response.data.data;
              if (angular.isArray(data)) {
                $scope.bankAccounts = data
              } else if (data && data.bankNameAz) {
                $scope.bankAccounts = [data]

              } else {
                $scope.bankAccounts = []
              }

              console.log($scope.bankAccounts)
            }, function (erro) {
              Loader.show(false)

            })
          }
        };


        $scope.print = function(){
          var data  = angular.copy($scope.data)
          var win = window.open();
          $http.post('/api/clearing/printPaymentOrder', data).then(function(response){
            win.location = "data:application/pdf;base64," + response.data.data;
          })
        }

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
