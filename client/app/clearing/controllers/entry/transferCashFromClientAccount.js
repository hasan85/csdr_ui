'use strict';

angular.module('cmisApp')
  .controller('TransferCashFromClientAccountController',
    ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService', 'Loader', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', '$kWindow', 'gettextCatalog', 'SweetAlert', 'personFindService', '$http',
      function ($scope, $windowInstance, id, appConstants, operationService, Loader, helperFunctionsService, operationState, task, referenceDataService, $kWindow, gettextCatalog, SweetAlert, personFindService, $http) {


        $scope.metaData = {};
        $scope.data = angular.fromJson(task.draft);


        $scope.serviceClasses = [
          {code: 'BALANCE_SERVICE_TRD', nameAz: 'Ticarət bölməsi'},
          {code: 'BALANCE_SERVICE_SUB', nameAz: 'Abunə yazılışı bölməsi'},
          {code: 'BALANCE_SERVICE_DIS', nameAz: 'Ödəniş bölməsi'}
        ];


        referenceDataService.getCurrencies().then(function (data) {
          $scope.metaData.currencies = data;
          Loader.show(false);
        });


        $scope.prepareData = function (data) {
          return data
        }

        //Initialize scope variables [[
        $scope.config = {
          screenId: id,
          taskKey: id,
          task: task,
          form: {
            name: "outgoingPayments",
            data: {}
          },
          operationType: appConstants.operationTypes.entry,
          window: $windowInstance,
          state: operationState,
          buttons: {
            complete: {
              click: function () {

                var data = $scope.prepareData(angular.copy($scope.data));
                $scope.config.completeTask(data);

              }
            }
          }
        };

        $scope.findAccount = function () {

          personFindService.findMoneyAccounts().then(function (data) {
            $scope.data.account = {
              accountId: data.accountId,
              accountNumber: data.accountNumber,
              name: data.name
            };
          })

        };
        function getAmount() {
          $scope.amount = 0;
          if ($scope.data.account && $scope.data.serviceClass && $scope.data.currency) {
            $scope.amountLoading = true;
            $http.post('/api/records/getFreeCashAmount/', {
              data: {
                accountID: $scope.data.account.accountId,
                serviceClassCode: $scope.data.serviceClass.code,
                currencyCode: $scope.data.currency
              }
            }).then(function (response) {
              $scope.amountLoading = false
              $scope.amount = response.data.data
            }, function (erro) {
              Loader.show(false)

            })
          }
        };
        $scope.$watch('data.currency', function (newVal) {
          if (newVal) getAmount();
        });
        $scope.$watch('data.account', function (newVal) {
          getAmount();
        });
        $scope.$watch('data.serviceClass', function (newVal) {
          getAmount();
        });

// Check if form is dirty
        $scope.$on('closeTask', function () {
          $scope.config.showTaskSavePrompt(false);
        });

// Save task as draft
        $scope.$on('saveTask', function () {
          $scope.config.saveTask(angular.copy($scope.data));
        });

      }])
;
