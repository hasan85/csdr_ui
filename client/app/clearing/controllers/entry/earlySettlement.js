'use strict';

angular.module('cmisApp')
  .controller('EarlySettlementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', '$rootScope',
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, dataSearchService, gettextCatalog, SweetAlert, $rootScope) {

      $scope.metaData = {
        currencies: []
      };

      Loader.show(true);

      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;
        Loader.show(false);
      });

      var prepareFormData = function (viewModel) {
        var result = [];
        for (var i = 0; i < viewModel.length; i++) {
          if (viewModel[i] && viewModel[i].ID) {

            viewModel[i].regDate = helperFunctionsService.generateDateTime(viewModel[i].regDate);
            viewModel[i].settlementDate = helperFunctionsService.generateDateTime(viewModel[i].settlementDate);
            viewModel[i].valueDate = helperFunctionsService.generateDateTime(viewModel[i].valueDate);
            result.push(viewModel[i]);
          }
        }
        return result;
      };

      $scope.earlySettlementData = angular.fromJson(task.draft);

      $scope.mainGridOptions = {
        columns: [
          {
            field: "selectedForEarlySettlement",
            title: gettextCatalog.getString("Select"),
            width: "8rem",
            template: function (e) {
              var t = "<input type='checkbox' data-bind='checked:selectedForEarlySettlement' > ";
              return t;
            }
          },
          {
            field: "recordNumber",
            title: gettextCatalog.getString("Trade Number"),
            width: "10rem"
          },
          {
            field: "regDate",
            title: gettextCatalog.getString("Trade Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "settlementDate",
            title: gettextCatalog.getString("Settlement Date"),
            type: "date",
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "buySideMember.name",
            title: gettextCatalog.getString("Buy Side Member"),
            width: "10rem"
          },
          {
            field: "buySideClient.name",
            title: gettextCatalog.getString("Buy Side Client"),
            width: "10rem"
          },
          {
            field: "sellSideMember.name",
            title: gettextCatalog.getString("Sell Side Member"),
            width: "10rem"
          },
          {
            field: "sellSideClient.name",
            title: gettextCatalog.getString("Sell Side Client"),
            width: "10rem"
          },
          {
            field: "instrument.ISIN",
            title: gettextCatalog.getString("Instrument"),
            width: "10rem"
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity"),
            width: "10rem"
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount"),
            width: "10rem"
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency"),
            width: "10rem"
          },
          {
            field: "recordStatus.name" + $rootScope.lnC,
            title: gettextCatalog.getString("Trade Status"),
            width: "10rem"
          },
          {
            field: "valueDate",
            title: gettextCatalog.getString("Value Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "maturityDate",
            title: gettextCatalog.getString("Maturity Date"),
            width: "10rem",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "comment",
            title: gettextCatalog.getString("Comment"),
            width: "10rem"
          },
          {
            field: "price",
            title: gettextCatalog.getString("Price"),
            width: "10rem"
          },
        ],
        dataBound: function () {
          var rows = this.tbody.children();
          var dataItems = this.dataSource.view();
          for (var i = 0; i < dataItems.length; i++) {
            kendo.bind(rows[i], dataItems[i]);
          }
        },
        dataSource: {
          data: $scope.earlySettlementData.trades,
          schema: {
            model: {
              fields: {
                maturityDate: {type: "date"},
                valueDate: {type: "date"},
                settlementDate: {type: "date"},
                regDate: {type: "date"},
              }
            }
          }
        }
      };
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "earlySettlementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              // if ($scope.config.form.name.$valid) {

              var data = $scope.config.grid.dataSource.view();
              data = {trades: prepareFormData(angular.copy(data))};

              $scope.config.completeTask(data);
              // }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        // var taskPayload = task && task.draft ? angular.toJson(prepareFormData(angular.fromJson(task.draft))) : null;
        // var currentTask = angular.toJson(prepareFormData($scope.earlySettlementData));
        $scope.config.showTaskSavePrompt(false);

      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        var drSave = $scope.config.grid.dataSource.view();
        drSave = {trades: prepareFormData(angular.copy(drSave))};
        $scope.config.saveTask(drSave);
      });

    }]);
