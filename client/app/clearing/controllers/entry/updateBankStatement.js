'use strict';

angular.module('cmisApp')
  .controller('UpdateBankStatementController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', 'clearingService', "$filter",
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, dataSearchService, gettextCatalog, SweetAlert, clearingService, $filter) {

      $scope.metaData = {
        currencies: []
      };

      Loader.show(true);

      var prepareFormData = function (viewModel) {
        viewModel.currency = angular.fromJson(viewModel.currency);
        viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);
        return viewModel;
      };

      $scope.enterBankStatementData = angular.fromJson(task.draft);

      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;
        if ($scope.enterBankStatementData.currency && $scope.enterBankStatementData.currency.id) {
          $scope.enterBankStatementData.currency = $filter('json')(helperFunctionsService.findByIdInsideArray(data, $scope.enterBankStatementData.currency['id']));
        }
        if ($scope.enterBankStatementData.correspondentAccount && $scope.enterBankStatementData.correspondentAccount.accountId) {
          $scope.enterBankStatementData.correspondentAccount = $filter('json')(helperFunctionsService.findByIdInsideArrayByProperty($scope.enterBankStatementData.correspondentAccounts, $scope.enterBankStatementData.correspondentAccount['accountId'], 'accountId'));
        }
        Loader.show(false);
      });

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "enterBankStatementDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                console.log($scope.enterBankStatementData)
                $scope.config.completeTask(prepareFormData(angular.copy($scope.enterBankStatementData)));
              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      $scope.checkAccount = function (accountNumber) {
        var errorMessage = null;
        Loader.show(true);
        dataSearchService.getAccountInfo(accountNumber).then(function (data) {

          if (data && data.success == "false") {
            errorMessage = data.message;
            $scope.enterBankStatementData.accountNumber = null;
          } else if (data.success != "true") {
            errorMessage = gettextCatalog.getString('Internal Server Error')
          }

          if (errorMessage) {
            SweetAlert.swal('', errorMessage, 'error')
          }

          Loader.show(false);
        })
      };

      $scope.findBankStatement = function () {

        clearingService.findBankStatement({
          statusCode: "STATUS_PROCESSING"
        }).then(function (data) {
          console.log(data)
          console.log($scope.enterBankStatementData)
          if (data) {
            $scope.enterBankStatementData.ID = data.ID;
            $scope.enterBankStatementData.accountNumber = data.accountNumber;
            $scope.enterBankStatementData.amount = data.amount;
            $scope.enterBankStatementData.destination = data.destination;
            $scope.enterBankStatementData.referenceNumber = data.referenceNumber;
            $scope.enterBankStatementData.currency = $filter('json')(helperFunctionsService.findByIdInsideArray($scope.metaData.currencies, data['currency']['id']));
            $scope.enterBankStatementData.correspondentAccount = $filter('json')(helperFunctionsService.findByIdInsideArray($scope.enterBankStatementData.correspondentAccounts, data['correspondentAccount']['id']));
          }
        });

      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.enterBankStatementData));
      });

    }]);
