'use strict';

angular.module('cmisApp')
  .controller('EnterPaymentReceiptController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, SweetAlert, gettextCatalog) {

      var payload = angular.fromJson(task.draft);

      $scope.metaData = {
        currencies: []
      };

      Loader.show(true);

      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;
        Loader.show(false);
      });

      var prepareFormData = function (viewModel) {
        viewModel.currency = angular.fromJson(viewModel.currency);
        viewModel.correspondentAccount = angular.fromJson(viewModel.correspondentAccount);

        return viewModel;
      };
      var validateForm = function () {
        var isValid = false;
        for (var i = 0; i < $scope.enterPaymentReceiptData.bankStatements.length; i++) {
          if ($scope.enterPaymentReceiptData.bankStatements[i].approved) {
            isValid = true;
            break;
          }
        }
        return isValid;
      };
      $scope.enterPaymentReceiptData = angular.copy(payload);


      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "enterPaymentReceiptDataForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              if (validateForm()) {
                $scope.config.completeTask(prepareFormData(angular.copy($scope.enterPaymentReceiptData)));
              } else {
                SweetAlert.swal("", gettextCatalog.getString('You have to select at least one bank statement!'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);

      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.enterPaymentReceiptData));
      });

    }]);
