'use strict';

angular.module('cmisApp').controller('CorrectPaymentApprovalController', ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task',   function ($scope, $windowInstance, id, appConstants, taskId, operationState, task) {
  $scope.config = {
    screenId: id,
    taskId: taskId,
    task: task,
    operationType: appConstants.operationTypes.approval,
    window: $windowInstance,
    state: operationState
  };

}]);
