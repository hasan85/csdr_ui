'use strict';

angular.module('cmisApp')
  .controller('WithdrawalCashApprovalController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'Loader', 'helperFunctionsService', 'operationState', 'task',
    'referenceDataService', 'dataSearchService', 'gettextCatalog', 'SweetAlert', "$http", "$q", "$timeout",
    function ($scope, $windowInstance, id, appConstants, operationService,
              Loader, helperFunctionsService, operationState, task,
              referenceDataService, dataSearchService, gettextCatalog, SweetAlert, $http, $q, $timeout) {


      $scope.metaData = {
        currencies: []
      };

      $scope.rejectionConfig = {
        cancelRejectionEnabled: false
      };

      var validateForm = function (viewModel) {
        for (var i = 0; i < viewModel.positions.length; i++) {
          if (viewModel.positions[i].withdrawalAmount > 0) {
            return true;
          }
        }
        SweetAlert.swal("", gettextCatalog.getString('You have to make at least one withdrawal!'), 'error');
        return false;
      };

      $scope.enterWithdrawalOrderData = angular.fromJson(task.draft);
      console.log("Task draft: ",$scope.enterWithdrawalOrderData);

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "enterWithdrawalOrderDataForm",
          data: {},
          approveAll: null,
          rejectAll: null
        },
        operationType: appConstants.operationTypes.approval,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              var taskData = angular.copy($scope.enterWithdrawalOrderData);
              $scope.taskData = taskData;
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid && validateForm(taskData)) {
                $scope.config.completeTask(taskData);
              } else{
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      $scope.approveAllChanged = function(approve) {
        $scope.enterWithdrawalOrderData.positions.map(function(position) {
          position.approved = approve;
        })
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask(angular.copy($scope.enterWithdrawalOrderData));
      });

    }]);
