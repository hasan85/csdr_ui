'use strict';

angular.module('cmisApp').controller('EarlySettlementApprovalController', ["$rootScope", '$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', "gettextCatalog", function ($rootScope, $scope, $windowInstance, id, appConstants, taskId, operationState, task, gettextCatalog) {
  $scope.config = {
    screenId: id,
    taskId: taskId,
    task: task,
    operationType: appConstants.operationTypes.approval,
    window: $windowInstance,
    state: operationState
  };

  $scope.earlySettlementData = angular.fromJson(task.draft);

  $scope.mainGridOptions = {
    columns: [
      {
        field: "selectedForEarlySettlement",
        title: gettextCatalog.getString("Select for Early Settlement"),
        width: "15rem",
        template: function (e) {
          return "<input type='checkbox' style='pointer-events: none' data-bind='checked:selectedForEarlySettlement'>";
        }
      },
      {
        field: "recordNumber",
        title: gettextCatalog.getString("Trade Number"),
        width: "10rem"
      },
      {
        field: "regDate",
        title: gettextCatalog.getString("Trade Date"),
        width: "10rem",
        format: "{0:dd-MMMM-yyyy}"
      },
      {
        field: "settlementDate",
        title: gettextCatalog.getString("Settlement Date"),
        type: "date",
        width: "10rem",
        format: "{0:dd-MMMM-yyyy}"
      },
      {
        field: "buySideMember.name",
        title: gettextCatalog.getString("Buy Side Member"),
        width: "10rem"
      },
      {
        field: "buySideClient.name",
        title: gettextCatalog.getString("Buy Side Client"),
        width: "10rem"
      },
      {
        field: "sellSideMember.name",
        title: gettextCatalog.getString("Sell Side Member"),
        width: "10rem"
      },
      {
        field: "sellSideClient.name",
        title: gettextCatalog.getString("Sell Side Client"),
        width: "10rem"
      },
      {
        field: "instrument.ISIN",
        title: gettextCatalog.getString("Instrument"),
        width: "10rem"
      },
      {
        field: "quantity",
        title: gettextCatalog.getString("Quantity"),
        width: "10rem"
      },
      {
        field: "amount",
        title: gettextCatalog.getString("Amount"),
        width: "10rem"
      },
      {
        field: "currency.code",
        title: gettextCatalog.getString("Currency"),
        width: "10rem"
      },
      {
        field: "recordStatus.name" + $rootScope.lnC,
        title: gettextCatalog.getString("Trade Status"),
        width: "10rem"
      },
      {
        field: "valueDate",
        title: gettextCatalog.getString("Value Date"),
        width: "10rem",
        format: "{0:dd-MMMM-yyyy}"
      },
      {
        field: "maturityDate",
        title: gettextCatalog.getString("Maturity Date"),
        width: "10rem",
        format: "{0:dd-MMMM-yyyy}"
      },
      {
        field: "comment",
        title: gettextCatalog.getString("Comment"),
        width: "10rem"
      },
      {
        field: "price",
        title: gettextCatalog.getString("Price"),
        width: "10rem"
      }
    ],
    dataBound: function () {
      var rows = this.tbody.children();
      var dataItems = this.dataSource.view();
      for (var i = 0; i < dataItems.length; i++) {
        kendo.bind(rows[i], dataItems[i]);
      }
    },
    dataSource: {
      data: $scope.earlySettlementData.trades,
      schema: {
        model: {
          fields: {
            maturityDate: {type: "date"},
            valueDate: {type: "date"},
            settlementDate: {type: "date"},
            regDate: {type: "date"}
          }
        }
      }
    }
  };
}]);
