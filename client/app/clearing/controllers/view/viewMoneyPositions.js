'use strict';

angular.module('cmisApp')
  .controller('MoneyPositionsController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      // Generic search configuration for person
      $scope.searchConfig = {
        moneyPosition: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
