'use strict';

angular.module('cmisApp')
    .controller('ViewMemberAdvancesController', ['$scope', '$windowInstance', 'personFindService', 'id',
        function ($scope, $windowInstance, personFindService, id) {

            // Generic search configuration for person
            $scope.searchConfig = {
                memberAdvance: {
                    searchResult: {
                        data: {},
                        isEmpty: false
                    }
                },
                params: {
                    config: {
                        searchOnInit: true
                    }
                }
            };

            // View configuration
            $scope.config = {
                screenId: id,
                window: $windowInstance,
                mainGrid: {}
            };

        }]);
