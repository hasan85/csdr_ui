'use strict';

angular.module('cmisApp')
  .controller('SwiftPaymentsController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      // Generic search configuration for trade
      $scope.searchConfig = {
        subscriptionOrders: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
