'use strict';

angular.module('cmisApp')
  .controller('ViewCbarCloseBalanceController', ['$scope', '$windowInstance', 'personFindService', 'id',
    function ($scope, $windowInstance, personFindService, id) {

      $scope.searchConfig = {
        accountPrescription: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/records/getCBARDayCloseBalances/',
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

      $scope.selectPerson = function () {
        personFindService.selectPerson($scope.searchConfig.accountPrescription.selectedPersonIndex );

        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };


    }]);
