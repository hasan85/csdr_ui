'use strict';

angular.module('cmisApp')
  .controller('ClientAccountsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false
        },
        params: {
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };
      $scope.$watch('searchConfig.person.selectedPersonIndex', function (val) {

        if (val !== false) {
          //  $scope.config.buttons.view.disabled = false;
        }
        else {
          $scope.searchConfig.person.selectedPersonIndex = false;
        }

      });

    }]);
