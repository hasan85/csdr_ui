'use strict';

angular.module('cmisApp')
  .controller('BankStatementsController', ['$scope', '$windowInstance', 'personFindService', 'id', 'gettextCatalog',
    function ($scope, $windowInstance, personFindService, id, gettextCatalog) {

      // Generic search configuration for person
      $scope.searchConfig = {
        bankStatement: {
          searchResult: {
            data: {},
            isEmpty: false
          }
        },
        params: {
          searchUrl: '/api/clearing/getBankStatements',
          config: {
            searchOnInit: true
          }
        }
      };

      // View configuration
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {}
      };

    }]);
