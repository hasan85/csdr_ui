'use strict';

angular.module('cmisApp')
  .controller('AllClientSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'clearingService', 'helperFunctionsService',
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, clearingService, helperFunctionsService) {

      $scope.gridColumns = [
        {
          field: "name",
          title: gettextCatalog.getString("Client")
        },
        {
          field: "shareholderAccountNumber",
          title: gettextCatalog.getString("Account Number")
        }
      ];

      var validateForm = function (formData) {

        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

        result.success = true;

        return result;
      };

      var findPerson = function () {

        $scope.person.personDetails = false;

        if ($scope.person.searchResultGrid !== undefined) {
          $scope.person.searchResultGrid.refresh();
        }

        $scope.person.selectedPersonIndex = false;

        if ($scope.person.formName.$dirty) {
          $scope.person.formName.$submitted = true;
        }

        if ($scope.person.formName.$valid || !$scope.person.formName.$dirty) {

          $scope.person.searchResult.isEmpty = false;

          Loader.show(true);

          var formData = angular.copy($scope.person.form);

          var formValidationResult = {success: false};

          if ($scope.params.config.searchOnInit == true) {
            formValidationResult.success = true;
          } else {
            formValidationResult = validateForm(formData);
          }
          if (formValidationResult.success) {


            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }
            if ($scope.params.brokerId) {
              formData.personId = $scope.params.brokerId;
            }
            if ($scope.params.personClassCode) {
              formData.personClassCode = $scope.params.personClassCode;
            }

            clearingService.getClientShareholders($scope.params.searchUrl, formData).then(function (data) {

              $scope.person.searchResult.data = data;

              if (data.success === "true") {
                data = {
                  'data': data.data, pageSize: 20,
                  schema: {
                    model: {
                      fields: {
                        identificatorFinishDate: {type: "date"},
                      }
                    }
                  }
                };
                $scope.gridData = data;


              } else {
                $scope.person.searchResult.isEmpty = true;
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }


              Loader.show(false);
            });
            Loader.show(true);
          } else {
            Loader.show(false);
            //$scope.params.showSearchCriteriaBlock = true;
            SweetAlert.swal("", formValidationResult.message, "error");
          }
        }
      };


      var personSelected = function (data) {

        if(data) {
          $scope.searchConfig.person.selectedPerson = data;
        } else {
          $scope.searchConfig.person.selectedPerson = null;
        }
        
        var sResult = angular.copy($scope.person.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].id == data.id) {
            $scope.person.selectedPersonIndex = i;
            break;
          }
        }
      };

      $scope.personSelected = personSelected;

      var _params = {
        searchUrl: "/api/clearing/getAllClientAccounts",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {
            personType: {
              isVisible: true,
              default: appConstants.personClasses.juridicalPerson
            }
          },
          searchOnInit: true

        },
        labels: {
          lblInstruments: gettextCatalog.getString('Instruments'),
          lblMoney: gettextCatalog.getString('Money')
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var _person = {
        formName: 'personFindForm',
        form: {},
        data: {
          personClasses: [],
          personClassCodes: {
            natural_person: appConstants.personClasses.naturalPerson,
            juridical_person: appConstants.personClasses.juridicalPerson
          }
        },

        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedPersonType: $scope.params.config.criteriaForm.personType.default,
        selectedPersonIndex: false,
        findPerson: findPerson,
        personDetails: false
      };

      $scope.person = angular.merge($scope.searchConfig.person, _person);

      $scope.findPerson = findPerson;

      $scope.$on('refreshGrid', function () {
        findPerson();
      });

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.detailInit = function (e) {
        Loader.show(true);

        var searchData = {
          accountId: e.data.accountId,
          accountNumber: e.data.accountNumber,
          depoAccountId: e.data.depoAccountId,
          id: e.data.id,
          name: e.data.name,
        };
        clearingService._getAccountPositions(searchData).then(function (res) {

          Loader.show(false);
          if (res && res.isSuccess /*res.success === "true"*/) {
            if (res.data && res.data.securityPositions) {

              // res.data.securityPositions = {data: res.data.securityPositions, pageSize: 20}
            }
            e.data = angular.extend(e.data, res.data);
            e.pageSize = 5;
          } else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
          }
        });
      };

      $scope.resetForm = function () {
        $scope.person.form.fullName = null;
        $scope.person.form.accountNumber = null;
      };

      if ($scope.params.config.searchOnInit == true) {
        findPerson();
      }

      $scope.instrumentsGrid = function () {
        return {
          scrollable: false,
          pageable: false,
          columns: [
            {
              field: "instrument.instrumentName",
              title: gettextCatalog.getString("Instrument")
            },
            {
              field: "instrument.ISIN",
              title: gettextCatalog.getString("ISIN")
            },
            {
              field: "instrument.issuerName",
              title: gettextCatalog.getString("Issuer")
            },
            {
              field: "quantity",
              title: gettextCatalog.getString("Quantity")
            },
            {
              field: "orderBlockQuantity",
              title: gettextCatalog.getString("Order Block Quantity")
            }
          ]
        };
      };

    }]);
