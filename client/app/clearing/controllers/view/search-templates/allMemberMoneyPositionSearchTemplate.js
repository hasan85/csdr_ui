'use strict';

angular.module('cmisApp')
  .controller('AllMemberMoneyPositionSearchTemplateController',
  ['$scope', 'clearingService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', '$rootScope', 'helperFunctionsService', 'personFindService',
    function ($scope, clearingService, Loader, gettextCatalog, appConstants, SweetAlert, $rootScope, helperFunctionsService, personFindService) {

      $scope.gridColumns = [
        {
          field: "name",
          title: gettextCatalog.getString("Name"),
          width: '30%'
        },
        {
          field: "idDocument",
          title: gettextCatalog.getString("ID Document"),
          width: '20%'
        },
        {
          field: "addressLine",
          title: gettextCatalog.getString("Address"),
          width: '30%'
        },
        {
          field: "uniqueCode",
          title: gettextCatalog.getString("Unique ID"),
          width: '20%'
        }
      ];

      $scope.clientsGrid = {
        dataSource: {
          schema: {
            model: {
              fields: {
                amount: {type: "number"}
              }
            }
          }
        },
        columns: [

          {
            field: "account.name",
            title: gettextCatalog.getString("Client")
          },
          {
            field: "account.accountNumber",
            title: gettextCatalog.getString("Account Number")
          },
          {
            field: "serviceClass.nameAz",
            title: "Xidmət sinfi"
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency")
          },
          {
            field: "amount",
            title: gettextCatalog.getString("Amount"),
            template: "<div style='text-align: right'>#= kendo.toString(data.amount, 'n2') #</div>"
          },
          {
            field: "orderBlockAmount",
            title: gettextCatalog.getString("Order Block")

          }
        ]
      };

      var findData = function () {
        if ($scope.moneyPosition.searchResultGrid !== undefined) {
          $scope.moneyPosition.searchResultGrid.refresh();
        }
        $scope.moneyPosition.searchResult.isEmpty = false;
        Loader.show(true);
        personFindService.findPersonByData('/api/persons/getClearingMembers', null).then(function (data) {
          if (data.success === "true") {
            $scope.gridData = {'data': data.data, pageSize: 10};
            $scope.moneyPosition.searchResult.data = data.data;

          } else {
            $scope.moneyPosition.searchResult.isEmpty = true;
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
          }
          Loader.show(false);
        });
      };

      var _params = {
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        },
        labels: {
          lblClients: gettextCatalog.getString('Clients'),
          lblMoneyPosition: gettextCatalog.getString('Money Positions')
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      var _moneyPosition = {
        formName: 'moneyPositionFindDorm',
        form: {},
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedMoneyPositionIndex: false,
        findData: findData
      };

      $scope.moneyPosition = angular.merge($scope.searchConfig.moneyPosition, _moneyPosition);

      $scope.findData = findData;

      $scope.$on('refreshGrid', function () {
        findData();
      });

      if ($scope.params.config.searchOnInit == true) {
        findData();
      }

      $scope.detailInit = function (e) {
        Loader.show(true);

        clearingService.getTradingMemberWithClientMoneyPositions(e.data.id).then(function (res) {
          Loader.show(false);
          if (res && res.success === "true") {
            if (res.data && res.data) {
              e.data.memberPositions = res.data;
              e.data.memberPositions.childAccountMoneyPositions = {
                'data': e.data.memberPositions.childAccountMoneyPositions,
                pageSize: 10
              }
            }
          } else {
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
          }
        });
      };

    }]);
