'use strict';

angular.module('cmisApp')
  .controller('ClientAccountFindController', ['$scope', '$windowInstance', 'personFindService', 'brokerId', 'searchUrl', 'personClassCode',
    function ($scope, $windowInstance, personFindService, brokerId, searchUrl, personClassCode) {

      $scope.searchConfig = {
        person: {
          searchResult: {
            data: {},
            isEmpty: false
          },
          selectedPersonIndex: false,
          selectedPerson: null
        },
        params: {
          showSearchCriteriaBlock: true,
          brokerId: brokerId,
          searchUrl: searchUrl ? searchUrl : "/api/clearing/getTradingMemberAccount",
          personClassCode: personClassCode ? personClassCode : null,
        }
      };

      $scope.selectPerson = function () {
        personFindService.selectPerson($scope.searchConfig.person.selectedPerson);
        $windowInstance.close();
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };
    }]);
