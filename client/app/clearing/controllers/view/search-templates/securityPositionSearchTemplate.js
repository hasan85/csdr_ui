'use strict';

angular.module('cmisApp')
  .controller('SecurityPositionSearchTemplateController',
  ['$scope', 'clearingService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', '$rootScope','helperFunctionsService', "$filter",
    function ($scope, clearingService, Loader, gettextCatalog, appConstants, SweetAlert, $rootScope, helperFunctionsService, $filter) {

      $scope.gridColumns = [

        {
          field: "account.name",
          title: gettextCatalog.getString("Client")
        },
        {
          field: "instrument.instrumentName",
          title: gettextCatalog.getString("Instrument")
        },
        {
          field: "instrument.ISIN",
          title: gettextCatalog.getString("ISIN")
        },
        {
          field: "instrument.issuerName",
          title: gettextCatalog.getString("Issuer")
        },
        {
          field: "quantity",
          title: gettextCatalog.getString("Quantity"),
          groupable: false,
          type: "number",
          template: function(dataItem) {
            return dataItem.quantity ? $filter("formatNumber")(dataItem.quantity, false) : "";
          }
        },
        {
          field: "orderBlockQuantity",
          title: gettextCatalog.getString("Order Block"),
        },
      ];

      var findData = function () {

        if ($scope.securityPosition.searchResultGrid !== undefined) {
          $scope.securityPosition.searchResultGrid.refresh();
        }

        $scope.securityPosition.searchResult.isEmpty = false;

        Loader.show(true);
        clearingService.getTradingMemberWithClientSecurityPositions().then(function (data) {

          console.log(data);
          if (data.success === "true") {
            $scope.gridData = {
              'data': data.data.childAccountSecurityPositions,
              pageSize: 10,
              schema: {
                model: {
                  fields: {
                    quantity: {type: "number"}
                  }
                }
              }
            };
            $scope.securityPosition.searchResult.data = data.data;

          } else {
            $scope.securityPosition.searchResult.isEmpty = true;
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
          }
          Loader.show(false);
        });
      };

      var _params = {
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      var _securityPosition = {
        formName: 'securityPositionFindDorm',
        form: {},
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedSecurityPositionIndex: false,
        findData: findData
      };

      $scope.securityPosition = angular.merge($scope.searchConfig.securityPosition, _securityPosition);

      $scope.findData = findData;

      $scope.$on('refreshGrid', function () {
        findData();
      });

      if ($scope.params.config.searchOnInit == true) {
        findData();
      }

    }]);
