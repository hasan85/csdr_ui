'use strict';

angular.module('cmisApp')
  .controller('WithdrawalSearchTemplateController',
  ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService',
    '$http', 'helperFunctionsService', 'recordsService', '$rootScope', "$filter",
    function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService,
              $http, helperFunctionsService, recordsService, $rootScope, $filter) {

      $scope.metaData = {};
      Loader.show(true);
      referenceDataService.getCurrencies().then(function (data) {

        $scope.metaData.currencies = data;

        referenceDataService.addEmptyOption([
          $scope.metaData.currencies
        ]);

        Loader.show(false);
      });
      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

          result.success = true;

        return result;
      };
      var withdrawalSelected = function (data) {
        var sResult = angular.copy($scope.withdrawal.searchResult.data.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].ID == data.ID) {
            $scope.withdrawal.selectedWithdrawalIndex = i;
            break;
          }
        }
      };
      $scope.withdrawalSelected = withdrawalSelected;
      var _params = {
        searchUrl: "/api/clearing/getWithdrawals/",
        showSearchCriteriaBlock: false,
        config: {
          criteriaForm: {},
          searchOnInit: false
        },
        criteriaEnabled: true
      };
      $scope.params = angular.merge(_params, $scope.searchConfig.params);
      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: !$scope.params.showSearchCriteriaBlock
        },
        {
          size: '70%',
          collapsible: false
        }
      ];
      var _withdrawal = {
        formName: 'withdrawalFindForm',
        form: {},
        data: {},

        searchResult: {
          data: null,
          isEmpty: false
        },

        selectedWithdrawalIndex: false,
        withdrawalSelected: withdrawalSelected,
        withdrawalDetails: false
      };
      $scope.withdrawal = angular.merge($scope.searchConfig.withdrawal, _withdrawal);
      var findWithdrawal = function (e) {
        //$scope.person.withdrawalDetails = false;
        if ($scope.withdrawal.searchResultGrid !== undefined) {
          $scope.withdrawal.searchResultGrid.refresh();
        }

        $scope.withdrawal.selectedWithdrawalIndex = false;

        if ($scope.withdrawal.formName.$dirty) {
          $scope.withdrawal.formName.$submitted = true;
        }

        if ($scope.withdrawal.formName.$valid || !$scope.withdrawal.formName.$dirty) {

          $scope.withdrawal.searchResult.isEmpty = false;

          var formData = angular.copy($scope.withdrawal.form);

          formData.regDateStart = $scope.withdrawal.form.regDateStart ? $scope.withdrawal.form.regDateStartObj : null;
          formData.regDateFinish = $scope.withdrawal.form.regDateFinish ? $scope.withdrawal.form.regDateFinishObj : null;
          formData.settlementDateStart = $scope.withdrawal.form.settlementDateStart ? $scope.withdrawal.form.settlementDateStartObj : null;
          formData.settlementDateFinish = $scope.withdrawal.form.settlementDateFinish ? $scope.withdrawal.form.settlementDateFinishObj : null;
          formData.valueDateStart = $scope.withdrawal.form.valueDateStart ? $scope.withdrawal.form.valueDateStartObj : null;
          formData.valueDateFinish = $scope.withdrawal.form.valueDateFinish ? $scope.withdrawal.form.valueDateFinishObj : null;
          if ($scope.params.config.searchCriteria) {
            formData = angular.merge(formData, $scope.params.config.searchCriteria);
          }
          var requestData = angular.extend(formData, {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
          });

          Loader.show(true);
          $http({method: 'POST', url: $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {
              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data) {
                $scope.withdrawal.searchResult.data = data;
              } else {
                $scope.withdrawal.searchResult.isEmpty = true;
              }

              console.log('Response data', data);
              if (data['success'] === "true") {

                e.success({
                  Data: data.data ? data.data : [], Total: data.total, schema: {
                    model: {
                      fields: {
                        regDate: {type: "date"},
                        valueDate: {type: "date"}
                      }
                    }
                  }
                });
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
        }
      };
      $scope.withdrawal.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total",
            model: {
              fields: {

                withdrawalDate: {type: "date"},
              }
            }
          },
          transport: {
            read: function (e) {
              findWithdrawal(e);
            }
          },

          serverPaging: true,
          sort: {field: "withdrawalDate", dir: "desc"},
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [

          {
            field: "withdrawalDate",
            title: gettextCatalog.getString("Withdrawal Date"),
            format: "{0:dd-MMMM-yyyy HH:mm }",
            locked: true,
            lockable: false,
            width: "220px"
          }, {
            field: "account.brokerName",
            title: gettextCatalog.getString("Broker Name"),
            width: '12rem'
          },
          {
            field: "account.name",
            title: gettextCatalog.getString('Person'),
            width: '12rem'
          },
          {
            field: "account.accountNumber",
            title: gettextCatalog.getString("Account Number"),
            width: '12rem'
          },
          {
            field: "correspondentAccount.accountNumber",
            title: gettextCatalog.getString("Correspondent Account"),
            width: '12rem',
          },
          {
            field: "withdrawalAmount",
            title: gettextCatalog.getString("Withdrawal Amount"),
            width: '12rem',
            template: function(dataItem) {
              return dataItem.withdrawalAmount ? $filter("formatNumber")(dataItem.withdrawalAmount) : "";
            }
          },
          {
            field: "currency.code",
            title: gettextCatalog.getString("Currency"),
            width: '12rem',
          },
          {
            field: "selectedBankAccount",
            title: gettextCatalog.getString("To Account"),
            width: '12rem'
          },


        ]
      };
      $scope.withdrawalSelected = withdrawalSelected;
      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };
      $scope.searchCriteriaCollapse = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = false;
        });
      };
      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {
          $scope.params.showSearchCriteriaBlock = true;
        });
      };
      $scope.resetForm = function () {
        $scope.withdrawal.form.destination = null;
        $scope.withdrawal.form.referenceNumber = null;
        $scope.withdrawal.form.regDateStart = null;
        $scope.withdrawal.form.regDateFinish = null;
        $scope.withdrawal.form.settlementDateStart = null;
        $scope.withdrawal.form.settlementDateFinish = null;
        $scope.withdrawal.form.sellSideMemberName = null;
        $scope.withdrawal.form.sellSideClientName = null;
        $scope.withdrawal.form.buySideMemberName = null;
        $scope.withdrawal.form.buySideClientName = null;
        $scope.withdrawal.form.comment = null;
        $scope.withdrawal.form.currencyID = null;
      };
      $scope.findWithdrawal = function () {
        var formValidationResult = validateForm(angular.copy($scope.withdrawal.form));
        if (formValidationResult.success) {
          if ($scope.withdrawal.searchResultGrid) {
            $scope.withdrawal.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.withdrawal.searchResultGrid) {
          $scope.withdrawal.searchResultGrid = widget;
        }
      });

    }]);
