'use strict';

angular.module('cmisApp')
    .controller('ClientShareholderSearchTemplateController', ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'clearingService', 'helperFunctionsService', "$filter", function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, clearingService, helperFunctionsService, $filter) {

        function printGrid(gridInstance) {
            var gridElement = $(gridInstance.element).clone(),
                win = window.open('', '', 'width=800, height=500'),
                doc = win.document.open();

            var htmlStart =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title>Account Information</title>' +
                '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
                '<style>' +
                'html { font: 11pt sans-serif; }' +
                '.k-grid-toolbar, .k-grid-pager, .k-grid-header { display: none !important; }' +
                '.k-link {color: #000}' +
                'table {width: 100%;text-align: left;border-collapse: collapse;table-layout: auto !important;}'+
                'col.k-hierarchy-col {width: 20px;}td, th {padding: 18px 20px;display: block}' + "" +
                "thead, tbody {display: inline-block}" +
                '</style>' +
                '</head>' +
                '<body>';

            var htmlEnd =
                '</body>' +
                '</html>';

            var thead = gridElement.find(".k-grid-header").find("thead")[0].outerHTML;
            gridElement.find(".k-grid-content").find("tbody").find("tr").each(function() {
                if(!$(this).hasClass("k-state-selected")) {
                    $(this).remove();
                }
            });
            gridElement.find(".k-grid-content").find("table").append(thead);

            doc.write(htmlStart + gridElement.html() + htmlEnd);
            doc.close();
            setTimeout(function() {
                win.print();
                win.close();
            }, 1200);
        }

        $scope.printAccountInformation = function() {
            printGrid($scope.person.searchResultGrid);
        };

        $scope.gridColumns = [
            {
                field: "name",
                title: gettextCatalog.getString("Client")
            },
            {
                field: "accountNumber",
                title: gettextCatalog.getString("Account Number")
            },
            {
                field: "participantAccountNumber",
                title: gettextCatalog.getString("Client in order management system"),
                template: function (e) {
                    return e.participantAccountNumber ? '<b>' + e.participantAccountNumber + '</b>' : "";
                }
            },
            {
                field: "creationDate",
                title: gettextCatalog.getString("Creation Date"),
                format: "{0:dd-MMMM-yyyy}"
            }
        ];

        var validateForm = function (formData) {

            var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};

            result.success = true;

            return result;
        };

        var findPerson = function () {

            $scope.person.personDetails = false;

            if ($scope.person.searchResultGrid !== undefined) {
                $scope.person.searchResultGrid.refresh();
            }

            $scope.person.selectedPersonIndex = false;

            if ($scope.person.formName.$dirty) {
                $scope.person.formName.$submitted = true;
            }

            if ($scope.person.formName.$valid || !$scope.person.formName.$dirty) {

                $scope.person.searchResult.isEmpty = false;

                Loader.show(true);

                var formData = angular.copy($scope.person.form);

                var formValidationResult = {success: false};

                if ($scope.params.config.searchOnInit == true) {
                    formValidationResult.success = true;
                } else {
                    formValidationResult = validateForm(formData);
                }
                if (formValidationResult.success) {


                    if ($scope.params.config.searchCriteria) {
                        formData = angular.merge(formData, $scope.params.config.searchCriteria);
                    }
                    if ($scope.params.brokerId) {
                        formData.personId = $scope.params.brokerId;
                    }
                    if ($scope.params.personClassCode) {
                        formData.personClassCode = $scope.params.personClassCode;
                    }

                    clearingService.getClientShareholders($scope.params.searchUrl, formData).then(function (data) {

                        $scope.person.searchResult.data = data;

                        if (data.success === "true") {
                            data = {
                                data: data.data,
                                pageSize: 10,
                                schema: {
                                    model: {
                                        fields: {
                                            creationDate: {type: "date"}
                                        }
                                    }
                                }
                            };
                            $scope.gridData = data;
                        } else {
                            $scope.person.searchResult.isEmpty = true;
                            SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                        }


                        Loader.show(false);
                    });
                    Loader.show(true);
                } else {
                    Loader.show(false);
                    //$scope.params.showSearchCriteriaBlock = true;
                    SweetAlert.swal("", formValidationResult.message, "error");
                }
            }
        };


        var personSelected = function (data) {
          if(data) {
            $scope.searchConfig.person.selectedPerson = data;
          } else {
            $scope.searchConfig.person.selectedPerson = null;
          }
        };

        $scope.personSelected = personSelected;

        var _params = {
            searchUrl: "/api/clearing/getClientShareholders",
            showSearchCriteriaBlock: false,
            config: {
                criteriaForm: {
                    personType: {
                        isVisible: true,
                        default: appConstants.personClasses.juridicalPerson
                    }
                },
                searchOnInit: true

            },
            labels: {
                lblInstruments: gettextCatalog.getString('Instruments'),
                lblMoney: gettextCatalog.getString('Money')
            }
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);

        $scope.params.config.splitterPanesConfig = [
            {
                size: '30%',
                collapsible: true,
                collapsed: !$scope.params.showSearchCriteriaBlock
            },
            {
                size: '70%',
                collapsible: false
            }
        ];

        var _person = {
            formName: 'personFindForm',
            form: {},
            data: {
                personClasses: [],
                personClassCodes: {
                    natural_person: appConstants.personClasses.naturalPerson,
                    juridical_person: appConstants.personClasses.juridicalPerson
                }
            },

            searchResult: {
                data: null,
                isEmpty: false
            },
            selectedPersonType: $scope.params.config.criteriaForm.personType.default,
            selectedPersonIndex: false,
            findPerson: findPerson,
            personDetails: false
        };

        $scope.person = angular.merge($scope.searchConfig.person, _person);

        $scope.showPersonDetails = function () {

          Loader.show(true);
          personFindService.findPersonById($scope.searchConfig.person.searchResult.data.data[
            $scope.searchConfig.person.selectedPersonIndex]['id']).then(function (data) {
            $scope.person.personDetails = data;
            Loader.show(false);
          });

        };

        $scope.closePersonDetails = function () {
          $scope.person.personDetails = false;
        };

        $scope.changePersonType = function () {

          var personClasses = angular.copy($scope.person.data.personClasses);
          for (var i = 0; i < personClasses.length; i++) {
            if (personClasses[i].id == $scope.person.form.personClassId) {
              $scope.person.selectedPersonType = personClasses[i].code;
            }
          }
          $scope.resetForm();
        };

        $scope.findPerson = findPerson;

        $scope.$on('refreshGrid', function () {
            findPerson();
        });

        $scope.toggleSearchCriteriaBlock = function (splitter) {
            if ($scope.params.showSearchCriteriaBlock === false) {
                splitter.expand(".k-pane:first");
            } else {
                splitter.collapse(".k-pane:first");
            }
            $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
            $scope.$apply(function () {
                $scope.params.showSearchCriteriaBlock = false;
            });
        };

        $scope.searchCriteriaExpand = function () {
            $scope.$apply(function () {
                $scope.params.showSearchCriteriaBlock = true;
            });
        };

        $scope.detailInit = function (e) {
            Loader.show(true);

            var searchData = {
                accountId: e.data.accountId,
                accountNumber: e.data.accountNumber,
                depoAccountId: e.data.depoAccountId,
                id: e.data.id,
                name: e.data.name,
            };
            clearingService.getAccountPositions(searchData).then(function (res) {


                Loader.show(false);
                if (res && res.success === "true") {
                    if (res.data && res.data.securityPositions) {

                        res.data.securityPositions = {data: res.data.securityPositions, pageSize: 10}
                    }
                    e.data = angular.extend(e.data, res.data);
                    e.pageSize = 5;
                } else {
                    SweetAlert.swal("", helperFunctionsService.showErrorMessage(res), 'error');
                }
            });
        };

        $scope.resetForm = function () {
            $scope.person.form.fullName = null;
            $scope.person.form.accountNumber = null;
        };

        if ($scope.params.config.searchOnInit == true) {
            findPerson();
        }

        $scope.instrumentsGrid = function () {
            return {
                scrollable: false,
                pageable: false,
                columns: [
                    {
                        field: "instrument.instrumentName",
                        title: gettextCatalog.getString("Instrument")
                    },
                    {
                        field: "instrument.ISIN",
                        title: gettextCatalog.getString("ISIN")
                    },
                    {
                        field: "instrument.issuerName",
                        title: gettextCatalog.getString("Issuer")
                    },
                    {
                        field: "quantity",
                        title: gettextCatalog.getString("Quantity"),
                      template: function(dataItem) {
                        return dataItem.quantity ? $filter("formatNumber")(dataItem.quantity, false) : "";
                      }
                    },
                    {
                        field: "orderBlockQuantity",
                        title: gettextCatalog.getString("Order Block Quantity")
                    }
                ]
            };
        };

    }]);
