'use strict';

angular.module('cmisApp')
  .controller('ClearingLimitSearchTemplateController',
  ['$scope', 'clearingService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', '$rootScope','helperFunctionsService',
    function ($scope, clearingService, Loader, gettextCatalog, appConstants, SweetAlert, $rootScope, helperFunctionsService) {

      $scope.gridColumns = [

        {
          field: "account.name",
          title: gettextCatalog.getString("Member")
        },
        {
          field: "limit",
          title: gettextCatalog.getString("Limit"),
          template: "<div style='text-align: right'>#= kendo.toString(limit, 'n2') # </div>"
        },
        {
          field: "block",
          title: gettextCatalog.getString("Block")
        },
        {
          field: "currency.code",
          title: gettextCatalog.getString("Currency")
        }

      ];


      var findData = function () {

        if ($scope.clearingLimit.searchResultGrid !== undefined) {
          $scope.clearingLimit.searchResultGrid.refresh();
        }

        $scope.clearingLimit.searchResult.isEmpty = false;

        Loader.show(true);


        clearingService.getClearingLimitsByUrl($scope.params.searchUrl, {}).then(function (data) {
          if (data.success === "true") {
            data = {'data': data.data, pageSize: 10};
            $scope.gridData = data;
            $scope.clearingLimit.searchResult.data = data;

          } else {
            $scope.clearingLimit.searchResult.isEmpty = true;
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
          }
          Loader.show(false);
        });


      };

      var _params = {
        searchUrl: "/api/clearing/getClearingLimits",
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      var _clearingLimit = {
        formName: 'clearingLimitFindDorm',
        form: {},
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedClearingLimitIndex: false,
        findData: findData
      };

      $scope.clearingLimit = angular.merge($scope.searchConfig.clearingLimit, _clearingLimit);

      $scope.findData = findData;

      $scope.$on('refreshGrid', function () {
        findData();
      });

      if ($scope.params.config.searchOnInit == true) {
        findData();
      }

    }]);
