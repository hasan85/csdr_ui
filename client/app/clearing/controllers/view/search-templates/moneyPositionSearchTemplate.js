'use strict';

angular.module('cmisApp')
  .controller('MoneyPositionSearchTemplateController',
  ['$scope', 'clearingService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', '$rootScope', 'helperFunctionsService', "$filter",
    function ($scope, clearingService, Loader, gettextCatalog, appConstants, SweetAlert, $rootScope, helperFunctionsService, $filter) {

      $scope.gridColumns = [

        {
          field: "account.name",
          title: gettextCatalog.getString("Client")
        },

        {
          field: "currency.code",
          title: gettextCatalog.getString("Currency")
        },
        {
          field: "amount",
          title: gettextCatalog.getString("Amount"),
          template: function(dataItem) {
            return dataItem.amount ? $filter("formatNumber")(dataItem.amount) : "";
          }
        },
        {
          field: "orderBlockAmount",
          title: gettextCatalog.getString("Order Block"),

        }
      ];


      var findData = function () {

        if ($scope.moneyPosition.searchResultGrid !== undefined) {
          $scope.moneyPosition.searchResultGrid.refresh();
        }

        $scope.moneyPosition.searchResult.isEmpty = false;

        Loader.show(true);
        clearingService.getTradingMemberWithClientMoneyPositions().then(function (data) {
          if (data.success === "true") {
            $scope.gridData = {'data': data.data.childAccountMoneyPositions, pageSize: 10};
            $scope.moneyPosition.searchResult.data = data.data;
          } else {
            $scope.moneyPosition.searchResult.isEmpty = true;
            SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
          }
          Loader.show(false);
        });
      };

      var _params = {
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      var _moneyPosition = {
        formName: 'moneyPositionFindDorm',
        form: {},
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedMoneyPositionIndex: false,
        findData: findData
      };

      $scope.moneyPosition = angular.merge($scope.searchConfig.moneyPosition, _moneyPosition);

      $scope.findData = findData;

      $scope.$on('refreshGrid', function () {
        findData();
      });

      if ($scope.params.config.searchOnInit == true) {
        findData();
      }

    }]);
