'use strict';

angular.module('cmisApp')
  .controller('ViewMemberAdvancesSearchTemplateController',
  ['$scope', 'clearingService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', '$rootScope', 'helperFunctionsService', '$http',
    function ($scope, clearingService, Loader, gettextCatalog, appConstants, SweetAlert, $rootScope, helperFunctionsService, $http) {

      $scope.gridColumns = [
        //{
        //  field: "paymentNumber",
        //  title: gettextCatalog.getString("Payment Number"),
        //  width: '50%'
        //},
        //{
        //  field: "paymentDate",
        //  title: gettextCatalog.getString("Payment Date"),
        //  width: '50%',
        //  format: "{0:dd-MMMM-yyyy HH:mm }"
        //},
        {
          field: "member.name",
          title: gettextCatalog.getString("Name"),
          width: '50%'
        },
        //{
        //  field: "correspondentAccount.accountNumber",
        //  title: gettextCatalog.getString("Correspondent Account"),
        //  width: '50%'
        //},
        {
          field: "advance",
          title: gettextCatalog.getString("Advance"),
          width: '50%'
        }
      ];

      var findData = function () {
        if ($scope.memberAdvance.searchResultGrid !== undefined) {
          $scope.memberAdvance.searchResultGrid.refresh();
        }
        $scope.memberAdvance.searchResult.isEmpty = false;
        Loader.show(true);

        $http.post('/api/clearing/getMemberAdvances', {}).then(function (result) {
          if (result.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);

            var data = result.data;
            if (data.success === "true") {
              $scope.gridData = {
                'data': data.data, pageSize: 10, schema: {
                  model: {
                    fields: {
                      paymentDate: {type: "date"},
                    }
                  }
                }
              };
              $scope.memberAdvance.searchResult.data = data.data;

            } else {
              $scope.memberAdvance.searchResult.isEmpty = true;
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
            Loader.show(false);
          }
        });

      };

      var _params = {
        showSearchCriteriaBlock: false,
        config: {
          searchOnInit: false
        },
        labels: {
          lblClients: gettextCatalog.getString('Clients'),
          lblmemberAdvance: gettextCatalog.getString('Money Positions')
        }
      };

      $scope.params = angular.merge(_params, $scope.searchConfig.params);

      var _memberAdvance = {
        formName: 'memberAdvanceFindDorm',
        form: {},
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedmemberAdvanceIndex: false,
        findData: findData
      };

      $scope.memberAdvance = angular.merge($scope.searchConfig.memberAdvance, _memberAdvance);

      $scope.findData = findData;

      $scope.$on('refreshGrid', function () {
        findData();
      });

      if ($scope.params.config.searchOnInit == true) {
        findData();
      }

    }]);
