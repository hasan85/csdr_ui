'use strict';

angular.module('cmisApp')
  .factory('clearingService',
  ["$http", "$q", "$kWindow", 'gettextCatalog', 'helperFunctionsService',
    function ($http, $q, $kWindow, gettextCatalog, helperFunctionsService) {

      var deferred = $q.defer();

      var getBankStatementsByUrl = function (url, criteria) {
        return $http.post(url, {criteria: criteria}).then(function (result) {

          if (result.data && result.data.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
      };

      var getClearingLimitsByUrl = function (url, criteria) {
        return $http.post(url, {criteria: criteria}).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
      };
      var getMoneyPositionsByUrl = function (url, criteria) {
        return $http.post(url, {criteria: criteria}).then(function (result) {
          if (result.data && result.data.data) {

            if (result.data.data) {
              result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
              result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
              if (result.data.data.childAccounts) {
                for (var i = 0; i < result.data.data.childAccounts.length; i++) {
                  result.data.data.childAccounts[i].moneyPositions = helperFunctionsService.convertObjectToArray(
                    result.data.data.childAccounts[i].moneyPositions);
                }
              }
            }

          }
          return result.data;
        })
      };
      var getSecurityPositionsByUrl = function (url, criteria) {
        return $http.post(url, {criteria: criteria}).then(function (result) {
          if (result.data && result.data.data) {

            if (result.data.data) {
              result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
              result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
              if (result.data.data.childAccounts) {
                for (var i = 0; i < result.data.data.childAccounts.length; i++) {
                  result.data.data.childAccounts[i].securityPositions = helperFunctionsService.convertObjectToArray(
                    result.data.data.childAccounts[i].securityPositions);
                }
              }
            }
          }
          return result.data;
        })
      };

      var getOrdersByUrl = function (url, criteria) {
        return $http.post(url, {criteria: criteria}).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        })
      };

      var findBankStatement = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Pending Bank Statements'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/bank-statement-find.html',
          controller: 'BankStatementFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };




      var selectBankStatement = function (data) {
        // Select Bank Statement, resolve promise
        deferred.resolve(data);
        deferred = $q.defer();
      };

      var getAccountPositions = function (account) {
        return $http.post('/api/clearing/getAccountPositions/', {account: account}).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
            result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
            result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
          }
          return result.data;
        })
      };
        var _getAccountPositions = function (account) {
            return $http.post('/api/reportsDataServices/getAccountPositions/', {account: account}).then(function (result) {
                if (result.data && result.data.data) {
                    result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
                    result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
                    result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
                }
                return result.data;
            })
        };

      // var getClientShareholders = function (account) {
      //   return $http.post('/api/clearing/getClientShareholders/', {account: account}).then(function (result) {
      //     if (result.data && result.data.data) {
      //       result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
      //       result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
      //       result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
      //     }
      //     return result.data;
      //   })
      // };

      var getTradingMemberWithClientSecurityPositions = function () {
        return $http.get('/api/clearing/getTradingMemberWithClientSecurityPositions').then(function (result) {
          if (result.data && result.data.data) {
            result.data.data.childAccountSecurityPositions = helperFunctionsService.convertObjectToArray(result.data.data.childAccountSecurityPositions);
            result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
          }
          return result.data;
        })
      };

      var getTradingMemberWithClientMoneyPositions = function (id) {
        id = id || null;
        return $http.get('/api/clearing/getTradingMemberWithClientMoneyPositions/' + id).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
            result.data.data.childAccountMoneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.childAccountMoneyPositions);
          }
          return result.data;
        })
      };

      var getTradingMemberAccount = function (url, formData) {
        return $http.post(url, {criteria: formData}).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
            result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
            result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
          }
          return result.data;
        });
      };
      var getClientShareholders = function (url, formData) {
        return $http.post(url, {criteria: formData}).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
            result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
            result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
          }
          return result.data;
        });
      };

      var getMoneyAccounts = function (url, formData) {
        return $http.post(url, {criteria: formData}).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data.childAccounts = helperFunctionsService.convertObjectToArray(result.data.data.childAccounts);
            result.data.data.moneyPositions = helperFunctionsService.convertObjectToArray(result.data.data.moneyPositions);
            result.data.data.securityPositions = helperFunctionsService.convertObjectToArray(result.data.data.securityPositions);
          }
          return result.data;
        });
      };

      // var getClientShareholders = function (url, formData) {
      //   console.log(formData);
      //   return $http.post(url, {criteria: formData}).then(function (result) {
      //     if (result.data && result.data.data) {
      //       result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
      //     }
      //     return result.data;
      //   });
      // };

      var getDepoMembers = function (url, formData) {
        return $http.post(url, {criteria: formData}).then(function (result) {
          if (result.data && result.data.data) {
            result.data.data = helperFunctionsService.convertObjectToArray(result.data.data);
          }
          return result.data;
        });
      };


      return {
        getBankStatementsByUrl: getBankStatementsByUrl,
        selectBankStatement: selectBankStatement,
        findBankStatement: findBankStatement,
        getClearingLimitsByUrl: getClearingLimitsByUrl,
        getMoneyPositionsByUrl: getMoneyPositionsByUrl,
        getSecurityPositionsByUrl: getSecurityPositionsByUrl,
        getOrdersByUrl: getOrdersByUrl,
        getTradingMemberAccount: getTradingMemberAccount,
        getAccountPositions: getAccountPositions,
        _getAccountPositions: _getAccountPositions,
        getTradingMemberWithClientSecurityPositions: getTradingMemberWithClientSecurityPositions,
        getTradingMemberWithClientMoneyPositions: getTradingMemberWithClientMoneyPositions,
        getClientShareholders:getClientShareholders,
        getMoneyAccounts:getMoneyAccounts,
        getDepoMembers: getDepoMembers
      };

    }]);
