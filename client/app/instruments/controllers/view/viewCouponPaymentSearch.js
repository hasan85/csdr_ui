'use strict';

angular.module('cmisApp')
  .controller('ViewCouponPaymentSearchController',
    ['$scope', 'personFindService', 'Loader', 'gettextCatalog', 'appConstants', 'SweetAlert', 'referenceDataService', '$http', 'helperFunctionsService', 'clearingService', '$rootScope',
      function ($scope, personFindService, Loader, gettextCatalog, appConstants, SweetAlert, referenceDataService, $http, helperFunctionsService, clearingService, $rootScope) {

        var validateForm = function (formData) {

          var result = {
            success: false,
            message: gettextCatalog.getString('You have to fill at least one input field!')
          };
          result.success = true;
          // }
          return result;
        };

        var personSelected = function (data) {

          var sResult = angular.copy($scope.couponPayment.searchResult.data.data);

          for (var i = 0; i < sResult.length; i++) {

            if (sResult[i].id == data.id) {
              $scope.couponPayment.selectedPersonIndex = i;
              break;
            }
          }
        };

        $scope.personSelected = personSelected;

        var _params = {
          searchUrl: "/api/instruments/getCouponPaymentInstruments",
          showSearchCriteriaBlock: false,
          config: {
            criteriaForm: {
              personType: {
                isVisible: true,
                default: appConstants.personClasses.juridicalPerson
              },
              accountNumber: {
                isVisible: false
              }
            },
            searchOnInit: false
          },
          criteriaEnabled: true
        };

        $scope.params = angular.merge(_params, $scope.searchConfig.params);


        $scope.params.config.splitterPanesConfig = [
          {
            size: '30%',
            collapsible: true,
            collapsed: !$scope.params.showSearchCriteriaBlock
          },
          {
            size: '70%',
            collapsible: false
          }
        ];

        var _person = {
          formName: 'personFindForm',
          form: {},
          data: {
            personClasses: [],
            personClassCodes: {
              natural_person: appConstants.personClasses.naturalPerson,
              juridical_person: appConstants.personClasses.juridicalPerson
            }
          },

          searchResult: {
            data: null,
            isEmpty: false
          },
          selectedPersonType: $scope.params.config.criteriaForm.personType.default,
          selectedPersonIndex: false,
          personSelected: personSelected,
          personDetails: false
        };

        $scope.couponPayment = angular.merge($scope.searchConfig.person, _person);


        var findPerson = function (e) {

          if ($scope.couponPayment.searchResultGrid !== undefined) {
            $scope.couponPayment.searchResultGrid.refresh();
          }

          $scope.couponPayment.selectedPersonIndex = false;

          if ($scope.couponPayment.formName.$dirty) {
            $scope.couponPayment.formName.$submitted = true;
          }

          if ($scope.couponPayment.formName.$valid || !$scope.couponPayment.formName.$dirty) {

            $scope.couponPayment.searchResult.isEmpty = false;

            var formData = angular.copy($scope.couponPayment.form);

            if (formData.idDocumentClass == "") {
              formData.idDocumentClass = null;
            }
            if ($scope.params.config.searchCriteria) {
              formData = angular.merge(formData, $scope.params.config.searchCriteria);
            }


            if (formData.maturityEndDate) {
              formData.maturityEndDate = helperFunctionsService.generateDateTime(formData.maturityEndDateObj)
              delete  formData.maturityEndDateObj
            }

            if (formData.maturityStartDate) {
              formData.maturityStartDate = helperFunctionsService.generateDateTime(formData.maturityStartDateObj)
              delete  formData.maturityStartDateObj
            }


            var requestData = angular.extend(formData, {
              skip: e.data.skip,
              take: e.data.take,
              sort: e.data.sort
            });


            Loader.show(true);
            $http({
              method: 'POST',
              url: $scope.params.searchUrl,
              data: {data: requestData}
            }).success(function (data, status, headers, config) {
              if (data) {
                $scope.couponPayment.searchResult.data = data;
              } else {
                $scope.couponPayment.searchResult.isEmpty = true;
              }

              if (data['success'] === "true") {
                data.data = helperFunctionsService.convertObjectToArray(data.data);

                e.success({Data: data.data ? data.data : [], Total: data.total});
              } else {
                SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
              }

              Loader.show(false);
            });
          }
        };

        Loader.show(true);

        personFindService.getMetaDataForPersonSearch().then(function () {


          $scope.couponPayment.mainGridOptions = {
            dataSource: {
              schema: {
                data: "Data",
                total: "Total",
                model: {
                  fields: {
                    maturityDate: {type: "date"}
                  }
                }
              },
              transport: {
                read: function (e) {
                  findPerson(e);
                }
              },

              serverPaging: true,
              serverSorting: true
            },
            selectable: true,
            scrollable: true,
            pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
            sortable: true,
            resizable: true,
            columns: [
              {
                field: "ISIN",
                title: gettextCatalog.getString("ISIN"),
                width: '17%'
              },
              {
                field: "issuerName",
                title: gettextCatalog.getString("Issuer Name"),
                width: '20%'
              },
              {
                field: "parValue",
                title: gettextCatalog.getString("Par Value"),
                width: '6%'
              },

              {
                field: "couponRate",
                title: gettextCatalog.getString("Coupon Rate"),
                width: '6%'
              },
              {
                field: "currency",
                title: gettextCatalog.getString("Currency"),
                width: '6%'
              },
              {
                field: "quantity",
                title: gettextCatalog.getString("Quantity"),
                width: '6%'
              },
              {
                field: "CFI.name" + $rootScope.lnC,
                title: gettextCatalog.getString("CFI"),
                width: '15%'
              },
              {
                field: "maturityDate",
                title: gettextCatalog.getString("Maturity Date"),
                width: '10%',
                format: "{0:dd-MMMM-yyyy}"
              }
            ]
          };


          Loader.show(false);
        });

        $scope.personSelected = personSelected;

        $scope.showPaymentDetails = function () {

          Loader.show(true);
          personFindService.findPersonById($scope.searchConfig.person.searchResult.data.data[
            $scope.searchConfig.person.selectedPersonIndex]['id']).then(function (data) {
            $scope.couponPayment.personDetails = data;
            Loader.show(false);
          });

        };

        $scope.closePersonDetails = function () {
          $scope.couponPayment.personDetails = false;
        };

        $scope.changePersonType = function () {

          var personClasses = angular.copy($scope.couponPayment.data.personClasses);
          for (var i = 0; i < personClasses.length; i++) {
            if (personClasses[i].id == $scope.couponPayment.form.personClassId) {
              $scope.couponPayment.selectedPersonType = personClasses[i].code;
            }
          }
          $scope.resetForm();
        };

        $scope.toggleSearchCriteriaBlock = function (splitter) {
          if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
          } else {
            splitter.collapse(".k-pane:first");
          }
          $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
        };

        $scope.searchCriteriaCollapse = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = false;
          });
        };

        $scope.searchCriteriaExpand = function () {
          $scope.$apply(function () {
            $scope.params.showSearchCriteriaBlock = true;
          });
        };


        $scope.resetForm = function () {

          $scope.couponPayment.form.name = null;
          $scope.couponPayment.form.fullName = null;
          $scope.couponPayment.form.firstName = null;
          $scope.couponPayment.form.uniqueCode = null;
          $scope.couponPayment.form.middleName = null;
          $scope.couponPayment.form.lastName = null;
          $scope.couponPayment.form.plainAddress = null;
          $scope.couponPayment.form.accountNumber = null;
          $scope.couponPayment.form.idDocumentSeries = null;
          $scope.couponPayment.form.idDocumentClass = "";
          $scope.couponPayment.form.idDocumentNumber = null;
          $scope.couponPayment.form.isSuspended = false;
          $scope.couponPayment.form.isDeleted = false;

        };

        $scope.detailInit = function (e) {
          //Loader.show(true);

        };

        $scope.paymentdetailGrid = function (dataItem) {

          return {
            dataSource: {
              transport: {
                read: function (e) {
                  Loader.show(true);
                  $http({
                    method: 'POST',
                    url: '/api/instruments/getVMCouponPaymentsByInstrument/',
                    data: {instrumentId: dataItem.id}
                  }).success(function (data) {

                    if (data['success'] === "true") {
                      data.data = helperFunctionsService.convertObjectToArray(data.data);

                      e.success({data: data.data ? data.data : [], total: data.data.length});
                    } else {
                      SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                    }

                    Loader.show(false);
                  });
                }
              },
              pageSize: 50,
              schema: {
                total: "total",
                data: "data",
                model: {
                  fields: {
                    effectiveDate: {type: "date"},
                    recordDate: {type: "date"},
                    valueDate: {type: "date"}
                  }
                }
              }
            },
            sortable: {
              mode: "single"
            },
            // toolbar:["excel"],
            scrollable: true,
            pageable: true,
            filterable:true,
            columns: [
              {
                field: "recordDate",
                title: 'Reyestr tarixi',
                width: "10rem",
                format: "{0:dd-MMMM-yyyy HH:mm}"
              },
              {
                field: "valueDate",
                title: gettextCatalog.getString("Payment Date"),
                width: "10rem",
                format: "{0:dd-MMMM-yyyy HH:mm}"
              },
              {
                field: "interestAmount",
                title: "Faiz məbləği",
                width: "10rem"
              },
              {
                field: "principalAmount",
                title: "Əsas məbləği",
                width: "10rem"
              },
              {
                field: "status.name" + $rootScope.lnC,
                title: gettextCatalog.getString("Status"),
                width: "10rem"
              }
            ]
          };
        };


        $scope.paymentRecoddetailGrid = function (dataItem) {
          return {
            // dataBound: function () {
            //   var grid = $scope.paymentRecoddetailGridInstance
            //   for (var i = 0; i < grid.columns.length; i++) {
            //     grid.autoFitColumn(i);
            //   }
            // },
            dataSource: {
              transport: {
                read: function (e) {
                  Loader.show(true);
                  $http({
                    method: 'POST',
                    url: '/api/instruments/getCouponPaymentRecords/',
                    data: {couponPaymentID: dataItem.id}
                  }).success(function (data) {

                    if (data['success'] === "true" && data.data) {
                      data.data = helperFunctionsService.convertObjectToArray(data.data);

                      e.success({data: data.data ? data.data : [], total: data.data.length});
                    } else {
                      // SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                    }

                    Loader.show(false);
                  });
                }
              },
              pageSize: 50,
              schema: {
                total: "total",
                data: "data",
                model: {}
              }
            },
            sortable: {
              mode: "single"
            },
            excel: {
              allPages: true
            },
            // toolbar:["excel"],
            pageable: true,
            filterable:true,
            columns: [
              {
                field: "personName",
                title: gettextCatalog.getString("Person Name"),
                width: "10rem"
              },
              {
                field: "accountNumber",
                title: gettextCatalog.getString("Account Number"),
                width: "10rem"
              },
              {
                field: "amount",
                title: gettextCatalog.getString("Amount"),
                width: "10rem"
              },
              {
                field: "tax",
                title: gettextCatalog.getString("Vergi"),
                width: "10rem"
              },
              {
                field: "status",
                template:  "#= data.status ? data.status.nameAz: ''  #",
                title: gettextCatalog.getString("Status"),
                width: "10rem"
              },
              {
                field: "comment",
                title: gettextCatalog.getString("Comment"),
                width: "20rem"
              }

            ]
          };
        };


        $scope.searchCouponPayment = function () {

          var formValidationResult = validateForm(angular.copy($scope.couponPayment.form));
          if (formValidationResult.success) {
            if ($scope.couponPayment.searchResultGrid) {
              $scope.couponPayment.searchResultGrid.dataSource.page(1);
            }
          }
          else {
            SweetAlert.swal("", formValidationResult.message, "error");
          }

        };

        $scope.$on("kendoWidgetCreated", function (event, widget) {
          if (widget === $scope.couponPayment.searchResultGrid) {
            $scope.couponPayment.searchResultGrid = widget;
          }
        });

      }]);

