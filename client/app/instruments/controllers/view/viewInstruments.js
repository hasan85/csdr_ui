'use strict';

angular.module('cmisApp')
  .controller('ViewInstrumentsController', [
    '$scope', 'instrumentFindService', 'Loader', 'gettextCatalog', '$windowInstance', 'id',
    'referenceDataService', 'SweetAlert', 'helperFunctionsService', '$http','$rootScope', "$filter",
    function ($scope, instrumentFindService, Loader, gettextCatalog, $windowInstance, id,
              referenceDataService, SweetAlert, helperFunctionsService, $http, $rootScope, $filter) {

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        //  if (formData.isin || formData.issuerName || formData.currencyID || formData.isInBlackList || formData.isInDebtorsList || formData.searchByRejection) {
        result.success = true;
        //  }
        return result;
      };
      $scope.metaData = {};
      Loader.show(true);
      referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;
        referenceDataService.addEmptyOption([
          $scope.metaData.currencies
        ]);
        Loader.show(false);
      });
      $scope.instrument = {
        formName: 'instrumentFindForm',
        form: {
          searchByRejection: false
        },
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedInstrumentIndex: false
      };
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view: {
            title: gettextCatalog.getString('View'),
            disabled: true,
            click: function () {
              $scope.$broadcast('showPersonDetails');
            }
          },
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        }
      };

      var paramsLocal = {
        config: {
          issuer: {
            isVisible: true
          },
          ISIN: {
            isVisible: true
          }
        },
        params: {
          issuerId: null,
          ISIN: null
        },
        showSearchCriteriaBlock: false,
        searchUrl: 'findInstrument/',
        searchOnInit: false,
      };
      $scope.params = paramsLocal;
      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: true
        },
        {
          size: '70%',
          collapsible: false
        }
      ];

      var findInstrument = function (e) {
        if ($scope.instrument.searchResultGrid !== undefined) {
          $scope.instrument.searchResultGrid.refresh();
        }

        if (!$scope.instrument.form) {
          $scope.instrument.form = {
            isSubscription: null
          }
        } else {
          if ($scope.instrument.form.isSubscription === undefined || $scope.instrument.form.isSubscription === null) {
            $scope.instrument.form.isSubscription = null;
          }
        }
        if (!$scope.instrument.form) {
          $scope.instrument.form = {
            commissionFree: null
          }
        } else {
          if ($scope.instrument.form.commissionFree === undefined || $scope.instrument.form.commissionFree === null) {
            $scope.instrument.form.commissionFree = null;
          }
        }
        if (!$scope.instrument.form) {
          $scope.instrument.form = {
            isIssue: null
          }
        } else {
          if ($scope.instrument.form.isIssue === undefined || $scope.instrument.form.isIssue === null) {
            $scope.instrument.form.isIssue = null;
          }
        }

        if ($scope.params.params.issuerId) {
          $scope.instrument.form.issuerId = $scope.params.params.issuerId;
        }
        if ($scope.params.params.ISIN) {
          $scope.instrument.form.ISIN = $scope.params.params.ISIN;
        }

        $scope.instrument.selectedInstrumentIndex = false;

        if ($scope.instrument.formName.$dirty) {
          $scope.instrument.formName.$submitted = true;
        }

        if (!$scope.instrument.form.searchByRejection) {

          $scope.instrument.form.searchByDebtorsList = false;
          $scope.instrument.form.isInDebtorsList = null;

        }
        else {

          $scope.instrument.form.searchByDebtorsList = true;

          if ($scope.instrument.form.isInDebtorsList === null) {
            $scope.instrument.form.isInDebtorsList = false;
          }
          //if ($scope.instrument.form.isInBlackList === null) {
          //  $scope.instrument.form.isInBlackList = false;
          //}
        }

        $scope.instrument.searchResult.isEmpty = false;
        $scope.instrument.form.searchByBlacklist = true;
        $scope.instrument.form.isInBlackList = false;
        var requestData = angular.extend(angular.copy($scope.instrument.form), {
          skip: e.data.skip,
          take: e.data.take,
          sort: e.data.sort
        });

        Loader.show(true);
        $http({method: 'POST', url: "/api/common/" + $scope.params.searchUrl, data: {data: requestData}}).
          success(function (data, status, headers, config) {
            if (data) {
              $scope.instrument.searchResult.data = data;
            } else {
              $scope.instrument.searchResult.isEmpty = true;
            }
            if (data['success'] === "true") {
              data.data = helperFunctionsService.convertObjectToArray(data.data);
              e.success({Data: data.data ? data.data : [], Total: data.total});
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
            Loader.show(false);
          });
      };

      $scope.findInstrument = function () {

        var formValidationResult = validateForm(angular.copy($scope.instrument.form));
        if (formValidationResult.success) {
          if ($scope.instrument.searchResultGrid) {
            $scope.instrument.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };
      if ($scope.params.searchOnInit) {
        findInstrument();
      }

      $scope.selectInstrument = function () {
        instrumentFindService.selectInstrument($scope.instrument.searchResult.data[$scope.instrument.selectedInstrumentIndex]);
        $windowInstance.close();
      };

      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.instrument.instrumentSelected = function (data) {

        var sResult = angular.copy($scope.instrument.searchResult.data.data);

        for (var i = 0; i < sResult.length; i++) {

          if (sResult[i].id == data.id) {

            $scope.instrument.selectedInstrumentIndex = i;

            break;
          }
        }
      };

      $scope.searchCriteriaCollapse = function () {

        $scope.$apply(function () {

          $scope.params.showSearchCriteriaBlock = false;
        });
      };

      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {

          $scope.params.showSearchCriteriaBlock = true;
        });
      };

      $scope.resetForm = function () {
        $scope.instrument.form = {
          isin: null,
          issuerName: null,
          currencyID: '',
          isInDebtorsList: null,
          searchByRejection: false,
          isIssue: false,
          isSubscription: false,
          commissionFree: false

        };
      };

      $scope.instrument.form.currencyID = '';


      $scope.instrument.mainGridOptions = {
        excel: {
          allPages: true
        },
        rowTemplate: function(dataItem) {
          return '<tr class="'+(dataItem.isInDebtorsList == 'true' ? 'debtors-list-color' : '')+'" data-uid="'+dataItem.uid+'">' +
            '<td>'+dataItem.instrumentName+'</td>' +
            '<td>'+dataItem.isin+'</td>' +
            '<td>'+dataItem.issuerName+'</td>' +
            '<td>'+dataItem.parValue+'</td>' +
            '<td>'+dataItem.currency+'</td>' +
            '<td>'+$filter("formatNumber")(dataItem.quantity, false)+'</td>' +
            '<td>'+dataItem.CFI.nameAz+'</td>' +
            '<td align="center"><input type="checkbox" value="'+dataItem.isIssue+'" disabled '+(dataItem.isIssue == 'true' ? 'checked' : '')+'></td>' +
            '<td align="center"><input type="checkbox" value="'+dataItem.isSubscription+'" disabled '+(dataItem.isSubscription == 'true' ? 'checked' : '')+'></td>' +
            '<td align="center"><input type="checkbox" value="'+dataItem.commissionFree+'" disabled '+(dataItem.commissionFree == 'true' ? 'checked' : '')+'></td>' +
            '<td>' + $filter('shortDate')(dataItem.maturityDate) + '</td>' +
            '</tr>';
        },
        dataSource: {
          schema: {
            data: "Data",
            total: "Total"
          },
          transport: {
            read: function (e) {
              findInstrument(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "instrumentName",
            title: gettextCatalog.getString("Name")
          },
          {
            field: "isin",
            title: gettextCatalog.getString("ISIN")
          },
          {
            field: "issuerName",
            title: gettextCatalog.getString("Issuer")
          },
          {
            field: "parValue",
            title: gettextCatalog.getString("Par Value")
          },
          {
            field: "currency",
            title: gettextCatalog.getString("Currency")
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity")
          },
          {
            field: "CFI",
            title: gettextCatalog.getString("CFI")
          },
          {
            field: "isIssue",
            title: gettextCatalog.getString("Is Issue")
          },
          {
            field: "isSubscription",
            title: gettextCatalog.getString("Is Subscription")
          },
          {
            field: "commissionFree",
            title: gettextCatalog.getString("Commission Free")
          },
          {
            field: "maturityDate",
            title: gettextCatalog.getString("Maturity Date")
          }
        ]
      };

      $scope.showInstrumentDetails = function () {
        Loader.show(true);
        instrumentFindService.findInstrumentById($scope.instrument.searchResult.data.data[
          $scope.instrument.selectedInstrumentIndex]['id']).then(function (data) {
          console.log(data);
          if(!angular.isArray(data.underwriterNames)) {
            data.underwriterNames = [data.underwriterNames];
          }
          $scope.instrument.instrumentDetails = data;
          Loader.show(false);
        });
      };

      $scope.closeInstrumentDetails = function () {
        $scope.instrument.instrumentDetails = false;
      };

      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.instrument.searchResultGrid) {
          $scope.instrument.searchResultGrid = widget;
        }
      });
    }]);
