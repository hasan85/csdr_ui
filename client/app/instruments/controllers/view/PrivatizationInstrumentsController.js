'use strict';

angular.module('cmisApp')
  .controller('PrivatizationInstrumentsController', ['$scope', 'instrumentFindService', 'Loader', 'gettextCatalog',
    '$windowInstance', 'referenceDataService', 'SweetAlert', 'helperFunctionsService', '$http', 'id',
    function ($scope, instrumentFindService, Loader, gettextCatalog, $windowInstance, referenceDataService,
              SweetAlert, helperFunctionsService, $http, id) {
      $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
          view: {
            title: gettextCatalog.getString('View'),
            disabled: true,
            click: function () {
              $scope.$broadcast('showPersonDetails');
            }
          },
          print: false,
          refresh: {
            click: function () {
              $scope.$broadcast('refreshGrid');
            }
          }
        }
      };

      var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        if (formData.isin || formData.currencyID || formData.issuerName) {
          result.success = true;
        }
        return result;
      };
      $scope.metaData = {};
      Loader.show(true);
      referenceDataService.getCurrencies().then(function (data) {

        $scope.metaData.currencies = data;

        referenceDataService.addEmptyOption([
          $scope.metaData.currencies
        ]);

        Loader.show(false);
      });


      $scope.instrument = {
        formName: 'instrumentFindForm',
        form: {},
        data: {},
        searchResult: {
          data: null,
          isEmpty: false
        },
        selectedInstrumentIndex: false
      };

      var paramsLocal = {
        config: {
          ISIN: {
            isVisible: true
          }
        },
        params: {
          issuerId: null,
          ISIN: null
        },
        showSearchCriteriaBlock: false,
        searchUrl: 'searchPrivatizationInstruments/',
        searchOnInit: true,
      };

      $scope.params = angular.merge(paramsLocal, {});

      $scope.params.config.splitterPanesConfig = [
        {
          size: '30%',
          collapsible: true,
          collapsed: false
        },
        {
          size: '70%',
          collapsible: false
        }
      ];


      var findInstrument = function (e) {
        if ($scope.instrument.searchResultGrid !== undefined) {
          $scope.instrument.searchResultGrid.refresh();
        }

        if ($scope.params.params.issuerId) {
          $scope.instrument.form.issuerId = $scope.params.params.issuerId;
        }
        if ($scope.params.params.ISIN) {
          $scope.instrument.form.ISIN = $scope.params.params.ISIN;
        }

        $scope.instrument.selectedInstrumentIndex = false;

        if ($scope.instrument.formName.$dirty) {
          $scope.instrument.formName.$submitted = true;
        }

        $scope.instrument.searchResult.isEmpty = false;

        var requestData = angular.extend(angular.copy($scope.instrument.form), {
          skip: e.data.skip,
          take: e.data.take,
          sort: e.data.sort
        });

        Loader.show(true);
        $http({method: 'POST', url: "/api/instruments/" + $scope.params.searchUrl, data: {data: requestData}}).
          success(function (data, status, headers, config) {
            if (data) {
              $scope.instrument.searchResult.data = data;
            } else {
              $scope.instrument.searchResult.isEmpty = true;
            }
            if (data['success'] === "true") {
              data.data = helperFunctionsService.convertObjectToArray(data.data);

              e.success({Data: data.data ? data.data : [], Total: data.total});
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
            Loader.show(false);
          });
      };

      $scope.findInstrument = function () {

        var formValidationResult = validateForm(angular.copy($scope.instrument.form));
        if (formValidationResult.success) {
          if ($scope.instrument.searchResultGrid) {
            $scope.instrument.searchResultGrid.dataSource.page(1);
          }
        }
        else {
          SweetAlert.swal("", formValidationResult.message, "error");
        }

      };


      $scope.selectInstrument = function () {
        instrumentFindService.selectInstrument($scope.instrument.searchResult.data.data[$scope.instrument.selectedInstrumentIndex]);
        $windowInstance.close();
      };

      $scope.instrument.instrumentSelected = function (data) {

        var sResult = angular.copy($scope.instrument.searchResult.data.data);

        for (var i = 0; i < sResult.length; i++) {

          if (sResult[i].id == data.id) {
            $scope.instrument.selectedInstrumentIndex = i;
            break;
          }
        }
      };

      $scope.searchCriteriaCollapse = function () {

        $scope.$apply(function () {

          $scope.params.showSearchCriteriaBlock = false;
        });
      };
      $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {

          $scope.params.showSearchCriteriaBlock = true;
        });
      };
      $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
          splitter.expand(".k-pane:first");
        } else {
          splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
      };

      $scope.closeWindow = function () {
        $windowInstance.close();
      };

      $scope.resetForm = function () {
        $scope.instrument.form = {
          isin: null,
          currencyID: null
        };
      };

      $scope.instrument.form.currencyID = '';
      $scope.instrument.mainGridOptions = {
        dataSource: {
          schema: {
            data: "Data",
            total: "Total"
          },
          transport: {
            read: function (e) {
              findInstrument(e);
            }
          },

          serverPaging: true,
          serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
          {
            field: "issuerName",
            title: gettextCatalog.getString("Issuer")
          },
          {
            field: "isin",
            title: gettextCatalog.getString("ISIN")
          },
          {
            field: "parValue",
            title: gettextCatalog.getString("Par Value")
          },
          {
            field: "currency",
            title: gettextCatalog.getString("Currency")
          },
          {
            field: "quantity",
            title: gettextCatalog.getString("Quantity")
          }
        ]
      };

      $scope.balanceGrid = function (dataItem) {

        return {
          selectable: true,
          excel: {
            allPages: true
          },
          dataSource: {
            transport: {
              read: function (e) {
                Loader.show(true);
                $http({
                  method: 'POST',
                  url: '/api/reports/getHoldingReport/',
                  data:{
                    data: {
                      instrumentID: dataItem.id
                    }
                  }
                }).success(function (data) {
                  Loader.show(false);
                  if (data['success'] === "true") {
                    data.data = helperFunctionsService.convertObjectToArray(data.data);
                    e.success({Data: data.data ? data.data : [], Total: data.data ? data.data.length : 0});
                  } else {
                    SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                  }
                });
              }
            },
            schema: {
              data: "Data",
              total: "Total"
            },
            sort: {field: "createDate", dir: "desc"},
            pageSize: 10,
          },
          scrollable: true,
          pageable: true,
          columns: [
            {
              field: "account.accountNumber",
              title: gettextCatalog.getString('Account No.'),
              width: "2rem"
            },
            {
              field: "account.name",
              title: gettextCatalog.getString("Person"),
              width: "2rem"
            },
            {
              field: "quantity",
              title: gettextCatalog.getString("Quantity"),
              width: "2rem"
            },

            {
              title: gettextCatalog.getString("Encumberence"),
              columns: [

                {
                  field: "pledgeQuantity",
                  width: "2rem",
                  title: gettextCatalog.getString("Pledged"),
                },
                {
                  field: "repoQuantity",
                  title: gettextCatalog.getString("Repo"),
                  width: "2rem"
                },
              ]
            },
            {
              field: "frozenQuantity",
              title: gettextCatalog.getString("Frozen"),
              width: "2rem"
            }
          ],
        };
      };
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.instrument.searchResultGrid) {
          $scope.instrument.searchResultGrid = widget;
        }
      });
    }]);
