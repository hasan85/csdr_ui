'use strict';

angular.module('cmisApp').controller('ViewInstrumentDataChangeHistoryController', ['$scope', 'instrumentFindService', 'Loader', 'gettextCatalog', '$windowInstance', 'id', 'referenceDataService', 'SweetAlert', 'helperFunctionsService', '$http', function ($scope, instrumentFindService, Loader, gettextCatalog, $windowInstance, id, referenceDataService, SweetAlert, helperFunctionsService, $http) {

    var validateForm = function (formData) {
        var result = {success: false, message: gettextCatalog.getString('You have to fill at least one input field!')};
        result.success = true;
        return result;
    };
    $scope.metaData = {};
    Loader.show(true);
    $scope.instrument = {
        formName: 'instrumentFindForm',
        form: {
            searchByRejection: false
        },
        data: {},
        searchResult: {
            data: null,
            isEmpty: false
        },
        selectedInstrumentIndex: false
    };
    $scope.config = {
        screenId: id,
        window: $windowInstance,
        mainGrid: {},
        buttons: {
            view: {
                title: gettextCatalog.getString('View'),
                disabled: true,
                click: function () {
                    $scope.$broadcast('showPersonDetails');
                }
            },
            print: false,
            refresh: {
                click: function () {
                    $scope.$broadcast('refreshGrid');
                }
            }
        }
    };

    $scope.params = {
        config: {
            issuer: {
                isVisible: true
            },
            ISIN: {
                isVisible: true
            }
        },
        params: {
            issuerId: null,
            ISIN: null
        },
        showSearchCriteriaBlock: false,
        searchUrl: 'getDataChangedInstruments/',
        searchOnInit: false
    };
    $scope.params.config.splitterPanesConfig = [
        {
            size: '30%',
            collapsible: true,
            collapsed: true
        },
        {
            size: '70%',
            collapsible: false
        }
    ];

    referenceDataService.getCurrencies().then(function (data) {
        $scope.metaData.currencies = data;
        referenceDataService.addEmptyOption([
            $scope.metaData.currencies
        ]);
    });
    var getInstrumentDataChangeHistory = function(e) {
        Loader.show(true);
        var selectedInstrument = $scope.instrument.searchResultGrid.dataItem($scope.instrument.searchResultGrid.select());
        $http({method: 'POST', url: "/api/common/getInstrumentDataChangeHistory", data: {
            instrumentID: selectedInstrument.id
        }}).
            success(function (data, status, headers, config) {
                console.log("data");
                if (data['success'] === "true") {
                    data.data = helperFunctionsService.convertObjectToArray(data.data);
                    e.success({Data: data.data ? data.data : [], Total: data.total});
                } else {
                    SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }
                Loader.show(false);
            });
    };
    var findInstrument = function (e) {
        if ($scope.instrument.searchResultGrid !== undefined) {
            $scope.instrument.searchResultGrid.refresh();
        }

        if ($scope.params.params.issuerId) {
            $scope.instrument.form.issuerId = $scope.params.params.issuerId;
        }
        if ($scope.params.params.isin) {
            $scope.instrument.form.isin = $scope.params.params.isin;
        }

        $scope.instrument.selectedInstrumentIndex = false;

        if ($scope.instrument.formName.$dirty) {
            $scope.instrument.formName.$submitted = true;
        }
        if (!$scope.instrument.form.searchByRejection) {

            $scope.instrument.form.searchByDebtorsList = false;
            $scope.instrument.form.isInDebtorsList = null;

        }
        else {

            $scope.instrument.form.searchByDebtorsList = true;

            if ($scope.instrument.form.isInDebtorsList === null) {
                $scope.instrument.form.isInDebtorsList = false;
            }
            //if ($scope.instrument.form.isInBlackList === null) {
            //  $scope.instrument.form.isInBlackList = false;
            //}
        }

        $scope.instrument.searchResult.isEmpty = false;
        $scope.instrument.form.searchByBlacklist = true;
        $scope.instrument.form.isInBlackList = false;
        var requestData = angular.extend(angular.copy($scope.instrument.form), {
            skip: e.data.skip,
            take: e.data.take,
            sort: e.data.sort
        });

        Loader.show(true);
        $http({method: 'POST', url: "/api/common/" + $scope.params.searchUrl, data: {data: requestData}}).
            success(function (data, status, headers, config) {
                if (data) {
                    $scope.instrument.searchResult.data = data;
                } else {
                    $scope.instrument.searchResult.isEmpty = true;
                }
                if (data['success'] === "true") {
                    data.data = helperFunctionsService.convertObjectToArray(data.data);

                    console.log(data.data);
                    e.success({Data: data.data ? data.data : [], Total: data.total});
                } else {
                    SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
                }
                Loader.show(false);
            });
    };

    $scope.findInstrument = function () {

        var formValidationResult = validateForm(angular.copy($scope.instrument.form));
        if (formValidationResult.success) {
            if ($scope.instrument.searchResultGrid) {
                $scope.instrument.searchResultGrid.dataSource.page(1);
            }
        }
        else {
            SweetAlert.swal("", formValidationResult.message, "error");
        }

    };
    if ($scope.params.searchOnInit) {
        findInstrument();
    }

    $scope.selectInstrument = function () {
        instrumentFindService.selectInstrument($scope.instrument.searchResult.data[$scope.instrument.selectedInstrumentIndex]);
        $windowInstance.close();
    };

    $scope.toggleSearchCriteriaBlock = function (splitter) {
        if ($scope.params.showSearchCriteriaBlock === false) {
            splitter.expand(".k-pane:first");
        } else {
            splitter.collapse(".k-pane:first");
        }
        $scope.params.showSearchCriteriaBlock = !$scope.params.showSearchCriteriaBlock;
    };

    $scope.instrument.instrumentSelected = function (data) {

        var sResult = angular.copy($scope.instrument.searchResult.data.data);

        for (var i = 0; i < sResult.length; i++) {

            if (sResult[i].id == data.id) {

                $scope.instrument.selectedInstrumentIndex = i;

                break;
            }
        }
    };

    $scope.searchCriteriaCollapse = function () {

        $scope.$apply(function () {

            $scope.params.showSearchCriteriaBlock = false;
        });
    };

    $scope.searchCriteriaExpand = function () {
        $scope.$apply(function () {

            $scope.params.showSearchCriteriaBlock = true;
        });
    };

    $scope.resetForm = function () {
        $scope.instrument.form = {
            isin: null,
            issuerName: null,
            currencyID: '',
            isInDebtorsList: null,
            searchByRejection: false

        };
    };

    $scope.instrument.form.currencyID = '';


    $scope.instrument.mainGridOptions = {
        excel: {
            allPages: true
        },
        dataSource: {
            schema: {
                data: "Data",
                total: "Total"
            },
            transport: {
                read: function (e) {
                    findInstrument(e);
                }
            },

            serverPaging: true,
            serverSorting: true
        },
        selectable: true,
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
            {
                field: "instrumentName",
                title: gettextCatalog.getString("Name")
            },
            {
                field: "ISIN",
                title: gettextCatalog.getString("ISIN")
            },
            {
                field: "issuerName",
                title: gettextCatalog.getString("Issuer")
            },
            {
                field: "parValue",
                title: gettextCatalog.getString("Par Value")
            },
            {
                field: "currency",
                title: gettextCatalog.getString("Currency")
            },
            {
                field: "quantity",
                title: gettextCatalog.getString("Quantity")
            },
            {
                field: "CFI.name"+$scope.lnC,
                title: gettextCatalog.getString("CFI")
            }
        ]
    };

    $scope.instrument.detailGridOptions = {
        excel: {
            allPages: true
        },
        dataSource: {
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    fields: {
                        startDate: {type: "date"},
                        finishDate: {type: "date"}
                    }
                }
            },
            transport: {
                read: function (e) {
                    getInstrumentDataChangeHistory(e);
                }
            },
            serverPaging: true,
            serverSorting: true
        },
        scrollable: true,
        pageable: {"pageSize": 20, "refresh": true, "pageSizes": true},
        sortable: true,
        resizable: true,
        columns: [
            {
                field: "startDate",
                title: gettextCatalog.getString("Start Date"),
                width: "6rem",
                format: "{0:dd-MMMM-yyyy HH:mm:ss}"
            },
            {
                field: "finishDate",
                title: gettextCatalog.getString("Finish Date"),
                width: "6rem",
                format: "{0:dd-MMMM-yyyy HH:mm:ss}"
            },
            {
                field: "corporateActionClass",
                title: gettextCatalog.getString("Corporate Action Class"),
                width: "10rem"
            },
            {
                field: "corporateActionNumber",
                title: gettextCatalog.getString("Corporate Action Number"),
                width: "10rem"
            },
            {
                field: "ISIN",
                title: gettextCatalog.getString("ISIN"),
                width: "10rem"
            },
            {
                field: "issuerName",
                title: gettextCatalog.getString("Issuer Name"),
                width: "10rem"
            },
            {
                field: "instrumentName",
                title: gettextCatalog.getString("Instrument Name"),
                width: "10rem"
            },
            {
                field: "quantity",
                title: gettextCatalog.getString("Quantity"),
                width: "10rem"
            },
            {
                field: "parValue",
                title: gettextCatalog.getString("Par Value"),
                width: "10rem"
            },
            {
                field: "cfi.name"+$scope.lnC,
                title: gettextCatalog.getString("CFI"),
                width: "10rem"
            },
            {
                field: "hairCut",
                title: gettextCatalog.getString("Hair Cut"),
                width: "10rem"
            }
        ]
    };

    $scope.showInstrumentDetails = function () {
        $scope.instrument.instrumentDetails = true;
    };

    $scope.closeInstrumentDetails = function () {
        $scope.instrument.instrumentDetails = false;
    };

    $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.instrument.searchResultGrid) {
            $scope.instrument.searchResultGrid = widget;
        }
    });
}]);
