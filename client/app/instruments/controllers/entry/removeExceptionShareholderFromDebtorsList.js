'use strict';

angular.module('cmisApp')
  .controller('RemoveExceptionShareholderFromDebtorsListController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'debtorsService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', '$http', '$rootScope',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, debtorsService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $http, $rootScope) {

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {};
        }
        if (draft.instrument === null) {
          draft.instrument = {};
        }
        if (draft.shareholder === null) {
          draft.shareholder = {};
        }
        return draft;
      };
      var prepareFormData = function (formData) {
        return formData;
      };
      $scope.whiteListData = initializeFormData(angular.fromJson(task.draft));
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "addToDebtorsListForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formBuf = prepareFormData(angular.copy($scope.whiteListData));
                $scope.config.completeTask(formBuf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
        selectedShareholderIndex: false,
        labels: {
          lblSelectShareholder: gettextCatalog.getString('Select Shareholder')
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.whiteListData);
      });

      // Search instrument
      $scope.findInstrument = function () {

        debtorsService.findDebtor().then(function (data) {

          $scope.whiteListData.rejectionID = data.ID;
          $scope.whiteListData.instrument.ISIN = data.instrument.ISIN;
          $scope.whiteListData.instrument.id = data.instrument.id;
          $scope.whiteListData.instrument.issuerName = data.instrument.issuerName;
          $scope.whiteListData.instrument.instrumentName = data.instrument.instrumentName;


          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/instruments/getWhiteListForInstrumentRejectionList/',
            data: {
              id: data.ID,
            }
          }).success(function (data) {

            Loader.show(false);
            if (data['success'] === "true") {
              data.data = helperFunctionsService.convertObjectToArray(data.data);
              if (data.data) {
                for (var i = 0; i < data.data.length; i++) {
                  data.data[i].rejectionActionClasses = helperFunctionsService.convertObjectToArray(data.data[i].rejectionActionClasses);

                }
              }
              $scope.whiteListData.whiteList = {
                data: data.data,
                pageSize: 10,
                schema: {
                  model: {
                    fields: {
                      startDate: {type: "date"},
                      finishDate: {type: "date"},
                    }
                  }
                }
              };
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
          });


        });
      };

      $scope.gridData = {
        scrollable: true,
        pageable: true,
        columns: [
          {
            field: "shareholder.name",
            title: gettextCatalog.getString("Shareholder")
          }, {
            field: "shareholder.accountNumber",
            title: gettextCatalog.getString("Account Number")
          },

          {
            field: "startDate",
            title: gettextCatalog.getString("Start Date"),
            type: "date",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "finishDate",
            title: gettextCatalog.getString("Finish Date"),
            type: "date",
            format: "{0:dd-MMMM-yyyy}"
          },
          {
            field: "rejectionActionClasses",
            title: gettextCatalog.getString("Action Classes"),
            template: function (e) {
              var t = "<ul class=\"list-no-style\">";
              if (e.rejectionActionClasses) {
                for (var i = 0; i < e.rejectionActionClasses.length; i++) {
                  if (e.rejectionActionClasses[i].isSelected == true) {
                    t += "<li>  " + e.rejectionActionClasses[i]['actionClass']['name' + $rootScope.lnC] + "</li>";
                  }
                }
              }
              t += "</ul>";
              return t;
            },
          }
        ]
      };

      $scope.shareholderSelected = function (data) {
        var sResult = angular.copy($scope.whiteListData.whiteList.data);
        for (var i = 0; i < sResult.length; i++) {
          if (sResult[i].shareholder.accountId == data.shareholder.accountId) {
            $scope.config.selectedShareholderIndex = i;
            break;
          }
        }
      };

      $scope.selectShareholder = function () {
        var selectedData = $scope.whiteListData.whiteList.data[$scope.config.selectedShareholderIndex];
        $scope.whiteListData.shareholder = selectedData.shareholder;
        $scope.whiteListData.ID = selectedData.ID;
        $scope.config.shareholderSelectionWindow.close();
      };

      $scope.showShareHolderSelectionWindow = function () {
        $scope.config.shareholderSelectionWindow.open();
        $scope.config.shareholderSelectionWindow.center();
      };
      $scope.closeShareHolderSelectionWindow = function () {
        $scope.config.shareholderSelectionWindow.close();
      };
      $scope.$on("kendoWidgetCreated", function (event, widget) {
        if (widget === $scope.config.shareholderSelectionWindow) {
          $scope.config.shareholderSelectionWindow = widget;
        }
      });

    }]);
