'use strict';

angular.module('cmisApp')
  .controller('AddExceptionShareholderToBlackListController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'debtorsService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService',
    'Loader', 'SweetAlert', 'gettextCatalog', '$http', '$rootScope',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, debtorsService, helperFunctionsService, operationState, task, referenceDataService,
              Loader, SweetAlert, gettextCatalog, $http, $rootScope) {

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {};
        }
        if (draft.instrument === null) {
          draft.instrument = {};
        }
        if (draft.shareholder === null) {
          draft.shareholder = {};
        }
        draft.rejectionActionClasses = helperFunctionsService.convertObjectToArray(draft.rejectionActionClasses);
        return draft;
      };
      var prepareFormData = function (formData) {

        if (formData.startDateObj) {
          formData.startDate = helperFunctionsService.generateDateTime(formData.startDateObj);
          //delete formData.startDateObj;
        } else if (formData.startDate) {
          formData.startDate = helperFunctionsService.generateDateTime(formData.startDate);
        }
        if (formData.finishDateObj) {
          formData.finishDate = helperFunctionsService.generateDateTime(formData.finishDateObj);
          delete formData.finishDateObj;
        } else if (formData.finishDate) {
          formData.finishDate = helperFunctionsService.generateDateTime(formData.finishDate);
        } else {
          formData.finishDate = null;
        }

        return formData;
      };
      $scope.whiteListData = initializeFormData(angular.fromJson(task.draft));
      debtorsService.normalizeRejectionRecordTask($scope.whiteListData, {
        rejectionActionClasses: $scope.whiteListData.rejectionActionClasses
      });
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "addToDebtorsListForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formBuf = prepareFormData(angular.copy($scope.whiteListData));
                $scope.config.completeTask(formBuf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.whiteListData);
      });

      // Search instrument
      $scope.findInstrument = function () {
        debtorsService.findBlackList().then(function (data) {
          $scope.whiteListData.rejectionID = data.ID;
          $scope.whiteListData.instrument.ISIN = data.instrument.ISIN;
          $scope.whiteListData.instrument.id = data.instrument.id;
          $scope.whiteListData.instrument.issuerName = data.instrument.issuerName;
          $scope.whiteListData.instrument.instrumentName = data.instrument.instrumentName;
        });
      };
      $scope.findShareholder = function () {

        $scope.whiteListData.shareholder = {};

        personFindService.findShareholder({
          isSuspended: false
        }).then(function (person) {

          $scope.whiteListData.shareholder.name = person.name;
          $scope.whiteListData.shareholder.accountId = person.accountId;
          $scope.whiteListData.shareholder.id = person.id;
          $scope.whiteListData.shareholder.accountNumber = person.accountNumber;

          Loader.show(true);
          $http({
            method: 'POST',
            url: '/api/instruments/getShareholderInWhiteList/',
            data: {
              rejectionId: $scope.whiteListData.rejectionID,
              shareholderAccountId: person.accountId,
            }
          }).success(function (data) {
            Loader.show(false);
            if (data['success'] === "true") {
              if (data.data) {
                data.data.rejectionActionClasses = helperFunctionsService.convertObjectToArray(data.data.rejectionActionClasses);

                for (var i = 0; i < data.data.rejectionActionClasses.length; i++) {
                  var obj = data.data.rejectionActionClasses[i];
                  if (obj.isSelected === "true") {
                    obj.isSelected = true;
                  }
                  if (obj.isSelected === "false") {
                    obj.isSelected = false;
                  }
                }
                $scope.whiteListData.ID = data.data.ID;
                $scope.whiteListData.startDate = data.data.startDate;
                $scope.whiteListData.finishDate = data.data.finishDate;
                $scope.whiteListData.rejectionActionClasses = data.data.rejectionActionClasses;
              }
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(data), 'error');
            }
          });
        });
      };
    }]);
