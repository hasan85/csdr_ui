'use strict';

angular.module('cmisApp')
  .controller('AddInstrumentToDebtorsListController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'debtorsService',
    'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, debtorsService,
              referenceDataService, Loader, SweetAlert, gettextCatalog) {

      var operationKeys = {
        blackListEntry: "add_instrument_to_black_list_entry",
        debtorsListEntry: "add_instrument_to_debtors_list_entry"
      };

      var initializeFormData = function (draft) {
        if (!draft) {
          draft = {};
        }
        if (!draft.issuer || draft.issuer === null) {
          draft.issuer = {};
        }
        return draft;
      };
      var prepareFormData = function (formData) {
        if (formData.startDateObj) {
          formData.startDate = helperFunctionsService.generateDateTime(formData.startDateObj);
          delete formData.startDateObj;
        }
        if (formData.finishDateObj) {
          formData.finishDate = helperFunctionsService.generateDateTime(formData.finishDateObj);
          delete formData.finishDateObj;
        } else {
          formData.finishDate = null;
        }
        return formData;
      };

      $scope.debtorsData = initializeFormData(angular.fromJson(task.draft));

      debtorsService.normalizeRejectionRecordTask($scope.debtorsData, {
        rejectionActionClasses: $scope.debtorsData.rejectionActionClasses
      });
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "addToDebtorsListForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formBuf = prepareFormData(angular.copy($scope.debtorsData));
                $scope.config.completeTask(formBuf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.debtorsData);
      });

      // Search issuer
      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {

          $scope.debtorsData.issuer.name = data.name;
          $scope.debtorsData.issuer.accountId = data.accountId;
          $scope.debtorsData.issuer.id = data.id;
          $scope.debtorsData.issuer.accountNumber = data.accountNumber;

          Loader.show(true);

          var criteria = {};
          if (operationKeys.blackListEntry === task.key) {
            criteria = {
              issuerId: data.id,
              searchByDebtorsList: false,
              searchByBlacklist: true,
              isInBlackList: false
            }
          }
          else if (operationKeys.debtorsListEntry === task.key) {
            criteria = {
              issuerId: data.id,
              searchByBlacklist: false,
              searchByDebtorsList: true,
              isInDebtorsList: false
            }
          }
          instrumentFindService.findInstrumentByIssuer(criteria).then(function (instrumentSearchResult) {

            if (instrumentSearchResult.success == "true") {
              if (instrumentSearchResult.data) {
                $scope.debtorsData.instruments = instrumentSearchResult.data.map(function (instrument) {
                  return {
                    instrument: {
                      id: instrument.id,
                      instrumentName: instrument.instrumentName,
                      ISIN: instrument.isin
                    },
                    isInRejectionList: true
                  }
                });
              }

            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(instrumentSearchResult), 'error');
            }

            Loader.show(false);


          });

        });
      };
    }]);
