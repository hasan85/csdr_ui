'use strict';

angular.module('cmisApp')
  .controller('RemoveInstrumentFromBlackListController',
  ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',
    'personFindService', 'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', '$http', '$rootScope',
    function ($scope, $windowInstance, id, appConstants, operationService,
              personFindService, instrumentFindService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $http, $rootScope) {

      var initializeFormData = function (draft) {
        if (draft == "\"data\"") {
          draft = {};
        }
        if (!draft.issuer ||draft.issuer === null) {
          draft.issuer = {};
        }

        return draft;
      };
      var prepareFormData = function (formData) {
        return formData;
      };
      $scope.whiteListData = initializeFormData(angular.fromJson(task.draft));
      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "addToDebtorsListForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {

              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formBuf = prepareFormData(angular.copy($scope.whiteListData));
                $scope.config.completeTask(formBuf);

              } else {
                SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
              }
            }
          }
        },
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt(false);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.whiteListData);
      });

      $scope.findIssuer = function () {
        personFindService.findIssuer().then(function (data) {
          $scope.whiteListData.issuer.name = data.name;
          $scope.whiteListData.issuer.accountId = data.accountId;
          $scope.whiteListData.issuer.id = data.id;
          $scope.whiteListData.issuer.accountNumber = data.accountNumber;
          Loader.show(true);
          instrumentFindService.findInstrumentInBlackListByIssuer(data.id).then(function (instrumentSearchResult) {

            if (instrumentSearchResult.success == "true") {
              if (instrumentSearchResult.data) {
                $scope.whiteListData.instruments = instrumentSearchResult.data.map(function (instrument) {
                  return {
                    ID: instrument.ID,
                    instrument: {
                      id: instrument.instrument ? instrument.instrument.id : "",
                      ISIN: instrument.instrument ? instrument.instrument.ISIN : ''
                    },
                    isInRejectionList: true
                  }
                });
              }
            } else {
              SweetAlert.swal("", helperFunctionsService.showErrorMessage(instrumentSearchResult), 'error');
            }
            Loader.show(false);

          });
        });
      };

    }]);
