'use strict';

angular.module('cmisApp').controller('RestoreCancelledInstrumentController', ['$scope', '$windowInstance', 'id', 'appConstants', 'operationService',     'personFindService', 'debtorsService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService', 'Loader', 'SweetAlert', 'gettextCatalog', '$http', '$rootScope', function ($scope, $windowInstance, id, appConstants, operationService, personFindService, debtorsService, helperFunctionsService, operationState, task, referenceDataService, Loader, SweetAlert, gettextCatalog, $http, $rootScope) {

  var initializeFormData = function (draft) {
    if (!draft) {
      draft = {};
    }
    return draft;
  };
  var prepareFormData = function (formData) {
    return formData;
  };

  $scope.taskData = initializeFormData(angular.fromJson(task.draft));

  $scope.getCancelledInstrumentDataByISIN = function(ISIN) {
    $scope.totalQuantity = 0;
    Loader.show(true);
    $http({
      method: "POST",
      url: "/api/instruments/getCancelledInstrumentDataByISIN",
      data: {
        ISIN: ISIN
      }
    }).then(function(response) {
      if(response.data) {
        if(response.data.data) {
          $scope.instrumentData = response.data.data;
          $scope.instrumentData.vmBalances.map(function (item) {
            $scope.totalQuantity += item.quantity;
          });
          $scope.taskData.instrumentId = response.data.data.oldInstrument.id;
        } else {
          SweetAlert.swal('', gettextCatalog.getString(response.data.message), 'error');
        }
      }
      Loader.show(false);
    }, function(errorResponse) {
      console.log(errorResponse);
      Loader.show(false);
    })
  };

  //Initialize scope variables [[
  $scope.config = {
    screenId: id,
    taskKey: id,
    task: task,
    form: {
      name: "restoreCancelledInstrument",
      data: {}
    },
    operationType: appConstants.operationTypes.entry,
    window: $windowInstance,
    state: operationState,
    buttons: {
      complete: {
        click: function () {
          $scope.config.form.name.$submitted = true;
          if ($scope.config.form.name.$valid) {
            var formBuf = prepareFormData(angular.copy($scope.taskData));
            $scope.config.completeTask(formBuf);
          } else {
            SweetAlert.swal('', gettextCatalog.getString('Form Validation Error! \n Please check inputted fields'), 'error');
          }
        }
      }
    }
  };

  // Check if form is dirty
  $scope.$on('closeTask', function () {
    $scope.config.showTaskSavePrompt(false);
  });

  // Save task as draft
  $scope.$on('saveTask', function () {
    $scope.config.saveTask($scope.whiteListData);
  });

}]);
