'use strict';

angular.module('cmisApp')
    .controller('DistributeCouponPaymentTaskController',
        ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'helperFunctionsService', 'gettextCatalog',
            '$http', 'SweetAlert', 'Loader',
            function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, helperFunctionsService, gettextCatalog,
                      $http, SweetAlert, Loader) {

                window.$scope = $scope;

                //Initialize scope variables [[

                setTimeout(function () {
                    $windowInstance.widget.setOptions({width: '80%'});
                }, 200);

                $scope.operationState = operationState;

                function resizeGrid() {
                    var gridElement = this.element;
                    var dataArea = gridElement.find(".k-grid-content");
                    var newHeight = gridElement.parent().innerHeight() - 260;
                    var diff = gridElement.innerHeight() - dataArea.innerHeight();
                    gridElement.height(newHeight);
                    dataArea.height(newHeight - diff);
                }


                $scope.config = {
                    screenId: id,
                    taskId: taskId,
                    task: task,
                    operationType: appConstants.operationTypes.approval,
                    window: $windowInstance,
                    state: operationState,
                    buttons: {
                        reject: null,
                        complete: {
                            click: function () {
                                $scope.config.completeTaskApprove($scope.data);
                            }
                        }
                    }

                };

                $scope.data = {};

                function findData(e) {

                    if (operationState === 'completed') {
                        $scope.data = angular.fromJson(task.draft);
                        $scope.data.paymentList = helperFunctionsService.convertObjectToArray($scope.data.payments);
                        e.success({
                            Data: $scope.data.paymentList ? $scope.data.paymentList : [],
                            Total: $scope.data.paymentList.length ? $scope.data.paymentList.length : 0
                        });
                        return;
                    }

                    if ($scope.mainGrid !== undefined) {
                        $scope.mainGrid.refresh();
                    }

                    Loader.show(true);

                    $http.get('/api/reportsDataServices/checkCouponPayment/' + task.id).then(function (res) {
                        Loader.show(false);
                        var data = res.data;
                        var payload = angular.fromJson(task.draft);

                        if (!data.isSuccess) {
                            SweetAlert.swal('', data.message, 'error');
                        }

                        if (data.data) {
                            payload = angular.copy(data.data);
                        }
                        $scope.data = payload;
                        $scope.data.paymentList = helperFunctionsService.convertObjectToArray($scope.data.payments);

                        e.success({
                            Data: $scope.data.paymentList ? $scope.data.paymentList : [],
                            Total: $scope.data.paymentList.length ? $scope.data.paymentList.length : 0
                        });

                        console.log(payload);

                        $scope.data.isSuccess = data.isSuccess;
                    });
                }

                $scope.mainGridOptions = {
                    scrollable: true,
                    pageable: {"pageSize": 10, "refresh": true, "pageSizes": true},
                    resizable: true,
                    columns: [
                        {
                            field: "accountNumber",
                            title: gettextCatalog.getString("Account Number")
                        },
                        {
                            field: "personName",
                            title: gettextCatalog.getString("Person")
                        },
                        {
                            field: "amount",
                            title: gettextCatalog.getString("Amount"),
                            template: "<div style='text-align: right'>#= kendo.toString(amount, 'n2') # </div>"
                        },
                        {
                            field: "tax",
                            title: "ÖMV"
                        }


                    ],
                    dataSource: {
                        schema: {
                            data: 'Data',
                            total: "Total"
                        },
                        transport: {
                            read: function (e) {
                                findData(e);
                            }
                        }
                    },
                    dataBound: resizeGrid,
                    schema: {
                        model: {
                            fields: {}
                        }
                    }
                };

                $scope.$on("kendoWidgetCreated", function (event, widget) {
                    $scope.mainGrid = widget;
                });


            }]);
