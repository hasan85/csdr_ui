'use strict';

angular.module('cmisApp')
  .controller('InstrumentDataEntryTemplateController',
  ['$scope',
    function ($scope) {

      var customIdentificatorsTemplate = {
        type: null,
        number: null
      };

      var couponPaymentTemplate = {
        paymentDate: null,
        amount: null
      };

      var securityGroupTemplate = {
        nameAz: null,
        nameEn: null,
        id: null,
        code: null
      };

      var fiscalDatesTemplate = {
        value: null
      };

      //Add/remove custom Identificator
      $scope.addNewIdentificator = function (model) {
        model.push(angular.copy(customIdentificatorsTemplate));
      };

      $scope.removeIdentificator = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove coupon payments
      $scope.addNewCouponPayment = function (model) {
        model.push(angular.copy(couponPaymentTemplate));
      };

      $scope.removeCouponPayment = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove fiscal date
      $scope.addNewFiscalDate = function (model) {

        model.push(angular.copy(fiscalDatesTemplate));
      };

      $scope.removeFiscalDate = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove security groups
      $scope.addNewSecurityGroup = function (model) {
        model.push(angular.copy(securityGroupTemplate));
      };

      $scope.removeSecurityGroup = function (index, model) {
        model.splice(index, 1);
      };

    }]);
