'use strict';

angular.module('cmisApp')
  .controller('ManageInstrumentDataController',
  ['$scope', 'gettextCatalog', '$windowInstance', 'id', 'appConstants', 'operationService',
    'instrumentFindService', 'helperFunctionsService', 'operationState', 'task', 'referenceDataService',
    'Loader', 'personFindService', 'issueRegistrationService', 'manageInstrumentService',
    function ($scope, gettextCatalog, $windowInstance, id, appConstants, operationService,
              instrumentFindService, helperFunctionsService, operationState, task, referenceDataService,
              Loader, personFindService, issueRegistrationService, manageInstrumentService) {

      // metadata
      $scope.metaData = {
        sel1: [],
        sel2: [],
        sel3: [],
        sel4: [],
        sel5: [],
        sel6: [],
        convertableStockes: null
      };

      // Initialize local variables
      var commonStockConfig, convertibleStockConfig, bondConfig, mutualFundConfig, subscriptionConfig, warrantConfig = {};
      var customIdentificatorsTemplate = {
        type: null,
        number: null
      };
      var fiscalDatesTemplate = {
        value: null
      };
      var couponPaymentTemplate = {
        paymentDate: null,
        amount: null
      };
      var securityGroupTemplate = {
        nameAz: null,
        nameEn: null,
        id: null,
        code: null
      };

      $scope.instrument = {
        customIdentificators: [
          angular.copy(customIdentificatorsTemplate)
        ],
        fiscalDates: [
          angular.copy(fiscalDatesTemplate)
        ],
        issuer: {},
        foreign: false,
        CFI: {},
        couponPayments: [
          angular.copy(couponPaymentTemplate)
        ],
        securityGroups: [
          angular.copy(securityGroupTemplate)
        ],
        cfiForm: {
          name: 'cfiGenerateForm',
          generatedCFI: 'EDPEKDE',
          sel1: '',
          sel2: '',
          sel3: '',
          sel4: '',
          sel5: '',
          sel6: ''
        },
        config: {
          fields: {},
          placement: {
            classCodes: {
              public: 'PLACEMENT_CLASS_PUBLIC',
              private: 'PLACEMENT_CLASS_PRIVATE',
              auction: 'PLACEMENT_CLASS_AUCTION'
            },

            currentCode: null
          },
          security: {
            securityTypes: {
              equity: 'Equity',
              bond: 'Bond'
            },
            currentSecurity: null,
            isEquity: false,
            isBond: false,
            isIPO: true,
            isRS: false,
            isRW: false,
            isEP: false,
            isEU: false
          },
          labels: {
            periodStartDate: null,
            periodEndDate: null
          }
        },
        haircutRatio: 0,
        conventionalDayCount: 0
      };

      //Initialize scope variables [[
      $scope.config = {
        screenId: id,
        taskKey: id,
        task: task,
        form: {
          name: "instrumentDataEntryForm",
          data: {}
        },
        operationType: appConstants.operationTypes.entry,
        window: $windowInstance,
        state: operationState,
        buttons: {
          complete: {
            click: function () {
              $scope.config.form.name.$submitted = true;
              if ($scope.config.form.name.$valid) {
                var formData = manageInstrumentService.prepareFormData($scope.instrument);
                $scope.config.completeTask(formData);
              }
            }
          }
        }
      };

      // Check if form is dirty
      $scope.$on('closeTask', function () {
        $scope.config.showTaskSavePrompt($scope.config.form.name.$dirty);
      });

      // Save task as draft
      $scope.$on('saveTask', function () {
        $scope.config.saveTask($scope.instrument);
      });

      //Get metadata
      issueRegistrationService.getMetaData().then(function (data) {
        $scope.metaData = {
          regAuthClasses: data.registrationAuthorityClasses,
          placementClasses: data.placementClasses,
          identificatorClasses: data.identificatorClasses,
          currencies: data.currencies,
          securityGroups: data.securityGroups,
          sel1: data.CFIcategories,
          couponPaymentMethodClasses: data.couponPaymentMethodClasses,
          convertableStockes: null
        };
        var payload = angular.fromJson(task.draft);
        if (payload) {
          if (payload.haircutRatio) {
            $scope.instrument.haircutRatio = payload.haircutRatio * 100;
          }
          if (payload.conventionalDayCount) {
            $scope.instrument.conventionalDayCount = payload.conventionalDayCount;
          }

          // Get config data
          var commonStockConfigRaw = payload['commonStockConfiguration'] ? angular.fromJson(payload['commonStockConfiguration']) : null;
          var convertibleStockConfigRaw = payload['convertibleStockConfiguration'] ? angular.fromJson(payload['convertibleStockConfiguration']) : null;
          var bondConfigRaw = payload['bondConfiguration'] ? angular.fromJson(payload['bondConfiguration']) : null;

          var mutualFundConfigurationRaw = payload['mutualFundConfiguration'] ? angular.fromJson(payload['mutualFundConfiguration']) : null;
          var warrantConfigurationRaw = payload['warrantConfiguration'] ? angular.fromJson(payload['warrantConfiguration']) : null;
          var subscriptionConfigurationRaw = payload['subscriptionConfiguration'] ? angular.fromJson(payload['subscriptionConfiguration']) : null;

          // For save as draft
          $scope.instrument['commonStockConfiguration'] = commonStockConfigRaw;
          $scope.instrument['convertibleStockConfiguration'] = convertibleStockConfigRaw;
          $scope.instrument['bondConfiguration'] = bondConfigRaw;
          $scope.instrument['mutualFundConfiguration'] = mutualFundConfigurationRaw;
          $scope.instrument['warrantConfiguration'] = warrantConfigurationRaw;
          $scope.instrument['subscriptionConfiguration'] = subscriptionConfigurationRaw;

          // Prepare config data
          commonStockConfig = commonStockConfigRaw ? manageInstrumentService.generateFormConfig(commonStockConfigRaw) : null;
          convertibleStockConfig = convertibleStockConfigRaw ? manageInstrumentService.generateFormConfig(convertibleStockConfigRaw) : null;
          bondConfig = bondConfigRaw ? manageInstrumentService.generateFormConfig(bondConfigRaw) : null;
          mutualFundConfig = mutualFundConfigurationRaw ? manageInstrumentService.generateFormConfig(mutualFundConfigurationRaw) : null;
          warrantConfig = warrantConfigurationRaw ? manageInstrumentService.generateFormConfig(warrantConfigurationRaw) : null;
          subscriptionConfig = subscriptionConfigurationRaw ? manageInstrumentService.generateFormConfig(subscriptionConfigurationRaw) : null;

          $scope.instrument.config.fields = commonStockConfig;
          $scope.instrument.partialFormName = $scope.config.form.name;
        }
      });

      //Add/remove custom Identificator
      $scope.addNewIdentificator = function (model) {

        model.push(angular.copy(customIdentificatorsTemplate));
      };

      $scope.removeIdentificator = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove coupon payments
      $scope.addNewCouponPayment = function (model) {
        model.push(angular.copy(couponPaymentTemplate));
      };

      $scope.removeCouponPayment = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove fiscal date
      $scope.addNewFiscalDate = function (model) {

        model.push(angular.copy(fiscalDatesTemplate));
      };

      $scope.removeFiscalDate = function (index, model) {
        model.splice(index, 1);
      };

      //Add/remove security groups
      $scope.addNewSecurityGroup = function (model) {
        model.push(angular.copy(securityGroupTemplate));
      };

      $scope.removeSecurityGroup = function (index, model) {
        model.splice(index, 1);
      };

      // Find issuer
      $scope.findIssuer = function () {

        personFindService.findIssuer().then(function (data) {

          $scope.instrument.issuer.name = data.name;
          $scope.instrument.issuer.id = data.id;
          $scope.instrument.issuer.accountId = data.accountId;
          $scope.instrument.issuer.accountNumber = data.accountNumber;

          if ($scope.instrument.config.security.isRS || $scope.instrument.config.security.isRW) {
            issueRegistrationService.getIssuerEquities(data.id).then(function (data) {

              $scope.metaData.convertableStockes = data;

            });
          }
        });
      };

      // Query CFI hierarchy
      $scope.getChildOptions = function (classId, index) {

        if (index != 6) {
          var startIndex;

          if (classId == '') {
            startIndex = index + 1;
          } else {

            startIndex = index + 2;
            classId = parseInt(classId);

            if (classId > 0) {

              Loader.show(true);

              referenceDataService.getCFIClasses(classId).then(function (data) {

                var nextClassesIndex = index + 1;
                $scope.metaData['sel' + nextClassesIndex] = data;

                Loader.show(false);

              });
            }
          }
          if (startIndex <= 6) {
            for (var i = startIndex; i <= 6; i++) {
              $scope.metaData['sel' + i] = null;
            }
          }
        }

      };

      // Get generated CFI
      $scope.generateCFI = function (window) {
        $scope.instrument.cfiForm.name.$submitted = true;
        if ($scope.instrument.cfiForm.name.$valid) {
          for (var i = 0; i < $scope.metaData.sel6.length; i++) {
            var searchedID = parseInt($scope.instrument.cfiForm.sel6);
            var currentID = parseInt($scope.metaData.sel6[i]['id']);
            if (searchedID === currentID) {
              $scope.instrument.CFI = $scope.metaData.sel6[i];
              window.close();
              $scope.instrument.cfiForm.sel6 =
                $scope.instrument.cfiForm.sel5 =
                  $scope.instrument.cfiForm.sel4 =
                    $scope.instrument.cfiForm.sel3 =
                      $scope.instrument.cfiForm.sel2 = '';


              if (helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_D')) {
                $scope.instrument.config.fields = bondConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_R_W')) {

                $scope.instrument.config.security.isRW = true;
                $scope.instrument.config.fields = warrantConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_R_S')) {

                $scope.instrument.config.security.isRS = true;
                $scope.instrument.config.fields = subscriptionConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_E_S')) {

                $scope.instrument.config.fields = commonStockConfig;
              }

              if (
                helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_E_P') ||
                helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_E_R') ||
                helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_E_C') ||
                helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_E_F') ||
                helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_E_V')

              ) {
                $scope.instrument.config.fields = convertibleStockConfig;
              }

              if (helperFunctionsService.startsWith($scope.instrument.CFI.code, 'CFI_E_U')) {
                $scope.instrument.config.fields = mutualFundConfig;
              }

              break;
            }
          }
        }
      };

    }]);
