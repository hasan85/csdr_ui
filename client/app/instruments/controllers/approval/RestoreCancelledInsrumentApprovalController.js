'use strict';

angular.module('cmisApp').controller('RestoreCancelledInstrumentApprovalController', ['$scope', '$windowInstance', 'id', 'appConstants', 'taskId', 'operationState', 'task', 'helperFunctionsService', "Loader", "$http", "SweetAlert", "$timeout",  function ($scope, $windowInstance, id, appConstants, taskId, operationState, task, helperFunctionsService, Loader, $http, SweetAlert, $timeout) {
  $scope.config = {
    screenId: id,
    taskId: taskId,
    task: task,
    operationType: appConstants.operationTypes.approval,
    window: $windowInstance,
    state: operationState
  };

  $scope.draft = angular.fromJson(task.draft);

  $timeout(function() {
    Loader.show(true);

    $scope.totalQuantity = 0;
    $http({
      method: "POST",
      url: "/api/instruments/getCancelledInstrumentDataByISIN",
      data: {
        ISIN: $scope.draft.isin
      }
    }).then(function(response) {
      if(response.data) {
        if(response.data.data) {
          $scope.instrumentData = response.data.data;
          $scope.instrumentData.vmBalances.map(function (item) {
            $scope.totalQuantity += item.quantity;
          });
        } else {
          SweetAlert.swal('', gettextCatalog.getString(response.data.message), 'error');
        }
      }
      Loader.show(false);
    }, function(errorResponse) {
      console.log(errorResponse);
      Loader.show(false);
    });
  }, 700);

}]);
