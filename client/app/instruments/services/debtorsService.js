'use strict';

angular.module('cmisApp')
  .factory('debtorsService', ["$q", "Loader", "$http", 'helperFunctionsService', 'gettextCatalog', '$kWindow',
    function ($q, Loader, $http, helperFunctionsService, gettextCatalog, $kWindow) {

      var deferred = $q.defer();

      var findDebtor = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Instrument In Debtors List'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/debtor-find.html',
          controller: 'DebtorFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };
      var findBlackList = function (searchCriteria) {
        deferred = $q.defer();
        $kWindow.open({

          title: gettextCatalog.getString('Find Instrument In Black List'),
          actions: ['Close'],
          isNotTile: true,
          width: '90%',
          height: '90%',
          pinned: true,
          modal: true,
          templateUrl: 'app/common/templates/black-list-find.html',
          controller: 'BlackListFindController',
          resolve: {
            id: function () {
              return 0;
            },
            searchCriteria: function () {
              return searchCriteria;
            }
          }
        });

        return deferred.promise;
      };
      var selectDebtor = function (data) {
        // Select Bank Statement, resolve promise
        deferred.resolve(data);
        deferred = $q.defer();
      };

      var normalizeRejectionRecordTask = function (model, metaData) {
        if (model.rejectionActionClass && model.rejectionActionClass.id) {
          model.rejectionActionClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.rejectionActionClasses, model.rejectionActionClass['id']));
        }

        if (model.startDate && !model.startDateObj) {
          model.startDateObj = helperFunctionsService.parseDate(model.startDate);
          model.startDate = helperFunctionsService.generateDate(model.startDateObj);
        }
        if (model.finishDate && !model.finishDateObj) {
          model.finishDateObj = helperFunctionsService.parseDate(model.finishDate);
          model.finishDate = helperFunctionsService.generateDate(model.finishDateObj);
        }

      };
      return {
        findDebtor: findDebtor,
        selectDebtor: selectDebtor,
        findBlackList: findBlackList,
        normalizeRejectionRecordTask:normalizeRejectionRecordTask
      };

    }]);
