'use strict';

angular.module('cmisApp')
  .factory('manageInstrumentService', ["referenceDataService", "$q", "Loader", 'helperFunctionsService', '$http', '$filter',
    function (referenceDataService, $q, Loader, helperFunctionsService, $http, $filter) {

      var getInstrumentFormTemplate = function () {

        var customIdentificatorsTemplate = {
          type: null,
          number: null
        };
        var fiscalDatesTemplate = {
          value: null
        };
        var couponPaymentTemplate = {
          paymentDate: null,
          amount: null
        };
        var securityGroupTemplate = {
          nameAz: null,
          nameEn: null,
          id: null,
          code: null
        };

        var instrumentFormTemplate = {

          customIdentificators: [
            angular.copy(customIdentificatorsTemplate)
          ],
          fiscalDates: [
            angular.copy(fiscalDatesTemplate)
          ],
          issuer: {},
          foreign: false,
          CFI: {},

          couponPayments: [
            angular.copy(couponPaymentTemplate)
          ],
          securityGroups: [
            angular.copy(securityGroupTemplate)
          ],
          cfiForm: {
            name: 'cfiGenerateForm',
            generatedCFI: 'EDPEKDE',
            sel1: '',
            sel2: '',
            sel3: '',
            sel4: '',
            sel5: '',
            sel6: ''
          },
          config: {
            fields: {}
          },
          haircutRatio: 0,
          conventionalDayCount: 0,
          metaData: {}
        };

        return instrumentFormTemplate;
      };

      //Return meta data
      var getMetaData = function () {

        Loader.show(true);
        var deferred = $q.defer();
        var metaData = {};

        var getIdentificatorClasses = function (value) {
          metaData.identificatorClasses = value;
        };
        var getIndividualSecurityGroups = function (value) {
          metaData.securityGroups = value;
        };
        var getCFICategories = function (value) {
          metaData.CFIcategories = value;
        };
        var getCurrencies = function (value) {
          metaData.currencies = value;
        };

        $q.all([
          referenceDataService.getInstrumentIdentificatorTypes().then(getIdentificatorClasses),
          referenceDataService.getCurrencies().then(getCurrencies),
          referenceDataService.getIndividualSecurityGroups().then(getIndividualSecurityGroups),
          referenceDataService.getCFIClasses(0).then(getCFICategories),
        ])
          .then(function () {
            deferred.resolve(metaData);
            Loader.show(false);
          });

        return deferred.promise;
      };

      var prepareFormData = function (formData) {

        var result = {};

        result['commonStockConfiguration'] = typeof formData['commonStockConfiguration'] === "string" ?
          formData['commonStockConfiguration'] : angular.toJson(formData['commonStockConfiguration']);

        result['convertibleStockConfiguration'] = typeof formData['convertibleStockConfiguration'] === "string" ?
          formData['convertibleStockConfiguration'] : angular.toJson(formData['convertibleStockConfiguration']);

        result['bondConfiguration'] = typeof formData['bondConfiguration'] === "string" ?
          formData['bondConfiguration'] : angular.toJson(formData['bondConfiguration']);

        result['mutualFundConfiguration'] = typeof formData['mutualFundConfiguration'] === "string" ?
          formData['mutualFundConfiguration'] : angular.toJson(formData['mutualFundConfiguration']);

        result['warrantConfiguration'] = typeof formData['warrantConfiguration'] === "string" ?
          formData['warrantConfiguration'] : angular.toJson(formData['warrantConfiguration']);

        result['subscriptionConfiguration'] = typeof formData['subscriptionConfiguration'] === "string" ?
          formData['subscriptionConfiguration'] : angular.toJson(formData['subscriptionConfiguration']);

        //
        //result['convertibleStockConfiguration'] = formData['convertibleStockConfiguration'];
        //result['bondConfiguration'] = formData['bondConfiguration'];
        //result['mutualFundConfiguration'] = formData['mutualFundConfiguration'];
        //result['warrantConfiguration'] = formData['warrantConfiguration'];
        //result['subscriptionConfiguration'] = formData['subscriptionConfiguration'];


        result.id = formData.id ? formData.id : null;
        result.ISIN = formData.ISIN ? formData.ISIN : null;
        result.localeCode = formData.localeCode ? formData.localeCode : null;
        result.isPaymentAgent = formData.isPaymentAgent ? formData.isPaymentAgent : false;
        result.isTaxAgent = formData.isTaxAgent ? formData.isTaxAgent : false;
        result.CFI = formData.CFI ? formData.CFI : null;
        result.name = formData.name ? formData.name : null;
        result.abbreviation = formData.abbreviation ? formData.abbreviation : null;
        result.issuer = formData.issuer ? formData.issuer : null;
        result.foreign = formData.foreign ? formData.foreign : null;
        result.quantity = formData.quantity > -1 ? formData.quantity : null;
        result.parValue = formData.parValue > -1 ? formData.parValue : null;
        result.couponRate = formData.couponRate > -1 ? (formData.couponRate / 100) : null;
        result.conventionalDayCount = formData.conventionalDayCount > -1 ? formData.conventionalDayCount : null;

        result.nominator = formData.nominator > -1 ? formData.nominator : null;
        result.discriminator = formData.discriminator > -1 ? formData.discriminator : null;

        if (formData.couponPaymentMethod && formData.couponPaymentMethod != '') {
          result.couponPaymentMethod = angular.fromJson(formData.couponPaymentMethod);
        }
        if (formData.fractionTreatmentClass && formData.fractionTreatmentClass != '') {
          result.fractionTreatmentClass = angular.fromJson(formData.fractionTreatmentClass);
        }
        if (formData.convertableStock && formData.convertableStock != '') {
          result.convertableStock = angular.fromJson(formData.convertableStock);
        }

        if (formData.maturityDateObj) {
          result.maturityDate = helperFunctionsService.generateDateTime(formData.maturityDateObj);
        }
        if (formData.periodStartDateObj) {
          result.periodStartDate = helperFunctionsService.generateDateTime(formData.periodStartDateObj);
        }
        if (formData.periodEndDateObj) {
          result.periodEndDate = helperFunctionsService.generateDateTime(formData.periodEndDateObj);
        }

        //if (formData.haircutRatio) {
        result.haircutRatio = (formData.haircutRatio / 100);
        //}

        result.fiscalDates = angular.copy(formData.fiscalDates);

        if (result.fiscalDates && result.fiscalDates.length > 0) {

          if (result.fiscalDates.length == 1 && !result.fiscalDates[0].value) {

            result.fiscalDates = null;

          }
          else {
            for (var i = 0; i < result.fiscalDates.length; i++) {

              if (result.fiscalDates[i].value) {
                var dateObject = angular.isDate(result.fiscalDates[i].valueObj) ? result.fiscalDates[i].valueObj : new Date(result.fiscalDates[i].valueObj);
                var month = (parseInt(dateObject.getMonth()) + 1);
                var day = dateObject.getDate();

                result.fiscalDates[i] = {
                  month: month,
                  day: day
                };
              }

            }
          }
        }

        else {
          result.fiscalDates = null;
        }


        result.customIdentificators = null;
        var customIdentificatorsBuf = angular.copy(formData.customIdentificators);

        if (customIdentificatorsBuf && customIdentificatorsBuf.length > 0) {
          result.customIdentificators = [];
          for (var i = 0; i < customIdentificatorsBuf.length; i++) {
            if (customIdentificatorsBuf[i] != '' && customIdentificatorsBuf[i]['number']) {
              customIdentificatorsBuf[i]['type'] = angular.fromJson(customIdentificatorsBuf[i]['type']);
              result.customIdentificators.push(customIdentificatorsBuf[i]);
            }
          }
          if (result.customIdentificators.length == 0) {
            result.customIdentificators = null;
          }
        }

        result.couponPayments = null;
        var couponPaymentsBuf = angular.copy(formData.couponPayments);
        if (couponPaymentsBuf && couponPaymentsBuf.length > 0) {
          result.couponPayments = [];
          for (var i = 0; i < couponPaymentsBuf.length; i++) {
            if (couponPaymentsBuf[i] && couponPaymentsBuf[i] != '' && couponPaymentsBuf[i]['amount']) {
              couponPaymentsBuf[i]['paymentDate'] = helperFunctionsService.generateDateTime(couponPaymentsBuf[i]['paymentDateObj']);
              result.couponPayments.push(couponPaymentsBuf[i]);
            }
          }

          if (result.couponPayments.length == 0) {
            result.couponPayments = null
          }
        }

        result.securityGroups = null;
        var securityGroupsBuf = angular.copy(formData.securityGroups);
        result.securityGroups = [];
        if (securityGroupsBuf && securityGroupsBuf.length > 0) {
          for (var i = 0; i < securityGroupsBuf.length; i++) {
            if (securityGroupsBuf[i] != '') {
              securityGroupsBuf[i] = angular.fromJson(securityGroupsBuf[i]);
              result.securityGroups.push(securityGroupsBuf[i]);
            }
          }
          if (result.securityGroups.length == 0) {
            result.securityGroups = null;
          }
        }

        if (formData.currency && formData.currency != '') {
          result.currency = angular.fromJson(formData.currency);
        }
        return result;
      };

      var getInTreasureQuantity = function (id) {
        return $http.get("/api/instruments/" + "getInTreasureQuantity/" + id).then(function (result) {
          return result.data;
        })
      };

      var getBondCashFlow = function (id) {
        return $http.get("/api/instruments/" + "getBondCashFlow/" + id).then(function (result) {

          if (result.data && result.data['data']) {
            result.data['data'] = helperFunctionsService.convertObjectToArray(result.data['data']);
          }
          return result.data;
        })
      };

      // Generate form configuration
      var generateFormConfig = function (rawData) {
        var configModel = {

          "isEnRequired": {
            "is_mandatory": false,
            "is_visible": false
          },
          "ISIN": {
            "is_mandatory": false,
            "is_visible": false
          },
          "locale_code": {
            "is_mandatory": false,
            "is_visible": false
          },
          "CFI": {
            "is_mandatory": false,
            "is_visible": false
          },
          "name": {
            "is_mandatory": false,
            "is_visible": false
          },
          "abbreviation": {
            "is_mandatory": false,
            "is_visible": false
          },
          "issuer": {
            "is_mandatory": false,
            "is_visible": false,
            fields: {
              "account_id": {
                "is_mandatory": false,
                "is_visible": false
              },
              "name": {
                "is_mandatory": false,
                "is_visible": false
              },
              "account_number": {
                "is_mandatory": false,
                "is_visible": false
              },
            }
          },
          "foreign": {
            "is_mandatory": false,
            "is_visible": false
          },
          "quantity": {
            "is_mandatory": false,
            "is_visible": false
          },
          "par_value": {
            "is_mandatory": false,
            "is_visible": false
          },
          "currency": {
            "is_mandatory": false,
            "is_visible": false
          },
          "custom_identificators": {
            "is_mandatory": false,
            "is_visible": false,
            fields: {
              "type": {
                "is_mandatory": false,
                "is_visible": false
              },
              "number": {
                "is_mandatory": false,
                "is_visible": false
              },
            }
          },

          "security_groups": {
            "is_mandatory": false,
            "is_visible": false
          },
          "coupon_rate": {
            "is_mandatory": false,
            "is_visible": false
          },
          "maturity_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "coupon_payments": {
            "is_mandatory": false,
            "is_visible": false,
            "fields": {
              "payment_date": {
                "is_mandatory": false,
                "is_visible": false
              },
              "amount": {
                "is_mandatory": false,
                "is_visible": false
              },
            },
          },
          "coupon_payment_method": {
            "is_mandatory": false,
            "is_visible": false
          },
          "fiscal_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "haircut_ratio": {
            "is_mandatory": false,
            "is_visible": false
          },
          "conventional_day_count": {
            "is_mandatory": false,
            "is_visible": false
          },
          "convertible_stock": {
            "is_mandatory": false,
            "is_visible": false
          },
          "exercise_period_start_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "exercise_period_end_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "establishment_start_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "establishment_end_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "SR_last_trading_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "finish_date": {
            "is_mandatory": false,
            "is_visible": false
          },
          "exercise_price": {
            "is_mandatory": false,
            "is_visible": false
          },

        };

        for (var i = 0; i < rawData.childs.length; i++) {

          var panel = rawData.childs[i];
          var panelName = panel['name'];

          configModel[panelName]['is_mandatory'] = panel['mandatory'];
          configModel[panelName]['is_visible'] = panel['visible'];

          if (panel.childs) {
            for (var j = 0; j < panel.childs.length; j++) {
              var field = panel.childs[j];
              var fieldName = field['name'];

              if (configModel[panelName].fields[fieldName]) {
                configModel[panelName].fields[fieldName]['is_mandatory'] = field['mandatory'];
                configModel[panelName].fields[fieldName]['is_visible'] = field['visible'];
              }
            }
          }
        }

        return configModel;

      };

      var normalizeReturnedTaskData = function (model, metaData) {
        if (model.couponPaymentMethod && model.couponPaymentMethod.id) {
          model.couponPaymentMethod = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.couponPaymentMethodClasses, model.couponPaymentMethod['id']));
        }
        if (model.currency && model.currency.id) {
          model.currency = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.currencies, model.currency['id']));
        }
        //model.couponRate = model.couponRate > -1 ? (model.couponRate / 100) : null;
        //model.haircutRatio = model.haircutRatio > -1 ? (model.haircutRatio / 100) : null;
        if (model.fractionTreatmentClass && model.fractionTreatmentClass.id) {
          model.fractionTreatmentClass = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.fractionTreatmentClasses, model.fractionTreatmentClass['id']));
        }

        if (model.maturityDate && !model.maturityDateObj) {
          model.maturityDateObj = helperFunctionsService.parseDate(model.maturityDate);
          model.maturityDate = helperFunctionsService.generateDate(model.maturityDateObj);
        }
        if (model.periodStartDate && !model.periodStartDateObj) {
          model.periodStartDateObj = helperFunctionsService.parseDate(model.periodStartDate);
          model.periodStartDate = helperFunctionsService.generateDate(model.periodStartDateObj);
        }

        if (model.periodEndDate && !model.periodEndDateObj) {
          model.periodEndDateObj = helperFunctionsService.parseDate(model.periodEndDate);
          model.periodEndDate = helperFunctionsService.generateDate(model.periodEndDateObj);
        }
        if (model.securityGroups && model.securityGroups.length) {
          for (var i = 0; i < model.securityGroups.length; i++) {
            if(model.securityGroups[i]){
              model.securityGroups[i] = $filter('json')(helperFunctionsService.findByIdInsideArray(metaData.securityGroups, model.securityGroups[i]['id']));

            }
                     }
        }
        if (model.couponPayments && model.couponPayments.length) {
          for (var i = 0; i < model.couponPayments.length; i++) {
            var obj = model.couponPayments[i];
            if (obj.paymentDate && !obj.paymentDateObj) {
              obj.paymentDateObj = helperFunctionsService.parseDate(obj.paymentDate);
              obj.paymentDate = helperFunctionsService.generateDate(obj.paymentDateObj);
            }
          }
        }
        if (model.fiscalDates && model.fiscalDates.length) {
          for (var i = 0; i < model.fiscalDates.length; i++) {
            var obj = model.fiscalDates[i];
            if (obj.day && obj.month) {
              obj.valueObj = new Date(2016, obj.month - 1, obj.day);
              obj.value = $filter('date')(obj.valueObj, 'dd MM');

            }
          }
        }
      };


      return {
        getMetaData: getMetaData,
        prepareFormData: prepareFormData,
        generateFormConfig: generateFormConfig,
        getInTreasureQuantity: getInTreasureQuantity,
        getInstrumentFormTemplate: getInstrumentFormTemplate,
        getBondCashFlow: getBondCashFlow,
        normalizeReturnedTaskData: normalizeReturnedTaskData
      };

    }])
;
