'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

var connectTimeout = require('connect-timeout');
var longTimeout = connectTimeout('600s');

router.get('/getTileConfigurations', auth.requiresLogin, controller.getTileConfigurations);
router.get('/getUserData', auth.requiresLogin, controller.getUserData);
router.get('/getSingleOperation', auth.requiresLogin, controller.getSingleOperation);
router.get('/startProcess', auth.requiresLogin, controller.startProcess);
router.post('/startProcessByMessage', auth.requiresLogin, controller.startProcessByMessage);
router.post('/completeTask',longTimeout, auth.requiresLogin, controller.completeTask);
router.post('/uploadTaskAttachment', auth.requiresLogin, controller.uploadTaskAttachment);
router.post('/removeDocument/:id', auth.requiresLogin, controller.removeDocument);
router.get('/getDocumentByID/:id', auth.requiresLogin, controller.getDocumentByID);
router.get('/openTask', auth.requiresLogin, controller.openTask);
router.get('/openFinishedTask/:processId', auth.requiresLogin, controller.openFinishedTask);
router.post('/getMyActiveTasks', auth.requiresLogin, controller.getMyActiveTasks);
router.post('/getMyUnclaimedTasks', auth.requiresLogin, controller.getMyUnclaimedTasks);
router.post('/getMyFinishedTasks', auth.requiresLogin, controller.getMyFinishedTasks);
router.post('/getMyCompletedTasks', auth.requiresLogin, controller.getMyCompletedTasks);
router.post('/getAllCompletedTasks', auth.requiresLogin, controller.getAllCompletedTasks);
router.post('/getAllFinishedTasks', auth.requiresLogin, controller.getAllFinishedTasks);
router.post('/getMyControlledOperations', auth.requiresLogin, controller.getMyControlledOperations);
router.get('/claimTask', auth.requiresLogin, controller.claimTask);
router.post('/saveTask', auth.requiresLogin, controller.saveTask);
router.post('/cancelTask', auth.requiresLogin, controller.cancelTask);
router.post('/getTasksByProcessInstanceId/', auth.requiresLogin, controller.getTasksByProcessInstanceId);
router.get('/getDashBoardContents/:dashboardKey', auth.requiresLogin, controller.getDashBoardContents);
router.post('/sendTaskComment', auth.requiresLogin, controller.sendTaskComment);
router.post('/sendTaskDocument', auth.requiresLogin, controller.sendTaskDocument);
module.exports = router;
