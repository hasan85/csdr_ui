'use strict';

var _ = require('lodash');
var request = require('request');
var config = require('../../config/environment');
var soap = require("../../lib/soap-wrapper");
var namespace = config.servers.bpm.namespace;
var url = config.servers.bpm.url;
var jwt = require('jsonwebtoken');
var screenTypes = {
  view: 'view',
  operation: 'operation'
};
var fs = require('fs');
var multer = require('multer');
var upload = multer({
  dest: config.temp,
  limits: {
    files: 10,
    fileSize: config.fileSizeLimit
  }
}).any();

// Task Data [[[
var dashboardTiles = {

  "TASKS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-large  bg-crimson fg-white",
    "icon": "tasks",
    "defaultName": "My tasks",
    "nameAz": "Tasks",
    "nameEn": "Tasks",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/tasks.html'
    }
  },
  "ACCOUNTS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-large  bg-crimson fg-white cursor-pointer",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/accounts.html'
    }
  },
  "MONEY_POSITIONS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-large  bg-crimson fg-white cursor-pointer",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/money-positions.html'
    }
  },

  "MEMBER_INFORMATION_DASHBOARD": {
    "classes": "tile tile-dashboard tile-large  bg-crimson fg-white ",
    "icon": "tasks",
    "defaultName": "Information",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/member-information.html'
    }
  },
  "SECURITY_POSITIONS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide  bg-steel fg-white cursor-pointer",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {

      "template": 'app/dashboards/templates/security-positions.html'
    }
  },
  "WITHDRAWALS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide  bg-grayLight fg-white cursor-pointer",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/withdrawals.html'
    }
  },
  "CLEARING_LIMITS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide  bg-darkBlue fg-white cursor-pointer",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/clearing-limits.html'
    }
  },
  "CLEARING_ACCOUNT_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide  bg-darkBlue fg-white",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/clearing-account.html'
    }
  },
  "CLIENTS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide  bg-darkCyan fg-white cursor-pointer",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/clients.html'
    }
  },
  "PAYMENTS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide  bg-darkRed fg-white cursor-pointer",
    "icon": "tasks",
    "defaultName": "Accounts",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/payments.html'
    }
  },
  "CASH_BALANCE": {
    "classes": "tile tile-dashboard tile-wide tile-large bg-darkCyan fg-white",
    "icon": "tasks",
    "defaultName": "Cash Balance",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/cash-balance.html'
    }
  },
  "BUSINESS_DAY": {
    "classes": "tile tile-dashboard tile-wide tile-large tile-compact bg-darkCyan fg-white",
    "icon": "tasks",
    "defaultName": "Business Day",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/business-day.html'
    }
  },
  "MEMBER_CASH_ACCOUNTS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide tile-large tile-compact bg-darkCyan fg-white",
    "icon": "tasks",
    "defaultName": "Business Day",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/member-cash-accounts.html'
    }
  },
  "NDC_BANK_ACCOUNTS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide tile-large tile-compact bg-darkCyan fg-white",
    "icon": "tasks",
    "defaultName": "Business Day",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/ndc-bank-accounts.html'
    }
  },
  "NDC_CASH_ACCOUNTS_DASHBOARD": {
    "classes": "tile tile-dashboard tile-wide tile-large tile-compact bg-darkCyan fg-white",
    "icon": "tasks",
    "defaultName": "Business Day",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/ndc-cash-accounts.html'
    }
  },
  "SHAREHOLDER_ACCOUNT_STATEMENT": {
    "classes": "tile tile-dashboard tile-wide tile-large tile-compact bg-darkCyan fg-white",
    "icon": "tasks",
    "defaultName": "Hesaba baxış",
    "type": screenTypes.view,
    "contentSettings": {
      "template": 'app/dashboards/templates/shareholder-account-statement.html'
    }
  }

};

var tileConfigurations = {

  "view-auctions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'ViewAuctionsController',
    "template": 'app/records/templates/view-auctions.html'
  },

  "pledgee-data-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Shareholder data entry",
    "type": screenTypes.operation
  },
  "person-data-field-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Person data field entry",
    "type": screenTypes.operation
  },
  "global-configuration-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "cog",
    "defaultName": "Global configuration entry",
    "type": screenTypes.operation
  },
  "role-rules-management": {
    "classes": "tile bg-crimson fg-white",
    "icon": "cog",
    "defaultName": "Role Rules Management",
    "type": screenTypes.operation
  },
  "account-rules-management": {
    "classes": "tile bg-crimson fg-white",
    "icon": "cog",
    "defaultName": "Account Rules Management",
    "type": screenTypes.operation
  },
  "freezing-registration": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Shareholder data entry",
    "type": screenTypes.operation
  },
  "unfreezing-securities": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Unfreezing Securities",
    "type": screenTypes.operation
  },

  "shareholder-data-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Shareholder data entry",
    "type": screenTypes.operation
  },
  "tax-exempt-account": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Tax Exempt Account",
    "type": screenTypes.operation
  },

  "remove-tax-exempt-account": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Remove Tax Exempt Account",
    "type": screenTypes.operation
  },

  // Accounts [[[
  "investor-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Investor Account Opening",
    "type": screenTypes.operation
  },
  "investor-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Investor Data Change",
    "type": screenTypes.operation
  },

  "system-owner-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "system-owner-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "trading-member-account-modification": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "issuer-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "issuer-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "nominee-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "nominee-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "depository-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "depository-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "pledgee-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "pledgee-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "billing-member-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "billing-member-account-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "trading-member-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "trading-member-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "settlement-agent-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "settlement-agent-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "depository-member-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "depository-member-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },

  "clearing-member-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },
  "clearing-member-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "type": screenTypes.operation
  },


  // Accounts  ]]]
  "shareholder-data-entry-model": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Shareholder data entry",
    "type": screenTypes.operation
  },
  "issue-registration": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Issue registration",
    "type": screenTypes.operation
  },
  "issue-finalization": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Issue Finalization",
    "type": screenTypes.operation
  },
  "instrument-deletion": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instrument deletion",
    "type": screenTypes.operation
  },
  "subscription-access-management": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Subscription access management",
    "type": screenTypes.operation
  },
  "subscription-order-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Subscription order",
    "type": screenTypes.operation
  },

  "return-of-capital": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Return of capital",
    "type": screenTypes.operation
  },

  "cash-dividend": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Cash dividend",
    "type": screenTypes.operation
  },

  "haircut-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Haircut change",
    "type": screenTypes.operation
  },

  "parvalue-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Par value change",
    "type": screenTypes.operation
  },

  "instrument-conversion": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instrument conversion",
    "type": screenTypes.operation
  },
  "instrument-data-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instrument data entry",
    "type": screenTypes.operation
  },

  "bonus-shares": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Bonus shares",
    "type": screenTypes.operation
  },

  "stock-split": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Stock split",
    "type": screenTypes.operation
  },


  "demerger": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "De Merger",
    "type": screenTypes.operation

  },
  "merger": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Merger",
    "type": screenTypes.operation

  },

  "issuer-data-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Issuer data entry",
    "type": screenTypes.operation
  },

  "member-data-entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "user",
    "defaultName": "Member data entry",
    "type": screenTypes.operation
  },

  "currency": {
    "classes": "tile bg-crimson fg-white",
    "icon": "euro",
    "defaultName": "Currency management",
    "type": screenTypes.operation
  },
  "currency-rate-manual-entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "random",
    "defaultName": "Exchange rate management",
    "type": screenTypes.operation
  },
  "pledge-register": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Pledge registration",
    "type": screenTypes.operation

  },
  "grant-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Grant operation",
    "type": screenTypes.operation
  },
  "title-transfer-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Title transfer",
    "type": screenTypes.operation
  },
  "intra-account-transfer": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Share transfer between intra accounts",
    "type": screenTypes.operation
  },
  "correcting-database": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Correcting Database",
    "type": screenTypes.operation
  },
  "succession-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Succession operation",
    "type": screenTypes.operation
  },
  "otc-trade": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "OTC Trade",
    "type": screenTypes.operation
  },
  "enter-member-advance": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Enter member advance",
    "type": screenTypes.operation
  },
  "member-cross-trade-registration": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "OTC Trade",
    "type": screenTypes.operation
  },
  "title-assignment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Title Assignment",
    "type": screenTypes.operation
  },
  "buy-back": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Buy Back",
    "type": screenTypes.operation
  },

  "transfer-cancelled-shares-to-issuer-account": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Transfer Cancelled Shares To Issuer Account",
    "type": screenTypes.operation
  },
  "freezing-issuer-securities": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Freezing issuer securities",
    "type": screenTypes.operation
  },
  "freezing-shareholder-securities": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Unfreezing issuer securities",
    "type": screenTypes.operation
  },
  "unfreezing-issuer-securities": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Unfreezing issuer securities",
    "type": screenTypes.operation
  },
  "unfreezing-shareholder-securities": {
    "classes": "tile bg-crimson fg-white",
    "icon": "lock",
    "defaultName": "Unfreezing shareholder securities",
    "type": screenTypes.operation
  },
  "pledge-closure": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Pledge closure",
    "type": screenTypes.operation
  },
  "add-instrument-to-debtors-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Add Instruments To Debtors List",
    "type": screenTypes.operation
  },
  "add-instrument-to-commission-exception-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Add Instrument To Commission Exception List",
    "type": screenTypes.operation
  },

  "restore-cancelled-instrument": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Restore cancelled instrument",
    "type": screenTypes.operation
  },

  "add-exception-shareholder-to-debtors-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Add Exception Shareholder To Debtors List",
    "type": screenTypes.operation
  },
  "remove-exception-shareholder-from-debtors-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Remove Exception Shareholder From Debtors List",
    "type": screenTypes.operation
  },
  "add-instrument-to-black-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Add Instrument To Black List",
    "type": screenTypes.operation
  },

  "add-exception-shareholder-to-black-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Add Exception Shareholder To Black List",
    "type": screenTypes.operation
  },
  "remove-exception-shareholder-from-black-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Remove Exception Shareholder From Black List",
    "type": screenTypes.operation
  },
  "remove-instrument-from-debtors-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Remove Instrument From Debtors List",
    "type": screenTypes.operation
  },
  "cash-dividend-payment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Cash Dividend Payment",
    "type": screenTypes.operation
  },
  "configure-bis-model": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Configure BIS Model",
    "type": screenTypes.operation
  },
  "otc-deal-broker": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "OTC Deal Broker",
    "type": screenTypes.operation
  },

  "handle-mt103-payment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Handle MT103 Payments",
    "type": screenTypes.operation
  },

  "handle-mt102-payment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Handle MT103 Payments",
    "type": screenTypes.operation
  },

  "correct-payment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Correct Payment",
    "type": screenTypes.operation
  },

  "otc-trade-member": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "OTC Deal Broker",
    "type": screenTypes.operation
  },

  "approve-withdrawals-by-operation-department": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "OTC Deal Broker",
    "type": screenTypes.operation,
    "controller": 'ApproveWithdrawalsByOperationDepartmentController',
    "template": 'app/records/templates/approve-withdrawals-by-operation-department.html'
  },


  "return-unknown-payment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "OTC Deal Broker",
    "type": screenTypes.operation,
    "controller": 'ReturnUnknownPaymentOperationController',
    "template": 'app/records/templates/return_unknown_payment_operation.html'
  },


  "approve-withdrawals-by-chairman": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "OTC Deal Broker",
    "type": screenTypes.operation,
    "controller": 'ApproveWithdrawalsByChairmanController',
    "template": 'app/records/templates/approve-withdrawals-by-chairman.html'
  },


  "approve-matched-trade": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "OTC Deal Broker",
    "type": screenTypes.operation
  },
  "otc_trade_member_confirm": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "OTC Deal Broker",
    "type": screenTypes.operation
  },

  "confirm-matched-trade": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Confirm Matched Trade",
    "type": screenTypes.operation
  },
  "remove-instrument-from-black-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Remove Instrument From Debtors List",
    "type": screenTypes.operation
  },
  "remove-instrument-from-commission-exception-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Remove Instrument From Commission Exception List",
    "type": screenTypes.operation
  },
  "decrease-capital": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Remove Instrument From Commission Exception List",
    "type": screenTypes.operation
  },

  "view-authorizations": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "tree-deciduous",
    "defaultName": "View Authorizations",
    "type": screenTypes.view,
    "controller": 'ViewAuthorizationsController',
    "template": 'app/configurations/templates/view-authorizations.html'
  },

  "view-globus-transactions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Globus Transactions",
    "type": screenTypes.view,
    "controller": 'ViewGlobusTransactionsController',
    "template": 'app/globus/templates/view-globus-transactions.html'
  },
  "view-securities-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Securities Report",
    "type": screenTypes.view,
    "controller": 'ViewSecuritiesReportController',
    "template": 'app/reports/templates/view-securities-report.html'
  },

  "view-globus-global-operations": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Globus Transactions",
    "type": screenTypes.view,
    "controller": 'ViewGlobusGlobalOperationsController',
    "template": 'app/globus/templates/view-globus-global-operations.html'
  },
  "view-tax-exempt-accounts": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Tax Exempt Accounts",
    "type": screenTypes.view,
    "controller": 'ViewTaxExemptAccounts',
    "template": 'app/accounts/templates/view-tax-exempt-accounts.html'
  },

  // Accounts [[[
  "view-account-consolidations": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Account Consolidation",
    "type": screenTypes.view,
    "controller": 'ViewAccountConsolidationsController',
    "template": 'app/accounts/templates/view-account-consolidations.html'
  },

  "view-shareholders": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholders",
    "type": screenTypes.view,
    "controller": 'ShareholdersController',
    "template": 'app/persons/templates/shareholders.html'
  },

  "view-issuers": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Issuers",
    "type": screenTypes.view,
    "controller": 'IssuersController',
    "template": 'app/persons/templates/issuers.html'
  },

  "view-members": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Issuers",
    "type": screenTypes.view,
    "controller": 'MembersController',
    "template": 'app/persons/templates/persons.html'
  },
  "view-member-client-shareholders": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Member Client Shareholders",
    "type": screenTypes.view,
    "controller": 'MemberClientShareholdersController',
    "template": 'app/persons/templates/member-client-shareholders.html'
  },
  "view-persons": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'PersonsController',
    "template": 'app/persons/templates/persons.html'
  },
  //"view-persons": {
  //  "classes": "tile bg-crimson fg-white",
  //  "icon": "file",
  //  "defaultName": "Persons",
  //  "type": screenTypes.view,
  //  "controller": 'TestPersonsController',
  //  "template": 'app/persons/templates/test-persons.html'
  //},
  "view-all-member-money-positions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholders",
    "type": screenTypes.view,
    "controller": 'AllMemberMoneyPositionsController',
    "template": 'app/clearing/templates/all-member-money-positions.html'
  },

  // Clearing [[
  "view-member-advances": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View member advances",
    "type": screenTypes.view,
    "controller": 'ViewMemberAdvancesController',
    "template": 'app/clearing/templates/view-member-advances.html'
  },
  "entry-bank-statement": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Enter Bank Sstatement",
    "type": screenTypes.operation,
    "controller": 'EnterBankStatementController',
    "template": 'app/clearing/templates/enter-bank-statement.html'
  },
  "update-bank-statement": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Enter Bank Sstatement",
    "type": screenTypes.operation,
    "controller": 'UpdateBankStatementController',
    "template": 'app/clearing/templates/update-bank-statement.html'
  },
  "enter-payment-receipt": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Enter Payment Receipt",
    "type": screenTypes.operation,
    "controller": 'EnterPaymentReceiptController',
    "template": 'app/clearing/templates/enter-payment-receipt.html'
  },
  "enter-clearing-limit": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Enter Clearing Limit",
    "type": screenTypes.operation,
    "controller": 'EnterClearingLimitController',
    "template": 'app/clearing/templates/enter-clearing-limit.html'
  },
  "enter-withdrawal-order": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Enter Withdrawal Order",
    "type": screenTypes.operation,
    "controller": 'EnterWithdrawalOrderController',
    "template": 'app/clearing/templates/enter-withdrawal-order.html'
  },
  "early-settlement": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Early Settlement",
    "type": screenTypes.operation,
    "controller": 'EarlySettlementController',
    "template": 'app/clearing/templates/early-settlement.html'
  },
  "subscription-payment-manual-matching": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Subscription Payment Manual Matching",
    "type": screenTypes.operation,
    "controller": 'EarlySettlementController',
    "template": 'app/clearing/templates/subscription-payment-manual-matching.html'
  },

  "settlement-default-management": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Settlement Default Management",
    "type": screenTypes.operation,
    "controller": 'SettlementDefaultManagementController',
    "template": 'app/settlement/templates/settlement-default-management.html'
  },

  "withdrawal-subscription-money": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Withdrawal Subscription Money",
    "type": screenTypes.operation
  },

  "view-orders": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'OrdersController',
    "template": 'app/records/templates/orders.html'
  },
  "view-subscription-orders": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'SubscriptionOrdersController',
    "template": 'app/records/templates/subscription-orders.html'
  },
  "view-payments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'ViewPaymentsController',
    "template": 'app/records/templates/view-payments.html'
  },

  "view-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'TradesController',
    "template": 'app/records/templates/trades.html'
  },

  "view-shareholder-count-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholder count",
    "type": screenTypes.view,
    "controller": 'ViewShareholderCountReport',
    "template": 'app/records/templates/view-shareholder-count-report.html'
  },

  "view-mt-messages": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholder count",
    "type": screenTypes.view,
    "controller": 'ViewMTMessagesController',
    "template": 'app/records/templates/view-mt-messages.html'
  },


  "issuer-services-report": {
    // "member-services-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholder count",
    "type": screenTypes.view,
    "controller": 'IssuerServiceReportController',
    "template": 'app/reports/templates/issuer-service-report.html'
  },

  "member-services-report": {
    // "member-services-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholder count",
    "type": screenTypes.view,
    "controller": 'MemberServiceReportController',
    "template": 'app/reports/templates/member-service-report.html'
  },


  "ndc-azips-services-report": {
    // "member-services-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholder count",
    "type": screenTypes.view,
    "controller": 'NDCAzipsServiceReportController',
    "template": 'app/reports/templates/ndc-azips-service-report.html'
  },


  "view-account-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Shareholder count",
    "type": screenTypes.view,
    "controller": 'ViewAccountRecordsController',
    "template": 'app/records/templates/view-account-records.html'
  },

  "view-repo-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'ViewRepoTradesController',
    "template": 'app/records/templates/view-repo-trades.html'
  },

  "view-issuer-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'TradesController',
    "template": 'app/records/templates/trades.html'
  },
  "view-ibar-market-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'IbarMarketTradesController',
    "template": 'app/records/templates/ibar-market-trades.html'
  },

  "view-otc-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'OTCTradesController',
    "template": 'app/records/templates/OTC-trades.html'
  },
  "view-issuer-otc-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'IssuerOTCTradesController',
    "template": 'app/records/templates/issuer-otc-trades.html'
  },
  "view-ibar-otc-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'IbarOtcTradesController',
    "template": 'app/records/templates/ibar-otc-trades.html'
  },
  "view-market-trade-commissions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'MemberTradeCommissionController',
    "template": 'app/records/templates/member-trade-commission.html'
  },
  "view-otc-trade-commissions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'OTCTradeCommissionController',
    "template": 'app/records/templates/otc-trade-commission.html'
  },
  "view-account-opening-commissions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'AccountOpeningCommissionController',
    "template": 'app/records/templates/account-opening-commission.html'
  },
  "view-transfer-pledged-titles": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'TransferPledgedTitlesController',
    "template": 'app/records/templates/transfer-pledged-titles.html'
  },
  "view-successions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'SuccessionsController',
    "template": 'app/records/templates/succession.html'
  },
  "view-withdrawals": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'WithdrawalsController',
    "template": 'app/clearing/templates/withdrawals.html'
  },
  "view-swift-payments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Swift Payments",
    "type": screenTypes.view,
    "controller": 'SwiftPaymentsController',
    "template": 'app/clearing/templates/swift-payments.html'
  },
  //
  "view-net-obligations": {
    // "view-trades": {!@
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Clearing Limits",
    "type": screenTypes.view,
    "controller": 'NetObligationsController',
    "template": 'app/settlement/templates/net-obligations.html'
  },

  "view-freezing-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'FreezingRecordsController',
    "template": 'app/records/templates/freezing-records.html'
  },
  "view-unfreezing-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'FreezingRecordsController',
    "template": 'app/records/templates/freezing-records.html'
  },
  "view-grant-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'GrantRecordsController',
    "template": 'app/records/templates/grant-records.html'
  },

  "view-title-transfer-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'TitleTransferRecordsController',
    "template": 'app/records/templates/title-transfer-records.html'
  },
  "view-pledge-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'PledgeRecordsController',
    "template": 'app/records/templates/pledge-records.html'
  },
  "view-pledged-shares": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'PledgedSharesController',
    "template": 'app/records/templates/pledged-shares.html'
  },
  "view-net-instruments-obligations": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'NetInstrumentObligationsController',
    "template": 'app/settlement/templates/net-instrument-obligations.html'
  },


  "view-nonmatched-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'NonMatchedTradesController',
    "template": 'app/records/templates/non-matched-trades.html'
  },
  "view-matched-trades": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'MatchedTradesController',
    "template": 'app/records/templates/matched-trades.html'
  },

  //Clearing ]]

  // Reports [[
  "view-account-statements": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'AccountStatementsController',
    "template": 'app/reports/templates/account-statements.html'
  },


  "view-iamas-account": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": "ViewIamasAccountController",
    "template": "app/accounts/templates/view-iamas-account.html"
  },
  "view-holding-reports": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'HoldingReportsController',
    "template": 'app/reports/templates/account-statements.html'
  },
  "view-title-abstracts": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'TitleAbstractsController',
    "template": 'app/reports/templates/account-statements.html'
  },
  "view-local-issuers-foreign-traders-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'LocalIssuersForeignTradersReportController',
    "template": 'app/reports/templates/local-issuers-foreign-traders-report.html'
  },
  "view-bdd-monthly-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    //"defaultName": "View Business Development Department Monthly Report",
    "type": screenTypes.view,
    "controller": 'BusinessDevelopmentDepartmentMonthlyReportController',
    "template": 'app/reports/templates/business-development-department-monthly-report.html'
  },

  "view-quarterly-client-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Quarterly Client Report",
    "type": screenTypes.view,
    "controller": 'QuarterlyClientReportController',
    "template": 'app/reports/templates/quarterly-client-report.html'
  },
  "view-undelivered-sms-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Undelivered SMS Report",
    "type": screenTypes.view,
    "controller": 'UndeliveredSMSReportController',
    "template": 'app/reports/templates/undelivered-sms-report.html'
  },


  // Reports ]]

  "view-accounts": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Accounts",
    "type": screenTypes.view,
    "controller": 'AccountsController',
    "template": 'app/persons/templates/accounts.html'
  },
  "view-client-accounts": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Client Account",
    "type": screenTypes.view,
    "controller": 'ClientAccountsController',
    "template": 'app/clearing/templates/client-accounts.html'
  },
  "view-client-shareholders": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Client Account",
    "type": screenTypes.view,
    "controller": 'ClientShareholdersController',
    "template": 'app/clearing/templates/client-shareholders.html'
  },

  "view-bank-statements": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Client Account",
    "type": screenTypes.view,
    "controller": 'BankStatementsController',
    "template": 'app/clearing/templates/bank-statements.html'
  },
  "view-clearing-limits": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Clearing Limits",
    "type": screenTypes.view,
    "controller": 'ClearingLimitController',
    "template": 'app/clearing/templates/clearing-limits.html'
  },

  "view-money-positions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Money Positions",
    "type": screenTypes.view,
    "controller": 'MoneyPositionsController',
    "template": 'app/clearing/templates/money-positions.html'
  },
  "view-security-positions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Security Positions",
    "type": screenTypes.view,
    "controller": 'SecurityPositionsController',
    "template": 'app/clearing/templates/security-positions.html'
  },

  "view-nominees": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'NomineesController',
    "template": 'app/persons/templates/persons.html'
  },
  "view-depositories": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'DepositoriesController',
    "template": 'app/persons/templates/persons.html'
  },
  "view-pledgees": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'PledgeesController',
    "template": 'app/persons/templates/persons.html'
  },
  "view-billing-members": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'BillingMembersController',
    "template": 'app/persons/templates/persons.html'
  },

  "view-settlement-agents": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'SettlementAgentsController',
    "template": 'app/persons/templates/persons.html'
  },
  "view-depository-members": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'DepositoryMembersController',
    "template": 'app/persons/templates/persons.html'
  },
  "view-clearing-members": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'ClearingMembersController',
    "template": 'app/persons/templates/persons.html'
  },
  "view-trading-members": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Persons",
    "type": screenTypes.view,
    "controller": 'TradingMembersController',
    "template": 'app/persons/templates/persons.html'
  },

  "view-issuer-instruments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'IssuerInstrumentsController',
    "template": 'app/instruments/templates/view-issuer-instruments.html'
  },
  "view-privatization-instruments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Privatization instruments",
    "type": screenTypes.view,
    "controller": 'PrivatizationInstrumentsController',
    "template": 'app/instruments/templates/view-privatization-instruments.html'
  },
  "view-account-prescriptions": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'AccountPrescriptionsController',
    "template": 'app/accounts/templates/view-account-prescriptions.html'
  },

  //"view-account-prescriptions": {
  //
  "view-underwriters": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'UnderwritersController',
    "template": 'app/accounts/templates/view-underwriters.html'
  },

  "view-incoming-payment-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'ViewIncomingPaymentRecordsController',
    "template": 'app/records/templates/incoming-payment-records.html'
  },

  "view-test-azips": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'ViewAzipsViewController',
    "template": 'app/records/templates/view-azips-test.html'
  },


  "view-account-cash-balances": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'ViewAccountCashBalancesController',
    "template": 'app/records/templates/view-account-cash-balances.html'
  },


  "view-unknown-payments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'ViewUnknownPaymentRecordsController',
    "template": 'app/records/templates/unknown-payment-records.html'
  },


  "view-outgoing-payment-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'ViewOutgoingPaymentRecordsController',
    "template": 'app/records/templates/view-outgoing-payment-records.html'
  },


  "view-instruments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'ViewInstrumentsController',
    "template": 'app/instruments/templates/view-instruments.html'
  },
  "view-instrument-data-change-history": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Instruments",
    "type": screenTypes.view,
    "controller": 'ViewInstrumentDataChangeHistoryController',
    "template": 'app/instruments/templates/view-instrument-data-change-history.html'
  },
  "view-instruments-in-debtors-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Instruments In Debtors List",
    "type": screenTypes.view,
    "controller": 'InstrumentsInDebtorsListController',
    "template": 'app/instruments/templates/view-instruments-in-debtors-list.html'
  },
  "view-instruments-in-black-list": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "View Instruments In Debtors List",
    "type": screenTypes.view,
    "controller": 'InstrumentsInBlackListController',
    "template": 'app/instruments/templates/view-instruments-in-black-list.html'
  },

  "active-tasks": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "My Active Tasks",
    "type": screenTypes.view,
    "controller": 'MyActiveTasksController',
    "template": 'app/main/templates/my-active-tasks.html'
  },
  "completed-tasks": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "My Completed Tasks",
    "type": screenTypes.view,
    "controller": 'MyCompletedTasksController',
    "template": 'app/main/templates/my-completed-tasks.html'
  },
  "candidate-tasks": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "My Candidate Tasks",
    "type": screenTypes.view,
    "controller": 'MyCandidateTasksController',
    "template": 'app/main/templates/my-candidate-tasks.html'
  },
  "finish-tasks": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Finished tasks",
    "type": screenTypes.view,
    "controller": 'FinishedTasksController',
    "template": 'app/main/templates/finished-tasks.html'
  },
  "view-controlled-processes": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "My Controlled Tasks",
    "type": screenTypes.view,
    "controller": 'MyControlledTasksController',
    "template": 'app/main/templates/my-controlled-tasks.html'
  },
  "corporate-action": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Corporate actions",
    "type": screenTypes.view,
    "controller": 'ViewCorporateActionsController',
    "template": 'app/corporate-actions/templates/view-corporate-actions.html'
  },
  "send-batch-data": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'SendBatchDataController',
    "template": 'app/temp/templates/send-batch-data.html'
  },
  "issue-account-statement": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Corporate actions",
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatement',
    "template": 'app/reports/templates/issue-account-statement.html'
  },
  "issue-account-statement-member": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Corporate actions",
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatementMember',
    "template": 'app/reports/templates/issue-account-statement-member.html'
  },
  "issue-holding-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Corporate actions",
    "type": screenTypes.operation,
    "controller": 'IssueHoldingReport',
    "template": 'app/reports/templates/issue-holding-report.html'
  },
  "issue-title-abstract": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Corporate actions",
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractController',
    "template": 'app/reports/templates/issue-title-abstract.html'
  },

  "issue-title-abstract-member": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Issue title abstract",
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractMemberController',
    "template": 'app/reports/templates/issue-title-abstract.html'
  },

  "client-account-opening": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'ClientAccountOpeningController',
    "template": 'app/accounts/templates/client-account-opening.html'
  },

  "underwriter-contract": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'UnderwriterContractController',
    "template": 'app/accounts/templates/client-account-opening.html'
  },
  "account-suspension": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Account suspension",
    "type": screenTypes.operation,
    "controller": 'AccountSuspensionController',
    "template": 'app/accounts/templates/account-suspension.html'
  },
  "account-unsuspension": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Account suspension",
    "type": screenTypes.operation,
    "controller": 'AccountUnSuspensionController',
    "template": 'app/accounts/templates/account-unsuspension.html'
  },
  "account-deletion": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Account deletion",
    "type": screenTypes.operation,
    "controller": 'AccountDeletionController',
    "template": 'app/accounts/templates/account-deletion.html'
  },

  "account-consolidation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Account deletion",
    "type": screenTypes.operation,
    "controller": 'AccountConsolidationController',
    "template": 'app/accounts/templates/account-consolidation.html'
  },
  // Accounts ]]


  "modify-bond-cash-flow": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Modify Bond cash flow",
    "type": screenTypes.operation,
    "controller": 'ModifyBondCashFlowController',
    "template": 'app/accounts/templates/modify-bond-cash-flow.html'
  },
  "batch-entryOfAllotment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Corporate actions",
    "type": screenTypes.operation,
    "controller": 'BatchEntryOfAllotmentController',
    "template": 'app/records/templates/batch-entry-allotment.html'
  },
  "batch-entry-of-auction": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Auction Entry actions",
    "type": screenTypes.operation
  },
  "auction-finalization": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Auction Finalization",
    "type": screenTypes.operation
  },
  "manage-bank-info": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Auction Finalization",
    "type": screenTypes.operation,
    "controller": 'ManageBankInfoController',
    "template": 'app/currency/templates/manage-bank-info.html'
  },

  "organization-structure-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "tree-deciduous",
    "defaultName": "Organization Structure Management",
    "type": screenTypes.operation,
    "controller": 'OrgNodeStructureManagementController',
    "template": 'app/configurations/templates/org-node-structure-management.html'
  },

  "employee-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "tree-deciduous",
    "defaultName": "Employee Management",
    "type": screenTypes.operation,
    "controller": 'EmployeeManagementController',
    "template": 'app/configurations/templates/employee-management.html'
  },
  "user-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "tree-deciduous",
    "defaultName": "User Management",
    "type": screenTypes.operation,
    "controller": 'UserManagementController',
    "template": 'app/configurations/templates/user-management.html'
  },
  "authorization-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "tree-deciduous",
    "defaultName": "Authorization Management",
    "type": screenTypes.operation,
    "controller": 'AuthorizationManagementController',
    "template": 'app/configurations/templates/authorization-management.html'
  },
  "operation-parameters-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "tree-deciduous",
    "defaultName": "Operation Parameters Management",
    "type": screenTypes.operation,
    "controller": 'OperationParametersManagementController',
    "template": 'app/configurations/templates/operation-parameters-management.html'
  },
  "case-processing": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "tree-deciduous",
    "defaultName": "Case Processing",
    "type": screenTypes.operation,
    "controller": 'CaseProcessingController',
    "template": 'app/configurations/templates/case-processing.html'
  },

  "business-calendar-management": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Business Calendar Management",
    "type": screenTypes.operation,
    "controller": 'BusinessCalendarManagementController',
    "template": 'app/configurations/templates/business-calendar-management.html'
  },
  "transfer-pledged-title": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer Pledged Title",
    "type": screenTypes.operation,
    "controller": 'TransferPledgedTitleController',
    "template": 'app/records/templates/transfer-pledged-title.html'
  },
  "juridical-client-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Juridical Client Data Change",
    "type": screenTypes.operation
  },
  "juridical-client-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Juridical Client Account Opening",
    "type": screenTypes.operation
  },
  "natural-client-data-change": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Natural Client Data Change",
    "type": screenTypes.operation
  },
  "natural-client-account-opening": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Natural Client Account Opening",
    "type": screenTypes.operation
  },
  "pledge-reregistration": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Pledge Reregistration",
    "type": screenTypes.operation
  },
  //"case-processing": {
  //  "classes": "tile tile-wide bg-crimson fg-white",
  //  "icon": "tree-deciduous",
  //  "defaultName": "Case Processing",
  //  "type": screenTypes.operation,
  //  "controller": 'CaseProcessingController',
  //  "template": 'app/temp/templates/test-task.html'
  //},
  "pledge-endorsement": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Pledge Endorsement",
    "type": screenTypes.operation
  },
  "pledge-transfer": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Pledge Transfer",
    "type": screenTypes.operation
  },
  "pledge-invocation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Pledge Invocation",
    "type": screenTypes.operation
  },
  "manage-order-whitelist": {
    "classes": "tile bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Manage Order Whitelist",
    "type": screenTypes.operation
  },
  "manage-operation-positions": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Manage Operation Positions",
    "type": screenTypes.operation,
    "controller": 'ManageOperationPositionsController',
    "template": 'app/configurations/templates/manage-operation-positions.html'
  },
  "business-day-opening": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Business Day Opening",
    "type": screenTypes.operation,
    "controller": 'BusinessDayOpeningController',
    "template": 'app/corporate-actions/templates/business-day-opening.html'
  },
  "business-day-closing": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Business Day Closing",
    "type": screenTypes.operation,
    "controller": 'BusinessDayClosingController',
    "template": 'app/corporate-actions/templates/business-day-closing.html'
  },
  "withdrawal-cash": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "remove",
    "defaultName": "Withdrawal Cash",
    "type": screenTypes.operation
  },
  "transfer-to-issuer-account": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'TransferToIssuerAccountController',
    "template": 'app/records/templates/transfer-to-issuer-account.html'
  },


  "cancel-operation": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelTasksController',
    "template": '/app/configurations/templates/cancel-tasks.html'
  },


  "cancel-incompleted-operation": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelIncompletedOperationController',
    "template": '/app/records/templates/cancel-incompleted-operation.html'
  },
  "shareholder-ui-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagmentController",
    "template": "app/management/templates/shareholder_ui_managment_entry.html"
  },
  "natural-person-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "NaturalPersonManagementController",
    "template": "app/management/templates/natural_person_management_entry.html"
    // "controller": "TestController",
    // "template": 'app/person-account/templates/test.html'
  },
  "issuer-ui-management": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagmentController",
    "template": "app/management/templates/shareholder_ui_managment_entry.html"
  },
  "generate-anna-master-file": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "GenerateAnnaMasterFileController",
    "template": "/app/anna/templates/generate-anna-master-file-entry.html"
  },
  "distribute": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "DistributeCouponPaymentTaskController",
    "template": "app/instruments/templates/distribute-coupon-payment-task-approval.html"
  },


  "notary-trade": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'NotaryTradeController',
    "template": 'app/corporate-actions/templates/notary-trade.html'
  },

  "notary-settlement-trade": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'NotarySettlementTradeController',
    "template": 'app/corporate-actions/templates/notary-settlement-trade.html'
  },


  "view-coupon-payments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "type": screenTypes.view,
    "controller": 'ViewCouponPaymentController',
    "template": 'app/instruments/templates/view-coupon-payment-instrument.html'
  },


  "outgoing-payment-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'OutgoingPaymentController',
    "template": 'app/records/templates/outgoing-payment-entry.html'
  },


  "send-error-coupon-payments": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'SendErrorCouponPaymentController',
    "template": 'app/corporate-actions/templates/send-error-coupon-payments.html'
  },


  "issuer-coupon-payment": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'IssuerCouponPaymentController',
    "template": 'app/corporate-actions/templates/issuer-coupon-payment.html'
  },

  "withdrawal-ndc-clearing-cash": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'WithdrawalNdcClearingCashController',
    "template": 'app/clearing/templates/withdrawal-ndc-clearing-cash.html'
  },


  "coupon-payment-schedule-manager": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'CouponPaymentScheduleManagerController',
    "template": 'app/corporate-actions/templates/coupon-payment-schedule-manager.html'
  },


  "incoming-payment-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'AssignMemberController',
    "template": 'app/clearing/templates/assign-member.html'
  },

  "coupon-payment-schedule-test": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'AssignAccountAndServiceClassController',
    "template": 'app/clearing/templates/assign-account-and-service-class.html'
  },


  "withdrawal-VAT-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'AssignAccountAndServiceClassController',
    "template": 'app/clearing/templates/assign-account-and-service-class.html'
  },


  "unknown-withdrawal-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'UnknownWithdrawalOperationController',
    "template": 'app/records/templates/approve-withdrawals-by-chairman.html'
  },


  "withdraw-forex-amount": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'WithdrawalForexAmountController',
    "template": 'app/records/templates/approve-withdrawals-by-chairman.html'
  },

  "transfer-cash-between-internal-accounts": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'TransferCashBetweenInternalAccountsController',
    "template": 'app/records/templates/transfer-cash-between-internal-accounts.html'
  },

  "print-payment-order": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'PrintPaymentOrderController',
    "template": 'app/clearing/templates/print-payment-order.html'
  },

  "commission-limit-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'CommissionLimitController',
    "template": 'app/clearing/templates/commission-limit.html'
  },

  "withdraw-revenue-operation": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'WithdrawalRevenueOperationController',
    "template": 'client/app/clearing/templates/client-shareholders.html'
  },

  "cbar-day-close-balance": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'ViewCbarCloseBalanceController',
    "template": 'app/clearing/templates/cbar-day-close-balance.html'
  },

  "foreign-account-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'ForeignAccountRecordsController',
    "template": 'app/records/templates/foreign-account-records.html'
  },

  "local-account-records": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'LocalAccountRecordsController',
    "template": 'app/records/templates/local-account-records.html'
  },

  "remaining-balances": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'RemainingBalancesController',
    "template": 'app/records/templates/remaining-balances.html'
  },

  "shareholder-account-statement": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'shareholderAccountStatementController',
    "template": 'app/records/templates/shareholder-account-statement.html'
  },

  "service-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'ServiceReportController',
    "template": 'app/records/templates/service-report.html'
  },

  "cbar-cash-record": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'CbarCashRecordController',
    "template": 'app/records/templates/cbar-cash-record.html'
  },


  "tax-record": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'TaxRecordController',
    "template": 'app/records/templates/tax-record.html'
  },
  "tax-record2": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'TaxRecord2Controller',
    "template": 'app/records/templates/tax-record.html'
  },

  "ndc-cbar-report": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.view,
    "controller": 'NDCCbarController',
    "template": 'app/reports/templates/ndc-cbar-report.html'
  },

  "transfer-cash-from-client-account": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'TransferCashFromClientAccountController',
    "template": 'app/clearing/templates/transfer-cash-from-client-account.html'
  }
};


var allOperationsData = {

    "trs_batch_process_print": {
        "type": screenTypes.operation,
        "controller": "TRSBatchProcessPrintController",
        "template": "app/temp/templates/trs_batch_process_print.html"
    },

  "handle_mt103_payment": {
    "type": screenTypes.operation,
    "controller": 'HandleMT103PaymentEntryController',
    "template": 'app/records/templates/handle-mt103-payment-entry.html'
  },

  "handle_mt102_payment": {
    "type": screenTypes.operation,
    "controller": 'HandleMT102PaymentEntryController',
    "template": 'app/records/templates/handle-mt102-payment-entry.html'
  },

  "correct_payment_entry": {
    "type": screenTypes.operation,
    "controller": 'CorrectPaymentEntryController',
    "template": 'app/clearing/templates/correct-payment-entry.html'
  },
  "correct_payment_approve": {
    "type": screenTypes.operation,
    "controller": 'CorrectPaymentApprovalController',
    "template": 'app/clearing/templates/correct-payment-approval.html'
  },

  "global_configuration_entry": {
    "type": screenTypes.operation,
    "controller": 'GlobalConfigurationsEntryController',
    "template": 'app/configurations/templates/global-configurations.html'
  },
  "global_configuration_approve": {
    "type": screenTypes.operation,
    "controller": 'GlobalConfigurationsEntryApprovalController',
    "template": 'app/configurations/templates/global-configurations-approval.html'
  },

  "role_rules_management_entry": {
    "type": screenTypes.operation,
    "controller": 'RoleRulesManagementEntryController',
    "template": 'app/configurations/templates/role-rules-management.html'
  },

  "role_rules_management_approve": {
    "type": screenTypes.operation,
    "controller": 'RoleRulesManagementApprovalController',
    "template": 'app/configurations/templates/role-rules-management-approval.html'
  },

  "account_rules_management_entry": {
    "type": screenTypes.operation,
    "controller": 'AccountRulesManagementEntryController',
    "template": 'app/configurations/templates/account-rules-management.html'
  },

  "account_rules_management_approve": {
    "type": screenTypes.operation,
    "controller": 'AccountRulesManagementApprovalController',
    "template": 'app/configurations/templates/account-rules-management-approval.html'
  },

  "person_data_field_entry": {
    "type": screenTypes.operation,
    "controller": 'PersonDataFieldRuleEntryController',
    "template": 'app/configurations/templates/person-data-field-rule.html'
  },

  "person_data_field_approve": {
    "type": screenTypes.operation,
    "controller": 'PersonDataFieldRuleEntryApprovalController',
    "template": 'app/configurations/templates/person-data-field-rule-approval.html'
  },

  // Reports [[
  "issue_account_statement_entry": {
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatement',
    "template": 'app/reports/templates/issue-account-statement.html'
  },

  "issue_account_statement_approve": {
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatementControllerApproval',
    "template": 'app/reports/templates/issue-account-statement-approval.html'
  },

  "issue_account_statement_print": {
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatementPrintController',
    "template": 'app/reports/templates/issue-account-statement-print.html'
  },
  "shareholder_ui_management_print": {
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagementPrintController",
    "template": "app/management/templates/shareholder_ui_management_print.html"
  },
  "issuer_ui_management_print": {
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagementPrintController",
    "template": "app/management/templates/shareholder_ui_management_print.html"
  },


  "issue_account_statement_member_entry": {
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatementMember',
    "template": 'app/reports/templates/issue-account-statement-member.html'
  },

  "issue_account_statement_member_approve": {
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatementMemberControllerApproval',
    "template": 'app/reports/templates/issue-account-statement-member-approval.html'
  },

  "issue_account_statement_member_print": {
    "type": screenTypes.operation,
    "controller": 'IssueAccountStatementMemberPrintController',
    "template": 'app/reports/templates/issue-account-statement-member-print.html'
  },


  "issue_holding_report_entry": {
    "type": screenTypes.operation,
    "controller": 'IssueHoldingReportController',
    "template": 'app/reports/templates/issue-holding-report.html'
  },
  "issue_holding_report_approve": {
    "type": screenTypes.operation,
    "controller": 'IssueHoldingReportApprovalController',
    "template": 'app/reports/templates/issue-holding-report-approval.html'
  },
  "issue_holding_report_print": {
    "type": screenTypes.operation,
    "controller": 'IssueHoldingReportPrintController',
    "template": 'app/reports/templates/issue-holding-report-print.html'
  },

  "title_abstract_entry": {
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractController',
    "template": 'app/reports/templates/issue-title-abstract.html'
  },
  "title_abstract_approve": {
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractApprovalController',
    "template": 'app/reports/templates/issue-title-abstract-approval.html'
  },
  "title_abstract_print": {
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractPrintController',
    "template": 'app/reports/templates/issue-title-abstract-print.html'
  },

  "issue_title_abstract_member_entry": {
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractMemberController',
    "template": 'app/reports/templates/issue-title-abstract-member.html'
  },
  "issue_title_abstract_member_approve": {
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractMemberApprovalController',
    "template": 'app/reports/templates/issue-title-abstract-member-approval.html'
  },
  "issue_title_abstract_member_print": {
    "type": screenTypes.operation,
    "controller": 'IssueTitleAbstractMemberPrintController',
    "template": 'app/reports/templates/issue-title-abstract-member-print.html'
  },

  "client_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ClientAccountOpeningController',
    "template": 'app/accounts/templates/client-account-opening.html'
  },
  "distribute_approve": {
    "type": screenTypes.operation,
    "controller": "DistributeCouponPaymentTaskController",
    "template": "app/instruments/templates/distribute-coupon-payment-task-approval.html"
  },

  "client_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ClientAccountOpeningApprovalController',
    "template": 'app/accounts/templates/client-account-opening-approval.html'
  },


  "underwriter_contract_entry": {
    "type": screenTypes.operation,
    "controller": 'UnderwriterContractController',
    "template": 'app/accounts/templates/underwriter-contract.html'
  },

  "underwriter_contract_approve": {
    "type": screenTypes.operation,
    "controller": 'ClientAccountOpeningApprovalController',
    "template": 'app/accounts/templates/underwriter-contract-approval.html'
  },

  "underwriter_contract_print": {
    "type": screenTypes.operation,
    "controller": 'ClientAccountOpeningApprovalController',
    "template": 'app/accounts/templates/client-account-opening-print.html'
  },
  "client_account_opening_print": {
    "type": screenTypes.operation,
    "controller": 'ClientAccountOpeningApprovalController',
    "template": 'app/accounts/templates/client-account-opening-print.html'
  },

  "account_suspension_entry": {
    "type": screenTypes.operation,
    "controller": 'AccountSuspensionController',
    "template": 'app/accounts/templates/account-suspension.html'
  },

  "account_suspension_approve": {
    "type": screenTypes.operation,
    "controller": 'AccountSuspensionApprovalController',
    "template": 'app/accounts/templates/account-suspension-approval.html'
  },
  "account_unsuspension_entry": {
    "type": screenTypes.operation,
    "controller": 'AccountUnSuspensionController',
    "template": 'app/accounts/templates/account-unsuspension.html'
  },

  "account_unsuspension_approve": {
    "type": screenTypes.operation,
    "controller": 'AccountUnSuspensionApprovalController',
    "template": 'app/accounts/templates/account-unsuspension-approval.html'
  },

  "account_deletion_entry": {
    "type": screenTypes.operation,
    "controller": 'AccountDeletionController',
    "template": 'app/accounts/templates/account-deletion.html'
  },

  "account_deletion_approve": {
    "type": screenTypes.operation,
    "controller": 'AccountDeletionApprovalController',
    "template": 'app/accounts/templates/account-deletion-approval.html'
  },

  "account_consolidation_entry": {
    "type": screenTypes.operation,
    "controller": 'AccountConsolidationController',
    "template": 'app/accounts/templates/account-consolidation.html'
  },

  "account_consolidation_approve": {
    "type": screenTypes.operation,
    "controller": 'AccountConsolidationApprovalController',
    "template": 'app/accounts/templates/account-consolidation-approval.html'
  },

  // Accounts ]]

  // Clearing [[
  "entry_bank_statement_entry": {
    "type": screenTypes.operation,
    "controller": 'EnterBankStatementController',
    "template": 'app/clearing/templates/enter-bank-statement.html'
  },

  "entry_bank_statement_approve": {
    "type": screenTypes.operation,
    "controller": 'EnterBankStatementApprovalController',
    "template": 'app/clearing/templates/enter-bank-statement-approval.html'
  },

  "update_bank_statement_entry": {
    "type": screenTypes.operation,
    "controller": 'UpdateBankStatementController',
    "template": 'app/clearing/templates/update-bank-statement.html'
  },

  "update_bank_statement_approve": {
    "type": screenTypes.operation,
    "controller": 'UpdateBankStatementApprovalController',
    "template": 'app/clearing/templates/update-bank-statement-approval.html'
  },

  "enter_payment_receipt_entry": {
    "type": screenTypes.operation,
    "controller": 'EnterPaymentReceiptController',
    "template": 'app/clearing/templates/enter-payment-receipt.html'
  },

  "enter_payment_receipt_approve": {
    "type": screenTypes.operation,
    "controller": 'EnterPaymentReceiptApprovalController',
    "template": 'app/clearing/templates/enter-payment-receipt-approval.html'
  },

  "enter_clearing_limit_entry": {
    "type": screenTypes.operation,
    "controller": 'EnterClearingLimitController',
    "template": 'app/clearing/templates/enter-clearing-limit.html'
  },

  "enter_clearing_limit_approve": {
    "type": screenTypes.operation,
    "controller": 'EnterClearingLimitApprovalController',
    "template": 'app/clearing/templates/enter-clearing-limit-approval.html'
  },

  // #TODO
  "enter_withdrawal_order_entry": {
    "type": screenTypes.operation,
    "controller": 'EnterWithdrawalOrderController',
    "template": 'app/clearing/templates/enter-withdrawal-order.html'
  },
  "enter_withdrawal_order_approve": {
    "type": screenTypes.operation,
    "controller": 'EnterWithdrawalOrderApprovalController',
    "template": 'app/clearing/templates/enter-withdrawal-order-approval.html'
  },

  "early_settlement_entry": {
    "type": screenTypes.operation,
    "controller": 'EarlySettlementController',
    "template": 'app/clearing/templates/early-settlement.html'
  },
  "early_settlement_approve": {
    "type": screenTypes.operation,
    "controller": 'EarlySettlementApprovalController',
    "template": 'app/clearing/templates/early-settlement-approval.html'
  },
  "subscription_payment_manual_matching_entry": {
    "type": screenTypes.operation,
    "controller": 'SubscriptionPaymentManualMatchingController',
    "template": 'app/clearing/templates/subscription-payment-manual-matching.html'
  },
  "subscription_payment_manual_matching_approve": {
    "type": screenTypes.operation,
    "controller": 'SubscriptionPaymentManualMatchingApprovalController',
    "template": 'app/clearing/templates/subscription-payment-manual-matching-approval.html'
  },
  // Clearing ]]

  // Settlement [[
  "settlement_default_management_entry": {
    "type": screenTypes.operation,
    "controller": 'SettlementDefaultManagementController',
    "template": 'app/settlement/templates/settlement-default-management.html'
  },
  "settlement_default_management_approve": {
    "type": screenTypes.operation,
    "controller": 'SettlementDefaultManagementApprovalController',
    "template": 'app/settlement/templates/settlement-default-management-approval.html'
  },

  //"default_management_entry": {
  //  "type": screenTypes.operation,
  //  "controller": 'DefaultManagementController',
  //  "template": 'app/settlement/templates/default-management.html'
  //},
  //"default_management_approve": {
  //  "type": screenTypes.operation,
  //  "controller": 'DefaultManagementApprovalController',
  //  "template": 'app/clearing/templates/default-management-approval.html'
  //},
  //Settlement ]]

  // Records [[
  "modify_bond_cash_flow_entry": {
    "type": screenTypes.operation,
    "controller": 'ModifyBondCashFlowController',
    "template": 'app/corporate-actions/templates/modify-bond-cash-flow.html'
  },

  "modify_bond_cash_flow_approve": {
    "type": screenTypes.operation,
    "controller": 'ModifyBondCashFlowApprovalController',
    "template": 'app/corporate-actions/templates/modify-bond-cash-flow-approval.html'
  },

  "batch_entryOfAllotment_entry": {
    "type": screenTypes.operation,
    "controller": 'BatchEntryOfAllotmentController',
    "template": 'app/records/templates/batch-entry-of-allotment.html'
  },

  "batch_entryOfAllotment_approve": {
    "type": screenTypes.operation,
    "controller": 'BatchEntryOfAllotmentApprovalController',
    "template": 'app/records/templates/batch-entry-of-allotment-approval.html'
  },

  //"batch_entryOfAllotment_entry": {
  "batch_entry_of_auction_entry": {
    "type": screenTypes.operation,
    "controller": 'BatchEntryOfAllotmentController',
    "template": 'app/records/templates/batch-entry-of-auction.html'
  },

  "batch_entry_of_auction_approve": {
    "type": screenTypes.operation,
    "controller": 'BatchEntryOfAllotmentApprovalController',
    "template": 'app/records/templates/batch-entry-of-auction-approval.html'
  },

  "auction_finalization_entry": {
    "type": screenTypes.operation,
    "controller": 'AuctionFinalizationController',
    "template": 'app/records/templates/auction-finalization.html'
  },

  "auction_finalization_approve": {
    "type": screenTypes.operation,
    "controller": 'AuctionFinalizationApprovalController',
    "template": 'app/records/templates/auction-finalization-approval.html'
  },

  "organization_structure_management_entry": {
    "type": screenTypes.operation,
    "controller": 'OrgNodeStructureManagementController',
    "template": 'app/configurations/templates/org-node-structure-management.html'
  },
  "organization_structure_management_approve": {
    "type": screenTypes.operation,
    "controller": 'OrgNodeStructureManagementApprovalController',
    "template": 'app/configurations/templates/org-node-structure-management-approval.html'
  },


  "employee_management_entry": {
    "type": screenTypes.operation,
    "controller": 'EmployeeManagementController',
    "template": 'app/configurations/templates/employee-management.html'
  },
  "employee_management_approve": {
    "type": screenTypes.operation,
    "controller": 'EmployeeManagementApprovalController',
    "template": 'app/configurations/templates/employee-management-approval.html'
  },

  "user_management_entry": {
    "type": screenTypes.operation,
    "controller": 'UserManagementController',
    "template": 'app/configurations/templates/user-management.html'
  },
  "user_management_approve": {
    "type": screenTypes.operation,
    "controller": 'UserManagementApprovalController',
    "template": 'app/configurations/templates/user-management-approval.html'
  },

  "authorization_management_entry": {
    "type": screenTypes.operation,
    "controller": 'AuthorizationManagementController',
    "template": 'app/configurations/templates/authorization-management.html'
  },
  "authorization_management_approve": {
    "type": screenTypes.operation,
    "controller": 'AuthorizationManagementApprovalController',
    "template": 'app/configurations/templates/authorization-management-approval.html'
  },
  "operation_parameters_management_entry": {
    "type": screenTypes.operation,
    "controller": 'OperationParametersManagementController',
    "template": 'app/configurations/templates/operation-parameters-management.html'
  },
  "operation_parameters_management_approve": {
    "type": screenTypes.operation,
    "controller": 'OperationParametersManagementApprovalController',
    "template": 'app/configurations/templates/operation-parameters-management-approval.html'
  },

  "case_processing_entry": {
    "type": screenTypes.operation,
    "controller": 'CaseProcessingController',
    "template": 'app/configurations/templates/case-processing.html'
  },

  "case_processing_approve": {
    "type": screenTypes.operation,
    "controller": 'CaseProcessingApprovalController',
    "template": 'app/configurations/templates/case-processing-approval.html'
  },

  "case_processing_print": {
    "type": screenTypes.operation,
    "controller": 'CaseProcessingPrintController',
    "template": 'app/configurations/templates/case-processing-print.html'
  },

  "business_calendar_management_entry": {
    "type": screenTypes.operation,
    "controller": 'BusinessCalendarManagementController',
    "template": 'app/configurations/templates/business-calendar-management.html'
  },
  "business_calendar_management_approve": {
    "type": screenTypes.operation,
    "controller": 'BusinessCalendarManagementApprovalController',
    "template": 'app/configurations/templates/business-calendar-management-approval.html'
  },


  // Person Data Entry [[
  "remove_tax_exempt_account_entry": {
    "type": screenTypes.operation,
    'controller': 'RemoveTaxExemptAccountController',
    "template": 'app/accounts/templates/remove-tax-exempt-account.html'
  },
  "remove_tax_exempt_account_approve": {
    'type': screenTypes.operation,
    'controller': 'RemoveTaxExemptAccountApproveController',
    'template': 'app/accounts/templates/remove-tax-exempt-account-approval.html'
  },
  "remove_tax_exempt_account_second_approve": {
    'type': screenTypes.operation,
    'controller': 'RemoveTaxExemptAccountApproveController',
    'template': 'app/accounts/templates/remove-tax-exempt-account-approval.html'
  },
  "tax_exempt_account_entry": {
    'type': screenTypes.operation,
    'controller': 'TaxExemptAccountController',
    'template': 'app/accounts/templates/tax-exempt-account.html'
  },
  "tax_exempt_account_approve": {
    'type': screenTypes.operation,
    'controller': 'TaxExemptAccountApproveController',
    'template': 'app/accounts/templates/tax-exempt-account-approval.html'
  },
  "tax_exempt_account_second_approve": {
    'type': screenTypes.operation,
    'controller': 'TaxExemptAccountApproveController',
    'template': 'app/accounts/templates/tax-exempt-account-approval.html'
  },

  "investor_account_opening_entry": {
    "type": screenTypes.operation,

    // "template": "app/person-account/templates/iamas-add-person-template.html",
    // "controller": 'IamasAddPersonTemplateController'


    // on prudtion these properties must be included
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "investor_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "investor_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "investor_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "system_owner_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "system_owner_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "system_owner_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "system_owner_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "trading_member_account_modification_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "trading_member_account_modification_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "issuer_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "issuer_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "issuer_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "issuer_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },


  "nominee_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "nominee_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "nominee_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "nominee_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "depository_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "depository_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "depository_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "depository_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "pledgee_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "pledgee_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "pledgee_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "pledgee_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "billing_member_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "billing_member_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "billing_member_account_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "billing_member_account_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "trading_member_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "trading_member_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "person_data_print": {
    "type": screenTypes.operation,
    "controller": 'PersonDataPrintController',
    "template": 'app/person-account/templates/person-data-print.html'
  },

  "trading_member_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "trading_member_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "settlement_agent_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "settlement_agent_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "settlement_agent_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "settlement_agent_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },


  "depository_member_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "depository_member_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "depository_member_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "depository_member_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "clearing_member_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "clearing_member_account_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },
  "clearing_member_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "clearing_member_data_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  //]]


  // Entry tasks
  //"shareholder_data_entry_task": {
  //  "type": screenTypes.operation,
  //  "controller": 'ManagePersonDataController',
  //  "template": 'app/person-account/templates/manage-shareholder-data.html'
  //},

  //"shareholder_data_entry": {
  //  "type": screenTypes.operation,
  //  "controller": 'ManagePersonDataController',
  //  "template": 'app/person-account/templates/manage-shareholder-data.html'
  //},

  "issue_registration_user_entry": {
    "type": screenTypes.operation,
    "controller": 'IssueRegistrationController',
    "template": 'app/corporate-actions/templates/issue-registration.html'
  },
  "issue_finalization_user_entry": {
    "type": screenTypes.operation,
    "controller": 'IssueFinalizationController',
    "template": 'app/corporate-actions/templates/issue-finalization.html'
  },
  "instrument_deletion_user_entry": {
    "type": screenTypes.operation,
    "controller": 'InstrumentDeletionController',
    "template": 'app/corporate-actions/templates/instrument-deletion.html'
  },

  "subscription_access_management_entry": {
    "type": screenTypes.operation,
    "controller": 'SubscriptionAccessManagementController',
    "template": 'app/corporate-actions/templates/subscription-access-management-entry.html'
  },

  "subscription_access_management_approve": {
    "type": screenTypes.operation,
    "controller": 'SubscriptionAccessManagementApprovalController',
    "template": 'app/corporate-actions/templates/subscription-access-management-approval.html'
  },

  "subscription_order_entry": {
    "type": screenTypes.operation,
    "controller": 'SubscriptionOrderController',
    "template": 'app/corporate-actions/templates/subscription-order-entry.html'
  },

  "subscription_order_approve": {
    "type": screenTypes.operation,
    "controller": 'SubscriptionOrderApproveController',
    "template": 'app/corporate-actions/templates/subscription-order-approve.html'
  },

  "return_of_capital_entry": {
    "type": screenTypes.operation,
    "controller": 'ReturnOfCapitalController',
    "template": 'app/corporate-actions/templates/return-of-capital.html'
  },

  "cash_dividend_entry": {
    "type": screenTypes.operation,
    "controller": 'CashDividendController',
    "template": 'app/corporate-actions/templates/cash-dividend.html'
  },

  "haircut_change_entry": {
    "type": screenTypes.operation,
    "controller": 'HaircutChangeController',
    "template": 'app/corporate-actions/templates/haircut-change.html'
  },

  "parvalue_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ParValueChangeController',
    "template": 'app/corporate-actions/templates/par-value-change.html'
  },
  "bonus_shares_entry": {
    "type": screenTypes.operation,
    "controller": 'BonusSharesController',
    "template": 'app/corporate-actions/templates/bonus-shares.html'
  },

  "stock_split_entry": {
    "type": screenTypes.operation,
    "controller": 'StockSplitController',
    "template": 'app/corporate-actions/templates/stock-split.html'
  },

  "demerger_user_entry": {
    "type": screenTypes.operation,
    "controller": 'DeMergerController',
    "template": 'app/corporate-actions/templates/de-merger.html'
  },
  "merger_user_entry": {
    "type": screenTypes.operation,
    "controller": 'MergerController',
    "template": 'app/corporate-actions/templates/merger.html'
  },

  "instrument_conversion_user_entry": {
    "type": screenTypes.operation,
    "controller": 'InstrumentConversionController',
    "template": 'app/corporate-actions/templates/instrument-conversion.html'
  },

  "issuer_data_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "member_data_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "instrument_user_data_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageInstrumentDataController',
    "template": 'app/instruments/templates/manage-instrument-data.html'
  },

  //"shareholder_double_entry_task": {
  //  "type": screenTypes.operation,
  //  "controller": 'ManagePersonDataController',
  //  "template": 'app/person-account/templates/manage-shareholder-data.html'
  //},

  "shareholder_data_entry": {
    "type": screenTypes.operation,
    "controller": 'ManagePersonDataController',
    "template": 'app/person-account/templates/manage-shareholder-data.html'
  },

  "shareholder_visa_tasks": {
    "type": screenTypes.operation,
    "controller": 'ShareholderDataVisaController',
    "template": 'app/person-account/templates/shareholder-data-visa.html'
  },

  //"shareholder_error_handling_task": {
  //  "type": screenTypes.operation,
  //  "controller": 'ShareholderDataAdminController',
  //  "template": 'app/person-account/templates/shareholder-data-admin.html'
  //},

  "error_handler_task": {
    "type": screenTypes.operation,
    "controller": 'ErrorHandlerTaskController',
    "template": 'app/common/templates/error-handler-task.html'
  },
  //"shareholder_approve_tasks": {
  //  "type": screenTypes.operation,
  //  "controller": 'ManageShareholderDataApprovalController',
  //  "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  //},

  "shareholder_data_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/person-account/templates/manage-shareholder-data-approval.html'
  },

  "issue_registration_approve": {
    "type": screenTypes.operation,
    "controller": 'IssueRegistrationApprovalController',
    "template": 'app/corporate-actions/templates/issue-registration-approval.html'
  },

  "instrument_deletion_approve": {
    "type": screenTypes.operation,
    "controller": 'InstrumentDeletionApprovalController',
    "template": 'app/corporate-actions/templates/instrument-deletion-approval.html'
  },

  "return_of_capital_approve": {
    "type": screenTypes.operation,
    "controller": 'ReturnOfCapitalApprovalController',
    "template": 'app/corporate-actions/templates/return-of-capital-approval.html'
  },

  "cash_dividend_approve": {
    "type": screenTypes.operation,
    "controller": 'CashDividendApprovalController',
    "template": 'app/corporate-actions/templates/cash-dividend-approval.html'
  },

  "haircut_change_approve": {
    "type": screenTypes.operation,
    "controller": 'HaircutChangeApprovalController',
    "template": 'app/corporate-actions/templates/haircut-change-approval.html'
  },

  "parvalue_change_approve": {
    "type": screenTypes.operation,
    "controller": 'ParValueChangeApprovalController',
    "template": 'app/corporate-actions/templates/par-value-change-approval.html'
  },

  "bonus_shares_approve": {
    "type": screenTypes.operation,
    "controller": 'BonusSharesApprovalController',
    "template": 'app/corporate-actions/templates/bonus-shares-approval.html'
  },

  "stock_split_approve": {
    "type": screenTypes.operation,
    "controller": 'StockSplitApprovalController',
    "template": 'app/corporate-actions/templates/stock-split-approval.html'
  },

  "demerger_approve_entry": {
    "type": screenTypes.operation,
    "controller": 'DeMergerApprovalController',
    "template": 'app/corporate-actions/templates/de-merger-approval.html'
  },

  "merger_approve_entry": {
    "type": screenTypes.operation,
    "controller": 'MergerApprovalController',
    "template": 'app/corporate-actions/templates/merger-approval.html'
  },

  "instrument_conversion_approve": {
    "type": screenTypes.operation,
    "controller": 'InstrumentConversionApprovalController',
    "template": 'app/corporate-actions/templates/instrument-conversion-approval.html'
  },

  "issue_finalization_user_approve": {
    "type": screenTypes.operation,
    "controller": 'IssueFinalizationApprovalController',
    "template": 'app/corporate-actions/templates/issue-finalization-approval.html'
  },

  "issuer_approve_task": {
    "type": screenTypes.operation,
    "controller": 'ManageIssuerDataApprovalController',
    "template": 'app/person-account/templates/manage-issuer-data-approval.html'
  },
  "member_data_entry_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageMemberDataApprovalController',
    "template": 'app/person-account/templates/manage-issuer-data-approval.html'
  },
  "instrument_data_entry_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageInstrumentDataApprovalController',
    "template": 'app/instruments/templates/manage-instrument-data-approval.html'
  },

  "currency_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageCurrencyController',
    "template": 'app/currency/templates/manage-currency.html'
  },
  // Approvals
  "currency_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageCurrencyApprovalController',
    "template": 'app/currency/templates/manage-currency-approval.html'

  },

  "currency_rate_manual_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageExchangeRateController',
    "template": 'app/currency/templates/manage-exchange-rate.html'
  },
  "currency_rate_manual_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageExchangeRateApprovalController',
    "template": 'app/currency/templates/manage-exchange-rate-approval.html'
  },

  "pledge_register_entry": {

    "type": screenTypes.operation,
    "controller": 'PledgeRegistrationController',
    "template": 'app/records/templates/pledge-registration.html'

  },
  "pledge_register_approve": {
    "type": screenTypes.operation,
    "controller": 'PledgeRegistrationApprovalController',
    "template": 'app/records/templates/pledge-registration-approval.html'
  },
  "pledge_closure_entry": {
    "type": screenTypes.operation,
    "controller": 'PledgeClosureController',
    "template": 'app/records/templates/pledge-closure.html'
  },
  "pledge_closure_approve": {
    "type": screenTypes.operation,
    "controller": 'PledgeClosureApprovalController',
    "template": 'app/records/templates/pledge-closure-approval.html'
  },

  "shareholder_view": {
    "type": screenTypes.view,
    "controller": 'ShareholdersController',
    "template": 'app/persons/templates/persons.html'
  },

  "title_transfer_entry": {
    "type": screenTypes.operation,
    "controller": 'TitleTransferController',
    "template": 'app/records/templates/title-transfer.html'
  },
  "title_transfer_approve": {
    "type": screenTypes.operation,
    "controller": 'TitleTransferApprovalController',
    "template": 'app/records/templates/title-transfer-approval.html'
  },

  "intra_account_transfer_entry": {
    "type": screenTypes.operation,
    "controller": 'ShareTransferBetweenIntraAccountsController',
    "template": 'app/records/templates/share-transfer-between-intra-accounts-entry.html'
  },
  "intra_account_transfer_approve": {
    "type": screenTypes.operation,
    "controller": 'ShareTransferBetweenIntraAccountsApprovalController',
    "template": 'app/records/templates/share-transfer-between-intra-accounts-approval.html'
  },

  "correcting_database_entry": {
    "type": screenTypes.operation,
    "controller": 'CorrectingDatabaseController',
    "template": 'app/records/templates/correcting-database.html'
  },
  "correcting_database_approve": {
    "type": screenTypes.operation,
    "controller": 'CorrectingDatabaseApprovalController',
    "template": 'app/records/templates/correcting-database-approval.html'
  },
  "grant_operation_entry": {
    "type": screenTypes.operation,
    "controller": 'TitleTransferController',
    "template": 'app/records/templates/title-transfer.html'
  },
  "grant_operation_approve": {
    "type": screenTypes.operation,
    "controller": 'TitleTransferApprovalController',
    "template": 'app/records/templates/title-transfer-approval.html'
  },


  "otc_trade_entry": {
    "type": screenTypes.operation,
    "controller": 'OTCTradeController',
    "template": 'app/records/templates/otc-trade.html'
  },

  "otc_trade_approve": {
    "type": screenTypes.operation,
    "controller": 'OTCTradeApprovalController',
    "template": 'app/records/templates/otc-trade-approval.html'
  },


  "manage_bank_info_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageBankInfoController',
    "template": 'app/currency/templates/manage-bank-info.html'
  },
  "manage_bank_info_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageBankInfoApprovalController',
    "template": 'app/currency/templates/manage-bank-info-approve.html'
  },

  "enter_member_advance_entry": {
    "type": screenTypes.operation,
    "controller": 'EnterMemberAdvanceController',
    "template": 'app/clearing/templates/enter-member-advance.html'
  },
  "enter_member_advance_approve": {
    "type": screenTypes.operation,
    "controller": 'EnterMemberAdvanceApprovalController',
    "template": 'app/clearing/templates/enter-member-advance-approval.html'
  },

  "member_cross_trade_registration_entry": {
    "type": screenTypes.operation,
    "controller": 'MemberCrossTradeRegistrationController',
    "template": 'app/records/templates/member-cross-trade-registration.html'
  },
  "member_cross_trade_registration_approve": {
    "type": screenTypes.operation,
    "controller": 'MemberCrossTradeRegistrationApprovalController',
    "template": 'app/records/templates/member-cross-trade-registration-approval.html'
  },
  "member_cross_trade_registration_systemowner_approve": {
    "type": screenTypes.operation,
    "controller": 'MemberCrossTradeRegistrationApprovalController',
    "template": 'app/records/templates/member-cross-trade-registration-approval.html'
  },

  "title_assignment_entry": {
    "type": screenTypes.operation,
    "controller": 'TitleAssignmentController',
    "template": 'app/records/templates/title-assignment.html'
  },
  "title_assignment_approve": {
    "type": screenTypes.operation,
    "controller": 'TitleAssignmentApprovalController',
    "template": 'app/records/templates/title-assignment-approval.html'
  },

  "buy_back_entry": {
    "type": screenTypes.operation,
    "controller": 'BuyBackController',
    "template": 'app/records/templates/buy-back.html'
  },
  "buy_back_approve": {
    "type": screenTypes.operation,
    "controller": 'BuyBackApprovalController',
    "template": 'app/records/templates/buy-back-approval.html'
  },
  "transfer_cancelled_shares_to_issuer_account_entry": {
    "type": screenTypes.operation,
    "controller": 'TransferCancelledSharesToIssuerAccountController',
    "template": 'app/records/templates/transfer-cancelled-shares-to-issuer-account.html'
  },
  "transfer_cancelled_shares_to_issuer_account_approve": {
    "type": screenTypes.operation,
    "controller": 'TransferCancelledSharesToIssuerAccountApprovalController',
    "template": 'app/records/templates/transfer-cancelled-shares-to-issuer-account-approval.html'
  },


  "freezing_issuer_securities_entry": {
    "type": screenTypes.operation,
    "controller": 'FreezingIssuerSecuritiesController',
    "template": 'app/records/templates/freezing-issuer-securities.html'
  },
  "freezing_registration_entry": {
    "type": screenTypes.operation,
    "controller": 'FreezingRegistrationController',
    "template": 'app/records/templates/freezing-registration.html'
  },
  "freezing_registration_approve": {
    "type": screenTypes.operation,
    "controller": 'FreezingRegistrationApprovalController',
    "template": 'app/records/templates/freezing-registration-approval.html'
  },

  "unfreezing_securities_entry": {
    "type": screenTypes.operation,
    "controller": 'UnfreezingSecuritiesController',
    "template": 'app/records/templates/unfreezing-securities.html'
  },
  "unfreezing_securities_approve": {
    "type": screenTypes.operation,
    "controller": 'UnfreezingSecuritiesApprovalController',
    "template": 'app/records/templates/unfreezing-securities-approval.html'
  },

  //#TODO
  "succession_operation_entry": {
    "type": screenTypes.operation,
    "controller": 'SuccessionOperationController',
    "template": 'app/records/templates/succession-operation.html'
  },

  "succession_operation_approve": {
    "type": screenTypes.operation,
    "controller": 'SuccessionOperationApprovalController',
    "template": 'app/records/templates/succession-operation-approval.html'
  },

  "remove_exception_shareholder_from_debtors_list_entry": {
    "type": screenTypes.operation,
    "controller": 'RemoveExceptionShareholderFromDebtorsListController',
    "template": 'app/instruments/templates/remove-exception-shareholder-from-debtors-list.html'
  },
  "remove_exception_shareholder_from_debtors_list_approve": {
    "type": screenTypes.operation,
    "controller": 'RemoveExceptionShareholderFromDebtorsListApprovalController',
    "template": 'app/instruments/templates/remove-exception-shareholder-from-debtors-list-approval.html'
  },
  "add_instrument_to_debtors_list_entry": {
    "type": screenTypes.operation,
    "controller": 'AddInstrumentToDebtorsListController',
    "template": 'app/instruments/templates/add-instrument-to-debtors-list.html'
  },
  "add_instrument_to_debtors_list_approve": {
    "type": screenTypes.operation,
    "controller": 'AddInstrumentToDebtorsListApprovalController',
    "template": 'app/instruments/templates/add-instrument-to-debtors-list-approval.html'
  },
  "add_instrument_to_commission_exception_list_entry": {
    "type": screenTypes.operation,
    "controller": 'AddInstrumentToCommissionExceptionListController',
    "template": 'app/instruments/templates/add-instrument-to-commission-exception-list.html'
  },
  "add_instrument_to_commission_exception_list_approve": {
    "type": screenTypes.operation,
    "controller": 'AddInstrumentToCommissionExceptionListApprovalController',
    "template": 'app/instruments/templates/add-instrument-to-commission-exception-list-approval.html'
  },

  "restore_cancelled_instrument_entry": {
    "type": screenTypes.operation,
    "controller": 'RestoreCancelledInstrumentController',
    "template": 'app/instruments/templates/restore-cancelled-instrument.html'
  },
  "restore_cancelled_instrument_approve": {
    "type": screenTypes.operation,
    "controller": 'RestoreCancelledInstrumentApprovalController',
    "template": 'app/instruments/templates/restore-cancelled-instrument-approval.html'
  },


  "add_instrument_to_black_list_entry": {
    "type": screenTypes.operation,
    "controller": 'AddInstrumentToDebtorsListController',
    "template": 'app/instruments/templates/add-instrument-to-debtors-list.html'

  },
  "add_instrument_to_black_list_approve": {
    "type": screenTypes.operation,
    "controller": 'AddInstrumentToDebtorsListApprovalController',
    "template": 'app/instruments/templates/add-instrument-to-debtors-list-approval.html'
  },

  "add_exception_shareholder_to_black_list_entry": {
    "type": screenTypes.operation,
    "controller": 'AddExceptionShareholderToBlackListController',
    "template": 'app/instruments/templates/add-exception-shareholder-to-debtors-list.html'

  },
  //"succession_operation_entry": {
  "add_exception_shareholder_to_debtors_list_entry": {
    "type": screenTypes.operation,
    "controller": 'AddExceptionShareholderToDebtorsListController',
    "template": 'app/instruments/templates/add-exception-shareholder-to-debtors-list.html'
  },
  "add_exception_shareholder_to_debtors_list_approve": {
    "type": screenTypes.operation,
    "controller": 'AddExceptionShareholderToDebtorsListApprovalController',
    "template": 'app/instruments/templates/add-exception-shareholder-to-debtors-list-approval.html'
  },

  "add_exception_shareholder_to_black_list_approve": {
    "type": screenTypes.operation,
    "controller": 'AddExceptionShareholderToDebtorsListApprovalController',
    "template": 'app/instruments/templates/add-exception-shareholder-to-debtors-list-approval.html'
  },
  "remove_exception_shareholder_from_black_list_entry": {
    "type": screenTypes.operation,
    "controller": 'RemoveExceptionShareholderFromBlackListController',
    "template": 'app/instruments/templates/remove-exception-shareholder-from-debtors-list.html'

  },
  "remove_exception_shareholder_from_black_list_approve": {
    "type": screenTypes.operation,
    "controller": 'RemoveExceptionShareholderFromDebtorsListApprovalController',
    "template": 'app/instruments/templates/remove-exception-shareholder-from-debtors-list-approval.html'
  },
  "remove_instrument_from_debtors_list_entry": {
    "type": screenTypes.operation,
    "controller": 'RemoveInstrumentFromDebtorsListController',
    "template": 'app/instruments/templates/remove-instrument-from-debtors-list.html'

  },
  "remove_instrument_from_debtors_list_approve": {
    "type": screenTypes.operation,
    "controller": 'RemoveInstrumentFromDebtorsListApprovalController',
    "template": 'app/instruments/templates/remove-instrument-from-debtors-list-approval.html'
  },
  "remove_instrument_from_commission_exception_list_entry": {
    "type": screenTypes.operation,
    "controller": 'RemoveInstrumentFromCommissionExceptionListController',
    "template": 'app/instruments/templates/remove-instrument-from-commission-exception-list.html'

  },
  "remove_instrument_from_commission_exception_list_approve": {
    "type": screenTypes.operation,
    "controller": 'RemoveInstrumentFromCommissionExceptionListApprovalController',
    "template": 'app/instruments/templates/remove-instrument-from-commission-exception-list-approval.html'
  },
  "cash_dividend_payment_entry": {
    "type": screenTypes.operation,
    "controller": 'CashDividendPaymentController',
    "template": 'app/corporate-actions/templates/cash-dividend-payment.html'

  },
  "cash_dividend_payment_approve": {
    "type": screenTypes.operation,
    "controller": 'CashDividendPaymentApprovalController',
    "template": 'app/corporate-actions/templates/cash-dividend-payment-approval.html'
  },
  "cash_dividend_payment_print": {
    "type": screenTypes.operation,
    "controller": 'CashDividendPaymentPrintController',
    "template": 'app/corporate-actions/templates/cash-dividend-payment-print.html'
  },
  "withdrawal_subscription_money_entry": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalSubscriptionMoneyController',
    "template": 'app/corporate-actions/templates/withdrawal-subscription-money.html'

  },
  "withdrawal_subscription_money_approve": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalSubscriptionMoneyApprovalController',
    "template": 'app/corporate-actions/templates/withdrawal-subscription-money-approval.html'
  },
  "withdrawal_subscription_money_print": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalSubscriptionMoneyPrintController',
    "template": 'app/corporate-actions/templates/withdrawal-subscription-money-print.html'
  },
  "configure_bis_model_entry": {
    "type": screenTypes.operation,
    "controller": 'ConfigureBISModelController',
    "template": 'app/configurations/templates/configure-BIS-model.html'
  },

  "configure_bis_model_approve": {
    "type": screenTypes.operation,
    "controller": 'ConfigureBISModelApprovalController',
    "template": 'app/configurations/templates/configure-BIS-model-approval.html'
  },
  "otc_deal_broker_entry": {
    "type": screenTypes.operation,
    "controller": 'OTCDealBrokerController',
    "template": 'app/records/templates/OTC-deal-broker.html'
  },

  "otc_deal_broker_approve": {
    "type": screenTypes.operation,
    "controller": 'OTCDealBrokerApprovalController',
    "template": 'app/records/templates/OTC-deal-broker-approval.html'
  },


  "outgoing_payment_entry": {
    "type": screenTypes.operation,
    "controller": 'OutgoingPaymentController',
    "template": 'app/records/templates/outgoing-payment-entry.html'
  },

  "outgoing_payment_approve": {
    "type": screenTypes.operation,
    "controller": 'OutgoingPaymentApprovalController',
    "template": 'app/records/templates/outgoing-payment-approval.html'
  },


  "approve_withdrawals_by_opdep_entry": {
    "type": screenTypes.operation,
    "controller": 'ApproveWithdrawalsByOperationDepartmentController',
    "template": 'app/records/templates/approve-withdrawals-by-operation-department.html'
  },

  "approve_withdrawals_by_opdep_approve": {
    "type": screenTypes.operation,
    "controller": 'ApproveWithdrawalsByOperationDepartmentApproveController',
    "template": 'app/records/templates/approve-withdrawals-by-operation-department-approval.html'
  },


  "withdraw_forex_amount_entry": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalForexAmountController',
    "template": 'app/records/templates/withdrawal-forex-amount.html'
  },


  "withdraw_forex_amount_approve": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalForexAmountApproveController',
    "template": 'app/records/templates/withdrawal-forex-amount-approval.html'
  },


  "unknown_withdrawal_operation_entry": {
    "type": screenTypes.operation,
    "controller": 'UnknownWithdrawalOperationController',
    "template": 'app/records/templates/unknown-withdrawal-operation.html'
  },


  "unknown_withdrawal_operation_approve": {
    "type": screenTypes.operation,
    "controller": 'UnknownWithdrawalOpeationApproveController',
    "template": 'app/records/templates/unknown-withdrawal-operation-approval.html'
  },


  "unknown_withdrawal_operation_second_approve": {
    "type": screenTypes.operation,
    "controller": 'UnknownWithdrawalOpeationApproveController',
    "template": 'app/records/templates/unknown-withdrawal-operation-approval.html'
  },


  "otc_trade_member_entry": {
    "type": screenTypes.operation,
    "controller": 'OTCTradeMemberController',
    "template": 'app/records/templates/OTC-trade-member.html'
  },

  "otc_trade_member_approve": {
    "type": screenTypes.operation,
    "controller": 'OTCTradeMemberApprovalController',
    "template": 'app/records/templates/OTC-trade-member-approval.html'
  },


  "commission_limit_entry": {
    "type": screenTypes.operation,
    "controller": 'CommissionLimitController',
    "template": 'app/clearing/templates/commission-limit.html'
  },

  "commission_limit_approve": {
    "type": screenTypes.operation,
    "controller": 'CommissionLimitApproveController',
    "template": 'app/clearing/templates/commission-limit-approve.html'
  },


  "assign_member": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'AssignMemberController',
    "template": 'app/records/templates/assign-member.html'
  },

  "approve_withdrawals_by_chairman_entry": {
    "type": screenTypes.operation,
    "controller": 'ApproveWithdrawalsByChairmanController',
    "template": 'app/records/templates/approve-withdrawals-by-chairman.html'
  },


  "assign_account_and_service_class": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'AssignAccountAndServiceClassController',
    "template": 'app/records/templates/assign-account-and-service-class.html'
  },

  "return_unknown_payment_operation_entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'ReturnUnknownPaymentOperationController',
    "template": 'app/records/templates/return_unknown_payment_operation.html'
  },

  "return_unknown_payment_operation_approve": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'ReturnUnknownPaymentOperationApproveController',
    "template": 'app/records/templates/return_unknown_payment_operation_approval.html'
  },


  "withdrawal_VAT_operation_entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'NDCWithdrawalOperationDataController',
    "template": 'app/records/templates/ndc-withdrawal-operation-data.html'
  },

  "withdrawal_VAT_operation_approve": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'NDCWithdrawalOperationDataApproveController',
    "template": 'app/records/templates/ndc-withdrawal-operation-data-approval.html'
  },

  "approve_matched_trade_entry": {
    "type": screenTypes.operation,
    "controller": 'ApproveMatchedTradeController',
    "template": 'app/records/templates/approve-matched-trade.html'
  },

  "approve_matched_trade_approve": {
    "type": screenTypes.operation,
    "controller": 'ApproveMatchedTradeApprovalController',
    "template": 'app/records/templates/approve-matched-trade-approval.html'
  },

  "otc_trade_member_confirm_entry": {
    "type": screenTypes.operation,
    "controller": 'ConfirmMatchedTradeController',
    "template": 'app/clearing/templates/confirm-matched-trade.html'
  },

  "otc_trade_member_confirm_approve": {
    "type": screenTypes.operation,
    "controller": 'ConfirmMatchedTradeApprovalController',
    "template": 'app/clearing/templates/confirm-matched-trade-approval.html'
  },


  "confirm_matched_trade_entry": {
    "type": screenTypes.operation,
    "controller": 'ConfirmMatchedTradeController',
    "template": 'app/clearing/templates/confirm-matched-trade.html'
  },

  "confirm_matched_trade_approve": {
    "type": screenTypes.operation,
    "controller": 'ConfirmMatchedTradeApprovalController',
    "template": 'app/clearing/templates/confirm-matched-trade-approval.html'
  },
  "remove_instrument_from_black_list_entry": {
    "type": screenTypes.operation,
    "controller": 'RemoveInstrumentFromBlackListController',
    "template": 'app/instruments/templates/remove-instrument-from-debtors-list.html'

  },
  "remove_instrument_from_black_list_approve": {
    "type": screenTypes.operation,
    "controller": 'RemoveInstrumentFromDebtorsListApprovalController',
    "template": 'app/instruments/templates/remove-instrument-from-debtors-list-approval.html'
  },
  "transfer_pledged_title_entry": {
    "type": screenTypes.operation,
    "controller": 'TransferPledgedTitleController',
    "template": 'app/records/templates/transfer-pledged-title.html'

  },
  "transfer_pledged_title_approve": {
    "type": screenTypes.operation,
    "controller": 'TransferPledgedTitleApprovalController',
    "template": 'app/records/templates/transfer-pledged-title-approval.html'
  },
  "juridical_client_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageClientAccountDataController',
    "template": 'app/accounts/templates/manage-client-account-data.html'

  },
  "juridical_client_data_change_approve_1": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "juridical_client_data_change_approve_2": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "juridical_client_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageClientAccountDataController',
    "template": 'app/accounts/templates/manage-client-account-data.html'

  },
  "juridical_client_account_opening_approve_1": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "juridical_client_account_opening_approve_2": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "natural_client_data_change_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageClientAccountDataController',
    "template": 'app/accounts/templates/manage-client-account-data.html'

  },
  "natural_client_data_change_approve_1": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "natural_client_data_change_approve_2": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "natural_client_account_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageClientAccountDataController',
    "template": 'app/accounts/templates/manage-client-account-data.html'

  },
  "natural_client_account_opening_approve_1": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "natural_client_account_opening_approve_2": {
    "type": screenTypes.operation,
    "controller": 'ManageShareholderDataApprovalController',
    "template": 'app/accounts/templates/manage-client-account-data-approval.html'
  },
  "pledge_reregistration_entry": {
    "type": screenTypes.operation,
    "controller": 'PledgeReRegistrationController',
    "template": 'app/records/templates/pledge-re-registration.html'

  },
  "pledge_reregistration_approve": {
    "type": screenTypes.operation,
    "controller": 'PledgeReRegistrationApprovalController',
    "template": 'app/records/templates/pledge-re-registration-approval.html'
  },
  //"case_processing_entry": {
  //  "type": screenTypes.operation,
  //  "controller": 'CaseProcessingController',
  //  "template": 'app/temp/templates/test-task.html'
  //},
  //
  //"case_processing_approve": {
  //  "type": screenTypes.operation,
  //  "controller": 'CaseProcessingApprovalController',
  //  "template": 'app/temp/templates/test-task-approval.html'
  //},
  "pledge_endorsement_entry": {
    "type": screenTypes.operation,
    "controller": 'PledgeEndorsementController',
    "template": 'app/records/templates/pledge-endorsement.html'
  },
  "pledge_endorsement_approve": {
    "type": screenTypes.operation,
    "controller": 'PledgeEndorsementApprovalController',
    "template": 'app/records/templates/pledge-endorsement-approval.html'
  },
  "pledge_transfer_entry": {
    "type": screenTypes.operation,
    "controller": 'PledgeTransferController',
    "template": 'app/records/templates/pledge-transfer.html'
  },
  "pledge_transfer_approve": {
    "type": screenTypes.operation,
    "controller": 'PledgeTransferApprovalController',
    "template": 'app/records/templates/pledge-transfer-approval.html'
  },
  "pledge_invocation_entry": {
    "type": screenTypes.operation,
    "controller": 'PledgeInvocationController',
    "template": 'app/records/templates/pledge-invocation.html'
  },
  "pledge_invocation_approve": {
    "type": screenTypes.operation,
    "controller": 'PledgeInvocationApprovalController',
    "template": 'app/records/templates/pledge-invocation-approval.html'
  },
  "manage_order_whitelist_entry": {
    "type": screenTypes.operation,
    "controller": 'OrderWhitelistManagementController',
    "template": 'app/configurations/templates/order-whitelist-management.html'
  },
  "manage_order_whitelist_approve": {
    "type": screenTypes.operation,
    "controller": 'OrderWhitelistManagementApprovalController',
    "template": 'app/configurations/templates/order-whitelist-management-approval.html'
  },
  "manage_operation_positions_entry": {
    "type": screenTypes.operation,
    "controller": 'ManageOperationPositionsController',
    "template": 'app/configurations/templates/manage-operation-positions.html'
  },
  "manage_operation_positions_approve": {
    "type": screenTypes.operation,
    "controller": 'ManageOperationPositionsApprovalController',
    "template": 'app/configurations/templates/manage-operation-positions-approval.html'
  },
  "business_day_opening_entry": {
    "type": screenTypes.operation,
    "controller": 'BusinessDayOpeningController',
    "template": 'app/corporate-actions/templates/business-day-opening.html'
  },
  "business_day_opening_approve": {
    "type": screenTypes.operation,
    "controller": 'BusinessDayOpeningApprovalController',
    "template": 'app/corporate-actions/templates/business-day-opening-approval.html'
  },
  "business_day_closing_entry": {
    "type": screenTypes.operation,
    "controller": 'BusinessDayClosingController',
    "template": 'app/corporate-actions/templates/business-day-closing.html'
  },
  "business_day_closing_approve": {
    "type": screenTypes.operation,
    "controller": 'BusinessDayClosingApprovalController',
    "template": 'app/corporate-actions/templates/business-day-closing-approval.html'
  },

  "withdrawal_cash_entry": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalCashController',
    "template": 'app/clearing/templates/withdrawal-cash.html'
  },
  "withdrawal_cash_check": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalCashCheckController',
    "template": 'app/clearing/templates/withdrawal-cash-check.html'
  },
  "withdrawal_cash_approve": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalCashApprovalController',
    "template": 'app/clearing/templates/withdrawal-cash-approval.html'
  },
  "withdrawal_cash_signing": {
    "type": screenTypes.operation,
    "controller": 'WithdrawalCashSigningController',
    "template": 'app/clearing/templates/withdrawal-cash-signing.html'
  },

  "transfer_to_issuer_account_entry": {
    "type": screenTypes.operation,
    "controller": 'TransferToIssuerAccountController',
    "template": 'app/records/templates/transfer-to-issuer-account.html'
  },
  "transfer_to_issuer_account_approve": {
    "type": screenTypes.operation,
    "controller": 'TransferToIssuerAccountApprovalController',
    "template": 'app/records/templates/transfer-to-issuer-account-approval.html'
  },

  "cancel_operation_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelTasksController',
    "template": 'app/configurations/templates/cancel-tasks.html'
  },

  "cancel_operation_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelTasksApprovalController',
    "template": 'app/configurations/templates/cancel-tasks-approval.html'
  },

  "cancel_incompleted_operation_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelIncompletedOperationController',
    "template": 'app/records/templates/cancel-incompleted-operation.html'
  },

  "cancel_incompleted_operation_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelIncompletedOperationApproveController',
    "template": 'app/records/templates/cancel-incompleted-operation-approve.html'
  },
  "natural_person_management_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    // "controller": "NaturalPersonManagementController",
    // "template": "app/management/templates/natural_person_management_entry.html"
    "controller": "naturalPersonManagementEntryController",
    "template": 'app/person-account/templates/naturalPersonManagementEntry.html'
  },
  "natural_person_management_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "NaturalPersonManagementApproveController",
    "template": "app/management/templates/natural_person_management_approve.html"
  },
  "shareholder_ui_management_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagmentController",
    "template": "app/management/templates/shareholder_ui_managment_entry.html"
  },
  "shareholder_ui_management_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagmentApproveController",
    "template": "app/management/templates/shareholder_ui_management_approve.html"
  },
  "issuer_ui_management_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagmentController",
    "template": "app/management/templates/shareholder_ui_managment_entry.html"
  },
  "issuer_ui_management_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "ShareholderUiManagmentApproveController",
    "template": "app/management/templates/shareholder_ui_management_approve.html"
  },

  "cancel_operation_2nd_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelTasksApprovalController',
    "template": 'app/configurations/templates/cancel-tasks-approval.html'
  },


  "cancel_operation_review": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'CancelOperationReviewController',
    "template": 'app/configurations/templates/cancel-operation-review.html'
  },

  "coupon_payment_schedule_manager_entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'CouponPaymentScheduleManagerController',
    "template": 'app/corporate-actions/templates/coupon-payment-schedule-manager.html'
  },


  "coupon_payment_schedule_manager_approve": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'CouponPaymentScheduleManagerApprovalController',
    "template": 'app/corporate-actions/templates/coupon-payment-schedule-manager_approval.html'
  },

  "notary_trade_entry": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'NotaryTradeController',
    "template": 'app/records/templates/notary-trade.html'
  },

  "notary_trade_approve": {
    "classes": "tile bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Transfer to Issuer Account",
    "type": screenTypes.operation,
    "controller": 'NotaryTradeApprovalController',
    "template": 'app/records/templates/notary-trade-approval.html'
  },

  //todo comment out
  "issuer_coupon_payment_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'IssuerCouponPaymentController',
    "template": 'app/corporate-actions/templates/issuer-coupon-payment.html'
  },
  "issuer_coupon_payment_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'IssuerCouponPaymentApproveController',
    "template": 'app/corporate-actions/templates/issuer_coupon_payment_approval.html'
  },
  "notary_settlement_trade_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'NotarySettlementTradeController',
    "template": 'app/records/templates/notary-settlement-trade.html'
  },
  "notary_settlement_trade_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'NotarySettlementTradeControllerApprove',
    "template": 'client/app/records/templates/notary-settlement-trade-approval.html'
  },

  "send_error_coupon_payments_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'SendErrorCouponPaymentController',
    "template": 'app/corporate-actions/templates/send-error-coupon-payments.html'
  },
  "send_error_coupon_payments_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'SendErrorCouponPaymentApproveController',
    "template": 'app/corporate-actions/templates/send-error-coupon-payments-approval.html'
  },

  "withdrawal_ndc_clearing_cash_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'WithdrawalNdcClearingCashController',
    "template": 'app/clearing/templates/withdrawal-ndc-clearing-cash.html'
  },

  "withdrawal_ndc_clearing_cash_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'WithdrawalNdcClearingCashApprovalController',
    "template": 'app/clearing/templates/withdrawal-ndc-clearing-cash-approval.html'
  },


  "transfer_cash_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'TransferCashBetweenInternalAccountsController',
    "template": 'app/records/templates/transfer-cash-between-internal-accounts.html'
  },


  "transfer_cash_receiver_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'TransferCashReceiverApproveController',
    "template": 'app/records/templates/transfer_cash_receiver_approve.html'
  },

  "withdraw_revenue_operation_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'WithdrawalRevenueOperationController',
    "template": 'app/clearing/templates/withdrawal-revenue-operation.html'
  },

  "withdraw_revenue_operation_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'WithdrawalRevenueOperationApproveController',
    "template": 'app/clearing/templates/withdrawal-revenue-operation-approval.html'
  },


  "transfer_cash_ndc_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'TransferCashNdcApproveController',
    "template": 'app/records/templates/transfer_cash_ndc_approve.html'
  },

  "transfer_cash_from_client_account_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'TransferCashFromClientAccountController',
    "template": 'app/clearing/templates/transfer-cash-from-client-account.html'
  },

  "transfer_cash_from_client_account_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'TransferCashFromClientAccountApproveController',
    "template": 'app/clearing/templates/transfer-cash-from-client-account-approve.html'
  },
  "transfer_cash_from_client_account_second_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": "TransferCashFromClientAccountSecondApproveController",
    "template": "app/clearing/templates/transfer-cash-from-client-account-second-approve.html"
  },

  "decrease_capital_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'DecreaseCapitalController',
    "template": 'app/corporate-actions/templates/decrease-capital.html'
  },

  "decrease_capital_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'DecreaseCapitalApprovalController',
    "template": 'app/corporate-actions/templates/decrease-capital-approval.html'
  },
  "generate_anna_master_file_entry": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'GenerateAnnaMasterFileController',
    "template": 'app/anna/templates/generate-anna-master-file-entry.html'
  },
  "generate_anna_master_file_approve": {
    "classes": "tile tile-wide bg-crimson fg-white",
    "icon": "file",
    "defaultName": "Client Account Opening",
    "type": screenTypes.operation,
    "controller": 'GenerateAnnaMasterFileApproveController',
    "template": 'app/anna/templates/generate-anna-master-file-approval.html'
  },
  "generate_anna_master_file_print": {
    "type": screenTypes.operation,
    "controller": "GenerateAnnaMasterFilePrintController",
    "template": "app/anna/templates/generate-anna-master-file-print.html"
  },


};
// Task Data ]]]

exports.getUserData = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getUserParameters'
  };
  soap.createClient(url, options, function (err, ctx) {

    var tokenData = {
      userId: req.user.id
    };

    if (err) {
      res.json({success: false});
      console.error(err);
    }
    else {
      var userToken = jwt.sign(tokenData, config.secrets.jwt, {expiresIn: config.cookie.expires * 60});
      if (ctx.responseObject.return && ctx.responseObject.return.data)
        ctx.responseObject.return.data.token = userToken;
      res.json(ctx.responseObject.return);
    }

  });

};


exports.getSingleOperation = function (req, res) {
  return res.json(allOperationsData[req.query['key']]);
};


exports.getTileConfigurations = function (req, res) {
  var configurations = {
    dashboardTiles: dashboardTiles,
    otherTiles: tileConfigurations
  };
  res.json(configurations);
};


exports.startProcess = function (req, res, next) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'startProcess',
    args: {
      processDefinitionKey: req.query.taskKey
    }
  };


  soap.createClient(url, options, function (err, ctx) {
    //# TODO fix error handling
    if (err) {
      console.log(err);
    }
    else {
      var resData = ctx.responseObject.return;

      if (!resData.success) {
        res.json(resData);
      } else {
        if (resData.data) {
          resData.data['config'] = allOperationsData[resData.data.key];
        }
        res.json(resData);
      }

    }

  });
};


exports.startProcessByMessage = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'startProcessByMessage',
    args: {
      messageName: req.query.messageName,
      senderId: req.query.senderId
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.log(err);
    }
    else {
      var resData = ctx.responseObject.return.data;
      if (resData) {
        resData['config'] = allOperationsData[resData.key];
      }
      res.json(resData);
    }
  });
};


exports.completeTask = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'completeTask',
    args: req.body
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
exports.uploadTaskAttachment = function (req, res) {
  upload(req, res, function (err) {
    var payload = JSON.parse(req.body.payload);
    if (err) {
      return res.json({success: false, message: err});
    }
    var remaining = req.files ? req.files.length : 0;
    var convertedFiles = [];
    if (req.files && req.files.length) {
      req.files.forEach(function (item) {
        // if (item.mimetype == "application/pdf") {
        var currentFile = item.path;
        fs.readFile(currentFile, function (err, fileBinary) {
          if (err) {
            console.error(err);
          }
          else {
            // Convert file to Base64 encoding
            var base64File = new Buffer(fileBinary, 'binary').toString('base64');
            convertedFiles.push(base64File);
            fs.unlink(currentFile, function (err) {
              if (err) {
                console.error(err);
              }
            });
          }
          remaining -= 1;
          if (remaining == 0) {
            payload.docData = convertedFiles[0];
            payload.mimeType = item.mimetype;
            var fileNameArr = item.originalname.split(".")
            var fileExtension = fileNameArr[fileNameArr.length - 1];
            var soapClientOptions = {
              user: req.user,
              namespace: namespace,
              method: 'uploadDocument',
              args: {
                docID: payload.docID ? payload.docID : null,
                docData: payload.docData ? payload.docData : null,
                docClass: payload.docClass ? payload.docClass : null,
                procInstID: payload.procInstID ? payload.procInstID : null,
                mimeType: payload.mimeType ? (payload.mimeType + ";" + fileExtension) : null,
              }
            };
            soap.createClient(url, soapClientOptions, function (err, ctx) {
              if (err) {
                console.error(err);
              }
              else {
                res.json(ctx.responseObject.return);
              }
            });
          }
        });
        //}
        //else {
        //  res.json({
        //    success: "false",
        //    message: "Yalnız pdf faylar yükləyə bilərsiniz!"
        //  })
        //}
      });
    }
  });
};
exports.removeDocument = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'removeDocument',
    args: {docID: req.params.id}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};

exports.getDocumentByID = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getDocumentByID',
    args: {ID: req.params.id}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};


exports.saveTask = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'saveTask',
    args: req.body.data
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
exports.openTask = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'openTask',
    args: {
      taskId: req.query.taskId
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.log(err);
    }
    else {
      var resData = ctx.responseObject.return.data;
      if (resData) {
        resData['config'] = allOperationsData[resData.key];
      }
      res.json(resData);
    }
  });

};
exports.openFinishedTask = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'openFinishedTask',
    args: {
      processId: req.params.processId
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.log(err);
    }
    else {
      var resData = ctx.responseObject.return.data;
      if (resData) {
        resData['config'] = allOperationsData[resData.key];
      }
      res.json(resData);
    }

  });

};
exports.getMyActiveTasks = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMyActiveTasks',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getMyFinishedTasks = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMyFinishedTasks',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });

};

exports.getAllFinishedTasks = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAllFinishedTasks',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });

};


exports.getMyCompletedTasks = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCompletedTasks',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });

};

exports.getAllCompletedTasks = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAllCompletedTasks',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });

};


exports.getMyUnclaimedTasks = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMyUnclaimedTasks',
    args: {
      criteria: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });

};
exports.getMyControlledTasks = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMyControlledTasks',
    args: {
      skip: req.body.skip,
      take: req.body.take
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });
};
exports.claimTask = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'claimTask',
    args: {
      taskId: req.query.taskId
    }
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return.data);

  });

};
exports.cancelTask = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'cancelTask',
    args: req.body.data
  };
  soap.createClient(url, options, function (err) {

    if (err)
      console.log(err);
    else
      res.json({});

  });
};
exports.getMyControlledOperations = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMyControlledOperations',
    args: {
      criteria: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });
};
exports.getTasksByProcessInstanceId = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTasksByProcessInstanceId',
    args: {
      processInstanceId: req.body.processInstanceId
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });
};
exports.getDashBoardContents = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getDashBoardContents',
    args: {
      dashboardKey: req.params.dashboardKey
    }
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });
};
exports.sendTaskComment = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'sendTaskComment',
    args: req.body.data
  };
  res.json({

    success: "true",
    data: {
      "id": "yetAnotherCommentId",
      "userId": "yetAnotherUserId",
      "taskId": "aTaskId",
      "time": new Date(),
      "message": "yetAnotherMessage"
    }

  });

};
exports.sendTaskDocument = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'sendTaskDocument',
    args: {
      taskId: req.body.taskId,
      document: req.body.document
    }
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.log(err);
    else
      res.json(ctx.responseObject.return);

  });
};


