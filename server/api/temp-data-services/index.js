'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();


router.get('/getTrades', auth.requiresLogin, controller.getTrades); //+
router.get('/getNonMatchedTrades', auth.requiresLogin, controller.getNonMatchedTrades); // +
router.get('/getNonConfirmedTrades', auth.requiresLogin, controller.getNonConfirmedTrades); // +
router.get('/getDefaultedTrades', auth.requiresLogin, controller.getDefaultedTrades); // +

router.get('/getEarlySettlementTrades', auth.requiresLogin, controller.getEarlySettlementTrades);
router.get('/getEarlyDeliveryTrades', auth.requiresLogin, controller.getEarlyDeliveryTrades);

router.get('/getSecurityLenders', auth.requiresLogin, controller.getSecurityLenders); +
router.get('/getCurrencies', auth.requiresLogin, controller.getCurrencies); // +
router.get('/getExchangeRates', auth.requiresLogin, controller.getExchangeRates);// +
router.get('/getCountries', auth.requiresLogin, controller.getCountries); // +

router.get('/getTransactions', auth.requiresLogin, controller.getTransactions);
router.get('/getPledgeRegistration', auth.requiresLogin, controller.getPledgeRegistration);
router.get('/getPledgedShares', auth.requiresLogin, controller.getPledgedShares);


module.exports = router;
