'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

var convertObjectToArray = function (field) {
  if (field && field.constructor !== Array) {
    field = [field];
  }
  return field;
};

exports.getTrades = function (req, res) {
  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getTrades',
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getNonMatchedTrades = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getNonMatchedTrades',
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });

};
exports.getNonConfirmedTrades = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getNonConfirmedTrades',
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getDefaultedTrades = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getDefaultedTrades',
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getEarlySettlementTrades = function (req, res) {


  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getEarlySettlementTrades',

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });

};
exports.getEarlyDeliveryTrades = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getEarlyDeliveryTrades',

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getSecurityLenders = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getSecurityLenders',

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};

exports.getCurrencies = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getCurrencies',

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getExchangeRates = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getExchangeRates',

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getCountries = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getTempCountries'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getTransactions = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getTransactions',
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getPledgeRegistration = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getPledgeRegistration',
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};
exports.getPledgedShares = function (req, res) {

  var options = {
    namespace: namespace,
    user: req.user,
    method: 'getPledgedShares',
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err)
    else
      res.json(convertObjectToArray(ctx.responseObject.return));
  });
};

