/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');

var request = require('request');
var soap = require("../../lib/soap-wrapper");
var jwt = require('jsonwebtoken');


var config = require('../../config/environment');

var SERVICE_HOST = "https://glaring-torch-743.firebaseio.com/";
var openam = require('../../lib/openam');

var users = {
  "jack": {
    "contexts": [{
      "id": 1,
      "name_az": "Kontekst 1",
      "name_en": "Context 1"
    }, {
      "id": 2,
      "name_az": "Kontekst 2",
      "name_en": "Context 2"
    }],
    "fullName": "Jack Doe",
    "settings": {
      "current_context": "User context option1",
      "default_context": "User context option1",
      "default_tile_organization_option": 1,
      "language": "az"
    }
  },
  "john": {
    "contexts": [{
      "id": 1,
      "name_az": "Kontekst 1",
      "name_en": "Context 1"
    }, {
      "id": 2,
      "name_az": "Kontekst 2",
      "name_en": "Context 2"
    }],
    "fullName": "John Doe",
    "settings": {
      "current_context": 1,
      "default_context": 2,
      "default_tile_organization_option": 3,
      "language": "en"
    }
  }
};

var getUser = function (req, res) {

  //request({
  //  url: SERVICE_HOST + 'users/' + req.user.id + ".json",
  //  method: 'GET',
  //  json: true
  //
  //}, function (error, response, body) {
  //  var resp = false;
  //  if (!error) {
  //    resp = body;
  //  }

  var resp = users[req.user.id];
  var tokenData = {
    userId: req.user.id
  };

  // We are sending the profile inside the token
  var userToken = jwt.sign(tokenData, config.secrets.jwt, {expiresIn: config.cookie.expires * 60});

  resp.token = userToken;

  res.json(resp);
  // });
};

exports.getUser = function (req, res) {

  getUser(req, res);
};

exports.changeContext = function (req, res) {

  request(
    {
      url: SERVICE_HOST + 'users/' + req.user.id + '/settings/.json',
      method: 'PATCH',
      json: {
        current_context: req.body.context,
      }
    }, function (error, response, body) {
      getUser(req, res);
    });
};

exports.changeSetting = function (req, res) {

  var data = {};

  data[req.body.key] = req.body.value;

  request(
    {
      url: SERVICE_HOST + 'users/' + req.user.id + '/settings/.json',
      method: 'PATCH',
      json: data
    }, function (error, response, body) {

      getUser(req, res);

    });
};

exports.changePassword = function (req, res) {

  req.sanitizeBody('passwordCurrent').trim();
  req.sanitizeBody('passwordNew').trim();

  openam.changePassword(req.user, req.body.passwordCurrent, req.body.passwordNew, function (err, succ) {

    if(err){
      res.json({
        success: "false",
        message: err.message
      });
    }
    else{
      res.json({
        success: "true",
        message:null
      });
    }

  });

};

exports.export = function (req, res) {
  console.log(req.body.fileName, req.body.contentType);
  res.set('Content-Disposition', 'attachment; filename="'+req.body.fileName+'"');
  res.set('Content-Type', req.body.contentType);
  res.end(req.body.base64, 'base64');
};
