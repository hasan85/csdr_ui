'use strict';

var express = require('express');
var controller = require('./main.controller');

var auth = require('../../auth/authorization');
var router = express.Router();

router.get('/getUser', auth.requiresLogin, controller.getUser);

router.post('/changePassword', auth.requiresLogin, controller.changePassword);

router.put('/changeContext', auth.requiresLogin, controller.changeContext);

router.put('/changeSetting', auth.requiresLogin, controller.changeSetting);

router.post("/export", auth.requiresLogin, controller.export);

module.exports = router;
