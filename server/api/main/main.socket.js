/**
 * Broadcast updates to client when the  changes occur
 */

'use strict';
exports.register = function (sockets, socket, clients) {
  socket.on("newUnclaimedTask", function (userId) {
    console.log('new unc');
    if (clients[userId]) {
      for (var i = 0; i < clients[userId]['sockets'].length; i++) {
        var socketId = clients[userId]['sockets'][i];
        sockets[socketId].emit('newUnclaimedTask');
      }
    }
  });
};
