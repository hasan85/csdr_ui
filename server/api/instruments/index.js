'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.get('/getInTreasureQuantity/:instrumentId', auth.requiresLogin, controller.getInTreasureQuantity);
router.get('/getBondCashFlow/:instrumentId', auth.requiresLogin, controller.getBondCashFlow);
router.post('/searchIssuerInstruments/', auth.requiresLogin, controller.searchIssuerInstruments);
router.post('/searchPrivatizationInstruments/', auth.requiresLogin, controller.searchPrivatizationInstruments);
router.post('/searchBond/', auth.requiresLogin, controller.searchBond);
router.post('/getCouponPaymentInstruments/', auth.requiresLogin, controller.getCouponPaymentInstruments);
router.post('/getVMCouponPaymentsByInstrument/', auth.requiresLogin, controller.getVMCouponPaymentsByInstrument);
router.post('/getCouponPaymentRecords/', auth.requiresLogin, controller.getCouponPaymentRecords);
router.post('/getPaidCouponPayments/', auth.requiresLogin, controller.getPaidCouponPayments);

router.post('/getInstrumentsInDebtorsList/', auth.requiresLogin, controller.getInstrumentsInDebtorsList);
router.post('/getInstrumentsInBlackList/', auth.requiresLogin, controller.getInstrumentsInBlackList);
router.post('/getShareholderInWhiteList/', auth.requiresLogin, controller.getShareholderInWhiteList);
router.post('/getWhiteListForInstrumentRejectionList/', auth.requiresLogin, controller.getWhiteListForInstrumentRejectionList);
router.get('/getInstrumentById/:instrumentId', auth.requiresLogin, controller.getInstrumentById);

router.post("/getCancelledInstrumentDataByISIN", auth.requiresLogin, controller.getCancelledInstrumentDataByISIN)

module.exports = router;
