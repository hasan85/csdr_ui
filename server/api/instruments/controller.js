'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

exports.getInTreasureQuantity = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInTreasureQuantity',
    args: {instrumentId: req.params.instrumentId}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};


exports.getBondCashFlow = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getBondCashFlow',
    args: {instrumentId: req.params.instrumentId}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.searchIssuerInstruments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchIssuerInstruments',
    args: {
      criteria: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};


exports.getPaidCouponPayments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPaidCouponPayments',
    args: {
      criteria: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.searchPrivatizationInstruments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchPrivatizationInstruments',
    args: {
      criteria: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.searchBond = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchBond',
    args: {
      criteria: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });

};


exports.getCouponPaymentInstruments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCouponPaymentInstruments',
    args: {
      criteria: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });

};
exports.getVMCouponPaymentsByInstrument = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getVMCouponPaymentsByInstrument',
    args: {instrumentID: req.body.instrumentId}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });

};

exports.getCouponPaymentRecords = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCouponPaymentRecords',
    args: {couponPaymentID: req.body.couponPaymentID}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });

};

exports.getInstrumentsInDebtorsList = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInstrumentsInDebtorsList',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getInstrumentsInBlackList = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInstrumentsInBlackList',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getWhiteListForInstrumentRejectionList = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getWhiteListForInstrumentRejectionList',
    args: {
      rejectionID: req.body.id
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getShareholderInWhiteList = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getShareholderInWhiteList',
    args: {
      rejectionID: req.body.rejectionId,
      shareholderAccountID: req.body.shareholderAccountId
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getInstrumentById = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInstrumentById',
    args: {
      instrumentId: req.params.instrumentId,
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getCancelledInstrumentDataByISIN = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCancelledInstrumentDataByISIN',
    args: {
      ISIN: req.body.ISIN,
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
