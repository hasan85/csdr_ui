'use strict';

// order of service methods must be kept

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.reportsDataServices.namespace;
var url = config.servers.reportsDataServices.url;


var thrift = require("../../lib/thrift-wrapper");
var DataReportsService = require("../no-api-thrift-files/gen-nodejs/DataSearchService");
var servicedata_types = require("../no-api-thrift-files/gen-nodejs/servicedata_types");
var system_types = require("../no-api-thrift-files/gen-nodejs/system_types");
var uimanager = require("../no-api-thrift-files/gen-nodejs/uimanager_types");
var common = require("../no-api-thrift-files/gen-nodejs/commondata_types");


exports.getStatisticsReports = function (req, res) {
  var options = {
    user: req.user,
    service: DataReportsService,
    method: 'getStatisticsReports',
    args: {
      instrumentType: servicedata_types.InstrumentType[req.body.data.instrumentType],
      searchDate: req.body.data.date,
      currencyId: req.body.data.currencyId
    }
  };
  console.log(req.user);

  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else res.json(ctx.responseObject);
  });
};

exports.getDeponents = function (req, res) {
  var options = {
    user: req.user,
    service: DataReportsService,
    method: 'getShareholders',
    args: {
      criteria: {
        data: {
          sconsumer: {
            name: req.body.data.name,
            isJuridical: req.body.data.isJuridical ? "true" : "false",
            isAscending: "true",
            skip: req.body.data.skip,
            take: req.body.data.take
          }
        }
      }
    }

  };
  console.log(req.body);

  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else {
      var resp = ctx.responseObject;
      res.json(resp);
    }
  });
};

exports.getIssuers = function (req, res) {
  var options = {
    user: req.user,
    service: DataReportsService,
    method: 'getIssuers',
    args: {
      criteria: {
        data: {
          sconsumer: {
            name: req.body.data.name,
            isJuridical: req.body.data.isJuridical ? "true" : "false",
            isAscending: "true",
            skip: req.body.data.skip,
            take: req.body.data.take
          }
        }
      }
    }

  };

  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else res.json(ctx.responseObject);
  });
};

exports.isUserExist = function (req, res) {
  var options = {
    user: req.user,
    service: DataReportsService,
    method: 'isUserExist',
    args: {
      personID: req.body.personID,
      username: req.body.username
    }
  };

  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else res.json(ctx.responseObject);
  });
};

exports.findPersonFromIamas = function (req, res) {
  if (req.body.idCard.personId) {
    req.body.idCard.personId = String(req.body.idCard.personId);
  }
  var options = {
    user: req.user,
    service: DataReportsService,
    method: 'findPersonFromIamas',
    args: {
      idCard: new uimanager.IDCARD({
        number: req.body.idCard.number,
        pinCode: req.body.idCard.pinCode,
        isDMX: req.body.idCard.isDMX,
        processInstanceId: req.body.idCard.processInstanceId,
        activityInstanceId: req.body.idCard.activityInstanceId
      })
    }
  };

  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else {
      res.json(ctx.responseObject);
    }
  });
};
exports.getPersons = function (req, res) {

  // gelen datani correct elemek lazimdi.
  var options = {
    user: req.user,
    service: DataReportsService,
    method: "getShareholderPersons",
    args: {
      criteria: {
        data: {
          person: {
            contractID: req.body.data.contractID,
            skip: req.body.data.skip,
            take: req.body.data.take,
            name: req.body.data.name,
            number: req.body.data.number,
            surname: req.body.data.surname,
            pinCode: req.body.data.pinCode
          }
        },
        sort: req.body.data.sort
      }
    }

  };


  console.log(req.body);
  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else {
      res.json(ctx.responseObject);
    }
  });
};

exports.getIssuerPersons = function (req, res) {
    var options = {
        user: req.user,
        service: DataReportsService,
        method: "getIssuerPersons",
        args: {
            criteria: {
                data: {
                    person: {
                        contractID: req.body.data.contractID,
                        skip: req.body.data.skip,
                        take: req.body.data.take,
                        name: req.body.data.name,
                        number: req.body.data.number,
                        surname: req.body.data.surname,
                        pinCode: req.body.data.pinCode
                    }
                },
                sort: req.body.data.sort
            }
        }
    };
    console.log(req.body);
    thrift.createClient(options, function (err, ctx) {
        if (err) console.info(err);
        else {
            res.json(ctx.responseObject);
        }
    });
};

exports.searchJuridicalPersons = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchJuridicalPersons',
    args: {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err) console.info(err);
    else res.json(ctx.responseObject.return);
  });
};

exports.findPersonFromCSDR = function (req, res) {
  var options = {
    user: req.user,
    service: DataReportsService,
    method: 'findPersonFromCSDR',
    args: {
      idCard: req.body.idCard
    }
  };

  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else {
      res.json(ctx.responseObject);
    }
  });
};

exports.proceedTRSBatchMessages = function (req, res) {
  var options = {
    user: req.user,
    service: DataReportsService,
    method: 'proceedTRSBatchMessages',
    args: {}
  };

  thrift.createClient(options, function (err, ctx) {
    if (err) console.info(err);
    else {
      res.json(ctx.responseObject);
    }
  });
};


exports.checkCouponPayment = function (req, res) {
    var options = {
        user: req.user,
        service: DataReportsService,
        method: 'checkCouponPayment',
        args: {
            taskId: req.params.taskId
        }
    };

    thrift.createClient(options, function (err, ctx) {
        if (err) console.info(err);
        else {
            res.json(ctx.responseObject);
        }
    });
};

exports.proceedTRSBatchProcess = function (req, res) {
    var options = {
        user: req.user,
        service: DataReportsService,
        method: 'proceedTRSBatchProcess',
        args: {}
    };

    thrift.createClient(options, function (err, ctx) {
        if (err) console.info(err);
        else
            res.json(ctx.responseObject);
    });
};

exports.getAccountPositions = function (req, res) {
    var options = {
        user: req.user,
        service: DataReportsService,
        method: 'getAccountPositions',
        args: {
            account: req.body.account
        }
    };

    thrift.createClient(options, function (err, ctx) {
        if (err) console.info(err);
        else
            res.json(ctx.responseObject);
    });

};