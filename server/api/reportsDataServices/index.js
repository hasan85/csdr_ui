'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

// api/reportsDataServices/getIssuers

router.post('/getStatisticsReports', auth.requiresLogin, controller.getStatisticsReports);
router.post('/searchJuridicalPersons', auth.requiresLogin, controller.searchJuridicalPersons);
router.post("/getIssuers", auth.requiresLogin, controller.getIssuers);
router.post("/getDeponents", auth.requiresLogin, controller.getDeponents);
router.post("/findPersonFromIamas", auth.requiresLogin, controller.findPersonFromIamas);
router.post("/getShareholderPersons", auth.requiresLogin, controller.getPersons);
router.get("/isUserExist", auth.requiresLogin, controller.isUserExist);
router.post("/findPersonFromCSDR", auth.requiresLogin, controller.findPersonFromCSDR);
router.get("/proceedTRSBatchMessages", auth.requiresLogin, controller.proceedTRSBatchMessages);
router.get('/checkCouponPayment/:taskId', auth.requiresLogin, controller.checkCouponPayment);
router.get('/proceedTRSBatchProcess', auth.requiresLogin, controller.proceedTRSBatchProcess);
router.post('/getAccountPositions', auth.requiresLogin, controller.getAccountPositions);
router.post('/getIssuerPersons', auth.requiresLogin, controller.getIssuerPersons);
module.exports = router;

