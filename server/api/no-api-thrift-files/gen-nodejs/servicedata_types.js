//
// Autogenerated by Thrift Compiler (0.11.0)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//
"use strict";

var thrift = require('thrift');
var Thrift = thrift.Thrift;
var Q = thrift.Q;

var system_ttypes = require('./system_types');


var ttypes = module.exports = {};
ttypes.InstrumentType = {
  'ShortTermBond' : 0,
  'LongTermBond' : 1,
  'Share' : 2,
  'CbarNotes' : 3
};
var IssuerBusinessCapacity = module.exports.IssuerBusinessCapacity = function(args) {
  this.nonfinancial = null;
  this.financial = null;
  this.governmentManagement = null;
  this.natural = null;
  this.publicAndnonCommersion = null;
  this.nonresident = null;
  if (args) {
    if (args.nonfinancial !== undefined && args.nonfinancial !== null) {
      this.nonfinancial = args.nonfinancial;
    }
    if (args.financial !== undefined && args.financial !== null) {
      this.financial = args.financial;
    }
    if (args.governmentManagement !== undefined && args.governmentManagement !== null) {
      this.governmentManagement = args.governmentManagement;
    }
    if (args.natural !== undefined && args.natural !== null) {
      this.natural = args.natural;
    }
    if (args.publicAndnonCommersion !== undefined && args.publicAndnonCommersion !== null) {
      this.publicAndnonCommersion = args.publicAndnonCommersion;
    }
    if (args.nonresident !== undefined && args.nonresident !== null) {
      this.nonresident = args.nonresident;
    }
  }
};
IssuerBusinessCapacity.prototype = {};
IssuerBusinessCapacity.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRING) {
        this.nonfinancial = input.readString();
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.STRING) {
        this.financial = input.readString();
      } else {
        input.skip(ftype);
      }
      break;
      case 3:
      if (ftype == Thrift.Type.STRING) {
        this.governmentManagement = input.readString();
      } else {
        input.skip(ftype);
      }
      break;
      case 4:
      if (ftype == Thrift.Type.STRING) {
        this.natural = input.readString();
      } else {
        input.skip(ftype);
      }
      break;
      case 5:
      if (ftype == Thrift.Type.STRING) {
        this.publicAndnonCommersion = input.readString();
      } else {
        input.skip(ftype);
      }
      break;
      case 6:
      if (ftype == Thrift.Type.STRING) {
        this.nonresident = input.readString();
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

IssuerBusinessCapacity.prototype.write = function(output) {
  output.writeStructBegin('IssuerBusinessCapacity');
  if (this.nonfinancial !== null && this.nonfinancial !== undefined) {
    output.writeFieldBegin('nonfinancial', Thrift.Type.STRING, 1);
    output.writeString(this.nonfinancial);
    output.writeFieldEnd();
  }
  if (this.financial !== null && this.financial !== undefined) {
    output.writeFieldBegin('financial', Thrift.Type.STRING, 2);
    output.writeString(this.financial);
    output.writeFieldEnd();
  }
  if (this.governmentManagement !== null && this.governmentManagement !== undefined) {
    output.writeFieldBegin('governmentManagement', Thrift.Type.STRING, 3);
    output.writeString(this.governmentManagement);
    output.writeFieldEnd();
  }
  if (this.natural !== null && this.natural !== undefined) {
    output.writeFieldBegin('natural', Thrift.Type.STRING, 4);
    output.writeString(this.natural);
    output.writeFieldEnd();
  }
  if (this.publicAndnonCommersion !== null && this.publicAndnonCommersion !== undefined) {
    output.writeFieldBegin('publicAndnonCommersion', Thrift.Type.STRING, 5);
    output.writeString(this.publicAndnonCommersion);
    output.writeFieldEnd();
  }
  if (this.nonresident !== null && this.nonresident !== undefined) {
    output.writeFieldBegin('nonresident', Thrift.Type.STRING, 6);
    output.writeString(this.nonresident);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var DetailedInfo = module.exports.DetailedInfo = function(args) {
  this.issuer = null;
  this.capacity = null;
  if (args) {
    if (args.issuer !== undefined && args.issuer !== null) {
      this.issuer = args.issuer;
    }
    if (args.capacity !== undefined && args.capacity !== null) {
      this.capacity = new ttypes.IssuerBusinessCapacity(args.capacity);
    }
  }
};
DetailedInfo.prototype = {};
DetailedInfo.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRING) {
        this.issuer = input.readString();
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.STRUCT) {
        this.capacity = new ttypes.IssuerBusinessCapacity();
        this.capacity.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

DetailedInfo.prototype.write = function(output) {
  output.writeStructBegin('DetailedInfo');
  if (this.issuer !== null && this.issuer !== undefined) {
    output.writeFieldBegin('issuer', Thrift.Type.STRING, 1);
    output.writeString(this.issuer);
    output.writeFieldEnd();
  }
  if (this.capacity !== null && this.capacity !== undefined) {
    output.writeFieldBegin('capacity', Thrift.Type.STRUCT, 2);
    this.capacity.write(output);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var IssuerBusinessClass = module.exports.IssuerBusinessClass = function(args) {
  this.capacity = null;
  this.detailedInfos = null;
  if (args) {
    if (args.capacity !== undefined && args.capacity !== null) {
      this.capacity = new ttypes.IssuerBusinessCapacity(args.capacity);
    }
    if (args.detailedInfos !== undefined && args.detailedInfos !== null) {
      this.detailedInfos = Thrift.copyList(args.detailedInfos, [ttypes.DetailedInfo]);
    }
  }
};
IssuerBusinessClass.prototype = {};
IssuerBusinessClass.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRUCT) {
        this.capacity = new ttypes.IssuerBusinessCapacity();
        this.capacity.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.LIST) {
        var _size0 = 0;
        var _rtmp34;
        this.detailedInfos = [];
        var _etype3 = 0;
        _rtmp34 = input.readListBegin();
        _etype3 = _rtmp34.etype;
        _size0 = _rtmp34.size;
        for (var _i5 = 0; _i5 < _size0; ++_i5)
        {
          var elem6 = null;
          elem6 = new ttypes.DetailedInfo();
          elem6.read(input);
          this.detailedInfos.push(elem6);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

IssuerBusinessClass.prototype.write = function(output) {
  output.writeStructBegin('IssuerBusinessClass');
  if (this.capacity !== null && this.capacity !== undefined) {
    output.writeFieldBegin('capacity', Thrift.Type.STRUCT, 1);
    this.capacity.write(output);
    output.writeFieldEnd();
  }
  if (this.detailedInfos !== null && this.detailedInfos !== undefined) {
    output.writeFieldBegin('detailedInfos', Thrift.Type.LIST, 2);
    output.writeListBegin(Thrift.Type.STRUCT, this.detailedInfos.length);
    for (var iter7 in this.detailedInfos)
    {
      if (this.detailedInfos.hasOwnProperty(iter7))
      {
        iter7 = this.detailedInfos[iter7];
        iter7.write(output);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var StatisticsReports = module.exports.StatisticsReports = function(args) {
  this.financialSector = null;
  this.nonfinancialSector = null;
  this.governmentManagementCompanies = null;
  if (args) {
    if (args.financialSector !== undefined && args.financialSector !== null) {
      this.financialSector = new ttypes.IssuerBusinessClass(args.financialSector);
    }
    if (args.nonfinancialSector !== undefined && args.nonfinancialSector !== null) {
      this.nonfinancialSector = new ttypes.IssuerBusinessClass(args.nonfinancialSector);
    }
    if (args.governmentManagementCompanies !== undefined && args.governmentManagementCompanies !== null) {
      this.governmentManagementCompanies = new ttypes.IssuerBusinessClass(args.governmentManagementCompanies);
    }
  }
};
StatisticsReports.prototype = {};
StatisticsReports.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRUCT) {
        this.financialSector = new ttypes.IssuerBusinessClass();
        this.financialSector.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.STRUCT) {
        this.nonfinancialSector = new ttypes.IssuerBusinessClass();
        this.nonfinancialSector.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 3:
      if (ftype == Thrift.Type.STRUCT) {
        this.governmentManagementCompanies = new ttypes.IssuerBusinessClass();
        this.governmentManagementCompanies.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

StatisticsReports.prototype.write = function(output) {
  output.writeStructBegin('StatisticsReports');
  if (this.financialSector !== null && this.financialSector !== undefined) {
    output.writeFieldBegin('financialSector', Thrift.Type.STRUCT, 1);
    this.financialSector.write(output);
    output.writeFieldEnd();
  }
  if (this.nonfinancialSector !== null && this.nonfinancialSector !== undefined) {
    output.writeFieldBegin('nonfinancialSector', Thrift.Type.STRUCT, 2);
    this.nonfinancialSector.write(output);
    output.writeFieldEnd();
  }
  if (this.governmentManagementCompanies !== null && this.governmentManagementCompanies !== undefined) {
    output.writeFieldBegin('governmentManagementCompanies', Thrift.Type.STRUCT, 3);
    this.governmentManagementCompanies.write(output);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

