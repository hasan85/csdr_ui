namespace java az.mdm.thrift.service
namespace js thrift.service

include "system.thrift"
include "servicedata.thrift"
include "commondata.thrift"
include "uimanager.thrift"

/**
* -----------1661 - view-securities-report--------------------------
* 1661 pəncərəsində statistik məlumatları gətirmək üzün istifadə olunan metoddur(funkiyadır)
* getStatisticsReports 3 parametr alır:
* --1-tokenInfo - log-in olmağını yoxlamaq üçün istifadə olunur.
* --2-instrumentType - 1661 pəncərəsində kağızın tipi daxil edilir.Məcburidir
* --3-searchDate - 1661 pəncərəsindən tarix seçilir.Tarik verilmədikdə mövcud tarixi qəbul edir.
* --4-currencyId - 1661 pəncərəsindən valyuta seçilir.Məcburidir
**/
service DataSearchService {
     system.String  getStatisticsReports(1: commondata.TokenLoginInfo tokenInfo,2: servicedata.InstrumentType instrumentType,3: system.Date searchDate,4: system.Long currencyId),
     system.String  getShareholders(1: commondata.TokenLoginInfo tokenInfo,2: commondata.SearchCriteria criteria),
     system.String  getIssuers(1: commondata.TokenLoginInfo tokenInfo,2: commondata.SearchCriteria criteria),
     system.String  getShareholderPersons(1: commondata.TokenLoginInfo tokenInfo,2: commondata.SearchCriteria criteria),
     system.String  getIssuerPersons(1: commondata.TokenLoginInfo tokenInfo,2: commondata.SearchCriteria criteria),
     system.String  findPersonFromIamas(1: commondata.TokenLoginInfo tokenInfo,2: uimanager.IDCARD idCard),
     system.String  findPersonFromCSDR(1: commondata.TokenLoginInfo tokenInfo,2: uimanager.IDCARD idCard),
     system.String  isUserExist(1: commondata.TokenLoginInfo tokenInfo,2: system.String username),
     system.String  proceedTRSBatchMessages(1: commondata.TokenLoginInfo tokenInfo),
     system.String  proceedTRSBatchProcess(1: commondata.TokenLoginInfo tokenInfo),
     system.String  getNaturalPersons(1: commondata.TokenLoginInfo tokenInfo,2: commondata.SearchCriteria criteria),
     system.String  checkCouponPayment(1: commondata.TokenLoginInfo tokenInfo,2: system.String taskId),
     system.String  sendPacket(1: system.String packet),
     system.String  getAccountPositions(1: commondata.TokenLoginInfo tokenInfo,2: commondata.VMAccount account)
}

