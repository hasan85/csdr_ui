namespace java az.mdm.thrift.common
namespace js thrift.common

include "servicedata.thrift"
include "system.thrift"
include "uimanager.thrift"

/**
* Xəta baş verdikdə xətanın kodunu və bu xətaya uyğun mesajı özündə saxlayır
**/
struct ExceptionModel{
     1:  system.String resultCode,
     2:  system.String message
}
/**
* StatisticsReports class-ından məlumat gəldikdə gələn məlumatlı verir
**/
union TObject {
    1:  servicedata.StatisticsReports statisticsReports,
    2:  uimanager.FindPerson findPerson,
    4:  uimanager.PersonalData person,
    5:  system.boolean isUserExist,
    6:  system.boolean isNull,
    7:  system.boolean isSuccess
}

struct TObjectList {
    1:  list<uimanager.SearchPersons> searchPersons,
    2:  list<uimanager.UIPerson> persons,
    3:  system.long total
}

/**
* Məlumat olduqda məlumatı xəta olduqda xətanı verir
**/
union SearchResult {
    1:  TObject data,
    2:  ExceptionModel exceptionModel
}

union SearchResultList {
    1:  TObjectList data,
    2:  ExceptionModel exceptionModel,
}
/**
* Istifadəçi log-in olan zaman istifadəçi məlumatını özündə cəmləyən token yaranır.
* Həmin token və digər log-in məlumatlarını özündə saxlayır
**/
struct TokenLoginInfo {
    1:  system.String userToken,
    2:  system.boolean isAsanImzaLogin,
    3:  system.String phoneNumber,
    4:  system.String asanUserId
}

struct TaskData {
    1:  system.String id,
    2:  system.String processInstanceId,
    3:  system.String processKey,
    4:  system.String key,
    5:  system.String nameAz,
    6:  system.String nameEn,
    7:  system.String createDate,
    8:  system.String dueDate,
    9:  system.String description,
    10: system.int priority,
    11: list<TaskComment> comment,
    12: list<TaskAttachment> document,
    13: system.String processNameAz,
    14: system.String processNameEn,
    15: system.String draft,
    16: system.String approverDecision,
    17: system.String assigne,
    18: system.String previousTaskAssigne,
    19: system.String endDate,
    20: system.String status,
    21: system.String screenID,
    22: system.String shortDescription
}

struct TaskComment {
    1: system.String id,
    2: system.String createDate,
    3: system.String userId,
    4: system.String taskId,
    5: system.String processInstanceId,
    6: system.String message,
}

struct TaskAttachment {
    1: system.VMLocaleName documentClass,
    2: system.int minLimit,
    4: system.int maxLimit,
    5: system.boolean readonly,
    6: system.String condition,
    7: list<VMDocumentData> documents
}

struct VMDocumentData {
    1: system.String ID,
    2: system.String documentData
}

struct SearchCriteria {
    1: SearchData data,
    2: list<SortDescription> sort
}

union SearchData {
    1: uimanager.SConsumerCriteria sconsumer,
    2: uimanager.PersonCriteria person
}

struct SortDescription {
    1: system.String field,
    2: system.String dir
}


struct VMAccount {
    1: system.String id,
    2: system.String accountId,
    3: system.String accountNumber,
    4: system.String name
}


