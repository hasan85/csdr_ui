namespace java az.mdm.thrift.system
namespace js thrift.system


//Bu tiplər bütün modellərdə istifadə olunur
typedef bool boolean
typedef i16 short
typedef i32 int
typedef i64 long
typedef string Date  //yyyy-MM-dd'T'HH:mm:ss
typedef string BigDecimal
typedef string String
typedef string Long

/**Bu class Javada Long obyektini əvəz etmək üçün nəzərdə tutulub
* Javada long primitiv tipi null qəbul etmir.long primitiv tipini obyekt kimi istifadə
* etmək lazım olduqda istifadə olunur
**/

struct TSimpleValue {
   1: optional String id,
   2: String value
}

struct VMLocaleName {
    1: String id,
    2: String nameAz,
    3: String nameEn,
    4: String code
}

