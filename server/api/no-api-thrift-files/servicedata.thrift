namespace java az.mdm.thrift.viewmodels.statisticsreports
namespace js thrift.viewmodels.statisticsreports

include "system.thrift"

/**
* -----------1661 - view-securities-report--------------------------
* Qiymətli kağızın növlərini özündə saxlayır
* --ShortTermBond - Qısamüddətli istiqraz
* --LongTermBond - Uzunmüddətli istiqraz
* --Share - Səhm
* --CbarNotes - Mərkəzi Bankın Notları
**/
enum InstrumentType {
        ShortTermBond,LongTermBond,Share,CbarNotes
}


/**
* -----------1661 - view-securities-report--------------------------
* Emitentin Bizness sinifinə uyğun deponentin biznes siniflərinə görə balans üzrə həcmləri özündə saxlayır
**/
struct IssuerBusinessCapacity {
     1: system.BigDecimal nonfinancial, //Qeyri-maliyyə sektoru üzrə həcm
     2: system.BigDecimal financial, //Maliyyə sektoru üzrə həcm
     3: system.BigDecimal  governmentManagement,
     4: system.BigDecimal natural, //Dövlət idarəetmə orqanları üzrə həcm
     5: system.BigDecimal publicAndnonCommersion, //Ev təsərrüfatı (fiziki şəxslər)
     6: system.BigDecimal nonresident,//Qeyri-rezident deponentlərin depo hesablarında olan həcm
}

/**
* -----------1661 - view-securities-report--------------------------
* Hər həcm üçün detallı məlumatı özündə saxlayır
**/
struct DetailedInfo {
     1: system.String issuer, //emitentin adı
     2: IssuerBusinessCapacity capacity //emitentin deponentin biznes siniflərinə görə həcmləri
}

/**
* -----------1661 - view-securities-report--------------------------
* 1661 pəncərəsinə qoyulacaq ümumi həcmləri və bu həcmlər haqqında detallı məlumatı özündə saxlayır
**/
struct IssuerBusinessClass {
     1: IssuerBusinessCapacity capacity, //ümumi həcmlər
     2: list<DetailedInfo> detailedInfos //detallı məlumat
}


/**
* -----------1661 - view-securities-report--------------------------
* 1661 pəncərəsindəki biznes siniflərinə görə statistik məlumatları özündə saxlayır
**/
struct StatisticsReports {
     1: IssuerBusinessClass financialSector, //Maliyyə sektoru üzrə statistik məlumatlar
     2: IssuerBusinessClass nonfinancialSector, //Qeyri-maliyyə sektoru üzrə statistik məlumatlar
     3: IssuerBusinessClass governmentManagementCompanies //Dövlət idarəetmə orqanları üzrə statistik məlumatlar
}

