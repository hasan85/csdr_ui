/**
* -----------UI Interface--------------------------
**/

namespace java az.mdm.thrift.viewmodels.uimanager
namespace js thrift.viewmodels.uimanager

include "system.thrift"

enum AuthMethod{
   WithPassword,WithAsanImza
}

struct SearchPersons {
     1:  optional system.String ID,
     2:  optional system.String emailID,
     3:  optional system.String emailAddress,
     4:  optional system.String userID,
     5:  optional system.String username,
     6:  system.boolean isHasUsername,
     7:  optional AuthMethod authMethod,
     8:  system.String name,
     9:  optional system.String serial,
     10: optional system.String number,
     11: optional system.String pinCode,
     12: system.boolean isJuridical,
     13: system.boolean isIamas,
     14: optional system.String countryId,
     15: optional system.String countryCode,
     16: optional system.String country2A,
     17: optional system.String country3A,
     18: optional system.String countryName
}

struct UIPerson {
     1:  optional system.String ID,
     2:  system.TSimpleValue email,
     3:  system.TSimpleValue user,
     4:  system.boolean isHasUsername,
     5:  optional AuthMethod authMethod,
     6:  system.VMLocaleName name,
     7:  system.VMLocaleName surname,
     8:  system.VMLocaleName fathername,
     9:  system.String birthDate,
     10: UIIDCard idCard,
     11: system.TSimpleValue pinCode,
     12: UICountry country,
     13: system.TSimpleValue address,
     14: system.boolean isIamas
}

struct PersonalData {
     1:  optional system.String ID,
     2:  system.VMLocaleName name,
     3:  system.VMLocaleName surname,
     4:  system.VMLocaleName fathername,
     5:  system.String birthDate,
     6:  UIIDCard idCard,
     7:  UIIDCard passport,
     8:  system.TSimpleValue pinCode,
     9:  UICountry country,
     10:  system.TSimpleValue legalAddress,
     11: system.TSimpleValue actualAddress,
     12: list<PersonalBankAccount> bankAccounts,
     13: list<system.TSimpleValue> emails,
     14: list<PersonalPhone> phoneNumbers
     15: system.boolean isIamas
}

struct PersonalPhone {
    1:  optional system.String id,
    2:  system.VMLocaleName type,
    3:  system.String number,
    4:  system.VMLocaleName operator
}

struct PersonalBankAccount {
   1:  system.String id,
   2:  system.String bankNameAz,
   3:  system.String bankNameEn,
   4:  system.String branchNameAz,
   5:  system.String branchNameEn,
   6:  system.String iban,
   7:  system.String account,
   8:  system.String currentAccount,
   9:  system.String correspondentAccount,
   10: system.String tin,
   11: system.String swiftbic,
   12: system.VMLocaleName currency,
   13: system.String bankCode,
   14: system.String correspondentBankName,
   15: system.String correspondentBankSwiftCode
}

struct UIIDCard {
   1: optional system.String id,
   2: system.String series,
   3: system.String number
}

struct UICountry {
   1:  optional system.String id,
   2:  system.String numericCode,
   3:  system.String alfa2Code,
   4:  system.String alfa3Code,
   5:  system.String name
}

struct FindPerson {
   1:  PersonalData fromIAMAS,
   2:  PersonalData fromCSDR,
   3:  list<PersonalData> fromIAMASByPin,
}

struct IDCARD {
    1:  system.String series,
    2:  system.String number,
    3:  system.String pinCode,
    4:  system.boolean isDMX,
    5:  system.String processInstanceId,
    6:  system.String activityInstanceId
}

struct SConsumerCriteria {
    1:  system.String name,
    2:  system.String isJuridical,
    3:  system.String isAscending,
    4:  system.int skip,
    5:  system.int take
}

struct PersonCriteria {
    1:  system.String contractID,
    2:  system.String contractClassCode,
    3:  system.String series,
    4:  system.String number,
    5:  system.String passportSeries,
    6:  system.String passportNumber,
    7:  system.String pinCode,
    8:  system.String birthDate,
    9:  system.String name,
    10: system.String surname,
    11: system.String fathername,
    12: system.String address
    13: system.int skip,
    14: system.int take,
    15: system.long personID
}




