'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.post('/getNetObligationRecords', auth.requiresLogin, controller.getNetObligationRecords);
router.post('/getInstrumentObligations', auth.requiresLogin, controller.getInstrumentObligations);

module.exports = router;
