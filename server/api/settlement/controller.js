'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

exports.getNetObligationRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getNetObligationRecords',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });
};
exports.getInstrumentObligations = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInstrumentObligations',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });
};
