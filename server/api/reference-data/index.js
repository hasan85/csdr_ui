'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();


router.get('/getCountries', auth.requiresLogin, controller.getCountries);
router.get('/getCurrencies', auth.requiresLogin, controller.getCurrencies);
router.get('/getPhoneNumberTypes', auth.requiresLogin, controller.getPhoneNumberTypes);
router.get('/getIdDocumentTypes', auth.requiresLogin, controller.getIdDocumentTypes);
router.get('/getSignerPositions', auth.requiresLogin, controller.getSignerPositions);
router.get('/getPersonClasses', auth.requiresLogin, controller.getPersonClasses);
router.get('/getLegalFormClasses', auth.requiresLogin, controller.getLegalFormClasses);
router.get('/getBusinessClasses', auth.requiresLogin, controller.getBusinessClasses);
router.get('/getApprovalTaskDecisions', auth.requiresLogin, controller.getApprovalTaskDecisions);
router.get('/getAdminTaskDecisions', auth.requiresLogin, controller.getAdminTaskDecisions);
router.get('/getInstrumentIdentificatorTypes', auth.requiresLogin, controller.getInstrumentIdentificatorTypes);
router.get('/getIndividualSecurityGroups', auth.requiresLogin, controller.getIndividualSecurityGroups);
router.get('/getRegistrationAuthorityClasses', auth.requiresLogin, controller.getRegistrationAuthorityClasses);
router.get('/getCFIClasses/:parentId', auth.requiresLogin, controller.getCFIClasses);
router.get('/getcurrentCFIClasses/:parentId', auth.requiresLogin, controller.getcurrentCFIClasses);
router.get('/getCFICategoryByPattern/:pattern', auth.requiresLogin, controller.getCFICategoryByPattern);
router.get('/getPlacementClasses/', auth.requiresLogin, controller.getPlacementClasses);
router.get('/getCorporateActionCategories/', auth.requiresLogin, controller.getCorporateActionCategories);
router.get('/getCorporateActionClassesByCategory/:categoryId', auth.requiresLogin, controller.getCorporateActionClassesByCategory);
router.get('/getCouponPaymentMethodClasses/', auth.requiresLogin, controller.getCouponPaymentMethodClasses);
router.get('/getCAInstrumentDeletionClasses/', auth.requiresLogin, controller.getCAInstrumentDeletionClasses);
router.get('/getFractionTreatmentClasses/', auth.requiresLogin, controller.getFractionTreatmentClasses);
router.get('/getCABonusShareClasses/', auth.requiresLogin, controller.getCABonusShareClasses);
router.get('/getActionClasses/', auth.requiresLogin, controller.getActionClasses);
router.get('/getMemberRoles/', auth.requiresLogin, controller.getMemberRoles);
router.get('/getJuridicalPersonIdDocumentTypes/', auth.requiresLogin, controller.getJuridicalPersonIdDocumentTypes);
router.get('/getNaturalPersonIdDocumentTypes/', auth.requiresLogin, controller.getNaturalPersonIdDocumentTypes);
router.get('/getSubjectContextClasses/', auth.requiresLogin, controller.getSubjectContextClasses);
router.get('/getObjectRoleClasses/', auth.requiresLogin, controller.getObjectRoleClasses);
router.get('/getOperationTypeClasses/', auth.requiresLogin, controller.getOperationTypeClasses);
router.get('/getOrgNodeClasses/', auth.requiresLogin, controller.getOrgNodeClasses);
router.get('/getTaskTypes/', auth.requiresLogin, controller.getTaskTypes);
router.get('/getAccountStatusCheckingClasses/', auth.requiresLogin, controller.getAccountStatusCheckingClasses);
router.get('/getAccountRoles/', auth.requiresLogin, controller.getAccountRoles);
router.get('/getFunctionalAccountClasses/', auth.requiresLogin, controller.getFunctionalAccountClasses);
router.get('/getAuctionClasses/', auth.requiresLogin, controller.getAuctionClasses);
router.get('/getServiceClasses/', auth.requiresLogin, controller.getServiceClasses);
router.get('/getAZIPSSessionInfo/', auth.requiresLogin, controller.getAZIPSSessionInfo);

module.exports = router;
