'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.referenceData.namespace;
var url = config.servers.referenceData.url;

exports.getCountries = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCountries'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getCurrencies = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCurrencies'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getPhoneNumberTypes = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPhoneNumberTypes'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getIdDocumentTypes = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getIDDocumentTypes'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getSignerPositions = function (req, res) {


  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSignerPositions'

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getPersonClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPersonClasses'

  };

  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx.responseObject);
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getLegalFormClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getLegalFormClasses'

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getBusinessClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getBusinessClasses'

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getApprovalTaskDecisions = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getApprovalTaskDecisions'

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getAdminTaskDecisions = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAdminTaskDecisions'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getInstrumentIdentificatorTypes = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInstrumentIdentificatorTypes'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getIndividualSecurityGroups = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getIndividualSecurityGroups'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getRegistrationAuthorityClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getRegistrationAuthorityClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getcurrentCFIClasses = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getcurrentCFIClasses",
    args: {
      parentId: req.params.parentId
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getCFIClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCFIClasses',
    args: {
      parentId: req.params.parentId
    }

  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getCFICategoryByPattern = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCFICategoryByPattern',
    args: {
      pattern: req.params.pattern
    }

  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getPlacementClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPlacementClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getCorporateActionCategories = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCorporateActionCategories'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getCorporateActionClassesByCategory = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCorporateActionClassesByCategory',
    args: {
      categoryId: req.params.categoryId
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getCouponPaymentMethodClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCouponPaymentMethodClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getCAInstrumentDeletionClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCAInstrumentDeletionClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getFractionTreatmentClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getFractionTreatmentClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getCABonusShareClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCABonusShareClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getActionClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getActionClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getMemberRoles = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMemberRoles'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getJuridicalPersonIdDocumentTypes = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getJuridicalPersonIdDocumentTypes'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getNaturalPersonIdDocumentTypes = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getNaturalPersonIdDocumentTypes'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getSubjectContextClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSubjectContextClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getObjectRoleClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getObjectRoleClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getOperationTypeClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOperationTypeClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getOrgNodeClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOrgNodeClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getTaskTypes = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTaskTypes'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAccountStatusCheckingClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountStatusCheckingClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAccountRoles = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountRoles'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getFunctionalAccountClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getFunctionalAccountClasses'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAuctionClasses = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAuctionClasses'
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getServiceClasses = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getServiceClasses'
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};




exports.getAZIPSSessionInfo = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAZIPSSessionInfo'
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

