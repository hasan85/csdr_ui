'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

exports.getMemberAdvances = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: 'getMemberAdvances'
    };
    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else {
            res.json(ctx.responseObject.return);
        }
    });
};

exports.getBankStatements = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getBankStatements',
    args: {criteria: req.body.criteria}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });


};
exports.searchClientShareholder = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchClientShareholder',
    args: {criteria: req.body.data}
  };
  console.log(req.body);
  soap.createClient(url, options,function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getClearingLimits = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getClearingLimits'
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });


};
exports.getWithdrawals = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getWithdrawals',
    args: {
      criteria: req.body.data
    }
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });


};

exports.getTradingMemberAccount = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getTradingMemberAccount",
    args: {criteria: req.body.criteria}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getClientShareholders = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getClientShareholders",
    args: {criteria: req.body.criteria}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getMoneyAccounts = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getMoneyAccounts",
    args: {criteria: req.body.criteria}
  };
  console.log(req.body);
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getAllClientAccounts = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getAllClientAccounts",
    args: {criteria: req.body.criteria}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getTradingMemberAccountById = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getTradingMemberAccountById",
    args: {criteria: req.body.criteria}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getAccountRecordByWithdrawalID = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getAccountRecordByWithdrawalID",
    args: {operationID: req.body.operationID}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getTradingMemberWithClientSecurityPositions = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getTradingMemberWithClientSecurityPositions",

  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else

      res.json(ctx.responseObject.return);
  });
};
exports.getTradingMemberWithClientMoneyPositions = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getTradingMemberWithClientMoneyPositions",
    args: {
      id: req.params.id
    }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getAccountPositions = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountPositions',
    args: {account: req.body.account}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else {
        res.json(ctx.responseObject.return);
    }
  });
};

exports.getSwiftPayment = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getSwiftPayment",
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getMatchedRecordByPaymentID = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getMatchedRecordByPaymentID",
    args: req.body
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};



exports.printPaymentOrder = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "printPaymentOrder",
    args: {criteria: req.body}
  };

  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
