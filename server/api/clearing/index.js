'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.post('/getMemberAdvances', auth.requiresLogin, controller.getMemberAdvances);
router.post('/getBankStatements', auth.requiresLogin, controller.getBankStatements);
router.post('/getClearingLimits', auth.requiresLogin, controller.getClearingLimits);
router.get('/getTradingMemberWithClientMoneyPositions/:id', auth.requiresLogin, controller.getTradingMemberWithClientMoneyPositions);
router.get('/getTradingMemberWithClientSecurityPositions', auth.requiresLogin, controller.getTradingMemberWithClientSecurityPositions);
router.post('/getTradingMemberAccount', auth.requiresLogin, controller.getTradingMemberAccount);
router.post('/getClientShareholders', auth.requiresLogin, controller.getClientShareholders);
router.post('/getTradingMemberAccountById', auth.requiresLogin, controller.getTradingMemberAccountById);
router.post('/getAccountPositions', auth.requiresLogin, controller.getAccountPositions);
router.post('/getWithdrawals', auth.requiresLogin, controller.getWithdrawals);
router.post('/getAllClientAccounts', auth.requiresLogin, controller.getAllClientAccounts);
router.post('/getSwiftPayment', auth.requiresLogin, controller.getSwiftPayment);
router.post('/getMatchedRecordByPaymentID', auth.requiresLogin, controller.getMatchedRecordByPaymentID);
router.post('/getMoneyAccounts', auth.requiresLogin, controller.getMoneyAccounts);
router.post('/printPaymentOrder', auth.requiresLogin, controller.printPaymentOrder);
router.post('/getAccountRecordByWithdrawalID', auth.requiresLogin, controller.getAccountRecordByWithdrawalID);
router.post('/searchClientShareholder', auth.requiresLogin, controller.searchClientShareholder);
module.exports = router;
