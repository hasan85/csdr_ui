'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.post('/getPersons', auth.requiresLogin, controller.getPersons);
router.post('/getMemberClientAccounts', auth.requiresLogin, controller.getMemberClientAccounts);
router.post('/getParticipantAccounts', auth.requiresLogin, controller.getParticipantAccounts);
router.post('/getShareHolders', auth.requiresLogin, controller.getShareHolders);
router.post('/getIssuers', auth.requiresLogin, controller.getIssuers);
router.post('/getInstrumentsInCommissionExceptionListByIssuer', auth.requiresLogin, controller.getInstrumentsInCommissionExceptionListByIssuer);
router.post('/getInstrumentsOutOfCommissionExceptionListByIssuer', auth.requiresLogin, controller.getInstrumentsOutOfCommissionExceptionListByIssuer);

router.post('/getBillingMembers', auth.requiresLogin, controller.getBillingMembers);
router.post('/getDepositoryMembers', auth.requiresLogin, controller.getDepositoryMembers);
router.post('/getClearingMembers', auth.requiresLogin, controller.getClearingMembers);
router.post('/getTradingMembers', auth.requiresLogin, controller.getTradingMembers);
router.post('/searchTradingMembersAndSO', auth.requiresLogin, controller.searchTradingMembersAndSO);
router.post('/getSettlementAgents', auth.requiresLogin, controller.getSettlementAgents);
router.post('/getAnotherShareholderAccount', auth.requiresLogin, controller.getAnotherShareholderAccount);


router.post('/getDepositories', auth.requiresLogin, controller.getDepositories);
router.post('/getNominees', auth.requiresLogin, controller.getNominees);
router.post('/getPledgees', auth.requiresLogin, controller.getPledgees);
router.post('/getSystemOwners', auth.requiresLogin, controller.getSystemOwners);

router.post('/getMembers', auth.requiresLogin, controller.getMembers);
router.post('/getDepoMembers', auth.requiresLogin, controller.getDepoMembers);
router.post('/getBrokers', auth.requiresLogin, controller.getBrokers);


router.post('/getShareholdersOrIssuers', auth.requiresLogin, controller.getShareholdersOrIssuers);
router.post('/getTradingOrDepoMembers', auth.requiresLogin, controller.getTradingOrDepoMembers);

router.post('/checkAccountStatusByRole', auth.requiresLogin, controller.checkAccountStatusByRole);
router.get('/getPersonById/:id', auth.requiresLogin, controller.getPersonById);

router.post('/getFirstPersonByCriteria/', auth.requiresLogin, controller.getFirstPersonByCriteria);
router.get('/getPersonDetails/:personId', auth.requiresLogin, controller.getPersonDetails);

router.post('/getAccountPrescriptions', auth.requiresLogin, controller.getAccountPrescriptions);

router.post('/getUnderwriters', auth.requiresLogin, controller.getUnderwriters);

router.post('/getShareholderAccountsWithOperatorName', auth.requiresLogin, controller.getShareholderAccountsWithOperatorName);
router.post('/searchOtherTradingMembers', auth.requiresLogin, controller.searchOtherTradingMembers);


module.exports = router;
