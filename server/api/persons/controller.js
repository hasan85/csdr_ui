'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

exports.getPersons = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchPerson',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });


};

exports.getMemberClientAccounts = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMemberClientAccounts',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getParticipantAccounts = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getParticipantAccounts',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getShareHolders = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchShareholder',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getIssuers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchIssuer',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getInstrumentsInCommissionExceptionListByIssuer = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInstrumentsInCommissionExceptionListByIssuer',
    args: req.body.data
  };


  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getInstrumentsOutOfCommissionExceptionListByIssuer = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getInstrumentsOutOfCommissionExceptionListByIssuer',
    args: req.body.data
  };


  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getMembers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchMember',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getBillingMembers = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getBillingMembers',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getClearingMembers = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchClearingMember',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getTradingMembers = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchTradingMember',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.searchTradingMembersAndSO = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: 'searchTradingMembersAndSO',
        args: {criteria: req.body.data}
    };
    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else
            res.json(ctx.responseObject.return);
    });
};
exports.getSettlementAgents = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchSettlementAgent',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getDepositoryMembers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getDepositoryMembers',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getDepositories = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getDepositories',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getNominees = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getNominees',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getPledgees = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchPledgee',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getSystemOwners = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSystemOwners',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getDepoMembers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchDepoMember',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getBrokers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchBroker',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getTradingOrDepoMembers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchTradingOrDepoMember',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getShareholdersOrIssuers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchShareholderOrIssuer',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getPersonById = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPersonById',
    args: {
      personId: req.params.id
    }
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.checkAccountStatusByRole = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'checkAccountStatusByRole',
    args: {
      criteria: req.body.criteria,
      roleClassCode: req.body.roleClassCode
    }
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getFirstPersonByCriteria = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getFirstPersonByCriteria',
    args: {criteria: req.body.criteria}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getPersonDetails = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPersonDetails',
    args: {personID: req.params.personId}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAccountPrescriptions = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountPrescriptions',
    args: {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getUnderwriters = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getUnderwriters',
    args: {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};


exports.getShareholderAccountsWithOperatorName = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getShareholderAccountsWithOperatorName',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.searchOtherTradingMembers = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchOtherTradingMembers',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getAnotherShareholderAccount = function (req, res) {
  var requestData = {accountID: req.body.data.accountId, personID: req.body.data.personId}
  console.log(requestData)
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAnotherShareholderAccount',
    args: requestData
  };
  soap.createClient(url, options, function (err, ctx) {
    console.log(requestData)

    if (err)
      console.info(err);
    else
      console.log(ctx.responseObject.return)
      res.json(ctx.responseObject.return);
  });
};
