'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');
var soap = require("../../lib/soap-wrapper");
var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;
var fs = require('fs');
var multer = require('multer');
var upload = multer({
  dest: config.root + '/uploads/tmp/',
  limits: {
    files: 10,
    fileSize: config.fileSizeLimit
  }
}).any();

//var async = require('async');
exports.upload = function (req, res) {

  upload(req, res, function (err) {

    if (err) {
      return res.json({success: false, message: err});
    }

    var remaining = req.files.length;
    var convertedFiles = [];

    req.files.forEach(function (item) {

      var currentFile = item.path;

      fs.readFile(currentFile, function (err, fileBinary) {
        if (err) {
          console.error(err);
        }
        else {
          // Convert file to Base64 encoding
          var base64File = new Buffer(fileBinary, 'binary').toString('base64');

          convertedFiles.push(base64File);
          fs.unlink(currentFile, function (err) {
            if (err) {
              console.error(err);
            }
          });

        }
        remaining -= 1;
        if (remaining == 0) {

          var soapClientOptions = {
            user: req.user,
            namespace: namespace,
            method: 'parseAuctionFiles',
            args: {
              batchFiles: {
                files: convertedFiles,
              }
            }
          };
          soap.createClient(url, soapClientOptions, function (err, ctx) {
            if (err) {
              console.error(err);
            }
            else {
              res.json(ctx.responseObject.return);
            }
          });
        }
      });

    });

  });

};
exports.getTrades = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTrades',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getRepoTrades = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getRepoTrades',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getOrders = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOrders'
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
exports.getSubscriptionInstruments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSubscriptionInstruments'
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
exports.getSubscriptionOrdersBySubscriptionID = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSubscriptionOrdersBySubscriptionID',
    args: {criteria: req.body}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
exports.getFreezingRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getFreezingRecords',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });


};
exports.getPledgeRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPledgeRecords',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getPledgedBalances = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPledgedBalances',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getGrants = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getGrants',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getTitleTransfers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTitleTransfers',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getOTCTrades = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOTCTrades',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getMarketTradeCommission = function (req, res) {
  console.log(req.body);
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMarketTradeCommission',
    args: req.body
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getOTCTradeCommission = function (req, res) {
  console.log(req.body);
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOTCTradeCommission',
    args: req.body
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAccountOpeningTradeCommission = function (req, res) {
  console.log(req.body);
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountOpeningTradeCommission',
    args: req.body
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getTransferPledgedTitles = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTransferPledgedTitles',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getSuccessions = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSuccessions',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getMatchedTrades = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMatchedTrades',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getNonMatchedTrades = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getNonMatchedTrades',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAuctionRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAuctionRecords',
    args: {
      instrumentID: req.params.instrumentId
    }
  };
  console.log(options.args);
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getSubscriptionInstruments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSubscriptionInstruments',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getSubscriptionOrdersBySubscriptionID = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSubscriptionOrdersBySubscriptionID',
    args: {criteria: req.body}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
exports.getPayments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPayments',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};



exports.getWithdrawalData = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getWithdrawalData',
    args: req.body.data
  };

  console.log(options)
  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  })

};

exports.getBankAccounts = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getBankAccounts',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  })

};

exports.getPendingWithdrawals = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPendingWithdrawals',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  })

};

exports.searchTaxBalances = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchTaxBalances',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  })

};

exports.searchClearingMembersAndIssuers = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchClearingMembersAndIssuers',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  })

};


exports.getIncomingPaymentRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getIncomingPaymentRecords',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};

exports.getOutgoingPaymentRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOutgoingPaymentRecords',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
exports.getAccountCashData = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountCashData',
    args: req.body.data
  };

  console.log(options)
  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};

exports.azipsTestServices = function (req, res) {

  console.log(url);

  var options = {
    user: req.user,
    namespace: namespace,
    method: req.body.data.service,
    args: {criteria: req.body.data}
  };

  soap.createClient('http://192.168.31.13:8091/testAZIPSServices', options, function (err, ctx) {
    if (err) {
      // res.json(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });


};


exports.getAccountRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountRecords',
    args: {criteria: req.body.data}
  };

  console.log(options)
  soap.createClient(url, options, function (err, ctx) {
    console.log(ctx)
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};




exports.getOutgoingRecordsByAccountID = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOutgoingRecordsByAccountID',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};




exports.getOutgoingRecordsByAccountAndOperationClass = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOutgoingRecordsByAccountAndOperationClass',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getTaxExchanges = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTaxExchanges',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getRecordsByAccountID = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getRecordsByAccountID',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getOtherMemberClientShareholdersByAccountID = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOtherMemberClientShareholdersByAccountID',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getFreeCashAmount = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getFreeCashAmount',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getCBARDayCloseBalances = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCBARDayCloseBalances',
    args: {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getForeignAccountRecords = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getForeignAccountRecords',
    args: {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getLocalAccountRecords = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getLocalAccountRecords',
    args:  {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getRemainingBalances = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getRemainingBalances',
    args:  {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};




exports.getCBARCashRecord = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCBARCashRecord',
    args:  {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};



exports.getTAXRecord = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTAXRecord',
    args:  {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getNDCCBARCirculationReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getNDCCBARCirculationReport',
    args:  {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getOutgoingRecordsByAccountAndOperationClass = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOutgoingRecordsByAccountAndOperationClass',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getCouponPaymentRecords = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCouponPaymentRecords',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getCouponPaymentRecords2 = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getCouponPaymentRecords2',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getMyShareholderAccounts = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getShareholderAccounts'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getShareholderAccountStatement = function (req, res) {
console.log(req.body.data);
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getShareholderAccountStatement',
    args:  {criteria: req.body.data}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.DMXPersonalCardbyId = function (req, res) {
  console.log(req.body);
  var options = {
    user: req.user,
    namespace: namespace,
    method: "DMXPersonalCardbyId",
    args: {
      SerialId: req.body.id,
      pinCode: req.body.pinCode
    }
  };

  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.log(err);
    } else {
      res.json(ctx.responseObject.return);
    }
  });
};

exports.PersonalCardbyId = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "PersonalCardbyId",
    args: req.body
  };
  console.log(req.body);
  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.log(err);
    } else {
      res.json(ctx.responseObject.return);
    }
  });
};

exports.getPersonSettings = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: "getPersonSettings"
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};



