'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');

var router = express.Router();

router.post('/upload/', auth.requiresLogin, controller.upload);
router.post('/getTrades/', auth.requiresLogin, controller.getTrades);
router.post('/getRepoTrades/', auth.requiresLogin, controller.getRepoTrades);
router.post('/getOrders/', auth.requiresLogin, controller.getOrders);
router.post('/getSubscriptionInstruments/', auth.requiresLogin, controller.getSubscriptionInstruments);
router.post('/getSubscriptionOrdersBySubscriptionID/', auth.requiresLogin, controller.getSubscriptionOrdersBySubscriptionID);
router.post('/getFreezingRecords/', auth.requiresLogin, controller.getFreezingRecords);
router.post('/getPledgeRecords/', auth.requiresLogin, controller.getPledgeRecords);
router.post('/getPledgedBalances/', auth.requiresLogin, controller.getPledgedBalances);
router.post('/getTitleTransferRecords/', auth.requiresLogin, controller.getTitleTransfers);
router.post('/getGrantRecords/', auth.requiresLogin, controller.getGrants);
router.post('/getOTCTrades/', auth.requiresLogin, controller.getOTCTrades);
router.post('/getTransferPledgedTitles/', auth.requiresLogin, controller.getTransferPledgedTitles);
router.post('/getSuccessions/', auth.requiresLogin, controller.getSuccessions);
router.post('/getMatchedTrades/', auth.requiresLogin, controller.getMatchedTrades);
router.post('/getNonMatchedTrades/', auth.requiresLogin, controller.getNonMatchedTrades);
router.post('/getAuctionRecords/:instrumentId', auth.requiresLogin, controller.getAuctionRecords);
router.post('/getMarketTradeCommission/', auth.requiresLogin, controller.getMarketTradeCommission);
router.post('/getOTCTradeCommission/', auth.requiresLogin, controller.getOTCTradeCommission);
router.post('/getAccountOpeningTradeCommission/', auth.requiresLogin, controller.getAccountOpeningTradeCommission);
router.post('/getPayments/', auth.requiresLogin, controller.getPayments);
router.post('/getIncomingPaymentRecords/', auth.requiresLogin, controller.getIncomingPaymentRecords);
router.post('/azipsTestServices/', auth.requiresLogin, controller.azipsTestServices);
router.post('/getWithdrawalData/', auth.requiresLogin, controller.getWithdrawalData);
router.post('/getPendingWithdrawals/', auth.requiresLogin, controller.getPendingWithdrawals);
router.post('/getBankAccounts/', auth.requiresLogin, controller.getBankAccounts);
router.post('/getOutgoingPaymentRecords/', auth.requiresLogin, controller.getOutgoingPaymentRecords);
router.post('/getAccountCashData/', auth.requiresLogin, controller.getAccountCashData);
router.post('/getAccountRecords/', auth.requiresLogin, controller.getAccountRecords);
router.post('/searchTaxBalances/', auth.requiresLogin, controller.searchTaxBalances);
router.post('/searchClearingMembersAndIssuers/', auth.requiresLogin, controller.searchClearingMembersAndIssuers);
router.post('/getOutgoingRecordsByAccountID/', auth.requiresLogin, controller.getOutgoingRecordsByAccountID);
router.post('/getOutgoingRecordsByAccountAndOperationClass/', auth.requiresLogin, controller.getOutgoingRecordsByAccountAndOperationClass);
router.post('/getTaxExchanges/', auth.requiresLogin, controller.getTaxExchanges);
router.post('/getRecordsByAccountID/', auth.requiresLogin, controller.getRecordsByAccountID);
router.post('/getOtherMemberClientShareholdersByAccountID/', auth.requiresLogin, controller.getOtherMemberClientShareholdersByAccountID);
router.post('/getFreeCashAmount/', auth.requiresLogin, controller.getFreeCashAmount);
router.post('/getCBARDayCloseBalances/', auth.requiresLogin, controller.getCBARDayCloseBalances);
router.post('/getForeignAccountRecords/', auth.requiresLogin, controller.getForeignAccountRecords);
router.post('/getLocalAccountRecords/', auth.requiresLogin, controller.getLocalAccountRecords);
router.post('/getRemainingBalances/', auth.requiresLogin, controller.getRemainingBalances);
router.post('/getCBARCashRecord/', auth.requiresLogin, controller.getCBARCashRecord);
router.post('/getOutgoingRecordsByAccountAndOperationClass/', auth.requiresLogin, controller.getOutgoingRecordsByAccountAndOperationClass);
router.post('/getTAXRecord/', auth.requiresLogin, controller.getTAXRecord);
router.post('/getCouponPaymentRecords2/', auth.requiresLogin, controller.getCouponPaymentRecords2);
router.post("/getPersonSettings", auth.requiresLogin, controller.getPersonSettings);
router.post('/getCouponPaymentRecords/', auth.requiresLogin, controller.getCouponPaymentRecords);
router.post('/getNDCCBARCirculationReport/', auth.requiresLogin, controller.getNDCCBARCirculationReport);
router.get('/getMyShareholderAccounts/', auth.requiresLogin, controller.getMyShareholderAccounts);
router.post('/getShareholderAccountStatement/', auth.requiresLogin, controller.getShareholderAccountStatement);
router.post("/PersonalCardbyId", auth.requiresLogin, controller.PersonalCardbyId);
router.post("/DMXPersonalCardbyId", auth.requiresLogin, controller.DMXPersonalCardbyId);



module.exports = router;
