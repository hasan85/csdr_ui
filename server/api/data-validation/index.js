'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();


router.post('/checkBalance',auth.requiresLogin, controller.checkBalance);
router.post('/validateUnfreezableShares',auth.requiresLogin, controller.validateUnfreezableShares);
router.post('/sendBatchData',auth.requiresLogin, controller.sendBatchData);
router.post('/sendTestData',auth.requiresLogin, controller.sendTestData);
router.post('/sendActionFinish',auth.requiresLogin, controller.sendActionFinish);
router.post('/validateSuccessionOperation',auth.requiresLogin, controller.validateSuccessionOperation);
router.get('/initConfigs',auth.requiresLogin, controller.initConfigs);


module.exports = router;
