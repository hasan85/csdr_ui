'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.validationServices.namespace;
var url = config.servers.validationServices.url;

exports.initConfigs = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'initConfigs'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.checkBalance = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'checkBalance',
    args: req.body.args
  };
  //res.json({
  //  "data": {
  //    "amount": "0",
  //    "free": "true",
  //    "frozen": "false",
  //    "frozenQuantity": "0",
  //    "ID": 184,
  //    "instrument": {
  //      "CFI": {"code": "CFI_D_B_F_T_F_A", "id": 2897},
  //      "ISIN": "AZ2004019397",
  //      "id": 10,
  //      "instrumentName": "Instrument Name",
  //      "issuerName": "koala"
  //    },
  //    "operationBlockAmount": "0",
  //    "operationBlockQuantity": "0",
  //    "orderBlockAmount": "0",
  //    "orderBlockQuantity": 8000,
  //    "pledgeQuantity": "0",
  //    "pledged": "false",
  //    "quantity": 9000,
  //    "repo": "false",
  //    "repoQuantity": "0",
  //    "settlementBlockAmount": "0",
  //    "settlementBlockQuantity": "0",
  //    "tradeBlockAmount": "0",
  //    "tradeBlockQuantity": "0"
  //  },
  //  "resultCode": "0000",
  //  "success": "true",
  //  "total": "0"
  //});
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.validateUnfreezableShares = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'validateUnfreezableShares',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.sendBatchData = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'sendBatchData',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.sendTestData = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'sendTestData',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.sendActionFinish = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'sendActionFinish'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.validateSuccessionOperation = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'validateSuccessionOperation',
    args: {
      operationData: req.body.data
    }
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

