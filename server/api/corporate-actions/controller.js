'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

exports.getCorporateActions = function (req, res) {

  var options = {
    user:req.user,
    namespace: namespace,
    method: 'searchCorporateActions',
    args: req.body.data
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);

  });

};

exports.getCorporateActionById = function (req, res) {

  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getCorporateActionByID',
    args: {id: req.params.id}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);

  });

};

exports.getIssueRegistrations = function (req, res) {

  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getIssueRegistrations',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getCashDividends = function (req, res) {

  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getCashDividends',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getIssuerEquities = function (req, res) {

  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getIssuerEquities',
    args: {issuerId: req.params.issuerId}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getIssueQuantities = function (req, res) {

  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getIssueQuantities',
    args: {corporateActionId: req.params.corporateActionId}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getDecreasedCapitalData = function (req, res) {

  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getDecreasedCapitalData',
    args: {instrumentID: req.params.instrumentID}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.getRemainingPayments = function (req, res) {
console.log(req.body.data);
  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getRemainingPayments',
    args: {instrumentID: req.body.data.instrumentID}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);

  });

};
exports.getIssuerPayments = function (req, res) {
console.log(req.body.data);
  var options = {
    user:req.user,
    namespace: namespace,
    method: 'getIssuerPayments',
    args: {instrumentID: req.body.data.instrumentID}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);

  });

};
exports.sendSubscriptionPayment = function (req, res) {
console.log(req.body.data);
  var options = {
    user:req.user,
    namespace: namespace,
    method: 'sendSubscriptionPayment',
    args: {instrumentID: req.body.data.instrumentID}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);

  });

};


