'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.post('/getCorporateActions', auth.requiresLogin, controller.getCorporateActions);
router.post('/getIssueRegistrations', auth.requiresLogin, controller.getIssueRegistrations);
router.post('/getCashDividends', auth.requiresLogin, controller.getCashDividends);
router.get('/getIssuerEquities/:issuerId', auth.requiresLogin, controller.getIssuerEquities);
router.get('/getIssueQuantities/:corporateActionId', auth.requiresLogin, controller.getIssueQuantities);
router.get('/getDecreasedCapitalData/:instrumentID', auth.requiresLogin, controller.getDecreasedCapitalData);
router.get('/getCorporateActionById/:id', auth.requiresLogin, controller.getCorporateActionById);
router.post('/getRemainingPayments', auth.requiresLogin, controller.getRemainingPayments);
router.post('/getIssuerPayments', auth.requiresLogin, controller.getIssuerPayments);
router.post('/sendSubscriptionPayment', auth.requiresLogin, controller.sendSubscriptionPayment);

module.exports = router;
