'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

exports.testPdf = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'testPdf'
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getAccountStatement = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountStatement',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getHoldingReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getHoldingReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getIssuerHoldingReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getIssuerHoldingReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getAccountStatementDocuments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountStatementDocuments',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getTitleAbstractDocuments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getTitleAbstractDocuments',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getUndeliveredSMSRecords = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getSMSRecords',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getHoldingsReportDocuments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getHoldingsReportDocuments',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getLocalIssuersForeignTradersForBond = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getLocalIssuersForeignTradersForBond',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getLocalIssuersForeignTradersForStock = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getLocalIssuersForeignTradersForStock',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getMonthlyReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMonthlyReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getReportOnClientsForTheEndOfPeriod = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getReportOnClientsForTheEndOfPeriod',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getShareholderCountReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getShareholderCountReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getMemberServicesReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMemberServicesReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getIssuerServicesReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getIssuerServicesReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
exports.getNDCAZIPSServicesReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getNDCAZIPSServicesReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getServiceReportDetail = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getServiceReportDetail',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getServiceReport = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getServiceReport',
    args: req.body.data
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};
