'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.get('/testPdf', auth.requiresLogin, controller.testPdf);
router.post('/getAccountStatement/', auth.requiresLogin, controller.getAccountStatement);
router.post('/getMonthlyReport/', auth.requiresLogin, controller.getMonthlyReport);
router.post('/getReportOnClientsForTheEndOfPeriod/', auth.requiresLogin, controller.getReportOnClientsForTheEndOfPeriod);
router.post('/getHoldingReport/', auth.requiresLogin, controller.getHoldingReport);
router.post('/getIssuerHoldingReport/', auth.requiresLogin, controller.getIssuerHoldingReport);
router.post('/getLocalIssuersForeignTradersForBond/', auth.requiresLogin, controller.getLocalIssuersForeignTradersForBond);
router.post('/getLocalIssuersForeignTradersForStock/', auth.requiresLogin, controller.getLocalIssuersForeignTradersForStock);
router.post('/getAccountStatementDocuments', auth.requiresLogin, controller.getAccountStatementDocuments);
router.post('/getTitleAbstractDocuments', auth.requiresLogin, controller.getTitleAbstractDocuments);
router.post('/getHoldingsReportDocuments', auth.requiresLogin, controller.getHoldingsReportDocuments);
router.post('/getUndeliveredSMSRecords', auth.requiresLogin, controller.getUndeliveredSMSRecords);
router.post('/getShareholderCountReport', auth.requiresLogin, controller.getShareholderCountReport);
router.post('/getMemberServicesReport', auth.requiresLogin, controller.getMemberServicesReport);
router.post('/getIssuerServicesReport', auth.requiresLogin, controller.getIssuerServicesReport);
router.post('/getNDCAZIPSServicesReport', auth.requiresLogin, controller.getNDCAZIPSServicesReport);
router.post('/getServiceReport', auth.requiresLogin, controller.getServiceReport);
router.post('/getServiceReportDetail', auth.requiresLogin, controller.getServiceReportDetail);
module.exports = router;
