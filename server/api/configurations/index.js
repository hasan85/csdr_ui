'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.get('/getPersonConfigurationData/:subjectContextCode/:objectRoleCode/:operationTypeCode',
  auth.requiresLogin, controller.getPersonConfigurationData);


router.get('/getOrganizationStructure',
    auth.requiresLogin, controller.getOrganizationStructure);
router.get('/getAuthorizationByOrgNodeId/:orgNodeId',
    auth.requiresLogin, controller.getAuthorizationByOrgNodeId);

router.get('/getAllAuthorizations',
  auth.requiresLogin, controller.getAllAuthorizations);

router.post('/getMarketValuePriceList', auth.requiresLogin, controller.getMarketValuePriceList);


module.exports = router;
