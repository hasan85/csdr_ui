'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

var url2 = config.servers.bpm.url;

exports.getPersonConfigurationData = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getPersonConfigurationData',
    args: {
      subjectContextCode: req.params.subjectContextCode,
      objectRoleCode: req.params.objectRoleCode,
      operationTypeCode: req.params.operationTypeCode

    }
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getOrganizationStructure = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getOrganizationStructure',
    args: {}
  };
  soap.createClient(url2, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getAuthorizationByOrgNodeId = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAuthorizationByOrgNodeId',
    args: {
      orgNodeId: req.params.orgNodeId
    }
  };
  soap.createClient(url2, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.getAllAuthorizations = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAllAuthorizations'
  };
  soap.createClient(url2, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getMarketValuePriceList = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMarketValuePriceList'
  };
  soap.createClient(url2, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


