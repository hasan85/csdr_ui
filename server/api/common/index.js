'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../auth/authorization');
var router = express.Router();

router.post('/findInstrument', auth.requiresLogin, controller.findInstrument);
router.post('/findBond', auth.requiresLogin, controller.findBond);
router.post('/findStockOrBond', auth.requiresLogin, controller.findStockOrBond);
router.post('/findStock', auth.requiresLogin, controller.findStock);
router.post('/findIssue', auth.requiresLogin, controller.findIssue);
router.post('/findIssuedInstruments', auth.requiresLogin, controller.findIssuedInstruments);
router.get('/getAccountInfo/:number', auth.requiresLogin, controller.getAccountInfo);
router.get('/getAccountPrescription/:depoAccountID/:parentID', auth.requiresLogin, controller.getAccountPrescription);
router.post('/generateAccountOpeningDocuments', auth.requiresLogin, controller.generateAccountOpeningDocuments);
router.post('/getAccountConsolidations', auth.requiresLogin, controller.getAccountConsolidations);
router.post('/getDataChangedInstruments', auth.requiresLogin, controller.getDataChangedInstruments);
router.post('/getInstrumentDataChangeHistory', auth.requiresLogin, controller.getInstrumentDataChangeHistory);
router.post('/getGlobusTransactions', auth.requiresLogin, controller.getGlobusTransactions);
router.post('/getGlobusGlobalOperations', auth.requiresLogin, controller.getGlobusGlobalOperations);
router.post('/getMTMessages', auth.requiresLogin, controller.getMTMessages);
router.post('/searchTaxExemptAccounts', auth.requiresLogin, controller.searchTaxExemptAccounts);
router.get('/getIncompletedOperationById/:id', auth.requiresLogin, controller.getIncompletedOperationById);
router.post('/searchASBGenerator', auth.requiresLogin, controller.searchASBGenerator);
module.exports = router;
