'use strict';

var _ = require('lodash');

var request = require('request');
var config = require('../../config/environment');

var soap = require("../../lib/soap-wrapper");

var namespace = config.servers.dataSearch.namespace;
var url = config.servers.dataSearch.url;

exports.searchTaxExemptAccounts = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchTaxExemptAccounts',
    args: { criteria: req.body.data }
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err) console.info(err);
    else res.json(ctx.responseObject.return)
  });
};

exports.searchASBGenerator = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchASBGenerator',
    args: {criteria: req.body}
  };
  console.log(req.body);
  soap.createClient(url, options, function (err, ctx) {
    if (err) console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};

exports.findInstrument = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchActiveSecurity',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.findBond = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchBond',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {


    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.findStock = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchStock',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {


    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.findStockOrBond = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchStockOrBond',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.findIssue = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'findIssue',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};

exports.findIssuedInstruments = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'searchIssuedInstruments',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAccountInfo = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountInfo',
    args: {number: req.params.number}
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.getAccountPrescription = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getAccountPrescription',
    args: {
      depoAccountID: req.params.depoAccountID,
      parentID: req.params.parentID
    }
  };

  soap.createClient(url, options, function (err, ctx) {

    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });

};
exports.generateAccountOpeningDocuments = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: 'generateAccountOpeningDocuments',
        args: {personData: req.body}
    };
    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else
            res.json(ctx.responseObject.return);
    });
};

exports.getAccountConsolidations = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: 'getAccountConsolidations',
        args: {criteria: req.body.data}
    };

    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else
            res.json(ctx.responseObject.return);
    });
};

exports.getDataChangedInstruments = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: "getDataChangedInstruments",
        args: {criteria: req.body.data}
    };
    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else
            res.json(ctx.responseObject.return);
    });
};

exports.getInstrumentDataChangeHistory = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: 'getInstrumentDataChangeHistory',
        args: {
            instrumentID: req.body.instrumentID
        }
    };

    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else
            res.json(ctx.responseObject.return);
    });
};

exports.getGlobusTransactions = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: 'getGlobusTransactions',
        args: {criteria: req.body.data}
    };
    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else
            res.json(ctx.responseObject.return);
    });
};

exports.getGlobusGlobalOperations = function (req, res) {
    var options = {
        user: req.user,
        namespace: namespace,
        method: 'getGlobusGlobalOperations',
        args: {criteria: req.body.data}
    };
    soap.createClient(url, options, function (err, ctx) {
        if (err)
            console.info(err);
        else
            res.json(ctx.responseObject.return);
    });
};
exports.getMTMessages = function (req, res) {
  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getMTMessages',
    args: {criteria: req.body.data}
  };
  soap.createClient(url, options, function (err, ctx) {
    if (err)
      console.info(err);
    else
      res.json(ctx.responseObject.return);
  });
};


exports.getIncompletedOperationById = function (req, res) {

  var options = {
    user: req.user,
    namespace: namespace,
    method: 'getIncompletedOperation',
    args: {operationID: req.params.id}
  };

  console.log(options);

  soap.createClient(url, options, function (err, ctx) {
    if (err) {
      console.info(err);
    }
    else {
      res.json(ctx.responseObject.return);
    }
  });
};
