/**
 * Socket.io configuration
 */

'use strict';

var config = require('./environment');
var session = require("express-session");
var passport = require('passport');

// When the user connects.. perform this
function onConnect(sockets, socket) {

  // When the client emits 'info', this listens and executes
  socket.on('info', function (data) {
    console.info('[%s] %s', socket.address, JSON.stringify(data, null, 2));
  });

  console.log('-----------------', socket.id);
  // Insert sockets below
  //require('../api/asan-imza/main.socket').register(sockets, socket);
}

// When the user disconnects.. perform this
function onDisconnect(socket) {
}

module.exports = function (socketio) {

  socketio.on('connection', function (socket) {

    socket.address = socket.handshake.address !== null ?
    socket.handshake.address.address + ':' + socket.handshake.address.port :
      process.env.DOMAIN;
    socket.connectedAt = new Date();

    // Call onDisconnect.
    socket.on('disconnect', function () {
      onDisconnect(socket);
    });

    // Call onConnect.
    onConnect(socketio.sockets.connected, socket);

  });
};
