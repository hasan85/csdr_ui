'use strict';

var LocalStartegy = require('passport-local').Strategy;
var openam = require('../lib/openam/');

var useOpenAM = config.servers.authentication.useOpenAM;

module.exports = function () {

  return new LocalStartegy(

    function (username, password, cb) {
        openam.authenticateUser(username, password, function (err, user) {
          if (err) {
            return cb(err);
          }
          if (!user) {
            return cb(null, false);
          }
          return cb(null, user);
        });
    });
};
