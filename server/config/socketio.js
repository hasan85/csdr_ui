/**
 * Socket.io configuration
 */

'use strict';

var config = require('./environment');

var clients = {};

function onConnect(sockets, socket, socketio) {
  var userId = socket.decoded_token.userId;

  if (clients.hasOwnProperty(userId)) {

    clients[userId]['sockets'].push(socket.id);
  }
  else {
    clients[userId] = {
      sockets: [socket.id]
    }
  }
  socket.on('info', function (data) {
    console.info('[%s] %s', socket.handshake.address, JSON.stringify(data, null, 4));
  });

  socket.on("join", function (data) {
    socket.join(data.userID);
    socketio.sockets.adapter.rooms[data.userID].orgNodes = data.orgNodes;
    console.log("user joined to room", data.userID);
  });
}

function onDisconnect(socket) {
  var userId = socket.decoded_token.userId;
  var index = clients[userId]['sockets'].indexOf(socket.id);
  if (index != -1) {
    clients[userId]['sockets'].splice(index, 1);
  }
  if (clients[userId]['sockets'].length == 0) {
    delete clients[userId];
  }
}

module.exports = function (socketio) {

  socketio.use(require('socketio-jwt').authorize({
    secret: config.secrets.jwt,
    handshake: true
  }));

  socketio.on('connection', function (socket) {

    socket.on('disconnect', function () {
      onDisconnect(socket);
    });

    onConnect(socketio.sockets.connected, socket, socketio);

  });
};
