'use strict';

var SamlStrategy = require('passport-saml').Strategy;

module.exports = function () {

  return new SamlStrategy(
    {
      entryPoint: "http://ndcdr001s:8081/OpenAM-12.0.0/saml2/jsp/idpSSOInit.jsp?"
      + "metaAlias=/idp"
      + "&spEntityID=http://ndcui.local:9000/metadata/",

      callbackUrl: 'http://ndcui.local:9000/login/callback/',

      logoutUrl: "http://ndcdr001s:8081/OpenAM-12.0.0/saml2/jsp/idpSingleLogoutInit.jsp?"
      + "binding=urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
      + "&RelayState=http://ndcui.local:9000/saml/logout/callback",

      issuer: 'http://ndcui.local:9000/'
    },
    function (profile, done) {

      //console.info(profile.getAssertionXml());
     console.info(profile);
      return done(null,
        {
          id: profile.id,
          email: profile.email,
         // displayName: profile.cn,
        //  firstName: profile.givenName,
         // lastName: profile.sn,
          sessionIndex: profile.sessionIndex,
          saml: {
            nameID: profile.nameID,
            nameIDFormat: profile.nameIDFormat,
            token:profile.getAssertionXml()
          }
        });
    });
}
