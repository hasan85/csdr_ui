'use strict';

// Production specific configuration
// =================================
// http://192.168.31.13:8091/dataSearchServices?wsdl
// var webServicesUrl = "http://cds.cmis.az:8091/";
var webServicesUrl = "http://192.168.31.13:8091/";
module.exports = {
  // Server IP
  ip: '192.168.31.13',
  // Server port
  port: 9080,
  portHttps: 9443,
  thriftPort: 9090,
  // currentServerHost: "https://192.168.21.111:9443/",
  // currentServerHost: "http://192.168.31.13:9080/",
  currentServerHost: "http://localhost:9080/",
  servers: {
    authentication: {
      //url: "http://ndcdr001s:8081/OpenAM-12.0.0/",
      // url: "http://openam.gazcmccmd1.local:8080/OpenAM-12.0.0/",
     url: "http://openam.cmis.az:8080/OpenAM-12.0.0/",
     //  url: "http://ndcdr001s:8081/OpenAM-12.0.0/",
      useOpenAM: true
    },
    bpm: {
      url: webServicesUrl + "BPMServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    referenceData: {
      url: webServicesUrl + "referenceDataServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    dataSearch: {
      url: webServicesUrl + "dataSearchServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    validationServices: {
      url: webServicesUrl + "validationServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    reportsDataServices: {
      url: webServicesUrl + "reportsDataServices?wsdl",
      namespace: "http://uiservices.mdm.az/"
    }
  },
  expressStaticFilesPath: "public"
};
