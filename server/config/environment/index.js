'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  fileSizeLimit:5242880,
  // Root path of server
  root: path.normalize(__dirname + '/../../..'),
  temp: path.normalize(__dirname + '/../../../uploads/tmp'),

  // Server port
  port: process.env.PORT || 9000,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'qaw9d8qwdas9udas-0d9asjadja90hasdasdh',
    jwt: 'as90qw98qhasihas98ashuasjhbas98asasdgasd8'
  },
  cookie: {
    expires: 10 * 3600 * 1000,
  },

  servers: {
    authentication: {
      url: "http://openam.gazcmccmd1.local:8080/OpenAM-12.0.0/",
      useOpenAM :true
    },
    bpm: {
      url: "http://localhost:8091/BPMServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    referenceData: {
      url: "http://localhost:8091/referenceDataServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    dataSearch: {
      url: "http://localhost:8091/dataSearchServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    validationServices: {
      url: "http://localhost:8091/validationServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    reportsDataServices: {
      url: "http://localhost:8091/reportsDataServices?wsdl",
      namespace: 'http://uiservices.mdm.az/'
    }
  }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
