'use strict';

// Development specific configuration
// ==================================
var ip = '192.168.31.13';
//var webservicesUrl = 'http://openam.cmis.az:8091/';
//var webservicesUrl = 'http://192.168.150.10:8091/';
//var webservicesUrl = 'http://192.168.150.104:8091/';
// var webservicesUrl = 'http://192.168.1.111:8091/';
//var webservicesUrl = 'http://localhost:8091/';
//test
// var webservicesUrl = "http://192.168.250.84:8091/";
var webservicesUrl = 'http://' + ip + ':8091/';
// var webservicesUrl = 'http://dev.cmis.az:8091/';
//development
//var webservicesUrl = 'http://172.16.43.13:8091/';
// var webservicesUrl = 'http://localhost:8091/';



module.exports = {
  port: 9000,
  currentServerHost: "http://localhost:9000/",
  portHttps: 9443,
  expressStaticFilesPath: "client",
  thriftPort: 9090,
  ip: ip,
  servers: {
    authentication: {
      //url: "http://ndcdr001s:8081/OpenAM-12.0.0/",
      url: "http://openam.gazcmccmd1.local:8080/OpenAM-12.0.0/",
      // url: 'http://openam.cmis.az:8080/OpenAM-12.0.0/',
      // url: "http://openam.cmis.az:8080/OpenAM-12.0.0/ ",
      //  url: "http://ndcdr001s:8081/OpenAM-12.0.0/",
      useOpenAM: true
    },
    bpm: {
      url: webservicesUrl + "BPMServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    referenceData: {
      url: webservicesUrl + "referenceDataServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    dataSearch: {
      url: webservicesUrl + "dataSearchServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    validationServices: {
      url: webservicesUrl + "validationServices?wsdl",
      namespace: 'http://services.uiservices.simberg.org/'
    },
    reportsDataServices: {
      url: webservicesUrl + "reportsDataServices?wsdl",
      namespace: "http://uiservices.mdm.az/"
    }
  }
};

// http://192.168.31.13:8091/BPMServices?wsdl
// http://192.168.31.13:8091/referenceDataServices?wsdl
// http://192.168.31.13:8091/dataSearchServices?wsdl
// http://192.168.31.13:8091/validationServices?wsdl
