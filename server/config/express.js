/**
 * Express configuration
 */

'use strict';

var express = require('express');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var config = require('./environment');
var passport = require("passport");
var session = require("express-session");
var i18n = require('i18n-2');
var expressValidator = require('express-validator');
var FileStore = require('session-file-store')(session);
require('./passport')(passport);
var timeout = require('connect-timeout');
var uuid = require('uuid');

module.exports = function (app) {
  var env = app.get('env');
  app.set('views', config.root + '/server/views');
  // app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'ejs');
  app.use(timeout('600000s'));
  app.use(compression());
  app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
 // app.use(haltOnTimedout);

  app.use(bodyParser.json({limit: '50mb'}));

  app.use(expressValidator());
  app.use(methodOverride());
  app.use(cookieParser(config.secrets.session));
  //app.use(haltOnTimedout);


  app.use(session(
    {
      genid: function (req) {
        return uuid.v4() // use UUIDs for session IDs
      },
      key: 'express.sid',
      secret: config.secrets.session,
      cookie: {httpOnly: true, maxAge: 86400000}, // time im ms
      resave: false,
      saveUninitialized: false,
      httpOnly: true,
      secure: true,
      ephemeral: true,
      store: new FileStore({
        useAsync: true,
        path: config.root + "/sessions",
        reapInterval: 5000,
        retries: 10,
        logFn: function() {}
      })
    }));

  app.use(passport.initialize());
  app.use(passport.session());

  if ('production' === env) {
    app.use(favicon(path.join(config.root, 'public', 'favicon.ico')));

    // Check if user is authenticated then serve static files
    // app.use(express.static(path.join(config.root, 'public')));
    app.set('appPath', config.root + '/public');
    app.use(morgan('dev'));
  }

  if ('development' === env || 'test' === env) {
    //app.use(require('connect-livereload')());
    app.use(express.static(path.join(config.root, '.tmp')));

    // Check if user is authenticated then serve static files
    //app.use(express.static(path.join(config.root, 'client')));
    app.set('appPath', 'client');
    app.use(morgan('dev'));
    app.use(errorHandler()); // Error handler - has to be last


  }

  // I18n support
  i18n.expressBind(app, {
    locales: ['az', 'en'],
    cookieName: 'locale',
    extension: '.json',
    directory:config.root + "/locales",
  });

  app.use(function (req, res, next) {

    req.i18n.setLocaleFromCookie();
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.header('X-Frame-Options', 'SAMEORIGIN');

    if (req.url.match(/^\/(css|js|img|font)\/.+/)) {
      res.setHeader('Cache-Control', 'public, max-age=3600');
    }

    next();

  });

  app.use(function (err, req, res, next) {

    console.dir(err.stack);

    res.json({
      success: 'false',
      message: 'Internal Server error (UI)'
    });
    // console.log(err);
    // console.dir((err));
    // console.error(err.message);
    //res.status(500);
    // res.json();
  });

  //function haltOnTimedout(req, res, next){
  //  next();
  //}
};
