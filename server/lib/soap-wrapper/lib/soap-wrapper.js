"use strict";

var ws = require('../../ws.js'),
  Http = ws.Http;
var WSDL = require('./wsdl');

// Create soap request
function _createRequest(options) {

  var argsStr = '', methodStr = '';


  if (options.args) {

    var xml2js = require('xml2js');

    var builder = new xml2js.Builder({headless: true, explicitRoot: false, strict: false});
    var argsStr = builder.buildObject(options.args);
    var argsStr = argsStr.replace("<root>", "");
    var argsStr = argsStr.replace("</root>", "");

    methodStr = '<tns:' + options.method + '>' + argsStr + '</tns:' + options.method + '>';
  } else {
    methodStr = '<tns:' + options.method + '/>';
  }

  var request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="' + options.namespace + '">'
    + '<soapenv:Header>'
    + '</soapenv:Header>'
    + ' <soapenv:Body>'
    + methodStr
    + ' </soapenv:Body>'
    + '</soapenv:Envelope>';


  return request;

}

// Create soap client
function createClient(url, options, callback) {

  if (!url) {
    throw new Error("Url is required!");
  }
  if (!options.method || !options.namespace) {
    throw new Error("Method and namespace options are required!");
  }

  var request = _createRequest(options);

  var ctx = {
    request: request,
    url: url,
    contentType: "text/xml",
    userToken: options.user.tokenId,
    isAsanImzaLogin: options.user.isAsanImzaLogin ? true : false,
    phoneNumber: options.user.phoneNumber,
    asanUserId: options.user.asanUserId
  };

  var handlers = [new Http()];

  ws.send(handlers, ctx, function (ctx) {
    if (ctx.response) {

      WSDL.xmlToObject(ctx.response, options.method, function (err, result) {
        ctx.responseObject = result;
        if (err) {
          ctx.responseObject = {
            return: {
              success: "false",
              message: "Internal Server Error (Soap Fault)"
            }
          };
          console.error(err);
          err = false;
        }
        callback(err, ctx);
      });
    }
    else {

      ctx.responseObject = {
        return: {
          success: "false",
          message: "Web Services Not Working"
        }
      };

      return callback(false, ctx);
    }

  });
}

exports.createClient = createClient;
