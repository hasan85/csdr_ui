"use strict";
var Int64 = require("node-int64");

Int64.prototype.toJSON = function () {
  return this.toString()
};

var thrift = require("thrift");
var config = require("../../../config/environment");
var SystemTypes = require("../../../api/no-api-thrift-files/gen-nodejs/commondata_types");


function createConnection() {
  var options = {
    transport: require("../../../../node_modules/thrift/lib/nodejs/lib/thrift/framed_transport"), // thrift.TFramedTransport() not working
    protocol: require("../../../../node_modules/thrift/lib/nodejs/lib/thrift/binary_protocol")
  };
  var ip = config.ip;

  // var ip = "10.20.11.112";
  var port = config.thriftPort;

  var connection = thrift.createConnection(ip, port, options);

  return connection;
}

function fixArgs(options) {
  var args = [];
  options.user.userToken = options.user.tokenId;
  args.push(new SystemTypes.TokenLoginInfo(options.user));
  for (var k in options.args) {
    if (options.args.hasOwnProperty(k)) {
      args.push(options.args[k]);
    }
  }
  return args;
}

module.exports.createClient = function (options, cb) {
  var connection = createConnection();
  var args = fixArgs({
    user: options.user,
    args: options.args
  });


  function clientHandler(err, result) {
    console.log();
    if (result) {
      var ctx = {
        responseObject: JSON.parse(result)
      };
      if (err) {
        ctx.responseObject = {
          exceptionModel: {
            message: "Web service not working",
            resultCode: "9999"
          }
        };
        err = false;
      }
      cb(err, ctx);
    } else {
      var ctx = {
        responseObject: {
          exceptionModel: {
            resultCode: "9999",
            message: "Web service not working."
          }
        }
      };
      cb(false, ctx);

    }

    connection.end();

  }

  function onConnected() {
    console.log("Connected..");
    var client = thrift.createClient(options.service, connection);
    try {
      client[options.method].apply(client, args);
    } catch (err) {
      console.log(err);
      cb(false, {
        responseObject: {
            resultCode: "9999",
            message: "Web service not working."
          }
      });

      connection.end();

    }
  }


  args.push(clientHandler);
  connection.on("error", function (err) {
    cb(false, {
      responseObject: {
        exceptionModel: err
      }
    });
    console.log(err);
  });
  connection.on("connect", onConnected);
};
