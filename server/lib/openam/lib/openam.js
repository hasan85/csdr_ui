request = require('request');
config = require('../../../config/environment');

var validateToken = function (tokenId, cb) {
  request({
    headers: {
      'Content-Type': 'application/json',
    },
    url: config.servers.authentication.url + 'json/sessions/' + tokenId + "?_action=validate",
    method: 'POST',
    json: true

  }, function (error, response, body) {

    if (error) {
      console.log(error);
      cb(error, null);
    } else {

      if (body.valid == true) {
        cb(null, body.uid)
      } else {
        cb(new Error('Invalid token'), null);
      }
    }

  });

};

exports.authenticateUser = function (username, password, cb) {
  console.log("username", username);
  console.log("password", password);
  process.nextTick(function () {
    request({
      headers: {
        'Content-Type': 'application/json',
        'X-OpenAM-Username': username,
        'X-OpenAM-Password': password,
        'Accept-API-Version': 'resource=2.0, protocol=1.0'
      },
      url: config.servers.authentication.url + 'json/authenticate',
      method: 'POST',
      json: true

    }, function (error, response, body) {
      console.log("OPENAM_USER", body);
      if (body) {
        if (body.code && body.code == 401) {
          cb(body.message);
        } else if (body.tokenId) {
          console.log("-----",body.tokenId);
          validateToken(body.tokenId, function (err, userId) {
            if (err)
              cb(err, null);
            else
              cb(null, {
                'id': userId,
                'tokenId': body.tokenId
              });
          })
        }
      }

    });

  });
};

exports.asanImzaLogin = function (username, password, cb) {
  console.log("ASAN_IMZA_LOGIN_REQUEST_SENDING_TO_OPENAM");
  request({
    headers: {
      'Content-Type': 'application/json',
      'X-OpenAM-Username': username,
      'X-OpenAM-Password': password,
      'Accept-API-Version': 'resource=2.0, protocol=1.0'
    },
    url: config.servers.authentication.url + 'json/authenticate?realm=/test&module=asanAuth&authIndexType=MODULE&authIndexValue=asanAuth',
    method: 'POST',
    json: true

  }, function (error, response, body) {
    console.log("OPENAM_ASAN_IMZA_LOGIN_RESPONSE RETURNED", "error", error, "body", body);
    if (body) {
      if (body.code && body.code == 401) {
        cb(body.message);
      } else if (body.tokenId) {

        validateToken(body.tokenId, function (err, userId) {
          if (err)
            cb(err, null);
          else
            cb(null, {
              'id': userId,
              'tokenId': body.tokenId
            });
        })
      }
    }

  });
};


exports.logout = function (tokenId) {

  request({
    headers: {
      'Content-Type': 'application/json',
      'iplanetDirectoryPro': tokenId
    },
    url: config.servers.authentication.url + 'json/sessions/?_action=logout',
    method: 'POST',
    json: true

  }, function (error, response, body) {
    if (body && body.code && body.code == 401) {
      console.log(body);
    }
  });

};

exports.forgotPassword = function (username, cb) {

  var subject = "Reset your forgotten password.",
    message = "Follow this link to reset your password";

  request({
    headers: {
      'Content-Type': 'application/json',
    },
    url: config.servers.authentication.url + 'json/users/?_action=forgotPassword',
    method: 'POST',
    body: {
      'username': username,
      'subject': subject,
      'message': message
    },
    json: true

  }, function (error, response, body) {

    if (error) {
      cb(null, true)
    } else {
      cb(body, null);
    }

  });

};

exports.changePassword = function (user, currentPassword, newPassword, cb) {

  console.log(arguments);
  request({
    headers: {
      'Content-Type': 'application/json',
      'iplanetDirectoryPro': user.tokenId
    },
    url: config.servers.authentication.url + 'json/users/' + user.id + '?_action=changePassword',
    method: 'POST',
    body: {
      "currentpassword": currentPassword,
      "userpassword": newPassword
    },
    json: true

  }, function (error, response, body) {
    console.log(body);
    if (body.code) {
      cb(body, null);
    } else {
      cb(null, true);
    }
  });
};

exports.forgotPasswordReset = function (data, cb) {

  request({
    headers: {
      'Content-Type': 'application/json',
    },
    url: config.servers.authentication.url + 'json/users/?_action=forgotPasswordReset',
    method: 'POST',
    body: data,
    json: true

  }, function (error, response, body) {

    if (body.code)
      cb(body, null);
    else
      cb(null, true);

  });
};

exports.confirm = function (data, cb) {
  request({
    headers: {
      'Content-Type': 'application/json',
    },
    url: config.servers.authentication.url + 'json/users/?_action=confirm',
    method: 'POST',
    body: data,
    json: true

  }, function (error, response, body) {

    if (body.code)
      cb(body, null);
    else
      cb(null, body);

  });
};

exports.validateToken = validateToken;
