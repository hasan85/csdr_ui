'use strict';

/**
 * Generic require login routing middleware
 */
exports.requiresLogin = function (req, res, next) {
  if (!req.isAuthenticated()) {
    var isAjaxRequest = req.xhr;
    if (isAjaxRequest) {
      res.json({
        success: "false",
        message: "Your session expired. You have to logout and login again"
      });
    } else {
      return res.redirect('/index');
    }
  }
  next();
};
