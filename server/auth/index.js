var express = require('express');
var passport = require("passport");

var router = express.Router();
var auth = require('./authorization');
var openam = require('../lib/openam');

var useOpenAM = config.servers.authentication.useOpenAM;


var path = require("path");
var fs = require("fs");

module.exports = function (app) {
  // 89.147.200.182
  app.get('/index', function (req, res) {
    if (req.isAuthenticated()) {
      res.redirect('/');
    }
    else {
      res.render('index', {
        errors: {},
        form: {
          username: '',
          password: ''
        }
      });
    }
  });

  app.post('/index', function (req, res, next) {
    req.checkBody('username', req.i18n.__('Username is required')).notEmpty();
    req.checkBody('password', req.i18n.__('Password is required')).notEmpty();

    req.sanitizeBody('username').trim();
    req.sanitizeBody('password').trim();

    var form = {
      username: req.body.username,
      password: req.body.password
    };
    console.log(req.body);
    //validate
    var errors = req.validationErrors(true);
    if (req.body.username.length && req.body.password.length) {
      errors = false;
    }
    if (errors) {
      res.render('index', {errors: errors, form: form});
    }
    else {
      passport.authenticate('local', function (err, user, info) {
        console.log("44", user, err);
        if (err || !user) {
          return res.render('index', {
            errors: {server: {msg: req.i18n.__("Invalid username or password")}}, form: form
          });
        } else {
          req.logIn(user, function () {
            res.redirect('/');
          })
        }
      })(req, res, next);
    }
  });

  app.post('/asanImzaLoginXHR', function (req, res, next) {
    console.time("ASAN_IMZA_LOGIN_REQUEST");
    console.log("ASAN_IMZA_LOGIN_REQUEST_CALLED_BY_CLIENT");
    console.log("ASAN_IMZA_LOGIN_REQUEST_INFO", req.hostname, req.ip, req.ips, req.xhr);

    var phoneNumber = req.body.phoneNumber;
    phoneNumber = phoneNumber.replace(/\s/g, "").replace("(", '').replace(')', '');
    var callbackUrl = req.protocol + "://" + req.get("host") + "/asan-imza/sendVerificationCode?socketId=" + req.body.socketId;
    var username = "url=" + callbackUrl + ";phone=" + phoneNumber;
    openam.asanImzaLogin(username, req.body.userId, function (err, user) {


      console.timeEnd("ASAN_IMZA_LOGIN_REQUEST");

      console.log("ASAN_IMZA_LOGIN_RESPONSE_ERROR", err, !!err, !user);
      if (err || !user) {
        console.log("SENDING_ASAN_IMZA_LOGIN_RESPONSE_ERROR_TO_CLIENT", err);
        res.json({
          success: false,
          errorCode: "500",
          message: err
        });
      } else {
        user.isAsanImzaLogin = true;
        user.phoneNumber = phoneNumber;
        user.asanUserId = req.body.userId;
        req.logIn(user, function () {
          res.json({success: true});
        });
      }
    });

  });

  app.get('/logout', auth.requiresLogin, function (req, res) {


    if (useOpenAM) {
      openam.logout(req.user.tokenId);
    }
    req.logout();
    req.session.destroy();
    res.redirect('/index');

  });

  app.get('/check', auth.requiresLogin, function (req, res) {
    res.json('authenticated');
  });

  app.get('/changePassword', auth.requiresLogin, function (req, res) {

    //if(useOpenAM) {
    //  openam.changePassword(req.user, 'changeit', 'test', function (err, succ) {
    //    if (succ) {
    //      res.redirect('/logout');
    //    } else {
    //      res.json(err);
    //    }
    //  });
    //} else{
    //  res.redirect('/logout');
    //}

  });

  app.get('/confirm', function (req, res) {

    var isValidToken = false;

    req.checkQuery('confirmationId').notEmpty();
    req.checkQuery('tokenId').notEmpty();
    req.checkQuery('username').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
      res.redirect('/index');
    } else {
      var token = {
        confirmationId: req.query.confirmationId.replace(/ /g, '+'),
        tokenId: req.query.tokenId.replace(/ /g, '+'),
        username: req.query.username.replace(/ /g, '+')
      };
      openam.confirm(token, function (err, token) {
        if (err) {
          isValidToken = false;
        } else {

          isValidToken = true;
        }
        res.render('confirm', {
          token: token,
          errors: null,
          isValidToken: isValidToken,
          form: {password: '', password2: ''},
          success: false
        });

      })
    }
  });

  app.post('/confirm', function (req, res) {

    req.sanitizeBody('username').trim();
    req.sanitizeBody('password').trim();

    req.checkBody('password', req.i18n.__('Password is required')).notEmpty();
    req.checkBody('password2', req.i18n.__('Password confirmation is required')).notEmpty();
    req.checkBody('password2', req.i18n.__('Passwords do not match')).equals(req.body.password);
    req.checkBody('password', req.i18n.__('Password must be between 8 and 25 character!')).len(8, 25);


    var errors = req.validationErrors(true);
    var success = false;

    var token = {
      confirmationId: req.body.confirmationId,
      tokenId: req.body.tokenId,
      username: req.body.username,
    };

    var form = {
      password: req.body.password ? req.body.password : '',
      password2: req.body.password2 ? req.body.password2 : ''
    }

    if (errors) {
      success = false;
      res.render('confirm', {
        isValidToken: true,
        errors: errors,
        form: form,
        token: token,
        success: success
      });
    }
    else {

      var requestData = token;
      requestData.userpassword = form.password;

      openam.forgotPasswordReset(requestData, function (err, succ) {


        if (err) {
          errors = {
            password: {
              msg: req.i18n.__(err.message)
            }
          }

        } else {
          success = true;

        }
        res.render('confirm', {
          isValidToken: true,
          errors: errors,
          form: form,
          token: form,
          success: success
        });

      });


    }

  });

  app.get('/forgotPassword', function (req, res) {

    res.render('forgotPassword', {errors: {}, success: false});

  });

  app.post('/forgotPassword', function (req, res) {

    req.checkBody('username', req.i18n.__('Username is required')).notEmpty();
    req.sanitizeBody('username').trim();
    var errors = req.validationErrors(true);
    if (errors) {
      res.render('forgotPassword', {errors: errors, success: false})
    }
    else {
      openam.forgotPassword(req.body.username, function (err, succ) {
        var success = false;
        if (err && err.code) {
          if (err.code == 404) {
            errors = {
              username: {msg: req.i18n.__('User name not found')}
            };
          } else if (err.code == 400) {
            errors = {
              username: {msg: req.i18n.__('No email provided in profile')}
            };
          } else {
            errors = {
              username: {msg: err.message}
            };
          }
        }
        else {
          success = true;
        }
        res.render('forgotPassword', {errors: errors, success: success})
      });
    }
  });

  app.get('/ln', function (req, res) {
    if (req.query.lang == 'en' || req.query.lang == 'az') {
      res.cookie('locale', req.query.lang);
      req.i18n.setLocaleFromCookie();
    }
    var backURL = req.header('Referer') || '/index';
    res.redirect(backURL);
  });

};
