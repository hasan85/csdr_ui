var express = require('express');
var passport = require("passport");

var router = express.Router();


module.exports = function (app, socketio) {
  app.get('/asan-imza/sendVerificationCode', function (req, res) {

      console.log("sendVerificationCode called", new Date());
      console.log("sendVerificationCode Request params", req.query, socketio.sockets.connected[req.query.socketId].status);

      if (socketio && socketio.sockets.connected && socketio.sockets.connected[req.query.socketId]) {
        if(socketio.sockets.connected[req.query.socketId].status != 'ver_code_accepted') {
          socketio.sockets.connected[req.query.socketId].status = 'ver_code_accepted';
          socketio.sockets.connected[req.query.socketId].emit('sendAsanImzaVerificationCode', {
            socketId: req.query.socketId,
            verificationCode: req.query.verCode
          });
          res.json({
            success: "true"
          });
        } else {
          res.json({
            success: false,
            message: "Already received ver code"
          });
        }
      }
      else {
        res.json({
          success: false,
          message: "Socket not found!"
        });
      }
    }
  );
};
