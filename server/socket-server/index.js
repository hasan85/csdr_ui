"use strict";

var express = require( 'express' );
var passport = require( "passport" );

function isInArray( arr, check ) {
  for ( var i = 0; i < check.length; i++ ) {
    if ( arr.indexOf( check[ i ] ) > -1 ) {
      return true;
    }
  }
}


module.exports = function ( app, socketio ) {


  app.get( '/testSocket', function ( req, res ) {
    socketio.emit( 'UPDATE_AZIPS_DASHBOARD', {
      test: 1
    } );
  } );

  app.post( '/http-listener', function ( req, res ) {
    alert( req.body.type )
    switch ( req.body.type ) {
      case "INCOMING_TASK":
        {
          let rooms = socketio.sockets.adapter.rooms;
          for ( let roomName in rooms ) {
            if ( rooms.hasOwnProperty( roomName ) ) {
              let room = rooms[ roomName ];

              if ( room.orgNodes && req.body.orgNodes ) {
                if ( isInArray( room.orgNodes, req.body.orgNodes ) ) {
                  socketio.to( roomName ).emit( "INCOMING_TASK", req.body );
                }
              }

            }
          }
          break;
        }
      case "CLAIMED_TASK":
        {
          let rooms = socketio.sockets.adapter.rooms;
          for ( let roomName in rooms ) {
            if ( rooms.hasOwnProperty( roomName ) ) {
              let room = rooms[ roomName ];

              if ( room.orgNodes && req.body.orgNodes ) {
                if ( isInArray( room.orgNodes, req.body.orgNodes ) ) {
                  socketio.to( roomName ).emit( "CLAIMED_TASK", req.body );
                }
              }

            }
          }
          break;
        }
      case "UPDATE_AZIPS_DASHBOARD":
        {
          let rooms = socketio.sockets.adapter.rooms;
          for ( let roomName in rooms ) {
            if ( rooms.hasOwnProperty( roomName ) ) {
              let room = rooms[ roomName ];

              if ( room.orgNodes && req.body.orgNodes ) {
                if ( isInArray( room.orgNodes, req.body.orgNodes ) ) {
                  socketio.to( roomName ).emit( "UPDATE_AZIPS_DASHBOARD", req.body );
                }
              }

            }
          }
          break;
        }
      default:
        {
          break;
        }
    }
    process.nextTick( function () {
      res.end();
    } );
  } );

};
