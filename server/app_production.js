/**
 * Main application file
 */

'use strict';

// Set default node environment to development
//process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.NODE_ENV = 'production';

var express = require('express');
var https = require('https');
var http = require('http');
var fs = require('fs');

var config = require('./config/environment');

// Setup server
var app = express();


// Comment on development [[
var httpApp = express();
httpApp.set('port', config.port);
//httpApp.get("*", function (req, res, next) {
// res.redirect("https://" + req.headers.host + "/" + req.path);
//});

var credentials = {
  key: fs.readFileSync(config.root+'/csdr_key.pem'),
  cert: fs.readFileSync(config.root+'/csdr_cert.pem')
};

var httpsServer = https.createServer(credentials,app);

var socketio = require('socket.io')(httpsServer, {
  serveClient: (config.env !== 'production'),
  path: '/socket.io-client'
});

require('./config/socketio')(socketio);
require('./config/express')(app);
require('./routes')(app, socketio);

// Comment on development [[

httpsServer.listen(config.portHttps,config.ip, function() {
  console.log('Express HTTPS server listening on port ' + config.portHttps);
});
//httpServer.listen(httpApp.get('port'),config.ip, function() {
// console.log('Express HTTP server listening on port '+ httpApp.get('port'));

//});

// Comment on development ]]
// Start server
//server.listen(config.port, config.ip, function () {
//  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
//});

//server.timeout = 900000;
// Expose app
exports = module.exports = app;



