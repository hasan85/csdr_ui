/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
//process.env.NODE_ENV = 'production';
console.log(process.env.NODE_ENV);

var express = require('express');
var https = require('https');
var http = require('http');
var fs = require('fs');

var config = require('./config/environment');

// Setup server
var app = express();
var httpServer = http.createServer(app);

var socketio = require('socket.io')(httpServer, {
  serveClient: (config.env !== 'production'),
  path: '/socket.io-client'
});

var socketioAsanImza = require('socket.io')(httpServer, {
  serveClient: (config.env !== 'production'),
  path: '/socket-asan-imza'
});

require('./config/socketio')(socketio);
require('./config/socketioAsanImza')(socketioAsanImza);
require('./config/express')(app);
require('./routes')(app, socketio, socketioAsanImza);

httpServer.listen(config.port, function() {
  console.log('Express HTTP server listening on port '+ config.port, config.ip);
});


httpServer.timeout = 600000;

module.exports = module.exports = app;
