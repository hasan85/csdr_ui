/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var auth = require('./auth/authorization');
var express = require('express');
var path = require('path');
var config = require('./config/environment');

module.exports = function (app, socketio,socketioAsanImza) {


  // This file must be called first!!

  require('./auth/index')(app);

  require('./socket-server/asanImza')(app, socketioAsanImza);
  require('./socket-server/index')(app, socketio);

  // Serve static files
  app.use('/', express.static(path.join(config.root, 'server/static')));

  // Ensure user logged otherwise don't serve static files
  app.use('/', auth.requiresLogin);

  // Serve static files
  app.use('/', express.static(path.join(config.root, config.expressStaticFilesPath)));

  //Api routes

  app.use('/api/main', require('./api/main'));
  app.use('/api/common', require('./api/common'));
  app.use('/api/reference-data', require('./api/reference-data'));
  app.use('/api/data-validation', require('./api/data-validation'));
  app.use('/api/persons', require('./api/persons'));
  app.use('/api/records', require('./api/records'));
  app.use('/api/task-services', require('./api/task-services'));
  app.use('/api/corporate-actions', require('./api/corporate-actions'));
  app.use('/api/instruments', require('./api/instruments'));
  app.use('/api/reports', require('./api/reports'));
  app.use('/api/tds', require('./api/temp-data-services'));
  app.use('/api/configurations', require('./api/configurations'));
  app.use('/api/clearing', require('./api/clearing'));
  app.use('/api/settlement', require('./api/settlement'));
  app.use('/api/reportsDataServices', require('./api/reportsDataServices'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(auth.requiresLogin, function (req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });

};
